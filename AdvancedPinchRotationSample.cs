﻿using System;
using UnityEngine;

public class AdvancedPinchRotationSample : SampleBase
{
    private Material originalMaterial;
    public Material pinchAndRotationMaterial;
    public PinchGestureRecognizer pinchGesture;
    public Material pinchMaterial;
    public float pinchScaleFactor = 0.02f;
    public RotationGestureRecognizer rotationGesture;
    public Material rotationMaterial;
    public Transform target;

    private bool CanBeginPinch(GestureRecognizer gr, FingerGestures.IFingerList touches)
    {
        return !this.rotationGesture.IsActive;
    }

    private bool CanBeginRotation(GestureRecognizer gr, FingerGestures.IFingerList touches)
    {
        return !this.pinchGesture.IsActive;
    }

    private void Gesture_OnStateChanged(GestureRecognizer source)
    {
        if ((source.PreviousState == GestureRecognizer.GestureState.Ready) && (source.State == GestureRecognizer.GestureState.InProgress))
        {
            base.UI.StatusText = source + " gesture started";
        }
        else if (source.PreviousState == GestureRecognizer.GestureState.InProgress)
        {
            if (source.State == GestureRecognizer.GestureState.Failed)
            {
                base.UI.StatusText = source + " gesture failed";
            }
            else if (source.State == GestureRecognizer.GestureState.Recognized)
            {
                base.UI.StatusText = source + " gesture ended";
            }
        }
        this.UpdateTargetMaterial();
    }

    protected override string GetHelpText()
    {
        return "This sample demonstrates advanced use of the GestureRecognizer classes for Pinch and Rotation";
    }

    private void OnPinchMove(PinchGestureRecognizer source)
    {
        base.UI.StatusText = "Pinch updated by " + source.Delta + " degrees";
        Transform transform = this.target.transform;
        transform.localScale += (Vector3) ((source.Delta * this.pinchScaleFactor) * Vector3.one);
    }

    private void OnRotationMove(RotationGestureRecognizer source)
    {
        base.UI.StatusText = "Rotation updated by " + source.RotationDelta + " degrees";
        this.target.Rotate(0f, 0f, source.RotationDelta);
    }

    protected override void Start()
    {
        base.Start();
        base.UI.StatusText = "Use two fingers anywhere on the screen to rotate and scale the green object.";
        this.originalMaterial = this.target.renderer.sharedMaterial;
        this.pinchGesture.OnStateChanged += new FGComponent.EventDelegate<GestureRecognizer>(this.Gesture_OnStateChanged);
        this.pinchGesture.OnPinchMove += new FGComponent.EventDelegate<PinchGestureRecognizer>(this.OnPinchMove);
        this.pinchGesture.SetCanBeginDelegate(new GestureRecognizer.CanBeginDelegate(this.CanBeginPinch));
        this.rotationGesture.OnStateChanged += new FGComponent.EventDelegate<GestureRecognizer>(this.Gesture_OnStateChanged);
        this.rotationGesture.OnRotationMove += new FGComponent.EventDelegate<RotationGestureRecognizer>(this.OnRotationMove);
        this.rotationGesture.SetCanBeginDelegate(new GestureRecognizer.CanBeginDelegate(this.CanBeginRotation));
    }

    private void UpdateTargetMaterial()
    {
        Material pinchAndRotationMaterial;
        if (this.pinchGesture.IsActive && this.rotationGesture.IsActive)
        {
            pinchAndRotationMaterial = this.pinchAndRotationMaterial;
        }
        else if (this.pinchGesture.IsActive)
        {
            pinchAndRotationMaterial = this.pinchMaterial;
        }
        else if (this.rotationGesture.IsActive)
        {
            pinchAndRotationMaterial = this.rotationMaterial;
        }
        else
        {
            pinchAndRotationMaterial = this.originalMaterial;
        }
        this.target.renderer.sharedMaterial = pinchAndRotationMaterial;
    }
}

