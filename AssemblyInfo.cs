﻿// Assembly Assembly-CSharp, Version 1.0.0.0

[assembly: System.Reflection.AssemblyTitle("HOTween")]
[assembly: System.Reflection.AssemblyDescription("A Tween Engine for Unity")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyCompany("Holoville")]
[assembly: System.Reflection.AssemblyProduct("HOTween")]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 Daniele Giardini 2012")]
[assembly: System.Reflection.AssemblyTrademark("Daniele Giardini - Holoville")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Runtime.InteropServices.Guid("34e39807-3eb0-4395-9a97-9818c02a6e43")]
[assembly: System.Reflection.AssemblyFileVersion("1.0.0.0")]
[assembly: System.Runtime.CompilerServices.Extension]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]

