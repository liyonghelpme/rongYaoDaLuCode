﻿using System;
using UnityEngine;

public class CSharpSkeleton : MonoBehaviour
{
    private void FingerGestures_OnDragBegin(Vector2 fingerPos, Vector2 startPos)
    {
    }

    private void FingerGestures_OnDragEnd(Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnDragMove(Vector2 fingerPos, Vector2 delta)
    {
    }

    private void FingerGestures_OnFingerDown(int fingerIndex, Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnFingerDragBegin(int fingerIndex, Vector2 fingerPos, Vector2 startPos)
    {
    }

    private void FingerGestures_OnFingerDragEnd(int fingerIndex, Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnFingerDragMove(int fingerIndex, Vector2 fingerPos, Vector2 delta)
    {
    }

    private void FingerGestures_OnFingerLongPress(int fingerIndex, Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnFingerMove(int fingerIndex, Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnFingerMoveBegin(int fingerIndex, Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnFingerMoveEnd(int fingerIndex, Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnFingerStationary(int fingerIndex, Vector2 fingerPos, float elapsedTime)
    {
    }

    private void FingerGestures_OnFingerStationaryBegin(int fingerIndex, Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnFingerStationaryEnd(int fingerIndex, Vector2 fingerPos, float elapsedTime)
    {
    }

    private void FingerGestures_OnFingerSwipe(int fingerIndex, Vector2 startPos, FingerGestures.SwipeDirection direction, float velocity)
    {
    }

    private void FingerGestures_OnFingerTap(int fingerIndex, Vector2 fingerPos, int tapCount)
    {
    }

    private void FingerGestures_OnFingerUp(int fingerIndex, Vector2 fingerPos, float timeHeldDown)
    {
    }

    private void FingerGestures_OnLongPress(Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnPinchBegin(Vector2 fingerPos1, Vector2 fingerPos2)
    {
    }

    private void FingerGestures_OnPinchEnd(Vector2 fingerPos1, Vector2 fingerPos2)
    {
    }

    private void FingerGestures_OnPinchMove(Vector2 fingerPos1, Vector2 fingerPos2, float delta)
    {
    }

    private void FingerGestures_OnRotationBegin(Vector2 fingerPos1, Vector2 fingerPos2)
    {
    }

    private void FingerGestures_OnRotationEnd(Vector2 fingerPos1, Vector2 fingerPos2, float totalRotationAngle)
    {
    }

    private void FingerGestures_OnRotationMove(Vector2 fingerPos1, Vector2 fingerPos2, float rotationAngleDelta)
    {
    }

    private void FingerGestures_OnSwipe(Vector2 startPos, FingerGestures.SwipeDirection direction, float velocity)
    {
    }

    private void FingerGestures_OnTap(Vector2 fingerPos, int tapCount)
    {
    }

    private void FingerGestures_OnTwoFingerDragBegin(Vector2 fingerPos, Vector2 startPos)
    {
    }

    private void FingerGestures_OnTwoFingerDragEnd(Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnTwoFingerDragMove(Vector2 fingerPos, Vector2 delta)
    {
    }

    private void FingerGestures_OnTwoFingerLongPress(Vector2 fingerPos)
    {
    }

    private void FingerGestures_OnTwoFingerSwipe(Vector2 startPos, FingerGestures.SwipeDirection direction, float velocity)
    {
    }

    private void FingerGestures_OnTwoFingerTap(Vector2 fingerPos, int tapCount)
    {
    }

    private void OnDisable()
    {
        FingerGestures.OnFingerDown -= new FingerGestures.FingerDownEventHandler(this.FingerGestures_OnFingerDown);
        FingerGestures.OnFingerStationaryBegin -= new FingerGestures.FingerStationaryBeginEventHandler(this.FingerGestures_OnFingerStationaryBegin);
        FingerGestures.OnFingerStationary -= new FingerGestures.FingerStationaryEventHandler(this.FingerGestures_OnFingerStationary);
        FingerGestures.OnFingerStationaryEnd -= new FingerGestures.FingerStationaryEndEventHandler(this.FingerGestures_OnFingerStationaryEnd);
        FingerGestures.OnFingerMoveBegin -= new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveBegin);
        FingerGestures.OnFingerMove -= new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMove);
        FingerGestures.OnFingerMoveEnd -= new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveEnd);
        FingerGestures.OnFingerUp -= new FingerGestures.FingerUpEventHandler(this.FingerGestures_OnFingerUp);
        FingerGestures.OnFingerLongPress -= new FingerGestures.FingerLongPressEventHandler(this.FingerGestures_OnFingerLongPress);
        FingerGestures.OnFingerTap -= new FingerGestures.FingerTapEventHandler(this.FingerGestures_OnFingerTap);
        FingerGestures.OnFingerSwipe -= new FingerGestures.FingerSwipeEventHandler(this.FingerGestures_OnFingerSwipe);
        FingerGestures.OnFingerDragBegin -= new FingerGestures.FingerDragBeginEventHandler(this.FingerGestures_OnFingerDragBegin);
        FingerGestures.OnFingerDragMove -= new FingerGestures.FingerDragMoveEventHandler(this.FingerGestures_OnFingerDragMove);
        FingerGestures.OnFingerDragEnd -= new FingerGestures.FingerDragEndEventHandler(this.FingerGestures_OnFingerDragEnd);
        FingerGestures.OnLongPress -= new FingerGestures.LongPressEventHandler(this.FingerGestures_OnLongPress);
        FingerGestures.OnTap -= new FingerGestures.TapEventHandler(this.FingerGestures_OnTap);
        FingerGestures.OnSwipe -= new FingerGestures.SwipeEventHandler(this.FingerGestures_OnSwipe);
        FingerGestures.OnDragBegin -= new FingerGestures.DragBeginEventHandler(this.FingerGestures_OnDragBegin);
        FingerGestures.OnDragMove -= new FingerGestures.DragMoveEventHandler(this.FingerGestures_OnDragMove);
        FingerGestures.OnDragEnd -= new FingerGestures.DragEndEventHandler(this.FingerGestures_OnDragEnd);
        FingerGestures.OnPinchBegin -= new FingerGestures.PinchEventHandler(this.FingerGestures_OnPinchBegin);
        FingerGestures.OnPinchMove -= new FingerGestures.PinchMoveEventHandler(this.FingerGestures_OnPinchMove);
        FingerGestures.OnPinchEnd -= new FingerGestures.PinchEventHandler(this.FingerGestures_OnPinchEnd);
        FingerGestures.OnRotationBegin -= new FingerGestures.RotationBeginEventHandler(this.FingerGestures_OnRotationBegin);
        FingerGestures.OnRotationMove -= new FingerGestures.RotationMoveEventHandler(this.FingerGestures_OnRotationMove);
        FingerGestures.OnRotationEnd -= new FingerGestures.RotationEndEventHandler(this.FingerGestures_OnRotationEnd);
        FingerGestures.OnTwoFingerLongPress -= new FingerGestures.LongPressEventHandler(this.FingerGestures_OnTwoFingerLongPress);
        FingerGestures.OnTwoFingerTap -= new FingerGestures.TapEventHandler(this.FingerGestures_OnTwoFingerTap);
        FingerGestures.OnTwoFingerSwipe -= new FingerGestures.SwipeEventHandler(this.FingerGestures_OnTwoFingerSwipe);
        FingerGestures.OnTwoFingerDragBegin -= new FingerGestures.DragBeginEventHandler(this.FingerGestures_OnTwoFingerDragBegin);
        FingerGestures.OnTwoFingerDragMove -= new FingerGestures.DragMoveEventHandler(this.FingerGestures_OnTwoFingerDragMove);
        FingerGestures.OnTwoFingerDragEnd -= new FingerGestures.DragEndEventHandler(this.FingerGestures_OnTwoFingerDragEnd);
    }

    private void OnEnable()
    {
        FingerGestures.OnFingerDown += new FingerGestures.FingerDownEventHandler(this.FingerGestures_OnFingerDown);
        FingerGestures.OnFingerStationaryBegin += new FingerGestures.FingerStationaryBeginEventHandler(this.FingerGestures_OnFingerStationaryBegin);
        FingerGestures.OnFingerStationary += new FingerGestures.FingerStationaryEventHandler(this.FingerGestures_OnFingerStationary);
        FingerGestures.OnFingerStationaryEnd += new FingerGestures.FingerStationaryEndEventHandler(this.FingerGestures_OnFingerStationaryEnd);
        FingerGestures.OnFingerMoveBegin += new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveBegin);
        FingerGestures.OnFingerMove += new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMove);
        FingerGestures.OnFingerMoveEnd += new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveEnd);
        FingerGestures.OnFingerUp += new FingerGestures.FingerUpEventHandler(this.FingerGestures_OnFingerUp);
        FingerGestures.OnFingerLongPress += new FingerGestures.FingerLongPressEventHandler(this.FingerGestures_OnFingerLongPress);
        FingerGestures.OnFingerTap += new FingerGestures.FingerTapEventHandler(this.FingerGestures_OnFingerTap);
        FingerGestures.OnFingerSwipe += new FingerGestures.FingerSwipeEventHandler(this.FingerGestures_OnFingerSwipe);
        FingerGestures.OnFingerDragBegin += new FingerGestures.FingerDragBeginEventHandler(this.FingerGestures_OnFingerDragBegin);
        FingerGestures.OnFingerDragMove += new FingerGestures.FingerDragMoveEventHandler(this.FingerGestures_OnFingerDragMove);
        FingerGestures.OnFingerDragEnd += new FingerGestures.FingerDragEndEventHandler(this.FingerGestures_OnFingerDragEnd);
        FingerGestures.OnLongPress += new FingerGestures.LongPressEventHandler(this.FingerGestures_OnLongPress);
        FingerGestures.OnTap += new FingerGestures.TapEventHandler(this.FingerGestures_OnTap);
        FingerGestures.OnSwipe += new FingerGestures.SwipeEventHandler(this.FingerGestures_OnSwipe);
        FingerGestures.OnDragBegin += new FingerGestures.DragBeginEventHandler(this.FingerGestures_OnDragBegin);
        FingerGestures.OnDragMove += new FingerGestures.DragMoveEventHandler(this.FingerGestures_OnDragMove);
        FingerGestures.OnDragEnd += new FingerGestures.DragEndEventHandler(this.FingerGestures_OnDragEnd);
        FingerGestures.OnPinchBegin += new FingerGestures.PinchEventHandler(this.FingerGestures_OnPinchBegin);
        FingerGestures.OnPinchMove += new FingerGestures.PinchMoveEventHandler(this.FingerGestures_OnPinchMove);
        FingerGestures.OnPinchEnd += new FingerGestures.PinchEventHandler(this.FingerGestures_OnPinchEnd);
        FingerGestures.OnRotationBegin += new FingerGestures.RotationBeginEventHandler(this.FingerGestures_OnRotationBegin);
        FingerGestures.OnRotationMove += new FingerGestures.RotationMoveEventHandler(this.FingerGestures_OnRotationMove);
        FingerGestures.OnRotationEnd += new FingerGestures.RotationEndEventHandler(this.FingerGestures_OnRotationEnd);
        FingerGestures.OnTwoFingerLongPress += new FingerGestures.LongPressEventHandler(this.FingerGestures_OnTwoFingerLongPress);
        FingerGestures.OnTwoFingerTap += new FingerGestures.TapEventHandler(this.FingerGestures_OnTwoFingerTap);
        FingerGestures.OnTwoFingerSwipe += new FingerGestures.SwipeEventHandler(this.FingerGestures_OnTwoFingerSwipe);
        FingerGestures.OnTwoFingerDragBegin += new FingerGestures.DragBeginEventHandler(this.FingerGestures_OnTwoFingerDragBegin);
        FingerGestures.OnTwoFingerDragMove += new FingerGestures.DragMoveEventHandler(this.FingerGestures_OnTwoFingerDragMove);
        FingerGestures.OnTwoFingerDragEnd += new FingerGestures.DragEndEventHandler(this.FingerGestures_OnTwoFingerDragEnd);
    }
}

