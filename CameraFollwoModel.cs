﻿using System;
using UnityEngine;

public class CameraFollwoModel : MonoBehaviour
{
    private Vector3 delta;
    private Vector3 position;
    public Transform target;

    public void setTarget(Transform target)
    {
        this.target = target;
        base.transform.localPosition = new Vector3(0f, 0f, -3.593668f);
        base.transform.localEulerAngles = new Vector3(20.15945f, 0f, 0f);
        this.delta = this.target.position - base.transform.position;
        base.transform.position = target.position - this.delta;
        Debug.Log(string.Concat(new object[] { this.target.localPosition, " ", this.delta, " ", target.localPosition, target.parent.name }));
    }

    private void Start()
    {
        this.position = base.transform.localPosition;
        if (this.target != null)
        {
            this.delta = this.target.position - base.transform.position;
        }
    }

    private void Update()
    {
        if (this.target != null)
        {
            base.transform.position = this.target.position - this.delta;
        }
    }
}

