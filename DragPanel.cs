﻿using System;
using UnityEngine;

public class DragPanel : MonoBehaviour
{
    private float actual_time;
    private Bounds bounds;
    public DragType dragType = DragType.Vertical;
    private Vector3 interval;
    private bool is_pressed;
    private bool is_tranform;
    private Vector3 last_position;
    private Vector3 localPosition;
    private Transform mTrans;
    public int pages = 1;
    private UIPanel panel;
    private Plane plane;
    private float real_time;
    public bool scrollByPage;
    private float start_time;
    private Vector3 target_position;
    private float time_delta;

    private void Awake()
    {
        this.mTrans = base.transform;
        this.localPosition = this.mTrans.localPosition;
        this.panel = base.GetComponent<UIPanel>();
    }

    public void MoveAbsolute(Vector3 absolute)
    {
        Vector3 vector = base.transform.InverseTransformPoint(absolute);
        Vector3 vector2 = base.transform.InverseTransformPoint(Vector3.zero);
        this.MoveRelative(vector - vector2);
    }

    public void MoveRelative(Vector3 relative)
    {
        Vector3 localPosition = this.mTrans.localPosition;
        Vector4 clipRange = this.panel.clipRange;
        if (this.dragType == DragType.Both)
        {
            localPosition.y += relative.y;
            localPosition.x += relative.x;
            clipRange.y -= relative.y;
            clipRange.x -= relative.x;
        }
        else if (this.dragType == DragType.Vertical)
        {
            localPosition.y += relative.y;
            clipRange.y -= relative.y;
        }
        else if (this.dragType == DragType.Horizontal)
        {
            localPosition.x += relative.x;
            clipRange.x -= relative.x;
        }
        this.mTrans.localPosition = localPosition;
        this.panel.clipRange = clipRange;
    }

    private void OnDrag(Vector2 delta)
    {
        Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos);
        float enter = 0f;
        if (this.plane.Raycast(ray, out enter))
        {
            Vector3 point = ray.GetPoint(enter);
            Vector3 direction = point - this.last_position;
            this.last_position = point;
            if ((direction.x != 0f) || (direction.y != 0f))
            {
                direction = this.mTrans.InverseTransformDirection(direction);
                direction.Scale(Vector3.one);
                direction = this.mTrans.TransformDirection(direction);
            }
            this.MoveAbsolute(direction);
        }
    }

    private void OnPress(bool pressed)
    {
        if (pressed)
        {
            this.last_position = UICamera.lastHit.point;
            this.plane = new Plane((Vector3) (this.mTrans.rotation * Vector3.back), this.last_position);
            this.is_tranform = false;
        }
        else
        {
            this.is_tranform = true;
            Vector3 zero = Vector3.zero;
            if (!this.scrollByPage)
            {
                zero = this.panel.CalculateConstrainOffset(this.bounds.min, this.bounds.max);
                if (zero.magnitude > 0.001f)
                {
                    this.target_position = this.mTrans.localPosition + zero;
                }
            }
            else
            {
                int num = 1;
                this.interval = (Vector3) ((this.bounds.max - this.bounds.min) / ((float) this.pages));
                if (this.dragType == DragType.Vertical)
                {
                    num = (int) Mathf.Ceil((this.mTrans.localPosition.y - this.localPosition.y) / this.interval.y);
                    num = Mathf.Clamp(num, 1, this.pages);
                    zero.x = this.localPosition.x;
                    zero.y = this.localPosition.y + ((num - 1) * this.interval.y);
                }
                else if (this.dragType == DragType.Horizontal)
                {
                    num = (int) Mathf.Ceil((this.mTrans.localPosition.x - this.localPosition.x) / this.interval.x);
                    num = Mathf.Clamp(num, 1, this.pages);
                    zero.y = this.localPosition.y;
                    zero.x = this.localPosition.x + ((num - 1) * this.interval.x);
                }
                this.target_position = zero;
            }
        }
    }

    public void refreshBoxCollider()
    {
        this.bounds = NGUIMath.CalculateRelativeWidgetBounds(this.mTrans, this.mTrans);
        Debug.Log(this.bounds + " d sd ds ");
        NGUITools.AddWidgetCollider(base.gameObject);
    }

    private void Start()
    {
    }

    private void Update()
    {
        if (this.is_tranform)
        {
            this.UpdateRealTimeDelta();
            Vector3 vector = Vector3.Lerp(this.mTrans.localPosition, this.target_position, this.time_delta * 4f);
            this.MoveRelative(vector - this.mTrans.localPosition);
            if (Vector3.Magnitude(this.target_position - this.mTrans.localPosition) <= 1E-05f)
            {
                this.is_tranform = false;
            }
        }
    }

    protected void UpdateRealTimeDelta()
    {
        this.real_time = Time.realtimeSinceStartup;
        float b = this.real_time - this.start_time;
        this.actual_time += Mathf.Max(0f, b);
        this.time_delta = 0.001f * Mathf.Round(this.actual_time * 1000f);
        this.actual_time -= this.time_delta;
        if (this.actual_time > 0.25f)
        {
            this.time_delta = 0.25f;
        }
        this.start_time = this.real_time;
    }

    public enum DragType
    {
        Both,
        Vertical,
        Horizontal
    }
}

