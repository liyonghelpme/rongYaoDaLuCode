﻿using System;
using UnityEngine;

public class DragTrail : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public LineRenderer lineRendererPrefab;

    private void OnDragBegin()
    {
        this.lineRenderer.enabled = true;
        this.lineRenderer.SetPosition(0, base.transform.position);
        this.lineRenderer.SetPosition(1, base.transform.position);
        this.lineRenderer.SetWidth(0.01f, base.transform.localScale.x);
    }

    private void OnDragEnd()
    {
        this.lineRenderer.enabled = false;
    }

    private void Start()
    {
        this.lineRenderer = UnityEngine.Object.Instantiate(this.lineRendererPrefab, base.transform.position, base.transform.rotation) as LineRenderer;
        this.lineRenderer.transform.parent = base.transform;
        this.lineRenderer.enabled = false;
    }

    private void Update()
    {
        if (this.lineRenderer.enabled)
        {
            this.lineRenderer.SetPosition(1, base.transform.position);
        }
    }
}

