﻿using com.game.data;
using com.game.manager;
using com.game.module.core;
using com.game.module.Dungeon;
using com.game.Public.Message;
using System;
using System.Collections.Generic;
using UnityEngine;

public class DungeonChapterItemView
{
    private Button _btn;
    private int _dungeonId;
    private DungeonInfo _dungeonInfo;
    private SysDungeonVo _dungeonSysVo;
    private GameObject _go;
    private UISprite _icon;
    private List<UISprite> _stars;
    private UISprite background;
    private UISprite iconII;
    private static UIAtlas s_atlas;

    public DungeonChapterItemView(GameObject go)
    {
        this._go = go;
        this._btn = this._go.GetComponent<Button>();
        this._btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        this._stars = new List<UISprite>();
        for (int i = 1; i <= 3; i++)
        {
            this._stars.Add(NGUITools.FindInChild<UISprite>(this._go, "star" + i));
        }
        this._icon = NGUITools.FindInChild<UISprite>(this._go, "icon");
        this.background = NGUITools.FindInChild<UISprite>(this._go, "background");
        this.iconII = NGUITools.FindInChild<UISprite>(this._go, "iconII");
        if ((null == s_atlas) && (null != Singleton<DungeonChapterView>.Instance.gameObject))
        {
            s_atlas = NGUITools.FindInChild<UISprite>(Singleton<DungeonChapterView>.Instance.gameObject, "list/selectFlag").atlas;
        }
    }

    private void OnClick(GameObject go)
    {
        if (!Singleton<DungeonMode>.Instance.CanEnterDungeon((uint) this._dungeonId))
        {
            MessageManager.Show("该关卡还未开启！");
        }
        else
        {
            Singleton<DungeonInfoView>.Instance.Show(this._dungeonId);
        }
    }

    private void UpdateView()
    {
        this.background.spriteName = (this._dungeonSysVo.subtype != 1) ? "jingYhead" : "puThead";
        this.iconII.SetActive(this._dungeonSysVo.subtype == 2);
        bool state = (this._dungeonInfo != null) && (this._dungeonInfo.duplicateInfo.grade > 0);
        int num = state ? this._dungeonInfo.duplicateInfo.grade : 0;
        for (int i = 0; i < this._stars.Count; i++)
        {
            this._stars[i].SetActive(state);
            if (state)
            {
                this._stars[i].spriteName = "xingxing";
                if (i < num)
                {
                    this._stars[i].ShowAsMyself();
                }
                else
                {
                    this._stars[i].ShowAsGray();
                }
            }
        }
        if (!Singleton<DungeonMode>.Instance.CanEnterDungeon((uint) this._dungeonId))
        {
            this._icon.atlas = s_atlas;
            this._icon.spriteName = "locked";
        }
        else
        {
            this._icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this._icon.spriteName = this._dungeonSysVo.icon_type.ToString();
        }
        this._icon.MakePixelPerfect();
    }

    public Button Btn
    {
        get
        {
            return this._btn;
        }
    }

    public int dungeonId
    {
        get
        {
            return this._dungeonId;
        }
        set
        {
            this._dungeonId = value;
            this._dungeonInfo = Singleton<DungeonMode>.Instance.getDungeonInfo((uint) this._dungeonId);
            this._dungeonSysVo = BaseDataMgr.instance.getDungeon((uint) this._dungeonId);
            this.UpdateView();
        }
    }

    public GameObject go
    {
        get
        {
            return this._go;
        }
    }
}

