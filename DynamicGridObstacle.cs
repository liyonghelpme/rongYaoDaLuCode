﻿using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DynamicGridObstacle : MonoBehaviour
{
    public float checkTime = 0.2f;
    private Collider col;
    private bool isWaitingForUpdate;
    private Bounds prevBounds;
    public float updateError = 1f;

    public float BoundsVolume(Bounds b)
    {
        return Math.Abs((float) ((b.size.x * b.size.y) * b.size.z));
    }

    public void DoUpdateGraphs()
    {
        if (this.col != null)
        {
            this.isWaitingForUpdate = false;
            Bounds b = this.col.bounds;
            Bounds bounds2 = b;
            bounds2.Encapsulate(this.prevBounds);
            if (this.BoundsVolume(bounds2) < (this.BoundsVolume(b) + this.BoundsVolume(this.prevBounds)))
            {
                AstarPath.active.UpdateGraphs(bounds2);
            }
            else
            {
                AstarPath.active.UpdateGraphs(this.prevBounds);
                AstarPath.active.UpdateGraphs(b);
            }
            this.prevBounds = b;
        }
    }

    public void OnDestroy()
    {
        if (AstarPath.active != null)
        {
            GraphUpdateObject ob = new GraphUpdateObject(this.prevBounds);
            AstarPath.active.UpdateGraphs(ob);
        }
    }

    private void Start()
    {
        this.col = base.collider;
        if (base.collider == null)
        {
            UnityEngine.Debug.LogError("A collider must be attached to the GameObject for DynamicGridObstacle to work");
        }
        base.StartCoroutine(this.UpdateGraphs());
    }

    [DebuggerHidden]
    private IEnumerator UpdateGraphs()
    {
        return new <UpdateGraphs>c__IteratorF { <>f__this = this };
    }

    [CompilerGenerated]
    private sealed class <UpdateGraphs>c__IteratorF : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal DynamicGridObstacle <>f__this;
        internal Vector3 <maxDiff>__3;
        internal Bounds <merged>__1;
        internal Vector3 <minDiff>__2;
        internal Bounds <newBounds>__0;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    if ((this.<>f__this.col != null) && (AstarPath.active != null))
                    {
                        goto Label_0209;
                    }
                    UnityEngine.Debug.LogWarning("No collider is attached to the GameObject. Canceling check");
                    goto Label_0230;

                case 1:
                    break;

                case 2:
                    goto Label_0209;

                default:
                    goto Label_0230;
            }
        Label_0086:
            if (this.<>f__this.isWaitingForUpdate)
            {
                this.$current = new WaitForSeconds(this.<>f__this.checkTime);
                this.$PC = 1;
            }
            else
            {
                this.<newBounds>__0 = this.<>f__this.col.bounds;
                this.<merged>__1 = this.<newBounds>__0;
                this.<merged>__1.Encapsulate(this.<>f__this.prevBounds);
                this.<minDiff>__2 = this.<merged>__1.min - this.<newBounds>__0.min;
                this.<maxDiff>__3 = this.<merged>__1.max - this.<newBounds>__0.max;
                if ((((Mathf.Abs(this.<minDiff>__2.x) > this.<>f__this.updateError) || (Mathf.Abs(this.<minDiff>__2.y) > this.<>f__this.updateError)) || ((Mathf.Abs(this.<minDiff>__2.z) > this.<>f__this.updateError) || (Mathf.Abs(this.<maxDiff>__3.x) > this.<>f__this.updateError))) || ((Mathf.Abs(this.<maxDiff>__3.y) > this.<>f__this.updateError) || (Mathf.Abs(this.<maxDiff>__3.z) > this.<>f__this.updateError)))
                {
                    this.<>f__this.isWaitingForUpdate = true;
                    this.<>f__this.DoUpdateGraphs();
                }
                this.$current = new WaitForSeconds(this.<>f__this.checkTime);
                this.$PC = 2;
            }
            return true;
        Label_0209:
            if (this.<>f__this.col != null)
            {
                goto Label_0086;
            }
            this.<>f__this.OnDestroy();
            this.$PC = -1;
        Label_0230:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }
}

