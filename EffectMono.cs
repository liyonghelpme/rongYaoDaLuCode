﻿using System;
using UnityEngine;

public class EffectMono : MonoBehaviour
{
    public bool isLoop;
    public bool isNeedMirror;
    [HideInInspector]
    public byte launcherForce;
    private float lifeTime;
    public float LifeTime;
    private float timeCount;

    private float getLifeTime()
    {
        float length = 0f;
        foreach (ParticleSystem system in base.gameObject.GetComponentsInChildren<ParticleSystem>())
        {
            if (system.isPlaying && (length < (system.duration + system.startLifetime)))
            {
                length = system.duration + system.startLifetime;
            }
        }
        Animation[] componentsInChildren = base.gameObject.GetComponentsInChildren<Animation>();
        if (componentsInChildren.Length > 0)
        {
            foreach (Animation animation in componentsInChildren)
            {
                if (((animation.clip != null) && animation.isPlaying) && (length < animation.clip.length))
                {
                    length = animation.clip.length;
                }
            }
        }
        return length;
    }

    private void Start()
    {
        if (this.isNeedMirror && (this.launcherForce == 1))
        {
            base.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        if (!this.isLoop)
        {
            this.lifeTime = this.LifeTime;
            if (this.lifeTime <= 0f)
            {
                this.lifeTime = this.getLifeTime();
            }
        }
    }

    private void Update()
    {
        if (!this.isLoop)
        {
            this.timeCount += Time.deltaTime;
            if (this.timeCount > this.lifeTime)
            {
                this.isLoop = true;
                UnityEngine.Object.DestroyImmediate(base.gameObject);
            }
        }
    }
}

