﻿using System;
using UnityEngine;

public class EmitParticles : MonoBehaviour
{
    public Transform down;
    public ParticleEmitter emitter;
    public Transform left;
    public Transform right;
    public Transform up;

    public void Emit()
    {
        this.emitter.Emit();
    }

    public void EmitDown()
    {
        this.emitter.transform.rotation = this.down.rotation;
        this.Emit();
    }

    public void EmitLeft()
    {
        this.emitter.transform.rotation = this.left.rotation;
        this.Emit();
    }

    public void EmitRight()
    {
        this.emitter.transform.rotation = this.right.rotation;
        this.Emit();
    }

    public void EmitUp()
    {
        this.emitter.transform.rotation = this.up.rotation;
        this.Emit();
    }
}

