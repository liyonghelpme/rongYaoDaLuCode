﻿namespace FastDynamicMemberAccessor
{
    using System;

    internal sealed class ChainingAccessor : IMemberAccessor
    {
        private readonly IMemberAccessor _chain;
        private readonly IMemberAccessor _pimp;

        internal ChainingAccessor(IMemberAccessor impl, IMemberAccessor chain)
        {
            this._pimp = impl;
            this._chain = chain;
        }

        public object Get(object target)
        {
            return this._pimp.Get(this._chain.Get(target));
        }

        public void Set(object target, object value)
        {
            this._pimp.Set(this._chain.Get(target), value);
        }
    }
}

