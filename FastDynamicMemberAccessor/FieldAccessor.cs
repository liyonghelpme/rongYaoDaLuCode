﻿namespace FastDynamicMemberAccessor
{
    using System;
    using System.Reflection;
    using System.Reflection.Emit;

    internal class FieldAccessor : MemberAccessor
    {
        private readonly bool _canRead;
        private readonly bool _canWrite;
        private readonly Type _propertyType;

        internal FieldAccessor(FieldInfo fieldInfo) : base(fieldInfo)
        {
            this._canRead = true;
            this._canWrite = !(fieldInfo.IsLiteral || fieldInfo.IsInitOnly);
            this._propertyType = fieldInfo.FieldType;
        }

        protected override void _EmitGetter(TypeBuilder myType)
        {
            Type[] parameterTypes = new Type[] { typeof(object) };
            Type returnType = typeof(object);
            ILGenerator iLGenerator = myType.DefineMethod("Get", MethodAttributes.Virtual | MethodAttributes.Public, returnType, parameterTypes).GetILGenerator();
            FieldInfo field = base._targetType.GetField(base._fieldName);
            if (field != null)
            {
                iLGenerator.DeclareLocal(typeof(object));
                iLGenerator.Emit(OpCodes.Ldarg_1);
                iLGenerator.Emit(OpCodes.Castclass, base._targetType);
                iLGenerator.Emit(OpCodes.Ldfld, field);
                if (field.FieldType.IsValueType)
                {
                    iLGenerator.Emit(OpCodes.Box, field.FieldType);
                }
                iLGenerator.Emit(OpCodes.Stloc_0);
                iLGenerator.Emit(OpCodes.Ldloc_0);
            }
            else
            {
                iLGenerator.ThrowException(typeof(MissingMethodException));
            }
            iLGenerator.Emit(OpCodes.Ret);
        }

        protected override void _EmitSetter(TypeBuilder myType)
        {
            Type[] parameterTypes = new Type[] { typeof(object), typeof(object) };
            Type returnType = null;
            ILGenerator iLGenerator = myType.DefineMethod("Set", MethodAttributes.Virtual | MethodAttributes.Public, returnType, parameterTypes).GetILGenerator();
            FieldInfo field = base._targetType.GetField(base._fieldName);
            if (field != null)
            {
                Type fieldType = field.FieldType;
                iLGenerator.DeclareLocal(fieldType);
                iLGenerator.Emit(OpCodes.Ldarg_1);
                iLGenerator.Emit(OpCodes.Castclass, base._targetType);
                iLGenerator.Emit(OpCodes.Ldarg_2);
                if (fieldType.IsValueType)
                {
                    iLGenerator.Emit(OpCodes.Unbox, fieldType);
                    if (MemberAccessor.s_TypeHash[fieldType] != null)
                    {
                        OpCode opcode = (OpCode) MemberAccessor.s_TypeHash[fieldType];
                        iLGenerator.Emit(opcode);
                    }
                    else
                    {
                        iLGenerator.Emit(OpCodes.Ldobj, fieldType);
                    }
                }
                else
                {
                    iLGenerator.Emit(OpCodes.Castclass, fieldType);
                }
                iLGenerator.Emit(OpCodes.Stfld, field);
            }
            else
            {
                iLGenerator.ThrowException(typeof(MissingMethodException));
            }
            iLGenerator.Emit(OpCodes.Ret);
        }

        internal override bool CanRead
        {
            get
            {
                return this._canRead;
            }
        }

        internal override bool CanWrite
        {
            get
            {
                return this._canWrite;
            }
        }

        internal override Type MemberType
        {
            get
            {
                return this._propertyType;
            }
        }
    }
}

