﻿namespace FastDynamicMemberAccessor
{
    using System;

    internal class MemberAccessorException : Exception
    {
        internal MemberAccessorException(string message) : base(message)
        {
        }
    }
}

