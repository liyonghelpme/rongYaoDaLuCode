﻿using System;
using UnityEngine;

public class FingerEventsSamplePart1 : SampleBase
{
    public float chargeDelay = 0.5f;
    public float chargeTime = 5f;
    public GameObject fingerDownObject;
    public GameObject fingerStationaryObject;
    public GameObject fingerUpObject;
    public float maxSationaryParticleEmissionCount = 50f;
    public float minSationaryParticleEmissionCount = 5f;
    private Material originalMaterial;
    public int requiredTapCount = 2;
    private int stationaryFingerIndex = -1;
    public Material stationaryMaterial;
    private ParticleEmitter stationaryParticleEmitter;

    private bool CheckSpawnParticles(Vector2 fingerPos, GameObject requiredObject)
    {
        GameObject obj2 = SampleBase.PickObject(fingerPos);
        if ((obj2 == null) || (obj2 != requiredObject))
        {
            return false;
        }
        this.SpawnParticles(obj2);
        return true;
    }

    private void FingerGestures_OnFingerDown(int fingerIndex, Vector2 fingerPos)
    {
        this.CheckSpawnParticles(fingerPos, this.fingerDownObject);
    }

    private void FingerGestures_OnFingerStationary(int fingerIndex, Vector2 fingerPos, float elapsedTime)
    {
        if ((elapsedTime >= this.chargeDelay) && (SampleBase.PickObject(fingerPos) == this.fingerStationaryObject))
        {
            float t = Mathf.Clamp01((elapsedTime - this.chargeDelay) / this.chargeTime);
            float num2 = Mathf.Lerp(this.minSationaryParticleEmissionCount, this.maxSationaryParticleEmissionCount, t);
            this.stationaryParticleEmitter.minEmission = num2;
            this.stationaryParticleEmitter.maxEmission = num2;
            this.stationaryParticleEmitter.emit = true;
            base.UI.StatusText = "Charge: " + ((100f * t)).ToString("N1") + "%";
        }
    }

    private void FingerGestures_OnFingerStationaryBegin(int fingerIndex, Vector2 fingerPos)
    {
        if (this.stationaryFingerIndex == -1)
        {
            GameObject obj2 = SampleBase.PickObject(fingerPos);
            if (obj2 == this.fingerStationaryObject)
            {
                base.UI.StatusText = "Begin stationary on finger " + fingerIndex;
                this.stationaryFingerIndex = fingerIndex;
                this.originalMaterial = obj2.renderer.sharedMaterial;
                obj2.renderer.sharedMaterial = this.stationaryMaterial;
            }
        }
    }

    private void FingerGestures_OnFingerStationaryEnd(int fingerIndex, Vector2 fingerPos, float elapsedTime)
    {
        if (fingerIndex == this.stationaryFingerIndex)
        {
            object[] objArray1 = new object[] { "Stationary ended on finger ", fingerIndex, " - ", elapsedTime.ToString("N1"), " seconds elapsed" };
            base.UI.StatusText = string.Concat(objArray1);
            this.StopStationaryParticleEmitter();
            this.fingerStationaryObject.renderer.sharedMaterial = this.originalMaterial;
            this.stationaryFingerIndex = -1;
        }
    }

    private void FingerGestures_OnFingerUp(int fingerIndex, Vector2 fingerPos, float timeHeldDown)
    {
        this.CheckSpawnParticles(fingerPos, this.fingerUpObject);
        FingerGestures.Finger finger = FingerGestures.GetFinger(fingerIndex);
        Debug.Log(string.Concat(new object[] { "Finger was lifted up at ", finger.Position, " and moved ", finger.DistanceFromStart.ToString("N0"), " pixels from its initial position at ", finger.StartPosition }));
    }

    protected override string GetHelpText()
    {
        return "This sample lets you visualize and understand the FingerDown, FingerStationary and FingerUp events.\r\n\r\nINSTRUCTIONS:\r\n- Press, hold and release the red and blue spheres\r\n- Press & hold the green sphere without moving for a few seconds";
    }

    private void OnDisable()
    {
        FingerGestures.OnFingerDown -= new FingerGestures.FingerDownEventHandler(this.FingerGestures_OnFingerDown);
        FingerGestures.OnFingerUp -= new FingerGestures.FingerUpEventHandler(this.FingerGestures_OnFingerUp);
        FingerGestures.OnFingerStationaryBegin -= new FingerGestures.FingerStationaryBeginEventHandler(this.FingerGestures_OnFingerStationaryBegin);
        FingerGestures.OnFingerStationary -= new FingerGestures.FingerStationaryEventHandler(this.FingerGestures_OnFingerStationary);
        FingerGestures.OnFingerStationaryEnd -= new FingerGestures.FingerStationaryEndEventHandler(this.FingerGestures_OnFingerStationaryEnd);
    }

    private void OnEnable()
    {
        Debug.Log("Registering finger gesture events from C# script");
        FingerGestures.OnFingerDown += new FingerGestures.FingerDownEventHandler(this.FingerGestures_OnFingerDown);
        FingerGestures.OnFingerUp += new FingerGestures.FingerUpEventHandler(this.FingerGestures_OnFingerUp);
        FingerGestures.OnFingerStationaryBegin += new FingerGestures.FingerStationaryBeginEventHandler(this.FingerGestures_OnFingerStationaryBegin);
        FingerGestures.OnFingerStationary += new FingerGestures.FingerStationaryEventHandler(this.FingerGestures_OnFingerStationary);
        FingerGestures.OnFingerStationaryEnd += new FingerGestures.FingerStationaryEndEventHandler(this.FingerGestures_OnFingerStationaryEnd);
    }

    private void SpawnParticles(GameObject obj)
    {
        ParticleEmitter componentInChildren = obj.GetComponentInChildren<ParticleEmitter>();
        if (componentInChildren != null)
        {
            componentInChildren.Emit();
        }
    }

    protected override void Start()
    {
        base.Start();
        if (this.fingerStationaryObject != null)
        {
            this.stationaryParticleEmitter = this.fingerStationaryObject.GetComponentInChildren<ParticleEmitter>();
        }
    }

    private void StopStationaryParticleEmitter()
    {
        this.stationaryParticleEmitter.emit = false;
        base.UI.StatusText = string.Empty;
    }
}

