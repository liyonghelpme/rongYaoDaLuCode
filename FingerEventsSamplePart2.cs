﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FingerEventsSamplePart2 : SampleBase
{
    public GameObject fingerDownMarkerPrefab;
    public GameObject fingerMoveBeginMarkerPrefab;
    public GameObject fingerMoveEndMarkerPrefab;
    public GameObject fingerUpMarkerPrefab;
    public LineRenderer lineRendererPrefab;
    private PathRenderer[] paths;

    private bool CheckSpawnParticles(Vector2 fingerPos, GameObject requiredObject)
    {
        GameObject obj2 = SampleBase.PickObject(fingerPos);
        if ((obj2 == null) || (obj2 != requiredObject))
        {
            return false;
        }
        this.SpawnParticles(obj2);
        return true;
    }

    private void FingerGestures_OnFingerDown(int fingerIndex, Vector2 fingerPos)
    {
        PathRenderer renderer = this.paths[fingerIndex];
        renderer.Reset();
        renderer.AddPoint(fingerPos, this.fingerDownMarkerPrefab);
    }

    private void FingerGestures_OnFingerMove(int fingerIndex, Vector2 fingerPos)
    {
        this.paths[fingerIndex].AddPoint(fingerPos);
    }

    private void FingerGestures_OnFingerMoveBegin(int fingerIndex, Vector2 fingerPos)
    {
        base.UI.StatusText = "Started moving finger " + fingerIndex;
        this.paths[fingerIndex].AddPoint(fingerPos, this.fingerMoveBeginMarkerPrefab);
    }

    private void FingerGestures_OnFingerMoveEnd(int fingerIndex, Vector2 fingerPos)
    {
        base.UI.StatusText = "Stopped moving finger " + fingerIndex;
        this.paths[fingerIndex].AddPoint(fingerPos, this.fingerMoveEndMarkerPrefab);
    }

    private void FingerGestures_OnFingerUp(int fingerIndex, Vector2 fingerPos, float timeHeldDown)
    {
        this.paths[fingerIndex].AddPoint(fingerPos, this.fingerUpMarkerPrefab);
        object[] objArray1 = new object[] { "Finger ", fingerIndex, " was held down for ", timeHeldDown.ToString("N2"), " seconds" };
        base.UI.StatusText = string.Concat(objArray1);
    }

    protected override string GetHelpText()
    {
        return "This sample lets you visualize the FingerDown, FingerMoveBegin, FingerMove, FingerMoveEnd and FingerUp events.\r\n\r\nINSTRUCTIONS:\r\nMove your finger accross the screen and observe what happens.\r\n\r\nLEGEND:\r\n- Red Circle = FingerDown position\r\n- Yellow Square = FingerMoveBegin position\r\n- Green Sphere = FingerMoveEnd position\r\n- Blue Circle = FingerUp position";
    }

    private void OnDisable()
    {
        FingerGestures.OnFingerDown -= new FingerGestures.FingerDownEventHandler(this.FingerGestures_OnFingerDown);
        FingerGestures.OnFingerUp -= new FingerGestures.FingerUpEventHandler(this.FingerGestures_OnFingerUp);
        FingerGestures.OnFingerMoveBegin -= new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveBegin);
        FingerGestures.OnFingerMove -= new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMove);
        FingerGestures.OnFingerMoveEnd -= new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveEnd);
    }

    private void OnEnable()
    {
        Debug.Log("Registering finger gesture events from C# script");
        FingerGestures.OnFingerDown += new FingerGestures.FingerDownEventHandler(this.FingerGestures_OnFingerDown);
        FingerGestures.OnFingerUp += new FingerGestures.FingerUpEventHandler(this.FingerGestures_OnFingerUp);
        FingerGestures.OnFingerMoveBegin += new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveBegin);
        FingerGestures.OnFingerMove += new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMove);
        FingerGestures.OnFingerMoveEnd += new FingerGestures.FingerMoveEventHandler(this.FingerGestures_OnFingerMoveEnd);
    }

    private void SpawnParticles(GameObject obj)
    {
        ParticleEmitter componentInChildren = obj.GetComponentInChildren<ParticleEmitter>();
        if (componentInChildren != null)
        {
            componentInChildren.Emit();
        }
    }

    protected override void Start()
    {
        base.Start();
        base.UI.StatusText = "Drag your fingers anywhere on the screen";
        this.paths = new PathRenderer[FingerGestures.Instance.MaxFingers];
        for (int i = 0; i < this.paths.Length; i++)
        {
            this.paths[i] = new PathRenderer(i, this.lineRendererPrefab);
        }
    }

    private class PathRenderer
    {
        private LineRenderer lineRenderer;
        private List<GameObject> markers = new List<GameObject>();
        private List<Vector3> points = new List<Vector3>();

        public PathRenderer(int index, LineRenderer lineRendererPrefab)
        {
            this.lineRenderer = UnityEngine.Object.Instantiate(lineRendererPrefab) as LineRenderer;
            this.lineRenderer.name = lineRendererPrefab.name + index;
            this.lineRenderer.enabled = true;
            this.UpdateLines();
        }

        private GameObject AddMarker(Vector2 pos, GameObject prefab)
        {
            GameObject item = UnityEngine.Object.Instantiate(prefab, (Vector3) pos, Quaternion.identity) as GameObject;
            object[] objArray1 = new object[] { prefab.name, "(", this.markers.Count, ")" };
            item.name = string.Concat(objArray1);
            this.markers.Add(item);
            return item;
        }

        public void AddPoint(Vector2 screenPos)
        {
            this.AddPoint(screenPos, null);
        }

        public void AddPoint(Vector2 screenPos, GameObject markerPrefab)
        {
            Vector3 worldPos = SampleBase.GetWorldPos(screenPos);
            if (markerPrefab != null)
            {
                this.AddMarker(worldPos, markerPrefab);
            }
            this.points.Add(worldPos);
            this.UpdateLines();
        }

        public void Reset()
        {
            this.points.Clear();
            this.UpdateLines();
            foreach (GameObject obj2 in this.markers)
            {
                UnityEngine.Object.Destroy(obj2);
            }
            this.markers.Clear();
        }

        private void UpdateLines()
        {
            this.lineRenderer.SetVertexCount(this.points.Count);
            for (int i = 0; i < this.points.Count; i++)
            {
                this.lineRenderer.SetPosition(i, this.points[i]);
            }
        }
    }
}

