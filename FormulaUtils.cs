﻿using System;

public class FormulaUtils
{
    private static int f(int lv)
    {
        int num = 1;
        int num2 = 4;
        return ((lv * num) + num2);
    }

    public static int stregthCost(int baseCost, int lv)
    {
        return (baseCost * f(lv));
    }

    public static int strength(int @base, int level, double ratio)
    {
        return (int) Math.Floor((double) (@base * Math.Pow(ratio, (double) level)));
    }

    public static int strength(int baseValue, int lv, int ratio)
    {
        float num = 0.01f;
        return (int) Math.Floor((double) (baseValue + (((f(lv) * ratio) * lv) * num)));
    }

    public static int xianshu(int baseCost, int level, double ratio)
    {
        return (int) Math.Floor((double) (baseCost * Math.Pow(ratio, (double) level)));
    }
}

