﻿using System;
using UnityEngine;

public class GestureStateTracker : MonoBehaviour
{
    public GestureRecognizer gesture;

    private void Awake()
    {
        if (this.gesture == null)
        {
            this.gesture = base.GetComponent<GestureRecognizer>();
        }
    }

    private void gesture_OnStateChanged(GestureRecognizer source)
    {
        Debug.Log(string.Concat(new object[] { "Gesture ", source, " changed from ", source.PreviousState, " to ", source.State }));
    }

    private void OnDisable()
    {
        if (this.gesture != null)
        {
            this.gesture.OnStateChanged -= new FGComponent.EventDelegate<GestureRecognizer>(this.gesture_OnStateChanged);
        }
    }

    private void OnEnable()
    {
        if (this.gesture != null)
        {
            this.gesture.OnStateChanged += new FGComponent.EventDelegate<GestureRecognizer>(this.gesture_OnStateChanged);
        }
    }
}

