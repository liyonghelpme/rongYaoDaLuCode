﻿using com.liyong;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GridManager
{
    [CompilerGenerated]
    private static Func<int, BoxObject, <>__AnonType26<int, BoxObject>> <>f__am$cache3;
    [CompilerGenerated]
    private static Func<<>__AnonType26<int, BoxObject>, BoxObject> <>f__am$cache4;
    private const int COFF = 0x2710;
    public float CutX = 150f;
    public float CutY = 6f;
    private Dictionary<int, List<BoxObject>> gridToBox = new Dictionary<int, List<BoxObject>>();
    private const int MIN_OFF = 0x4e20;

    public GridManager(float cx, float cy)
    {
        this.CutX = cx;
        this.CutY = cy;
    }

    public void AddBox(BoxObject box)
    {
        float x = box.X - (box.Width / 2f);
        float num2 = box.X + (box.Width / 2f);
        float y = box.Y;
        float num4 = box.Y + box.Height;
        Util.Int2 num5 = this.PosToGridId(new Vector3(x, y));
        Util.Int2 num6 = this.PosToGridId(new Vector3(num2, num4));
        for (int i = num5.x; i <= num6.x; i++)
        {
            for (int j = num5.y; j <= num6.y; j++)
            {
                int item = this.GridId(i, j);
                box.gridIndex.Add(item);
                if (this.gridToBox.ContainsKey(item))
                {
                    this.gridToBox[item].Add(box);
                }
                else
                {
                    this.gridToBox[item] = new List<BoxObject>();
                    this.gridToBox[item].Add(box);
                }
            }
        }
    }

    public bool CheckCollision(int gid)
    {
        return this.gridToBox.ContainsKey(gid);
    }

    private Util.Int2 DivGridId(int gid)
    {
        return new Util.Int2 { x = gid / 0x2710, y = gid % 0x2710 };
    }

    public IEnumerable<BoxObject> GetNeibors(BoxObject box)
    {
        <GetNeibors>c__AnonStorey122 storey = new <GetNeibors>c__AnonStorey122 {
            box = box,
            <>f__this = this
        };
        if (<>f__am$cache3 == null)
        {
            <>f__am$cache3 = (gid, b) => new <>__AnonType26<int, BoxObject>(gid, b);
        }
        if (<>f__am$cache4 == null)
        {
            <>f__am$cache4 = <>__TranspIdent41 => <>__TranspIdent41.b;
        }
        return storey.box.gridIndex.SelectMany<int, BoxObject, <>__AnonType26<int, BoxObject>>(new Func<int, IEnumerable<BoxObject>>(storey.<>m__1E8), <>f__am$cache3).Where<<>__AnonType26<int, BoxObject>>(new Func<<>__AnonType26<int, BoxObject>, bool>(storey.<>m__1EA)).Select<<>__AnonType26<int, BoxObject>, BoxObject>(<>f__am$cache4).ToList<BoxObject>();
    }

    public int[] GetNeiborsCrowdSituation(Util.Int2 gid)
    {
        int num = gid.x - 1;
        int num2 = gid.x + 1;
        int num3 = gid.y - 1;
        int num4 = gid.y + 1;
        int[] numArray = new int[9];
        int index = 0;
        for (int i = num; i <= num2; i++)
        {
            for (int j = num3; j <= num4; j++)
            {
                int key = this.GridId(i, j);
                if (this.gridToBox.ContainsKey(key))
                {
                    numArray[index] = this.gridToBox[key].Count;
                }
                else
                {
                    numArray[index] = 0;
                }
                index++;
            }
        }
        return numArray;
    }

    private int GridId(int x, int y)
    {
        return ((x * 0x2710) + y);
    }

    public Vector2 GridIdToPos(int gid)
    {
        Util.Int2 num = this.DivGridId(gid);
        float x = (num.x * this.CutX) - 20000f;
        return new Vector2(x, (num.y * this.CutY) - 20000f);
    }

    public Util.Int2 PosToGridId(Vector3 pos)
    {
        int num = Mathf.RoundToInt((pos.x + 20000f) / this.CutX);
        int num2 = Mathf.RoundToInt((pos.y + 20000f) / this.CutY);
        return new Util.Int2 { x = num, y = num2 };
    }

    public void RemoveBox(BoxObject box)
    {
        foreach (int num in box.gridIndex)
        {
            this.gridToBox[num].Remove(box);
            if (this.gridToBox[num].Count == 0)
            {
                this.gridToBox.Remove(num);
            }
        }
        box.gridIndex.Clear();
    }

    public void UpdateBox(BoxObject box)
    {
        this.RemoveBox(box);
        this.AddBox(box);
    }

    [CompilerGenerated]
    private sealed class <GetNeibors>c__AnonStorey122
    {
        internal GridManager <>f__this;
        internal BoxObject box;

        internal IEnumerable<BoxObject> <>m__1E8(int gid)
        {
            return this.<>f__this.gridToBox[gid];
        }

        internal bool <>m__1EA(<>__AnonType26<int, BoxObject> <>__TranspIdent41)
        {
            return (<>__TranspIdent41.b != this.box);
        }
    }
}

