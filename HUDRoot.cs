﻿using System;
using UnityEngine;

[AddComponentMenu("NGUI/Examples/HUD Root")]
public class HUDRoot : MonoBehaviour
{
    public static GameObject go;
    public static HUDRoot instane;
    public GameObject prefab;

    private void Awake()
    {
        instane = this;
        go = base.gameObject;
    }
}

