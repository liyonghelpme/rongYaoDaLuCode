﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, AddComponentMenu("NGUI/Examples/HUD Text")]
public class HUDTextII : MonoBehaviour
{
    public AnimationCurve alphaCurve;
    public bool applyGradient;
    public UIFont bitmapFont;
    private int counter;
    public UILabel.Effect effect;
    public Color effectColor = Color.black;
    [HideInInspector, SerializeField]
    private UIFont font;
    public int fontSize = 0x10;
    public FontStyle fontStyle;
    public Color gradienBottom = new Color(0.7f, 0.7f, 0.7f);
    public Color gradientTop = Color.white;
    private int index;
    private List<Entry> mList;
    private List<Entry> mUnused;
    private bool mUseDynamicFont;
    public AnimationCurve offsetCurve;
    public static GameObject Parent;
    public AnimationCurve scaleCurve;
    public Font trueTypeFont;

    public HUDTextII()
    {
        Keyframe[] keys = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(3f, 40f) };
        this.offsetCurve = new AnimationCurve(keys);
        Keyframe[] keyframeArray2 = new Keyframe[] { new Keyframe(1f, 1f), new Keyframe(3f, 0f) };
        this.alphaCurve = new AnimationCurve(keyframeArray2);
        Keyframe[] keyframeArray3 = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(0.25f, 1f) };
        this.scaleCurve = new AnimationCurve(keyframeArray3);
        this.mList = new List<Entry>();
        this.mUnused = new List<Entry>();
    }

    public void Add(object obj, Color c, float stayDuration)
    {
        if (base.enabled)
        {
            float realtimeSinceStartup = Time.realtimeSinceStartup;
            bool flag = false;
            float num2 = 0f;
            if (obj is float)
            {
                flag = true;
                num2 = (float) obj;
            }
            else if (obj is int)
            {
                flag = true;
                num2 = (int) obj;
            }
            if (flag)
            {
                if (num2 == 0f)
                {
                    return;
                }
                int count = this.mList.Count;
                while (count > 0)
                {
                    Entry entry = this.mList[--count];
                    if (((entry.time + 1f) >= realtimeSinceStartup) && (entry.val != 0f))
                    {
                        if ((entry.val < 0f) && (num2 < 0f))
                        {
                            entry.val += num2;
                            entry.label.text = Mathf.RoundToInt(entry.val).ToString();
                            return;
                        }
                        if ((entry.val > 0f) && (num2 > 0f))
                        {
                            entry.val += num2;
                            entry.label.text = "+" + Mathf.RoundToInt(entry.val);
                            return;
                        }
                    }
                }
            }
            Entry entry2 = this.Create();
            entry2.stay = stayDuration;
            entry2.label.color = c;
            entry2.val = num2;
            Vector3 position = base.gameObject.transform.position;
            position.y += this.index * 0.08f;
            position.x += this.index * 0.03f;
            this.index++;
            if (this.index == 3)
            {
                this.index = -2;
            }
            entry2.StartPoint = position;
            if (flag)
            {
                entry2.label.text = (num2 >= 0f) ? ("+" + Mathf.RoundToInt(entry2.val)) : Mathf.RoundToInt(entry2.val).ToString();
            }
            else
            {
                entry2.label.text = obj.ToString();
            }
            this.mList.Sort(new Comparison<Entry>(HUDTextII.Comparison));
        }
    }

    private static int Comparison(Entry a, Entry b)
    {
        if (a.movementStart < b.movementStart)
        {
            return -1;
        }
        if (a.movementStart > b.movementStart)
        {
            return 1;
        }
        return 0;
    }

    private Entry Create()
    {
        if (this.mUnused.Count > 0)
        {
            Entry entry = this.mUnused[this.mUnused.Count - 1];
            this.mUnused.RemoveAt(this.mUnused.Count - 1);
            entry.time = Time.realtimeSinceStartup;
            entry.label.depth = NGUITools.CalculateNextDepth(base.gameObject) + 10;
            entry.label.transform.position = new Vector3(10000f, 0f, 0f);
            NGUITools.SetActive(entry.label.gameObject, true);
            entry.offset = 0f;
            this.mList.Add(entry);
            return entry;
        }
        Entry item = new Entry {
            time = Time.realtimeSinceStartup,
            label = NGUITools.AddWidget<UILabel>(Parent)
        };
        item.label.name = this.counter.ToString();
        item.label.ambigiousFont = this.ambigiousFont;
        item.label.fontSize = this.fontSize;
        item.label.fontStyle = this.fontStyle;
        item.label.applyGradient = this.applyGradient;
        item.label.gradientTop = this.gradientTop;
        item.label.gradientBottom = this.gradienBottom;
        item.label.effectStyle = this.effect;
        item.label.effectColor = this.effectColor;
        item.label.overflowMethod = UILabel.Overflow.ResizeFreely;
        item.label.spacingX = -6;
        item.label.transform.position = new Vector3(10000f, 0f, 0f);
        item.label.cachedTransform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        this.mList.Add(item);
        this.counter++;
        return item;
    }

    private void Delete(Entry ent)
    {
        this.mList.Remove(ent);
        this.mUnused.Add(ent);
        NGUITools.SetActive(ent.label.gameObject, false);
    }

    private void OnDisable()
    {
        int count = this.mList.Count;
        while (count > 0)
        {
            Entry entry = this.mList[--count];
            if (entry.label != null)
            {
                entry.label.enabled = false;
            }
            else
            {
                this.mList.RemoveAt(count);
            }
        }
    }

    private void OnEnable()
    {
        if (this.font != null)
        {
            if (this.font.isDynamic)
            {
                this.trueTypeFont = this.font.dynamicFont;
                this.fontStyle = this.font.dynamicFontStyle;
                this.mUseDynamicFont = true;
            }
            else if (this.bitmapFont == null)
            {
                this.bitmapFont = this.font;
                this.mUseDynamicFont = false;
            }
            this.font = null;
        }
    }

    private void OnValidate()
    {
        Font trueTypeFont = this.trueTypeFont;
        UIFont bitmapFont = this.bitmapFont;
        this.bitmapFont = null;
        this.trueTypeFont = null;
        if ((trueTypeFont != null) && ((bitmapFont == null) || !this.mUseDynamicFont))
        {
            this.bitmapFont = null;
            this.trueTypeFont = trueTypeFont;
            this.mUseDynamicFont = true;
        }
        else if (bitmapFont != null)
        {
            if (bitmapFont.isDynamic)
            {
                this.trueTypeFont = bitmapFont.dynamicFont;
                this.fontStyle = bitmapFont.dynamicFontStyle;
                this.fontSize = bitmapFont.defaultSize;
                this.mUseDynamicFont = true;
            }
            else
            {
                this.bitmapFont = bitmapFont;
                this.mUseDynamicFont = false;
            }
        }
        else
        {
            this.trueTypeFont = trueTypeFont;
            this.mUseDynamicFont = true;
        }
    }

    private void Start()
    {
        this.index = UnityEngine.Random.Range(-2, 2);
    }

    private void Update()
    {
        float time = RealTime.time;
        Keyframe[] keys = this.offsetCurve.keys;
        Keyframe[] keyframeArray2 = this.alphaCurve.keys;
        Keyframe[] keyframeArray3 = this.scaleCurve.keys;
        float a = keys[keys.Length - 1].time;
        float b = keyframeArray2[keyframeArray2.Length - 1].time;
        float num5 = Mathf.Max(keyframeArray3[keyframeArray3.Length - 1].time, Mathf.Max(a, b));
        int count = this.mList.Count;
        while (count > 0)
        {
            Entry ent = this.mList[--count];
            float num7 = time - ent.movementStart;
            ent.offset = this.offsetCurve.Evaluate(num7);
            ent.label.transform.position = new Vector3(ent.StartPoint.x, ent.StartPoint.y + (ent.offset * 0.01f), ent.StartPoint.z);
            float num8 = this.alphaCurve.Evaluate(num7);
            ent.label.alpha = num8;
            Color effectColor = ent.label.effectColor;
            effectColor.a = num8 * 0.5f;
            ent.label.effectColor = effectColor;
            float x = this.scaleCurve.Evaluate(time - ent.time);
            if (x < 0.001f)
            {
                x = 0.001f;
            }
            ent.label.cachedTransform.localScale = new Vector3(x, x, x);
            if (num7 > num5)
            {
                this.Delete(ent);
            }
            else
            {
                ent.label.enabled = true;
            }
        }
    }

    public UnityEngine.Object ambigiousFont
    {
        get
        {
            if (this.trueTypeFont != null)
            {
                return this.trueTypeFont;
            }
            if (this.bitmapFont != null)
            {
                return this.bitmapFont;
            }
            return this.font;
        }
        set
        {
            if (value is Font)
            {
                this.trueTypeFont = value as Font;
                this.bitmapFont = null;
                this.font = null;
            }
            else if (value is UIFont)
            {
                this.bitmapFont = value as UIFont;
                this.trueTypeFont = null;
                this.font = null;
            }
        }
    }

    public bool isVisible
    {
        get
        {
            return (this.mList.Count != 0);
        }
    }

    protected class Entry
    {
        public UILabel label;
        public float offset;
        public Vector3 StartPoint;
        public float stay;
        public float time;
        public float val;

        public float movementStart
        {
            get
            {
                return (this.time + this.stay);
            }
        }
    }
}

