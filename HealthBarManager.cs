﻿using com.liyong;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class HealthBarManager
{
    private float CutX = 100f;
    private float CutY = 8f;
    private GridManager gridManager;
    private Dictionary<int, List<MyHealthBar>> gridToHealthBar = new Dictionary<int, List<MyHealthBar>>();
    public static HealthBarManager Instance = new HealthBarManager();
    private int lastFreeGid = 0x3e8;
    private int totalCount;
    private int UpdateNum;

    public HealthBarManager()
    {
        this.gridManager = new GridManager(this.CutX, this.CutY);
    }

    public Vector2 AddHealthBar(MyHealthBar healthBar)
    {
        Vector3 idealPos = healthBar.idealPos;
        Util.Int2 num = this.posToGridId(idealPos);
        int freeGrid = this.GetFreeGrid(num.x, num.y);
        this.lastFreeGid = num.y;
        Util.Int2 num3 = this.DivGridId(freeGrid);
        healthBar.gridX = num3.x;
        healthBar.gridY = num3.y;
        this.AddHealthBar(freeGrid, healthBar);
        Vector2 vector2 = this.gridIdToPos(freeGrid);
        vector2.x = idealPos.x;
        healthBar.addYet = true;
        return vector2;
    }

    private void AddHealthBar(int gid, MyHealthBar bar)
    {
        if (!this.gridToHealthBar.ContainsKey(gid))
        {
            this.gridToHealthBar[gid] = new List<MyHealthBar>();
        }
        this.gridToHealthBar[gid].Add(bar);
        this.totalCount++;
    }

    public bool CanUpdate()
    {
        return (this.UpdateNum <= 11);
    }

    public bool CheckActiveBloodNum()
    {
        return (this.totalCount <= 5);
    }

    public void ClearUpdateNum()
    {
        this.UpdateNum = 0;
    }

    private Util.Int2 DivGridId(int gid)
    {
        return new Util.Int2 { x = gid / 0x3e8, y = gid % 0x3e8 };
    }

    private int GetFreeGrid(int x, int y)
    {
        for (int i = 0; this.isCollision(x, y) && (i <= 2); i++)
        {
            y++;
        }
        return this.GridId(x, y);
    }

    public GridManager GetGridManager()
    {
        return this.gridManager;
    }

    private int GridId(int x, int y)
    {
        return ((x * 0x3e8) + y);
    }

    private Vector2 gridIdToPos(int gid)
    {
        Util.Int2 num = this.DivGridId(gid);
        float x = (num.x * this.CutX) - 2000f;
        return new Vector2(x, (num.y * this.CutY) - 2000f);
    }

    private bool isCollision(int x, int y)
    {
        return false;
    }

    private Util.Int2 posToGridId(Vector3 pos)
    {
        int num = Mathf.RoundToInt((pos.x + 2000f) / this.CutX);
        int num2 = Mathf.RoundToInt((pos.y + 2000f) / this.CutY);
        return new Util.Int2 { x = num, y = num2 };
    }

    private void RemoveBar(MyHealthBar healthBar)
    {
        int key = this.GridId(healthBar.gridX, healthBar.gridY);
        if (this.gridToHealthBar.ContainsKey(key))
        {
            this.gridToHealthBar[key].Remove(healthBar);
            this.totalCount--;
            if (this.gridToHealthBar.Count == 0)
            {
                this.gridToHealthBar.Remove(key);
            }
        }
    }

    public void RemoveHealthBar(MyHealthBar healthBar)
    {
        if (healthBar.addYet)
        {
            healthBar.addYet = false;
            this.RemoveBar(healthBar);
        }
    }

    public UpdateResult UpdateHealthBar(MyHealthBar healthBar, int idleFrame = 0)
    {
        int y = 0x3e9;
        if (healthBar.addYet)
        {
            Util.Int2 num2 = this.posToGridId(healthBar.idealPos);
            y = num2.y;
            int gridX = healthBar.gridX;
            if (((healthBar.gridY == num2.y) && (gridX == num2.x)) && (idleFrame <= 20))
            {
                return new UpdateResult { isCache = true };
            }
            this.RemoveHealthBar(healthBar);
        }
        this.UpdateNum++;
        Vector2 vector = this.AddHealthBar(healthBar);
        if (y == this.lastFreeGid)
        {
            vector.y = healthBar.idealPos.y;
        }
        return new UpdateResult { pos = vector };
    }

    public class UpdateResult
    {
        public bool isCache;
        public Vector2 pos;
    }
}

