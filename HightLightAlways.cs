﻿using System;
using UnityEngine;

public class HightLightAlways : MonoBehaviour
{
    private GameObject hightLight;
    private string spriteName;

    private void Awake()
    {
        Transform transform = base.transform.FindChild("hightlight");
        if (transform == null)
        {
            transform = base.transform.FindChild("light");
        }
        this.hightLight = transform.gameObject;
        if (this.hightLight != null)
        {
            this.hightLight.SetActive(false);
        }
    }

    public void disableHightLight()
    {
        if (this.hightLight.activeInHierarchy)
        {
            this.hightLight.SetActive(false);
        }
    }

    public void enableHightLight()
    {
        this.hightLight.SetActive(true);
    }

    private void OnPress(bool isPress)
    {
        if ((this.hightLight != null) && isPress)
        {
            this.hightLight.SetActive(true);
        }
    }
}

