﻿using System;
using UnityEngine;

public class HightLightControl : MonoBehaviour
{
    private GameObject hightLight;
    private string spriteName;

    private void Awake()
    {
        Transform transform = base.transform.FindChild("hightlight");
        if (transform == null)
        {
            transform = base.transform.FindChild("light");
        }
        this.hightLight = transform.gameObject;
        if (this.hightLight != null)
        {
            this.hightLight.SetActive(false);
        }
    }

    private void OnPress(bool isPress)
    {
        if (this.hightLight != null)
        {
            if (isPress)
            {
                this.hightLight.SetActive(true);
            }
            else
            {
                this.hightLight.SetActive(false);
            }
        }
    }
}

