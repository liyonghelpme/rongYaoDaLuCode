﻿namespace Holoville.HOTween
{
    using System;

    [Flags]
    public enum Axis
    {
        None = 0,
        W = 0x10,
        X = 2,
        Y = 4,
        Z = 8
    }
}

