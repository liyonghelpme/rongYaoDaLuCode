﻿namespace Holoville.HOTween
{
    using System;

    public enum CallbackType
    {
        OnStart,
        OnUpdate,
        OnStepComplete,
        OnComplete,
        OnPause,
        OnPlay,
        OnRewinded,
        OnPluginOverwritten
    }
}

