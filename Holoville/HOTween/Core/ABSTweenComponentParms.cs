﻿namespace Holoville.HOTween.Core
{
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    public abstract class ABSTweenComponentParms
    {
        protected bool autoKillOnComplete = true;
        protected string id = string.Empty;
        protected int intId = -1;
        protected bool isPaused;
        protected int loops = 1;
        protected LoopType loopType = Holoville.HOTween.HOTween.defLoopType;
        protected bool manageBehaviours;
        protected Behaviour[] managedBehavioursOff;
        protected Behaviour[] managedBehavioursOn;
        protected GameObject[] managedGameObjectsOff;
        protected GameObject[] managedGameObjectsOn;
        protected bool manageGameObjects;
        protected TweenDelegate.TweenCallback onComplete;
        protected object[] onCompleteParms;
        protected TweenDelegate.TweenCallbackWParms onCompleteWParms;
        protected TweenDelegate.TweenCallback onPause;
        protected object[] onPauseParms;
        protected TweenDelegate.TweenCallbackWParms onPauseWParms;
        protected TweenDelegate.TweenCallback onPlay;
        protected object[] onPlayParms;
        protected TweenDelegate.TweenCallbackWParms onPlayWParms;
        protected TweenDelegate.TweenCallback onPluginUpdated;
        protected object[] onPluginUpdatedParms;
        protected TweenDelegate.TweenCallbackWParms onPluginUpdatedWParms;
        protected TweenDelegate.TweenCallback onRewinded;
        protected object[] onRewindedParms;
        protected TweenDelegate.TweenCallbackWParms onRewindedWParms;
        protected TweenDelegate.TweenCallback onStart;
        protected object[] onStartParms;
        protected TweenDelegate.TweenCallbackWParms onStartWParms;
        protected TweenDelegate.TweenCallback onStepComplete;
        protected object[] onStepCompleteParms;
        protected TweenDelegate.TweenCallbackWParms onStepCompleteWParms;
        protected TweenDelegate.TweenCallback onUpdate;
        protected object[] onUpdateParms;
        protected TweenDelegate.TweenCallbackWParms onUpdateWParms;
        protected float timeScale = Holoville.HOTween.HOTween.defTimeScale;
        protected UpdateType updateType = Holoville.HOTween.HOTween.defUpdateType;

        protected ABSTweenComponentParms()
        {
        }

        protected void InitializeOwner(ABSTweenComponent p_owner)
        {
            p_owner._id = this.id;
            p_owner._intId = this.intId;
            p_owner._autoKillOnComplete = this.autoKillOnComplete;
            p_owner._updateType = this.updateType;
            p_owner._timeScale = this.timeScale;
            p_owner._loops = this.loops;
            p_owner._loopType = this.loopType;
            p_owner._isPaused = this.isPaused;
            p_owner.onStart = this.onStart;
            p_owner.onStartWParms = this.onStartWParms;
            p_owner.onStartParms = this.onStartParms;
            p_owner.onUpdate = this.onUpdate;
            p_owner.onUpdateWParms = this.onUpdateWParms;
            p_owner.onUpdateParms = this.onUpdateParms;
            p_owner.onPluginUpdated = this.onPluginUpdated;
            p_owner.onPluginUpdatedWParms = this.onPluginUpdatedWParms;
            p_owner.onPluginUpdatedParms = this.onPluginUpdatedParms;
            p_owner.onPause = this.onPause;
            p_owner.onPauseWParms = this.onPauseWParms;
            p_owner.onPauseParms = this.onPauseParms;
            p_owner.onPlay = this.onPlay;
            p_owner.onPlayWParms = this.onPlayWParms;
            p_owner.onPlayParms = this.onPlayParms;
            p_owner.onRewinded = this.onRewinded;
            p_owner.onRewindedWParms = this.onRewindedWParms;
            p_owner.onRewindedParms = this.onRewindedParms;
            p_owner.onStepComplete = this.onStepComplete;
            p_owner.onStepCompleteWParms = this.onStepCompleteWParms;
            p_owner.onStepCompleteParms = this.onStepCompleteParms;
            p_owner.onComplete = this.onComplete;
            p_owner.onCompleteWParms = this.onCompleteWParms;
            p_owner.onCompleteParms = this.onCompleteParms;
            p_owner.manageBehaviours = this.manageBehaviours;
            p_owner.manageGameObjects = this.manageGameObjects;
            p_owner.managedBehavioursOn = this.managedBehavioursOn;
            p_owner.managedBehavioursOff = this.managedBehavioursOff;
            p_owner.managedGameObjectsOn = this.managedGameObjectsOn;
            p_owner.managedGameObjectsOff = this.managedGameObjectsOff;
            if (this.manageBehaviours)
            {
                int num = ((this.managedBehavioursOn == null) ? 0 : this.managedBehavioursOn.Length) + ((this.managedBehavioursOff == null) ? 0 : this.managedBehavioursOff.Length);
                p_owner.managedBehavioursOriginalState = new bool[num];
            }
            if (this.manageGameObjects)
            {
                int num2 = ((this.managedGameObjectsOn == null) ? 0 : this.managedGameObjectsOn.Length) + ((this.managedGameObjectsOff == null) ? 0 : this.managedGameObjectsOff.Length);
                p_owner.managedGameObjectsOriginalState = new bool[num2];
            }
        }
    }
}

