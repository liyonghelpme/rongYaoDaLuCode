﻿namespace Holoville.HOTween.Core
{
    using Holoville.HOTween;
    using System;

    public class EaseInfo
    {
        private static EaseInfo defaultEaseInfo = new EaseInfo(new TweenDelegate.EaseFunc(Linear.EaseNone), null);
        public TweenDelegate.EaseFunc ease;
        private static EaseInfo easeInBackInfo = new EaseInfo(new TweenDelegate.EaseFunc(Back.EaseIn), new TweenDelegate.EaseFunc(Back.EaseOut));
        private static EaseInfo easeInBounceInfo = new EaseInfo(new TweenDelegate.EaseFunc(Bounce.EaseIn), new TweenDelegate.EaseFunc(Bounce.EaseOut));
        private static EaseInfo easeInCircInfo = new EaseInfo(new TweenDelegate.EaseFunc(Circ.EaseIn), new TweenDelegate.EaseFunc(Circ.EaseOut));
        private static EaseInfo easeInCubicInfo = new EaseInfo(new TweenDelegate.EaseFunc(Cubic.EaseIn), new TweenDelegate.EaseFunc(Cubic.EaseOut));
        private static EaseInfo easeInElasticInfo = new EaseInfo(new TweenDelegate.EaseFunc(Elastic.EaseIn), new TweenDelegate.EaseFunc(Elastic.EaseOut));
        private static EaseInfo easeInExpoInfo = new EaseInfo(new TweenDelegate.EaseFunc(Expo.EaseIn), new TweenDelegate.EaseFunc(Expo.EaseOut));
        private static EaseInfo easeInOutBackInfo = new EaseInfo(new TweenDelegate.EaseFunc(Back.EaseInOut), null);
        private static EaseInfo easeInOutBounceInfo = new EaseInfo(new TweenDelegate.EaseFunc(Bounce.EaseInOut), null);
        private static EaseInfo easeInOutCircInfo = new EaseInfo(new TweenDelegate.EaseFunc(Circ.EaseInOut), null);
        private static EaseInfo easeInOutCubicInfo = new EaseInfo(new TweenDelegate.EaseFunc(Cubic.EaseInOut), null);
        private static EaseInfo easeInOutElasticInfo = new EaseInfo(new TweenDelegate.EaseFunc(Elastic.EaseInOut), null);
        private static EaseInfo easeInOutExpoInfo = new EaseInfo(new TweenDelegate.EaseFunc(Expo.EaseInOut), null);
        private static EaseInfo easeInOutQuadInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quad.EaseInOut), null);
        private static EaseInfo easeInOutQuartInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quart.EaseInOut), null);
        private static EaseInfo easeInOutQuintInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quint.EaseInOut), null);
        private static EaseInfo easeInOutSineInfo = new EaseInfo(new TweenDelegate.EaseFunc(Sine.EaseInOut), null);
        private static EaseInfo easeInOutStrongInfo = new EaseInfo(new TweenDelegate.EaseFunc(Strong.EaseInOut), null);
        private static EaseInfo easeInQuadInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quad.EaseIn), new TweenDelegate.EaseFunc(Quad.EaseOut));
        private static EaseInfo easeInQuartInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quart.EaseIn), new TweenDelegate.EaseFunc(Quart.EaseOut));
        private static EaseInfo easeInQuintInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quint.EaseIn), new TweenDelegate.EaseFunc(Quint.EaseOut));
        private static EaseInfo easeInSineInfo = new EaseInfo(new TweenDelegate.EaseFunc(Sine.EaseIn), new TweenDelegate.EaseFunc(Sine.EaseOut));
        private static EaseInfo easeInStrongInfo = new EaseInfo(new TweenDelegate.EaseFunc(Strong.EaseIn), new TweenDelegate.EaseFunc(Strong.EaseOut));
        private static EaseInfo easeOutBackInfo = new EaseInfo(new TweenDelegate.EaseFunc(Back.EaseOut), new TweenDelegate.EaseFunc(Back.EaseIn));
        private static EaseInfo easeOutBounceInfo = new EaseInfo(new TweenDelegate.EaseFunc(Bounce.EaseOut), new TweenDelegate.EaseFunc(Bounce.EaseIn));
        private static EaseInfo easeOutCircInfo = new EaseInfo(new TweenDelegate.EaseFunc(Circ.EaseOut), new TweenDelegate.EaseFunc(Circ.EaseIn));
        private static EaseInfo easeOutCubicInfo = new EaseInfo(new TweenDelegate.EaseFunc(Cubic.EaseOut), new TweenDelegate.EaseFunc(Cubic.EaseIn));
        private static EaseInfo easeOutElasticInfo = new EaseInfo(new TweenDelegate.EaseFunc(Elastic.EaseOut), new TweenDelegate.EaseFunc(Elastic.EaseIn));
        private static EaseInfo easeOutExpoInfo = new EaseInfo(new TweenDelegate.EaseFunc(Expo.EaseOut), new TweenDelegate.EaseFunc(Expo.EaseIn));
        private static EaseInfo easeOutQuadInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quad.EaseOut), new TweenDelegate.EaseFunc(Quad.EaseIn));
        private static EaseInfo easeOutQuartInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quart.EaseOut), new TweenDelegate.EaseFunc(Quart.EaseIn));
        private static EaseInfo easeOutQuintInfo = new EaseInfo(new TweenDelegate.EaseFunc(Quint.EaseOut), new TweenDelegate.EaseFunc(Quint.EaseIn));
        private static EaseInfo easeOutSineInfo = new EaseInfo(new TweenDelegate.EaseFunc(Sine.EaseOut), new TweenDelegate.EaseFunc(Sine.EaseIn));
        private static EaseInfo easeOutStrongInfo = new EaseInfo(new TweenDelegate.EaseFunc(Strong.EaseOut), new TweenDelegate.EaseFunc(Strong.EaseIn));
        public TweenDelegate.EaseFunc inverseEase;

        private EaseInfo(TweenDelegate.EaseFunc p_ease, TweenDelegate.EaseFunc p_inverseEase)
        {
            this.ease = p_ease;
            this.inverseEase = p_inverseEase;
        }

        internal static EaseInfo GetEaseInfo(EaseType p_easeType)
        {
            switch (p_easeType)
            {
                case EaseType.EaseInSine:
                    return easeInSineInfo;

                case EaseType.EaseOutSine:
                    return easeOutSineInfo;

                case EaseType.EaseInOutSine:
                    return easeInOutSineInfo;

                case EaseType.EaseInQuad:
                    return easeInQuadInfo;

                case EaseType.EaseOutQuad:
                    return easeOutQuadInfo;

                case EaseType.EaseInOutQuad:
                    return easeInOutQuadInfo;

                case EaseType.EaseInCubic:
                    return easeInCubicInfo;

                case EaseType.EaseOutCubic:
                    return easeOutCubicInfo;

                case EaseType.EaseInOutCubic:
                    return easeInOutCubicInfo;

                case EaseType.EaseInQuart:
                    return easeInQuartInfo;

                case EaseType.EaseOutQuart:
                    return easeOutQuartInfo;

                case EaseType.EaseInOutQuart:
                    return easeInOutQuartInfo;

                case EaseType.EaseInQuint:
                    return easeInQuintInfo;

                case EaseType.EaseOutQuint:
                    return easeOutQuintInfo;

                case EaseType.EaseInOutQuint:
                    return easeInOutQuintInfo;

                case EaseType.EaseInExpo:
                    return easeInExpoInfo;

                case EaseType.EaseOutExpo:
                    return easeOutExpoInfo;

                case EaseType.EaseInOutExpo:
                    return easeInOutExpoInfo;

                case EaseType.EaseInCirc:
                    return easeInCircInfo;

                case EaseType.EaseOutCirc:
                    return easeOutCircInfo;

                case EaseType.EaseInOutCirc:
                    return easeInOutCircInfo;

                case EaseType.EaseInElastic:
                    return easeInElasticInfo;

                case EaseType.EaseOutElastic:
                    return easeOutElasticInfo;

                case EaseType.EaseInOutElastic:
                    return easeInOutElasticInfo;

                case EaseType.EaseInBack:
                    return easeInBackInfo;

                case EaseType.EaseOutBack:
                    return easeOutBackInfo;

                case EaseType.EaseInOutBack:
                    return easeInOutBackInfo;

                case EaseType.EaseInBounce:
                    return easeInBounceInfo;

                case EaseType.EaseOutBounce:
                    return easeOutBounceInfo;

                case EaseType.EaseInOutBounce:
                    return easeInOutBounceInfo;

                case EaseType.EaseInStrong:
                    return easeInStrongInfo;

                case EaseType.EaseOutStrong:
                    return easeOutStrongInfo;

                case EaseType.EaseInOutStrong:
                    return easeInOutStrongInfo;
            }
            return defaultEaseInfo;
        }
    }
}

