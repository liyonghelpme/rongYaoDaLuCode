﻿namespace Holoville.HOTween.Core.Easing
{
    using System;
    using UnityEngine;

    internal class EaseCurve
    {
        private AnimationCurve animCurve;

        public EaseCurve(AnimationCurve p_animCurve)
        {
            this.animCurve = p_animCurve;
        }

        public float Evaluate(float time, float startValue, float changeValue, float duration, float unusedOvershoot, float unusedPeriod)
        {
            Keyframe keyframe = this.animCurve[this.animCurve.length - 1];
            float num = keyframe.time;
            float num2 = time / duration;
            float num3 = this.animCurve.Evaluate(num2 * num);
            return ((changeValue * num3) + startValue);
        }
    }
}

