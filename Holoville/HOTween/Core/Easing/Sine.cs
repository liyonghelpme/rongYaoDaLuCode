﻿namespace Holoville.HOTween.Core.Easing
{
    using System;

    public static class Sine
    {
        private const float PiOver2 = 1.570796f;

        public static float EaseIn(float time, float startValue, float changeValue, float duration, float unusedOvershootOrAmplitude, float unusedPeriod)
        {
            return (((-changeValue * ((float) Math.Cos((double) ((time / duration) * 1.570796f)))) + changeValue) + startValue);
        }

        public static float EaseInOut(float time, float startValue, float changeValue, float duration, float unusedOvershootOrAmplitude, float unusedPeriod)
        {
            return (((-changeValue * 0.5f) * (((float) Math.Cos((double) ((3.141593f * time) / duration))) - 1f)) + startValue);
        }

        public static float EaseOut(float time, float startValue, float changeValue, float duration, float unusedOvershootOrAmplitude, float unusedPeriod)
        {
            return ((changeValue * ((float) Math.Sin((double) ((time / duration) * 1.570796f)))) + startValue);
        }
    }
}

