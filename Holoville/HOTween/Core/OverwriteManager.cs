﻿namespace Holoville.HOTween.Core
{
    using Holoville.HOTween;
    using Holoville.HOTween.Plugins.Core;
    using System;
    using System.Collections.Generic;

    internal class OverwriteManager
    {
        internal bool enabled;
        internal bool logWarnings;
        private readonly List<Tweener> runningTweens = new List<Tweener>();

        public void AddTween(Tweener p_tween)
        {
            if (this.enabled)
            {
                List<ABSTweenPlugin> plugins = p_tween.plugins;
                int num = this.runningTweens.Count - 1;
                int count = plugins.Count;
                for (int i = num; i > -1; i--)
                {
                    Tweener tweener = this.runningTweens[i];
                    List<ABSTweenPlugin> list2 = tweener.plugins;
                    int num4 = list2.Count;
                    if (tweener.target == p_tween.target)
                    {
                        for (int j = 0; j < count; j++)
                        {
                            ABSTweenPlugin plugin = plugins[j];
                            for (int k = num4 - 1; k > -1; k--)
                            {
                                ABSTweenPlugin plugin2 = list2[k];
                                if ((plugin2.propName == plugin.propName) && (((plugin.pluginId == -1) || (plugin2.pluginId == -1)) || (plugin2.pluginId == plugin.pluginId)))
                                {
                                    if ((tweener.isSequenced && p_tween.isSequenced) && (tweener.contSequence == p_tween.contSequence))
                                    {
                                        break;
                                    }
                                    if (!tweener._isPaused && (!tweener.isSequenced || !tweener.isComplete))
                                    {
                                        list2.RemoveAt(k);
                                        num4--;
                                        if (Holoville.HOTween.HOTween.isEditor && (Holoville.HOTween.HOTween.warningLevel == WarningLevel.Verbose))
                                        {
                                            string str = plugin.GetType().ToString();
                                            str = str.Substring(str.LastIndexOf(".") + 1);
                                            string str2 = plugin2.GetType().ToString();
                                            str2 = str2.Substring(str2.LastIndexOf(".") + 1);
                                            if (this.logWarnings)
                                            {
                                                TweenWarning.Log(string.Concat(new object[] { str, " is overwriting ", str2, " for ", tweener.target, ".", plugin2.propName }));
                                            }
                                        }
                                        if (num4 == 0)
                                        {
                                            if (tweener.isSequenced)
                                            {
                                                tweener.contSequence.Remove(tweener);
                                            }
                                            this.runningTweens.RemoveAt(i);
                                            tweener.Kill(false);
                                        }
                                        if (tweener.onPluginOverwritten != null)
                                        {
                                            tweener.onPluginOverwritten();
                                        }
                                        else if (tweener.onPluginOverwrittenWParms != null)
                                        {
                                            tweener.onPluginOverwrittenWParms(new TweenEvent(tweener, tweener.onPluginOverwrittenParms));
                                        }
                                        if (tweener.destroyed)
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.runningTweens.Add(p_tween);
        }

        public void RemoveTween(Tweener p_tween)
        {
            int count = this.runningTweens.Count;
            for (int i = 0; i < count; i++)
            {
                if (this.runningTweens[i] == p_tween)
                {
                    this.runningTweens.RemoveAt(i);
                    break;
                }
            }
        }
    }
}

