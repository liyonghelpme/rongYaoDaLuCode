﻿namespace Holoville.HOTween.Core
{
    using System;

    public class TweenException : Exception
    {
        public TweenException(string p_message) : base(p_message)
        {
        }
    }
}

