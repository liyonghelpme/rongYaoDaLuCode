﻿namespace Holoville.HOTween.Core
{
    using Holoville.HOTween;
    using System;
    using System.Collections.Generic;

    public class TweenInfo
    {
        public bool isSequence;
        public List<object> targets;
        public ABSTweenComponent tween;

        public TweenInfo(ABSTweenComponent tween)
        {
            this.tween = tween;
            this.isSequence = tween is Sequence;
            this.targets = tween.GetTweenTargets();
        }

        public bool isComplete
        {
            get
            {
                return this.tween.isComplete;
            }
        }

        public bool isEnabled
        {
            get
            {
                return this.tween.enabled;
            }
        }

        public bool isPaused
        {
            get
            {
                return this.tween.isPaused;
            }
        }
    }
}

