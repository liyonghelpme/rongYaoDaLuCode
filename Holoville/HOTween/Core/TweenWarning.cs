﻿namespace Holoville.HOTween.Core
{
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    internal static class TweenWarning
    {
        internal static void Log(string p_message)
        {
            Log(p_message, false);
        }

        internal static void Log(string p_message, bool p_verbose)
        {
            if ((Holoville.HOTween.HOTween.warningLevel != WarningLevel.None) && (!p_verbose || (Holoville.HOTween.HOTween.warningLevel == WarningLevel.Verbose)))
            {
                Debug.LogWarning("HOTween : " + p_message);
            }
        }
    }
}

