﻿namespace Holoville.HOTween.Core
{
    using System;
    using UnityEngine;

    internal static class Utils
    {
        internal static Quaternion MatrixToQuaternion(Matrix4x4 m)
        {
            Quaternion quaternion = new Quaternion();
            float num = ((1f + m[0, 0]) + m[1, 1]) + m[2, 2];
            if (num < 0f)
            {
                num = 0f;
            }
            quaternion.w = ((float) Math.Sqrt((double) num)) * 0.5f;
            num = ((1f + m[0, 0]) - m[1, 1]) - m[2, 2];
            if (num < 0f)
            {
                num = 0f;
            }
            quaternion.x = ((float) Math.Sqrt((double) num)) * 0.5f;
            num = ((1f - m[0, 0]) + m[1, 1]) - m[2, 2];
            if (num < 0f)
            {
                num = 0f;
            }
            quaternion.y = ((float) Math.Sqrt((double) num)) * 0.5f;
            num = ((1f - m[0, 0]) - m[1, 1]) + m[2, 2];
            if (num < 0f)
            {
                num = 0f;
            }
            quaternion.z = ((float) Math.Sqrt((double) num)) * 0.5f;
            quaternion.x *= Mathf.Sign(quaternion.x * (m[2, 1] - m[1, 2]));
            quaternion.y *= Mathf.Sign(quaternion.y * (m[0, 2] - m[2, 0]));
            quaternion.z *= Mathf.Sign(quaternion.z * (m[1, 0] - m[0, 1]));
            return quaternion;
        }

        internal static string SimpleClassName(System.Type p_class)
        {
            string str = p_class.ToString();
            return str.Substring(str.LastIndexOf('.') + 1);
        }
    }
}

