﻿namespace Holoville.HOTween
{
    using System;

    public enum EaseType
    {
        AnimationCurve = 0x1f,
        EaseInBack = 0x19,
        EaseInBounce = 0x1c,
        EaseInCirc = 0x13,
        EaseInCubic = 7,
        EaseInElastic = 0x16,
        EaseInExpo = 0x10,
        EaseInOutBack = 0x1b,
        EaseInOutBounce = 30,
        EaseInOutCirc = 0x15,
        EaseInOutCubic = 9,
        EaseInOutElastic = 0x18,
        EaseInOutExpo = 0x12,
        EaseInOutQuad = 6,
        EaseInOutQuart = 12,
        EaseInOutQuint = 15,
        EaseInOutSine = 3,
        [Obsolete("Use EaseInOutQuint instead.")]
        EaseInOutStrong = 0x22,
        EaseInQuad = 4,
        EaseInQuart = 10,
        EaseInQuint = 13,
        EaseInSine = 1,
        [Obsolete("Use EaseInQuint instead.")]
        EaseInStrong = 0x20,
        EaseOutBack = 0x1a,
        EaseOutBounce = 0x1d,
        EaseOutCirc = 20,
        EaseOutCubic = 8,
        EaseOutElastic = 0x17,
        EaseOutExpo = 0x11,
        EaseOutQuad = 5,
        EaseOutQuart = 11,
        EaseOutQuint = 14,
        EaseOutSine = 2,
        [Obsolete("Use EaseOutQuint instead.")]
        EaseOutStrong = 0x21,
        Linear = 0
    }
}

