﻿namespace Holoville.HOTween
{
    using Holoville.HOTween.Core;
    using Holoville.HOTween.Plugins;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class HOTween : MonoBehaviour
    {
        public const string AUTHOR = "Daniele Giardini - Holoville";
        public static float defEaseOvershootOrAmplitude = 1.70158f;
        public static float defEasePeriod = 0f;
        public static EaseType defEaseType = EaseType.EaseOutQuad;
        public static LoopType defLoopType = LoopType.Restart;
        public static float defTimeScale = 1f;
        public static UpdateType defUpdateType = UpdateType.Update;
        private const string GAMEOBJNAME = "HOTween";
        private static bool initialized;
        internal static bool isEditor;
        internal static bool isIOS;
        private static bool isPermanent;
        private static bool isQuitting;
        private static Holoville.HOTween.HOTween it;
        internal static List<ABSTweenComponent> onCompletes = new List<ABSTweenComponent>();
        internal static OverwriteManager overwriteManager;
        private static bool renameInstToCountTw;
        public static bool showPathGizmos;
        private static float time;
        private static GameObject tweenGOInstance;
        private static List<ABSTweenComponent> tweens;
        private static List<int> tweensToRemoveIndexes = new List<int>();
        public static readonly string VERSION = "1.3.010";
        public static WarningLevel warningLevel = WarningLevel.Verbose;

        internal static void AddSequence(Sequence p_sequence)
        {
            if (!initialized)
            {
                Init();
            }
            AddTween(p_sequence);
        }

        private static void AddTween(ABSTweenComponent p_tween)
        {
            if (tweenGOInstance == null)
            {
                NewTweenInstance();
            }
            if (tweens == null)
            {
                tweens = new List<ABSTweenComponent>();
                it.StartCoroutines();
            }
            tweens.Add(p_tween);
            SetGOName();
        }

        private static bool CheckClear()
        {
            if ((tweens == null) || (tweens.Count == 0))
            {
                Clear();
                if (isPermanent)
                {
                    SetGOName();
                }
                return true;
            }
            SetGOName();
            return false;
        }

        private static void Clear()
        {
            if (it != null)
            {
                it.StopAllCoroutines();
            }
            tweens = null;
            if (!isPermanent)
            {
                if (tweenGOInstance != null)
                {
                    UnityEngine.Object.Destroy(tweenGOInstance);
                }
                tweenGOInstance = null;
                it = null;
            }
        }

        public static int Complete()
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredComplete), true);
        }

        public static int Complete(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredComplete), true);
        }

        public static int Complete(Tweener p_tweener)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredComplete), true);
        }

        public static int Complete(int p_intId)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredComplete), true);
        }

        public static int Complete(object p_target)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredComplete), true);
        }

        public static int Complete(string p_id)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredComplete), true);
        }

        public static void DisableOverwriteManager()
        {
            if (overwriteManager != null)
            {
                overwriteManager.enabled = false;
            }
        }

        private static void DoFilteredComplete(int p_index, bool p_optionalBool)
        {
            tweens[p_index].Complete(false);
        }

        private static int DoFilteredIteration(object p_filter, TweenDelegate.FilterFunc p_operation, bool p_collectionChanger)
        {
            return DoFilteredIteration(p_filter, p_operation, p_collectionChanger, false);
        }

        private static int DoFilteredIteration(object p_filter, TweenDelegate.FilterFunc p_operation, bool p_collectionChanger, bool p_optionalBool)
        {
            if (tweens == null)
            {
                return 0;
            }
            int num = 0;
            int num2 = tweens.Count - 1;
            if (p_filter == null)
            {
                for (int i = num2; i > -1; i--)
                {
                    p_operation(i, p_optionalBool);
                    num++;
                }
            }
            else if (p_filter is int)
            {
                int num4 = (int) p_filter;
                for (int j = num2; j > -1; j--)
                {
                    if (tweens[j].intId == num4)
                    {
                        p_operation(j, p_optionalBool);
                        num++;
                    }
                }
            }
            else if (p_filter is string)
            {
                string str = (string) p_filter;
                for (int k = num2; k > -1; k--)
                {
                    if (tweens[k].id == str)
                    {
                        p_operation(k, p_optionalBool);
                        num++;
                    }
                }
            }
            else if (p_filter is Tweener)
            {
                Tweener tweener = p_filter as Tweener;
                for (int m = num2; m > -1; m--)
                {
                    if (tweens[m] == tweener)
                    {
                        p_operation(m, p_optionalBool);
                        num++;
                    }
                }
            }
            else if (p_filter is Sequence)
            {
                Sequence sequence = p_filter as Sequence;
                for (int n = num2; n > -1; n--)
                {
                    if (tweens[n] == sequence)
                    {
                        p_operation(n, p_optionalBool);
                        num++;
                    }
                }
            }
            else
            {
                for (int num9 = num2; num9 > -1; num9--)
                {
                    Tweener tweener2 = tweens[num9] as Tweener;
                    if ((tweener2 != null) && (tweener2.target == p_filter))
                    {
                        p_operation(num9, p_optionalBool);
                        num++;
                    }
                }
            }
            if (p_collectionChanger)
            {
                CheckClear();
            }
            return num;
        }

        private static void DoFilteredKill(int p_index, bool p_optionalBool)
        {
            tweens[p_index].Kill(false);
            if (isUpdateLoop)
            {
                if (tweensToRemoveIndexes.IndexOf(p_index) == -1)
                {
                    tweensToRemoveIndexes.Add(p_index);
                }
            }
            else
            {
                tweens.RemoveAt(p_index);
            }
        }

        private static void DoFilteredPause(int p_index, bool p_optionalBool)
        {
            tweens[p_index].Pause();
        }

        private static void DoFilteredPlay(int p_index, bool p_skipDelay)
        {
            ABSTweenComponent component = tweens[p_index];
            Tweener tweener = component as Tweener;
            if (tweener != null)
            {
                tweener.Play(p_skipDelay);
            }
            else
            {
                component.Play();
            }
        }

        private static void DoFilteredPlayBackwards(int p_index, bool p_optionalBool)
        {
            ABSTweenComponent component = tweens[p_index];
            Tweener tweener = component as Tweener;
            if (tweener != null)
            {
                tweener.PlayBackwards();
            }
            else
            {
                component.PlayBackwards();
            }
        }

        private static void DoFilteredPlayForward(int p_index, bool p_skipDelay)
        {
            ABSTweenComponent component = tweens[p_index];
            Tweener tweener = component as Tweener;
            if (tweener != null)
            {
                tweener.PlayForward(p_skipDelay);
            }
            else
            {
                component.PlayForward();
            }
        }

        private static void DoFilteredRestart(int p_index, bool p_skipDelay)
        {
            ABSTweenComponent component = tweens[p_index];
            Tweener tweener = component as Tweener;
            if (tweener != null)
            {
                tweener.Restart(p_skipDelay);
            }
            else
            {
                component.Restart();
            }
        }

        private static void DoFilteredReverse(int p_index, bool p_forcePlay = false)
        {
            tweens[p_index].Reverse(p_forcePlay);
        }

        private static void DoFilteredRewind(int p_index, bool p_skipDelay)
        {
            ABSTweenComponent component = tweens[p_index];
            Tweener tweener = component as Tweener;
            if (tweener != null)
            {
                tweener.Rewind(p_skipDelay);
            }
            else
            {
                component.Rewind();
            }
        }

        internal static void DoSendMessage(TweenEvent e)
        {
            GameObject obj2 = e.parms[0] as GameObject;
            if (obj2 != null)
            {
                string methodName = e.parms[1] as string;
                object obj3 = e.parms[2];
                SendMessageOptions options = (SendMessageOptions) ((int) e.parms[3]);
                if (obj3 != null)
                {
                    obj2.SendMessage(methodName, e.parms[2], options);
                }
                else
                {
                    obj2.SendMessage(methodName, options);
                }
            }
        }

        private static void DoUpdate(UpdateType p_updateType, float p_elapsed)
        {
            tweensToRemoveIndexes.Clear();
            isUpdateLoop = true;
            int count = tweens.Count;
            for (int i = 0; i < count; i++)
            {
                ABSTweenComponent component = tweens[i];
                if (((component.updateType == p_updateType) && component.Update(p_elapsed * component.timeScale)) && (component.destroyed || component.autoKillOnComplete))
                {
                    component.Kill(false);
                    if (tweensToRemoveIndexes.IndexOf(i) == -1)
                    {
                        tweensToRemoveIndexes.Add(i);
                    }
                }
            }
            isUpdateLoop = false;
            int num3 = tweensToRemoveIndexes.Count;
            if (num3 > 0)
            {
                tweensToRemoveIndexes.Sort();
                for (int j = 0; j < num3; j++)
                {
                    tweens.RemoveAt(tweensToRemoveIndexes[j] - j);
                }
            }
            int num5 = onCompletes.Count;
            if (num5 > 0)
            {
                for (int k = 0; k < num5; k++)
                {
                    onCompletes[k].OnCompleteDispatch();
                }
                onCompletes = new List<ABSTweenComponent>();
            }
        }

        public static void EnableOverwriteManager(bool logWarnings = true)
        {
            if (overwriteManager != null)
            {
                overwriteManager.enabled = true;
                overwriteManager.logWarnings = logWarnings;
            }
        }

        private void FixedUpdate()
        {
            if (tweens != null)
            {
                DoUpdate(UpdateType.FixedUpdate, Time.fixedDeltaTime);
                CheckClear();
            }
        }

        public static Tweener From(object p_target, float p_duration, TweenParms p_parms)
        {
            if (!initialized)
            {
                Init();
            }
            p_parms = p_parms.IsFrom();
            Tweener tweener = new Tweener(p_target, p_duration, p_parms);
            if (tweener.isEmpty)
            {
                return null;
            }
            AddTween(tweener);
            if (!tweener._isPaused)
            {
                tweener.Update(0f, true, true, false, true);
            }
            return tweener;
        }

        public static Tweener From(object p_target, float p_duration, string p_propName, object p_fromVal)
        {
            return From(p_target, p_duration, new TweenParms().Prop(p_propName, p_fromVal));
        }

        public static Tweener From(object p_target, float p_duration, string p_propName, object p_fromVal, bool p_isRelative)
        {
            return From(p_target, p_duration, new TweenParms().Prop(p_propName, p_fromVal, p_isRelative));
        }

        public static Tweener From(object p_target, float p_duration, string p_propName, object p_fromVal, bool p_isRelative, EaseType p_easeType, float p_delay)
        {
            return From(p_target, p_duration, new TweenParms().Prop(p_propName, p_fromVal, p_isRelative).Delay(p_delay).Ease(p_easeType));
        }

        public static List<IHOTweenComponent> GetAllPausedTweens()
        {
            if (tweens == null)
            {
                return new List<IHOTweenComponent>(1);
            }
            List<IHOTweenComponent> list = new List<IHOTweenComponent>(tweens.Count);
            foreach (ABSTweenComponent component in tweens)
            {
                if (component.isPaused)
                {
                    list.Add(component);
                }
            }
            return list;
        }

        public static List<IHOTweenComponent> GetAllPlayingTweens()
        {
            if (tweens == null)
            {
                return new List<IHOTweenComponent>(1);
            }
            List<IHOTweenComponent> list = new List<IHOTweenComponent>(tweens.Count);
            foreach (ABSTweenComponent component in tweens)
            {
                if (!component.isPaused)
                {
                    list.Add(component);
                }
            }
            return list;
        }

        public static List<IHOTweenComponent> GetAllTweens()
        {
            if (tweens == null)
            {
                return new List<IHOTweenComponent>(1);
            }
            List<IHOTweenComponent> list = new List<IHOTweenComponent>(tweens.Count);
            foreach (ABSTweenComponent component in tweens)
            {
                list.Add(component);
            }
            return list;
        }

        private static List<ABSTweenPlugin> GetPlugins()
        {
            if (tweens == null)
            {
                return null;
            }
            List<ABSTweenPlugin> list = new List<ABSTweenPlugin>();
            int count = tweens.Count;
            for (int i = 0; i < count; i++)
            {
                tweens[i].FillPluginsList(list);
            }
            return list;
        }

        public static List<Tweener> GetTweenersByTarget(object p_target, bool p_includeNestedTweens)
        {
            List<Tweener> list = new List<Tweener>();
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    ABSTweenComponent component = tweens[i];
                    Tweener item = component as Tweener;
                    if (item != null)
                    {
                        if (item.target == p_target)
                        {
                            list.Add(item);
                        }
                    }
                    else if (p_includeNestedTweens)
                    {
                        list.AddRange(((Sequence) component).GetTweenersByTarget(p_target));
                    }
                }
            }
            return list;
        }

        public static TweenInfo[] GetTweenInfos()
        {
            if (totTweens <= 0)
            {
                return null;
            }
            int count = tweens.Count;
            TweenInfo[] infoArray = new TweenInfo[count];
            for (int i = 0; i < count; i++)
            {
                infoArray[i] = new TweenInfo(tweens[i]);
            }
            return infoArray;
        }

        public static List<IHOTweenComponent> GetTweensById(string p_id, bool p_includeNestedTweens)
        {
            List<IHOTweenComponent> list = new List<IHOTweenComponent>();
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    ABSTweenComponent item = tweens[i];
                    if (p_includeNestedTweens)
                    {
                        list.AddRange(item.GetTweensById(p_id));
                    }
                    else if (item.id == p_id)
                    {
                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public static List<IHOTweenComponent> GetTweensByIntId(int p_intId, bool p_includeNestedTweens)
        {
            List<IHOTweenComponent> list = new List<IHOTweenComponent>();
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    ABSTweenComponent item = tweens[i];
                    if (p_includeNestedTweens)
                    {
                        list.AddRange(item.GetTweensByIntId(p_intId));
                    }
                    else if (item.intId == p_intId)
                    {
                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public static void Init()
        {
            Init(false, true, false);
        }

        public static void Init(bool p_permanentInstance)
        {
            Init(p_permanentInstance, true, false);
        }

        public static void Init(bool p_permanentInstance, bool p_renameInstanceToCountTweens, bool p_allowOverwriteManager)
        {
            if (!initialized)
            {
                initialized = true;
                isIOS = Application.platform == RuntimePlatform.IPhonePlayer;
                isEditor = Application.isEditor;
                isPermanent = p_permanentInstance;
                renameInstToCountTw = p_renameInstanceToCountTweens;
                if (p_allowOverwriteManager)
                {
                    overwriteManager = new OverwriteManager();
                }
                if (isPermanent && (tweenGOInstance == null))
                {
                    NewTweenInstance();
                    SetGOName();
                }
            }
        }

        public static bool IsLinkedTo(object p_target)
        {
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    ABSTweenComponent component = tweens[i];
                    if (component.IsLinkedTo(p_target))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsTweening(int p_id)
        {
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    if (tweens[i].IsTweening(p_id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsTweening(object p_target)
        {
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    if (tweens[i].IsTweening(p_target))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsTweening(string p_id)
        {
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    if (tweens[i].IsTweening(p_id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static int Kill()
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredKill), true);
        }

        public static int Kill(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredKill), true);
        }

        public static int Kill(Tweener p_tweener)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredKill), true);
        }

        public static int Kill(int p_intId)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredKill), true);
        }

        public static int Kill(object p_target)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredKill), true);
        }

        public static int Kill(string p_id)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredKill), true);
        }

        private void LateUpdate()
        {
            if (tweens != null)
            {
                DoUpdate(UpdateType.LateUpdate, Time.deltaTime);
                CheckClear();
            }
        }

        private static void NewTweenInstance()
        {
            tweenGOInstance = new GameObject("HOTween");
            it = tweenGOInstance.AddComponent<Holoville.HOTween.HOTween>();
            UnityEngine.Object.DontDestroyOnLoad(tweenGOInstance);
        }

        private void OnApplicationQuit()
        {
            isQuitting = true;
        }

        private void OnDestroy()
        {
            if (!isQuitting && (this == it))
            {
                Clear();
            }
        }

        private void OnDrawGizmos()
        {
            if ((tweens != null) && showPathGizmos)
            {
                List<ABSTweenPlugin> plugins = GetPlugins();
                int count = plugins.Count;
                for (int i = 0; i < count; i++)
                {
                    PlugVector3Path path = plugins[i] as PlugVector3Path;
                    if ((path != null) && (path.path != null))
                    {
                        path.path.GizmoDraw(path.pathPerc, false);
                    }
                }
            }
        }

        public static int Pause()
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPause), false);
        }

        public static int Pause(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPause), false);
        }

        public static int Pause(Tweener p_tweener)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPause), false);
        }

        public static int Pause(int p_intId)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPause), false);
        }

        public static int Pause(object p_target)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPause), false);
        }

        public static int Pause(string p_id)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPause), false);
        }

        public static int Play()
        {
            return Play(false);
        }

        public static int Play(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlay), false);
        }

        public static int Play(Tweener p_tweener)
        {
            return Play(p_tweener, false);
        }

        public static int Play(bool p_skipDelay)
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlay), false, p_skipDelay);
        }

        public static int Play(int p_intId)
        {
            return Play(p_intId, false);
        }

        public static int Play(object p_target)
        {
            return Play(p_target, false);
        }

        public static int Play(string p_id)
        {
            return Play(p_id, false);
        }

        public static int Play(Tweener p_tweener, bool p_skipDelay)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlay), false, p_skipDelay);
        }

        public static int Play(int p_intId, bool p_skipDelay)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlay), false, p_skipDelay);
        }

        public static int Play(object p_target, bool p_skipDelay)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlay), false, p_skipDelay);
        }

        public static int Play(string p_id, bool p_skipDelay)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlay), false, p_skipDelay);
        }

        public static int PlayBackwards()
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayBackwards), false);
        }

        public static int PlayBackwards(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayBackwards), false);
        }

        public static int PlayBackwards(Tweener p_tweener)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayBackwards), false);
        }

        public static int PlayBackwards(int p_intId)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayBackwards), false);
        }

        public static int PlayBackwards(object p_target)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayBackwards), false);
        }

        public static int PlayBackwards(string p_id)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayBackwards), false);
        }

        public static int PlayForward()
        {
            return PlayForward(false);
        }

        public static int PlayForward(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayForward), false);
        }

        public static int PlayForward(Tweener p_tweener)
        {
            return PlayForward(p_tweener, false);
        }

        public static int PlayForward(bool p_skipDelay)
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayForward), false, p_skipDelay);
        }

        public static int PlayForward(int p_intId)
        {
            return PlayForward(p_intId, false);
        }

        public static int PlayForward(object p_target)
        {
            return PlayForward(p_target, false);
        }

        public static int PlayForward(string p_id)
        {
            return PlayForward(p_id, false);
        }

        public static int PlayForward(Tweener p_tweener, bool p_skipDelay)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayForward), false, p_skipDelay);
        }

        public static int PlayForward(int p_intId, bool p_skipDelay)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayForward), false, p_skipDelay);
        }

        public static int PlayForward(object p_target, bool p_skipDelay)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayForward), false, p_skipDelay);
        }

        public static int PlayForward(string p_id, bool p_skipDelay)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredPlayForward), false, p_skipDelay);
        }

        public static Tweener Punch(object p_target, float p_duration, TweenParms p_parms, float p_punchAmplitude = 0.5f, float p_punchPeriod = 0.1f)
        {
            if (!initialized)
            {
                Init();
            }
            p_parms.Ease(EaseType.EaseOutElastic, p_punchAmplitude, p_punchPeriod);
            Tweener tweener = new Tweener(p_target, p_duration, p_parms);
            if (tweener.isEmpty)
            {
                return null;
            }
            AddTween(tweener);
            return tweener;
        }

        public static Tweener Punch(object p_target, float p_duration, string p_propName, object p_fromVal, float p_punchAmplitude = 0.5f, float p_punchPeriod = 0.1f)
        {
            TweenParms parms = new TweenParms().Prop(p_propName, p_fromVal).Ease(EaseType.EaseOutElastic, p_punchAmplitude, p_punchPeriod);
            return To(p_target, p_duration, parms);
        }

        public static Tweener Punch(object p_target, float p_duration, string p_propName, object p_fromVal, bool p_isRelative, float p_punchAmplitude = 0.5f, float p_punchPeriod = 0.1f)
        {
            TweenParms parms = new TweenParms().Prop(p_propName, p_fromVal, p_isRelative).Ease(EaseType.EaseOutElastic, p_punchAmplitude, p_punchPeriod);
            return To(p_target, p_duration, parms);
        }

        internal static void RemoveFromTweens(IHOTweenComponent p_tween)
        {
            if (tweens != null)
            {
                int count = tweens.Count;
                for (int i = 0; i < count; i++)
                {
                    if (tweens[i] == p_tween)
                    {
                        tweens.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        public static int Restart()
        {
            return Restart(false);
        }

        public static int Restart(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRestart), false);
        }

        public static int Restart(Tweener p_tweener)
        {
            return Restart(p_tweener, false);
        }

        public static int Restart(bool p_skipDelay)
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRestart), false, p_skipDelay);
        }

        public static int Restart(int p_intId)
        {
            return Restart(p_intId, false);
        }

        public static int Restart(object p_target)
        {
            return Restart(p_target, false);
        }

        public static int Restart(string p_id)
        {
            return Restart(p_id, false);
        }

        public static int Restart(Tweener p_tweener, bool p_skipDelay)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRestart), false, p_skipDelay);
        }

        public static int Restart(int p_intId, bool p_skipDelay)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRestart), false, p_skipDelay);
        }

        public static int Restart(object p_target, bool p_skipDelay)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRestart), false, p_skipDelay);
        }

        public static int Restart(string p_id, bool p_skipDelay)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRestart), false, p_skipDelay);
        }

        public static int Reverse(bool p_forcePlay = false)
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredReverse), p_forcePlay);
        }

        public static int Reverse(Sequence p_sequence, bool p_forcePlay = false)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredReverse), p_forcePlay);
        }

        public static int Reverse(Tweener p_tweener, bool p_forcePlay = false)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredReverse), p_forcePlay);
        }

        public static int Reverse(int p_intId, bool p_forcePlay = false)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredReverse), p_forcePlay);
        }

        public static int Reverse(object p_target, bool p_forcePlay = false)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredReverse), p_forcePlay);
        }

        public static int Reverse(string p_id, bool p_forcePlay = false)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredReverse), p_forcePlay);
        }

        public static int Rewind()
        {
            return Rewind(false);
        }

        public static int Rewind(Sequence p_sequence)
        {
            return DoFilteredIteration(p_sequence, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRewind), false);
        }

        public static int Rewind(Tweener p_tweener)
        {
            return Rewind(p_tweener, false);
        }

        public static int Rewind(bool p_skipDelay)
        {
            return DoFilteredIteration(null, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRewind), false, p_skipDelay);
        }

        public static int Rewind(int p_intId)
        {
            return Rewind(p_intId, false);
        }

        public static int Rewind(object p_target)
        {
            return Rewind(p_target, false);
        }

        public static int Rewind(string p_id)
        {
            return Rewind(p_id, false);
        }

        public static int Rewind(Tweener p_tweener, bool p_skipDelay)
        {
            return DoFilteredIteration(p_tweener, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRewind), false, p_skipDelay);
        }

        public static int Rewind(int p_intId, bool p_skipDelay)
        {
            return DoFilteredIteration(p_intId, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRewind), false, p_skipDelay);
        }

        public static int Rewind(object p_target, bool p_skipDelay)
        {
            return DoFilteredIteration(p_target, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRewind), false, p_skipDelay);
        }

        public static int Rewind(string p_id, bool p_skipDelay)
        {
            return DoFilteredIteration(p_id, new TweenDelegate.FilterFunc(Holoville.HOTween.HOTween.DoFilteredRewind), false, p_skipDelay);
        }

        private static void SetGOName()
        {
            if (((isEditor && renameInstToCountTw) && !isQuitting) && (tweenGOInstance != null))
            {
                tweenGOInstance.name = "HOTween : " + totTweens;
            }
        }

        public static Tweener Shake(object p_target, float p_duration, TweenParms p_parms, float p_shakeAmplitude = 0.1f, float p_shakePeriod = 0.12f)
        {
            if (!initialized)
            {
                Init();
            }
            p_parms.Ease(EaseType.EaseOutElastic, p_shakeAmplitude, p_shakePeriod).IsFrom();
            Tweener tweener = new Tweener(p_target, p_duration, p_parms);
            if (tweener.isEmpty)
            {
                return null;
            }
            AddTween(tweener);
            return tweener;
        }

        public static Tweener Shake(object p_target, float p_duration, string p_propName, object p_fromVal, float p_shakeAmplitude = 0.1f, float p_shakePeriod = 0.12f)
        {
            TweenParms parms = new TweenParms().Prop(p_propName, p_fromVal).Ease(EaseType.EaseOutElastic, p_shakeAmplitude, p_shakePeriod);
            return From(p_target, p_duration, parms);
        }

        public static Tweener Shake(object p_target, float p_duration, string p_propName, object p_fromVal, bool p_isRelative, float p_shakeAmplitude = 0.1f, float p_shakePeriod = 0.12f)
        {
            TweenParms parms = new TweenParms().Prop(p_propName, p_fromVal, p_isRelative).Ease(EaseType.EaseOutElastic, p_shakeAmplitude, p_shakePeriod);
            return From(p_target, p_duration, parms);
        }

        private void StartCoroutines()
        {
            time = Time.realtimeSinceStartup;
            base.StartCoroutine(this.StartCoroutines_StartTimeScaleIndependentUpdate());
        }

        [DebuggerHidden]
        private IEnumerator StartCoroutines_StartTimeScaleIndependentUpdate()
        {
            return new <StartCoroutines_StartTimeScaleIndependentUpdate>c__Iterator14 { <>f__this = this };
        }

        [DebuggerHidden]
        private static IEnumerator TimeScaleIndependentUpdate()
        {
            return new <TimeScaleIndependentUpdate>c__Iterator13();
        }

        public static Tweener To(object p_target, float p_duration, TweenParms p_parms)
        {
            if (!initialized)
            {
                Init();
            }
            Tweener tweener = new Tweener(p_target, p_duration, p_parms);
            if (tweener.isEmpty)
            {
                return null;
            }
            AddTween(tweener);
            return tweener;
        }

        public static Tweener To(object p_target, float p_duration, string p_propName, object p_endVal)
        {
            return To(p_target, p_duration, new TweenParms().Prop(p_propName, p_endVal));
        }

        public static Tweener To(object p_target, float p_duration, string p_propName, object p_endVal, bool p_isRelative)
        {
            return To(p_target, p_duration, new TweenParms().Prop(p_propName, p_endVal, p_isRelative));
        }

        public static Tweener To(object p_target, float p_duration, string p_propName, object p_endVal, bool p_isRelative, EaseType p_easeType, float p_delay)
        {
            return To(p_target, p_duration, new TweenParms().Prop(p_propName, p_endVal, p_isRelative).Delay(p_delay).Ease(p_easeType));
        }

        private void Update()
        {
            if (tweens != null)
            {
                DoUpdate(UpdateType.Update, Time.deltaTime);
                CheckClear();
            }
        }

        internal static bool isUpdateLoop
        {
            [CompilerGenerated]
            get
            {
                return <isUpdateLoop>k__BackingField;
            }
            [CompilerGenerated]
            private set
            {
                <isUpdateLoop>k__BackingField = value;
            }
        }

        public static int totTweens
        {
            get
            {
                if (tweens == null)
                {
                    return 0;
                }
                return tweens.Count;
            }
        }

        [CompilerGenerated]
        private sealed class <StartCoroutines_StartTimeScaleIndependentUpdate>c__Iterator14 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal Holoville.HOTween.HOTween <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.<>f__this.StartCoroutine(Holoville.HOTween.HOTween.TimeScaleIndependentUpdate());
                        break;

                    default:
                        break;
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TimeScaleIndependentUpdate>c__Iterator13 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal float <elapsed>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (Holoville.HOTween.HOTween.tweens != null)
                        {
                            this.<elapsed>__0 = Time.realtimeSinceStartup - Holoville.HOTween.HOTween.time;
                            Holoville.HOTween.HOTween.time = Time.realtimeSinceStartup;
                            Holoville.HOTween.HOTween.DoUpdate(UpdateType.TimeScaleIndependentUpdate, this.<elapsed>__0);
                            if (!Holoville.HOTween.HOTween.CheckClear())
                            {
                                this.$current = null;
                                this.$PC = 1;
                                return true;
                            }
                            break;
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

