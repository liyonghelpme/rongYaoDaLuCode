﻿namespace Holoville.HOTween
{
    using Holoville.HOTween.Core;
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;

    public interface IHOTweenComponent
    {
        void ApplyCallback(CallbackType p_callbackType, TweenDelegate.TweenCallback p_callback);
        void ApplyCallback(CallbackType p_callbackType, TweenDelegate.TweenCallbackWParms p_callback, params object[] p_callbackParms);
        void Complete();
        bool GoTo(float p_time);
        bool GoToAndPlay(float p_time);
        bool IsLinkedTo(object p_target);
        bool IsTweening(object p_target);
        void Kill();
        void Pause();
        void Play();
        void PlayBackwards();
        void PlayForward();
        void Restart();
        void Reverse(bool p_forcePlay = false);
        void Rewind();
        IEnumerator WaitForCompletion();
        IEnumerator WaitForRewind();

        bool autoKillOnComplete { get; set; }

        int completedLoops { get; }

        float duration { get; }

        float elapsed { get; }

        bool enabled { get; set; }

        float fullDuration { get; }

        float fullElapsed { get; }

        bool hasStarted { get; }

        string id { get; set; }

        int intId { get; set; }

        bool isComplete { get; }

        bool isEmpty { get; }

        bool isLoopingBack { get; }

        bool isPaused { get; }

        bool isReversed { get; }

        bool isSequenced { get; }

        int loops { get; set; }

        LoopType loopType { get; set; }

        float position { get; set; }

        float timeScale { get; set; }

        UpdateType updateType { get; }
    }
}

