﻿namespace Holoville.HOTween
{
    using System;

    public enum LoopType
    {
        Restart,
        Yoyo,
        YoyoInverse,
        Incremental
    }
}

