﻿namespace Holoville.HOTween.Plugins.Core
{
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    public class PlugColor : ABSTweenPlugin
    {
        private Color diffChangeVal;
        private Color typedEndVal;
        private Color typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(Color) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(Color) };

        public PlugColor(Color p_endVal) : base(p_endVal, false)
        {
        }

        public PlugColor(Color p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugColor(Color p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugColor(Color p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugColor(Color p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            float num = base.ease(p_totElapsed, 0f, 1f, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            this.SetValue(new Color(this.typedStartVal.r + (this.diffChangeVal.r * num), this.typedStartVal.g + (this.diffChangeVal.g * num), this.typedStartVal.b + (this.diffChangeVal.b * num), this.typedStartVal.a + (this.diffChangeVal.a * num)));
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = 1f / p_speed;
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        protected override void SetChangeVal()
        {
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.typedEndVal = this.typedStartVal + this.typedEndVal;
            }
            this.diffChangeVal = this.typedEndVal - this.typedStartVal;
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            this.typedStartVal += this.diffChangeVal * p_diffIncr;
            this.typedEndVal += this.diffChangeVal * p_diffIncr;
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                base._endVal = this.typedEndVal = (Color) value;
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    base._startVal = this.typedStartVal = this.typedEndVal + ((Color) value);
                }
                else
                {
                    base._startVal = this.typedStartVal = (Color) value;
                }
            }
        }
    }
}

