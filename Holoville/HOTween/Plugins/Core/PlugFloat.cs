﻿namespace Holoville.HOTween.Plugins.Core
{
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    public class PlugFloat : ABSTweenPlugin
    {
        private float changeVal;
        private float typedEndVal;
        private float typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(float), typeof(int) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(float) };

        public PlugFloat(float p_endVal) : base(p_endVal, false)
        {
        }

        public PlugFloat(float p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugFloat(float p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugFloat(float p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugFloat(float p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            float num = base.ease(p_totElapsed, this.typedStartVal, this.changeVal, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            if (base.tweenObj.pixelPerfect)
            {
                num = (int) num;
            }
            this.SetValue(num);
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.changeVal / p_speed;
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        protected override void SetChangeVal()
        {
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.changeVal = this.typedEndVal;
                this.endVal = this.typedStartVal + this.typedEndVal;
            }
            else
            {
                this.changeVal = this.typedEndVal - this.typedStartVal;
            }
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            this.typedStartVal += this.changeVal * p_diffIncr;
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                base._endVal = this.typedEndVal = Convert.ToSingle(value);
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    base._startVal = this.typedStartVal = this.typedEndVal + Convert.ToSingle(value);
                }
                else
                {
                    base._startVal = this.typedStartVal = Convert.ToSingle(value);
                }
            }
        }
    }
}

