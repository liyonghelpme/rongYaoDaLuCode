﻿namespace Holoville.HOTween.Plugins.Core
{
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    public class PlugRect : ABSTweenPlugin
    {
        private Rect diffChangeVal;
        private Rect typedEndVal;
        private Rect typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(Rect) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(Rect) };

        public PlugRect(Rect p_endVal) : base(p_endVal, false)
        {
        }

        public PlugRect(Rect p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugRect(Rect p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugRect(Rect p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugRect(Rect p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            float num = base.ease(p_totElapsed, 0f, 1f, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            Rect rect = new Rect {
                x = this.typedStartVal.x + (this.diffChangeVal.x * num),
                y = this.typedStartVal.y + (this.diffChangeVal.y * num),
                width = this.typedStartVal.width + (this.diffChangeVal.width * num),
                height = this.typedStartVal.height + (this.diffChangeVal.height * num)
            };
            this.SetValue(rect);
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.typedEndVal.width - this.typedStartVal.width;
            float num2 = this.typedEndVal.height - this.typedStartVal.height;
            float num3 = (float) Math.Sqrt((double) ((num * num) + (num2 * num2)));
            float num4 = num3 / p_speed;
            if (num4 < 0f)
            {
                num4 = -num4;
            }
            return num4;
        }

        protected override void SetChangeVal()
        {
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.typedEndVal.x += this.typedStartVal.x;
                this.typedEndVal.y += this.typedStartVal.y;
                this.typedEndVal.width += this.typedStartVal.width;
                this.typedEndVal.height += this.typedStartVal.height;
            }
            this.diffChangeVal = new Rect();
            this.diffChangeVal.x = this.typedEndVal.x - this.typedStartVal.x;
            this.diffChangeVal.y = this.typedEndVal.y - this.typedStartVal.y;
            this.diffChangeVal.width = this.typedEndVal.width - this.typedStartVal.width;
            this.diffChangeVal.height = this.typedEndVal.height - this.typedStartVal.height;
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            Rect rect;
            rect = new Rect(this.diffChangeVal.x, this.diffChangeVal.y, this.diffChangeVal.width, this.diffChangeVal.height) {
                x = rect.x * p_diffIncr,
                y = rect.y * p_diffIncr,
                width = rect.width * p_diffIncr,
                height = rect.height * p_diffIncr
            };
            this.typedStartVal.x += rect.x;
            this.typedStartVal.y += rect.y;
            this.typedStartVal.width += rect.width;
            this.typedStartVal.height += rect.height;
            this.typedEndVal.x += rect.x;
            this.typedEndVal.y += rect.y;
            this.typedEndVal.width += rect.width;
            this.typedEndVal.height += rect.height;
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                base._endVal = this.typedEndVal = (Rect) value;
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    this.typedStartVal = (Rect) value;
                    this.typedStartVal.x += this.typedEndVal.x;
                    this.typedStartVal.y += this.typedEndVal.y;
                    this.typedStartVal.width += this.typedEndVal.width;
                    this.typedStartVal.height += this.typedEndVal.height;
                    base._startVal = this.typedStartVal;
                }
                else
                {
                    base._startVal = this.typedStartVal = (Rect) value;
                }
            }
        }
    }
}

