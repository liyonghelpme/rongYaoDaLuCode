﻿namespace Holoville.HOTween.Plugins.Core
{
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    public class PlugVector2 : ABSTweenPlugin
    {
        private Vector2 changeVal;
        private Vector2 typedEndVal;
        private Vector2 typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(Vector2) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(Vector2) };

        public PlugVector2(Vector2 p_endVal) : base(p_endVal, false)
        {
        }

        public PlugVector2(Vector2 p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugVector2(Vector2 p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugVector2(Vector2 p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugVector2(Vector2 p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            float num = base.ease(p_totElapsed, 0f, 1f, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            if (base.tweenObj.pixelPerfect)
            {
                this.SetValue(new Vector2((float) ((int) (this.typedStartVal.x + (this.changeVal.x * num))), (float) ((int) (this.typedStartVal.y + (this.changeVal.y * num)))));
            }
            else
            {
                this.SetValue(new Vector2(this.typedStartVal.x + (this.changeVal.x * num), this.typedStartVal.y + (this.changeVal.y * num)));
            }
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.changeVal.magnitude / p_speed;
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        protected override void SetChangeVal()
        {
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.changeVal = this.typedEndVal;
                this.endVal = this.typedStartVal + this.typedEndVal;
            }
            else
            {
                this.changeVal = new Vector2(this.typedEndVal.x - this.typedStartVal.x, this.typedEndVal.y - this.typedStartVal.y);
            }
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            this.typedStartVal += this.changeVal * p_diffIncr;
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                base._endVal = this.typedEndVal = (Vector2) value;
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    base._startVal = this.typedStartVal = this.typedEndVal + ((Vector2) value);
                }
                else
                {
                    base._startVal = this.typedStartVal = (Vector2) value;
                }
            }
        }
    }
}

