﻿namespace Holoville.HOTween.Plugins
{
    using Holoville.HOTween;
    using Holoville.HOTween.Plugins.Core;
    using System;
    using UnityEngine;

    public class PlugQuaternion : ABSTweenPlugin
    {
        private bool beyond360;
        private Vector3 changeVal;
        private Vector3 typedEndVal;
        private Vector3 typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(Quaternion) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(Vector3), typeof(Quaternion) };

        public PlugQuaternion(Quaternion p_endVal) : base(p_endVal, false)
        {
        }

        public PlugQuaternion(Vector3 p_endVal) : base(p_endVal, false)
        {
        }

        public PlugQuaternion(Quaternion p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugQuaternion(Quaternion p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugQuaternion(Vector3 p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugQuaternion(Vector3 p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugQuaternion(Quaternion p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugQuaternion(Quaternion p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        public PlugQuaternion(Vector3 p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugQuaternion(Vector3 p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        public PlugQuaternion Beyond360()
        {
            return this.Beyond360(true);
        }

        public PlugQuaternion Beyond360(bool p_beyond360)
        {
            this.beyond360 = p_beyond360;
            return this;
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            float num = base.ease(p_totElapsed, 0f, 1f, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            this.SetValue(Quaternion.Euler(new Vector3(this.typedStartVal.x + (this.changeVal.x * num), this.typedStartVal.y + (this.changeVal.y * num), this.typedStartVal.z + (this.changeVal.z * num))));
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.changeVal.magnitude / (p_speed * 360f);
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        protected override void SetChangeVal()
        {
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.changeVal = this.typedEndVal;
                this.endVal = this.typedStartVal + this.typedEndVal;
            }
            else if (this.beyond360)
            {
                this.changeVal = this.typedEndVal - this.typedStartVal;
            }
            else
            {
                Vector3 typedEndVal = this.typedEndVal;
                if (typedEndVal.x > 360f)
                {
                    typedEndVal.x = typedEndVal.x % 360f;
                }
                if (typedEndVal.y > 360f)
                {
                    typedEndVal.y = typedEndVal.y % 360f;
                }
                if (typedEndVal.z > 360f)
                {
                    typedEndVal.z = typedEndVal.z % 360f;
                }
                this.changeVal = typedEndVal - this.typedStartVal;
                float num = (this.changeVal.x <= 0f) ? -this.changeVal.x : this.changeVal.x;
                if (num > 180f)
                {
                    this.changeVal.x = (this.changeVal.x <= 0f) ? (360f - num) : -(360f - num);
                }
                num = (this.changeVal.y <= 0f) ? -this.changeVal.y : this.changeVal.y;
                if (num > 180f)
                {
                    this.changeVal.y = (this.changeVal.y <= 0f) ? (360f - num) : -(360f - num);
                }
                num = (this.changeVal.z <= 0f) ? -this.changeVal.z : this.changeVal.z;
                if (num > 180f)
                {
                    this.changeVal.z = (this.changeVal.z <= 0f) ? (360f - num) : -(360f - num);
                }
            }
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            this.typedStartVal += this.changeVal * p_diffIncr;
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                if (value is Quaternion)
                {
                    base._endVal = value;
                    Quaternion quaternion = (Quaternion) value;
                    this.typedEndVal = quaternion.eulerAngles;
                }
                else
                {
                    base._endVal = Quaternion.Euler((Vector3) value);
                    this.typedEndVal = (Vector3) value;
                }
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    this.typedStartVal = this.typedEndVal + (!(value is Quaternion) ? ((Vector3) value) : ((Quaternion) value).eulerAngles);
                    base._startVal = Quaternion.Euler(this.typedStartVal);
                }
                else if (value is Quaternion)
                {
                    base._startVal = value;
                    Quaternion quaternion2 = (Quaternion) value;
                    this.typedStartVal = quaternion2.eulerAngles;
                }
                else
                {
                    base._startVal = Quaternion.Euler((Vector3) value);
                    this.typedStartVal = (Vector3) value;
                }
            }
        }
    }
}

