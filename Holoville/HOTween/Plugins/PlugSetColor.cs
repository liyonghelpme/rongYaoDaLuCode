﻿namespace Holoville.HOTween.Plugins
{
    using Holoville.HOTween;
    using Holoville.HOTween.Plugins.Core;
    using System;
    using UnityEngine;

    public class PlugSetColor : ABSTweenPlugin
    {
        private float changeVal;
        private string colorName;
        private Color diffChangeVal;
        private Color typedEndVal;
        private Color typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(Color) };
        internal static System.Type[] validTargetTypes = new System.Type[] { typeof(Material) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(Color) };

        public PlugSetColor(Color p_endVal) : this(p_endVal, false)
        {
        }

        public PlugSetColor(Color p_endVal, EaseType p_easeType) : this(p_endVal, p_easeType, false)
        {
        }

        public PlugSetColor(Color p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
            base.ignoreAccessor = true;
        }

        public PlugSetColor(Color p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
            base.ignoreAccessor = true;
        }

        public PlugSetColor(Color p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
            base.ignoreAccessor = true;
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            float t = base.ease(p_totElapsed, 0f, this.changeVal, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            this.SetValue(Color.Lerp(this.typedStartVal, this.typedEndVal, t));
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.changeVal / p_speed;
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        protected override object GetValue()
        {
            return ((Material) base.tweenObj.target).GetColor(this.colorName);
        }

        public PlugSetColor Property(ColorName p_colorName)
        {
            return this.Property(p_colorName.ToString());
        }

        public PlugSetColor Property(string p_propertyName)
        {
            this.colorName = p_propertyName;
            return this;
        }

        protected override void SetChangeVal()
        {
            this.changeVal = 1f;
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.typedEndVal = this.typedStartVal + this.typedEndVal;
            }
            this.diffChangeVal = this.typedEndVal - this.typedStartVal;
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            this.typedStartVal += this.diffChangeVal * p_diffIncr;
            this.typedEndVal += this.diffChangeVal * p_diffIncr;
        }

        protected override void SetValue(object p_value)
        {
            ((Material) base.tweenObj.target).SetColor(this.colorName, (Color) p_value);
        }

        internal override bool ValidateTarget(object p_target)
        {
            return (p_target is Material);
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                base._endVal = this.typedEndVal = (Color) value;
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    base._startVal = this.typedStartVal = this.typedEndVal + ((Color) value);
                }
                else
                {
                    base._startVal = this.typedStartVal = (Color) value;
                }
            }
        }

        public enum ColorName
        {
            _Color,
            _SpecColor,
            _Emission,
            _ReflectColor
        }
    }
}

