﻿namespace Holoville.HOTween.Plugins
{
    using Holoville.HOTween;
    using Holoville.HOTween.Plugins.Core;
    using System;
    using UnityEngine;

    public class PlugSetFloat : ABSTweenPlugin
    {
        private float changeVal;
        private string floatName;
        private float typedEndVal;
        private float typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(Color) };
        internal static System.Type[] validTargetTypes = new System.Type[] { typeof(Material) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(float) };

        public PlugSetFloat(float p_endVal) : this(p_endVal, false)
        {
        }

        public PlugSetFloat(float p_endVal, EaseType p_easeType) : this(p_endVal, p_easeType, false)
        {
        }

        public PlugSetFloat(float p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
            base.ignoreAccessor = true;
        }

        public PlugSetFloat(float p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
            base.ignoreAccessor = true;
        }

        public PlugSetFloat(float p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
            base.ignoreAccessor = true;
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            this.SetValue(base.ease(p_totElapsed, this.typedStartVal, this.changeVal, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod));
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.changeVal / p_speed;
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        protected override object GetValue()
        {
            return ((Material) base.tweenObj.target).GetFloat(this.floatName);
        }

        public PlugSetFloat Property(string p_propertyName)
        {
            this.floatName = p_propertyName;
            return this;
        }

        protected override void SetChangeVal()
        {
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.changeVal = this.typedEndVal;
                this.endVal = this.typedStartVal + this.typedEndVal;
            }
            else
            {
                this.changeVal = this.typedEndVal - this.typedStartVal;
            }
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            this.typedStartVal += this.changeVal * p_diffIncr;
        }

        protected override void SetValue(object p_value)
        {
            ((Material) base.tweenObj.target).SetFloat(this.floatName, Convert.ToSingle(p_value));
        }

        internal override bool ValidateTarget(object p_target)
        {
            return (p_target is Material);
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                base._endVal = this.typedEndVal = Convert.ToSingle(value);
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    base._startVal = this.typedStartVal = this.typedEndVal + Convert.ToSingle(value);
                }
                else
                {
                    base._startVal = this.typedStartVal = Convert.ToSingle(value);
                }
            }
        }
    }
}

