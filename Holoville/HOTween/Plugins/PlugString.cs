﻿namespace Holoville.HOTween.Plugins
{
    using Holoville.HOTween;
    using Holoville.HOTween.Plugins.Core;
    using System;
    using UnityEngine;

    public class PlugString : ABSTweenPlugin
    {
        private float changeVal;
        private string typedEndVal;
        private string typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(string) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(string) };

        public PlugString(string p_endVal) : base(p_endVal, false)
        {
        }

        public PlugString(string p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugString(string p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugString(string p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugString(string p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            string str;
            int length = (int) Math.Round((double) base.ease(p_totElapsed, 0f, this.changeVal, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod));
            if (base.isRelative)
            {
                str = this.typedStartVal + this.typedEndVal.Substring(0, length);
            }
            else
            {
                str = this.typedEndVal.Substring(0, length) + (((length < this.changeVal) && (length < this.typedStartVal.Length)) ? this.typedStartVal.Substring(length) : string.Empty);
            }
            this.SetValue(str);
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.changeVal / p_speed;
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        protected override void SetChangeVal()
        {
            this.changeVal = this.typedEndVal.Length;
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            if (p_diffIncr > 0)
            {
                while (p_diffIncr > 0)
                {
                    this.typedStartVal = this.typedStartVal + this.typedEndVal;
                    p_diffIncr--;
                }
            }
            else
            {
                this.typedStartVal = this.typedStartVal.Substring(0, this.typedStartVal.Length + (this.typedEndVal.Length * p_diffIncr));
            }
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                base._endVal = this.typedEndVal = (value != null) ? value.ToString() : string.Empty;
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom && base.isRelative)
                {
                    base._startVal = this.typedStartVal = this.typedEndVal + ((value != null) ? value.ToString() : string.Empty);
                }
                else
                {
                    base._startVal = this.typedStartVal = (value != null) ? value.ToString() : string.Empty;
                }
            }
        }
    }
}

