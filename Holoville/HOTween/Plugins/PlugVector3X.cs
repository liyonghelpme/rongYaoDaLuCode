﻿namespace Holoville.HOTween.Plugins
{
    using Holoville.HOTween;
    using Holoville.HOTween.Plugins.Core;
    using System;
    using UnityEngine;

    public class PlugVector3X : ABSTweenPlugin
    {
        protected float changeVal;
        protected float typedEndVal;
        protected float typedStartVal;
        internal static System.Type[] validPropTypes = new System.Type[] { typeof(Vector3) };
        internal static System.Type[] validValueTypes = new System.Type[] { typeof(float) };

        public PlugVector3X(float p_endVal) : base(p_endVal, false)
        {
        }

        public PlugVector3X(float p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugVector3X(float p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugVector3X(float p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugVector3X(float p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        internal override void Complete()
        {
            Vector3 vector = (Vector3) this.GetValue();
            vector.x = this.typedEndVal;
            this.SetValue(vector);
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            Vector3 vector = (Vector3) this.GetValue();
            vector.x = base.ease(p_totElapsed, this.typedStartVal, this.changeVal, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            if (base.tweenObj.pixelPerfect)
            {
                vector.x = (int) vector.x;
            }
            this.SetValue(vector);
        }

        protected override float GetSpeedBasedDuration(float p_speed)
        {
            float num = this.changeVal / p_speed;
            if (num < 0f)
            {
                num = -num;
            }
            return num;
        }

        internal override void Rewind()
        {
            Vector3 vector = (Vector3) this.GetValue();
            vector.x = this.typedStartVal;
            this.SetValue(vector);
        }

        protected override void SetChangeVal()
        {
            if (base.isRelative && !base.tweenObj.isFrom)
            {
                this.changeVal = this.typedEndVal;
                this.endVal = this.typedStartVal + this.typedEndVal;
            }
            else
            {
                this.changeVal = this.typedEndVal - this.typedStartVal;
            }
        }

        protected override void SetIncremental(int p_diffIncr)
        {
            this.typedStartVal += this.changeVal * p_diffIncr;
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                if (base.tweenObj.isFrom)
                {
                    base._endVal = value;
                    Vector3 vector = (Vector3) base._endVal;
                    this.typedEndVal = vector.x;
                }
                else
                {
                    base._endVal = this.typedEndVal = Convert.ToSingle(value);
                }
            }
        }

        internal override int pluginId
        {
            get
            {
                return 1;
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom)
                {
                    if (base.isRelative)
                    {
                        base._startVal = this.typedStartVal = this.typedEndVal + Convert.ToSingle(value);
                    }
                    else
                    {
                        base._startVal = this.typedStartVal = Convert.ToSingle(value);
                    }
                }
                else
                {
                    base._startVal = value;
                    Vector3 vector = (Vector3) base._startVal;
                    this.typedStartVal = vector.x;
                }
            }
        }
    }
}

