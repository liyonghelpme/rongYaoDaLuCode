﻿namespace Holoville.HOTween.Plugins
{
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    public class PlugVector3Z : PlugVector3X
    {
        public PlugVector3Z(float p_endVal) : base(p_endVal, false)
        {
        }

        public PlugVector3Z(float p_endVal, EaseType p_easeType) : base(p_endVal, p_easeType, false)
        {
        }

        public PlugVector3Z(float p_endVal, bool p_isRelative) : base(p_endVal, p_isRelative)
        {
        }

        public PlugVector3Z(float p_endVal, EaseType p_easeType, bool p_isRelative) : base(p_endVal, p_easeType, p_isRelative)
        {
        }

        public PlugVector3Z(float p_endVal, AnimationCurve p_easeAnimCurve, bool p_isRelative) : base(p_endVal, p_easeAnimCurve, p_isRelative)
        {
        }

        internal override void Complete()
        {
            Vector3 vector = (Vector3) this.GetValue();
            vector.z = base.typedEndVal;
            this.SetValue(vector);
        }

        protected override void DoUpdate(float p_totElapsed)
        {
            Vector3 vector = (Vector3) this.GetValue();
            vector.z = base.ease(p_totElapsed, base.typedStartVal, base.changeVal, base._duration, base.tweenObj.easeOvershootOrAmplitude, base.tweenObj.easePeriod);
            if (base.tweenObj.pixelPerfect)
            {
                vector.z = (int) vector.z;
            }
            this.SetValue(vector);
        }

        internal override void Rewind()
        {
            Vector3 vector = (Vector3) this.GetValue();
            vector.z = base.typedStartVal;
            this.SetValue(vector);
        }

        protected override object endVal
        {
            get
            {
                return base._endVal;
            }
            set
            {
                if (base.tweenObj.isFrom)
                {
                    base._endVal = value;
                    Vector3 vector = (Vector3) base._endVal;
                    base.typedEndVal = vector.z;
                }
                else
                {
                    base._endVal = base.typedEndVal = Convert.ToSingle(value);
                }
            }
        }

        internal override int pluginId
        {
            get
            {
                return 3;
            }
        }

        protected override object startVal
        {
            get
            {
                return base._startVal;
            }
            set
            {
                if (base.tweenObj.isFrom)
                {
                    if (base.isRelative)
                    {
                        base._startVal = base.typedStartVal = base.typedEndVal + Convert.ToSingle(value);
                    }
                    else
                    {
                        base._startVal = base.typedStartVal = Convert.ToSingle(value);
                    }
                }
                else
                {
                    base._startVal = value;
                    Vector3 vector = (Vector3) base._startVal;
                    base.typedStartVal = vector.z;
                }
            }
        }
    }
}

