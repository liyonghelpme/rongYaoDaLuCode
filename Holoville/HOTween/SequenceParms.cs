﻿namespace Holoville.HOTween
{
    using Holoville.HOTween.Core;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SequenceParms : ABSTweenComponentParms
    {
        public SequenceParms AutoKill(bool p_active)
        {
            base.autoKillOnComplete = p_active;
            return this;
        }

        public SequenceParms Id(string p_id)
        {
            base.id = p_id;
            return this;
        }

        internal void InitializeSequence(Sequence p_sequence)
        {
            base.InitializeOwner(p_sequence);
        }

        public SequenceParms IntId(int p_intId)
        {
            base.intId = p_intId;
            return this;
        }

        public SequenceParms KeepDisabled(Behaviour p_target)
        {
            if (p_target == null)
            {
                base.manageBehaviours = false;
                return this;
            }
            Behaviour[] behaviourArray1 = new Behaviour[] { p_target };
            return this.KeepEnabled(behaviourArray1, false);
        }

        public SequenceParms KeepDisabled(GameObject p_target)
        {
            if (p_target == null)
            {
                base.manageGameObjects = false;
                return this;
            }
            GameObject[] objArray1 = new GameObject[] { p_target };
            return this.KeepEnabled(objArray1, false);
        }

        public SequenceParms KeepDisabled(Behaviour[] p_targets)
        {
            return this.KeepEnabled(p_targets, false);
        }

        public SequenceParms KeepDisabled(GameObject[] p_targets)
        {
            return this.KeepEnabled(p_targets, false);
        }

        public SequenceParms KeepEnabled(Behaviour p_target)
        {
            if (p_target == null)
            {
                base.manageBehaviours = false;
                return this;
            }
            Behaviour[] behaviourArray1 = new Behaviour[] { p_target };
            return this.KeepEnabled(behaviourArray1, true);
        }

        public SequenceParms KeepEnabled(GameObject p_target)
        {
            if (p_target == null)
            {
                base.manageGameObjects = false;
                return this;
            }
            GameObject[] objArray1 = new GameObject[] { p_target };
            return this.KeepEnabled(objArray1, true);
        }

        public SequenceParms KeepEnabled(Behaviour[] p_targets)
        {
            return this.KeepEnabled(p_targets, true);
        }

        public SequenceParms KeepEnabled(GameObject[] p_targets)
        {
            return this.KeepEnabled(p_targets, true);
        }

        private SequenceParms KeepEnabled(Behaviour[] p_targets, bool p_enabled)
        {
            base.manageBehaviours = true;
            if (p_enabled)
            {
                base.managedBehavioursOn = p_targets;
            }
            else
            {
                base.managedBehavioursOff = p_targets;
            }
            return this;
        }

        private SequenceParms KeepEnabled(GameObject[] p_targets, bool p_enabled)
        {
            base.manageGameObjects = true;
            if (p_enabled)
            {
                base.managedGameObjectsOn = p_targets;
            }
            else
            {
                base.managedGameObjectsOff = p_targets;
            }
            return this;
        }

        public SequenceParms Loops(int p_loops)
        {
            return this.Loops(p_loops, Holoville.HOTween.HOTween.defLoopType);
        }

        public SequenceParms Loops(int p_loops, LoopType p_loopType)
        {
            base.loops = p_loops;
            base.loopType = p_loopType;
            return this;
        }

        public SequenceParms OnComplete(TweenDelegate.TweenCallback p_function)
        {
            base.onComplete = p_function;
            return this;
        }

        public SequenceParms OnComplete(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onCompleteWParms = p_function;
            base.onCompleteParms = p_funcParms;
            return this;
        }

        public SequenceParms OnComplete(GameObject p_sendMessageTarget, string p_methodName, object p_value = null, SendMessageOptions p_options = 0)
        {
            base.onCompleteWParms = new TweenDelegate.TweenCallbackWParms(Holoville.HOTween.HOTween.DoSendMessage);
            base.onCompleteParms = new object[] { p_sendMessageTarget, p_methodName, p_value, p_options };
            return this;
        }

        public SequenceParms OnPause(TweenDelegate.TweenCallback p_function)
        {
            base.onPause = p_function;
            return this;
        }

        public SequenceParms OnPause(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onPauseWParms = p_function;
            base.onPauseParms = p_funcParms;
            return this;
        }

        public SequenceParms OnPlay(TweenDelegate.TweenCallback p_function)
        {
            base.onPlay = p_function;
            return this;
        }

        public SequenceParms OnPlay(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onPlayWParms = p_function;
            base.onPlayParms = p_funcParms;
            return this;
        }

        public SequenceParms OnRewinded(TweenDelegate.TweenCallback p_function)
        {
            base.onRewinded = p_function;
            return this;
        }

        public SequenceParms OnRewinded(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onRewindedWParms = p_function;
            base.onRewindedParms = p_funcParms;
            return this;
        }

        public SequenceParms OnStart(TweenDelegate.TweenCallback p_function)
        {
            base.onStart = p_function;
            return this;
        }

        public SequenceParms OnStart(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onStartWParms = p_function;
            base.onStartParms = p_funcParms;
            return this;
        }

        public SequenceParms OnStepComplete(TweenDelegate.TweenCallback p_function)
        {
            base.onStepComplete = p_function;
            return this;
        }

        public SequenceParms OnStepComplete(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onStepCompleteWParms = p_function;
            base.onStepCompleteParms = p_funcParms;
            return this;
        }

        public SequenceParms OnStepComplete(GameObject p_sendMessageTarget, string p_methodName, object p_value = null, SendMessageOptions p_options = 0)
        {
            base.onStepCompleteWParms = new TweenDelegate.TweenCallbackWParms(Holoville.HOTween.HOTween.DoSendMessage);
            base.onStepCompleteParms = new object[] { p_sendMessageTarget, p_methodName, p_value, p_options };
            return this;
        }

        public SequenceParms OnUpdate(TweenDelegate.TweenCallback p_function)
        {
            base.onUpdate = p_function;
            return this;
        }

        public SequenceParms OnUpdate(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onUpdateWParms = p_function;
            base.onUpdateParms = p_funcParms;
            return this;
        }

        public SequenceParms TimeScale(float p_timeScale)
        {
            base.timeScale = p_timeScale;
            return this;
        }

        public SequenceParms UpdateType(Holoville.HOTween.UpdateType p_updateType)
        {
            base.updateType = p_updateType;
            return this;
        }
    }
}

