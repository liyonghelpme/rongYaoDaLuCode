﻿namespace Holoville.HOTween
{
    using Holoville.HOTween.Plugins.Core;
    using System;

    public class TweenEvent
    {
        private readonly object[] _parms;
        private readonly ABSTweenPlugin _plugin;
        private readonly IHOTweenComponent _tween;

        internal TweenEvent(IHOTweenComponent p_tween, object[] p_parms)
        {
            this._tween = p_tween;
            this._parms = p_parms;
            this._plugin = null;
        }

        internal TweenEvent(IHOTweenComponent p_tween, object[] p_parms, ABSTweenPlugin p_plugin)
        {
            this._tween = p_tween;
            this._parms = p_parms;
            this._plugin = p_plugin;
        }

        public object[] parms
        {
            get
            {
                return this._parms;
            }
        }

        public ABSTweenPlugin plugin
        {
            get
            {
                return this._plugin;
            }
        }

        public IHOTweenComponent tween
        {
            get
            {
                return this._tween;
            }
        }
    }
}

