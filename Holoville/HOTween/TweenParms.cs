﻿namespace Holoville.HOTween
{
    using Holoville.HOTween.Core;
    using Holoville.HOTween.Plugins;
    using Holoville.HOTween.Plugins.Core;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class TweenParms : ABSTweenComponentParms
    {
        private static readonly Dictionary<System.Type, string> _TypeToShortString;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map3;
        private float delay;
        private AnimationCurve easeAnimCurve;
        private float easeOvershootOrAmplitude = Holoville.HOTween.HOTween.defEaseOvershootOrAmplitude;
        private float easePeriod = Holoville.HOTween.HOTween.defEasePeriod;
        private EaseType easeType = Holoville.HOTween.HOTween.defEaseType;
        private bool isFrom;
        private TweenDelegate.TweenCallback onPluginOverwritten;
        private object[] onPluginOverwrittenParms;
        private TweenDelegate.TweenCallbackWParms onPluginOverwrittenWParms;
        private bool pixelPerfect;
        private List<HOTPropData> propDatas;
        private bool speedBased;

        static TweenParms()
        {
            Dictionary<System.Type, string> dictionary = new Dictionary<System.Type, string>(8);
            dictionary.Add(typeof(Vector2), "Vector2");
            dictionary.Add(typeof(Vector3), "Vector3");
            dictionary.Add(typeof(Vector4), "Vector4");
            dictionary.Add(typeof(Quaternion), "Quaternion");
            dictionary.Add(typeof(Color), "Color");
            dictionary.Add(typeof(Color32), "Color32");
            dictionary.Add(typeof(Rect), "Rect");
            dictionary.Add(typeof(string), "String");
            dictionary.Add(typeof(int), "Int32");
            _TypeToShortString = dictionary;
        }

        public TweenParms AutoKill(bool p_active)
        {
            base.autoKillOnComplete = p_active;
            return this;
        }

        public TweenParms Delay(float p_delay)
        {
            this.delay = p_delay;
            return this;
        }

        public TweenParms Ease(EaseType p_easeType)
        {
            return this.Ease(p_easeType, Holoville.HOTween.HOTween.defEaseOvershootOrAmplitude, Holoville.HOTween.HOTween.defEasePeriod);
        }

        public TweenParms Ease(AnimationCurve p_easeAnimationCurve)
        {
            this.easeType = EaseType.AnimationCurve;
            this.easeAnimCurve = p_easeAnimationCurve;
            return this;
        }

        public TweenParms Ease(EaseType p_easeType, float p_overshoot)
        {
            return this.Ease(p_easeType, p_overshoot, Holoville.HOTween.HOTween.defEasePeriod);
        }

        public TweenParms Ease(EaseType p_easeType, float p_amplitude, float p_period)
        {
            this.easeType = p_easeType;
            this.easeOvershootOrAmplitude = p_amplitude;
            this.easePeriod = p_period;
            return this;
        }

        public TweenParms Id(string p_id)
        {
            base.id = p_id;
            return this;
        }

        internal void InitializeObject(Tweener p_tweenObj, object p_target)
        {
            base.InitializeOwner(p_tweenObj);
            if (this.speedBased)
            {
                this.easeType = EaseType.Linear;
            }
            p_tweenObj._pixelPerfect = this.pixelPerfect;
            p_tweenObj._speedBased = this.speedBased;
            p_tweenObj._easeType = this.easeType;
            p_tweenObj._easeAnimationCurve = this.easeAnimCurve;
            p_tweenObj._easeOvershootOrAmplitude = this.easeOvershootOrAmplitude;
            p_tweenObj._easePeriod = this.easePeriod;
            p_tweenObj._delay = p_tweenObj.delayCount = this.delay;
            p_tweenObj.isFrom = this.isFrom;
            p_tweenObj.onPluginOverwritten = this.onPluginOverwritten;
            p_tweenObj.onPluginOverwrittenWParms = this.onPluginOverwrittenWParms;
            p_tweenObj.onPluginOverwrittenParms = this.onPluginOverwrittenParms;
            p_tweenObj.plugins = new List<ABSTweenPlugin>();
            System.Type type = p_target.GetType();
            FieldInfo field = null;
            int count = this.propDatas.Count;
            for (int i = 0; i < count; i++)
            {
                ABSTweenPlugin plugin;
                HOTPropData data = this.propDatas[i];
                PropertyInfo property = type.GetProperty(data.propName);
                if (property == null)
                {
                    field = type.GetField(data.propName);
                    if (field == null)
                    {
                        TweenWarning.Log(string.Concat(new object[] { "\"", p_target, ".", data.propName, "\" is missing, static, or not public. The tween for this property will not be created." }));
                        continue;
                    }
                }
                ABSTweenPlugin endValOrPlugin = data.endValOrPlugin as ABSTweenPlugin;
                if (endValOrPlugin != null)
                {
                    plugin = endValOrPlugin;
                    if (plugin.ValidateTarget(p_target))
                    {
                        if (plugin.initialized)
                        {
                            plugin = plugin.CloneBasic();
                        }
                        goto Label_0613;
                    }
                    TweenWarning.Log(string.Concat(new object[] { Utils.SimpleClassName(plugin.GetType()), " : Invalid target (", p_target, "). The tween for this property will not be created." }));
                    continue;
                }
                plugin = null;
                string key = (property == null) ? (!_TypeToShortString.ContainsKey(field.FieldType) ? string.Empty : _TypeToShortString[field.FieldType]) : (!_TypeToShortString.ContainsKey(property.PropertyType) ? string.Empty : _TypeToShortString[property.PropertyType]);
                if (key != null)
                {
                    int num4;
                    if (<>f__switch$map3 == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(9);
                        dictionary.Add("Vector2", 0);
                        dictionary.Add("Vector3", 1);
                        dictionary.Add("Vector4", 2);
                        dictionary.Add("Quaternion", 3);
                        dictionary.Add("Color", 4);
                        dictionary.Add("Color32", 5);
                        dictionary.Add("Rect", 6);
                        dictionary.Add("String", 7);
                        dictionary.Add("Int32", 8);
                        <>f__switch$map3 = dictionary;
                    }
                    if (<>f__switch$map3.TryGetValue(key, out num4))
                    {
                        switch (num4)
                        {
                            case 0:
                                if (ValidateValue(data.endValOrPlugin, PlugVector2.validValueTypes))
                                {
                                    goto Label_0328;
                                }
                                goto Label_05D1;

                            case 1:
                                if (ValidateValue(data.endValOrPlugin, PlugVector3.validValueTypes))
                                {
                                    goto Label_0362;
                                }
                                goto Label_05D1;

                            case 2:
                                if (ValidateValue(data.endValOrPlugin, PlugVector4.validValueTypes))
                                {
                                    goto Label_039C;
                                }
                                goto Label_05D1;

                            case 3:
                                if (ValidateValue(data.endValOrPlugin, PlugQuaternion.validValueTypes))
                                {
                                    goto Label_03D6;
                                }
                                goto Label_05D1;

                            case 4:
                                if (ValidateValue(data.endValOrPlugin, PlugColor.validValueTypes))
                                {
                                    goto Label_0440;
                                }
                                goto Label_05D1;

                            case 5:
                                if (ValidateValue(data.endValOrPlugin, PlugColor32.validValueTypes))
                                {
                                    goto Label_047A;
                                }
                                goto Label_05D1;

                            case 6:
                                if (ValidateValue(data.endValOrPlugin, PlugRect.validValueTypes))
                                {
                                    goto Label_04B4;
                                }
                                goto Label_05D1;

                            case 7:
                                if (ValidateValue(data.endValOrPlugin, PlugString.validValueTypes))
                                {
                                    goto Label_04EE;
                                }
                                goto Label_05D1;

                            case 8:
                                if (ValidateValue(data.endValOrPlugin, PlugInt.validValueTypes))
                                {
                                    goto Label_0528;
                                }
                                goto Label_05D1;
                        }
                    }
                }
                goto Label_0548;
            Label_0328:
                plugin = new PlugVector2((Vector2) data.endValOrPlugin, data.isRelative);
                goto Label_05D1;
            Label_0362:
                plugin = new PlugVector3((Vector3) data.endValOrPlugin, data.isRelative);
                goto Label_05D1;
            Label_039C:
                plugin = new PlugVector4((Vector4) data.endValOrPlugin, data.isRelative);
                goto Label_05D1;
            Label_03D6:
                if (data.endValOrPlugin is Vector3)
                {
                    plugin = new PlugQuaternion((Vector3) data.endValOrPlugin, data.isRelative);
                }
                else
                {
                    plugin = new PlugQuaternion((Quaternion) data.endValOrPlugin, data.isRelative);
                }
                goto Label_05D1;
            Label_0440:
                plugin = new PlugColor((Color) data.endValOrPlugin, data.isRelative);
                goto Label_05D1;
            Label_047A:
                plugin = new PlugColor32((Color32) data.endValOrPlugin, data.isRelative);
                goto Label_05D1;
            Label_04B4:
                plugin = new PlugRect((Rect) data.endValOrPlugin, data.isRelative);
                goto Label_05D1;
            Label_04EE:
                plugin = new PlugString(data.endValOrPlugin.ToString(), data.isRelative);
                goto Label_05D1;
            Label_0528:
                plugin = new PlugInt((float) ((int) data.endValOrPlugin), data.isRelative);
                goto Label_05D1;
            Label_0548:
                try
                {
                    plugin = new PlugFloat(Convert.ToSingle(data.endValOrPlugin), data.isRelative);
                }
                catch (Exception)
                {
                    TweenWarning.Log(string.Concat(new object[] { "No valid plugin for animating \"", p_target, ".", data.propName, "\" (of type ", (property == null) ? field.FieldType : property.PropertyType, "). The tween for this property will not be created." }));
                    continue;
                }
            Label_05D1:
                if (plugin == null)
                {
                    TweenWarning.Log(string.Concat(new object[] { "The end value set for \"", p_target, ".", data.propName, "\" tween is invalid. The tween for this property will not be created." }));
                    continue;
                }
            Label_0613:
                plugin.Init(p_tweenObj, data.propName, this.easeType, type, property, field);
                p_tweenObj.plugins.Add(plugin);
            }
        }

        public TweenParms IntId(int p_intId)
        {
            base.intId = p_intId;
            return this;
        }

        internal TweenParms IsFrom()
        {
            this.isFrom = true;
            return this;
        }

        public TweenParms KeepDisabled(Behaviour p_target)
        {
            if (p_target == null)
            {
                base.manageBehaviours = false;
                return this;
            }
            Behaviour[] behaviourArray1 = new Behaviour[] { p_target };
            return this.KeepEnabled(behaviourArray1, false);
        }

        public TweenParms KeepDisabled(GameObject p_target)
        {
            if (p_target == null)
            {
                base.manageGameObjects = false;
                return this;
            }
            GameObject[] objArray1 = new GameObject[] { p_target };
            return this.KeepEnabled(objArray1, false);
        }

        public TweenParms KeepDisabled(Behaviour[] p_targets)
        {
            return this.KeepEnabled(p_targets, false);
        }

        public TweenParms KeepDisabled(GameObject[] p_targets)
        {
            return this.KeepEnabled(p_targets, false);
        }

        public TweenParms KeepEnabled(Behaviour p_target)
        {
            if (p_target == null)
            {
                base.manageBehaviours = false;
                return this;
            }
            Behaviour[] behaviourArray1 = new Behaviour[] { p_target };
            return this.KeepEnabled(behaviourArray1, true);
        }

        public TweenParms KeepEnabled(GameObject p_target)
        {
            if (p_target == null)
            {
                base.manageGameObjects = false;
                return this;
            }
            GameObject[] objArray1 = new GameObject[] { p_target };
            return this.KeepEnabled(objArray1, true);
        }

        public TweenParms KeepEnabled(Behaviour[] p_targets)
        {
            return this.KeepEnabled(p_targets, true);
        }

        public TweenParms KeepEnabled(GameObject[] p_targets)
        {
            return this.KeepEnabled(p_targets, true);
        }

        private TweenParms KeepEnabled(Behaviour[] p_targets, bool p_enabled)
        {
            base.manageBehaviours = true;
            if (p_enabled)
            {
                base.managedBehavioursOn = p_targets;
            }
            else
            {
                base.managedBehavioursOff = p_targets;
            }
            return this;
        }

        private TweenParms KeepEnabled(GameObject[] p_targets, bool p_enabled)
        {
            base.manageGameObjects = true;
            if (p_enabled)
            {
                base.managedGameObjectsOn = p_targets;
            }
            else
            {
                base.managedGameObjectsOff = p_targets;
            }
            return this;
        }

        public TweenParms Loops(int p_loops)
        {
            return this.Loops(p_loops, Holoville.HOTween.HOTween.defLoopType);
        }

        public TweenParms Loops(int p_loops, LoopType p_loopType)
        {
            base.loops = p_loops;
            base.loopType = p_loopType;
            return this;
        }

        public TweenParms NewProp(string p_propName, ABSTweenPlugin p_plugin)
        {
            return this.NewProp(p_propName, p_plugin, false);
        }

        public TweenParms NewProp(string p_propName, object p_endVal)
        {
            return this.NewProp(p_propName, p_endVal, false);
        }

        public TweenParms NewProp(string p_propName, object p_endVal, bool p_isRelative)
        {
            this.propDatas = null;
            return this.Prop(p_propName, p_endVal, p_isRelative);
        }

        public TweenParms OnComplete(TweenDelegate.TweenCallback p_function)
        {
            base.onComplete = p_function;
            return this;
        }

        public TweenParms OnComplete(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onCompleteWParms = p_function;
            base.onCompleteParms = p_funcParms;
            return this;
        }

        public TweenParms OnComplete(GameObject p_sendMessageTarget, string p_methodName, object p_value = null, SendMessageOptions p_options = 0)
        {
            base.onCompleteWParms = new TweenDelegate.TweenCallbackWParms(Holoville.HOTween.HOTween.DoSendMessage);
            base.onCompleteParms = new object[] { p_sendMessageTarget, p_methodName, p_value, p_options };
            return this;
        }

        public TweenParms OnPause(TweenDelegate.TweenCallback p_function)
        {
            base.onPause = p_function;
            return this;
        }

        public TweenParms OnPause(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onPauseWParms = p_function;
            base.onPauseParms = p_funcParms;
            return this;
        }

        public TweenParms OnPlay(TweenDelegate.TweenCallback p_function)
        {
            base.onPlay = p_function;
            return this;
        }

        public TweenParms OnPlay(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onPlayWParms = p_function;
            base.onPlayParms = p_funcParms;
            return this;
        }

        public TweenParms OnPluginOverwritten(TweenDelegate.TweenCallback p_function)
        {
            this.onPluginOverwritten = p_function;
            return this;
        }

        public TweenParms OnPluginOverwritten(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            this.onPluginOverwrittenWParms = p_function;
            this.onPluginOverwrittenParms = p_funcParms;
            return this;
        }

        public TweenParms OnPluginUpdated(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onPluginUpdatedWParms = p_function;
            base.onPluginUpdatedParms = p_funcParms;
            return this;
        }

        public TweenParms OnRewinded(TweenDelegate.TweenCallback p_function)
        {
            base.onRewinded = p_function;
            return this;
        }

        public TweenParms OnRewinded(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onRewindedWParms = p_function;
            base.onRewindedParms = p_funcParms;
            return this;
        }

        public TweenParms OnStart(TweenDelegate.TweenCallback p_function)
        {
            base.onStart = p_function;
            return this;
        }

        public TweenParms OnStart(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onStartWParms = p_function;
            base.onStartParms = p_funcParms;
            return this;
        }

        public TweenParms OnStepComplete(TweenDelegate.TweenCallback p_function)
        {
            base.onStepComplete = p_function;
            return this;
        }

        public TweenParms OnStepComplete(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onStepCompleteWParms = p_function;
            base.onStepCompleteParms = p_funcParms;
            return this;
        }

        public TweenParms OnStepComplete(GameObject p_sendMessageTarget, string p_methodName, object p_value = null, SendMessageOptions p_options = 0)
        {
            base.onStepCompleteWParms = new TweenDelegate.TweenCallbackWParms(Holoville.HOTween.HOTween.DoSendMessage);
            base.onStepCompleteParms = new object[] { p_sendMessageTarget, p_methodName, p_value, p_options };
            return this;
        }

        public TweenParms OnUpdate(TweenDelegate.TweenCallback p_function)
        {
            base.onUpdate = p_function;
            return this;
        }

        public TweenParms OnUpdate(TweenDelegate.TweenCallbackWParms p_function, params object[] p_funcParms)
        {
            base.onUpdateWParms = p_function;
            base.onUpdateParms = p_funcParms;
            return this;
        }

        public TweenParms Pause()
        {
            return this.Pause(true);
        }

        public TweenParms Pause(bool p_pause)
        {
            base.isPaused = p_pause;
            return this;
        }

        public TweenParms PixelPerfect()
        {
            this.pixelPerfect = true;
            return this;
        }

        public TweenParms Prop(string p_propName, ABSTweenPlugin p_plugin)
        {
            return this.Prop(p_propName, p_plugin, false);
        }

        public TweenParms Prop(string p_propName, object p_endVal)
        {
            return this.Prop(p_propName, p_endVal, false);
        }

        public TweenParms Prop(string p_propName, object p_endVal, bool p_isRelative)
        {
            if (this.propDatas == null)
            {
                this.propDatas = new List<HOTPropData>();
            }
            this.propDatas.Add(new HOTPropData(p_propName, p_endVal, p_isRelative));
            return this;
        }

        public TweenParms SpeedBased()
        {
            return this.SpeedBased(true);
        }

        public TweenParms SpeedBased(bool p_speedBased)
        {
            this.speedBased = p_speedBased;
            return this;
        }

        public TweenParms TimeScale(float p_timeScale)
        {
            base.timeScale = p_timeScale;
            return this;
        }

        public TweenParms UpdateType(Holoville.HOTween.UpdateType p_updateType)
        {
            base.updateType = p_updateType;
            return this;
        }

        private static bool ValidateValue(object p_val, System.Type[] p_validVals)
        {
            return (Array.IndexOf<System.Type>(p_validVals, p_val.GetType()) != -1);
        }

        public bool hasProps
        {
            get
            {
                return (this.propDatas != null);
            }
        }

        private class HOTPropData
        {
            public readonly object endValOrPlugin;
            public readonly bool isRelative;
            public readonly string propName;

            public HOTPropData(string p_propName, object p_endValOrPlugin, bool p_isRelative)
            {
                this.propName = p_propName;
                this.endValOrPlugin = p_endValOrPlugin;
                this.isRelative = p_isRelative;
            }
        }
    }
}

