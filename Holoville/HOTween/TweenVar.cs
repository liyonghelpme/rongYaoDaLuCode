﻿namespace Holoville.HOTween
{
    using Holoville.HOTween.Core;
    using System;

    public class TweenVar
    {
        private EaseType _easeType;
        private float _elapsed;
        private float _endVal;
        private float _startVal;
        private float _value;
        private float changeVal;
        public float duration;
        private TweenDelegate.EaseFunc ease;

        public TweenVar(float p_startVal, float p_endVal, float p_duration) : this(p_startVal, p_endVal, p_duration, EaseType.Linear)
        {
        }

        public TweenVar(float p_startVal, float p_endVal, float p_duration, EaseType p_easeType)
        {
            this.startVal = this._value = p_startVal;
            this.endVal = p_endVal;
            this.duration = p_duration;
            this.easeType = p_easeType;
        }

        private void SetChangeVal()
        {
            this.changeVal = this.endVal - this.startVal;
        }

        public float Update(float p_elapsed)
        {
            return this.Update(p_elapsed, false);
        }

        public float Update(float p_elapsed, bool p_relative)
        {
            this._elapsed = !p_relative ? p_elapsed : (this._elapsed + p_elapsed);
            if (this._elapsed > this.duration)
            {
                this._elapsed = this.duration;
            }
            else if (this._elapsed < 0f)
            {
                this._elapsed = 0f;
            }
            this._value = this.ease(this._elapsed, this._startVal, this.changeVal, this.duration, Holoville.HOTween.HOTween.defEaseOvershootOrAmplitude, Holoville.HOTween.HOTween.defEasePeriod);
            return this._value;
        }

        public EaseType easeType
        {
            get
            {
                return this._easeType;
            }
            set
            {
                this._easeType = value;
                this.ease = EaseInfo.GetEaseInfo(this._easeType).ease;
            }
        }

        public float elapsed
        {
            get
            {
                return this._elapsed;
            }
        }

        public float endVal
        {
            get
            {
                return this._endVal;
            }
            set
            {
                this._endVal = value;
                this.SetChangeVal();
            }
        }

        public float startVal
        {
            get
            {
                return this._startVal;
            }
            set
            {
                this._startVal = value;
                this.SetChangeVal();
            }
        }

        public float value
        {
            get
            {
                return this._value;
            }
        }
    }
}

