﻿namespace Holoville.HOTween
{
    using System;

    public enum UpdateType
    {
        Update,
        LateUpdate,
        FixedUpdate,
        TimeScaleIndependentUpdate
    }
}

