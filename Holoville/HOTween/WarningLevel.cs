﻿namespace Holoville.HOTween
{
    using System;

    public enum WarningLevel
    {
        None,
        Important,
        Verbose
    }
}

