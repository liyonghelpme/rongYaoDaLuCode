﻿using com.liyong;
using System;
using UnityEngine;

public class KeyboardHandler : MonoBehaviour
{
    private CommandHandler cmd;

    private void Start()
    {
        this.cmd = base.GetComponent<CommandHandler>();
    }

    private void Update()
    {
        float axisRaw = Input.GetAxisRaw("Vertical");
        float num2 = Input.GetAxisRaw("Horizontal");
        this.cmd.AddCommand(string.Format("move {0} {1}", num2, axisRaw), false, 1f);
    }
}

