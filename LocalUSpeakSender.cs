﻿using MoPhoGames.USpeak.Interface;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

public class LocalUSpeakSender : MonoBehaviour, ISpeechDataHandler, IUSpeakTalkController
{
    private float jitter;
    private float packetLoss;
    private float ping;
    private bool recording;

    [DebuggerHidden]
    private IEnumerator fakeSendPacket(byte[] data)
    {
        return new <fakeSendPacket>c__Iterator20 { data = data, <$>data = data, <>f__this = this };
    }

    private void OnGUI()
    {
        GUILayout.Label("Ping - " + Mathf.Round(this.ping) + "ms", new GUILayoutOption[0]);
        GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(200f) };
        this.ping = GUILayout.HorizontalSlider(this.ping, 0f, 100f, options);
        GUILayout.Label("Ping Jitter - " + Mathf.Round(this.jitter) + "ms", new GUILayoutOption[0]);
        GUILayoutOption[] optionArray2 = new GUILayoutOption[] { GUILayout.Width(200f) };
        this.jitter = GUILayout.HorizontalSlider(this.jitter, 0f, 100f, optionArray2);
        GUILayout.Label("Packet Loss - " + Mathf.Round(this.packetLoss) + "%", new GUILayoutOption[0]);
        GUILayoutOption[] optionArray3 = new GUILayoutOption[] { GUILayout.Width(200f) };
        this.packetLoss = GUILayout.HorizontalSlider(this.packetLoss, 0f, 10f, optionArray3);
        GUILayout.Label("Using Microphone: " + Microphone.devices[0], new GUILayoutOption[0]);
        GUILayout.Space(10f);
        if (this.recording)
        {
            if (GUILayout.Button("Stop Recording", new GUILayoutOption[0]))
            {
                this.recording = false;
            }
        }
        else if (GUILayout.Button("Start Recording", new GUILayoutOption[0]))
        {
            this.recording = true;
        }
    }

    public void OnInspectorGUI()
    {
    }

    public bool ShouldSend()
    {
        return this.recording;
    }

    public void USpeakInitializeSettings(int data)
    {
        USpeaker.Get(this).InitializeSettings(data);
    }

    public void USpeakOnSerializeAudio(byte[] data)
    {
        base.StartCoroutine(this.fakeSendPacket(data));
    }

    [CompilerGenerated]
    private sealed class <fakeSendPacket>c__Iterator20 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal byte[] <$>data;
        internal LocalUSpeakSender <>f__this;
        internal float <delay>__1;
        internal float <rand>__0;
        internal byte[] data;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.<rand>__0 = UnityEngine.Random.Range((float) 0f, (float) 100f);
                    if (this.<rand>__0 >= this.<>f__this.packetLoss)
                    {
                        this.<delay>__1 = UnityEngine.Random.Range(this.<>f__this.ping, this.<>f__this.ping + this.<>f__this.jitter) / 1000f;
                        this.$current = new WaitForSeconds(this.<delay>__1);
                        this.$PC = 1;
                        return true;
                    }
                    break;

                case 1:
                    USpeaker.Get(this.<>f__this).ReceiveAudio(this.data);
                    this.$PC = -1;
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }
}

