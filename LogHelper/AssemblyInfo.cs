﻿// Assembly LogHelper, Version 1.0.0.0

[assembly: System.Reflection.AssemblyCompany("Microsoft")]
[assembly: System.Reflection.AssemblyProduct("LogHelper")]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyTitle("LogHelper")]
[assembly: System.Reflection.AssemblyDescription("")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 Microsoft 2015")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Reflection.AssemblyFileVersion("1.0.0.0")]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.InteropServices.Guid("6e86eaaa-0985-455a-b94f-0c3dc321b6aa")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]

