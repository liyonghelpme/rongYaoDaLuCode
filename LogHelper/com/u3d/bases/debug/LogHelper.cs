﻿namespace com.u3d.bases.debug
{
    using System;
    using System.Diagnostics;
    using UnityEngine;

    public class LogHelper
    {
        private static LogWriter m_logWriter;

        [Conditional("WALKER_DEBUG")]
        public static void DEBUG(string logString)
        {
            Debug.Log("[W_LOG]: " + logString);
        }

        [Conditional("WALKER_DEBUG")]
        public static void DEBUG(bool precond, string logString)
        {
        }

        [Conditional("WALKER_DEBUG")]
        public static void ERROR(string logString)
        {
            Debug.LogError("[W_ERROR]: " + logString);
        }

        public static void ERROR(bool precond, string logString)
        {
        }

        public static void Init()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Application.RegisterLogCallback(new Application.LogCallback(LogHelper.ProcessExceptionReport));
                if (m_logWriter != null)
                {
                    m_logWriter.Release();
                }
                m_logWriter = new LogWriter();
            }
        }

        public static void Init(int RunMode)
        {
            if (RunMode == 0)
            {
                Init();
            }
        }

        private static void Log(string msg, LogType type)
        {
            string str = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + '\n' + msg;
            m_logWriter.WriteLog(str, type);
        }

        private static void ProcessExceptionReport(string message, string stackTrace, LogType type)
        {
            Log(string.Concat(new object[] { "[SYS_", type.ToString(), "]:", message, '\n', stackTrace }), type);
        }

        public static void Release()
        {
            if (m_logWriter != null)
            {
                m_logWriter.Release();
                m_logWriter = null;
            }
        }

        [Conditional("WALKER_DEBUG")]
        public static void WARN(string logString)
        {
            Debug.LogWarning("[W_WARN]: " + logString);
        }

        [Conditional("WALKER_DEBUG")]
        public static void WARN(bool precond, string logString)
        {
        }
    }
}

