﻿namespace com.u3d.bases.debug
{
    using System;
    using System.IO;
    using System.Threading;
    using UnityEngine;

    public class LogWriter
    {
        private FileStream m_fs;
        private static readonly object m_lock = new object();
        private string m_logFileName = "Log_{0}.txt";
        private string m_LogFilePath = (Application.persistentDataPath + "/log/");
        private WriteFunc m_logWrite;
        private StreamWriter m_sw;

        public LogWriter()
        {
            if (!Directory.Exists(this.m_LogFilePath))
            {
                Directory.CreateDirectory(this.m_LogFilePath);
            }
            this.m_LogFilePath = this.m_LogFilePath + string.Format(this.m_logFileName, DateTime.Today.ToString("yyyyMMdd"));
            try
            {
                this.m_logWrite = new WriteFunc(this.Write);
                this.m_fs = new FileStream(this.m_LogFilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                this.m_sw = new StreamWriter(this.m_fs);
            }
            catch (Exception exception)
            {
                Debug.LogError(exception.Message);
            }
        }

        public void Release()
        {
            lock (m_lock)
            {
                if (this.m_sw != null)
                {
                    this.m_sw.Close();
                    this.m_sw.Dispose();
                }
                if (this.m_fs != null)
                {
                    this.m_fs.Close();
                    this.m_fs.Dispose();
                }
            }
        }

        private void Write(string msg, LogType level)
        {
            object @lock = m_lock;
            Monitor.Enter(@lock);
            try
            {
                switch (level)
                {
                    case LogType.Error:
                        Debug.LogError(msg);
                        break;

                    case LogType.Warning:
                        Debug.LogWarning(msg);
                        break;

                    case LogType.Log:
                        Debug.Log(msg);
                        break;

                    case LogType.Exception:
                        Debug.LogError(msg);
                        break;
                }
                if ((this.m_sw != null) && (level != LogType.Warning))
                {
                    this.m_sw.WriteLine(msg);
                    this.m_sw.Flush();
                }
            }
            catch (Exception)
            {
                Debug.LogError(msg);
            }
            finally
            {
                Monitor.Exit(@lock);
            }
        }

        public void WriteLog(string msg, LogType level)
        {
            this.m_logWrite.BeginInvoke(msg, level, null, null);
        }
    }
}

