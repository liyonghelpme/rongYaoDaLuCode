﻿namespace com.u3d.bases.debug
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public delegate void WriteFunc(string s, LogType type);
}

