﻿using System;
using UnityLog;

public class LogMessage
{
    public Log.Category category;
    public int frame;
    public Log.Level level;
    public string message;
    public int occurances;
    public string stack;
    public float time;

    public LogMessage(float time, int frame, Log.Category category, Log.Level level, string message, string stack)
    {
        this.time = time;
        this.frame = frame;
        this.category = category;
        this.level = level;
        this.message = (message != null) ? message : string.Empty;
        this.stack = (stack != null) ? stack : string.Empty;
        this.occurances = 1;
    }

    private string SanitizeString(string target)
    {
        return target.Replace("\"", "'");
    }

    public string ToCSV()
    {
        object[] args = new object[] { this.time, this.frame, this.category.ToString(), this.level.ToString(), this.SanitizeString(this.message), this.SanitizeString(this.stack) };
        return string.Format("{0:F2},{1:F0},{2},{3},\"{4}\",\"{5}\"", args);
    }
}

