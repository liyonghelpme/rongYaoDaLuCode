﻿using System;
using UnityEngine;

public class LookAtCameraBillBoardY : MonoBehaviour
{
    private Transform mainCamTransform;
    private Transform mTrans;

    private void LateUpdate()
    {
        if (this.mTrans != null)
        {
            Vector3 vector = this.mainCamTransform.position - base.transform.position;
            vector.x = vector.z = 0f;
            this.mTrans.LookAt(this.mainCamTransform.position - vector);
        }
    }

    private void OnEnable()
    {
        this.Start();
    }

    private void Start()
    {
        this.mainCamTransform = Camera.main.transform;
        this.mTrans = base.transform;
    }
}

