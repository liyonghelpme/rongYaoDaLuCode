﻿using System;
using UnityEngine;

[ExecuteInEditMode]
public class LookAtCameraBillboard : MonoBehaviour
{
    private Transform mainCamTransform;
    private Transform mTrans;

    private void LateUpdate()
    {
        if (this.mTrans != null)
        {
            this.mTrans.LookAt(this.mainCamTransform);
        }
    }

    private void OnEnable()
    {
        this.Start();
    }

    private void Start()
    {
        this.mainCamTransform = Camera.main.transform;
        this.mTrans = base.transform;
    }
}

