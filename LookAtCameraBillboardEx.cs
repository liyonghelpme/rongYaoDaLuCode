﻿using System;
using UnityEngine;

public class LookAtCameraBillboardEx : MonoBehaviour
{
    private BillboardMode _billboardMode = BillboardMode.up;
    private Transform mainCamTransform;
    private Transform mTrans;

    private void LateUpdate()
    {
        if (this.mTrans != null)
        {
            this.mTrans.LookAt(this.mainCamTransform, Vector3.up);
            this.mTrans.Rotate(this.mTrans.right, 90f, Space.World);
        }
    }

    private void OnEnable()
    {
        this.Start();
    }

    public void SetBillboardMode(BillboardMode mode)
    {
        this._billboardMode = mode;
    }

    private void Start()
    {
        this.mTrans = base.transform;
        this.mainCamTransform = Camera.main.transform;
    }

    public enum BillboardMode
    {
        forward,
        up,
        right
    }
}

