﻿using com.game.module.core;
using com.game.vo;
using System;
using UnityEngine;

public class MainToolView
{
    private GameObject _go;
    private bool _isAcive;
    private UIWidgetContainer _spiritWgt;
    private Button gold_btn;
    private UILabel gold_label;
    private Button jewel_btn;
    private UILabel jewel_label;
    private Button spirit_btn;
    private UILabel spirit_label;

    public MainToolView(GameObject go)
    {
        this._go = go;
        this.Init();
    }

    private void addGold(GameObject go)
    {
        Singleton<GoldBoxView>.Instance.OpenView();
    }

    private void addSpirit(GameObject go)
    {
        Singleton<BuyVigourView>.Instance.Show();
    }

    private void Init()
    {
        this.gold_label = NGUITools.FindInChild<UILabel>(this._go, "gold/num");
        this.spirit_label = NGUITools.FindInChild<UILabel>(this._go, "spirit/num");
        this.jewel_label = NGUITools.FindInChild<UILabel>(this._go, "jewel/num");
        this.gold_btn = NGUITools.FindInChild<Button>(this._go, "gold/add").GetComponent<Button>();
        this.spirit_btn = NGUITools.FindInChild<Button>(this._go, "spirit/add").GetComponent<Button>();
        this.jewel_btn = NGUITools.FindInChild<Button>(this._go, "jewel/add").GetComponent<Button>();
        this._spiritWgt = NGUITools.FindInChild<UIWidgetContainer>(this._go, "spirit/background");
    }

    private void InitEvent()
    {
        Singleton<RoleMode>.Instance.dataUpdated += new DataUpateHandler(this.OnRoleEvent);
        this.spirit_btn.onClick = new UIWidgetContainer.VoidDelegate(this.addSpirit);
        this._spiritWgt.onPress = new UIWidgetContainer.BoolDelegate(this.OnPressSpirit);
        this.gold_btn.onClick = new UIWidgetContainer.VoidDelegate(this.addGold);
        this.jewel_btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickDiam);
    }

    private void OnClickDiam(GameObject go)
    {
        Singleton<VIPView>.Instance.OpenView();
    }

    private void OnPressSpirit(GameObject go, bool b)
    {
        Singleton<VigourTipView>.Instance.OpenView();
    }

    private void OnRoleEvent(object sender, int code)
    {
        if (code == 1)
        {
            this.updateSpirit();
        }
        else if (Singleton<RoleMode>.Instance.UPDATE_FORTUNE == code)
        {
            this.updateJewel();
        }
    }

    private void UnInitEvent()
    {
        Singleton<RoleMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnRoleEvent);
        this.spirit_btn.onClick = null;
        this._spiritWgt.onPress = null;
        this.gold_btn.onClick = null;
        this.jewel_btn.onClick = null;
    }

    private void updateJewel()
    {
        this.jewel_label.text = MeVo.instance.DiamondStr;
        this.gold_label.text = MeVo.instance.diam.ToString();
    }

    private void updateSpirit()
    {
        this.spirit_label.text = MeVo.instance.vigour + "/" + MeVo.instance.vigourFull;
    }

    private void UpdateView()
    {
        this.updateJewel();
        this.updateSpirit();
    }

    public bool isActive
    {
        get
        {
            return this._isAcive;
        }
        set
        {
            if (value != this._isAcive)
            {
                this._isAcive = value;
                if (this._isAcive)
                {
                    this.UpdateView();
                    this.InitEvent();
                }
                else
                {
                    this.UnInitEvent();
                }
            }
        }
    }
}

