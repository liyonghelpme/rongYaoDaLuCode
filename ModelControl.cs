﻿using System;
using UnityEngine;

public class ModelControl : MonoBehaviour
{
    private Animation animation;
    private Animator animator;
    private bool isDrag;
    private Transform mTrans;
    public float speed = 1f;
    private int state;
    public Transform target;

    private void OnClick()
    {
        this.state++;
        if (this.state >= 3)
        {
            this.state = 0;
        }
        this.animator.SetInteger("State", this.state);
    }

    private void OnDrag(Vector2 delta)
    {
        if (this.animation.IsPlaying("stand"))
        {
            UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
            if (this.target != null)
            {
                this.target.localRotation = Quaternion.Euler(0f, (-0.5f * delta.x) * this.speed, 0f) * this.target.localRotation;
            }
            else
            {
                this.mTrans.localRotation = Quaternion.Euler(0f, (-0.5f * delta.x) * this.speed, 0f) * this.mTrans.localRotation;
            }
        }
    }

    private void Start()
    {
        this.mTrans = base.transform;
        this.animator = base.GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this.state = UnityEngine.Random.Range(0, 2);
            this.animator.SetInteger("State", this.state);
        }
    }
}

