﻿using System;
using UnityEngine;

public class MoveToFinger : MonoBehaviour
{
    private Vector3 GetWorldPos(Vector2 screenPos)
    {
        Camera main = Camera.main;
        return main.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, Mathf.Abs((float) (base.transform.position.z - main.transform.position.z))));
    }

    private void OnDisable()
    {
        FingerGestures.OnFingerDown -= new FingerGestures.FingerDownEventHandler(this.OnFingerDown);
    }

    private void OnEnable()
    {
        FingerGestures.OnFingerDown += new FingerGestures.FingerDownEventHandler(this.OnFingerDown);
    }

    private void OnFingerDown(int fingerIndex, Vector2 fingerPos)
    {
        base.transform.position = this.GetWorldPos(fingerPos);
    }
}

