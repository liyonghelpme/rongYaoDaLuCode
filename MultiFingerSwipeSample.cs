﻿using System;
using UnityEngine;

public class MultiFingerSwipeSample : SampleBase
{
    public float baseEmitSpeed = 4f;
    public GameObject sphereObject;
    public SwipeGestureRecognizer swipeGesture;
    public float swipeVelocityEmitSpeedScale = 0.001f;

    private bool CheckSpawnParticles(Vector2 fingerPos, GameObject requiredObject)
    {
        GameObject obj2 = SampleBase.PickObject(fingerPos);
        if ((obj2 == null) || (obj2 != requiredObject))
        {
            return false;
        }
        this.SpawnParticles(obj2);
        return true;
    }

    protected override string GetHelpText()
    {
        return ("Swipe: press the yellow sphere with " + this.swipeGesture.RequiredFingerCount + " fingers and move them in one of the four cardinal directions, then release. The speed of the motion is taken into account.");
    }

    private void OnSwipe(SwipeGestureRecognizer source)
    {
        GameObject obj2 = SampleBase.PickObject(source.StartPosition);
        if (obj2 == this.sphereObject)
        {
            object[] objArray1 = new object[] { "Swiped ", source.Direction, " with velocity: ", source.Velocity };
            base.UI.StatusText = string.Concat(objArray1);
            SwipeParticlesEmitter componentInChildren = obj2.GetComponentInChildren<SwipeParticlesEmitter>();
            if (componentInChildren != null)
            {
                componentInChildren.Emit(source.Direction, source.Velocity);
            }
        }
    }

    private void SpawnParticles(GameObject obj)
    {
        ParticleEmitter componentInChildren = obj.GetComponentInChildren<ParticleEmitter>();
        if (componentInChildren != null)
        {
            componentInChildren.Emit();
        }
    }

    protected override void Start()
    {
        base.Start();
        this.swipeGesture.OnSwipe += new FGComponent.EventDelegate<SwipeGestureRecognizer>(this.OnSwipe);
    }
}

