﻿using com.game.utils;
using System;
using UnityEngine;

public class NGUIJoystick : MonoBehaviour
{
    public Transform background;
    private Vector3 backgroundPos;
    private Vector3 buttonCenter;
    private BoxCollider buttonCollider;
    private Vector3 center;
    private Vector3 initVectorCenter;
    private Vector3 initVectorSize;
    public Transform JoystickGroup;
    private Vector3 mLastPos = Vector3.zero;
    private Plane mPlane;
    public bool onDrag;
    public bool onPressed;
    public Vector2 position;
    private Vector3 pressVectorCenter;
    private Vector3 pressVectorSize;
    private const float radius = 0.2f;
    private readonly Vector3 scale = Vector3.one;

    private void Awake()
    {
        this.center = this.JoystickGroup.localPosition;
        this.buttonCenter = base.transform.localPosition;
        this.backgroundPos = this.background.localPosition;
        this.buttonCollider = base.gameObject.GetComponent<BoxCollider>();
        this.initVectorCenter = new Vector3(0f, 0f, 0f);
        this.pressVectorCenter = new Vector3(0f, 0f, 0f);
        this.initVectorSize = new Vector3(170f, 170f, 0f);
        this.pressVectorSize = new Vector3(77f, 77f, 0f);
        this.buttonCollider.center = this.initVectorCenter;
        this.buttonCollider.size = this.initVectorSize;
    }

    private void OnDrag(Vector2 delta)
    {
        if (base.enabled && base.gameObject.activeInHierarchy)
        {
            float num;
            this.onDrag = true;
            Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos);
            if (this.mPlane.Raycast(ray, out num))
            {
                Vector3 vector4;
                Vector3 point = ray.GetPoint(num);
                Vector3 direction = point - this.mLastPos;
                this.mLastPos = point;
                if ((Math.Abs(direction.x) > 0f) || (Math.Abs(direction.y) > 0f))
                {
                    direction = base.transform.InverseTransformDirection(direction);
                    direction.Scale(this.scale);
                    direction = base.transform.TransformDirection(direction);
                }
                Vector3 tgPosition = base.transform.position + direction;
                PointUtils.ScreenByPoint(base.transform, tgPosition, 1, out vector4);
                base.transform.position = vector4;
                if (Math.Abs(Vector3.Distance(base.transform.position, this.background.position)) > 0.2f)
                {
                    Vector3 vector5 = this.background.position + (base.transform.position - this.background.position);
                    PointUtils.ScreenByPoint(this.background, vector5, 2, out vector4);
                    this.background.position += (Vector3) ((vector4 - this.background.position) * 0.3f);
                }
                this.position = new Vector2(base.transform.localPosition.x - this.background.localPosition.x, base.transform.localPosition.y - this.background.localPosition.y);
                this.position = this.position.normalized;
            }
        }
    }

    private void OnPress(bool pressed)
    {
        if ((base.enabled && base.gameObject.activeInHierarchy) && pressed)
        {
            this.mLastPos = UICamera.lastHit.point;
            this.JoystickGroup.position = this.mLastPos;
            base.transform.position = this.mLastPos;
            this.background.position = this.mLastPos;
            this.buttonCollider.center = this.pressVectorCenter;
            this.buttonCollider.size = this.pressVectorSize;
            Transform transform = UICamera.currentCamera.transform;
            this.mPlane = new Plane((Vector3) (transform.rotation * Vector3.back), this.mLastPos);
            this.onPressed = true;
        }
        if (!pressed)
        {
            this.JoystickGroup.localPosition = this.center;
            base.transform.localPosition = this.buttonCenter;
            this.background.localPosition = this.backgroundPos;
            this.buttonCollider.center = this.initVectorCenter;
            this.buttonCollider.size = this.initVectorSize;
            this.onPressed = false;
            this.onDrag = false;
        }
    }

    public void Reset()
    {
        this.OnPress(false);
    }
}

