﻿using System;
using UnityEngine;

public class OneFingerGestureSample : SampleBase
{
    private int dragFingerIndex = -1;
    public GameObject dragObject;
    public GameObject longPressObject;
    public int requiredTapCount = 2;
    public GameObject swipeObject;
    public GameObject tapObject;

    private bool CheckSpawnParticles(Vector2 fingerPos, GameObject requiredObject)
    {
        GameObject obj2 = SampleBase.PickObject(fingerPos);
        if ((obj2 == null) || (obj2 != requiredObject))
        {
            return false;
        }
        this.SpawnParticles(obj2);
        return true;
    }

    private void FingerGestures_OnFingerDragBegin(int fingerIndex, Vector2 fingerPos, Vector2 startPos)
    {
        GameObject obj2 = SampleBase.PickObject(startPos);
        if (obj2 == this.dragObject)
        {
            base.UI.StatusText = "Started dragging with finger " + fingerIndex;
            this.dragFingerIndex = fingerIndex;
            this.SpawnParticles(obj2);
        }
    }

    private void FingerGestures_OnFingerDragEnd(int fingerIndex, Vector2 fingerPos)
    {
        if (fingerIndex == this.dragFingerIndex)
        {
            base.UI.StatusText = "Stopped dragging with finger " + fingerIndex;
            this.dragFingerIndex = -1;
            this.SpawnParticles(this.dragObject);
        }
    }

    private void FingerGestures_OnFingerDragMove(int fingerIndex, Vector2 fingerPos, Vector2 delta)
    {
        if (fingerIndex == this.dragFingerIndex)
        {
            this.dragObject.transform.position = SampleBase.GetWorldPos(fingerPos);
        }
    }

    private void FingerGestures_OnFingerLongPress(int fingerIndex, Vector2 fingerPos)
    {
        if (this.CheckSpawnParticles(fingerPos, this.longPressObject))
        {
            base.UI.StatusText = "Performed a long-press with finger " + fingerIndex;
        }
    }

    private void FingerGestures_OnFingerSwipe(int fingerIndex, Vector2 startPos, FingerGestures.SwipeDirection direction, float velocity)
    {
        GameObject obj2 = SampleBase.PickObject(startPos);
        if (obj2 == this.swipeObject)
        {
            object[] objArray1 = new object[] { "Swiped ", direction, " with finger ", fingerIndex };
            base.UI.StatusText = string.Concat(objArray1);
            SwipeParticlesEmitter componentInChildren = obj2.GetComponentInChildren<SwipeParticlesEmitter>();
            if (componentInChildren != null)
            {
                componentInChildren.Emit(direction, velocity);
            }
        }
    }

    private void FingerGestures_OnFingerTap(int fingerIndex, Vector2 fingerPos, int tapCount)
    {
        if (tapCount >= this.requiredTapCount)
        {
            Debug.Log("Tapcount: " + tapCount);
            if (this.CheckSpawnParticles(fingerPos, this.tapObject))
            {
                object[] objArray1 = new object[] { "Tapped ", tapCount, " times with finger ", fingerIndex };
                base.UI.StatusText = string.Concat(objArray1);
            }
        }
    }

    protected override string GetHelpText()
    {
        return ("This sample demonstrates some of the supported single-finger gestures:\r\n\r\n- Drag: press the red sphere and move your finger to drag it around  \r\n\r\n- LongPress: keep your finger pressed on the cyan sphere for at least " + FingerGestures.Defaults.Fingers[0].LongPress.Duration + " seconds\r\n\r\n- Tap: rapidly press & release the purple sphere \r\n\r\n- Swipe: press the yellow sphere and move your finger in one of the four cardinal directions, then release. The speed of the motion is taken into account.");
    }

    private void OnDisable()
    {
        FingerGestures.OnFingerLongPress -= new FingerGestures.FingerLongPressEventHandler(this.FingerGestures_OnFingerLongPress);
        FingerGestures.OnFingerTap -= new FingerGestures.FingerTapEventHandler(this.FingerGestures_OnFingerTap);
        FingerGestures.OnFingerSwipe -= new FingerGestures.FingerSwipeEventHandler(this.FingerGestures_OnFingerSwipe);
        FingerGestures.OnFingerDragBegin -= new FingerGestures.FingerDragBeginEventHandler(this.FingerGestures_OnFingerDragBegin);
        FingerGestures.OnFingerDragMove -= new FingerGestures.FingerDragMoveEventHandler(this.FingerGestures_OnFingerDragMove);
        FingerGestures.OnFingerDragEnd -= new FingerGestures.FingerDragEndEventHandler(this.FingerGestures_OnFingerDragEnd);
    }

    private void OnEnable()
    {
        Debug.Log("Registering finger gesture events from C# script");
        FingerGestures.OnFingerLongPress += new FingerGestures.FingerLongPressEventHandler(this.FingerGestures_OnFingerLongPress);
        FingerGestures.OnFingerTap += new FingerGestures.FingerTapEventHandler(this.FingerGestures_OnFingerTap);
        FingerGestures.OnFingerSwipe += new FingerGestures.FingerSwipeEventHandler(this.FingerGestures_OnFingerSwipe);
        FingerGestures.OnFingerDragBegin += new FingerGestures.FingerDragBeginEventHandler(this.FingerGestures_OnFingerDragBegin);
        FingerGestures.OnFingerDragMove += new FingerGestures.FingerDragMoveEventHandler(this.FingerGestures_OnFingerDragMove);
        FingerGestures.OnFingerDragEnd += new FingerGestures.FingerDragEndEventHandler(this.FingerGestures_OnFingerDragEnd);
    }

    private void SpawnParticles(GameObject obj)
    {
        ParticleEmitter componentInChildren = obj.GetComponentInChildren<ParticleEmitter>();
        if (componentInChildren != null)
        {
            componentInChildren.Emit();
        }
    }
}

