﻿using com.game.data;
using com.game.manager;
using com.game.Public.Message;
using System;
using UnityEngine;

public class OpenLevelLimitManager : MonoBehaviour
{
    public static bool checkLeveByGuideLimitID(int id, int playerLevel)
    {
        bool flag = false;
        SysGuideLimitVo dataById = BaseDataMgr.instance.GetDataById<SysGuideLimitVo>((uint) id);
        if ((dataById.ID == id) && (playerLevel < dataById.lvl))
        {
            flag = true;
            MessageManager.Show(LanguageManager.GetWord("OpenLevelLimitManager.levelLimit", string.Empty + dataById.lvl));
        }
        return flag;
    }
}

