﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PAchieve
    {
        public uint data;
        public uint level;
        public uint type;

        public void read(MemoryStream msdata)
        {
            this.type = proto_util.readUInt(msdata);
            this.data = proto_util.readUInt(msdata);
            this.level = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PAchieve> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PAchieve item = new PAchieve();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.type);
            proto_util.writeUInt(msdata, this.data);
            proto_util.writeUInt(msdata, this.level);
        }

        public static void writeLoop(MemoryStream msdata, List<PAchieve> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PAchieve achieve in p)
            {
                achieve.write(msdata);
            }
        }
    }
}

