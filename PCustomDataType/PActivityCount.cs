﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PActivityCount
    {
        public uint activityId;
        public ushort count;

        public void read(MemoryStream msdata)
        {
            this.activityId = proto_util.readUInt(msdata);
            this.count = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PActivityCount> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PActivityCount item = new PActivityCount();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.activityId);
            proto_util.writeUShort(msdata, this.count);
        }

        public static void writeLoop(MemoryStream msdata, List<PActivityCount> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PActivityCount count in p)
            {
                count.write(msdata);
            }
        }
    }
}

