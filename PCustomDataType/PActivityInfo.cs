﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PActivityInfo
    {
        public uint activityId;
        public uint count;
        public uint duration;
        public byte isOpen;
        public ushort level;
        public uint link;
        public string name = string.Empty;
        public string pictureId = string.Empty;
        public uint startTime;

        public void read(MemoryStream msdata)
        {
            this.activityId = proto_util.readUInt(msdata);
            this.name = proto_util.readString(msdata);
            this.level = proto_util.readUShort(msdata);
            this.count = proto_util.readUInt(msdata);
            this.pictureId = proto_util.readString(msdata);
            this.startTime = proto_util.readUInt(msdata);
            this.duration = proto_util.readUInt(msdata);
            this.link = proto_util.readUInt(msdata);
            this.isOpen = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PActivityInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PActivityInfo item = new PActivityInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.activityId);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUShort(msdata, this.level);
            proto_util.writeUInt(msdata, this.count);
            proto_util.writeString(msdata, this.pictureId);
            proto_util.writeUInt(msdata, this.startTime);
            proto_util.writeUInt(msdata, this.duration);
            proto_util.writeUInt(msdata, this.link);
            proto_util.writeUByte(msdata, this.isOpen);
        }

        public static void writeLoop(MemoryStream msdata, List<PActivityInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PActivityInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

