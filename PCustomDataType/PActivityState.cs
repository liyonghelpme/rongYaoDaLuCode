﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PActivityState
    {
        public uint activityId;
        public ushort state;

        public void read(MemoryStream msdata)
        {
            this.activityId = proto_util.readUInt(msdata);
            this.state = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PActivityState> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PActivityState item = new PActivityState();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.activityId);
            proto_util.writeUShort(msdata, this.state);
        }

        public static void writeLoop(MemoryStream msdata, List<PActivityState> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PActivityState state in p)
            {
                state.write(msdata);
            }
        }
    }
}

