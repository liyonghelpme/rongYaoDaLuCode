﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PArenaFightAttr
    {
        public PNewAttr attr = new PNewAttr();
        public List<PGeneralSkillInfo> skill = new List<PGeneralSkillInfo>();

        public void read(MemoryStream msdata)
        {
            this.attr.read(msdata);
            PGeneralSkillInfo.readLoop(msdata, this.skill);
        }

        public static void readLoop(MemoryStream msdata, List<PArenaFightAttr> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PArenaFightAttr item = new PArenaFightAttr();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            this.attr.write(msdata);
            PGeneralSkillInfo.writeLoop(msdata, this.skill);
        }

        public static void writeLoop(MemoryStream msdata, List<PArenaFightAttr> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PArenaFightAttr attr in p)
            {
                attr.write(msdata);
            }
        }
    }
}

