﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PArenaFightInfo
    {
        public byte canChallenge;
        public uint fightPoint;
        public List<PGeneralIconInfo> generalList = new List<PGeneralIconInfo>();
        public ulong id;
        public string name = string.Empty;
        public uint pos;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.pos = proto_util.readUInt(msdata);
            this.fightPoint = proto_util.readUInt(msdata);
            this.name = proto_util.readString(msdata);
            this.canChallenge = proto_util.readUByte(msdata);
            PGeneralIconInfo.readLoop(msdata, this.generalList);
        }

        public static void readLoop(MemoryStream msdata, List<PArenaFightInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PArenaFightInfo item = new PArenaFightInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.pos);
            proto_util.writeUInt(msdata, this.fightPoint);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.canChallenge);
            PGeneralIconInfo.writeLoop(msdata, this.generalList);
        }

        public static void writeLoop(MemoryStream msdata, List<PArenaFightInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PArenaFightInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

