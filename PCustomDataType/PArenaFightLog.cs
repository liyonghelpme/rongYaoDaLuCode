﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PArenaFightLog
    {
        public string name = string.Empty;
        public uint pos;
        public ulong roleId;
        public uint time;
        public byte type;

        public void read(MemoryStream msdata)
        {
            this.time = proto_util.readUInt(msdata);
            this.type = proto_util.readUByte(msdata);
            this.roleId = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.pos = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PArenaFightLog> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PArenaFightLog item = new PArenaFightLog();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.time);
            proto_util.writeUByte(msdata, this.type);
            proto_util.writeULong(msdata, this.roleId);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUInt(msdata, this.pos);
        }

        public static void writeLoop(MemoryStream msdata, List<PArenaFightLog> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PArenaFightLog log in p)
            {
                log.write(msdata);
            }
        }
    }
}

