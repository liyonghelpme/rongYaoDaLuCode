﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PBaseAttr
    {
        public uint agi;
        public uint attMMax;
        public uint attMMin;
        public uint attPMax;
        public uint attPMin;
        public uint crit;
        public uint critRatio;
        public uint defM;
        public uint defP;
        public uint dodge;
        public uint fightPoint;
        public uint flex;
        public uint hit;
        public uint hpCur;
        public uint hpFull;
        public uint hurtRe;
        public uint luck;
        public uint mpCur;
        public uint mpFull;
        public uint phy;
        public uint speed;
        public uint str;
        public uint wit;

        public void read(MemoryStream msdata)
        {
            this.str = proto_util.readUInt(msdata);
            this.agi = proto_util.readUInt(msdata);
            this.phy = proto_util.readUInt(msdata);
            this.wit = proto_util.readUInt(msdata);
            this.hpCur = proto_util.readUInt(msdata);
            this.hpFull = proto_util.readUInt(msdata);
            this.mpCur = proto_util.readUInt(msdata);
            this.mpFull = proto_util.readUInt(msdata);
            this.attPMin = proto_util.readUInt(msdata);
            this.attPMax = proto_util.readUInt(msdata);
            this.attMMin = proto_util.readUInt(msdata);
            this.attMMax = proto_util.readUInt(msdata);
            this.defP = proto_util.readUInt(msdata);
            this.defM = proto_util.readUInt(msdata);
            this.hit = proto_util.readUInt(msdata);
            this.dodge = proto_util.readUInt(msdata);
            this.crit = proto_util.readUInt(msdata);
            this.critRatio = proto_util.readUInt(msdata);
            this.flex = proto_util.readUInt(msdata);
            this.hurtRe = proto_util.readUInt(msdata);
            this.speed = proto_util.readUInt(msdata);
            this.luck = proto_util.readUInt(msdata);
            this.fightPoint = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PBaseAttr> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PBaseAttr item = new PBaseAttr();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.str);
            proto_util.writeUInt(msdata, this.agi);
            proto_util.writeUInt(msdata, this.phy);
            proto_util.writeUInt(msdata, this.wit);
            proto_util.writeUInt(msdata, this.hpCur);
            proto_util.writeUInt(msdata, this.hpFull);
            proto_util.writeUInt(msdata, this.mpCur);
            proto_util.writeUInt(msdata, this.mpFull);
            proto_util.writeUInt(msdata, this.attPMin);
            proto_util.writeUInt(msdata, this.attPMax);
            proto_util.writeUInt(msdata, this.attMMin);
            proto_util.writeUInt(msdata, this.attMMax);
            proto_util.writeUInt(msdata, this.defP);
            proto_util.writeUInt(msdata, this.defM);
            proto_util.writeUInt(msdata, this.hit);
            proto_util.writeUInt(msdata, this.dodge);
            proto_util.writeUInt(msdata, this.crit);
            proto_util.writeUInt(msdata, this.critRatio);
            proto_util.writeUInt(msdata, this.flex);
            proto_util.writeUInt(msdata, this.hurtRe);
            proto_util.writeUInt(msdata, this.speed);
            proto_util.writeUInt(msdata, this.luck);
            proto_util.writeUInt(msdata, this.fightPoint);
        }

        public static void writeLoop(MemoryStream msdata, List<PBaseAttr> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PBaseAttr attr in p)
            {
                attr.write(msdata);
            }
        }
    }
}

