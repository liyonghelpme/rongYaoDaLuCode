﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PBattleObject
    {
        public int dir;
        public ulong id;
        public uint templateId;
        public int x;
        public int y;
        public int z;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.templateId = proto_util.readUInt(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.dir = proto_util.readInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PBattleObject> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PBattleObject item = new PBattleObject();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.templateId);
            proto_util.writeInt(msdata, this.x);
            proto_util.writeInt(msdata, this.y);
            proto_util.writeInt(msdata, this.z);
            proto_util.writeInt(msdata, this.dir);
        }

        public static void writeLoop(MemoryStream msdata, List<PBattleObject> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PBattleObject obj2 in p)
            {
                obj2.write(msdata);
            }
        }
    }
}

