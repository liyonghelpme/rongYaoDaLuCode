﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PBuff
    {
        public uint id;
        public byte isDebuff;
        public byte lvl;
        public byte type;
        public ushort val;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUInt(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.isDebuff = proto_util.readUByte(msdata);
            this.type = proto_util.readUByte(msdata);
            this.val = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PBuff> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PBuff item = new PBuff();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.id);
            proto_util.writeUByte(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.isDebuff);
            proto_util.writeUByte(msdata, this.type);
            proto_util.writeUShort(msdata, this.val);
        }

        public static void writeLoop(MemoryStream msdata, List<PBuff> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PBuff buff in p)
            {
                buff.write(msdata);
            }
        }
    }
}

