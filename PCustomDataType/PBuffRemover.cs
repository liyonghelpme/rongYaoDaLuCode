﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PBuffRemover
    {
        public uint buffId;
        public byte buffLvl;
        public int extra;
        public uint uinqueKey;

        public void read(MemoryStream msdata)
        {
            this.buffId = proto_util.readUInt(msdata);
            this.buffLvl = proto_util.readUByte(msdata);
            this.uinqueKey = proto_util.readUInt(msdata);
            this.extra = proto_util.readInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PBuffRemover> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PBuffRemover item = new PBuffRemover();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.buffId);
            proto_util.writeUByte(msdata, this.buffLvl);
            proto_util.writeUInt(msdata, this.uinqueKey);
            proto_util.writeInt(msdata, this.extra);
        }

        public static void writeLoop(MemoryStream msdata, List<PBuffRemover> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PBuffRemover remover in p)
            {
                remover.write(msdata);
            }
        }
    }
}

