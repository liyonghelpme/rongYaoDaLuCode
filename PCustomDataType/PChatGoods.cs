﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PChatGoods
    {
        public ulong id;
        public byte repos;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.repos = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PChatGoods> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PChatGoods item = new PChatGoods();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUByte(msdata, this.repos);
        }

        public static void writeLoop(MemoryStream msdata, List<PChatGoods> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PChatGoods goods in p)
            {
                goods.write(msdata);
            }
        }
    }
}

