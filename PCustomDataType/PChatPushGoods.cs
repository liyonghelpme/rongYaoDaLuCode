﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PChatPushGoods
    {
        public ulong id;
        public uint templateId;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.templateId = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PChatPushGoods> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PChatPushGoods item = new PChatPushGoods();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.templateId);
        }

        public static void writeLoop(MemoryStream msdata, List<PChatPushGoods> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PChatPushGoods goods in p)
            {
                goods.write(msdata);
            }
        }
    }
}

