﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PCoordinator
    {
        public ulong id;
        public ushort lv;
        public int rotateY;
        public int type;
        public int x;
        public int y;
        public int z;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.lv = proto_util.readUShort(msdata);
            this.type = proto_util.readInt(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.rotateY = proto_util.readInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PCoordinator> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PCoordinator item = new PCoordinator();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUShort(msdata, this.lv);
            proto_util.writeInt(msdata, this.type);
            proto_util.writeInt(msdata, this.x);
            proto_util.writeInt(msdata, this.y);
            proto_util.writeInt(msdata, this.z);
            proto_util.writeInt(msdata, this.rotateY);
        }

        public static void writeLoop(MemoryStream msdata, List<PCoordinator> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PCoordinator coordinator in p)
            {
                coordinator.write(msdata);
            }
        }
    }
}

