﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDamage
    {
        public List<PDamageBuff> buffList = new List<PDamageBuff>();
        public uint dmg;
        public int dmgForShow;
        public byte dmgType;
        public uint hp;
        public ulong id;
        public byte stateType;
        public byte type;
        public int x;
        public int y;
        public int z;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.dmgForShow = proto_util.readInt(msdata);
            this.dmg = proto_util.readUInt(msdata);
            this.hp = proto_util.readUInt(msdata);
            this.dmgType = proto_util.readUByte(msdata);
            this.stateType = proto_util.readUByte(msdata);
            PDamageBuff.readLoop(msdata, this.buffList);
        }

        public static void readLoop(MemoryStream msdata, List<PDamage> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDamage item = new PDamage();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUByte(msdata, this.type);
            proto_util.writeInt(msdata, this.x);
            proto_util.writeInt(msdata, this.y);
            proto_util.writeInt(msdata, this.z);
            proto_util.writeInt(msdata, this.dmgForShow);
            proto_util.writeUInt(msdata, this.dmg);
            proto_util.writeUInt(msdata, this.hp);
            proto_util.writeUByte(msdata, this.dmgType);
            proto_util.writeUByte(msdata, this.stateType);
            PDamageBuff.writeLoop(msdata, this.buffList);
        }

        public static void writeLoop(MemoryStream msdata, List<PDamage> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDamage damage in p)
            {
                damage.write(msdata);
            }
        }
    }
}

