﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDamageBuff
    {
        public ulong attackerId;
        public byte attackerType;
        public uint buffId;
        public int buffOwnVal1;
        public int buffOwnVal2;
        public byte lvl;
        public int val1;
        public int val2;

        public void read(MemoryStream msdata)
        {
            this.buffId = proto_util.readUInt(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.attackerId = proto_util.readULong(msdata);
            this.attackerType = proto_util.readUByte(msdata);
            this.val1 = proto_util.readInt(msdata);
            this.val2 = proto_util.readInt(msdata);
            this.buffOwnVal1 = proto_util.readInt(msdata);
            this.buffOwnVal2 = proto_util.readInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PDamageBuff> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDamageBuff item = new PDamageBuff();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.buffId);
            proto_util.writeUByte(msdata, this.lvl);
            proto_util.writeULong(msdata, this.attackerId);
            proto_util.writeUByte(msdata, this.attackerType);
            proto_util.writeInt(msdata, this.val1);
            proto_util.writeInt(msdata, this.val2);
            proto_util.writeInt(msdata, this.buffOwnVal1);
            proto_util.writeInt(msdata, this.buffOwnVal2);
        }

        public static void writeLoop(MemoryStream msdata, List<PDamageBuff> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDamageBuff buff in p)
            {
                buff.write(msdata);
            }
        }
    }
}

