﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDiamGoods
    {
        public ulong id;
        public uint remain;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.remain = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PDiamGoods> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDiamGoods item = new PDiamGoods();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.remain);
        }

        public static void writeLoop(MemoryStream msdata, List<PDiamGoods> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDiamGoods goods in p)
            {
                goods.write(msdata);
            }
        }
    }
}

