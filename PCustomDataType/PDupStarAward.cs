﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDupStarAward
    {
        public uint id;
        public byte state;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUInt(msdata);
            this.state = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PDupStarAward> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDupStarAward item = new PDupStarAward();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.id);
            proto_util.writeUByte(msdata, this.state);
        }

        public static void writeLoop(MemoryStream msdata, List<PDupStarAward> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDupStarAward award in p)
            {
                award.write(msdata);
            }
        }
    }
}

