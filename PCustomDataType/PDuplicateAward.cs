﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDuplicateAward
    {
        public List<PDuplicateDrop> dropList = new List<PDuplicateDrop>();
        public ulong exp;
        public ulong generalExp;
        public uint gold;
        public byte lvl;

        public void read(MemoryStream msdata)
        {
            this.exp = proto_util.readULong(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.generalExp = proto_util.readULong(msdata);
            this.gold = proto_util.readUInt(msdata);
            PDuplicateDrop.readLoop(msdata, this.dropList);
        }

        public static void readLoop(MemoryStream msdata, List<PDuplicateAward> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDuplicateAward item = new PDuplicateAward();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.exp);
            proto_util.writeUByte(msdata, this.lvl);
            proto_util.writeULong(msdata, this.generalExp);
            proto_util.writeUInt(msdata, this.gold);
            PDuplicateDrop.writeLoop(msdata, this.dropList);
        }

        public static void writeLoop(MemoryStream msdata, List<PDuplicateAward> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDuplicateAward award in p)
            {
                award.write(msdata);
            }
        }
    }
}

