﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDuplicateDrop
    {
        public ulong id;
        public byte num;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.num = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PDuplicateDrop> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDuplicateDrop item = new PDuplicateDrop();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUByte(msdata, this.num);
        }

        public static void writeLoop(MemoryStream msdata, List<PDuplicateDrop> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDuplicateDrop drop in p)
            {
                drop.write(msdata);
            }
        }
    }
}

