﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDuplicateInfo
    {
        public ushort buyCount;
        public uint count;
        public uint dupId;
        public byte grade;
        public uint passTime;

        public void read(MemoryStream msdata)
        {
            this.dupId = proto_util.readUInt(msdata);
            this.grade = proto_util.readUByte(msdata);
            this.passTime = proto_util.readUInt(msdata);
            this.count = proto_util.readUInt(msdata);
            this.buyCount = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PDuplicateInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDuplicateInfo item = new PDuplicateInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.dupId);
            proto_util.writeUByte(msdata, this.grade);
            proto_util.writeUInt(msdata, this.passTime);
            proto_util.writeUInt(msdata, this.count);
            proto_util.writeUShort(msdata, this.buyCount);
        }

        public static void writeLoop(MemoryStream msdata, List<PDuplicateInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDuplicateInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

