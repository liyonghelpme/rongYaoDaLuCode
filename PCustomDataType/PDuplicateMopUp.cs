﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PDuplicateMopUp
    {
        public List<PDuplicateDrop> dropList = new List<PDuplicateDrop>();
        public ulong exp;
        public uint gold;
        public uint id;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUInt(msdata);
            this.exp = proto_util.readULong(msdata);
            this.gold = proto_util.readUInt(msdata);
            PDuplicateDrop.readLoop(msdata, this.dropList);
        }

        public static void readLoop(MemoryStream msdata, List<PDuplicateMopUp> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PDuplicateMopUp item = new PDuplicateMopUp();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.id);
            proto_util.writeULong(msdata, this.exp);
            proto_util.writeUInt(msdata, this.gold);
            PDuplicateDrop.writeLoop(msdata, this.dropList);
        }

        public static void writeLoop(MemoryStream msdata, List<PDuplicateMopUp> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PDuplicateMopUp up in p)
            {
                up.write(msdata);
            }
        }
    }
}

