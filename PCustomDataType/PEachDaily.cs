﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PEachDaily
    {
        public ushort count;
        public uint id;
        public byte status;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUInt(msdata);
            this.status = proto_util.readUByte(msdata);
            this.count = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PEachDaily> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PEachDaily item = new PEachDaily();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.id);
            proto_util.writeUByte(msdata, this.status);
            proto_util.writeUShort(msdata, this.count);
        }

        public static void writeLoop(MemoryStream msdata, List<PEachDaily> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PEachDaily daily in p)
            {
                daily.write(msdata);
            }
        }
    }
}

