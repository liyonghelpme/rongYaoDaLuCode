﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PEquip
    {
        public List<PGemInHole> gemList = new List<PGemInHole>();
        public byte refine;
        public byte stren;
        public List<PSuitAttr> suitList = new List<PSuitAttr>();

        public void read(MemoryStream msdata)
        {
            this.stren = proto_util.readUByte(msdata);
            this.refine = proto_util.readUByte(msdata);
            PGemInHole.readLoop(msdata, this.gemList);
            PSuitAttr.readLoop(msdata, this.suitList);
        }

        public static void readLoop(MemoryStream msdata, List<PEquip> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PEquip item = new PEquip();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.stren);
            proto_util.writeUByte(msdata, this.refine);
            PGemInHole.writeLoop(msdata, this.gemList);
            PSuitAttr.writeLoop(msdata, this.suitList);
        }

        public static void writeLoop(MemoryStream msdata, List<PEquip> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PEquip equip in p)
            {
                equip.write(msdata);
            }
        }
    }
}

