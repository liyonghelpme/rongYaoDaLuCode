﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PGemInHole
    {
        public ushort energy;
        public ulong gemId;

        public void read(MemoryStream msdata)
        {
            this.gemId = proto_util.readULong(msdata);
            this.energy = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PGemInHole> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PGemInHole item = new PGemInHole();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.gemId);
            proto_util.writeUShort(msdata, this.energy);
        }

        public static void writeLoop(MemoryStream msdata, List<PGemInHole> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PGemInHole hole in p)
            {
                hole.write(msdata);
            }
        }
    }
}

