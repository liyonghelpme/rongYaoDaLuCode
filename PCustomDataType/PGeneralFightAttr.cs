﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PGeneralFightAttr
    {
        public PNewAttr attr = new PNewAttr();
        public byte place;
        public List<PGeneralSkillInfo> skill = new List<PGeneralSkillInfo>();

        public void read(MemoryStream msdata)
        {
            this.attr.read(msdata);
            this.place = proto_util.readUByte(msdata);
            PGeneralSkillInfo.readLoop(msdata, this.skill);
        }

        public static void readLoop(MemoryStream msdata, List<PGeneralFightAttr> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PGeneralFightAttr item = new PGeneralFightAttr();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            this.attr.write(msdata);
            proto_util.writeUByte(msdata, this.place);
            PGeneralSkillInfo.writeLoop(msdata, this.skill);
        }

        public static void writeLoop(MemoryStream msdata, List<PGeneralFightAttr> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PGeneralFightAttr attr in p)
            {
                attr.write(msdata);
            }
        }
    }
}

