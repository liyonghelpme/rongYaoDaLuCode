﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PGeneralIconInfo
    {
        public uint generalId;
        public ushort lvl;
        public byte star;

        public void read(MemoryStream msdata)
        {
            this.generalId = proto_util.readUInt(msdata);
            this.lvl = proto_util.readUShort(msdata);
            this.star = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PGeneralIconInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PGeneralIconInfo item = new PGeneralIconInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.generalId);
            proto_util.writeUShort(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.star);
        }

        public static void writeLoop(MemoryStream msdata, List<PGeneralIconInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PGeneralIconInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

