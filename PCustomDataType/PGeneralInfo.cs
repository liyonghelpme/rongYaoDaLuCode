﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PGeneralInfo
    {
        public ulong exp;
        public uint fightPoint;
        public uint generalId;
        public ulong id;
        public ushort lvl;
        public byte place;
        public byte quality;
        public List<PGeneralSkillInfo> skillList = new List<PGeneralSkillInfo>();
        public byte star;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.generalId = proto_util.readUInt(msdata);
            this.quality = proto_util.readUByte(msdata);
            this.place = proto_util.readUByte(msdata);
            this.exp = proto_util.readULong(msdata);
            this.lvl = proto_util.readUShort(msdata);
            this.star = proto_util.readUByte(msdata);
            this.fightPoint = proto_util.readUInt(msdata);
            PGeneralSkillInfo.readLoop(msdata, this.skillList);
        }

        public static void readLoop(MemoryStream msdata, List<PGeneralInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PGeneralInfo item = new PGeneralInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.generalId);
            proto_util.writeUByte(msdata, this.quality);
            proto_util.writeUByte(msdata, this.place);
            proto_util.writeULong(msdata, this.exp);
            proto_util.writeUShort(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.star);
            proto_util.writeUInt(msdata, this.fightPoint);
            PGeneralSkillInfo.writeLoop(msdata, this.skillList);
        }

        public static void writeLoop(MemoryStream msdata, List<PGeneralInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PGeneralInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

