﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PGeneralSkillInfo
    {
        public uint groupId;
        public ushort skillLvl;

        public void read(MemoryStream msdata)
        {
            this.groupId = proto_util.readUInt(msdata);
            this.skillLvl = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PGeneralSkillInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PGeneralSkillInfo item = new PGeneralSkillInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.groupId);
            proto_util.writeUShort(msdata, this.skillLvl);
        }

        public static void writeLoop(MemoryStream msdata, List<PGeneralSkillInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PGeneralSkillInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

