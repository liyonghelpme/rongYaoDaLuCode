﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PGoods
    {
        public ushort count;
        public ushort energy;
        public List<PEquip> equip = new List<PEquip>();
        public ulong expire;
        public uint goodsId;
        public ulong id;
        public byte pos;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.goodsId = proto_util.readUInt(msdata);
            this.count = proto_util.readUShort(msdata);
            this.pos = proto_util.readUByte(msdata);
            this.expire = proto_util.readULong(msdata);
            this.energy = proto_util.readUShort(msdata);
            PEquip.readLoop(msdata, this.equip);
        }

        public static void readLoop(MemoryStream msdata, List<PGoods> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PGoods item = new PGoods();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.goodsId);
            proto_util.writeUShort(msdata, this.count);
            proto_util.writeUByte(msdata, this.pos);
            proto_util.writeULong(msdata, this.expire);
            proto_util.writeUShort(msdata, this.energy);
            PEquip.writeLoop(msdata, this.equip);
        }

        public static void writeLoop(MemoryStream msdata, List<PGoods> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PGoods goods in p)
            {
                goods.write(msdata);
            }
        }
    }
}

