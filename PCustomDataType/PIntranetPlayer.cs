﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PIntranetPlayer
    {
        public ulong playerId;
        public string playerIp = string.Empty;

        public void read(MemoryStream msdata)
        {
            this.playerId = proto_util.readULong(msdata);
            this.playerIp = proto_util.readString(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PIntranetPlayer> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PIntranetPlayer item = new PIntranetPlayer();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.playerId);
            proto_util.writeString(msdata, this.playerIp);
        }

        public static void writeLoop(MemoryStream msdata, List<PIntranetPlayer> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PIntranetPlayer player in p)
            {
                player.write(msdata);
            }
        }
    }
}

