﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PItem
    {
        public byte key;
        public List<uint> value = new List<uint>();

        public void read(MemoryStream msdata)
        {
            this.key = proto_util.readUByte(msdata);
            proto_util.readLoopUInt(msdata, this.value);
        }

        public static void readLoop(MemoryStream msdata, List<PItem> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PItem item = new PItem();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.key);
            proto_util.writeLoopUInt(msdata, this.value);
        }

        public static void writeLoop(MemoryStream msdata, List<PItem> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PItem item in p)
            {
                item.write(msdata);
            }
        }
    }
}

