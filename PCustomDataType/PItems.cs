﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PItems
    {
        public ushort color;
        public List<PMakedItem> colorRes = new List<PMakedItem>();
        public uint count;
        public ulong expire;
        public ulong generalId;
        public List<PMakedItem> hole = new List<PMakedItem>();
        public ulong id;
        public byte pos;
        public ushort stren;
        public uint templateId;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.generalId = proto_util.readULong(msdata);
            this.templateId = proto_util.readUInt(msdata);
            this.count = proto_util.readUInt(msdata);
            this.pos = proto_util.readUByte(msdata);
            this.expire = proto_util.readULong(msdata);
            this.color = proto_util.readUShort(msdata);
            PMakedItem.readLoop(msdata, this.colorRes);
            this.stren = proto_util.readUShort(msdata);
            PMakedItem.readLoop(msdata, this.hole);
        }

        public static void readLoop(MemoryStream msdata, List<PItems> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PItems item = new PItems();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeULong(msdata, this.generalId);
            proto_util.writeUInt(msdata, this.templateId);
            proto_util.writeUInt(msdata, this.count);
            proto_util.writeUByte(msdata, this.pos);
            proto_util.writeULong(msdata, this.expire);
            proto_util.writeUShort(msdata, this.color);
            PMakedItem.writeLoop(msdata, this.colorRes);
            proto_util.writeUShort(msdata, this.stren);
            PMakedItem.writeLoop(msdata, this.hole);
        }

        public static void writeLoop(MemoryStream msdata, List<PItems> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PItems items in p)
            {
                items.write(msdata);
            }
        }
    }
}

