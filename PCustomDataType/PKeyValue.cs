﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PKeyValue
    {
        public uint key;
        public uint val;

        public void read(MemoryStream msdata)
        {
            this.key = proto_util.readUInt(msdata);
            this.val = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PKeyValue> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PKeyValue item = new PKeyValue();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.key);
            proto_util.writeUInt(msdata, this.val);
        }

        public static void writeLoop(MemoryStream msdata, List<PKeyValue> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PKeyValue value2 in p)
            {
                value2.write(msdata);
            }
        }
    }
}

