﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PLimitGoods
    {
        public uint count;
        public ulong id;
        public byte pos;
        public uint price;
        public uint sum;

        public void read(MemoryStream msdata)
        {
            this.pos = proto_util.readUByte(msdata);
            this.id = proto_util.readULong(msdata);
            this.price = proto_util.readUInt(msdata);
            this.sum = proto_util.readUInt(msdata);
            this.count = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PLimitGoods> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PLimitGoods item = new PLimitGoods();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.pos);
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.price);
            proto_util.writeUInt(msdata, this.sum);
            proto_util.writeUInt(msdata, this.count);
        }

        public static void writeLoop(MemoryStream msdata, List<PLimitGoods> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PLimitGoods goods in p)
            {
                goods.write(msdata);
            }
        }
    }
}

