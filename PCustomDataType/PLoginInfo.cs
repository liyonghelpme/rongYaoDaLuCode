﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PLoginInfo
    {
        public ulong id;
        public byte job;
        public uint lastLoginTime;
        public byte level;
        public string name = string.Empty;
        public uint serverId;
        public byte sex;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.job = proto_util.readUByte(msdata);
            this.level = proto_util.readUByte(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.lastLoginTime = proto_util.readUInt(msdata);
            this.serverId = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PLoginInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PLoginInfo item = new PLoginInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.job);
            proto_util.writeUByte(msdata, this.level);
            proto_util.writeUByte(msdata, this.sex);
            proto_util.writeUInt(msdata, this.lastLoginTime);
            proto_util.writeUInt(msdata, this.serverId);
        }

        public static void writeLoop(MemoryStream msdata, List<PLoginInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PLoginInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

