﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PMailAttach
    {
        public ushort count;
        public uint id;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUInt(msdata);
            this.count = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PMailAttach> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PMailAttach item = new PMailAttach();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.id);
            proto_util.writeUShort(msdata, this.count);
        }

        public static void writeLoop(MemoryStream msdata, List<PMailAttach> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PMailAttach attach in p)
            {
                attach.write(msdata);
            }
        }
    }
}

