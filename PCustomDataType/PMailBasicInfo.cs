﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PMailBasicInfo
    {
        public ulong id;
        public byte isRead;
        public uint senderId;
        public string senderNick = string.Empty;
        public uint sentTime;
        public string title = string.Empty;
        public byte type;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.senderId = proto_util.readUInt(msdata);
            this.senderNick = proto_util.readString(msdata);
            this.isRead = proto_util.readUByte(msdata);
            this.type = proto_util.readUByte(msdata);
            this.title = proto_util.readString(msdata);
            this.sentTime = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PMailBasicInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PMailBasicInfo item = new PMailBasicInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.senderId);
            proto_util.writeString(msdata, this.senderNick);
            proto_util.writeUByte(msdata, this.isRead);
            proto_util.writeUByte(msdata, this.type);
            proto_util.writeString(msdata, this.title);
            proto_util.writeUInt(msdata, this.sentTime);
        }

        public static void writeLoop(MemoryStream msdata, List<PMailBasicInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PMailBasicInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

