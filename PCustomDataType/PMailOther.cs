﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PMailOther
    {
        public string data = string.Empty;
        public byte type;

        public void read(MemoryStream msdata)
        {
            this.type = proto_util.readUByte(msdata);
            this.data = proto_util.readString(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PMailOther> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PMailOther item = new PMailOther();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.type);
            proto_util.writeString(msdata, this.data);
        }

        public static void writeLoop(MemoryStream msdata, List<PMailOther> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PMailOther other in p)
            {
                other.write(msdata);
            }
        }
    }
}

