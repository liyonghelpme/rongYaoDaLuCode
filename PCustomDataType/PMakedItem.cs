﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PMakedItem
    {
        public ushort num;
        public uint tid;

        public void read(MemoryStream msdata)
        {
            this.tid = proto_util.readUInt(msdata);
            this.num = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PMakedItem> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PMakedItem item = new PMakedItem();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.tid);
            proto_util.writeUShort(msdata, this.num);
        }

        public static void writeLoop(MemoryStream msdata, List<PMakedItem> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PMakedItem item in p)
            {
                item.write(msdata);
            }
        }
    }
}

