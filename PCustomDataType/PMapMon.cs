﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PMapMon
    {
        public byte born;
        public List<uint> buffList = new List<uint>();
        public byte dir;
        public int groupId;
        public uint hp;
        public uint hpFull;
        public ulong id;
        public ushort lvl;
        public int maxAliveTime;
        public uint monid;
        public ulong parentMonId;
        public int parentMonTemplateId;
        public byte rebornCount;
        public int rotateY;
        public ushort speed;
        public byte state;
        public int x;
        public int y;
        public int z;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.monid = proto_util.readUInt(msdata);
            this.lvl = proto_util.readUShort(msdata);
            this.parentMonId = proto_util.readULong(msdata);
            this.parentMonTemplateId = proto_util.readInt(msdata);
            this.rebornCount = proto_util.readUByte(msdata);
            this.maxAliveTime = proto_util.readInt(msdata);
            this.dir = proto_util.readUByte(msdata);
            this.hp = proto_util.readUInt(msdata);
            this.hpFull = proto_util.readUInt(msdata);
            this.speed = proto_util.readUShort(msdata);
            this.born = proto_util.readUByte(msdata);
            this.state = proto_util.readUByte(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.rotateY = proto_util.readInt(msdata);
            this.groupId = proto_util.readInt(msdata);
            proto_util.readLoopUInt(msdata, this.buffList);
        }

        public static void readLoop(MemoryStream msdata, List<PMapMon> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PMapMon item = new PMapMon();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.monid);
            proto_util.writeUShort(msdata, this.lvl);
            proto_util.writeULong(msdata, this.parentMonId);
            proto_util.writeInt(msdata, this.parentMonTemplateId);
            proto_util.writeUByte(msdata, this.rebornCount);
            proto_util.writeInt(msdata, this.maxAliveTime);
            proto_util.writeUByte(msdata, this.dir);
            proto_util.writeUInt(msdata, this.hp);
            proto_util.writeUInt(msdata, this.hpFull);
            proto_util.writeUShort(msdata, this.speed);
            proto_util.writeUByte(msdata, this.born);
            proto_util.writeUByte(msdata, this.state);
            proto_util.writeInt(msdata, this.x);
            proto_util.writeInt(msdata, this.y);
            proto_util.writeInt(msdata, this.z);
            proto_util.writeInt(msdata, this.rotateY);
            proto_util.writeInt(msdata, this.groupId);
            proto_util.writeLoopUInt(msdata, this.buffList);
        }

        public static void writeLoop(MemoryStream msdata, List<PMapMon> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PMapMon mon in p)
            {
                mon.write(msdata);
            }
        }
    }
}

