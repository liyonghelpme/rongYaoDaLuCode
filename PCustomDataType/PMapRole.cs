﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PMapRole
    {
        public List<uint> buffList = new List<uint>();
        public byte fightState;
        public List<PNewAttr> generalAttr = new List<PNewAttr>();
        public ulong id;
        public byte level;
        public uint mapId;
        public string name = string.Empty;
        public uint petId;
        public byte sex;
        public PStylebin styleBin = new PStylebin();
        public ulong useGeneral;
        public int x;
        public int y;
        public int z;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.level = proto_util.readUByte(msdata);
            this.useGeneral = proto_util.readULong(msdata);
            this.styleBin.read(msdata);
            this.mapId = proto_util.readUInt(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.petId = proto_util.readUInt(msdata);
            this.fightState = proto_util.readUByte(msdata);
            proto_util.readLoopUInt(msdata, this.buffList);
            PNewAttr.readLoop(msdata, this.generalAttr);
        }

        public static void readLoop(MemoryStream msdata, List<PMapRole> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PMapRole item = new PMapRole();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.sex);
            proto_util.writeUByte(msdata, this.level);
            proto_util.writeULong(msdata, this.useGeneral);
            this.styleBin.write(msdata);
            proto_util.writeUInt(msdata, this.mapId);
            proto_util.writeInt(msdata, this.x);
            proto_util.writeInt(msdata, this.y);
            proto_util.writeInt(msdata, this.z);
            proto_util.writeUInt(msdata, this.petId);
            proto_util.writeUByte(msdata, this.fightState);
            proto_util.writeLoopUInt(msdata, this.buffList);
            PNewAttr.writeLoop(msdata, this.generalAttr);
        }

        public static void writeLoop(MemoryStream msdata, List<PMapRole> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PMapRole role in p)
            {
                role.write(msdata);
            }
        }
    }
}

