﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PMaterial
    {
        public ushort num;
        public ulong tid;

        public void read(MemoryStream msdata)
        {
            this.tid = proto_util.readULong(msdata);
            this.num = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PMaterial> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PMaterial item = new PMaterial();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.tid);
            proto_util.writeUShort(msdata, this.num);
        }

        public static void writeLoop(MemoryStream msdata, List<PMaterial> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PMaterial material in p)
            {
                material.write(msdata);
            }
        }
    }
}

