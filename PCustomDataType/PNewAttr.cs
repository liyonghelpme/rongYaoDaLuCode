﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PNewAttr
    {
        public uint abDef;
        public uint abHurt;
        public uint atkCdDec;
        public uint atkNormalAdd;
        public uint atkSpeedAdd;
        public uint attM;
        public uint attMPen;
        public uint attMVam;
        public uint attP;
        public uint attPPen;
        public uint attPVam;
        public uint crit;
        public uint critAdd;
        public uint defM;
        public uint defMPen;
        public uint defP;
        public uint defPPen;
        public uint dmgBackM;
        public uint dmgBackP;
        public uint dodge;
        public uint fightPoint;
        public uint flex;
        public uint generalId;
        public uint hit;
        public uint hp;
        public uint hpFull;
        public uint hpRe;
        public ulong id;
        public uint moveSpeed;
        public uint moveSpeedAdd;
        public byte quality;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.generalId = proto_util.readUInt(msdata);
            this.quality = proto_util.readUByte(msdata);
            this.fightPoint = proto_util.readUInt(msdata);
            this.hp = proto_util.readUInt(msdata);
            this.hpFull = proto_util.readUInt(msdata);
            this.attP = proto_util.readUInt(msdata);
            this.attM = proto_util.readUInt(msdata);
            this.defP = proto_util.readUInt(msdata);
            this.defM = proto_util.readUInt(msdata);
            this.hit = proto_util.readUInt(msdata);
            this.dodge = proto_util.readUInt(msdata);
            this.crit = proto_util.readUInt(msdata);
            this.flex = proto_util.readUInt(msdata);
            this.abDef = proto_util.readUInt(msdata);
            this.abHurt = proto_util.readUInt(msdata);
            this.hpRe = proto_util.readUInt(msdata);
            this.moveSpeed = proto_util.readUInt(msdata);
            this.moveSpeedAdd = proto_util.readUInt(msdata);
            this.attPPen = proto_util.readUInt(msdata);
            this.attMPen = proto_util.readUInt(msdata);
            this.attPVam = proto_util.readUInt(msdata);
            this.attMVam = proto_util.readUInt(msdata);
            this.critAdd = proto_util.readUInt(msdata);
            this.atkSpeedAdd = proto_util.readUInt(msdata);
            this.atkCdDec = proto_util.readUInt(msdata);
            this.atkNormalAdd = proto_util.readUInt(msdata);
            this.defPPen = proto_util.readUInt(msdata);
            this.defMPen = proto_util.readUInt(msdata);
            this.dmgBackP = proto_util.readUInt(msdata);
            this.dmgBackM = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PNewAttr> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PNewAttr item = new PNewAttr();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.generalId);
            proto_util.writeUByte(msdata, this.quality);
            proto_util.writeUInt(msdata, this.fightPoint);
            proto_util.writeUInt(msdata, this.hp);
            proto_util.writeUInt(msdata, this.hpFull);
            proto_util.writeUInt(msdata, this.attP);
            proto_util.writeUInt(msdata, this.attM);
            proto_util.writeUInt(msdata, this.defP);
            proto_util.writeUInt(msdata, this.defM);
            proto_util.writeUInt(msdata, this.hit);
            proto_util.writeUInt(msdata, this.dodge);
            proto_util.writeUInt(msdata, this.crit);
            proto_util.writeUInt(msdata, this.flex);
            proto_util.writeUInt(msdata, this.abDef);
            proto_util.writeUInt(msdata, this.abHurt);
            proto_util.writeUInt(msdata, this.hpRe);
            proto_util.writeUInt(msdata, this.moveSpeed);
            proto_util.writeUInt(msdata, this.moveSpeedAdd);
            proto_util.writeUInt(msdata, this.attPPen);
            proto_util.writeUInt(msdata, this.attMPen);
            proto_util.writeUInt(msdata, this.attPVam);
            proto_util.writeUInt(msdata, this.attMVam);
            proto_util.writeUInt(msdata, this.critAdd);
            proto_util.writeUInt(msdata, this.atkSpeedAdd);
            proto_util.writeUInt(msdata, this.atkCdDec);
            proto_util.writeUInt(msdata, this.atkNormalAdd);
            proto_util.writeUInt(msdata, this.defPPen);
            proto_util.writeUInt(msdata, this.defMPen);
            proto_util.writeUInt(msdata, this.dmgBackP);
            proto_util.writeUInt(msdata, this.dmgBackM);
        }

        public static void writeLoop(MemoryStream msdata, List<PNewAttr> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PNewAttr attr in p)
            {
                attr.write(msdata);
            }
        }
    }
}

