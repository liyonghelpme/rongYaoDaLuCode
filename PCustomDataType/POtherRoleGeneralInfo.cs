﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class POtherRoleGeneralInfo
    {
        public List<PItems> equipList = new List<PItems>();
        public ulong exp;
        public ulong fightEffect;
        public PNewAttr generalAttrInfo = new PNewAttr();
        public uint generalTemplateId;
        public ulong id;
        public ushort lvl;
        public byte place;
        public byte quality;
        public List<PGeneralSkillInfo> skillList = new List<PGeneralSkillInfo>();
        public byte star;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.generalTemplateId = proto_util.readUInt(msdata);
            this.quality = proto_util.readUByte(msdata);
            this.place = proto_util.readUByte(msdata);
            this.exp = proto_util.readULong(msdata);
            this.fightEffect = proto_util.readULong(msdata);
            this.lvl = proto_util.readUShort(msdata);
            this.star = proto_util.readUByte(msdata);
            this.generalAttrInfo.read(msdata);
            PGeneralSkillInfo.readLoop(msdata, this.skillList);
            PItems.readLoop(msdata, this.equipList);
        }

        public static void readLoop(MemoryStream msdata, List<POtherRoleGeneralInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                POtherRoleGeneralInfo item = new POtherRoleGeneralInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.generalTemplateId);
            proto_util.writeUByte(msdata, this.quality);
            proto_util.writeUByte(msdata, this.place);
            proto_util.writeULong(msdata, this.exp);
            proto_util.writeULong(msdata, this.fightEffect);
            proto_util.writeUShort(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.star);
            this.generalAttrInfo.write(msdata);
            PGeneralSkillInfo.writeLoop(msdata, this.skillList);
            PItems.writeLoop(msdata, this.equipList);
        }

        public static void writeLoop(MemoryStream msdata, List<POtherRoleGeneralInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (POtherRoleGeneralInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

