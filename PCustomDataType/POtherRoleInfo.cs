﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class POtherRoleInfo
    {
        public ulong exp;
        public uint fightEffect;
        public uint generalId;
        public List<PSetGeneralInfo> generalIdList = new List<PSetGeneralInfo>();
        public string guildName = string.Empty;
        public ulong id;
        public ushort lvl;
        public ushort medalNum;
        public string name = string.Empty;
        public string title = string.Empty;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.generalId = proto_util.readUInt(msdata);
            this.name = proto_util.readString(msdata);
            this.lvl = proto_util.readUShort(msdata);
            this.exp = proto_util.readULong(msdata);
            this.title = proto_util.readString(msdata);
            this.guildName = proto_util.readString(msdata);
            this.fightEffect = proto_util.readUInt(msdata);
            PSetGeneralInfo.readLoop(msdata, this.generalIdList);
            this.medalNum = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<POtherRoleInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                POtherRoleInfo item = new POtherRoleInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.generalId);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUShort(msdata, this.lvl);
            proto_util.writeULong(msdata, this.exp);
            proto_util.writeString(msdata, this.title);
            proto_util.writeString(msdata, this.guildName);
            proto_util.writeUInt(msdata, this.fightEffect);
            PSetGeneralInfo.writeLoop(msdata, this.generalIdList);
            proto_util.writeUShort(msdata, this.medalNum);
        }

        public static void writeLoop(MemoryStream msdata, List<POtherRoleInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (POtherRoleInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

