﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PPvpPrivate
    {
        public int dir;
        public int groupId;
        public ulong monId;
        public int x;
        public int y;
        public int z;

        public void read(MemoryStream msdata)
        {
            this.monId = proto_util.readULong(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.dir = proto_util.readInt(msdata);
            this.groupId = proto_util.readInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PPvpPrivate> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PPvpPrivate item = new PPvpPrivate();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.monId);
            proto_util.writeInt(msdata, this.x);
            proto_util.writeInt(msdata, this.y);
            proto_util.writeInt(msdata, this.z);
            proto_util.writeInt(msdata, this.dir);
            proto_util.writeInt(msdata, this.groupId);
        }

        public static void writeLoop(MemoryStream msdata, List<PPvpPrivate> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PPvpPrivate @private in p)
            {
                @private.write(msdata);
            }
        }
    }
}

