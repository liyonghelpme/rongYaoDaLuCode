﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRank
    {
        public List<uint> data = new List<uint>();
        public List<PItems> goodsList = new List<PItems>();
        public ushort pos;
        public List<PRankRole> role = new List<PRankRole>();

        public void read(MemoryStream msdata)
        {
            this.pos = proto_util.readUShort(msdata);
            PRankRole.readLoop(msdata, this.role);
            proto_util.readLoopUInt(msdata, this.data);
            PItems.readLoop(msdata, this.goodsList);
        }

        public static void readLoop(MemoryStream msdata, List<PRank> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRank item = new PRank();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUShort(msdata, this.pos);
            PRankRole.writeLoop(msdata, this.role);
            proto_util.writeLoopUInt(msdata, this.data);
            PItems.writeLoop(msdata, this.goodsList);
        }

        public static void writeLoop(MemoryStream msdata, List<PRank> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRank rank in p)
            {
                rank.write(msdata);
            }
        }
    }
}

