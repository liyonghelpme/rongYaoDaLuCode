﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRankNew
    {
        public uint iconId;
        public string name = string.Empty;
        public ushort pos;
        public ulong userid;
        public uint value;

        public void read(MemoryStream msdata)
        {
            this.pos = proto_util.readUShort(msdata);
            this.userid = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.iconId = proto_util.readUInt(msdata);
            this.value = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRankNew> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRankNew item = new PRankNew();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUShort(msdata, this.pos);
            proto_util.writeULong(msdata, this.userid);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUInt(msdata, this.iconId);
            proto_util.writeUInt(msdata, this.value);
        }

        public static void writeLoop(MemoryStream msdata, List<PRankNew> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRankNew new2 in p)
            {
                new2.write(msdata);
            }
        }
    }
}

