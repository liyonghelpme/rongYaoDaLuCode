﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRankRole
    {
        public uint fightpoint;
        public byte job;
        public byte lvl;
        public string name = string.Empty;
        public byte sex;
        public ulong userid;

        public void read(MemoryStream msdata)
        {
            this.userid = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.job = proto_util.readUByte(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.fightpoint = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRankRole> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRankRole item = new PRankRole();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.userid);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.job);
            proto_util.writeUByte(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.sex);
            proto_util.writeUInt(msdata, this.fightpoint);
        }

        public static void writeLoop(MemoryStream msdata, List<PRankRole> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRankRole role in p)
            {
                role.write(msdata);
            }
        }
    }
}

