﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRecruit
    {
        public byte color;
        public uint id;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUInt(msdata);
            this.color = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRecruit> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRecruit item = new PRecruit();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.id);
            proto_util.writeUByte(msdata, this.color);
        }

        public static void writeLoop(MemoryStream msdata, List<PRecruit> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRecruit recruit in p)
            {
                recruit.write(msdata);
            }
        }
    }
}

