﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRelationInfo
    {
        public uint fightpoint;
        public uint intimate;
        public byte isOnline;
        public byte job;
        public uint lastLogoutTime;
        public byte lvl;
        public uint mapid;
        public string name = string.Empty;
        public ulong roleId;
        public byte sex;
        public byte vip;

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.job = proto_util.readUByte(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.mapid = proto_util.readUInt(msdata);
            this.lastLogoutTime = proto_util.readUInt(msdata);
            this.isOnline = proto_util.readUByte(msdata);
            this.vip = proto_util.readUByte(msdata);
            this.intimate = proto_util.readUInt(msdata);
            this.fightpoint = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRelationInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRelationInfo item = new PRelationInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.roleId);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.job);
            proto_util.writeUByte(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.sex);
            proto_util.writeUInt(msdata, this.mapid);
            proto_util.writeUInt(msdata, this.lastLogoutTime);
            proto_util.writeUByte(msdata, this.isOnline);
            proto_util.writeUByte(msdata, this.vip);
            proto_util.writeUInt(msdata, this.intimate);
            proto_util.writeUInt(msdata, this.fightpoint);
        }

        public static void writeLoop(MemoryStream msdata, List<PRelationInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRelationInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

