﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRelationNear
    {
        public uint fightpoint;
        public byte job;
        public byte lvl;
        public string name = string.Empty;
        public ulong roleId;
        public byte sex;
        public byte vip;

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.job = proto_util.readUByte(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.fightpoint = proto_util.readUInt(msdata);
            this.vip = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRelationNear> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRelationNear item = new PRelationNear();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.roleId);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.job);
            proto_util.writeUByte(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.sex);
            proto_util.writeUInt(msdata, this.fightpoint);
            proto_util.writeUByte(msdata, this.vip);
        }

        public static void writeLoop(MemoryStream msdata, List<PRelationNear> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRelationNear near in p)
            {
                near.write(msdata);
            }
        }
    }
}

