﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRole
    {
        public ulong id;
        public byte job;
        public byte level;
        public uint mapId;
        public string name = string.Empty;
        public byte sex;
        public int x;
        public int y;
        public int z;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.level = proto_util.readUByte(msdata);
            this.job = proto_util.readUByte(msdata);
            this.mapId = proto_util.readUInt(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRole> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRole item = new PRole();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.sex);
            proto_util.writeUByte(msdata, this.level);
            proto_util.writeUByte(msdata, this.job);
            proto_util.writeUInt(msdata, this.mapId);
            proto_util.writeInt(msdata, this.x);
            proto_util.writeInt(msdata, this.y);
            proto_util.writeInt(msdata, this.z);
        }

        public static void writeLoop(MemoryStream msdata, List<PRole> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRole role in p)
            {
                role.write(msdata);
            }
        }
    }
}

