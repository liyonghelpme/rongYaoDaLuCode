﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRoleAttr
    {
        public ulong combat;
        public string customFace = string.Empty;
        public uint diam;
        public uint diamBind;
        public ulong exp;
        public ulong expFull;
        public ulong generalId;
        public uint gold;
        public ulong guildId;
        public string guildName = string.Empty;
        public byte hasCombine;
        public ulong id;
        public byte job;
        public byte level;
        public string name = string.Empty;
        public byte nation;
        public uint repu;
        public byte sex;
        public PStylebin styleBin = new PStylebin();
        public List<uint> titleList = new List<uint>();
        public ushort vigour;
        public ushort vigourFull;
        public byte vip;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.level = proto_util.readUByte(msdata);
            this.job = proto_util.readUByte(msdata);
            this.exp = proto_util.readULong(msdata);
            this.expFull = proto_util.readULong(msdata);
            this.combat = proto_util.readULong(msdata);
            this.vip = proto_util.readUByte(msdata);
            this.nation = proto_util.readUByte(msdata);
            this.gold = proto_util.readUInt(msdata);
            this.diam = proto_util.readUInt(msdata);
            this.diamBind = proto_util.readUInt(msdata);
            this.repu = proto_util.readUInt(msdata);
            this.vigour = proto_util.readUShort(msdata);
            this.vigourFull = proto_util.readUShort(msdata);
            this.hasCombine = proto_util.readUByte(msdata);
            this.customFace = proto_util.readString(msdata);
            proto_util.readLoopUInt(msdata, this.titleList);
            this.guildId = proto_util.readULong(msdata);
            this.guildName = proto_util.readString(msdata);
            this.generalId = proto_util.readULong(msdata);
            this.styleBin.read(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRoleAttr> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRoleAttr item = new PRoleAttr();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.sex);
            proto_util.writeUByte(msdata, this.level);
            proto_util.writeUByte(msdata, this.job);
            proto_util.writeULong(msdata, this.exp);
            proto_util.writeULong(msdata, this.expFull);
            proto_util.writeULong(msdata, this.combat);
            proto_util.writeUByte(msdata, this.vip);
            proto_util.writeUByte(msdata, this.nation);
            proto_util.writeUInt(msdata, this.gold);
            proto_util.writeUInt(msdata, this.diam);
            proto_util.writeUInt(msdata, this.diamBind);
            proto_util.writeUInt(msdata, this.repu);
            proto_util.writeUShort(msdata, this.vigour);
            proto_util.writeUShort(msdata, this.vigourFull);
            proto_util.writeUByte(msdata, this.hasCombine);
            proto_util.writeString(msdata, this.customFace);
            proto_util.writeLoopUInt(msdata, this.titleList);
            proto_util.writeULong(msdata, this.guildId);
            proto_util.writeString(msdata, this.guildName);
            proto_util.writeULong(msdata, this.generalId);
            this.styleBin.write(msdata);
        }

        public static void writeLoop(MemoryStream msdata, List<PRoleAttr> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRoleAttr attr in p)
            {
                attr.write(msdata);
            }
        }
    }
}

