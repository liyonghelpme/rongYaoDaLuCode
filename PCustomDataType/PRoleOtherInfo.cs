﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRoleOtherInfo
    {
        public uint fightEffect;
        public uint generalId;
        public uint generalId1;
        public uint generalId2;
        public uint generalId3;
        public string guildName = string.Empty;
        public ulong id;
        public ushort lvl;
        public string name = string.Empty;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.generalId = proto_util.readUInt(msdata);
            this.name = proto_util.readString(msdata);
            this.lvl = proto_util.readUShort(msdata);
            this.guildName = proto_util.readString(msdata);
            this.fightEffect = proto_util.readUInt(msdata);
            this.generalId1 = proto_util.readUInt(msdata);
            this.generalId2 = proto_util.readUInt(msdata);
            this.generalId3 = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRoleOtherInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRoleOtherInfo item = new PRoleOtherInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.generalId);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUShort(msdata, this.lvl);
            proto_util.writeString(msdata, this.guildName);
            proto_util.writeUInt(msdata, this.fightEffect);
            proto_util.writeUInt(msdata, this.generalId1);
            proto_util.writeUInt(msdata, this.generalId2);
            proto_util.writeUInt(msdata, this.generalId3);
        }

        public static void writeLoop(MemoryStream msdata, List<PRoleOtherInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRoleOtherInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

