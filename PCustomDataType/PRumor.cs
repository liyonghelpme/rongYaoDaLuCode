﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRumor
    {
        public List<PRumorGoods> goods = new List<PRumorGoods>();
        public List<uint> iData = new List<uint>();
        public List<PRumorMon> mon = new List<PRumorMon>();
        public List<PRumorRole> role = new List<PRumorRole>();
        public List<string> sData = new List<string>();

        public void read(MemoryStream msdata)
        {
            PRumorRole.readLoop(msdata, this.role);
            PRumorGoods.readLoop(msdata, this.goods);
            PRumorMon.readLoop(msdata, this.mon);
            proto_util.readLoopUInt(msdata, this.iData);
            proto_util.readLoopString(msdata, this.sData);
        }

        public static void readLoop(MemoryStream msdata, List<PRumor> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRumor item = new PRumor();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            PRumorRole.writeLoop(msdata, this.role);
            PRumorGoods.writeLoop(msdata, this.goods);
            PRumorMon.writeLoop(msdata, this.mon);
            proto_util.writeLoopUInt(msdata, this.iData);
            proto_util.writeLoopString(msdata, this.sData);
        }

        public static void writeLoop(MemoryStream msdata, List<PRumor> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRumor rumor in p)
            {
                rumor.write(msdata);
            }
        }
    }
}

