﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRumorGoods
    {
        public uint templateId;
        public ulong uniqueId;

        public void read(MemoryStream msdata)
        {
            this.templateId = proto_util.readUInt(msdata);
            this.uniqueId = proto_util.readULong(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRumorGoods> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRumorGoods item = new PRumorGoods();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.templateId);
            proto_util.writeULong(msdata, this.uniqueId);
        }

        public static void writeLoop(MemoryStream msdata, List<PRumorGoods> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRumorGoods goods in p)
            {
                goods.write(msdata);
            }
        }
    }
}

