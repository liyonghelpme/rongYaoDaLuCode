﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRumorMon
    {
        public ulong monId;

        public void read(MemoryStream msdata)
        {
            this.monId = proto_util.readULong(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRumorMon> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRumorMon item = new PRumorMon();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.monId);
        }

        public static void writeLoop(MemoryStream msdata, List<PRumorMon> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRumorMon mon in p)
            {
                mon.write(msdata);
            }
        }
    }
}

