﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PRumorRole
    {
        public byte job;
        public byte lvl;
        public string name = string.Empty;
        public byte sex;
        public ulong userid;

        public void read(MemoryStream msdata)
        {
            this.userid = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.job = proto_util.readUByte(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.sex = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PRumorRole> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PRumorRole item = new PRumorRole();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.userid);
            proto_util.writeString(msdata, this.name);
            proto_util.writeUByte(msdata, this.job);
            proto_util.writeUByte(msdata, this.lvl);
            proto_util.writeUByte(msdata, this.sex);
        }

        public static void writeLoop(MemoryStream msdata, List<PRumorRole> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PRumorRole role in p)
            {
                role.write(msdata);
            }
        }
    }
}

