﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PSetGeneralInfo
    {
        public ulong id;
        public byte place;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.place = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PSetGeneralInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PSetGeneralInfo item = new PSetGeneralInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUByte(msdata, this.place);
        }

        public static void writeLoop(MemoryStream msdata, List<PSetGeneralInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PSetGeneralInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

