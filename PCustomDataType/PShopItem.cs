﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PShopItem
    {
        public int amount;
        public ulong id;
        public uint isFixed;
        public uint price;
        public uint shopId;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.shopId = proto_util.readUInt(msdata);
            this.price = proto_util.readUInt(msdata);
            this.amount = proto_util.readInt(msdata);
            this.isFixed = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PShopItem> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PShopItem item = new PShopItem();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUInt(msdata, this.shopId);
            proto_util.writeUInt(msdata, this.price);
            proto_util.writeInt(msdata, this.amount);
            proto_util.writeUInt(msdata, this.isFixed);
        }

        public static void writeLoop(MemoryStream msdata, List<PShopItem> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PShopItem item in p)
            {
                item.write(msdata);
            }
        }
    }
}

