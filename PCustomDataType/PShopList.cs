﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PShopList
    {
        public uint endTime;
        public List<PShopItem> itemList = new List<PShopItem>();
        public ushort refreshCount;
        public byte type;

        public void read(MemoryStream msdata)
        {
            this.type = proto_util.readUByte(msdata);
            this.refreshCount = proto_util.readUShort(msdata);
            this.endTime = proto_util.readUInt(msdata);
            PShopItem.readLoop(msdata, this.itemList);
        }

        public static void readLoop(MemoryStream msdata, List<PShopList> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PShopList item = new PShopList();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.type);
            proto_util.writeUShort(msdata, this.refreshCount);
            proto_util.writeUInt(msdata, this.endTime);
            PShopItem.writeLoop(msdata, this.itemList);
        }

        public static void writeLoop(MemoryStream msdata, List<PShopList> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PShopList list in p)
            {
                list.write(msdata);
            }
        }
    }
}

