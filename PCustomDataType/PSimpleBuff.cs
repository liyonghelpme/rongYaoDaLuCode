﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PSimpleBuff
    {
        public uint buffId;
        public byte level;

        public void read(MemoryStream msdata)
        {
            this.buffId = proto_util.readUInt(msdata);
            this.level = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PSimpleBuff> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PSimpleBuff item = new PSimpleBuff();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.buffId);
            proto_util.writeUByte(msdata, this.level);
        }

        public static void writeLoop(MemoryStream msdata, List<PSimpleBuff> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PSimpleBuff buff in p)
            {
                buff.write(msdata);
            }
        }
    }
}

