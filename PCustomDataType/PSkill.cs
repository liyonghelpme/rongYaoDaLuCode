﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PSkill
    {
        public uint nextiD;
        public uint skillId;

        public void read(MemoryStream msdata)
        {
            this.skillId = proto_util.readUInt(msdata);
            this.nextiD = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PSkill> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PSkill item = new PSkill();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.skillId);
            proto_util.writeUInt(msdata, this.nextiD);
        }

        public static void writeLoop(MemoryStream msdata, List<PSkill> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PSkill skill in p)
            {
                skill.write(msdata);
            }
        }
    }
}

