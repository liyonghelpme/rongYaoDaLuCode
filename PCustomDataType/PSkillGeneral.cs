﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PSkillGeneral
    {
        public ulong generalId;
        public List<PGeneralSkillInfo> skillList = new List<PGeneralSkillInfo>();

        public void read(MemoryStream msdata)
        {
            this.generalId = proto_util.readULong(msdata);
            PGeneralSkillInfo.readLoop(msdata, this.skillList);
        }

        public static void readLoop(MemoryStream msdata, List<PSkillGeneral> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PSkillGeneral item = new PSkillGeneral();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.generalId);
            PGeneralSkillInfo.writeLoop(msdata, this.skillList);
        }

        public static void writeLoop(MemoryStream msdata, List<PSkillGeneral> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PSkillGeneral general in p)
            {
                general.write(msdata);
            }
        }
    }
}

