﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PStylebin
    {
        public int id;
        public byte quality;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readInt(msdata);
            this.quality = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PStylebin> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PStylebin item = new PStylebin();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeInt(msdata, this.id);
            proto_util.writeUByte(msdata, this.quality);
        }

        public static void writeLoop(MemoryStream msdata, List<PStylebin> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PStylebin stylebin in p)
            {
                stylebin.write(msdata);
            }
        }
    }
}

