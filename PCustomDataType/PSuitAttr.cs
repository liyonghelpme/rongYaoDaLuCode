﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PSuitAttr
    {
        public ushort attr;
        public byte id;
        public byte pos;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUByte(msdata);
            this.attr = proto_util.readUShort(msdata);
            this.pos = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PSuitAttr> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PSuitAttr item = new PSuitAttr();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.id);
            proto_util.writeUShort(msdata, this.attr);
            proto_util.writeUByte(msdata, this.pos);
        }

        public static void writeLoop(MemoryStream msdata, List<PSuitAttr> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PSuitAttr attr in p)
            {
                attr.write(msdata);
            }
        }
    }
}

