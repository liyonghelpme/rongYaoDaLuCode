﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PSysBuff
    {
        public uint time;
        public ushort type;

        public void read(MemoryStream msdata)
        {
            this.type = proto_util.readUShort(msdata);
            this.time = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PSysBuff> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PSysBuff item = new PSysBuff();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUShort(msdata, this.type);
            proto_util.writeUInt(msdata, this.time);
        }

        public static void writeLoop(MemoryStream msdata, List<PSysBuff> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PSysBuff buff in p)
            {
                buff.write(msdata);
            }
        }
    }
}

