﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PSystemSettings
    {
        public byte key;
        public byte value;

        public void read(MemoryStream msdata)
        {
            this.key = proto_util.readUByte(msdata);
            this.value = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PSystemSettings> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PSystemSettings item = new PSystemSettings();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.key);
            proto_util.writeUByte(msdata, this.value);
        }

        public static void writeLoop(MemoryStream msdata, List<PSystemSettings> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PSystemSettings settings in p)
            {
                settings.write(msdata);
            }
        }
    }
}

