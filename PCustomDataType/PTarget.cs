﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PTarget
    {
        public ulong id;
        public byte type;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PTarget> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PTarget item = new PTarget();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUByte(msdata, this.type);
        }

        public static void writeLoop(MemoryStream msdata, List<PTarget> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PTarget target in p)
            {
                target.write(msdata);
            }
        }
    }
}

