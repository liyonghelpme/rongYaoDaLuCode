﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PTaskDoing
    {
        public byte state;
        public uint taskId;
        public List<PTaskTrack> track = new List<PTaskTrack>();

        public void read(MemoryStream msdata)
        {
            this.taskId = proto_util.readUInt(msdata);
            this.state = proto_util.readUByte(msdata);
            PTaskTrack.readLoop(msdata, this.track);
        }

        public static void readLoop(MemoryStream msdata, List<PTaskDoing> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PTaskDoing item = new PTaskDoing();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.taskId);
            proto_util.writeUByte(msdata, this.state);
            PTaskTrack.writeLoop(msdata, this.track);
        }

        public static void writeLoop(MemoryStream msdata, List<PTaskDoing> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PTaskDoing doing in p)
            {
                doing.write(msdata);
            }
        }
    }
}

