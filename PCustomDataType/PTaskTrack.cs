﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PTaskTrack
    {
        public ushort id;
        public ushort n;
        public ushort sum;
        public byte type;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
            this.n = proto_util.readUShort(msdata);
            this.sum = proto_util.readUShort(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PTaskTrack> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PTaskTrack item = new PTaskTrack();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUShort(msdata, this.id);
            proto_util.writeUByte(msdata, this.type);
            proto_util.writeUShort(msdata, this.n);
            proto_util.writeUShort(msdata, this.sum);
        }

        public static void writeLoop(MemoryStream msdata, List<PTaskTrack> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PTaskTrack track in p)
            {
                track.write(msdata);
            }
        }
    }
}

