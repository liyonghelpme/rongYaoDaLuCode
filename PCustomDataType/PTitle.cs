﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PTitle
    {
        public uint count;
        public uint leftTime;
        public byte status;
        public uint titleId;

        public void read(MemoryStream msdata)
        {
            this.titleId = proto_util.readUInt(msdata);
            this.status = proto_util.readUByte(msdata);
            this.leftTime = proto_util.readUInt(msdata);
            this.count = proto_util.readUInt(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PTitle> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PTitle item = new PTitle();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUInt(msdata, this.titleId);
            proto_util.writeUByte(msdata, this.status);
            proto_util.writeUInt(msdata, this.leftTime);
            proto_util.writeUInt(msdata, this.count);
        }

        public static void writeLoop(MemoryStream msdata, List<PTitle> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PTitle title in p)
            {
                title.write(msdata);
            }
        }
    }
}

