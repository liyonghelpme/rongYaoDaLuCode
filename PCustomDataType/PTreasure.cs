﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PTreasure
    {
        public ulong id;
        public byte num;

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.num = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PTreasure> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PTreasure item = new PTreasure();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeULong(msdata, this.id);
            proto_util.writeUByte(msdata, this.num);
        }

        public static void writeLoop(MemoryStream msdata, List<PTreasure> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PTreasure treasure in p)
            {
                treasure.write(msdata);
            }
        }
    }
}

