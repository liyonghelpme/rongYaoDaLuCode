﻿namespace PCustomDataType
{
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class PVipRewardGetInfo
    {
        public byte state;
        public byte vipLevel;

        public void read(MemoryStream msdata)
        {
            this.vipLevel = proto_util.readUByte(msdata);
            this.state = proto_util.readUByte(msdata);
        }

        public static void readLoop(MemoryStream msdata, List<PVipRewardGetInfo> p)
        {
            int num = proto_util.readShort(msdata);
            for (int i = 0; i < num; i++)
            {
                PVipRewardGetInfo item = new PVipRewardGetInfo();
                item.read(msdata);
                p.Add(item);
            }
        }

        public void write(MemoryStream msdata)
        {
            proto_util.writeUByte(msdata, this.vipLevel);
            proto_util.writeUByte(msdata, this.state);
        }

        public static void writeLoop(MemoryStream msdata, List<PVipRewardGetInfo> p)
        {
            proto_util.writeShort(msdata, (short) p.Count);
            foreach (PVipRewardGetInfo info in p)
            {
                info.write(msdata);
            }
        }
    }
}

