﻿using com.game.module.core;
using com.u3d.bases.debug;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PageIndex : MonoBehaviour
{
    public string atlasName = "common";
    public string backgroundSprite = "fanye_background";
    public UICenterOnChild centerOnChild;
    public int current;
    public int depth = 100;
    public string hightligthSprite = "fanye_hightlight";
    private Transform mTrans;
    public float padding = 36f;
    public Vector2 size = new Vector2(22f, 22f);
    private List<UISprite> spriteList = new List<UISprite>();
    private int total;

    private void Awake()
    {
        this.mTrans = base.transform;
        if (this.spriteList.Count == 0)
        {
            GameObject obj2 = new GameObject("page");
            UISprite item = obj2.AddComponent<UISprite>();
            obj2.layer = this.mTrans.gameObject.layer;
            item.width = (int) this.size.x;
            item.height = (int) this.size.y;
            item.depth = 100;
            item.atlas = Singleton<AtlasManager>.Instance.GetAtlas(this.atlasName);
            this.spriteList.Add(item);
            if (this.centerOnChild != null)
            {
                this.RegisterOnCenter(this.centerOnChild);
            }
        }
    }

    public void InitPage(int current, int total)
    {
        float num;
        this.Awake();
        this.current = current;
        this.total = total;
        Vector3 zero = Vector3.zero;
        if ((total % 2) == 1)
        {
            num = this.padding * (total / 2);
        }
        else
        {
            num = (this.padding * (total / 2)) - (this.size.x / 2f);
        }
        GameObject gameObject = this.spriteList[0].gameObject;
        while (this.spriteList.Count < total)
        {
            this.spriteList.Add((UnityEngine.Object.Instantiate(gameObject) as GameObject).GetComponent<UISprite>());
        }
        zero.x -= num;
        int num2 = 0;
        int count = this.spriteList.Count;
        while (num2 < count)
        {
            UISprite rect = this.spriteList[num2];
            if (num2 < total)
            {
                if (current == (num2 + 1))
                {
                    rect.spriteName = this.hightligthSprite;
                    rect.transform.localScale = (Vector3) (Vector3.one * 1.1f);
                }
                else
                {
                    rect.spriteName = this.backgroundSprite;
                    rect.transform.localScale = Vector3.one;
                }
                rect.transform.parent = this.mTrans;
                rect.transform.localPosition = zero;
                zero.x += this.padding;
                rect.SetActive(true);
                rect.depth = 100 + num2;
                rect.MakePixelPerfect();
            }
            else
            {
                rect.gameObject.SetActive(false);
            }
            num2++;
        }
    }

    private void PageIndexCallBack()
    {
        Log.info(this, "Child Count : " + NGUITools.GetActiveChildrenCount(this.centerOnChild.gameObject));
        this.InitPage(int.Parse(this.centerOnChild.centeredObject.name), NGUITools.GetActiveChildrenCount(this.centerOnChild.gameObject));
    }

    public void RegisterOnCenter(UICenterOnChild center)
    {
        this.centerOnChild = center;
        this.centerOnChild.onCenterFinish = new UICenterOnChild.OnCenterFinish(this.PageIndexCallBack);
    }

    public void TurnLeft()
    {
        this.current--;
        this.TurnTo(this.current);
    }

    public void TurnRight()
    {
        this.current++;
        this.TurnTo(this.current);
    }

    public void TurnTo(int num)
    {
        if (num < 1)
        {
            num = 1;
        }
        else if (num > this.total)
        {
            num = this.total;
        }
        this.InitPage(num, this.total);
    }
}

