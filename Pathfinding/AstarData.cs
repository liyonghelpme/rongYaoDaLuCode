﻿namespace Pathfinding
{
    using Pathfinding.Serialization;
    using Pathfinding.Util;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;

    [Serializable]
    public class AstarData
    {
        [SerializeField]
        public bool cacheStartup;
        public bool compress;
        [SerializeField]
        private byte[] data;
        public byte[] data_backup;
        public byte[] data_cachedStartup;
        public uint dataChecksum;
        [NonSerialized]
        public NavGraph[] graphs = new NavGraph[0];
        public System.Type[] graphTypes;
        [NonSerialized]
        public GridGraph gridGraph;
        public bool hasBeenReverted;
        [NonSerialized]
        public NavMeshGraph navmesh;
        [NonSerialized]
        public PointGraph pointGraph;
        [NonSerialized]
        public RecastGraph recastGraph;
        public byte[] revertData;
        [NonSerialized]
        public UserConnection[] userConnections = new UserConnection[0];

        public void AddGraph(NavGraph graph)
        {
            AstarPath.active.BlockUntilPathQueueBlocked();
            for (int i = 0; i < this.graphs.Length; i++)
            {
                if (this.graphs[i] == null)
                {
                    this.graphs[i] = graph;
                    return;
                }
            }
            if ((this.graphs != null) && (this.graphs.Length >= 2L))
            {
                throw new Exception("Graph Count Limit Reached. You cannot have more than " + 3 + " graphs. Some compiler directives can change this limit, e.g ASTAR_MORE_AREAS, look under the 'Optimizations' tab in the A* Inspector");
            }
            this.graphs = new List<NavGraph>(this.graphs) { graph }.ToArray();
            this.UpdateShortcuts();
            graph.active = this.active;
            graph.Awake();
            graph.graphIndex = (uint) (this.graphs.Length - 1);
        }

        public NavGraph AddGraph(string type)
        {
            NavGraph graph = null;
            for (int i = 0; i < this.graphTypes.Length; i++)
            {
                if (this.graphTypes[i].Name == type)
                {
                    graph = this.CreateGraph(this.graphTypes[i]);
                }
            }
            if (graph == null)
            {
                UnityEngine.Debug.LogError("No NavGraph of type '" + type + "' could be found");
                return null;
            }
            this.AddGraph(graph);
            return graph;
        }

        public NavGraph AddGraph(System.Type type)
        {
            NavGraph graph = null;
            for (int i = 0; i < this.graphTypes.Length; i++)
            {
                if (object.Equals(this.graphTypes[i], type))
                {
                    graph = this.CreateGraph(this.graphTypes[i]);
                }
            }
            if (graph == null)
            {
                UnityEngine.Debug.LogError(string.Concat(new object[] { "No NavGraph of type '", type, "' could be found, ", this.graphTypes.Length, " graph types are avaliable" }));
                return null;
            }
            this.AddGraph(graph);
            return graph;
        }

        public void Awake()
        {
            this.userConnections = new UserConnection[0];
            this.graphs = new NavGraph[0];
            if (this.cacheStartup && (this.data_cachedStartup != null))
            {
                this.LoadFromCache();
            }
            else
            {
                this.DeserializeGraphs();
            }
        }

        private void ClearGraphs()
        {
            if (this.graphs != null)
            {
                for (int i = 0; i < this.graphs.Length; i++)
                {
                    if (this.graphs[i] != null)
                    {
                        this.graphs[i].OnDestroy();
                    }
                }
                this.graphs = null;
                this.UpdateShortcuts();
            }
        }

        public NavGraph CreateGraph(string type)
        {
            UnityEngine.Debug.Log("Creating Graph of type '" + type + "'");
            for (int i = 0; i < this.graphTypes.Length; i++)
            {
                if (this.graphTypes[i].Name == type)
                {
                    return this.CreateGraph(this.graphTypes[i]);
                }
            }
            UnityEngine.Debug.LogError("Graph type (" + type + ") wasn't found");
            return null;
        }

        public NavGraph CreateGraph(System.Type type)
        {
            NavGraph graph = Activator.CreateInstance(type) as NavGraph;
            graph.active = this.active;
            return graph;
        }

        public void DeserializeGraphs()
        {
            if (this.data != null)
            {
                this.DeserializeGraphs(this.data);
            }
        }

        public void DeserializeGraphs(byte[] bytes)
        {
            AstarPath.active.BlockUntilPathQueueBlocked();
            try
            {
                if (bytes == null)
                {
                    throw new ArgumentNullException("Bytes should not be null when passed to DeserializeGraphs");
                }
                AstarSerializer sr = new AstarSerializer(this);
                if (sr.OpenDeserialize(bytes))
                {
                    this.DeserializeGraphsPart(sr);
                    sr.CloseDeserialize();
                }
                else
                {
                    UnityEngine.Debug.Log("Invalid data file (cannot read zip).\nThe data is either corrupt or it was saved using a 3.0.x or earlier version of the system");
                }
                this.active.VerifyIntegrity();
            }
            catch (Exception exception)
            {
                UnityEngine.Debug.LogWarning("Caught exception while deserializing data.\n" + exception);
                this.data_backup = bytes;
            }
        }

        public void DeserializeGraphsAdditive(byte[] bytes)
        {
            AstarPath.active.BlockUntilPathQueueBlocked();
            try
            {
                if (bytes == null)
                {
                    throw new ArgumentNullException("Bytes should not be null when passed to DeserializeGraphs");
                }
                AstarSerializer sr = new AstarSerializer(this);
                if (sr.OpenDeserialize(bytes))
                {
                    this.DeserializeGraphsPartAdditive(sr);
                    sr.CloseDeserialize();
                }
                else
                {
                    UnityEngine.Debug.Log("Invalid data file (cannot read zip).");
                }
                this.active.VerifyIntegrity();
            }
            catch (Exception exception)
            {
                UnityEngine.Debug.LogWarning("Caught exception while deserializing data.\n" + exception);
            }
        }

        public void DeserializeGraphsPart(AstarSerializer sr)
        {
            this.ClearGraphs();
            this.graphs = sr.DeserializeGraphs();
            if (this.graphs != null)
            {
                for (int i = 0; i < this.graphs.Length; i++)
                {
                    if (this.graphs[i] != null)
                    {
                        this.graphs[i].graphIndex = (uint) i;
                    }
                }
            }
            this.userConnections = sr.DeserializeUserConnections();
            sr.DeserializeExtraInfo();
            sr.PostDeserialization();
        }

        public void DeserializeGraphsPartAdditive(AstarSerializer sr)
        {
            if (this.graphs == null)
            {
                this.graphs = new NavGraph[0];
            }
            if (this.userConnections == null)
            {
                this.userConnections = new UserConnection[0];
            }
            List<NavGraph> list = new List<NavGraph>(this.graphs);
            list.AddRange(sr.DeserializeGraphs());
            this.graphs = list.ToArray();
            if (this.graphs != null)
            {
                for (int j = 0; j < this.graphs.Length; j++)
                {
                    if (this.graphs[j] != null)
                    {
                        this.graphs[j].graphIndex = (uint) j;
                    }
                }
            }
            List<UserConnection> list2 = new List<UserConnection>(this.userConnections);
            list2.AddRange(sr.DeserializeUserConnections());
            this.userConnections = list2.ToArray();
            sr.DeserializeNodes();
            <DeserializeGraphsPartAdditive>c__AnonStoreyAE yae = new <DeserializeGraphsPartAdditive>c__AnonStoreyAE {
                i = 0
            };
            while (yae.i < this.graphs.Length)
            {
                if (this.graphs[yae.i] != null)
                {
                    this.graphs[yae.i].GetNodes(new GraphNodeDelegateCancelable(yae.<>m__1));
                }
                yae.i++;
            }
            sr.DeserializeExtraInfo();
            sr.PostDeserialization();
            for (int i = 0; i < this.graphs.Length; i++)
            {
                for (int k = i + 1; k < this.graphs.Length; k++)
                {
                    if (((this.graphs[i] != null) && (this.graphs[k] != null)) && (this.graphs[i].guid == this.graphs[k].guid))
                    {
                        UnityEngine.Debug.LogWarning("Guid Conflict when importing graphs additively. Imported graph will get a new Guid.\nThis message is (relatively) harmless.");
                        this.graphs[i].guid = Pathfinding.Util.Guid.NewGuid();
                        break;
                    }
                }
            }
        }

        public NavGraph FindGraphOfType(System.Type type)
        {
            if (this.graphs != null)
            {
                for (int i = 0; i < this.graphs.Length; i++)
                {
                    if ((this.graphs[i] != null) && object.Equals(this.graphs[i].GetType(), type))
                    {
                        return this.graphs[i];
                    }
                }
            }
            return null;
        }

        [DebuggerHidden]
        public IEnumerable FindGraphsOfType(System.Type type)
        {
            return new <FindGraphsOfType>c__Iterator4 { type = type, <$>type = type, <>f__this = this, $PC = -2 };
        }

        public void FindGraphTypes()
        {
            System.Type[] types = Assembly.GetAssembly(typeof(AstarPath)).GetTypes();
            List<System.Type> list = new List<System.Type>();
            foreach (System.Type type in types)
            {
                for (System.Type type2 = type.BaseType; type2 != null; type2 = type2.BaseType)
                {
                    if (object.Equals(type2, typeof(NavGraph)))
                    {
                        list.Add(type);
                        break;
                    }
                }
            }
            this.graphTypes = list.ToArray();
        }

        public byte[] GetData()
        {
            return this.data;
        }

        public static NavGraph GetGraph(GraphNode node)
        {
            if (node == null)
            {
                return null;
            }
            AstarPath active = AstarPath.active;
            if (active == null)
            {
                return null;
            }
            AstarData astarData = active.astarData;
            if (astarData == null)
            {
                return null;
            }
            if (astarData.graphs == null)
            {
                return null;
            }
            uint graphIndex = node.GraphIndex;
            if (graphIndex >= astarData.graphs.Length)
            {
                return null;
            }
            return astarData.graphs[graphIndex];
        }

        public int GetGraphIndex(NavGraph graph)
        {
            if (graph == null)
            {
                throw new ArgumentNullException("graph");
            }
            if (this.graphs != null)
            {
                for (int i = 0; i < this.graphs.Length; i++)
                {
                    if (graph == this.graphs[i])
                    {
                        return i;
                    }
                }
            }
            UnityEngine.Debug.LogError("Graph doesn't exist");
            return -1;
        }

        public System.Type GetGraphType(string type)
        {
            for (int i = 0; i < this.graphTypes.Length; i++)
            {
                if (this.graphTypes[i].Name == type)
                {
                    return this.graphTypes[i];
                }
            }
            return null;
        }

        public GraphNode GetNode(int graphIndex, int nodeIndex)
        {
            return this.GetNode(graphIndex, nodeIndex, this.graphs);
        }

        public GraphNode GetNode(int graphIndex, int nodeIndex, NavGraph[] graphs)
        {
            throw new NotImplementedException();
        }

        [DebuggerHidden]
        public IEnumerable GetRaycastableGraphs()
        {
            return new <GetRaycastableGraphs>c__Iterator6 { <>f__this = this, $PC = -2 };
        }

        [DebuggerHidden]
        public IEnumerable GetUpdateableGraphs()
        {
            return new <GetUpdateableGraphs>c__Iterator5 { <>f__this = this, $PC = -2 };
        }

        public NavGraph GuidToGraph(Pathfinding.Util.Guid guid)
        {
            if (this.graphs != null)
            {
                for (int i = 0; i < this.graphs.Length; i++)
                {
                    if ((this.graphs[i] != null) && (this.graphs[i].guid == guid))
                    {
                        return this.graphs[i];
                    }
                }
            }
            return null;
        }

        public int GuidToIndex(Pathfinding.Util.Guid guid)
        {
            if (this.graphs != null)
            {
                for (int i = 0; i < this.graphs.Length; i++)
                {
                    if ((this.graphs[i] != null) && (this.graphs[i].guid == guid))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public void LoadFromCache()
        {
            AstarPath.active.BlockUntilPathQueueBlocked();
            if ((this.data_cachedStartup != null) && (this.data_cachedStartup.Length > 0))
            {
                this.DeserializeGraphs(this.data_cachedStartup);
                GraphModifier.TriggerEvent(GraphModifier.EventType.PostCacheLoad);
            }
            else
            {
                UnityEngine.Debug.LogError("Can't load from cache since the cache is empty");
            }
        }

        public void OnDestroy()
        {
            this.ClearGraphs();
        }

        public bool RemoveGraph(NavGraph graph)
        {
            graph.SafeOnDestroy();
            int index = 0;
            while (index < this.graphs.Length)
            {
                if (this.graphs[index] == graph)
                {
                    break;
                }
                index++;
            }
            if (index == this.graphs.Length)
            {
                return false;
            }
            this.graphs[index] = null;
            this.UpdateShortcuts();
            return true;
        }

        public void SaveCacheData(SerializeSettings settings)
        {
            this.data_cachedStartup = this.SerializeGraphs(settings);
            this.cacheStartup = true;
        }

        public byte[] SerializeGraphs()
        {
            return this.SerializeGraphs(SerializeSettings.Settings);
        }

        public byte[] SerializeGraphs(SerializeSettings settings)
        {
            uint num;
            return this.SerializeGraphs(settings, out num);
        }

        public byte[] SerializeGraphs(SerializeSettings settings, out uint checksum)
        {
            AstarPath.active.BlockUntilPathQueueBlocked();
            AstarSerializer sr = new AstarSerializer(this, settings);
            sr.OpenSerialize();
            this.SerializeGraphsPart(sr);
            byte[] buffer = sr.CloseSerialize();
            checksum = sr.GetChecksum();
            return buffer;
        }

        public void SerializeGraphsPart(AstarSerializer sr)
        {
            sr.SerializeGraphs(this.graphs);
            sr.SerializeUserConnections(this.userConnections);
            sr.SerializeNodes();
            sr.SerializeExtraInfo();
        }

        public void SetData(byte[] data, uint checksum)
        {
            this.data = data;
            this.dataChecksum = checksum;
        }

        public void UpdateShortcuts()
        {
            this.navmesh = (NavMeshGraph) this.FindGraphOfType(typeof(NavMeshGraph));
            this.gridGraph = (GridGraph) this.FindGraphOfType(typeof(GridGraph));
            this.pointGraph = (PointGraph) this.FindGraphOfType(typeof(PointGraph));
            this.recastGraph = (RecastGraph) this.FindGraphOfType(typeof(RecastGraph));
        }

        public AstarPath active
        {
            get
            {
                return AstarPath.active;
            }
        }

        [CompilerGenerated]
        private sealed class <DeserializeGraphsPartAdditive>c__AnonStoreyAE
        {
            internal int i;

            internal bool <>m__1(GraphNode node)
            {
                node.GraphIndex = (uint) this.i;
                return true;
            }
        }

        [CompilerGenerated]
        private sealed class <FindGraphsOfType>c__Iterator4 : IEnumerator, IEnumerable, IDisposable, IEnumerator<object>, IEnumerable<object>
        {
            internal object $current;
            internal int $PC;
            internal System.Type <$>type;
            internal AstarData <>f__this;
            internal int <i>__0;
            internal System.Type type;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.<>f__this.graphs != null)
                        {
                            this.<i>__0 = 0;
                            while (this.<i>__0 < this.<>f__this.graphs.Length)
                            {
                                if ((this.<>f__this.graphs[this.<i>__0] != null) && object.Equals(this.<>f__this.graphs[this.<i>__0].GetType(), this.type))
                                {
                                    this.$current = this.<>f__this.graphs[this.<i>__0];
                                    this.$PC = 1;
                                    return true;
                                }
                            Label_00A4:
                                this.<i>__0++;
                            }
                            this.$PC = -1;
                            break;
                        }
                        break;

                    case 1:
                        goto Label_00A4;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<object> IEnumerable<object>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new AstarData.<FindGraphsOfType>c__Iterator4 { <>f__this = this.<>f__this, type = this.<$>type };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetRaycastableGraphs>c__Iterator6 : IEnumerator, IEnumerable, IDisposable, IEnumerator<object>, IEnumerable<object>
        {
            internal object $current;
            internal int $PC;
            internal AstarData <>f__this;
            internal int <i>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.<>f__this.graphs != null)
                        {
                            this.<i>__0 = 0;
                            while (this.<i>__0 < this.<>f__this.graphs.Length)
                            {
                                if ((this.<>f__this.graphs[this.<i>__0] != null) && (this.<>f__this.graphs[this.<i>__0] is IRaycastableGraph))
                                {
                                    this.$current = this.<>f__this.graphs[this.<i>__0];
                                    this.$PC = 1;
                                    return true;
                                }
                            Label_0099:
                                this.<i>__0++;
                            }
                            this.$PC = -1;
                            break;
                        }
                        break;

                    case 1:
                        goto Label_0099;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<object> IEnumerable<object>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new AstarData.<GetRaycastableGraphs>c__Iterator6 { <>f__this = this.<>f__this };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetUpdateableGraphs>c__Iterator5 : IEnumerator, IEnumerable, IDisposable, IEnumerator<object>, IEnumerable<object>
        {
            internal object $current;
            internal int $PC;
            internal AstarData <>f__this;
            internal int <i>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.<>f__this.graphs != null)
                        {
                            this.<i>__0 = 0;
                            while (this.<i>__0 < this.<>f__this.graphs.Length)
                            {
                                if ((this.<>f__this.graphs[this.<i>__0] != null) && (this.<>f__this.graphs[this.<i>__0] is IUpdatableGraph))
                                {
                                    this.$current = this.<>f__this.graphs[this.<i>__0];
                                    this.$PC = 1;
                                    return true;
                                }
                            Label_0099:
                                this.<i>__0++;
                            }
                            this.$PC = -1;
                            break;
                        }
                        break;

                    case 1:
                        goto Label_0099;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<object> IEnumerable<object>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new AstarData.<GetUpdateableGraphs>c__Iterator5 { <>f__this = this.<>f__this };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

