﻿namespace Pathfinding
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class FleePath : RandomPath
    {
        public FleePath()
        {
        }

        [Obsolete("Please use the Construct method instead")]
        public FleePath(Vector3 start, Vector3 avoid, int length, OnPathDelegate callbackDelegate = null) : base(start, length, callbackDelegate)
        {
            throw new Exception("Please use the Construct method instead");
        }

        public static FleePath Construct(Vector3 start, Vector3 avoid, int searchLength, OnPathDelegate callback = null)
        {
            FleePath path = PathPool<FleePath>.GetPath();
            path.Setup(start, avoid, searchLength, callback);
            return path;
        }

        protected override void Recycle()
        {
            PathPool<FleePath>.Recycle(this);
        }

        protected void Setup(Vector3 start, Vector3 avoid, int searchLength, OnPathDelegate callback)
        {
            base.Setup(start, searchLength, callback);
            base.aim = avoid - start;
            base.aim = (Vector3) (base.aim * 10f);
            base.aim = start - base.aim;
        }
    }
}

