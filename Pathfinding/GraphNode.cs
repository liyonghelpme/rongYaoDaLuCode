﻿namespace Pathfinding
{
    using Pathfinding.Serialization;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public abstract class GraphNode
    {
        protected uint flags;
        private const uint FlagsAreaMask = 0x1fffe;
        private const int FlagsAreaOffset = 1;
        private const uint FlagsGraphMask = 0x60000;
        private const int FlagsGraphOffset = 0x11;
        private const uint FlagsTagMask = 0xf80000;
        private const int FlagsTagOffset = 0x13;
        private const uint FlagsWalkableMask = 1;
        private const int FlagsWalkableOffset = 0;
        public const uint MaxGraphCount = 3;
        public const uint MaxRegionCount = 0xffff;
        private int nodeIndex;
        private uint penalty;
        public Int3 position;

        public GraphNode(AstarPath astar)
        {
            if (astar == null)
            {
                throw new Exception("No active AstarPath object to bind to");
            }
            this.nodeIndex = astar.GetNewNodeIndex();
            astar.InitializeNode(this);
        }

        public abstract void AddConnection(GraphNode node, uint cost);
        public abstract void ClearConnections(bool alsoReverse);
        public virtual bool ContainsConnection(GraphNode node)
        {
            <ContainsConnection>c__AnonStoreyB6 yb = new <ContainsConnection>c__AnonStoreyB6 {
                node = node,
                contains = false
            };
            this.GetConnections(new GraphNodeDelegate(yb.<>m__D));
            return yb.contains;
        }

        public virtual void DeserializeNode(GraphSerializationContext ctx)
        {
            this.Penalty = ctx.reader.ReadUInt32();
            this.Flags = ctx.reader.ReadUInt32();
        }

        public virtual void DeserializeReferences(GraphSerializationContext ctx)
        {
        }

        public void Destroy()
        {
            if (this.nodeIndex != -1)
            {
                this.ClearConnections(true);
                if (AstarPath.active != null)
                {
                    AstarPath.active.DestroyNode(this);
                }
                this.nodeIndex = -1;
            }
        }

        public virtual void FloodFill(Stack<GraphNode> stack, uint region)
        {
            <FloodFill>c__AnonStoreyB5 yb = new <FloodFill>c__AnonStoreyB5 {
                region = region,
                stack = stack
            };
            this.GetConnections(new GraphNodeDelegate(yb.<>m__C));
        }

        public abstract void GetConnections(GraphNodeDelegate del);
        public virtual bool GetPortal(GraphNode other, List<Vector3> left, List<Vector3> right, bool backwards)
        {
            return false;
        }

        public abstract void Open(Path path, PathNode pathNode, PathHandler handler);
        public virtual void RecalculateConnectionCosts()
        {
        }

        public abstract void RemoveConnection(GraphNode node);
        public virtual void SerializeNode(GraphSerializationContext ctx)
        {
            ctx.writer.Write(this.Penalty);
            ctx.writer.Write(this.Flags);
        }

        public virtual void SerializeReferences(GraphSerializationContext ctx)
        {
        }

        public void UpdateG(Path path, PathNode pathNode)
        {
            pathNode.G = (pathNode.parent.G + pathNode.cost) + path.GetTraversalCost(this);
        }

        public virtual void UpdateRecursiveG(Path path, PathNode pathNode, PathHandler handler)
        {
            <UpdateRecursiveG>c__AnonStoreyB4 yb = new <UpdateRecursiveG>c__AnonStoreyB4 {
                handler = handler,
                pathNode = pathNode,
                path = path
            };
            this.UpdateG(yb.path, yb.pathNode);
            yb.handler.PushNode(yb.pathNode);
            this.GetConnections(new GraphNodeDelegate(yb.<>m__B));
        }

        public uint Area
        {
            get
            {
                return (uint) ((this.flags & 0x1fffe) >> 1);
            }
            set
            {
                this.flags = (this.flags & 0xfffe0001) | (value << 1);
            }
        }

        public bool Destroyed
        {
            get
            {
                return (this.nodeIndex == -1);
            }
        }

        public uint Flags
        {
            get
            {
                return this.flags;
            }
            set
            {
                this.flags = value;
            }
        }

        [Obsolete("This attribute is deprecated. Please use .GraphIndex (with a capital G)")]
        public uint graphIndex
        {
            get
            {
                return this.GraphIndex;
            }
            set
            {
                this.GraphIndex = value;
            }
        }

        public uint GraphIndex
        {
            get
            {
                return (uint) ((this.flags & 0x60000) >> 0x11);
            }
            set
            {
                this.flags = (this.flags & 0xfff9ffff) | (value << 0x11);
            }
        }

        public int NodeIndex
        {
            get
            {
                return this.nodeIndex;
            }
        }

        public uint Penalty
        {
            get
            {
                return this.penalty;
            }
            set
            {
                if (value > 0xfffff)
                {
                    Debug.LogWarning("Very high penalty applied. Are you sure negative values haven't underflowed?\nPenalty values this high could with long paths cause overflows and in some cases infinity loops because of that.\nPenalty value applied: " + value);
                }
                this.penalty = value;
            }
        }

        [Obsolete("This attribute is deprecated. Please use .position (not a capital P)")]
        public Int3 Position
        {
            get
            {
                return this.position;
            }
        }

        public uint Tag
        {
            get
            {
                return (uint) ((this.flags & 0xf80000) >> 0x13);
            }
            set
            {
                this.flags = (this.flags & 0xff07ffff) | (value << 0x13);
            }
        }

        [Obsolete("This attribute is deprecated. Please use .Tag (with a capital T)")]
        public uint tags
        {
            get
            {
                return this.Tag;
            }
            set
            {
                this.Tag = value;
            }
        }

        [Obsolete("This attribute is deprecated. Please use .Walkable (with a capital W)")]
        public bool walkable
        {
            get
            {
                return this.Walkable;
            }
            set
            {
                this.Walkable = value;
            }
        }

        public bool Walkable
        {
            get
            {
                return ((this.flags & 1) != 0);
            }
            set
            {
                this.flags = (this.flags & 0xfffffffe) | (!value ? 0 : 1);
            }
        }

        [CompilerGenerated]
        private sealed class <ContainsConnection>c__AnonStoreyB6
        {
            internal bool contains;
            internal GraphNode node;

            internal void <>m__D(GraphNode n)
            {
                if (n == this.node)
                {
                    this.contains = true;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <FloodFill>c__AnonStoreyB5
        {
            internal uint region;
            internal Stack<GraphNode> stack;

            internal void <>m__C(GraphNode other)
            {
                if (other.Area != this.region)
                {
                    other.Area = this.region;
                    this.stack.Push(other);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateRecursiveG>c__AnonStoreyB4
        {
            internal PathHandler handler;
            internal Path path;
            internal PathNode pathNode;

            internal void <>m__B(GraphNode other)
            {
                PathNode pathNode = this.handler.GetPathNode(other);
                if ((pathNode.parent == this.pathNode) && (pathNode.pathID == this.handler.PathID))
                {
                    other.UpdateRecursiveG(this.path, pathNode, this.handler);
                }
            }
        }
    }
}

