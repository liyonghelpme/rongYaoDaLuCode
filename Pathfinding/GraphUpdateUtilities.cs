﻿namespace Pathfinding
{
    using Pathfinding.Util;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class GraphUpdateUtilities
    {
        [Obsolete("This function has been moved to Pathfinding.Util.PathUtilities. Please use the version in that class")]
        public static bool IsPathPossible(List<GraphNode> nodes)
        {
            uint area = nodes[0].Area;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (!nodes[i].Walkable || (nodes[i].Area != area))
                {
                    return false;
                }
            }
            return true;
        }

        [Obsolete("This function has been moved to Pathfinding.Util.PathUtilities. Please use the version in that class")]
        public static bool IsPathPossible(GraphNode n1, GraphNode n2)
        {
            return ((n1.Walkable && n2.Walkable) && (n1.Area == n2.Area));
        }

        public static bool UpdateGraphsNoBlock(GraphUpdateObject guo, List<GraphNode> nodes, bool alwaysRevert = false)
        {
            <UpdateGraphsNoBlock>c__AnonStoreyC8 yc = new <UpdateGraphsNoBlock>c__AnonStoreyC8 {
                guo = guo,
                nodes = nodes,
                alwaysRevert = alwaysRevert
            };
            for (int i = 0; i < yc.nodes.Count; i++)
            {
                if (!yc.nodes[i].Walkable)
                {
                    return false;
                }
            }
            yc.guo.trackChangedNodes = true;
            yc.worked = true;
            AstarPath.RegisterSafeUpdate(new OnVoidDelegate(yc.<>m__24), true);
            AstarPath.active.FlushThreadSafeCallbacks();
            yc.guo.trackChangedNodes = false;
            return yc.worked;
        }

        public static bool UpdateGraphsNoBlock(GraphUpdateObject guo, GraphNode node1, GraphNode node2, bool alwaysRevert = false)
        {
            List<GraphNode> nodes = ListPool<GraphNode>.Claim();
            nodes.Add(node1);
            nodes.Add(node2);
            bool flag = UpdateGraphsNoBlock(guo, nodes, alwaysRevert);
            ListPool<GraphNode>.Release(nodes);
            return flag;
        }

        [CompilerGenerated]
        private sealed class <UpdateGraphsNoBlock>c__AnonStoreyC8
        {
            internal bool alwaysRevert;
            internal GraphUpdateObject guo;
            internal List<GraphNode> nodes;
            internal bool worked;

            internal void <>m__24()
            {
                AstarPath.active.UpdateGraphs(this.guo);
                AstarPath.active.QueueGraphUpdates();
                AstarPath.active.FlushGraphUpdates();
                this.worked = this.worked && PathUtilities.IsPathPossible(this.nodes);
                if (!this.worked || this.alwaysRevert)
                {
                    this.guo.RevertFromBackup();
                    AstarPath.active.FloodFill();
                }
            }
        }
    }
}

