﻿namespace Pathfinding
{
    using System;
    using System.Collections.Generic;

    public interface IFunnelGraph
    {
        void AddPortal(GraphNode n1, GraphNode n2, List<Vector3> left, List<Vector3> right);
        void BuildFunnelCorridor(List<GraphNode> path, int sIndex, int eIndex, List<Vector3> left, List<Vector3> right);
    }
}

