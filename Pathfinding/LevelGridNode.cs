﻿namespace Pathfinding
{
    using Pathfinding.Serialization;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class LevelGridNode : GraphNode
    {
        private static LayerGridGraph[] _gridGraphs = new LayerGridGraph[0];
        public const int ConnectionMask = 0xff;
        private const int ConnectionStride = 8;
        protected uint gridConnections;
        protected ushort gridFlags;
        private const int GridFlagsWalkableErosionMask = 0x100;
        private const int GridFlagsWalkableErosionOffset = 8;
        private const int GridFlagsWalkableTmpMask = 0x200;
        private const int GridFlagsWalkableTmpOffset = 9;
        protected static LayerGridGraph[] gridGraphs;
        public const int MaxLayerCount = 0xff;
        public const int NoConnection = 0xff;
        protected int nodeInGridIndex;

        public LevelGridNode(AstarPath astar) : base(astar)
        {
        }

        public override void AddConnection(GraphNode node, uint cost)
        {
            throw new NotImplementedException("Layered Grid Nodes do not have support for adding manual connections");
        }

        public override void ClearConnections(bool alsoReverse)
        {
            if (alsoReverse)
            {
                LayerGridGraph gridGraph = GetGridGraph(base.GraphIndex);
                int[] neighbourOffsets = gridGraph.neighbourOffsets;
                LevelGridNode[] nodes = gridGraph.nodes;
                for (int i = 0; i < 4; i++)
                {
                    int connectionValue = this.GetConnectionValue(i);
                    if (connectionValue != 0xff)
                    {
                        LevelGridNode node = nodes[(this.NodeInGridIndex + neighbourOffsets[i]) + ((gridGraph.width * gridGraph.depth) * connectionValue)];
                        if (node != null)
                        {
                            node.SetConnectionValue((i >= 4) ? 7 : ((i + 2) % 4), 0xff);
                        }
                    }
                }
            }
            this.ResetAllGridConnections();
        }

        public override void DeserializeNode(GraphSerializationContext ctx)
        {
            base.DeserializeNode(ctx);
            base.position = new Int3(ctx.reader.ReadInt32(), ctx.reader.ReadInt32(), ctx.reader.ReadInt32());
            this.gridFlags = ctx.reader.ReadUInt16();
            this.gridConnections = ctx.reader.ReadUInt32();
        }

        public override void FloodFill(Stack<GraphNode> stack, uint region)
        {
            int nodeInGridIndex = this.NodeInGridIndex;
            LayerGridGraph gridGraph = GetGridGraph(base.GraphIndex);
            int[] neighbourOffsets = gridGraph.neighbourOffsets;
            LevelGridNode[] nodes = gridGraph.nodes;
            for (int i = 0; i < 4; i++)
            {
                int connectionValue = this.GetConnectionValue(i);
                if (connectionValue != 0xff)
                {
                    LevelGridNode t = nodes[(nodeInGridIndex + neighbourOffsets[i]) + ((gridGraph.width * gridGraph.depth) * connectionValue)];
                    if ((t != null) && (t.Area != region))
                    {
                        t.Area = region;
                        stack.Push(t);
                    }
                }
            }
        }

        public bool GetConnection(int i)
        {
            return (((this.gridConnections >> (i * 8)) & 0xff) != 0xff);
        }

        public override void GetConnections(GraphNodeDelegate del)
        {
            int nodeInGridIndex = this.NodeInGridIndex;
            LayerGridGraph gridGraph = GetGridGraph(base.GraphIndex);
            int[] neighbourOffsets = gridGraph.neighbourOffsets;
            LevelGridNode[] nodes = gridGraph.nodes;
            for (int i = 0; i < 4; i++)
            {
                int connectionValue = this.GetConnectionValue(i);
                if (connectionValue != 0xff)
                {
                    LevelGridNode node = nodes[(nodeInGridIndex + neighbourOffsets[i]) + ((gridGraph.width * gridGraph.depth) * connectionValue)];
                    if (node != null)
                    {
                        del(node);
                    }
                }
            }
        }

        public int GetConnectionValue(int dir)
        {
            return (((int) (this.gridConnections >> (dir * 8))) & 0xff);
        }

        public static LayerGridGraph GetGridGraph(uint graphIndex)
        {
            return _gridGraphs[graphIndex];
        }

        public override bool GetPortal(GraphNode other, List<Vector3> left, List<Vector3> right, bool backwards)
        {
            if (backwards)
            {
                return true;
            }
            LayerGridGraph gridGraph = GetGridGraph(base.GraphIndex);
            int[] neighbourOffsets = gridGraph.neighbourOffsets;
            LevelGridNode[] nodes = gridGraph.nodes;
            int nodeInGridIndex = this.NodeInGridIndex;
            for (int i = 0; i < 4; i++)
            {
                int connectionValue = this.GetConnectionValue(i);
                if ((connectionValue != 0xff) && (other == nodes[(nodeInGridIndex + neighbourOffsets[i]) + ((gridGraph.width * gridGraph.depth) * connectionValue)]))
                {
                    Vector3 vector = (Vector3) (((Vector3) (base.position + other.position)) * 0.5f);
                    Vector3 vector2 = Vector3.Cross(gridGraph.collision.up, (Vector3) (other.position - base.position));
                    vector2.Normalize();
                    vector2 = (Vector3) (vector2 * (gridGraph.nodeSize * 0.5f));
                    left.Add(vector - vector2);
                    right.Add(vector + vector2);
                    return true;
                }
            }
            return false;
        }

        public bool HasAnyGridConnections()
        {
            return (this.gridConnections != uint.MaxValue);
        }

        public override void Open(Path path, PathNode pathNode, PathHandler handler)
        {
            LayerGridGraph gridGraph = GetGridGraph(base.GraphIndex);
            int[] neighbourOffsets = gridGraph.neighbourOffsets;
            uint[] neighbourCosts = gridGraph.neighbourCosts;
            LevelGridNode[] nodes = gridGraph.nodes;
            int nodeInGridIndex = this.NodeInGridIndex;
            for (int i = 0; i < 4; i++)
            {
                int connectionValue = this.GetConnectionValue(i);
                if (connectionValue != 0xff)
                {
                    GraphNode node = nodes[(nodeInGridIndex + neighbourOffsets[i]) + ((gridGraph.width * gridGraph.depth) * connectionValue)];
                    if (path.CanTraverse(node))
                    {
                        PathNode node2 = handler.GetPathNode(node);
                        if (node2.pathID != handler.PathID)
                        {
                            node2.parent = pathNode;
                            node2.pathID = handler.PathID;
                            node2.cost = neighbourCosts[i];
                            node2.H = path.CalculateHScore(node);
                            node.UpdateG(path, node2);
                            handler.PushNode(node2);
                        }
                        else
                        {
                            uint num4 = neighbourCosts[i];
                            if (((pathNode.G + num4) + path.GetTraversalCost(node)) < node2.G)
                            {
                                node2.cost = num4;
                                node2.parent = pathNode;
                                node.UpdateRecursiveG(path, node2, handler);
                            }
                            else if (((node2.G + num4) + path.GetTraversalCost(this)) < pathNode.G)
                            {
                                pathNode.parent = node2;
                                pathNode.cost = num4;
                                this.UpdateRecursiveG(path, pathNode, handler);
                            }
                        }
                    }
                }
            }
        }

        public override void RemoveConnection(GraphNode node)
        {
            throw new NotImplementedException("Layered Grid Nodes do not have support for adding manual connections");
        }

        public void ResetAllGridConnections()
        {
            this.gridConnections = uint.MaxValue;
        }

        public override void SerializeNode(GraphSerializationContext ctx)
        {
            base.SerializeNode(ctx);
            ctx.writer.Write(this.position.x);
            ctx.writer.Write(this.position.y);
            ctx.writer.Write(this.position.z);
            ctx.writer.Write(this.gridFlags);
            ctx.writer.Write(this.gridConnections);
        }

        public void SetConnectionValue(int dir, int value)
        {
            this.gridConnections = (uint) ((this.gridConnections & ~(((int) 0xff) << (dir * 8))) | (value << (dir * 8)));
        }

        public static void SetGridGraph(int graphIndex, LayerGridGraph graph)
        {
            if (_gridGraphs.Length <= graphIndex)
            {
                LayerGridGraph[] graphArray = new LayerGridGraph[graphIndex + 1];
                for (int i = 0; i < _gridGraphs.Length; i++)
                {
                    graphArray[i] = _gridGraphs[i];
                }
                _gridGraphs = graphArray;
            }
            _gridGraphs[graphIndex] = graph;
        }

        public void SetPosition(Int3 position)
        {
            base.position = position;
        }

        public override void UpdateRecursiveG(Path path, PathNode pathNode, PathHandler handler)
        {
            handler.PushNode(pathNode);
            base.UpdateG(path, pathNode);
            LayerGridGraph gridGraph = GetGridGraph(base.GraphIndex);
            int[] neighbourOffsets = gridGraph.neighbourOffsets;
            LevelGridNode[] nodes = gridGraph.nodes;
            int nodeInGridIndex = this.NodeInGridIndex;
            for (int i = 0; i < 4; i++)
            {
                int connectionValue = this.GetConnectionValue(i);
                if (connectionValue != 0xff)
                {
                    LevelGridNode node = nodes[(nodeInGridIndex + neighbourOffsets[i]) + ((gridGraph.width * gridGraph.depth) * connectionValue)];
                    PathNode node2 = handler.GetPathNode(node);
                    if (((node2 != null) && (node2.parent == pathNode)) && (node2.pathID == handler.PathID))
                    {
                        node.UpdateRecursiveG(path, node2, handler);
                    }
                }
            }
        }

        public int NodeInGridIndex
        {
            get
            {
                return this.nodeInGridIndex;
            }
            set
            {
                this.nodeInGridIndex = value;
            }
        }

        public bool TmpWalkable
        {
            get
            {
                return ((this.gridFlags & 0x200) != 0);
            }
            set
            {
                this.gridFlags = (ushort) ((this.gridFlags & -513) | (!value ? 0 : 0x200));
            }
        }

        public bool WalkableErosion
        {
            get
            {
                return ((this.gridFlags & 0x100) != 0);
            }
            set
            {
                this.gridFlags = (ushort) ((this.gridFlags & -257) | (!value ? 0 : 0x100));
            }
        }
    }
}

