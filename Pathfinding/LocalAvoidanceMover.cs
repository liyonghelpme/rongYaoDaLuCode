﻿namespace Pathfinding
{
    using System;
    using UnityEngine;

    [Obsolete("Use the RVO system instead"), RequireComponent(typeof(LocalAvoidance))]
    public class LocalAvoidanceMover : MonoBehaviour
    {
        private LocalAvoidance controller;
        public float speed = 2f;
        private Vector3 targetPoint;
        public float targetPointDist = 10f;

        private void Start()
        {
            this.targetPoint = ((Vector3) (base.transform.forward * this.targetPointDist)) + base.transform.position;
            this.controller = base.GetComponent<LocalAvoidance>();
        }

        private void Update()
        {
            if (this.controller != null)
            {
                Vector3 vector = this.targetPoint - base.transform.position;
                this.controller.SimpleMove((Vector3) (vector.normalized * this.speed));
            }
        }
    }
}

