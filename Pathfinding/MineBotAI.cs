﻿namespace Pathfinding
{
    using System;
    using UnityEngine;

    [RequireComponent(typeof(Seeker))]
    public class MineBotAI : AIPath
    {
        public Animation anim;
        public float animationSpeed = 0.2f;
        public GameObject endOfPathEffect;
        protected Vector3 lastTarget;
        public float sleepVelocity = 0.4f;

        public override Vector3 GetFeetPosition()
        {
            return base.tr.position;
        }

        public override void OnTargetReached()
        {
            if ((this.endOfPathEffect != null) && (Vector3.Distance(base.tr.position, this.lastTarget) > 1f))
            {
                UnityEngine.Object.Instantiate(this.endOfPathEffect, base.tr.position, base.tr.rotation);
                this.lastTarget = base.tr.position;
            }
        }

        public void Start()
        {
            this.anim["forward"].layer = 10;
            this.anim.Play("awake");
            this.anim.Play("forward");
            this.anim["awake"].wrapMode = WrapMode.Once;
            this.anim["awake"].speed = 0f;
            this.anim["awake"].normalizedTime = 1f;
            base.Start();
        }

        protected void Update()
        {
            Vector3 zero;
            if (base.canMove)
            {
                Vector3 speed = base.CalculateVelocity(this.GetFeetPosition());
                this.RotateTowards(base.targetDirection);
                speed.y = 0f;
                if (speed.sqrMagnitude <= (this.sleepVelocity * this.sleepVelocity))
                {
                    speed = Vector3.zero;
                }
                if (base.navController != null)
                {
                    zero = Vector3.zero;
                }
                else if (base.controller != null)
                {
                    base.controller.SimpleMove(speed);
                    zero = base.controller.velocity;
                }
                else
                {
                    Debug.LogWarning("No NavmeshController or CharacterController attached to GameObject");
                    zero = Vector3.zero;
                }
            }
            else
            {
                zero = Vector3.zero;
            }
            Vector3 vector3 = base.tr.InverseTransformDirection(zero);
            vector3.y = 0f;
            if (zero.sqrMagnitude <= (this.sleepVelocity * this.sleepVelocity))
            {
                this.anim.Blend("forward", 0f, 0.2f);
            }
            else
            {
                this.anim.Blend("forward", 1f, 0.2f);
                AnimationState state = this.anim["forward"];
                float z = vector3.z;
                state.speed = z * this.animationSpeed;
            }
        }
    }
}

