﻿namespace Pathfinding
{
    using Pathfinding.Util;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Text;
    using UnityEngine;

    public class MultiTargetPath : ABPath
    {
        public OnPathDelegate[] callbacks;
        public int chosenTarget;
        public int endsFound;
        public HeuristicMode heuristicMode;
        public bool inverted;
        public List<GraphNode>[] nodePaths;
        public Vector3[] originalTargetPoints;
        public bool pathsForAll;
        public int sequentialTarget;
        protected int targetNodeCount;
        public GraphNode[] targetNodes;
        public Vector3[] targetPoints;
        public bool[] targetsFound;
        public List<Vector3>[] vectorPaths;

        public MultiTargetPath()
        {
            this.pathsForAll = true;
            this.chosenTarget = -1;
            this.heuristicMode = HeuristicMode.Sequential;
            this.inverted = true;
        }

        [Obsolete("Please use the Construct method instead")]
        public MultiTargetPath(Vector3[] startPoints, Vector3 target, OnPathDelegate[] callbackDelegates, OnPathDelegate callbackDelegate = null) : this(target, startPoints, callbackDelegates, callbackDelegate)
        {
            this.inverted = true;
        }

        [Obsolete("Please use the Construct method instead")]
        public MultiTargetPath(Vector3 start, Vector3[] targets, OnPathDelegate[] callbackDelegates, OnPathDelegate callbackDelegate = null)
        {
            this.pathsForAll = true;
            this.chosenTarget = -1;
            this.heuristicMode = HeuristicMode.Sequential;
            this.inverted = true;
        }

        public override void CalculateStep(long targetTick)
        {
            for (int i = 0; base.CompleteState == PathCompleteState.NotCalculated; i++)
            {
                base.searchedNodes++;
                if (base.currentR.flag1)
                {
                    for (int j = 0; j < this.targetNodes.Length; j++)
                    {
                        if (!this.targetsFound[j] && (base.currentR.node == this.targetNodes[j]))
                        {
                            this.FoundTarget(base.currentR, j);
                            if (base.CompleteState != PathCompleteState.NotCalculated)
                            {
                                break;
                            }
                        }
                    }
                    if (this.targetNodeCount <= 0)
                    {
                        base.CompleteState = PathCompleteState.Complete;
                        break;
                    }
                }
                base.currentR.node.Open(this, base.currentR, base.pathHandler);
                if (base.pathHandler.HeapEmpty())
                {
                    base.CompleteState = PathCompleteState.Complete;
                    break;
                }
                base.currentR = base.pathHandler.PopNode();
                if (i > 500)
                {
                    if (DateTime.UtcNow.Ticks >= targetTick)
                    {
                        return;
                    }
                    i = 0;
                }
            }
        }

        public static MultiTargetPath Construct(Vector3[] startPoints, Vector3 target, OnPathDelegate[] callbackDelegates, OnPathDelegate callback = null)
        {
            MultiTargetPath path = Construct(target, startPoints, callbackDelegates, callback);
            path.inverted = true;
            return path;
        }

        public static MultiTargetPath Construct(Vector3 start, Vector3[] targets, OnPathDelegate[] callbackDelegates, OnPathDelegate callback = null)
        {
            MultiTargetPath path = PathPool<MultiTargetPath>.GetPath();
            path.Setup(start, targets, callbackDelegates, callback);
            return path;
        }

        public override string DebugString(PathLog logMode)
        {
            if ((logMode == PathLog.None) || (!base.error && (logMode == PathLog.OnlyErrors)))
            {
                return string.Empty;
            }
            StringBuilder debugStringBuilder = base.pathHandler.DebugStringBuilder;
            debugStringBuilder.Length = 0;
            debugStringBuilder.Append(!base.error ? "Path Completed : " : "Path Failed : ");
            debugStringBuilder.Append("Computation Time ");
            debugStringBuilder.Append(this.duration.ToString((logMode != PathLog.Heavy) ? "0.00" : "0.000"));
            debugStringBuilder.Append(" ms Searched Nodes ");
            debugStringBuilder.Append(base.searchedNodes);
            if (!base.error)
            {
                debugStringBuilder.Append("\nLast Found Path Length ");
                debugStringBuilder.Append((base.path != null) ? base.path.Count.ToString() : "Null");
                if (logMode == PathLog.Heavy)
                {
                    debugStringBuilder.Append("\nSearch Iterations " + base.searchIterations);
                    debugStringBuilder.Append("\nPaths (").Append(this.targetsFound.Length).Append("):");
                    for (int i = 0; i < this.targetsFound.Length; i++)
                    {
                        debugStringBuilder.Append("\n\n\tPath " + i).Append(" Found: ").Append(this.targetsFound[i]);
                        GraphNode node = (this.nodePaths[i] != null) ? this.nodePaths[i][this.nodePaths[i].Count - 1] : null;
                        debugStringBuilder.Append("\n\t\tLength: ");
                        debugStringBuilder.Append(this.nodePaths[i].Count);
                        if (node != null)
                        {
                            PathNode pathNode = base.pathHandler.GetPathNode(base.endNode);
                            if (pathNode != null)
                            {
                                debugStringBuilder.Append("\n\t\tEnd Node");
                                debugStringBuilder.Append("\n\t\t\tG: ");
                                debugStringBuilder.Append(pathNode.G);
                                debugStringBuilder.Append("\n\t\t\tH: ");
                                debugStringBuilder.Append(pathNode.H);
                                debugStringBuilder.Append("\n\t\t\tF: ");
                                debugStringBuilder.Append(pathNode.F);
                                debugStringBuilder.Append("\n\t\t\tPoint: ");
                                debugStringBuilder.Append(this.endPoint.ToString());
                                debugStringBuilder.Append("\n\t\t\tGraph: ");
                                debugStringBuilder.Append(base.endNode.GraphIndex);
                            }
                            else
                            {
                                debugStringBuilder.Append("\n\t\tEnd Node: Null");
                            }
                        }
                    }
                    debugStringBuilder.Append("\nStart Node");
                    debugStringBuilder.Append("\n\tPoint: ");
                    debugStringBuilder.Append(this.endPoint.ToString());
                    debugStringBuilder.Append("\n\tGraph: ");
                    debugStringBuilder.Append(base.startNode.GraphIndex);
                    debugStringBuilder.Append("\nBinary Heap size at completion: ");
                    debugStringBuilder.AppendLine((base.pathHandler.GetHeap() != null) ? ((base.pathHandler.GetHeap().numberOfItems - 2)).ToString() : "Null");
                }
            }
            if (base.error)
            {
                debugStringBuilder.Append("\nError: ");
                debugStringBuilder.Append(base.errorLog);
                debugStringBuilder.AppendLine();
            }
            if ((logMode == PathLog.Heavy) && !AstarPath.IsUsingMultithreading)
            {
                debugStringBuilder.Append("\nCallback references ");
                if (base.callback != null)
                {
                    debugStringBuilder.Append(base.callback.Target.GetType().FullName).AppendLine();
                }
                else
                {
                    debugStringBuilder.AppendLine("NULL");
                }
            }
            debugStringBuilder.Append("\nPath Number ");
            debugStringBuilder.Append(base.pathID);
            return debugStringBuilder.ToString();
        }

        public void FoundTarget(PathNode nodeR, int i)
        {
            nodeR.flag1 = false;
            this.Trace(nodeR);
            this.vectorPaths[i] = base.vectorPath;
            this.nodePaths[i] = base.path;
            base.vectorPath = ListPool<Vector3>.Claim();
            base.path = ListPool<GraphNode>.Claim();
            this.targetsFound[i] = true;
            this.targetNodeCount--;
            if (!this.pathsForAll)
            {
                base.CompleteState = PathCompleteState.Complete;
                this.chosenTarget = i;
                this.targetNodeCount = 0;
            }
            else if (this.targetNodeCount <= 0)
            {
                base.CompleteState = PathCompleteState.Complete;
            }
            else if (this.heuristicMode == HeuristicMode.MovingAverage)
            {
                Vector3 zero = Vector3.zero;
                int num = 0;
                for (int j = 0; j < this.targetPoints.Length; j++)
                {
                    if (!this.targetsFound[j])
                    {
                        zero += (Vector3) this.targetNodes[j].position;
                        num++;
                    }
                }
                if (num > 0)
                {
                    zero = (Vector3) (zero / ((float) num));
                }
                base.hTarget = (Int3) zero;
                this.RebuildOpenList();
            }
            else if (this.heuristicMode == HeuristicMode.MovingMidpoint)
            {
                Vector3 rhs = Vector3.zero;
                Vector3 position = Vector3.zero;
                bool flag = false;
                for (int k = 0; k < this.targetPoints.Length; k++)
                {
                    if (!this.targetsFound[k])
                    {
                        if (!flag)
                        {
                            rhs = (Vector3) this.targetNodes[k].position;
                            position = (Vector3) this.targetNodes[k].position;
                            flag = true;
                        }
                        else
                        {
                            rhs = Vector3.Min((Vector3) this.targetNodes[k].position, rhs);
                            position = Vector3.Max((Vector3) this.targetNodes[k].position, position);
                        }
                    }
                }
                Int3 num4 = (Int3) ((rhs + position) * 0.5f);
                base.hTarget = num4;
                this.RebuildOpenList();
            }
            else if ((this.heuristicMode == HeuristicMode.Sequential) && (this.sequentialTarget == i))
            {
                float num5 = 0f;
                for (int m = 0; m < this.targetPoints.Length; m++)
                {
                    if (!this.targetsFound[m])
                    {
                        Int3 num8 = this.targetNodes[m].position - base.startNode.position;
                        float sqrMagnitude = num8.sqrMagnitude;
                        if (sqrMagnitude > num5)
                        {
                            num5 = sqrMagnitude;
                            base.hTarget = (Int3) this.targetPoints[m];
                            this.sequentialTarget = m;
                        }
                    }
                }
                this.RebuildOpenList();
            }
        }

        public override void Initialize()
        {
            for (int i = 0; i < this.targetNodes.Length; i++)
            {
                if (base.startNode == this.targetNodes[i])
                {
                    PathNode pathNode = base.pathHandler.GetPathNode(base.startNode);
                    this.FoundTarget(pathNode, i);
                }
                else if (this.targetNodes[i] != null)
                {
                    base.pathHandler.GetPathNode(this.targetNodes[i]).flag1 = true;
                }
            }
            AstarPath.OnPathPostSearch = (OnPathDelegate) Delegate.Combine(AstarPath.OnPathPostSearch, new OnPathDelegate(this.ResetFlags));
            if (this.targetNodeCount <= 0)
            {
                base.CompleteState = PathCompleteState.Complete;
            }
            else
            {
                PathNode node2 = base.pathHandler.GetPathNode(base.startNode);
                node2.node = base.startNode;
                node2.pathID = base.pathID;
                node2.parent = null;
                node2.cost = 0;
                node2.G = base.GetTraversalCost(base.startNode);
                node2.H = base.CalculateHScore(base.startNode);
                base.startNode.Open(this, node2, base.pathHandler);
                base.searchedNodes++;
                if (base.pathHandler.HeapEmpty())
                {
                    base.LogError("No open points, the start node didn't open any nodes");
                    base.Error();
                }
                else
                {
                    base.currentR = base.pathHandler.PopNode();
                }
            }
        }

        public override void OnEnterPool()
        {
            if (this.vectorPaths != null)
            {
                for (int i = 0; i < this.vectorPaths.Length; i++)
                {
                    if (this.vectorPaths[i] != null)
                    {
                        ListPool<Vector3>.Release(this.vectorPaths[i]);
                    }
                }
            }
            this.vectorPaths = null;
            base.vectorPath = null;
            if (this.nodePaths != null)
            {
                for (int j = 0; j < this.nodePaths.Length; j++)
                {
                    if (this.nodePaths[j] != null)
                    {
                        ListPool<GraphNode>.Release(this.nodePaths[j]);
                    }
                }
            }
            this.nodePaths = null;
            base.path = null;
            base.OnEnterPool();
        }

        public override void Prepare()
        {
            base.nnConstraint.tags = base.enabledTags;
            NNInfo info = AstarPath.active.GetNearest(base.startPoint, base.nnConstraint, base.startHint);
            base.startNode = info.node;
            if (base.startNode == null)
            {
                base.LogError("Could not find start node for multi target path");
                base.Error();
            }
            else if (!base.startNode.Walkable)
            {
                base.LogError("Nearest node to the start point is not walkable");
                base.Error();
            }
            else
            {
                PathNNConstraint nnConstraint = base.nnConstraint as PathNNConstraint;
                if (nnConstraint != null)
                {
                    nnConstraint.SetStart(info.node);
                }
                this.vectorPaths = new List<Vector3>[this.targetPoints.Length];
                this.nodePaths = new List<GraphNode>[this.targetPoints.Length];
                this.targetNodes = new GraphNode[this.targetPoints.Length];
                this.targetsFound = new bool[this.targetPoints.Length];
                this.targetNodeCount = this.targetPoints.Length;
                bool flag = false;
                bool flag2 = false;
                bool flag3 = false;
                for (int i = 0; i < this.targetPoints.Length; i++)
                {
                    NNInfo nearest = AstarPath.active.GetNearest(this.targetPoints[i], base.nnConstraint);
                    this.targetNodes[i] = nearest.node;
                    this.targetPoints[i] = nearest.clampedPosition;
                    if (this.targetNodes[i] != null)
                    {
                        flag3 = true;
                        base.endNode = this.targetNodes[i];
                    }
                    bool flag4 = false;
                    if ((nearest.node != null) && nearest.node.Walkable)
                    {
                        flag = true;
                    }
                    else
                    {
                        flag4 = true;
                    }
                    if ((nearest.node != null) && (nearest.node.Area == base.startNode.Area))
                    {
                        flag2 = true;
                    }
                    else
                    {
                        flag4 = true;
                    }
                    if (flag4)
                    {
                        this.targetsFound[i] = true;
                        this.targetNodeCount--;
                    }
                }
                base.startPoint = info.clampedPosition;
                base.startIntPoint = (Int3) base.startPoint;
                if ((base.startNode == null) || !flag3)
                {
                    base.LogError("Couldn't find close nodes to either the start or the end (start = " + ((base.startNode == null) ? "not found" : "found") + " end = " + (!flag3 ? "none found" : "at least one found") + ")");
                    base.Error();
                }
                else if (!base.startNode.Walkable)
                {
                    base.LogError("The node closest to the start point is not walkable");
                    base.Error();
                }
                else if (!flag)
                {
                    base.LogError("No target nodes were walkable");
                    base.Error();
                }
                else if (!flag2)
                {
                    base.LogError("There are no valid paths to the targets");
                    base.Error();
                }
                else if (this.pathsForAll)
                {
                    if (this.heuristicMode == HeuristicMode.None)
                    {
                        base.heuristic = Heuristic.None;
                        base.heuristicScale = 0f;
                    }
                    else if ((this.heuristicMode == HeuristicMode.Average) || (this.heuristicMode == HeuristicMode.MovingAverage))
                    {
                        Vector3 zero = Vector3.zero;
                        for (int j = 0; j < this.targetNodes.Length; j++)
                        {
                            zero += (Vector3) this.targetNodes[j].position;
                        }
                        zero = (Vector3) (zero / ((float) this.targetNodes.Length));
                        base.hTarget = (Int3) zero;
                    }
                    else if ((this.heuristicMode == HeuristicMode.Midpoint) || (this.heuristicMode == HeuristicMode.MovingMidpoint))
                    {
                        Vector3 rhs = Vector3.zero;
                        Vector3 position = Vector3.zero;
                        bool flag5 = false;
                        for (int k = 0; k < this.targetPoints.Length; k++)
                        {
                            if (!this.targetsFound[k])
                            {
                                if (!flag5)
                                {
                                    rhs = (Vector3) this.targetNodes[k].position;
                                    position = (Vector3) this.targetNodes[k].position;
                                    flag5 = true;
                                }
                                else
                                {
                                    rhs = Vector3.Min((Vector3) this.targetNodes[k].position, rhs);
                                    position = Vector3.Max((Vector3) this.targetNodes[k].position, position);
                                }
                            }
                        }
                        Vector3 vector4 = (Vector3) ((rhs + position) * 0.5f);
                        base.hTarget = (Int3) vector4;
                    }
                    else if (this.heuristicMode == HeuristicMode.Sequential)
                    {
                        float num4 = 0f;
                        for (int m = 0; m < this.targetNodes.Length; m++)
                        {
                            if (!this.targetsFound[m])
                            {
                                Int3 num7 = this.targetNodes[m].position - base.startNode.position;
                                float sqrMagnitude = num7.sqrMagnitude;
                                if (sqrMagnitude > num4)
                                {
                                    num4 = sqrMagnitude;
                                    base.hTarget = (Int3) this.targetPoints[m];
                                    this.sequentialTarget = m;
                                }
                            }
                        }
                    }
                }
                else
                {
                    base.heuristic = Heuristic.None;
                    base.heuristicScale = 0f;
                }
            }
        }

        protected void RebuildOpenList()
        {
            BinaryHeapM heap = base.pathHandler.GetHeap();
            for (int i = 0; i < heap.numberOfItems; i++)
            {
                PathNode node = heap.GetNode(i);
                node.H = base.CalculateHScore(node.node);
            }
            base.pathHandler.RebuildHeap();
        }

        protected override void Recycle()
        {
            PathPool<MultiTargetPath>.Recycle(this);
        }

        public void ResetFlags(Path p)
        {
            AstarPath.OnPathPostSearch = (OnPathDelegate) Delegate.Remove(AstarPath.OnPathPostSearch, new OnPathDelegate(this.ResetFlags));
            if (p != this)
            {
                Debug.LogError("This should have been cleared after it was called on 'this' path. Was it not called? Or did the delegate reset not work?");
            }
            for (int i = 0; i < this.targetNodes.Length; i++)
            {
                if (this.targetNodes[i] != null)
                {
                    base.pathHandler.GetPathNode(this.targetNodes[i]).flag1 = false;
                }
            }
        }

        public override void ReturnPath()
        {
            if (base.error)
            {
                if (this.callbacks != null)
                {
                    for (int i = 0; i < this.callbacks.Length; i++)
                    {
                        if (this.callbacks[i] != null)
                        {
                            this.callbacks[i](this);
                        }
                    }
                }
                if (base.callback != null)
                {
                    base.callback(this);
                }
            }
            else
            {
                bool flag = false;
                Vector3 originalStartPoint = base.originalStartPoint;
                Vector3 startPoint = base.startPoint;
                GraphNode startNode = base.startNode;
                for (int j = 0; j < this.nodePaths.Length; j++)
                {
                    base.path = this.nodePaths[j];
                    if (base.path != null)
                    {
                        base.CompleteState = PathCompleteState.Complete;
                        flag = true;
                    }
                    else
                    {
                        base.CompleteState = PathCompleteState.Error;
                    }
                    if ((this.callbacks != null) && (this.callbacks[j] != null))
                    {
                        base.vectorPath = this.vectorPaths[j];
                        if (this.inverted)
                        {
                            base.endPoint = startPoint;
                            base.endNode = startNode;
                            base.startNode = this.targetNodes[j];
                            base.startPoint = this.targetPoints[j];
                            base.originalEndPoint = originalStartPoint;
                            base.originalStartPoint = this.originalTargetPoints[j];
                        }
                        else
                        {
                            base.endPoint = this.targetPoints[j];
                            base.originalEndPoint = this.originalTargetPoints[j];
                            base.endNode = this.targetNodes[j];
                        }
                        this.callbacks[j](this);
                        this.vectorPaths[j] = base.vectorPath;
                    }
                }
                if (flag)
                {
                    base.CompleteState = PathCompleteState.Complete;
                    if (!this.pathsForAll)
                    {
                        base.path = this.nodePaths[this.chosenTarget];
                        base.vectorPath = this.vectorPaths[this.chosenTarget];
                        if (this.inverted)
                        {
                            base.endPoint = startPoint;
                            base.endNode = startNode;
                            base.startNode = this.targetNodes[this.chosenTarget];
                            base.startPoint = this.targetPoints[this.chosenTarget];
                            base.originalEndPoint = originalStartPoint;
                            base.originalStartPoint = this.originalTargetPoints[this.chosenTarget];
                        }
                        else
                        {
                            base.endPoint = this.targetPoints[this.chosenTarget];
                            base.originalEndPoint = this.originalTargetPoints[this.chosenTarget];
                            base.endNode = this.targetNodes[this.chosenTarget];
                        }
                    }
                }
                else
                {
                    base.CompleteState = PathCompleteState.Error;
                }
                if (base.callback != null)
                {
                    base.callback(this);
                }
            }
        }

        protected void Setup(Vector3 start, Vector3[] targets, OnPathDelegate[] callbackDelegates, OnPathDelegate callback)
        {
            this.inverted = false;
            base.callback = callback;
            this.callbacks = callbackDelegates;
            this.targetPoints = targets;
            base.originalStartPoint = start;
            base.startPoint = start;
            base.startIntPoint = (Int3) start;
            if (targets.Length == 0)
            {
                base.Error();
                base.LogError("No targets were assigned to the MultiTargetPath");
            }
            else
            {
                base.endPoint = targets[0];
                this.originalTargetPoints = new Vector3[this.targetPoints.Length];
                for (int i = 0; i < this.targetPoints.Length; i++)
                {
                    this.originalTargetPoints[i] = this.targetPoints[i];
                }
            }
        }

        protected override void Trace(PathNode node)
        {
            base.Trace(node);
            if (this.inverted)
            {
                int num = base.path.Count / 2;
                for (int i = 0; i < num; i++)
                {
                    GraphNode node2 = base.path[i];
                    base.path[i] = base.path[(base.path.Count - i) - 1];
                    base.path[(base.path.Count - i) - 1] = node2;
                }
                for (int j = 0; j < num; j++)
                {
                    Vector3 vector = base.vectorPath[j];
                    base.vectorPath[j] = base.vectorPath[(base.vectorPath.Count - j) - 1];
                    base.vectorPath[(base.vectorPath.Count - j) - 1] = vector;
                }
            }
        }

        public enum HeuristicMode
        {
            None,
            Average,
            MovingAverage,
            Midpoint,
            MovingMidpoint,
            Sequential
        }
    }
}

