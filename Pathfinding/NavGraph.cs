﻿namespace Pathfinding
{
    using Pathfinding.Serialization;
    using Pathfinding.Serialization.JsonFx;
    using Pathfinding.Util;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public abstract class NavGraph
    {
        public byte[] _sguid;
        [CompilerGenerated]
        private static GraphNodeDelegateCancelable <>f__am$cacheA;
        public AstarPath active;
        [JsonMember]
        public bool drawGizmos = true;
        public uint graphIndex;
        [JsonMember]
        public bool infoScreenOpen;
        [JsonMember]
        public uint initialPenalty;
        public Matrix4x4 inverseMatrix;
        [JsonMember]
        public Matrix4x4 matrix;
        [JsonMember]
        public string name;
        [JsonMember]
        public bool open;

        protected NavGraph()
        {
        }

        public virtual void Awake()
        {
        }

        public virtual int CountNodes()
        {
            <CountNodes>c__AnonStoreyBE ybe = new <CountNodes>c__AnonStoreyBE {
                count = 0
            };
            GraphNodeDelegateCancelable del = new GraphNodeDelegateCancelable(ybe.<>m__16);
            this.GetNodes(del);
            return ybe.count;
        }

        public virtual void CreateNodes(int number)
        {
            throw new NotSupportedException();
        }

        public virtual void DeserializeExtraInfo(GraphSerializationContext ctx)
        {
        }

        public NNInfo GetNearest(Vector3 position)
        {
            return this.GetNearest(position, NNConstraint.None);
        }

        public NNInfo GetNearest(Vector3 position, NNConstraint constraint)
        {
            return this.GetNearest(position, constraint, null);
        }

        public virtual NNInfo GetNearest(Vector3 position, NNConstraint constraint, GraphNode hint)
        {
            <GetNearest>c__AnonStoreyC0 yc;
            yc = new <GetNearest>c__AnonStoreyC0 {
                position = position,
                constraint = constraint,
                maxDistSqr = !yc.constraint.constrainDistance ? float.PositiveInfinity : AstarPath.active.maxNearestNodeDistanceSqr,
                minDist = float.PositiveInfinity,
                minNode = null,
                minConstDist = float.PositiveInfinity,
                minConstNode = null
            };
            this.GetNodes(new GraphNodeDelegateCancelable(yc.<>m__18));
            NNInfo info = new NNInfo(yc.minNode) {
                constrainedNode = yc.minConstNode
            };
            if (yc.minConstNode != null)
            {
                info.constClampedPosition = (Vector3) yc.minConstNode.position;
                return info;
            }
            if (yc.minNode != null)
            {
                info.constrainedNode = yc.minNode;
                info.constClampedPosition = (Vector3) yc.minNode.position;
            }
            return info;
        }

        public virtual NNInfo GetNearestForce(Vector3 position, NNConstraint constraint)
        {
            return this.GetNearest(position, constraint);
        }

        public abstract void GetNodes(GraphNodeDelegateCancelable del);
        public bool InSearchTree(GraphNode node, Path path)
        {
            if ((path != null) && (path.pathHandler != null))
            {
                return (path.pathHandler.GetPathNode(node).pathID == path.pathID);
            }
            return true;
        }

        public virtual Color NodeColor(GraphNode node, PathHandler data)
        {
            Color nodeConnection = AstarColor.NodeConnection;
            bool flag = false;
            if (node == null)
            {
                return AstarColor.NodeConnection;
            }
            GraphDebugMode debugMode = AstarPath.active.debugMode;
            switch (debugMode)
            {
                case GraphDebugMode.Penalty:
                    nodeConnection = Color.Lerp(AstarColor.ConnectionLowLerp, AstarColor.ConnectionHighLerp, ((float) node.Penalty) / AstarPath.active.debugRoof);
                    flag = true;
                    break;

                case GraphDebugMode.Tags:
                    nodeConnection = AstarMath.IntToColor((int) node.Tag, 0.5f);
                    flag = true;
                    break;

                default:
                    if (debugMode == GraphDebugMode.Areas)
                    {
                        nodeConnection = AstarColor.GetAreaColor(node.Area);
                        flag = true;
                    }
                    break;
            }
            if (!flag)
            {
                if (data == null)
                {
                    return AstarColor.NodeConnection;
                }
                PathNode pathNode = data.GetPathNode(node);
                switch (AstarPath.active.debugMode)
                {
                    case GraphDebugMode.G:
                        nodeConnection = Color.Lerp(AstarColor.ConnectionLowLerp, AstarColor.ConnectionHighLerp, ((float) pathNode.G) / AstarPath.active.debugRoof);
                        break;

                    case GraphDebugMode.H:
                        nodeConnection = Color.Lerp(AstarColor.ConnectionLowLerp, AstarColor.ConnectionHighLerp, ((float) pathNode.H) / AstarPath.active.debugRoof);
                        break;

                    case GraphDebugMode.F:
                        nodeConnection = Color.Lerp(AstarColor.ConnectionLowLerp, AstarColor.ConnectionHighLerp, ((float) pathNode.F) / AstarPath.active.debugRoof);
                        break;
                }
            }
            nodeConnection.a *= 0.5f;
            return nodeConnection;
        }

        public virtual void OnDestroy()
        {
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = delegate (GraphNode node) {
                    node.Destroy();
                    return true;
                };
            }
            this.GetNodes(<>f__am$cacheA);
        }

        public virtual void OnDrawGizmos(bool drawNodes)
        {
            <OnDrawGizmos>c__AnonStoreyC1 yc = new <OnDrawGizmos>c__AnonStoreyC1 {
                <>f__this = this
            };
            if (drawNodes)
            {
                yc.data = AstarPath.active.debugPathData;
                yc.node = null;
                yc.del = new GraphNodeDelegate(yc.<>m__1A);
                this.GetNodes(new GraphNodeDelegateCancelable(yc.<>m__1B));
            }
        }

        public virtual void PostDeserialization()
        {
        }

        public virtual void RelocateNodes(Matrix4x4 oldMatrix, Matrix4x4 newMatrix)
        {
            <RelocateNodes>c__AnonStoreyBF ybf = new <RelocateNodes>c__AnonStoreyBF();
            Matrix4x4 inverse = oldMatrix.inverse;
            ybf.m = inverse * newMatrix;
            this.GetNodes(new GraphNodeDelegateCancelable(ybf.<>m__17));
            this.SetMatrix(newMatrix);
        }

        public void SafeOnDestroy()
        {
            AstarPath.RegisterSafeUpdate(new OnVoidDelegate(this.OnDestroy), false);
        }

        [Obsolete("Please use AstarPath.active.Scan or if you really want this.ScanInternal which has the same functionality as this method had")]
        public void Scan()
        {
            throw new Exception("This method is deprecated. Please use AstarPath.active.Scan or if you really want this.ScanInternal which has the same functionality as this method had.");
        }

        public void ScanGraph()
        {
            if (AstarPath.OnPreScan != null)
            {
                AstarPath.OnPreScan(AstarPath.active);
            }
            if (AstarPath.OnGraphPreScan != null)
            {
                AstarPath.OnGraphPreScan(this);
            }
            this.ScanInternal();
            if (AstarPath.OnGraphPostScan != null)
            {
                AstarPath.OnGraphPostScan(this);
            }
            if (AstarPath.OnPostScan != null)
            {
                AstarPath.OnPostScan(AstarPath.active);
            }
        }

        public void ScanInternal()
        {
            this.ScanInternal(null);
        }

        public abstract void ScanInternal(OnScanStatus statusCallback);
        public virtual void SerializeExtraInfo(GraphSerializationContext ctx)
        {
        }

        public void SetMatrix(Matrix4x4 m)
        {
            this.matrix = m;
            this.inverseMatrix = m.inverse;
        }

        [JsonMember]
        public Pathfinding.Util.Guid guid
        {
            get
            {
                if ((this._sguid == null) || (this._sguid.Length != 0x10))
                {
                    this._sguid = Pathfinding.Util.Guid.NewGuid().ToByteArray();
                }
                return new Pathfinding.Util.Guid(this._sguid);
            }
            set
            {
                this._sguid = value.ToByteArray();
            }
        }

        [CompilerGenerated]
        private sealed class <CountNodes>c__AnonStoreyBE
        {
            internal int count;

            internal bool <>m__16(GraphNode node)
            {
                this.count++;
                return true;
            }
        }

        [CompilerGenerated]
        private sealed class <GetNearest>c__AnonStoreyC0
        {
            internal NNConstraint constraint;
            internal float maxDistSqr;
            internal float minConstDist;
            internal GraphNode minConstNode;
            internal float minDist;
            internal GraphNode minNode;
            internal Vector3 position;

            internal bool <>m__18(GraphNode node)
            {
                Vector3 vector = this.position - ((Vector3) node.position);
                float sqrMagnitude = vector.sqrMagnitude;
                if (sqrMagnitude < this.minDist)
                {
                    this.minDist = sqrMagnitude;
                    this.minNode = node;
                }
                if (((sqrMagnitude < this.minConstDist) && (sqrMagnitude < this.maxDistSqr)) && this.constraint.Suitable(node))
                {
                    this.minConstDist = sqrMagnitude;
                    this.minConstNode = node;
                }
                return true;
            }
        }

        [CompilerGenerated]
        private sealed class <OnDrawGizmos>c__AnonStoreyC1
        {
            internal NavGraph <>f__this;
            internal PathHandler data;
            internal GraphNodeDelegate del;
            internal GraphNode node;

            internal void <>m__1A(GraphNode o)
            {
                Gizmos.DrawLine((Vector3) this.node.position, (Vector3) o.position);
            }

            internal bool <>m__1B(GraphNode _node)
            {
                this.node = _node;
                Gizmos.color = this.<>f__this.NodeColor(this.node, AstarPath.active.debugPathData);
                if (!AstarPath.active.showSearchTree || this.<>f__this.InSearchTree(this.node, AstarPath.active.debugPath))
                {
                    PathNode node = (this.data == null) ? null : this.data.GetPathNode(this.node);
                    if ((AstarPath.active.showSearchTree && (node != null)) && (node.parent != null))
                    {
                        Gizmos.DrawLine((Vector3) this.node.position, (Vector3) node.parent.node.position);
                    }
                    else
                    {
                        this.node.GetConnections(this.del);
                    }
                }
                return true;
            }
        }

        [CompilerGenerated]
        private sealed class <RelocateNodes>c__AnonStoreyBF
        {
            internal Matrix4x4 m;

            internal bool <>m__17(GraphNode node)
            {
                node.position = (Int3) this.m.MultiplyPoint((Vector3) node.position);
                return true;
            }
        }
    }
}

