﻿namespace Pathfinding
{
    using Pathfinding.ClipperLib;
    using Pathfinding.Util;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Pathfinding/Navmesh/Navmesh Cut")]
    public class NavmeshCut : MonoBehaviour
    {
        private static List<NavmeshCut> allCuts = new List<NavmeshCut>();
        private Bounds bounds;
        public Vector3 center;
        public float circleRadius = 1f;
        public int circleResolution = 6;
        private Vector3[][] contours;
        public bool cutsAddedGeom = true;
        private static readonly Dictionary<Int2, int> edges = new Dictionary<Int2, int>();
        public static readonly Color GizmoColor = new Color(0.145098f, 0.7215686f, 0.9372549f);
        public float height = 1f;
        public bool isDual;
        private Bounds lastBounds;
        private Mesh lastMesh;
        private Vector3 lastPosition;
        private Quaternion lastRotation;
        public Mesh mesh;
        public float meshScale = 1f;
        private static readonly Dictionary<int, int> pointers = new Dictionary<int, int>();
        public Vector2 rectangleSize = new Vector2(1f, 1f);
        protected Transform tr;
        public MeshType type;
        public float updateDistance = 0.4f;
        public float updateRotationDistance = 10f;
        public bool useRotation;
        private bool wasEnabled;

        public static  event Action<NavmeshCut> OnDestroyCallback;

        private static void AddCut(NavmeshCut obj)
        {
            allCuts.Add(obj);
        }

        public void Awake()
        {
            AddCut(this);
        }

        private void CalculateMeshContour()
        {
            if (this.mesh != null)
            {
                edges.Clear();
                pointers.Clear();
                Vector3[] vertices = this.mesh.vertices;
                int[] triangles = this.mesh.triangles;
                for (int i = 0; i < triangles.Length; i += 3)
                {
                    if (Polygon.IsClockwise(vertices[triangles[i]], vertices[triangles[i + 1]], vertices[triangles[i + 2]]))
                    {
                        int num2 = triangles[i];
                        triangles[i] = triangles[i + 2];
                        triangles[i + 2] = num2;
                    }
                    edges[new Int2(triangles[i], triangles[i + 1])] = i;
                    edges[new Int2(triangles[i + 1], triangles[i + 2])] = i;
                    edges[new Int2(triangles[i + 2], triangles[i])] = i;
                }
                for (int j = 0; j < triangles.Length; j += 3)
                {
                    for (int m = 0; m < 3; m++)
                    {
                        if (!edges.ContainsKey(new Int2(triangles[j + ((m + 1) % 3)], triangles[j + (m % 3)])))
                        {
                            pointers[triangles[j + (m % 3)]] = triangles[j + ((m + 1) % 3)];
                        }
                    }
                }
                List<Vector3[]> list = new List<Vector3[]>();
                List<Vector3> list2 = ListPool<Vector3>.Claim();
                for (int k = 0; k < vertices.Length; k++)
                {
                    if (!pointers.ContainsKey(k))
                    {
                        continue;
                    }
                    list2.Clear();
                    int index = k;
                    do
                    {
                        int num7 = pointers[index];
                        if (num7 == -1)
                        {
                            break;
                        }
                        pointers[index] = -1;
                        list2.Add(vertices[index]);
                        switch (num7)
                        {
                            case -1:
                                Debug.LogError("Invalid Mesh '" + this.mesh.name + " in " + base.gameObject.name);
                                break;
                        }
                    }
                    while (index != k);
                    if (list2.Count > 0)
                    {
                        list.Add(list2.ToArray());
                    }
                }
                ListPool<Vector3>.Release(list2);
                this.contours = list.ToArray();
            }
        }

        public void ForceUpdate()
        {
            this.lastPosition = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
        }

        public static List<NavmeshCut> GetAll()
        {
            return allCuts;
        }

        public static List<NavmeshCut> GetAllInRange(Bounds b)
        {
            List<NavmeshCut> list = ListPool<NavmeshCut>.Claim();
            for (int i = 0; i < allCuts.Count; i++)
            {
                if (allCuts[i].enabled && Intersects(b, allCuts[i].GetBounds()))
                {
                    list.Add(allCuts[i]);
                }
            }
            return list;
        }

        public Bounds GetBounds()
        {
            switch (this.type)
            {
                case MeshType.Rectangle:
                {
                    if (!this.useRotation)
                    {
                        this.bounds = new Bounds(this.tr.position + this.center, new Vector3(this.rectangleSize.x, this.height, this.rectangleSize.y));
                        break;
                    }
                    Matrix4x4 localToWorldMatrix = this.tr.localToWorldMatrix;
                    this.bounds = new Bounds(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(-this.rectangleSize.x, -this.height, -this.rectangleSize.y) * 0.5f))), Vector3.zero);
                    this.bounds.Encapsulate(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(this.rectangleSize.x, -this.height, -this.rectangleSize.y) * 0.5f))));
                    this.bounds.Encapsulate(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(this.rectangleSize.x, -this.height, this.rectangleSize.y) * 0.5f))));
                    this.bounds.Encapsulate(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(-this.rectangleSize.x, -this.height, this.rectangleSize.y) * 0.5f))));
                    this.bounds.Encapsulate(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(-this.rectangleSize.x, this.height, -this.rectangleSize.y) * 0.5f))));
                    this.bounds.Encapsulate(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(this.rectangleSize.x, this.height, -this.rectangleSize.y) * 0.5f))));
                    this.bounds.Encapsulate(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(this.rectangleSize.x, this.height, this.rectangleSize.y) * 0.5f))));
                    this.bounds.Encapsulate(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(-this.rectangleSize.x, this.height, this.rectangleSize.y) * 0.5f))));
                    break;
                }
                case MeshType.Circle:
                {
                    if (!this.useRotation)
                    {
                        this.bounds = new Bounds(base.transform.position + this.center, new Vector3(this.circleRadius * 2f, this.height, this.circleRadius * 2f));
                        break;
                    }
                    Matrix4x4 matrixx2 = this.tr.localToWorldMatrix;
                    this.bounds = new Bounds(matrixx2.MultiplyPoint3x4(this.center), new Vector3(this.circleRadius * 2f, this.height, this.circleRadius * 2f));
                    break;
                }
                case MeshType.CustomMesh:
                    if (this.mesh != null)
                    {
                        Bounds bounds = this.mesh.bounds;
                        if (this.useRotation)
                        {
                            Matrix4x4 matrixx3 = this.tr.localToWorldMatrix;
                            bounds.center = (Vector3) (bounds.center * this.meshScale);
                            bounds.size = (Vector3) (bounds.size * this.meshScale);
                            this.bounds = new Bounds(matrixx3.MultiplyPoint3x4(this.center + bounds.center), Vector3.zero);
                            Vector3 max = bounds.max;
                            Vector3 min = bounds.min;
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(max.x, max.y, max.z)));
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(min.x, max.y, max.z)));
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(min.x, max.y, min.z)));
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(max.x, max.y, min.z)));
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(max.x, min.y, max.z)));
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(min.x, min.y, max.z)));
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(min.x, min.y, min.z)));
                            this.bounds.Encapsulate(matrixx3.MultiplyPoint3x4(this.center + new Vector3(max.x, min.y, min.z)));
                            Vector3 size = this.bounds.size;
                            size.y = Mathf.Max(size.y, this.height * this.tr.lossyScale.y);
                            this.bounds.size = size;
                        }
                        else
                        {
                            Vector3 vector4 = (Vector3) (bounds.size * this.meshScale);
                            vector4.y = Mathf.Max(vector4.y, this.height);
                            this.bounds = new Bounds((base.transform.position + this.center) + ((Vector3) (bounds.center * this.meshScale)), vector4);
                        }
                        break;
                    }
                    break;
            }
            return this.bounds;
        }

        public void GetContour(List<List<IntPoint>> buffer)
        {
            List<IntPoint> list;
            if (this.circleResolution < 3)
            {
                this.circleResolution = 3;
            }
            Vector3 position = this.tr.position;
            switch (this.type)
            {
                case MeshType.Rectangle:
                {
                    list = ListPool<IntPoint>.Claim();
                    if (!this.useRotation)
                    {
                        position += this.center;
                        list.Add(this.V3ToIntPoint(position + ((Vector3) (new Vector3(-this.rectangleSize.x, 0f, -this.rectangleSize.y) * 0.5f))));
                        list.Add(this.V3ToIntPoint(position + ((Vector3) (new Vector3(this.rectangleSize.x, 0f, -this.rectangleSize.y) * 0.5f))));
                        list.Add(this.V3ToIntPoint(position + ((Vector3) (new Vector3(this.rectangleSize.x, 0f, this.rectangleSize.y) * 0.5f))));
                        list.Add(this.V3ToIntPoint(position + ((Vector3) (new Vector3(-this.rectangleSize.x, 0f, this.rectangleSize.y) * 0.5f))));
                        break;
                    }
                    Matrix4x4 localToWorldMatrix = this.tr.localToWorldMatrix;
                    list.Add(this.V3ToIntPoint(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(-this.rectangleSize.x, 0f, -this.rectangleSize.y) * 0.5f)))));
                    list.Add(this.V3ToIntPoint(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(this.rectangleSize.x, 0f, -this.rectangleSize.y) * 0.5f)))));
                    list.Add(this.V3ToIntPoint(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(this.rectangleSize.x, 0f, this.rectangleSize.y) * 0.5f)))));
                    list.Add(this.V3ToIntPoint(localToWorldMatrix.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(-this.rectangleSize.x, 0f, this.rectangleSize.y) * 0.5f)))));
                    break;
                }
                case MeshType.Circle:
                    list = ListPool<IntPoint>.Claim(this.circleResolution);
                    if (!this.useRotation)
                    {
                        position += this.center;
                        for (int i = 0; i < this.circleResolution; i++)
                        {
                            list.Add(this.V3ToIntPoint(position + ((Vector3) (new Vector3(Mathf.Cos(((i * 2) * 3.141593f) / ((float) this.circleResolution)), 0f, Mathf.Sin(((i * 2) * 3.141593f) / ((float) this.circleResolution))) * this.circleRadius))));
                        }
                    }
                    else
                    {
                        Matrix4x4 matrixx2 = this.tr.localToWorldMatrix;
                        for (int j = 0; j < this.circleResolution; j++)
                        {
                            list.Add(this.V3ToIntPoint(matrixx2.MultiplyPoint3x4(this.center + ((Vector3) (new Vector3(Mathf.Cos(((j * 2) * 3.141593f) / ((float) this.circleResolution)), 0f, Mathf.Sin(((j * 2) * 3.141593f) / ((float) this.circleResolution))) * this.circleRadius)))));
                        }
                    }
                    buffer.Add(list);
                    return;

                case MeshType.CustomMesh:
                    if ((this.mesh != this.lastMesh) || (this.contours == null))
                    {
                        this.CalculateMeshContour();
                        this.lastMesh = this.mesh;
                    }
                    if (this.contours != null)
                    {
                        position += this.center;
                        bool flag = Vector3.Dot(this.tr.up, Vector3.up) < 0f;
                        for (int k = 0; k < this.contours.Length; k++)
                        {
                            Vector3[] vectorArray = this.contours[k];
                            list = ListPool<IntPoint>.Claim(vectorArray.Length);
                            if (this.useRotation)
                            {
                                Matrix4x4 matrixx3 = this.tr.localToWorldMatrix;
                                for (int m = 0; m < vectorArray.Length; m++)
                                {
                                    list.Add(this.V3ToIntPoint(matrixx3.MultiplyPoint3x4(this.center + ((Vector3) (vectorArray[m] * this.meshScale)))));
                                }
                            }
                            else
                            {
                                for (int n = 0; n < vectorArray.Length; n++)
                                {
                                    list.Add(this.V3ToIntPoint(position + ((Vector3) (vectorArray[n] * this.meshScale))));
                                }
                            }
                            if (flag)
                            {
                                list.Reverse();
                            }
                            buffer.Add(list);
                        }
                    }
                    return;

                default:
                    return;
            }
            buffer.Add(list);
        }

        private static bool Intersects(Bounds b1, Bounds b2)
        {
            Vector3 min = b1.min;
            Vector3 max = b1.max;
            Vector3 vector3 = b2.min;
            Vector3 vector4 = b2.max;
            return (((((min.x <= vector4.x) && (max.x >= vector3.x)) && ((min.y <= vector4.y) && (max.y >= vector3.y))) && (min.z <= vector4.z)) && (max.z >= vector3.z));
        }

        public Vector3 IntPointToV3(IntPoint p)
        {
            Int3 num = new Int3((int) p.X, 0, (int) p.Y);
            return (Vector3) num;
        }

        public void NotifyUpdated()
        {
            this.wasEnabled = base.enabled;
            if (this.wasEnabled)
            {
                this.lastPosition = this.tr.position;
                this.lastBounds = this.GetBounds();
                if (this.useRotation)
                {
                    this.lastRotation = this.tr.rotation;
                }
            }
        }

        public void OnDestroy()
        {
            if (OnDestroyCallback != null)
            {
                OnDestroyCallback(this);
            }
            RemoveCut(this);
        }

        public void OnDrawGizmos()
        {
            if (this.tr == null)
            {
                this.tr = base.transform;
            }
            List<List<IntPoint>> buffer = ListPool<List<IntPoint>>.Claim();
            this.GetContour(buffer);
            Gizmos.color = GizmoColor;
            Bounds bounds = this.GetBounds();
            float y = bounds.min.y;
            Vector3 vector = (Vector3) (Vector3.up * (bounds.max.y - y));
            for (int i = 0; i < buffer.Count; i++)
            {
                List<IntPoint> list2 = buffer[i];
                for (int j = 0; j < list2.Count; j++)
                {
                    Vector3 from = this.IntPointToV3(list2[j]);
                    from.y = y;
                    Vector3 to = this.IntPointToV3(list2[(j + 1) % list2.Count]);
                    to.y = y;
                    Gizmos.DrawLine(from, to);
                    Gizmos.DrawLine(from + vector, to + vector);
                    Gizmos.DrawLine(from, from + vector);
                    Gizmos.DrawLine(to, to + vector);
                }
            }
            ListPool<List<IntPoint>>.Release(buffer);
        }

        public void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.Lerp(GizmoColor, new Color(1f, 1f, 1f, 0.2f), 0.9f);
            Bounds bounds = this.GetBounds();
            Gizmos.DrawCube(bounds.center, bounds.size);
            Gizmos.DrawWireCube(bounds.center, bounds.size);
        }

        public void OnEnable()
        {
            this.tr = base.transform;
            this.lastPosition = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
            this.lastRotation = this.tr.rotation;
        }

        private static void RemoveCut(NavmeshCut obj)
        {
            allCuts.Remove(obj);
        }

        public bool RequiresUpdate()
        {
            return ((this.wasEnabled != base.enabled) || (this.wasEnabled && (((this.tr.position - this.lastPosition).sqrMagnitude > (this.updateDistance * this.updateDistance)) || (this.useRotation && (Quaternion.Angle(this.lastRotation, this.tr.rotation) > this.updateRotationDistance)))));
        }

        public virtual void UsedForCut()
        {
        }

        public IntPoint V3ToIntPoint(Vector3 p)
        {
            Int3 num = (Int3) p;
            return new IntPoint((long) num.x, (long) num.z);
        }

        public Bounds LastBounds
        {
            get
            {
                return this.lastBounds;
            }
        }

        public enum MeshType
        {
            Rectangle,
            Circle,
            CustomMesh
        }
    }
}

