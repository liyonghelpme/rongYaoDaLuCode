﻿namespace Pathfinding
{
    using System;

    [Obsolete("This class has been replaced with GraphNode, it may be removed in future versions", true)]
    public class Node
    {
    }
}

