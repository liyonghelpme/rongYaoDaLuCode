﻿namespace Pathfinding
{
    using Pathfinding.Util;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public static class PathUtilities
    {
        public static List<GraphNode> BFS(GraphNode seed, int depth, int tagMask = -1)
        {
            GraphNodeDelegate delegate2;
            <BFS>c__AnonStoreyCA yca = new <BFS>c__AnonStoreyCA {
                tagMask = tagMask,
                que = ListPool<GraphNode>.Claim(),
                list = ListPool<GraphNode>.Claim(),
                map = new Dictionary<GraphNode, int>(),
                currentDist = 0
            };
            if (yca.tagMask == -1)
            {
                delegate2 = new GraphNodeDelegate(yca.<>m__27);
            }
            else
            {
                delegate2 = new GraphNodeDelegate(yca.<>m__28);
            }
            yca.map[seed] = yca.currentDist;
            delegate2(seed);
            while ((yca.que.Count > 0) && (yca.currentDist < depth))
            {
                GraphNode node = yca.que[yca.que.Count - 1];
                yca.currentDist = yca.map[node];
                yca.que.RemoveAt(yca.que.Count - 1);
                node.GetConnections(delegate2);
            }
            ListPool<GraphNode>.Release(yca.que);
            return yca.list;
        }

        public static void GetPointsAroundPoint(Vector3 p, IRaycastableGraph g, List<Vector3> previousPoints, float radius, float clearanceRadius)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }
            NavGraph graph = g as NavGraph;
            if (graph == null)
            {
                throw new ArgumentException("g is not a NavGraph");
            }
            NNInfo nearestForce = graph.GetNearestForce(p, NNConstraint.Default);
            p = nearestForce.clampedPosition;
            if (nearestForce.node != null)
            {
                radius = Mathf.Max(radius, (1.4142f * clearanceRadius) * Mathf.Sqrt((float) previousPoints.Count));
                clearanceRadius *= clearanceRadius;
                for (int i = 0; i < previousPoints.Count; i++)
                {
                    Vector3 vector = previousPoints[i];
                    float magnitude = vector.magnitude;
                    if (magnitude > 0f)
                    {
                        vector = (Vector3) (vector / magnitude);
                    }
                    float from = radius;
                    vector = (Vector3) (vector * from);
                    bool flag = false;
                    int num4 = 0;
                    do
                    {
                        GraphHitInfo info2;
                        Vector3 end = p + vector;
                        if (g.Linecast(p, end, nearestForce.node, out info2))
                        {
                            end = info2.point;
                        }
                        for (float j = 0.1f; j <= 1f; j += 0.05f)
                        {
                            Vector3 vector3 = ((Vector3) ((end - p) * j)) + p;
                            flag = true;
                            for (int k = 0; k < i; k++)
                            {
                                Vector3 vector4 = previousPoints[k] - vector3;
                                if (vector4.sqrMagnitude < clearanceRadius)
                                {
                                    flag = false;
                                    break;
                                }
                            }
                            if (flag)
                            {
                                previousPoints[i] = vector3;
                                break;
                            }
                        }
                        if (!flag)
                        {
                            if (num4 > 8)
                            {
                                flag = true;
                            }
                            else
                            {
                                clearanceRadius *= 0.9f;
                                vector = (Vector3) (UnityEngine.Random.onUnitSphere * Mathf.Lerp(from, radius, (float) (num4 / 5)));
                                vector.y = 0f;
                                num4++;
                            }
                        }
                    }
                    while (!flag);
                }
            }
        }

        public static void GetPointsAroundPointWorld(Vector3 p, IRaycastableGraph g, List<Vector3> previousPoints, float radius, float clearanceRadius)
        {
            if (previousPoints.Count != 0)
            {
                Vector3 zero = Vector3.zero;
                for (int i = 0; i < previousPoints.Count; i++)
                {
                    zero += previousPoints[i];
                }
                zero = (Vector3) (zero / ((float) previousPoints.Count));
                for (int j = 0; j < previousPoints.Count; j++)
                {
                    List<Vector3> list;
                    int num3;
                    Vector3 vector2 = list[num3];
                    (list = previousPoints)[num3 = j] = vector2 - zero;
                }
                GetPointsAroundPoint(p, g, previousPoints, radius, clearanceRadius);
            }
        }

        public static List<Vector3> GetPointsOnNodes(List<GraphNode> nodes, int count, float clearanceRadius = 0)
        {
            if (nodes == null)
            {
                throw new ArgumentNullException("nodes");
            }
            if (nodes.Count == 0)
            {
                throw new ArgumentException("no nodes passed");
            }
            System.Random random = new System.Random();
            List<Vector3> list = ListPool<Vector3>.Claim(count);
            clearanceRadius *= clearanceRadius;
            if ((nodes[0] is TriangleMeshNode) || (nodes[0] is GridNode))
            {
                List<float> list2 = ListPool<float>.Claim(nodes.Count);
                float item = 0f;
                for (int j = 0; j < nodes.Count; j++)
                {
                    TriangleMeshNode node = nodes[j] as TriangleMeshNode;
                    if (node != null)
                    {
                        float num3 = Math.Abs(Polygon.TriangleArea(node.GetVertex(0), node.GetVertex(1), node.GetVertex(2)));
                        item += num3;
                        list2.Add(item);
                    }
                    else
                    {
                        GridNode node2 = nodes[j] as GridNode;
                        if (node2 != null)
                        {
                            GridGraph gridGraph = GridNode.GetGridGraph(node2.GraphIndex);
                            float num4 = gridGraph.nodeSize * gridGraph.nodeSize;
                            item += num4;
                            list2.Add(item);
                        }
                        else
                        {
                            list2.Add(item);
                        }
                    }
                }
                for (int k = 0; k < count; k++)
                {
                    int num6 = 0;
                    int num7 = 10;
                    bool flag = false;
                    while (!flag)
                    {
                        Vector3 vector;
                        flag = true;
                        if (num6 >= num7)
                        {
                            clearanceRadius *= 0.8f;
                            num7 += 10;
                            if (num7 > 100)
                            {
                                clearanceRadius = 0f;
                            }
                        }
                        float num8 = ((float) random.NextDouble()) * item;
                        int num9 = list2.BinarySearch(num8);
                        if (num9 < 0)
                        {
                            num9 = ~num9;
                        }
                        if (num9 >= nodes.Count)
                        {
                            flag = false;
                            continue;
                        }
                        TriangleMeshNode node3 = nodes[num9] as TriangleMeshNode;
                        if (node3 != null)
                        {
                            float num10;
                            float num11;
                            do
                            {
                                num10 = (float) random.NextDouble();
                                num11 = (float) random.NextDouble();
                            }
                            while ((num10 + num11) > 1f);
                            vector = ((Vector3) ((((Vector3) (node3.GetVertex(1) - node3.GetVertex(0))) * num10) + (((Vector3) (node3.GetVertex(2) - node3.GetVertex(0))) * num11))) + ((Vector3) node3.GetVertex(0));
                        }
                        else
                        {
                            GridNode node4 = nodes[num9] as GridNode;
                            if (node4 != null)
                            {
                                GridGraph graph2 = GridNode.GetGridGraph(node4.GraphIndex);
                                float num12 = (float) random.NextDouble();
                                float num13 = (float) random.NextDouble();
                                vector = ((Vector3) node4.position) + ((Vector3) (new Vector3(num12 - 0.5f, 0f, num13 - 0.5f) * graph2.nodeSize));
                            }
                            else
                            {
                                list.Add((Vector3) nodes[num9].position);
                                break;
                            }
                        }
                        if (clearanceRadius > 0f)
                        {
                            for (int m = 0; m < list.Count; m++)
                            {
                                Vector3 vector2 = list[m] - vector;
                                if (vector2.sqrMagnitude < clearanceRadius)
                                {
                                    flag = false;
                                    break;
                                }
                            }
                        }
                        if (flag)
                        {
                            list.Add(vector);
                            break;
                        }
                        num6++;
                    }
                }
                ListPool<float>.Release(list2);
                return list;
            }
            for (int i = 0; i < count; i++)
            {
                list.Add((Vector3) nodes[random.Next(nodes.Count)].position);
            }
            return list;
        }

        public static List<GraphNode> GetReachableNodes(GraphNode seed, int tagMask = -1)
        {
            GraphNodeDelegate delegate2;
            <GetReachableNodes>c__AnonStoreyC9 yc = new <GetReachableNodes>c__AnonStoreyC9 {
                tagMask = tagMask,
                stack = StackPool<GraphNode>.Claim(),
                list = ListPool<GraphNode>.Claim(),
                map = new HashSet<GraphNode>()
            };
            if (yc.tagMask == -1)
            {
                delegate2 = new GraphNodeDelegate(yc.<>m__25);
            }
            else
            {
                delegate2 = new GraphNodeDelegate(yc.<>m__26);
            }
            delegate2(seed);
            while (yc.stack.Count > 0)
            {
                yc.stack.Pop().GetConnections(delegate2);
            }
            StackPool<GraphNode>.Release(yc.stack);
            return yc.list;
        }

        public static List<Vector3> GetSpiralPoints(int count, float clearance)
        {
            List<Vector3> list = ListPool<Vector3>.Claim(count);
            float a = clearance / 6.283185f;
            float t = 0f;
            list.Add(InvoluteOfCircle(a, t));
            for (int i = 0; i < count; i++)
            {
                Vector3 vector = list[list.Count - 1];
                float num4 = (-t / 2f) + Mathf.Sqrt(((t * t) / 4f) + ((2f * clearance) / a));
                float num5 = t + num4;
                float num6 = t + (2f * num4);
                while ((num6 - num5) > 0.01f)
                {
                    float num7 = (num5 + num6) / 2f;
                    Vector3 vector3 = InvoluteOfCircle(a, num7) - vector;
                    if (vector3.sqrMagnitude < (clearance * clearance))
                    {
                        num5 = num7;
                    }
                    else
                    {
                        num6 = num7;
                    }
                }
                list.Add(InvoluteOfCircle(a, num6));
                t = num6;
            }
            return list;
        }

        private static Vector3 InvoluteOfCircle(float a, float t)
        {
            return new Vector3(a * (Mathf.Cos(t) + (t * Mathf.Sin(t))), 0f, a * (Mathf.Sin(t) - (t * Mathf.Cos(t))));
        }

        public static bool IsPathPossible(List<GraphNode> nodes)
        {
            uint area = nodes[0].Area;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (!nodes[i].Walkable || (nodes[i].Area != area))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsPathPossible(GraphNode n1, GraphNode n2)
        {
            return ((n1.Walkable && n2.Walkable) && (n1.Area == n2.Area));
        }

        [CompilerGenerated]
        private sealed class <BFS>c__AnonStoreyCA
        {
            internal int currentDist;
            internal List<GraphNode> list;
            internal Dictionary<GraphNode, int> map;
            internal List<GraphNode> que;
            internal int tagMask;

            internal void <>m__27(GraphNode node)
            {
                if (node.Walkable && !this.map.ContainsKey(node))
                {
                    this.map.Add(node, this.currentDist + 1);
                    this.list.Add(node);
                    this.que.Add(node);
                }
            }

            internal void <>m__28(GraphNode node)
            {
                if ((node.Walkable && (((this.tagMask >> node.Tag) & 1) != 0)) && !this.map.ContainsKey(node))
                {
                    this.map.Add(node, this.currentDist + 1);
                    this.list.Add(node);
                    this.que.Add(node);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetReachableNodes>c__AnonStoreyC9
        {
            internal List<GraphNode> list;
            internal HashSet<GraphNode> map;
            internal Stack<GraphNode> stack;
            internal int tagMask;

            internal void <>m__25(GraphNode node)
            {
                if (node.Walkable && this.map.Add(node))
                {
                    this.list.Add(node);
                    this.stack.Push(node);
                }
            }

            internal void <>m__26(GraphNode node)
            {
                if ((node.Walkable && (((this.tagMask >> node.Tag) & 1) != 0)) && this.map.Add(node))
                {
                    this.list.Add(node);
                    this.stack.Push(node);
                }
            }
        }
    }
}

