﻿namespace Pathfinding
{
    using Pathfinding.Serialization;
    using Pathfinding.Serialization.JsonFx;
    using Pathfinding.Util;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    [JsonOptIn]
    public class PointGraph : NavGraph, IUpdatableGraph
    {
        [JsonMember]
        public bool autoLinkNodes = true;
        [JsonMember]
        public Vector3 limits;
        private Int3 lookupCellSize;
        [JsonMember]
        public LayerMask mask;
        [JsonMember]
        public float maxDistance;
        private Int3 maxLookup;
        private Int3 minLookup;
        public int nodeCount;
        private GameObject[] nodeGameObjects;
        private Dictionary<Int3, PointNode> nodeLookup;
        public PointNode[] nodes;
        [JsonMember]
        public bool optimizeFor2D;
        [JsonMember]
        public bool optimizeForSparseGraph;
        [JsonMember]
        public bool raycast = true;
        [JsonMember]
        public bool recursive = true;
        [JsonMember]
        public Transform root;
        [JsonMember]
        public string searchTag;
        [JsonMember]
        public bool thickRaycast;
        [JsonMember]
        public float thickRaycastRadius = 1f;
        private static readonly Int3[] ThreeDNeighbours = new Int3[] { 
            new Int3(-1, 0, -1), new Int3(0, 0, -1), new Int3(1, 0, -1), new Int3(-1, 0, 0), new Int3(0, 0, 0), new Int3(1, 0, 0), new Int3(-1, 0, 1), new Int3(0, 0, 1), new Int3(1, 0, 1), new Int3(-1, -1, -1), new Int3(0, -1, -1), new Int3(1, -1, -1), new Int3(-1, -1, 0), new Int3(0, -1, 0), new Int3(1, -1, 0), new Int3(-1, -1, 1), 
            new Int3(0, -1, 1), new Int3(1, -1, 1), new Int3(-1, 1, -1), new Int3(0, 1, -1), new Int3(1, 1, -1), new Int3(-1, 1, 0), new Int3(0, 1, 0), new Int3(1, 1, 0), new Int3(-1, 1, 1), new Int3(0, 1, 1), new Int3(1, 1, 1)
         };

        public void AddChildren(ref int c, Transform tr)
        {
            IEnumerator enumerator = tr.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    this.nodes[c].SetPosition((Int3) current.position);
                    this.nodes[c].Walkable = true;
                    this.nodeGameObjects[c] = current.gameObject;
                    c++;
                    this.AddChildren(ref c, current);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }

        public PointNode AddNode(Int3 position)
        {
            return this.AddNode<PointNode>(new PointNode(base.active), position);
        }

        public T AddNode<T>(T nd, Int3 position) where T: PointNode
        {
            if ((this.nodes == null) || (this.nodeCount == this.nodes.Length))
            {
                PointNode[] nodeArray = new PointNode[(this.nodes == null) ? 4 : Math.Max((int) (this.nodes.Length + 4), (int) (this.nodes.Length * 2))];
                for (int i = 0; i < this.nodeCount; i++)
                {
                    nodeArray[i] = this.nodes[i];
                }
                this.nodes = nodeArray;
            }
            nd.SetPosition(position);
            nd.GraphIndex = base.graphIndex;
            nd.Walkable = true;
            this.nodes[this.nodeCount] = nd;
            this.nodeCount++;
            this.AddToLookup(nd);
            return nd;
        }

        public void AddToLookup(PointNode node)
        {
            if (this.nodeLookup != null)
            {
                PointNode node2;
                Int3 key = this.WorldToLookupSpace(node.position);
                if (this.nodeLookup.Count == 0)
                {
                    this.minLookup = key;
                    this.maxLookup = key;
                }
                else
                {
                    int introduced2 = Math.Min(this.minLookup.x, key.x);
                    int introduced3 = Math.Min(this.minLookup.y, key.y);
                    this.minLookup = new Int3(introduced2, introduced3, Math.Min(this.minLookup.z, key.z));
                    int introduced4 = Math.Max(this.minLookup.x, key.x);
                    int introduced5 = Math.Max(this.minLookup.y, key.y);
                    this.maxLookup = new Int3(introduced4, introduced5, Math.Max(this.minLookup.z, key.z));
                }
                if (node.next != null)
                {
                    throw new Exception("This node has already been added to the lookup structure");
                }
                if (this.nodeLookup.TryGetValue(key, out node2))
                {
                    node.next = node2.next;
                    node2.next = node;
                }
                else
                {
                    this.nodeLookup[key] = node;
                }
            }
        }

        public GraphUpdateThreading CanUpdateAsync(GraphUpdateObject o)
        {
            return GraphUpdateThreading.UnityThread;
        }

        public static int CountChildren(Transform tr)
        {
            int num = 0;
            IEnumerator enumerator = tr.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    num++;
                    num += CountChildren(current);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            return num;
        }

        public override void DeserializeExtraInfo(GraphSerializationContext ctx)
        {
            int num = ctx.reader.ReadInt32();
            if (num == -1)
            {
                this.nodes = null;
            }
            else
            {
                this.nodes = new PointNode[num];
                this.nodeCount = num;
                for (int i = 0; i < this.nodes.Length; i++)
                {
                    if (ctx.reader.ReadInt32() != -1)
                    {
                        this.nodes[i] = new PointNode(base.active);
                        this.nodes[i].DeserializeNode(ctx);
                    }
                }
            }
        }

        public override NNInfo GetNearest(Vector3 position, NNConstraint constraint, GraphNode hint)
        {
            return this.GetNearestForce(position, constraint);
        }

        public override NNInfo GetNearestForce(Vector3 position, NNConstraint constraint)
        {
            if (this.nodes == null)
            {
                return new NNInfo();
            }
            float num = !constraint.constrainDistance ? float.PositiveInfinity : AstarPath.active.maxNearestNodeDistanceSqr;
            float positiveInfinity = float.PositiveInfinity;
            GraphNode node = null;
            float num3 = float.PositiveInfinity;
            GraphNode node2 = null;
            if (this.optimizeForSparseGraph)
            {
                Int3 key = this.WorldToLookupSpace((Int3) position);
                Int3 num5 = key - this.minLookup;
                int num6 = 0;
                int introduced48 = Math.Max(num6, Math.Abs(num5.x));
                int introduced49 = Math.Max(introduced48, Math.Abs(num5.y));
                num6 = Math.Max(introduced49, Math.Abs(num5.z));
                num5 = key - this.maxLookup;
                int introduced50 = Math.Max(num6, Math.Abs(num5.x));
                int introduced51 = Math.Max(introduced50, Math.Abs(num5.y));
                num6 = Math.Max(introduced51, Math.Abs(num5.z));
                PointNode next = null;
                if (this.nodeLookup.TryGetValue(key, out next))
                {
                    while (next != null)
                    {
                        Vector3 vector = position - ((Vector3) next.position);
                        float sqrMagnitude = vector.sqrMagnitude;
                        if (sqrMagnitude < positiveInfinity)
                        {
                            positiveInfinity = sqrMagnitude;
                            node = next;
                        }
                        if ((constraint == null) || (((sqrMagnitude < num3) && (sqrMagnitude < num)) && constraint.Suitable(next)))
                        {
                            num3 = sqrMagnitude;
                            node2 = next;
                        }
                        next = next.next;
                    }
                }
                for (int i = 1; i <= num6; i++)
                {
                    if (i >= 20)
                    {
                        Debug.LogWarning("Aborting GetNearest call at maximum distance because it has iterated too many times.\nIf you get this regularly, check your settings for PointGraph -> <b>Optimize For Sparse Graph</b> and PointGraph -> <b>Optimize For 2D</b>.\nThis happens when the closest node was very far away (20*link distance between nodes). When optimizing for sparse graphs, getting the nearest node from far away positions is <b>very slow</b>.\n");
                        break;
                    }
                    if (this.lookupCellSize.y == 0)
                    {
                        Int3 num9 = key + new Int3(-i, 0, -i);
                        for (int j = 0; j <= (2 * i); j++)
                        {
                            if (this.nodeLookup.TryGetValue(num9 + new Int3(j, 0, 0), out next))
                            {
                                while (next != null)
                                {
                                    Vector3 vector2 = position - ((Vector3) next.position);
                                    float num11 = vector2.sqrMagnitude;
                                    if (num11 < positiveInfinity)
                                    {
                                        positiveInfinity = num11;
                                        node = next;
                                    }
                                    if ((constraint == null) || (((num11 < num3) && (num11 < num)) && constraint.Suitable(next)))
                                    {
                                        num3 = num11;
                                        node2 = next;
                                    }
                                    next = next.next;
                                }
                            }
                            if (this.nodeLookup.TryGetValue(num9 + new Int3(j, 0, 2 * i), out next))
                            {
                                while (next != null)
                                {
                                    Vector3 vector3 = position - ((Vector3) next.position);
                                    float num12 = vector3.sqrMagnitude;
                                    if (num12 < positiveInfinity)
                                    {
                                        positiveInfinity = num12;
                                        node = next;
                                    }
                                    if ((constraint == null) || (((num12 < num3) && (num12 < num)) && constraint.Suitable(next)))
                                    {
                                        num3 = num12;
                                        node2 = next;
                                    }
                                    next = next.next;
                                }
                            }
                        }
                        for (int k = 1; k < (2 * i); k++)
                        {
                            if (this.nodeLookup.TryGetValue(num9 + new Int3(0, 0, k), out next))
                            {
                                while (next != null)
                                {
                                    Vector3 vector4 = position - ((Vector3) next.position);
                                    float num14 = vector4.sqrMagnitude;
                                    if (num14 < positiveInfinity)
                                    {
                                        positiveInfinity = num14;
                                        node = next;
                                    }
                                    if ((constraint == null) || (((num14 < num3) && (num14 < num)) && constraint.Suitable(next)))
                                    {
                                        num3 = num14;
                                        node2 = next;
                                    }
                                    next = next.next;
                                }
                            }
                            if (this.nodeLookup.TryGetValue(num9 + new Int3(2 * i, 0, k), out next))
                            {
                                while (next != null)
                                {
                                    Vector3 vector5 = position - ((Vector3) next.position);
                                    float num15 = vector5.sqrMagnitude;
                                    if (num15 < positiveInfinity)
                                    {
                                        positiveInfinity = num15;
                                        node = next;
                                    }
                                    if ((constraint == null) || (((num15 < num3) && (num15 < num)) && constraint.Suitable(next)))
                                    {
                                        num3 = num15;
                                        node2 = next;
                                    }
                                    next = next.next;
                                }
                            }
                        }
                    }
                    else
                    {
                        Int3 num16 = key + new Int3(-i, -i, -i);
                        for (int m = 0; m <= (2 * i); m++)
                        {
                            for (int num18 = 0; num18 <= (2 * i); num18++)
                            {
                                if (this.nodeLookup.TryGetValue(num16 + new Int3(m, num18, 0), out next))
                                {
                                    while (next != null)
                                    {
                                        Vector3 vector6 = position - ((Vector3) next.position);
                                        float num19 = vector6.sqrMagnitude;
                                        if (num19 < positiveInfinity)
                                        {
                                            positiveInfinity = num19;
                                            node = next;
                                        }
                                        if ((constraint == null) || (((num19 < num3) && (num19 < num)) && constraint.Suitable(next)))
                                        {
                                            num3 = num19;
                                            node2 = next;
                                        }
                                        next = next.next;
                                    }
                                }
                                if (this.nodeLookup.TryGetValue(num16 + new Int3(m, num18, 2 * i), out next))
                                {
                                    while (next != null)
                                    {
                                        Vector3 vector7 = position - ((Vector3) next.position);
                                        float num20 = vector7.sqrMagnitude;
                                        if (num20 < positiveInfinity)
                                        {
                                            positiveInfinity = num20;
                                            node = next;
                                        }
                                        if ((constraint == null) || (((num20 < num3) && (num20 < num)) && constraint.Suitable(next)))
                                        {
                                            num3 = num20;
                                            node2 = next;
                                        }
                                        next = next.next;
                                    }
                                }
                            }
                        }
                        for (int n = 1; n < (2 * i); n++)
                        {
                            for (int num22 = 0; num22 <= (2 * i); num22++)
                            {
                                if (this.nodeLookup.TryGetValue(num16 + new Int3(0, num22, n), out next))
                                {
                                    while (next != null)
                                    {
                                        Vector3 vector8 = position - ((Vector3) next.position);
                                        float num23 = vector8.sqrMagnitude;
                                        if (num23 < positiveInfinity)
                                        {
                                            positiveInfinity = num23;
                                            node = next;
                                        }
                                        if ((constraint == null) || (((num23 < num3) && (num23 < num)) && constraint.Suitable(next)))
                                        {
                                            num3 = num23;
                                            node2 = next;
                                        }
                                        next = next.next;
                                    }
                                }
                                if (this.nodeLookup.TryGetValue(num16 + new Int3(2 * i, num22, n), out next))
                                {
                                    while (next != null)
                                    {
                                        Vector3 vector9 = position - ((Vector3) next.position);
                                        float num24 = vector9.sqrMagnitude;
                                        if (num24 < positiveInfinity)
                                        {
                                            positiveInfinity = num24;
                                            node = next;
                                        }
                                        if ((constraint == null) || (((num24 < num3) && (num24 < num)) && constraint.Suitable(next)))
                                        {
                                            num3 = num24;
                                            node2 = next;
                                        }
                                        next = next.next;
                                    }
                                }
                            }
                        }
                        for (int num25 = 1; num25 < (2 * i); num25++)
                        {
                            for (int num26 = 1; num26 < (2 * i); num26++)
                            {
                                if (this.nodeLookup.TryGetValue(num16 + new Int3(num25, 0, num26), out next))
                                {
                                    while (next != null)
                                    {
                                        Vector3 vector10 = position - ((Vector3) next.position);
                                        float num27 = vector10.sqrMagnitude;
                                        if (num27 < positiveInfinity)
                                        {
                                            positiveInfinity = num27;
                                            node = next;
                                        }
                                        if ((constraint == null) || (((num27 < num3) && (num27 < num)) && constraint.Suitable(next)))
                                        {
                                            num3 = num27;
                                            node2 = next;
                                        }
                                        next = next.next;
                                    }
                                }
                                if (this.nodeLookup.TryGetValue(num16 + new Int3(num25, 2 * i, num26), out next))
                                {
                                    while (next != null)
                                    {
                                        Vector3 vector11 = position - ((Vector3) next.position);
                                        float num28 = vector11.sqrMagnitude;
                                        if (num28 < positiveInfinity)
                                        {
                                            positiveInfinity = num28;
                                            node = next;
                                        }
                                        if ((constraint == null) || (((num28 < num3) && (num28 < num)) && constraint.Suitable(next)))
                                        {
                                            num3 = num28;
                                            node2 = next;
                                        }
                                        next = next.next;
                                    }
                                }
                            }
                        }
                    }
                    if (node2 != null)
                    {
                        num6 = Math.Min(num6, i + 1);
                    }
                }
            }
            else
            {
                for (int num29 = 0; num29 < this.nodeCount; num29++)
                {
                    PointNode node4 = this.nodes[num29];
                    Vector3 vector12 = position - ((Vector3) node4.position);
                    float num30 = vector12.sqrMagnitude;
                    if (num30 < positiveInfinity)
                    {
                        positiveInfinity = num30;
                        node = node4;
                    }
                    if ((constraint == null) || (((num30 < num3) && (num30 < num)) && constraint.Suitable(node4)))
                    {
                        num3 = num30;
                        node2 = node4;
                    }
                }
            }
            NNInfo info = new NNInfo(node) {
                constrainedNode = node2
            };
            if (node2 != null)
            {
                info.constClampedPosition = (Vector3) node2.position;
                return info;
            }
            if (node != null)
            {
                info.constrainedNode = node;
                info.constClampedPosition = (Vector3) node.position;
            }
            return info;
        }

        public override void GetNodes(GraphNodeDelegateCancelable del)
        {
            if (this.nodes != null)
            {
                for (int i = 0; (i < this.nodeCount) && del(this.nodes[i]); i++)
                {
                }
            }
        }

        public virtual bool IsValidConnection(GraphNode a, GraphNode b, out float dist)
        {
            dist = 0f;
            if (a.Walkable && b.Walkable)
            {
                Vector3 vector = (Vector3) (a.position - b.position);
                if (((!Mathf.Approximately(this.limits.x, 0f) && (Mathf.Abs(vector.x) > this.limits.x)) || (!Mathf.Approximately(this.limits.y, 0f) && (Mathf.Abs(vector.y) > this.limits.y))) || (!Mathf.Approximately(this.limits.z, 0f) && (Mathf.Abs(vector.z) > this.limits.z)))
                {
                    return false;
                }
                dist = vector.magnitude;
                if ((this.maxDistance == 0f) || (dist < this.maxDistance))
                {
                    if (!this.raycast)
                    {
                        return true;
                    }
                    Ray ray = new Ray((Vector3) a.position, (Vector3) (b.position - a.position));
                    Ray ray2 = new Ray((Vector3) b.position, (Vector3) (a.position - b.position));
                    if (this.thickRaycast)
                    {
                        if (!Physics.SphereCast(ray, this.thickRaycastRadius, dist, (int) this.mask) && !Physics.SphereCast(ray2, this.thickRaycastRadius, dist, (int) this.mask))
                        {
                            return true;
                        }
                    }
                    else if (!Physics.Raycast(ray, dist, (int) this.mask) && !Physics.Raycast(ray2, dist, (int) this.mask))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override void PostDeserialization()
        {
            this.RebuildNodeLookup();
        }

        public void RebuildNodeLookup()
        {
            if (this.optimizeForSparseGraph)
            {
                if (this.maxDistance == 0f)
                {
                    this.lookupCellSize = (Int3) this.limits;
                }
                else
                {
                    this.lookupCellSize.x = Mathf.CeilToInt(1000f * ((this.limits.x == 0f) ? this.maxDistance : Mathf.Min(this.maxDistance, this.limits.x)));
                    this.lookupCellSize.y = Mathf.CeilToInt(1000f * ((this.limits.y == 0f) ? this.maxDistance : Mathf.Min(this.maxDistance, this.limits.y)));
                    this.lookupCellSize.z = Mathf.CeilToInt(1000f * ((this.limits.z == 0f) ? this.maxDistance : Mathf.Min(this.maxDistance, this.limits.z)));
                }
                if (this.optimizeFor2D)
                {
                    this.lookupCellSize.y = 0;
                }
                if (this.nodeLookup == null)
                {
                    this.nodeLookup = new Dictionary<Int3, PointNode>();
                }
                this.nodeLookup.Clear();
                for (int i = 0; i < this.nodeCount; i++)
                {
                    PointNode node = this.nodes[i];
                    this.AddToLookup(node);
                }
            }
        }

        public override void RelocateNodes(Matrix4x4 oldMatrix, Matrix4x4 newMatrix)
        {
            base.RelocateNodes(oldMatrix, newMatrix);
            this.RebuildNodeLookup();
        }

        public override void ScanInternal(OnScanStatus statusCallback)
        {
            if (this.root == null)
            {
                GameObject[] objArray = GameObject.FindGameObjectsWithTag(this.searchTag);
                this.nodeGameObjects = objArray;
                if (objArray == null)
                {
                    this.nodes = new PointNode[0];
                    this.nodeCount = 0;
                    return;
                }
                this.nodes = new PointNode[objArray.Length];
                this.nodeCount = this.nodes.Length;
                for (int i = 0; i < this.nodes.Length; i++)
                {
                    this.nodes[i] = new PointNode(base.active);
                }
                for (int j = 0; j < objArray.Length; j++)
                {
                    this.nodes[j].SetPosition((Int3) objArray[j].transform.position);
                    this.nodes[j].Walkable = true;
                }
            }
            else if (!this.recursive)
            {
                this.nodes = new PointNode[this.root.childCount];
                this.nodeCount = this.nodes.Length;
                for (int k = 0; k < this.nodes.Length; k++)
                {
                    this.nodes[k] = new PointNode(base.active);
                }
                this.nodeGameObjects = new GameObject[this.nodes.Length];
                int index = 0;
                IEnumerator enumerator = this.root.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        Transform current = (Transform) enumerator.Current;
                        this.nodes[index].SetPosition((Int3) current.position);
                        this.nodes[index].Walkable = true;
                        this.nodeGameObjects[index] = current.gameObject;
                        index++;
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable == null)
                    {
                    }
                    disposable.Dispose();
                }
            }
            else
            {
                this.nodes = new PointNode[CountChildren(this.root)];
                this.nodeCount = this.nodes.Length;
                for (int m = 0; m < this.nodes.Length; m++)
                {
                    this.nodes[m] = new PointNode(base.active);
                }
                this.nodeGameObjects = new GameObject[this.nodes.Length];
                int c = 0;
                this.AddChildren(ref c, this.root);
            }
            if (this.optimizeForSparseGraph)
            {
                this.RebuildNodeLookup();
            }
            if (this.maxDistance >= 0f)
            {
                List<PointNode> list = new List<PointNode>(3);
                List<uint> list2 = new List<uint>(3);
                for (int n = 0; n < this.nodes.Length; n++)
                {
                    list.Clear();
                    list2.Clear();
                    PointNode a = this.nodes[n];
                    if (this.optimizeForSparseGraph)
                    {
                        Int3 num8 = this.WorldToLookupSpace(a.position);
                        int num9 = (this.lookupCellSize.y != 0) ? ThreeDNeighbours.Length : 9;
                        for (int num10 = 0; num10 < num9; num10++)
                        {
                            PointNode next;
                            Int3 key = num8 + ThreeDNeighbours[num10];
                            if (this.nodeLookup.TryGetValue(key, out next))
                            {
                                while (next != null)
                                {
                                    float dist = 0f;
                                    if (this.IsValidConnection(a, next, out dist))
                                    {
                                        list.Add(next);
                                        list2.Add((uint) Mathf.RoundToInt(dist * 1000f));
                                    }
                                    next = next.next;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int num13 = 0; num13 < this.nodes.Length; num13++)
                        {
                            if (n != num13)
                            {
                                PointNode b = this.nodes[num13];
                                float num14 = 0f;
                                if (this.IsValidConnection(a, b, out num14))
                                {
                                    list.Add(b);
                                    list2.Add((uint) Mathf.RoundToInt(num14 * 1000f));
                                }
                            }
                        }
                    }
                    a.connections = list.ToArray();
                    a.connectionCosts = list2.ToArray();
                }
            }
            this.nodeGameObjects = null;
        }

        public override void SerializeExtraInfo(GraphSerializationContext ctx)
        {
            if (this.nodes == null)
            {
                ctx.writer.Write(-1);
            }
            ctx.writer.Write(this.nodeCount);
            for (int i = 0; i < this.nodeCount; i++)
            {
                if (this.nodes[i] == null)
                {
                    ctx.writer.Write(-1);
                }
                else
                {
                    ctx.writer.Write(0);
                    this.nodes[i].SerializeNode(ctx);
                }
            }
        }

        public void UpdateArea(GraphUpdateObject guo)
        {
            if (this.nodes != null)
            {
                for (int i = 0; i < this.nodeCount; i++)
                {
                    if (guo.bounds.Contains((Vector3) this.nodes[i].position))
                    {
                        guo.WillUpdateNode(this.nodes[i]);
                        guo.Apply(this.nodes[i]);
                    }
                }
                if (guo.updatePhysics)
                {
                    Bounds bounds = guo.bounds;
                    if (this.thickRaycast)
                    {
                        bounds.Expand((float) (this.thickRaycastRadius * 2f));
                    }
                    List<GraphNode> list = ListPool<GraphNode>.Claim();
                    List<uint> list2 = ListPool<uint>.Claim();
                    for (int j = 0; j < this.nodeCount; j++)
                    {
                        PointNode a = this.nodes[j];
                        Vector3 position = (Vector3) a.position;
                        List<GraphNode> list3 = null;
                        List<uint> list4 = null;
                        for (int k = 0; k < this.nodeCount; k++)
                        {
                            if (k != j)
                            {
                                Vector3 b = (Vector3) this.nodes[k].position;
                                if (Polygon.LineIntersectsBounds(bounds, position, b))
                                {
                                    float num4;
                                    PointNode node = this.nodes[k];
                                    bool flag = a.ContainsConnection(node);
                                    if (!flag && this.IsValidConnection(a, node, out num4))
                                    {
                                        if (list3 == null)
                                        {
                                            list.Clear();
                                            list2.Clear();
                                            list3 = list;
                                            list4 = list2;
                                            list3.AddRange(a.connections);
                                            list4.AddRange(a.connectionCosts);
                                        }
                                        uint item = (uint) Mathf.RoundToInt(num4 * 1000f);
                                        list3.Add(node);
                                        list4.Add(item);
                                    }
                                    else if (flag && !this.IsValidConnection(a, node, out num4))
                                    {
                                        if (list3 == null)
                                        {
                                            list.Clear();
                                            list2.Clear();
                                            list3 = list;
                                            list4 = list2;
                                            list3.AddRange(a.connections);
                                            list4.AddRange(a.connectionCosts);
                                        }
                                        int index = list3.IndexOf(node);
                                        if (index != -1)
                                        {
                                            list3.RemoveAt(index);
                                            list4.RemoveAt(index);
                                        }
                                    }
                                }
                            }
                        }
                        if (list3 != null)
                        {
                            a.connections = list3.ToArray();
                            a.connectionCosts = list4.ToArray();
                        }
                    }
                    ListPool<GraphNode>.Release(list);
                    ListPool<uint>.Release(list2);
                }
            }
        }

        public void UpdateAreaInit(GraphUpdateObject o)
        {
        }

        private Int3 WorldToLookupSpace(Int3 p)
        {
            Int3 zero = Int3.zero;
            zero.x = (this.lookupCellSize.x == 0) ? 0 : (p.x / this.lookupCellSize.x);
            zero.y = (this.lookupCellSize.y == 0) ? 0 : (p.y / this.lookupCellSize.y);
            zero.z = (this.lookupCellSize.z == 0) ? 0 : (p.z / this.lookupCellSize.z);
            return zero;
        }
    }
}

