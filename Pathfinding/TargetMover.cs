﻿namespace Pathfinding
{
    using System;
    using UnityEngine;

    public class TargetMover : MonoBehaviour
    {
        private RichAI[] ais;
        private AIPath[] ais2;
        private Camera cam;
        public LayerMask mask;
        public bool onlyOnDoubleClick;
        public Transform target;

        public void OnGUI()
        {
            if ((this.onlyOnDoubleClick && (this.cam != null)) && ((UnityEngine.Event.current.type == UnityEngine.EventType.MouseDown) && (UnityEngine.Event.current.clickCount == 2)))
            {
                this.UpdateTargetPosition();
            }
        }

        public void Start()
        {
            this.cam = Camera.main;
            this.ais = UnityEngine.Object.FindObjectsOfType(typeof(RichAI)) as RichAI[];
            this.ais2 = UnityEngine.Object.FindObjectsOfType(typeof(AIPath)) as AIPath[];
        }

        private void Update()
        {
            if (!this.onlyOnDoubleClick && (this.cam != null))
            {
                this.UpdateTargetPosition();
            }
        }

        public void UpdateTargetPosition()
        {
            RaycastHit hit;
            if (Physics.Raycast(this.cam.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, (int) this.mask) && (hit.point != this.target.position))
            {
                this.target.position = hit.point;
                if ((this.ais != null) && this.onlyOnDoubleClick)
                {
                    for (int i = 0; i < this.ais.Length; i++)
                    {
                        if (this.ais[i] != null)
                        {
                            this.ais[i].UpdatePath();
                        }
                    }
                }
                if ((this.ais2 != null) && this.onlyOnDoubleClick)
                {
                    for (int j = 0; j < this.ais2.Length; j++)
                    {
                        if (this.ais2[j] != null)
                        {
                            this.ais2[j].SearchPath();
                        }
                    }
                }
            }
        }
    }
}

