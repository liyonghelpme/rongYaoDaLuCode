﻿namespace Pathfinding.WindowsStore
{
    using System;

    public class WindowsStoreCompatibility
    {
        public static Type GetTypeFromInfo(Type type)
        {
            return type;
        }

        public static Type GetTypeInfo(Type type)
        {
            return type;
        }
    }
}

