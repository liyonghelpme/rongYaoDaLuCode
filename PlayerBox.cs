﻿using com.game.module.core;
using com.liyong;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBox : MonoBehaviour
{
    private BoxObject boxObject;
    private UILabel gridId;
    private Vector3 lastPos;
    private uint mLastCameraType;
    private GameObject nguiObj;
    private int screenHeight = 100;
    private int screenWidth = 80;
    private List<GameObject> sprites = new List<GameObject>();
    public GameObject testLabel;
    public GameObject testSprite;
    public static GameObject uiRoot;
    private Vector3 worldSize = new Vector3(0.5f, 2f, 0.5f);

    private void Awake()
    {
    }

    private void OnDestroy()
    {
        this.boxObject.RemoveBox();
        foreach (GameObject obj2 in this.sprites)
        {
            UnityEngine.Object.Destroy(obj2);
        }
        this.sprites.Clear();
        UnityEngine.Object.Destroy(this.nguiObj);
    }

    private void Start()
    {
        BoxObject obj4 = new BoxObject {
            Width = this.screenWidth,
            Height = this.screenHeight
        };
        this.boxObject = obj4;
        Vector3 vector = Camera.main.WorldToScreenPoint(base.transform.position);
        this.boxObject.X = (int) vector.x;
        this.boxObject.Y = (int) vector.y;
        this.boxObject.gridManager = HealthBarManager.Instance.GetGridManager();
        this.boxObject.AddToGridManager();
        GameObject obj2 = new GameObject("playerScreenObject");
        UnityEngine.Object.Destroy(obj2);
        this.nguiObj = NGUITools.AddChild(uiRoot, obj2);
        this.lastPos = new Vector3(1000f, 0f, 0f);
    }

    private void Update()
    {
        uint cameraTypeCount = Singleton<MapMode>.Instance.GetCameraTypeCount();
        if (cameraTypeCount != this.mLastCameraType)
        {
            this.mLastCameraType = cameraTypeCount;
            this.UpdateCamera();
        }
        Vector3 vector5 = base.transform.position - this.lastPos;
        if (vector5.sqrMagnitude > 1f)
        {
            this.lastPos = base.transform.position;
            Vector3 position = Camera.main.WorldToScreenPoint(base.transform.position);
            position.z = 0f;
            Vector3 vector2 = UICamera.currentCamera.ScreenToWorldPoint(position);
            this.nguiObj.transform.position = vector2;
            Vector3 localPosition = this.nguiObj.transform.localPosition;
            this.boxObject.UpdateAttribute(this.screenWidth, this.screenHeight, (float) ((int) localPosition.x), (float) ((int) localPosition.y));
        }
    }

    private void UpdateCamera()
    {
        int[,] numArray = new int[,] { { 80, 100 }, { 80, 100 }, { 0x57, 0x94 }, { 0xcf, 350 } };
        this.screenWidth = numArray[this.mLastCameraType, 0];
        this.screenHeight = numArray[this.mLastCameraType, 1];
    }
}

