﻿using com.game.module.core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

public class PlaytList
{
    private int index;
    public List<IPlay> playList = new List<IPlay>();

    public void AddCoroutine(IEnumerator coroutine, int index = -1)
    {
        this.AddPlay(new Task(coroutine, false), index);
    }

    public void AddDelegate(EventDelegate.Callback callBack, int index = -1)
    {
        this.AddPlay(new DelegatePlay(callBack), index);
    }

    public void AddPlay(IPlay play, int index = -1)
    {
        if (play != null)
        {
            if (index == -1)
            {
                this.playList.Add(play);
            }
            else if ((index >= 0) && (index <= this.playList.Count))
            {
                this.playList.Insert(index, play);
            }
        }
    }

    public void AddTimeInterval(float seconds, EventDelegate.Callback callBack)
    {
        Task play = this.WaitForSeconds(seconds);
        EventDelegate.Add(play.onFinish, callBack);
        this.AddPlay(play, this.index);
    }

    public void AddTimeInterval(float seconds, int index = -1)
    {
        this.AddPlay(this.WaitForSeconds(seconds), index);
    }

    public void ClearPlay()
    {
        this.playList.Clear();
    }

    public void ContatPlay()
    {
        int num = 0;
        int num2 = this.playList.Count - 1;
        while (num < num2)
        {
            if (this.playList[num].OnEnd != null)
            {
                EventDelegate.Add(this.playList[num].OnEnd, new EventDelegate.Callback(this.PlayNext));
            }
            num++;
        }
    }

    public void Continue()
    {
        this.PlayNext();
    }

    private void PlayNext()
    {
        this.index++;
        if (this.index < this.playList.Count)
        {
            this.playList[this.index].Begin();
        }
    }

    public void Start()
    {
        this.ContatPlay();
        this.index = 0;
        if (this.playList.Count > 0)
        {
            this.playList[0].Begin();
        }
    }

    public Task WaitForSeconds(float seconds)
    {
        return new Task(this.WaitForSecondsHandle(seconds), false);
    }

    [DebuggerHidden]
    private IEnumerator WaitForSecondsHandle(float seconds)
    {
        return new <WaitForSecondsHandle>c__Iterator25 { seconds = seconds, <$>seconds = seconds };
    }

    [CompilerGenerated]
    private sealed class <WaitForSecondsHandle>c__Iterator25 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal float <$>seconds;
        internal float seconds;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.$current = new WaitForSeconds(this.seconds);
                    this.$PC = 1;
                    return true;

                case 1:
                    this.$PC = -1;
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }

    private class DelegatePlay : IPlay
    {
        private EventDelegate.Callback callBack;

        public DelegatePlay(EventDelegate.Callback callBack)
        {
            this.callBack = callBack;
        }

        public void Begin()
        {
            if (this.callBack != null)
            {
                this.callBack();
            }
        }

        public List<EventDelegate> OnEnd
        {
            get
            {
                return null;
            }
        }
    }
}

