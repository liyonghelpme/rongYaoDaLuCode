﻿namespace Proto
{
    using System;
    using System.IO;

    public class AchieveGetMsg_42_2
    {
        public ulong achieveId;
        public uint achievePoint;
        public ushort code;

        public static int getCode()
        {
            return 0x2a02;
        }

        public void read(MemoryStream msdata)
        {
            this.achieveId = proto_util.readULong(msdata);
            this.achievePoint = proto_util.readUInt(msdata);
            this.code = proto_util.readUShort(msdata);
        }
    }
}

