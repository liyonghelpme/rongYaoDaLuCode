﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class AchievePushMsg_42_3
    {
        public PAchieve achieveStruct = new PAchieve();

        public static int getCode()
        {
            return 0x2a03;
        }

        public void read(MemoryStream msdata)
        {
            this.achieveStruct.read(msdata);
        }
    }
}

