﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class AchieveReqMsg_42_1
    {
        public List<ulong> achieveGot = new List<ulong>();
        public uint achievePoint;
        public List<PAchieve> achieveStructList = new List<PAchieve>();
        public ushort length;

        public static int getCode()
        {
            return 0x2a01;
        }

        public void read(MemoryStream msdata)
        {
            this.length = proto_util.readUShort(msdata);
            this.achievePoint = proto_util.readUInt(msdata);
            PAchieve.readLoop(msdata, this.achieveStructList);
            proto_util.readLoopULong(msdata, this.achieveGot);
        }
    }
}

