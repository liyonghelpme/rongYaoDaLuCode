﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ActivityCountMsg_9_2
    {
        public List<PActivityCount> activityCount = new List<PActivityCount>();

        public static int getCode()
        {
            return 0x902;
        }

        public void read(MemoryStream msdata)
        {
            PActivityCount.readLoop(msdata, this.activityCount);
        }
    }
}

