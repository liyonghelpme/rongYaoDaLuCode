﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ActivityInfoMsg_9_3
    {
        public List<PActivityInfo> activityInfo = new List<PActivityInfo>();

        public static int getCode()
        {
            return 0x903;
        }

        public void read(MemoryStream msdata)
        {
            PActivityInfo.readLoop(msdata, this.activityInfo);
        }
    }
}

