﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ActivityStateMsg_9_1
    {
        public List<PActivityState> activityState = new List<PActivityState>();

        public static int getCode()
        {
            return 0x901;
        }

        public void read(MemoryStream msdata)
        {
            PActivityState.readLoop(msdata, this.activityState);
        }
    }
}

