﻿namespace Proto
{
    using System;
    using System.IO;

    public class ArenaBuyTimeMsg_43_5
    {
        public ushort buyTimes;
        public ushort code;

        public static int getCode()
        {
            return 0x2b05;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.buyTimes = proto_util.readUShort(msdata);
        }
    }
}

