﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ArenaChallengeMsg_43_3
    {
        public List<PGeneralFightAttr> anemyList = new List<PGeneralFightAttr>();
        public ushort code;
        public List<PGeneralFightAttr> myList = new List<PGeneralFightAttr>();

        public static int getCode()
        {
            return 0x2b03;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PGeneralFightAttr.readLoop(msdata, this.myList);
            PGeneralFightAttr.readLoop(msdata, this.anemyList);
        }
    }
}

