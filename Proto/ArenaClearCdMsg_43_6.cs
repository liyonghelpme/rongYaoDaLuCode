﻿namespace Proto
{
    using System;
    using System.IO;

    public class ArenaClearCdMsg_43_6
    {
        public uint cd;
        public ushort code;

        public static int getCode()
        {
            return 0x2b06;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.cd = proto_util.readUInt(msdata);
        }
    }
}

