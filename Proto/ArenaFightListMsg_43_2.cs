﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ArenaFightListMsg_43_2
    {
        public ushort code;
        public List<PArenaFightInfo> list = new List<PArenaFightInfo>();

        public static int getCode()
        {
            return 0x2b02;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PArenaFightInfo.readLoop(msdata, this.list);
        }
    }
}

