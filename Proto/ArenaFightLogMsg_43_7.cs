﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ArenaFightLogMsg_43_7
    {
        public ushort code;
        public List<PArenaFightLog> logList = new List<PArenaFightLog>();

        public static int getCode()
        {
            return 0x2b07;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PArenaFightLog.readLoop(msdata, this.logList);
        }
    }
}

