﻿namespace Proto
{
    using System;
    using System.IO;

    public class ArenaRefreshFightListMsg_43_9
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2b09;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

