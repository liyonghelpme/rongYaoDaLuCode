﻿namespace Proto
{
    using System;
    using System.IO;

    public class ArenaResultMsg_43_4
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2b04;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

