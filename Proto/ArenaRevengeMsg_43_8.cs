﻿namespace Proto
{
    using System;
    using System.IO;

    public class ArenaRevengeMsg_43_8
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2b08;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

