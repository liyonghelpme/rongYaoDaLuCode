﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class ArenaRevengeUserInfoMsg_43_10
    {
        public PArenaFightInfo info = new PArenaFightInfo();

        public static int getCode()
        {
            return 0x2b0a;
        }

        public void read(MemoryStream msdata)
        {
            this.info.read(msdata);
        }
    }
}

