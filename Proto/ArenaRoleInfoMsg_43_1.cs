﻿namespace Proto
{
    using System;
    using System.IO;

    public class ArenaRoleInfoMsg_43_1
    {
        public uint buyTimes;
        public uint cd;
        public ushort challengeTimes;
        public ushort code;
        public uint pos;

        public static int getCode()
        {
            return 0x2b01;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.pos = proto_util.readUInt(msdata);
            this.challengeTimes = proto_util.readUShort(msdata);
            this.cd = proto_util.readUInt(msdata);
            this.buyTimes = proto_util.readUInt(msdata);
        }
    }
}

