﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class BuffAddSyncMsg_35_4
    {
        public List<PDamageBuff> buffList = new List<PDamageBuff>();
        public ulong id;
        public byte type;

        public static int getCode()
        {
            return 0x2304;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            PDamageBuff.readLoop(msdata, this.buffList);
        }
    }
}

