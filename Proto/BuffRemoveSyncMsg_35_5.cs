﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class BuffRemoveSyncMsg_35_5
    {
        public List<PBuffRemover> buffList = new List<PBuffRemover>();
        public ulong id;
        public byte type;

        public static int getCode()
        {
            return 0x2305;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            PBuffRemover.readLoop(msdata, this.buffList);
        }
    }
}

