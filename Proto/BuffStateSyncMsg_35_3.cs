﻿namespace Proto
{
    using System;
    using System.IO;

    public class BuffStateSyncMsg_35_3
    {
        public int buffId;
        public int buffLvl;
        public ulong id;
        public byte operation;
        public byte type;

        public static int getCode()
        {
            return 0x2303;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            this.operation = proto_util.readUByte(msdata);
            this.buffId = proto_util.readInt(msdata);
            this.buffLvl = proto_util.readInt(msdata);
        }
    }
}

