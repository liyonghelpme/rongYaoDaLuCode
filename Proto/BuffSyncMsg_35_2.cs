﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class BuffSyncMsg_35_2
    {
        public List<PSimpleBuff> buffList = new List<PSimpleBuff>();
        public byte buffSyncType;
        public ulong id;
        public byte type;

        public static int getCode()
        {
            return 0x2302;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            this.buffSyncType = proto_util.readUByte(msdata);
            PSimpleBuff.readLoop(msdata, this.buffList);
        }
    }
}

