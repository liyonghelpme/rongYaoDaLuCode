﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class BuffSysBuffMsg_35_1
    {
        public List<PSysBuff> list = new List<PSysBuff>();

        public static int getCode()
        {
            return 0x2301;
        }

        public void read(MemoryStream msdata)
        {
            PSysBuff.readLoop(msdata, this.list);
        }
    }
}

