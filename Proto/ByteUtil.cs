﻿namespace Proto
{
    using System;
    using UnityEngine;

    public class ByteUtil : MonoBehaviour
    {
        public static int Byte2Int(byte[] data)
        {
            int num = 0;
            for (int i = 0; i < (data.Length - 1); i++)
            {
                num += (data[i] * 0x100) * ((data.Length - i) - 1);
            }
            return (num + data[data.Length - 1]);
        }

        public static byte[] Number2Bytes(int src, int len)
        {
            byte[] buffer = new byte[len];
            for (short i = 0; i < len; i = (short) (i + 1))
            {
                buffer[(len - i) - 1] = (byte) (src & 0xff);
                src = src >> 8;
            }
            return buffer;
        }
    }
}

