﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatBasicInfoReqMsg_10_10
    {
        public ushort code;
        public ulong roleId;
        public byte roleJob;
        public byte roleLvl;
        public string roleName = string.Empty;
        public byte roleSex;

        public static int getCode()
        {
            return 0xa0a;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.roleId = proto_util.readULong(msdata);
            this.roleName = proto_util.readString(msdata);
            this.roleJob = proto_util.readUByte(msdata);
            this.roleLvl = proto_util.readUByte(msdata);
            this.roleSex = proto_util.readUByte(msdata);
        }
    }
}

