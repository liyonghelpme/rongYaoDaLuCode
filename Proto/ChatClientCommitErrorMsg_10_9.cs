﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatClientCommitErrorMsg_10_9
    {
        public ushort code;

        public static int getCode()
        {
            return 0xa09;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

