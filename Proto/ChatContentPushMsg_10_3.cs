﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ChatContentPushMsg_10_3
    {
        public byte chatType;
        public string content = string.Empty;
        public List<PChatPushGoods> goodsList = new List<PChatPushGoods>();
        public ulong senderId;
        public byte senderJob;
        public byte senderLvl;
        public string senderName = string.Empty;
        public byte senderNation;
        public short senderRoleIcon;
        public byte senderSex;
        public byte senderVip;
        public ushort serverId;

        public static int getCode()
        {
            return 0xa03;
        }

        public void read(MemoryStream msdata)
        {
            this.chatType = proto_util.readUByte(msdata);
            this.senderId = proto_util.readULong(msdata);
            this.serverId = proto_util.readUShort(msdata);
            this.senderName = proto_util.readString(msdata);
            this.senderNation = proto_util.readUByte(msdata);
            this.senderSex = proto_util.readUByte(msdata);
            this.senderJob = proto_util.readUByte(msdata);
            this.senderLvl = proto_util.readUByte(msdata);
            this.senderVip = proto_util.readUByte(msdata);
            this.senderRoleIcon = proto_util.readShort(msdata);
            this.content = proto_util.readString(msdata);
            PChatPushGoods.readLoop(msdata, this.goodsList);
        }
    }
}

