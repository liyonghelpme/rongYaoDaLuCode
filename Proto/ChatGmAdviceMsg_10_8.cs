﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatGmAdviceMsg_10_8
    {
        public ushort code;

        public static int getCode()
        {
            return 0xa08;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

