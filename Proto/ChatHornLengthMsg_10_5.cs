﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatHornLengthMsg_10_5
    {
        public byte length;

        public static int getCode()
        {
            return 0xa05;
        }

        public void read(MemoryStream msdata)
        {
            this.length = proto_util.readUByte(msdata);
        }
    }
}

