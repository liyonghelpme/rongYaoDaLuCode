﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatPrivateChatMsg_10_2
    {
        public ushort code;
        public ulong roleId;
        public string roleName = string.Empty;

        public static int getCode()
        {
            return 0xa02;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.roleId = proto_util.readULong(msdata);
            this.roleName = proto_util.readString(msdata);
        }
    }
}

