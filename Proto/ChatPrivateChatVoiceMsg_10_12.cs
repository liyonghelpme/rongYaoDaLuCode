﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatPrivateChatVoiceMsg_10_12
    {
        public ushort code;
        public ulong roleId;
        public string roleName = string.Empty;
        public uint voiceId;

        public static int getCode()
        {
            return 0xa0c;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.roleId = proto_util.readULong(msdata);
            this.roleName = proto_util.readString(msdata);
            this.voiceId = proto_util.readUInt(msdata);
        }
    }
}

