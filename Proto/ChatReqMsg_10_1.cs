﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatReqMsg_10_1
    {
        public ushort code;

        public static int getCode()
        {
            return 0xa01;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

