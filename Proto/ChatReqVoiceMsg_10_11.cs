﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatReqVoiceMsg_10_11
    {
        public ushort code;
        public uint voiceId;

        public static int getCode()
        {
            return 0xa0b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.voiceId = proto_util.readUInt(msdata);
        }
    }
}

