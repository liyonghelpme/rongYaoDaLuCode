﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ChatRumorMsg_10_7
    {
        public byte channel;
        public List<PRumor> content = new List<PRumor>();
        public byte must;
        public uint typeId;

        public static int getCode()
        {
            return 0xa07;
        }

        public void read(MemoryStream msdata)
        {
            this.channel = proto_util.readUByte(msdata);
            this.typeId = proto_util.readUInt(msdata);
            this.must = proto_util.readUByte(msdata);
            PRumor.readLoop(msdata, this.content);
        }
    }
}

