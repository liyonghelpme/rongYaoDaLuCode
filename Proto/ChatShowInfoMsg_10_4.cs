﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ChatShowInfoMsg_10_4
    {
        public ushort code;
        public List<PItems> goodsInfo = new List<PItems>();
        public byte type;

        public static int getCode()
        {
            return 0xa04;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
            PItems.readLoop(msdata, this.goodsInfo);
        }
    }
}

