﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatSysAnounceMsg_10_6
    {
        public string content = string.Empty;
        public string httpLink = string.Empty;

        public static int getCode()
        {
            return 0xa06;
        }

        public void read(MemoryStream msdata)
        {
            this.content = proto_util.readString(msdata);
            this.httpLink = proto_util.readString(msdata);
        }
    }
}

