﻿namespace Proto
{
    using System;
    using System.IO;

    public class ChatVoiceContentPushMsg_10_14
    {
        public byte chatType;
        public string contentText = string.Empty;
        public ulong senderId;
        public byte senderJob;
        public byte senderLvl;
        public string senderName = string.Empty;
        public byte senderNation;
        public short senderRoleIcon;
        public byte senderSex;
        public byte senderVip;
        public ushort serverId;
        public uint voiceId;
        public byte voiceTime;

        public static int getCode()
        {
            return 0xa0e;
        }

        public void read(MemoryStream msdata)
        {
            this.chatType = proto_util.readUByte(msdata);
            this.senderId = proto_util.readULong(msdata);
            this.serverId = proto_util.readUShort(msdata);
            this.senderName = proto_util.readString(msdata);
            this.senderNation = proto_util.readUByte(msdata);
            this.senderSex = proto_util.readUByte(msdata);
            this.senderJob = proto_util.readUByte(msdata);
            this.senderLvl = proto_util.readUByte(msdata);
            this.senderVip = proto_util.readUByte(msdata);
            this.senderRoleIcon = proto_util.readShort(msdata);
            this.voiceId = proto_util.readUInt(msdata);
            this.voiceTime = proto_util.readUByte(msdata);
            this.contentText = proto_util.readString(msdata);
        }
    }
}

