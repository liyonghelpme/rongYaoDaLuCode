﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ChatVoiceInfoReqMsg_10_13
    {
        public ushort code;
        public List<byte> contentVoice = new List<byte>();
        public uint voiceId;

        public static int getCode()
        {
            return 0xa0d;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.voiceId = proto_util.readUInt(msdata);
            proto_util.readLoopUByte(msdata, this.contentVoice);
        }
    }
}

