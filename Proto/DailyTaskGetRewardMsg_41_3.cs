﻿namespace Proto
{
    using System;
    using System.IO;

    public class DailyTaskGetRewardMsg_41_3
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2903;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

