﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class DailyTaskUpdateDailyMsg_41_2
    {
        public List<PEachDaily> dailyList = new List<PEachDaily>();

        public static int getCode()
        {
            return 0x2902;
        }

        public void read(MemoryStream msdata)
        {
            PEachDaily.readLoop(msdata, this.dailyList);
        }
    }
}

