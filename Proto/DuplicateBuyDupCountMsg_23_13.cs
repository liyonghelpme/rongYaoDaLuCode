﻿namespace Proto
{
    using System;
    using System.IO;

    public class DuplicateBuyDupCountMsg_23_13
    {
        public ushort code;

        public static int getCode()
        {
            return 0x170d;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

