﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class DuplicateDupAwardMsg_23_5
    {
        public List<PDuplicateAward> awardList = new List<PDuplicateAward>();
        public byte grade;
        public ushort time;
        public byte type;

        public static int getCode()
        {
            return 0x1705;
        }

        public void read(MemoryStream msdata)
        {
            this.type = proto_util.readUByte(msdata);
            this.grade = proto_util.readUByte(msdata);
            this.time = proto_util.readUShort(msdata);
            PDuplicateAward.readLoop(msdata, this.awardList);
        }
    }
}

