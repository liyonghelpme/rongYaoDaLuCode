﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class DuplicateDupDropMsg_23_8
    {
        public List<PDuplicateDrop> dropList = new List<PDuplicateDrop>();
        public uint gold;
        public ulong monId;

        public static int getCode()
        {
            return 0x1708;
        }

        public void read(MemoryStream msdata)
        {
            this.monId = proto_util.readULong(msdata);
            this.gold = proto_util.readUInt(msdata);
            PDuplicateDrop.readLoop(msdata, this.dropList);
        }
    }
}

