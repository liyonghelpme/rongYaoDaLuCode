﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class DuplicateDupEndMsg_23_10
    {
        public ushort code;
        public List<PDuplicateMopUp> dropList = new List<PDuplicateMopUp>();

        public static int getCode()
        {
            return 0x170a;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PDuplicateMopUp.readLoop(msdata, this.dropList);
        }
    }
}

