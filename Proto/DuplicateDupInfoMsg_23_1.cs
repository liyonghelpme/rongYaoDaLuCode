﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class DuplicateDupInfoMsg_23_1
    {
        public ushort code;
        public List<PDuplicateInfo> dupList = new List<PDuplicateInfo>();

        public static int getCode()
        {
            return 0x1701;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PDuplicateInfo.readLoop(msdata, this.dupList);
        }
    }
}

