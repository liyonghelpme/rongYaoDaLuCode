﻿namespace Proto
{
    using System;
    using System.IO;

    public class DuplicateDupResumeMsg_23_7
    {
        public ushort code;
        public uint dupId;
        public ushort gold;
        public ushort itemCount;
        public uint remainTime;

        public static int getCode()
        {
            return 0x1707;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.remainTime = proto_util.readUInt(msdata);
            this.dupId = proto_util.readUInt(msdata);
            this.itemCount = proto_util.readUShort(msdata);
            this.gold = proto_util.readUShort(msdata);
        }
    }
}

