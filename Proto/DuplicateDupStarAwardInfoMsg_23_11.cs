﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class DuplicateDupStarAwardInfoMsg_23_11
    {
        public ushort code;
        public List<PDupStarAward> stateList = new List<PDupStarAward>();

        public static int getCode()
        {
            return 0x170b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PDupStarAward.readLoop(msdata, this.stateList);
        }
    }
}

