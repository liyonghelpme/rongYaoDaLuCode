﻿namespace Proto
{
    using System;
    using System.IO;

    public class DuplicateEnterMissionMsg_23_3
    {
        public ushort code;
        public uint missionId;

        public static int getCode()
        {
            return 0x1703;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.missionId = proto_util.readUInt(msdata);
        }
    }
}

