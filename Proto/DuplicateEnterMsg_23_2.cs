﻿namespace Proto
{
    using System;
    using System.IO;

    public class DuplicateEnterMsg_23_2
    {
        public ushort code;
        public uint dupId;
        public uint missionId;

        public static int getCode()
        {
            return 0x1702;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.dupId = proto_util.readUInt(msdata);
            this.missionId = proto_util.readUInt(msdata);
        }
    }
}

