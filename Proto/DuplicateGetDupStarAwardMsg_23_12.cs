﻿namespace Proto
{
    using System;
    using System.IO;

    public class DuplicateGetDupStarAwardMsg_23_12
    {
        public ushort code;

        public static int getCode()
        {
            return 0x170c;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

