﻿namespace Proto
{
    using System;
    using System.IO;

    public class DuplicateQuitDupMsg_23_4
    {
        public ushort code;

        public static int getCode()
        {
            return 0x1704;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

