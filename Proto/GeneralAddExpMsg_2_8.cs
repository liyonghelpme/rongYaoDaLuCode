﻿namespace Proto
{
    using System;
    using System.IO;

    public class GeneralAddExpMsg_2_8
    {
        public ushort code;

        public static int getCode()
        {
            return 520;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

