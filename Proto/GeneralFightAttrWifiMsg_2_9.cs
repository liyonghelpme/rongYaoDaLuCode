﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GeneralFightAttrWifiMsg_2_9
    {
        public ushort code;
        public List<PGeneralFightAttr> generalAttr = new List<PGeneralFightAttr>();
        public List<PNewAttr> generalEquipAttr = new List<PNewAttr>();

        public static int getCode()
        {
            return 0x209;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PGeneralFightAttr.readLoop(msdata, this.generalAttr);
            PNewAttr.readLoop(msdata, this.generalEquipAttr);
        }
    }
}

