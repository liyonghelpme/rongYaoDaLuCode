﻿namespace Proto
{
    using System;
    using System.IO;

    public class GeneralGeneralLvlMsg_2_6
    {
        public ushort code;
        public ulong exp;
        public ulong generalId;
        public ushort lvl;

        public static int getCode()
        {
            return 0x206;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.generalId = proto_util.readULong(msdata);
            this.lvl = proto_util.readUShort(msdata);
            this.exp = proto_util.readULong(msdata);
        }
    }
}

