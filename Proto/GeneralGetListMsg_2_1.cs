﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GeneralGetListMsg_2_1
    {
        public List<PGeneralInfo> listInfo = new List<PGeneralInfo>();

        public static int getCode()
        {
            return 0x201;
        }

        public void read(MemoryStream msdata)
        {
            PGeneralInfo.readLoop(msdata, this.listInfo);
        }
    }
}

