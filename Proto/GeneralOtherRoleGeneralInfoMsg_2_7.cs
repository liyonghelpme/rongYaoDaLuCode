﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GeneralOtherRoleGeneralInfoMsg_2_7
    {
        public ushort code;
        public List<POtherRoleGeneralInfo> generalInfoList = new List<POtherRoleGeneralInfo>();

        public static int getCode()
        {
            return 0x207;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            POtherRoleGeneralInfo.readLoop(msdata, this.generalInfoList);
        }
    }
}

