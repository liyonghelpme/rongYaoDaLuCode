﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GeneralSetFightListMsg_2_2
    {
        public ushort code;
        public List<PSetGeneralInfo> setList = new List<PSetGeneralInfo>();
        public byte type;

        public static int getCode()
        {
            return 0x202;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
            PSetGeneralInfo.readLoop(msdata, this.setList);
        }
    }
}

