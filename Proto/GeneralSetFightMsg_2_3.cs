﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GeneralSetFightMsg_2_3
    {
        public ushort code;
        public ulong dupGeneralId;
        public uint fightGeneralHp;
        public List<PGeneralFightAttr> generalAttr = new List<PGeneralFightAttr>();

        public static int getCode()
        {
            return 0x203;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.dupGeneralId = proto_util.readULong(msdata);
            this.fightGeneralHp = proto_util.readUInt(msdata);
            PGeneralFightAttr.readLoop(msdata, this.generalAttr);
        }
    }
}

