﻿namespace Proto
{
    using System;
    using System.IO;

    public class GeneralStarMsg_2_5
    {
        public ushort code;
        public ulong generalId;
        public byte star;

        public static int getCode()
        {
            return 0x205;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.generalId = proto_util.readULong(msdata);
            this.star = proto_util.readUByte(msdata);
        }
    }
}

