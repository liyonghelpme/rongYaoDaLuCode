﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsBuyMsg_5_16
    {
        public ushort code;
        public uint templateId;

        public static int getCode()
        {
            return 0x510;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.templateId = proto_util.readUInt(msdata);
        }
    }
}

