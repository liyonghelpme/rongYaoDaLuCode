﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GoodsDeleteMsg_5_2
    {
        public List<ulong> id = new List<ulong>();
        public byte repos;

        public static int getCode()
        {
            return 0x502;
        }

        public void read(MemoryStream msdata)
        {
            this.repos = proto_util.readUByte(msdata);
            proto_util.readLoopULong(msdata, this.id);
        }
    }
}

