﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsDestroyMsg_5_10
    {
        public ushort code;
        public byte repos;
        public uint templateId;

        public static int getCode()
        {
            return 0x50a;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.templateId = proto_util.readUInt(msdata);
            this.repos = proto_util.readUByte(msdata);
        }
    }
}

