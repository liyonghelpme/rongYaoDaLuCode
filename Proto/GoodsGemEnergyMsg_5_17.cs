﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsGemEnergyMsg_5_17
    {
        public ushort energy;
        public ulong gemId;

        public static int getCode()
        {
            return 0x511;
        }

        public void read(MemoryStream msdata)
        {
            this.gemId = proto_util.readULong(msdata);
            this.energy = proto_util.readUShort(msdata);
        }
    }
}

