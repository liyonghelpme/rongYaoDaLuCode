﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GoodsInfoMsg_5_1
    {
        public List<PItems> goodsInfo = new List<PItems>();
        public byte repos;

        public static int getCode()
        {
            return 0x501;
        }

        public void read(MemoryStream msdata)
        {
            this.repos = proto_util.readUByte(msdata);
            PItems.readLoop(msdata, this.goodsInfo);
        }
    }
}

