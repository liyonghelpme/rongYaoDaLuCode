﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsPackTidyMsg_5_5
    {
        public ushort code;

        public static int getCode()
        {
            return 0x505;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

