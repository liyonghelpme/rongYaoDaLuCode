﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsPutonMsg_5_11
    {
        public ushort code;

        public static int getCode()
        {
            return 0x50b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

