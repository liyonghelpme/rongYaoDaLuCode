﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsSaleMsg_5_15
    {
        public ushort code;
        public uint templateId;

        public static int getCode()
        {
            return 0x50f;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.templateId = proto_util.readUInt(msdata);
        }
    }
}

