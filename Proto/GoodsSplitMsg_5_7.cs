﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsSplitMsg_5_7
    {
        public ushort code;

        public static int getCode()
        {
            return 0x507;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

