﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsSwapMsg_5_6
    {
        public ushort code;

        public static int getCode()
        {
            return 0x506;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

