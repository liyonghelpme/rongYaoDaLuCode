﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsTakeoffMsg_5_12
    {
        public ushort code;

        public static int getCode()
        {
            return 0x50c;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

