﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsToStorageBagMsg_5_14
    {
        public ushort code;

        public static int getCode()
        {
            return 0x50e;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

