﻿namespace Proto
{
    using System;
    using System.IO;

    public class GoodsUseMsg_5_9
    {
        public ushort code;
        public ushort templateId;

        public static int getCode()
        {
            return 0x509;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.templateId = proto_util.readUShort(msdata);
        }
    }
}

