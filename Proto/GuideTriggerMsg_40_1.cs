﻿namespace Proto
{
    using System;
    using System.IO;

    public class GuideTriggerMsg_40_1
    {
        public byte guideId;

        public static int getCode()
        {
            return 0x2801;
        }

        public void read(MemoryStream msdata)
        {
            this.guideId = proto_util.readUByte(msdata);
        }
    }
}

