﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemBatchUseMsg_36_5
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2405;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

