﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemEquipLvUpMsg_36_7
    {
        public ushort code;
        public ulong id;

        public static int getCode()
        {
            return 0x2407;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.code = proto_util.readUShort(msdata);
        }
    }
}

