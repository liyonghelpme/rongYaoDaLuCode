﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemEquipTreasureMountMsg_36_8
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2408;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

