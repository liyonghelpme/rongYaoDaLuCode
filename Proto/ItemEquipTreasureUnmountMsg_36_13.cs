﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemEquipTreasureUnmountMsg_36_13
    {
        public ushort code;

        public static int getCode()
        {
            return 0x240d;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

