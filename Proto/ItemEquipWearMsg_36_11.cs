﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemEquipWearMsg_36_11
    {
        public ushort code;

        public static int getCode()
        {
            return 0x240b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

