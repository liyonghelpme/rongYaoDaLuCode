﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ItemListMsg_36_1
    {
        public List<PItems> itemInfo = new List<PItems>();
        public byte repos;

        public static int getCode()
        {
            return 0x2401;
        }

        public void read(MemoryStream msdata)
        {
            this.repos = proto_util.readUByte(msdata);
            PItems.readLoop(msdata, this.itemInfo);
        }
    }
}

