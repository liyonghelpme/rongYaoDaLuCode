﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ItemLvPutMaterialMsg_36_6
    {
        public ushort code;
        public List<PMaterial> materialIds = new List<PMaterial>();

        public static int getCode()
        {
            return 0x2406;
        }

        public void read(MemoryStream msdata)
        {
            PMaterial.readLoop(msdata, this.materialIds);
            this.code = proto_util.readUShort(msdata);
        }
    }
}

