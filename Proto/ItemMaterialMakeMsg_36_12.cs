﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemMaterialMakeMsg_36_12
    {
        public ushort code;

        public static int getCode()
        {
            return 0x240c;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

