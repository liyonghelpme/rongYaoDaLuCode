﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemSellMsg_36_3
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2403;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

