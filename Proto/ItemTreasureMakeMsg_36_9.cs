﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemTreasureMakeMsg_36_9
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2409;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

