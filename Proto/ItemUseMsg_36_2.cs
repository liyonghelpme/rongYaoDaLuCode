﻿namespace Proto
{
    using System;
    using System.IO;

    public class ItemUseMsg_36_2
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2402;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

