﻿namespace Proto
{
    using System;
    using System.IO;

    public class LoginClientInfoMsg_1_6
    {
        public ushort code;

        public static int getCode()
        {
            return 0x106;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

