﻿namespace Proto
{
    using System;
    using System.IO;

    public class LoginCreateRoleMsg_1_3
    {
        public ushort code;
        public ulong id;

        public static int getCode()
        {
            return 0x103;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.id = proto_util.readULong(msdata);
        }
    }
}

