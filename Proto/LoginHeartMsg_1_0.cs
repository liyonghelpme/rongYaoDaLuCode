﻿namespace Proto
{
    using System;
    using System.IO;

    public class LoginHeartMsg_1_0
    {
        public uint serverTime;

        public static int getCode()
        {
            return 0x100;
        }

        public void read(MemoryStream msdata)
        {
            this.serverTime = proto_util.readUInt(msdata);
        }
    }
}

