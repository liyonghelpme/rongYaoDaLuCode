﻿namespace Proto
{
    using System;
    using System.IO;

    public class LoginLoginKeyMsg_1_8
    {
        public uint loginKey;

        public static int getCode()
        {
            return 0x108;
        }

        public void read(MemoryStream msdata)
        {
            this.loginKey = proto_util.readUInt(msdata);
        }
    }
}

