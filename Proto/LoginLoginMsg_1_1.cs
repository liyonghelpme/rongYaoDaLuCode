﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class LoginLoginMsg_1_1
    {
        public ushort code;
        public List<PLoginInfo> loginInfo = new List<PLoginInfo>();

        public static int getCode()
        {
            return 0x101;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PLoginInfo.readLoop(msdata, this.loginInfo);
        }
    }
}

