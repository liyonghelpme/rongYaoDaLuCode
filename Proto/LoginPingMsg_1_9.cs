﻿namespace Proto
{
    using System;
    using System.IO;

    public class LoginPingMsg_1_9
    {
        public ulong csend;
        public ulong ssend;

        public static int getCode()
        {
            return 0x109;
        }

        public void read(MemoryStream msdata)
        {
            this.csend = proto_util.readULong(msdata);
            this.ssend = proto_util.readULong(msdata);
        }
    }
}

