﻿namespace Proto
{
    using System;
    using System.IO;

    public class LoginRejectMsg_1_4
    {
        public byte code;

        public static int getCode()
        {
            return 260;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUByte(msdata);
        }
    }
}

