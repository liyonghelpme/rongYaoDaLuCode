﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class LoginSelectRoleMsg_1_2
    {
        public ushort code;
        public byte newbieDuplicate;
        public List<PRole> role = new List<PRole>();
        public uint time;
        public string version = string.Empty;

        public static int getCode()
        {
            return 0x102;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.version = proto_util.readString(msdata);
            this.time = proto_util.readUInt(msdata);
            PRole.readLoop(msdata, this.role);
            this.newbieDuplicate = proto_util.readUByte(msdata);
        }
    }
}

