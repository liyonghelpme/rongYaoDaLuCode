﻿namespace Proto
{
    using System;
    using System.IO;

    public class LoginStopMsg_1_5
    {
        public uint time;

        public static int getCode()
        {
            return 0x105;
        }

        public void read(MemoryStream msdata)
        {
            this.time = proto_util.readUInt(msdata);
        }
    }
}

