﻿namespace Proto
{
    using System;
    using System.IO;

    public class MailDelAllMsg_12_7
    {
        public ushort code;

        public static int getCode()
        {
            return 0xc07;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

