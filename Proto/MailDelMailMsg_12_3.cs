﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MailDelMailMsg_12_3
    {
        public ushort code;
        public List<ulong> mailIds = new List<ulong>();
        public byte totalCount;

        public static int getCode()
        {
            return 0xc03;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.totalCount = proto_util.readUByte(msdata);
            proto_util.readLoopULong(msdata, this.mailIds);
        }
    }
}

