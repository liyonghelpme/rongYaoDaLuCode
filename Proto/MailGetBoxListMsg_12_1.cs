﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MailGetBoxListMsg_12_1
    {
        public ushort code;
        public List<PMailBasicInfo> mailList = new List<PMailBasicInfo>();
        public byte totalCount;

        public static int getCode()
        {
            return 0xc01;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.totalCount = proto_util.readUByte(msdata);
            PMailBasicInfo.readLoop(msdata, this.mailList);
        }
    }
}

