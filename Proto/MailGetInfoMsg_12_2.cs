﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MailGetInfoMsg_12_2
    {
        public ushort code;
        public string content = string.Empty;
        public uint diam;
        public uint diamBind;
        public uint gold;
        public List<PMailAttach> mailAttachList = new List<PMailAttach>();
        public ulong mailId;
        public List<PMailOther> mailOtherDataList = new List<PMailOther>();
        public uint sendTime;
        public string title = string.Empty;

        public static int getCode()
        {
            return 0xc02;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.mailId = proto_util.readULong(msdata);
            this.title = proto_util.readString(msdata);
            this.sendTime = proto_util.readUInt(msdata);
            this.content = proto_util.readString(msdata);
            this.gold = proto_util.readUInt(msdata);
            this.diam = proto_util.readUInt(msdata);
            this.diamBind = proto_util.readUInt(msdata);
            PMailAttach.readLoop(msdata, this.mailAttachList);
            PMailOther.readLoop(msdata, this.mailOtherDataList);
        }
    }
}

