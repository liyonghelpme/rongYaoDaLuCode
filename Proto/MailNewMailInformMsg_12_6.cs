﻿namespace Proto
{
    using System;
    using System.IO;

    public class MailNewMailInformMsg_12_6
    {
        public ushort code;
        public byte mailCount;

        public static int getCode()
        {
            return 0xc06;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.mailCount = proto_util.readUByte(msdata);
        }
    }
}

