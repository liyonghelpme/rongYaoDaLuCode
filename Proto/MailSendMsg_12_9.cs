﻿namespace Proto
{
    using System;
    using System.IO;

    public class MailSendMsg_12_9
    {
        public ushort code;

        public static int getCode()
        {
            return 0xc09;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

