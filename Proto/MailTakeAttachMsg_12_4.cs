﻿namespace Proto
{
    using System;
    using System.IO;

    public class MailTakeAttachMsg_12_4
    {
        public ushort code;
        public ulong mailId;

        public static int getCode()
        {
            return 0xc04;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.mailId = proto_util.readULong(msdata);
        }
    }
}

