﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapAttackEnergySyncMsg_4_19
    {
        public uint attackEnergy;
        public uint fixTime;

        public static int getCode()
        {
            return 0x413;
        }

        public void read(MemoryStream msdata)
        {
            this.attackEnergy = proto_util.readUInt(msdata);
            this.fixTime = proto_util.readUInt(msdata);
        }
    }
}

