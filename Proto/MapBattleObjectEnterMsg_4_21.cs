﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapBattleObjectEnterMsg_4_21
    {
        public List<PBattleObject> battleObjectList = new List<PBattleObject>();

        public static int getCode()
        {
            return 0x415;
        }

        public void read(MemoryStream msdata)
        {
            PBattleObject.readLoop(msdata, this.battleObjectList);
        }
    }
}

