﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapEnterDoorMsg_4_18
    {
        public ushort code;

        public static int getCode()
        {
            return 0x412;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

