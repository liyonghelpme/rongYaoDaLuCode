﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapEnterReqMsg_4_17
    {
        public uint mapid;
        public int x;
        public int y;

        public static int getCode()
        {
            return 0x411;
        }

        public void read(MemoryStream msdata)
        {
            this.mapid = proto_util.readUInt(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
        }
    }
}

