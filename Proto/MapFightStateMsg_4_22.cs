﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapFightStateMsg_4_22
    {
        public ulong roleId;
        public byte state;

        public static int getCode()
        {
            return 0x416;
        }

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
            this.state = proto_util.readUByte(msdata);
        }
    }
}

