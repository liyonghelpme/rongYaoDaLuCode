﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapFinishMsg_4_15
    {
        public ushort code;

        public static int getCode()
        {
            return 0x40f;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

