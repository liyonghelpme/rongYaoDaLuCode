﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapMonBornSightMsg_4_23
    {
        public List<PMapMon> monList = new List<PMapMon>();
        public ulong summorId;
        public byte syncType;

        public static int getCode()
        {
            return 0x417;
        }

        public void read(MemoryStream msdata)
        {
            this.summorId = proto_util.readULong(msdata);
            this.syncType = proto_util.readUByte(msdata);
            PMapMon.readLoop(msdata, this.monList);
        }
    }
}

