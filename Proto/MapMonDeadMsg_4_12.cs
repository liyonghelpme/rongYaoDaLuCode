﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapMonDeadMsg_4_12
    {
        public ushort code;
        public ulong id;

        public static int getCode()
        {
            return 0x40c;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.code = proto_util.readUShort(msdata);
        }
    }
}

