﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapMonEnterMsg_4_7
    {
        public List<PMapMon> mons = new List<PMapMon>();

        public static int getCode()
        {
            return 0x407;
        }

        public void read(MemoryStream msdata)
        {
            PMapMon.readLoop(msdata, this.mons);
        }
    }
}

