﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapMonLeaveMsg_4_8
    {
        public List<ulong> ids = new List<ulong>();

        public static int getCode()
        {
            return 0x408;
        }

        public void read(MemoryStream msdata)
        {
            proto_util.readLoopULong(msdata, this.ids);
        }
    }
}

