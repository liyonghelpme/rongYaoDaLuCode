﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapMonMoveMsg_4_9
    {
        public ulong id;
        public ushort speed;
        public int x;
        public int y;

        public static int getCode()
        {
            return 0x409;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.speed = proto_util.readUShort(msdata);
        }
    }
}

