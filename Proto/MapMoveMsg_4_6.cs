﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapMoveMsg_4_6
    {
        public ulong id;
        public byte moveStatus;
        public int x;
        public int y;

        public static int getCode()
        {
            return 0x406;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.moveStatus = proto_util.readUByte(msdata);
        }
    }
}

