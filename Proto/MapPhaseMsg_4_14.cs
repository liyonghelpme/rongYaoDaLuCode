﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapPhaseMsg_4_14
    {
        public List<uint> box = new List<uint>();
        public ushort phase;

        public static int getCode()
        {
            return 0x40e;
        }

        public void read(MemoryStream msdata)
        {
            this.phase = proto_util.readUShort(msdata);
            proto_util.readLoopUInt(msdata, this.box);
        }
    }
}

