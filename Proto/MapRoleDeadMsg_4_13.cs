﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapRoleDeadMsg_4_13
    {
        public ulong id;

        public static int getCode()
        {
            return 0x40d;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
        }
    }
}

