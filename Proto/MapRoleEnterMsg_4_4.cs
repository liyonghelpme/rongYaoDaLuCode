﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapRoleEnterMsg_4_4
    {
        public List<PMapRole> roles = new List<PMapRole>();

        public static int getCode()
        {
            return 0x404;
        }

        public void read(MemoryStream msdata)
        {
            PMapRole.readLoop(msdata, this.roles);
        }
    }
}

