﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapRoleLeaveMsg_4_5
    {
        public List<ulong> roles = new List<ulong>();

        public static int getCode()
        {
            return 0x405;
        }

        public void read(MemoryStream msdata)
        {
            proto_util.readLoopULong(msdata, this.roles);
        }
    }
}

