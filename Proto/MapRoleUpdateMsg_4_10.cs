﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapRoleUpdateMsg_4_10
    {
        public List<PItem> changeList = new List<PItem>();
        public ulong id;

        public static int getCode()
        {
            return 0x40a;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            PItem.readLoop(msdata, this.changeList);
        }
    }
}

