﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MapSightMsg_4_2
    {
        public uint mapId;
        public List<PMapMon> monsEnter = new List<PMapMon>();
        public List<PMapRole> rolesEnter = new List<PMapRole>();
        public int x;
        public int y;
        public int z;

        public static int getCode()
        {
            return 0x402;
        }

        public void read(MemoryStream msdata)
        {
            this.mapId = proto_util.readUInt(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            PMapRole.readLoop(msdata, this.rolesEnter);
            PMapMon.readLoop(msdata, this.monsEnter);
        }
    }
}

