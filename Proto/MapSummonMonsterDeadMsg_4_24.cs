﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapSummonMonsterDeadMsg_4_24
    {
        public ulong summorId;

        public static int getCode()
        {
            return 0x418;
        }

        public void read(MemoryStream msdata)
        {
            this.summorId = proto_util.readULong(msdata);
        }
    }
}

