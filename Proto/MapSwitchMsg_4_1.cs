﻿namespace Proto
{
    using System;
    using System.IO;

    public class MapSwitchMsg_4_1
    {
        public ushort code;
        public string name = string.Empty;

        public static int getCode()
        {
            return 0x401;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.name = proto_util.readString(msdata);
        }
    }
}

