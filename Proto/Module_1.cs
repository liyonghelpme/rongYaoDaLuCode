﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_1
    {
        public static void write_1_0(MemoryStream msdata)
        {
        }

        public static void write_1_1(MemoryStream msdata, string accname, string key, byte fcm, string platform, string token, uint timestamp, string internalIp)
        {
            proto_util.writeString(msdata, accname);
            proto_util.writeString(msdata, key);
            proto_util.writeUByte(msdata, fcm);
            proto_util.writeString(msdata, platform);
            proto_util.writeString(msdata, token);
            proto_util.writeUInt(msdata, timestamp);
            proto_util.writeString(msdata, internalIp);
        }

        public static void write_1_2(MemoryStream msdata, ulong id, uint key)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUInt(msdata, key);
        }

        public static void write_1_3(MemoryStream msdata, string name, byte job, byte sex, uint general, uint servId)
        {
            proto_util.writeString(msdata, name);
            proto_util.writeUByte(msdata, job);
            proto_util.writeUByte(msdata, sex);
            proto_util.writeUInt(msdata, general);
            proto_util.writeUInt(msdata, servId);
        }

        public static void write_1_6(MemoryStream msdata, string os, string osVer, string device, string deviceType, string screen, string mno, string nm, string platform, uint servId)
        {
            proto_util.writeString(msdata, os);
            proto_util.writeString(msdata, osVer);
            proto_util.writeString(msdata, device);
            proto_util.writeString(msdata, deviceType);
            proto_util.writeString(msdata, screen);
            proto_util.writeString(msdata, mno);
            proto_util.writeString(msdata, nm);
            proto_util.writeString(msdata, platform);
            proto_util.writeUInt(msdata, servId);
        }

        public static void write_1_7(MemoryStream msdata, ushort step)
        {
            proto_util.writeUShort(msdata, step);
        }

        public static void write_1_9(MemoryStream msdata, ulong csend)
        {
            proto_util.writeULong(msdata, csend);
        }
    }
}

