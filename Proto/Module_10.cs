﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_10
    {
        public static void write_10_1(MemoryStream msdata, byte chatType, string content, List<PChatGoods> goodsList)
        {
            proto_util.writeUByte(msdata, chatType);
            proto_util.writeString(msdata, content);
            PChatGoods.writeLoop(msdata, goodsList);
        }

        public static void write_10_10(MemoryStream msdata, ulong roleId)
        {
            proto_util.writeULong(msdata, roleId);
        }

        public static void write_10_11(MemoryStream msdata, byte chatType, string contentText, byte voiceTime, List<byte> contentVoice)
        {
            proto_util.writeUByte(msdata, chatType);
            proto_util.writeString(msdata, contentText);
            proto_util.writeUByte(msdata, voiceTime);
            proto_util.writeLoopUByte(msdata, contentVoice);
        }

        public static void write_10_12(MemoryStream msdata, ulong roleId, string contentText, byte voiceTime, List<byte> contentVoice)
        {
            proto_util.writeULong(msdata, roleId);
            proto_util.writeString(msdata, contentText);
            proto_util.writeUByte(msdata, voiceTime);
            proto_util.writeLoopUByte(msdata, contentVoice);
        }

        public static void write_10_13(MemoryStream msdata, uint voiceId)
        {
            proto_util.writeUInt(msdata, voiceId);
        }

        public static void write_10_2(MemoryStream msdata, ulong roleId, string content, List<PChatGoods> goodsList)
        {
            proto_util.writeULong(msdata, roleId);
            proto_util.writeString(msdata, content);
            PChatGoods.writeLoop(msdata, goodsList);
        }

        public static void write_10_4(MemoryStream msdata, ulong id, byte type)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
        }

        public static void write_10_5(MemoryStream msdata)
        {
        }

        public static void write_10_8(MemoryStream msdata, string title, string content)
        {
            proto_util.writeString(msdata, title);
            proto_util.writeString(msdata, content);
        }

        public static void write_10_9(MemoryStream msdata, string flashver, byte errType, string desc)
        {
            proto_util.writeString(msdata, flashver);
            proto_util.writeUByte(msdata, errType);
            proto_util.writeString(msdata, desc);
        }
    }
}

