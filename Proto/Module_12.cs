﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_12
    {
        public static void write_12_1(MemoryStream msdata)
        {
        }

        public static void write_12_2(MemoryStream msdata, ulong mailId)
        {
            proto_util.writeULong(msdata, mailId);
        }

        public static void write_12_3(MemoryStream msdata, List<ulong> mailIds)
        {
            proto_util.writeLoopULong(msdata, mailIds);
        }

        public static void write_12_4(MemoryStream msdata, ulong mailId)
        {
            proto_util.writeULong(msdata, mailId);
        }

        public static void write_12_7(MemoryStream msdata)
        {
        }

        public static void write_12_8(MemoryStream msdata)
        {
        }

        public static void write_12_9(MemoryStream msdata, List<ulong> recvIdList, string title, string content, uint diam, uint gold, List<PMailAttach> attachList)
        {
            proto_util.writeLoopULong(msdata, recvIdList);
            proto_util.writeString(msdata, title);
            proto_util.writeString(msdata, content);
            proto_util.writeUInt(msdata, diam);
            proto_util.writeUInt(msdata, gold);
            PMailAttach.writeLoop(msdata, attachList);
        }
    }
}

