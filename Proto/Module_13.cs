﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_13
    {
        public static void write_13_0(MemoryStream msdata, uint skillId)
        {
            proto_util.writeUInt(msdata, skillId);
        }

        public static void write_13_1(MemoryStream msdata, byte seq, uint skillId, byte dir, List<PTarget> target)
        {
            proto_util.writeUByte(msdata, seq);
            proto_util.writeUInt(msdata, skillId);
            proto_util.writeUByte(msdata, dir);
            PTarget.writeLoop(msdata, target);
        }

        public static void write_13_11(MemoryStream msdata, ulong id, byte type, uint skillId, int eularY)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
            proto_util.writeUInt(msdata, skillId);
            proto_util.writeInt(msdata, eularY);
        }

        public static void write_13_12(MemoryStream msdata, ushort seq, ulong actorId, int actorType, uint skillId, List<PDamage> damageList)
        {
            proto_util.writeUShort(msdata, seq);
            proto_util.writeULong(msdata, actorId);
            proto_util.writeInt(msdata, actorType);
            proto_util.writeUInt(msdata, skillId);
            PDamage.writeLoop(msdata, damageList);
        }

        public static void write_13_4(MemoryStream msdata)
        {
        }

        public static void write_13_5(MemoryStream msdata, uint skillId, uint nextId)
        {
            proto_util.writeUInt(msdata, skillId);
            proto_util.writeUInt(msdata, nextId);
        }

        public static void write_13_6(MemoryStream msdata, List<PSkill> skillids)
        {
            PSkill.writeLoop(msdata, skillids);
        }

        public static void write_13_7(MemoryStream msdata, List<uint> skillids)
        {
            proto_util.writeLoopUInt(msdata, skillids);
        }
    }
}

