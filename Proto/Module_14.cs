﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_14
    {
        public static void write_14_1(MemoryStream msdata, uint pvpId, bool isMaster, int x, int y, int z)
        {
            proto_util.writeUInt(msdata, pvpId);
            proto_util.writeBool(msdata, isMaster);
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
            proto_util.writeInt(msdata, z);
        }

        public static void write_14_10(MemoryStream msdata)
        {
        }

        public static void write_14_11(MemoryStream msdata)
        {
        }

        public static void write_14_14(MemoryStream msdata, List<PMapMon> monList)
        {
            PMapMon.writeLoop(msdata, monList);
        }

        public static void write_14_15(MemoryStream msdata, List<PPvpPrivate> requestPrivateList)
        {
            PPvpPrivate.writeLoop(msdata, requestPrivateList);
        }

        public static void write_14_16(MemoryStream msdata, List<PMapMon> privateList)
        {
            PMapMon.writeLoop(msdata, privateList);
        }

        public static void write_14_17(MemoryStream msdata, ulong id, byte type, uint delta, uint endHp)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
            proto_util.writeUInt(msdata, delta);
            proto_util.writeUInt(msdata, endHp);
        }

        public static void write_14_18(MemoryStream msdata, byte soul)
        {
            proto_util.writeUByte(msdata, soul);
        }

        public static void write_14_19(MemoryStream msdata, List<PBattleObject> battleObjectList)
        {
            PBattleObject.writeLoop(msdata, battleObjectList);
        }

        public static void write_14_2(MemoryStream msdata, byte status)
        {
            proto_util.writeUByte(msdata, status);
        }

        public static void write_14_20(MemoryStream msdata, List<PBattleObject> battleObjectList)
        {
            PBattleObject.writeLoop(msdata, battleObjectList);
        }

        public static void write_14_21(MemoryStream msdata, ulong generalId, int x, int y, int z, int rotateY, ulong battleObjectId)
        {
            proto_util.writeULong(msdata, generalId);
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
            proto_util.writeInt(msdata, z);
            proto_util.writeInt(msdata, rotateY);
            proto_util.writeULong(msdata, battleObjectId);
        }

        public static void write_14_22(MemoryStream msdata, ulong summorId, byte syncType, List<PMapMon> monList)
        {
            proto_util.writeULong(msdata, summorId);
            proto_util.writeUByte(msdata, syncType);
            PMapMon.writeLoop(msdata, monList);
        }

        public static void write_14_3(MemoryStream msdata, byte sender, List<PCoordinator> coordinator, List<PNewAttr> generalAttr, List<PNewAttr> generalEquipAttr)
        {
            proto_util.writeUByte(msdata, sender);
            PCoordinator.writeLoop(msdata, coordinator);
            PNewAttr.writeLoop(msdata, generalAttr);
            PNewAttr.writeLoop(msdata, generalEquipAttr);
        }

        public static void write_14_4(MemoryStream msdata, bool isForce, List<PCoordinator> coordinator)
        {
            proto_util.writeBool(msdata, isForce);
            PCoordinator.writeLoop(msdata, coordinator);
        }

        public static void write_14_5(MemoryStream msdata, ulong id, byte type, uint skillId, int eularY, ulong targetId, byte targetType)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
            proto_util.writeUInt(msdata, skillId);
            proto_util.writeInt(msdata, eularY);
            proto_util.writeULong(msdata, targetId);
            proto_util.writeUByte(msdata, targetType);
        }

        public static void write_14_6(MemoryStream msdata, ushort seq, ulong actorId, int actorType, uint skillId, List<PDamage> damageList)
        {
            proto_util.writeUShort(msdata, seq);
            proto_util.writeULong(msdata, actorId);
            proto_util.writeInt(msdata, actorType);
            proto_util.writeUInt(msdata, skillId);
            PDamage.writeLoop(msdata, damageList);
        }

        public static void write_14_7(MemoryStream msdata, ulong id, int x, int y, int z)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
            proto_util.writeInt(msdata, z);
        }

        public static void write_14_8(MemoryStream msdata, ulong id, int x, int y, int z)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
            proto_util.writeInt(msdata, z);
        }

        public static void write_14_9(MemoryStream msdata, List<PCoordinator> coordinatorList)
        {
            PCoordinator.writeLoop(msdata, coordinatorList);
        }
    }
}

