﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_15
    {
        public static void write_15_1(MemoryStream msdata, byte type)
        {
            proto_util.writeUByte(msdata, type);
        }

        public static void write_15_2(MemoryStream msdata, byte type, ulong shopId, uint amount)
        {
            proto_util.writeUByte(msdata, type);
            proto_util.writeULong(msdata, shopId);
            proto_util.writeUInt(msdata, amount);
        }

        public static void write_15_3(MemoryStream msdata, byte type)
        {
            proto_util.writeUByte(msdata, type);
        }
    }
}

