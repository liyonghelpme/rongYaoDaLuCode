﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_19
    {
        public static void write_19_1(MemoryStream msdata)
        {
        }

        public static void write_19_2(MemoryStream msdata, ulong generalId, uint skillGroupId)
        {
            proto_util.writeULong(msdata, generalId);
            proto_util.writeUInt(msdata, skillGroupId);
        }

        public static void write_19_4(MemoryStream msdata, byte skillPointRequest)
        {
            proto_util.writeUByte(msdata, skillPointRequest);
        }
    }
}

