﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_2
    {
        public static void write_2_1(MemoryStream msdata)
        {
        }

        public static void write_2_2(MemoryStream msdata, byte type, List<PSetGeneralInfo> setList)
        {
            proto_util.writeUByte(msdata, type);
            PSetGeneralInfo.writeLoop(msdata, setList);
        }

        public static void write_2_3(MemoryStream msdata, ulong id, uint hp)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUInt(msdata, hp);
        }

        public static void write_2_4(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_2_5(MemoryStream msdata, ulong generalId)
        {
            proto_util.writeULong(msdata, generalId);
        }

        public static void write_2_6(MemoryStream msdata, ulong generalId)
        {
            proto_util.writeULong(msdata, generalId);
        }

        public static void write_2_7(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_2_8(MemoryStream msdata, ulong id, uint itemId, ushort count)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUInt(msdata, itemId);
            proto_util.writeUShort(msdata, count);
        }
    }
}

