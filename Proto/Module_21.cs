﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_21
    {
        public static void write_21_1(MemoryStream msdata, ushort type, ushort counter)
        {
            proto_util.writeUShort(msdata, type);
            proto_util.writeUShort(msdata, counter);
        }
    }
}

