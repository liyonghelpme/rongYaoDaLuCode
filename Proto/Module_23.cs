﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_23
    {
        public static void write_23_1(MemoryStream msdata)
        {
        }

        public static void write_23_10(MemoryStream msdata, string checkKey, uint dupId, byte isSucc, uint time, byte star, ushort killCount, List<ushort> chest)
        {
            proto_util.writeString(msdata, checkKey);
            proto_util.writeUInt(msdata, dupId);
            proto_util.writeUByte(msdata, isSucc);
            proto_util.writeUInt(msdata, time);
            proto_util.writeUByte(msdata, star);
            proto_util.writeUShort(msdata, killCount);
            proto_util.writeLoopUShort(msdata, chest);
        }

        public static void write_23_11(MemoryStream msdata)
        {
        }

        public static void write_23_12(MemoryStream msdata, uint id)
        {
            proto_util.writeUInt(msdata, id);
        }

        public static void write_23_13(MemoryStream msdata, uint dupId)
        {
            proto_util.writeUInt(msdata, dupId);
        }

        public static void write_23_2(MemoryStream msdata, uint dupId, int x, int y, int z)
        {
            proto_util.writeUInt(msdata, dupId);
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
            proto_util.writeInt(msdata, z);
        }

        public static void write_23_3(MemoryStream msdata)
        {
        }

        public static void write_23_4(MemoryStream msdata)
        {
        }

        public static void write_23_5(MemoryStream msdata)
        {
        }

        public static void write_23_6(MemoryStream msdata)
        {
        }

        public static void write_23_7(MemoryStream msdata)
        {
        }

        public static void write_23_8(MemoryStream msdata)
        {
        }

        public static void write_23_9(MemoryStream msdata, uint dupId, byte count)
        {
            proto_util.writeUInt(msdata, dupId);
            proto_util.writeUByte(msdata, count);
        }
    }
}

