﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_3
    {
        public static void write_3_1(MemoryStream msdata)
        {
        }

        public static void write_3_12(MemoryStream msdata, byte inBackground)
        {
            proto_util.writeUByte(msdata, inBackground);
        }

        public static void write_3_13(MemoryStream msdata)
        {
        }

        public static void write_3_14(MemoryStream msdata, uint plot)
        {
            proto_util.writeUInt(msdata, plot);
        }

        public static void write_3_2(MemoryStream msdata)
        {
        }

        public static void write_3_20(MemoryStream msdata, byte type)
        {
            proto_util.writeUByte(msdata, type);
        }

        public static void write_3_3(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_3_40(MemoryStream msdata, byte type)
        {
            proto_util.writeUByte(msdata, type);
        }

        public static void write_3_42(MemoryStream msdata)
        {
        }

        public static void write_3_43(MemoryStream msdata, byte type)
        {
            proto_util.writeUByte(msdata, type);
        }

        public static void write_3_44(MemoryStream msdata, byte state, int x, int y, int z, int dir)
        {
            proto_util.writeUByte(msdata, state);
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
            proto_util.writeInt(msdata, z);
            proto_util.writeInt(msdata, dir);
        }

        public static void write_3_5(MemoryStream msdata)
        {
        }

        public static void write_3_50(MemoryStream msdata)
        {
        }

        public static void write_3_6(MemoryStream msdata, uint hp, uint mp)
        {
            proto_util.writeUInt(msdata, hp);
            proto_util.writeUInt(msdata, mp);
        }

        public static void write_3_7(MemoryStream msdata, uint iconId)
        {
            proto_util.writeUInt(msdata, iconId);
        }

        public static void write_3_8(MemoryStream msdata, string name)
        {
            proto_util.writeString(msdata, name);
        }

        public static void write_3_9(MemoryStream msdata, ulong generalId)
        {
            proto_util.writeULong(msdata, generalId);
        }
    }
}

