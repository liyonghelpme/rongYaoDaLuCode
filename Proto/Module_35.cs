﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_35
    {
        public static void write_35_1(MemoryStream msdata)
        {
        }

        public static void write_35_2(MemoryStream msdata, ulong id, byte type, byte buffSyncType, List<PSimpleBuff> buffList)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
            proto_util.writeUByte(msdata, buffSyncType);
            PSimpleBuff.writeLoop(msdata, buffList);
        }

        public static void write_35_3(MemoryStream msdata, ulong id, byte type, byte operation, int buffId, int buffLvl)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
            proto_util.writeUByte(msdata, operation);
            proto_util.writeInt(msdata, buffId);
            proto_util.writeInt(msdata, buffLvl);
        }

        public static void write_35_4(MemoryStream msdata, ulong id, byte type, List<PDamageBuff> buffList)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
            PDamageBuff.writeLoop(msdata, buffList);
        }

        public static void write_35_5(MemoryStream msdata, ulong id, byte type, List<PBuffRemover> buffList)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, type);
            PBuffRemover.writeLoop(msdata, buffList);
        }
    }
}

