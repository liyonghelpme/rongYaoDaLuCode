﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_36
    {
        public static void write_36_1(MemoryStream msdata, byte repos)
        {
            proto_util.writeUByte(msdata, repos);
        }

        public static void write_36_10(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_36_11(MemoryStream msdata, ulong itemId, ulong generalId)
        {
            proto_util.writeULong(msdata, itemId);
            proto_util.writeULong(msdata, generalId);
        }

        public static void write_36_12(MemoryStream msdata, uint itemTid, ushort num)
        {
            proto_util.writeUInt(msdata, itemTid);
            proto_util.writeUShort(msdata, num);
        }

        public static void write_36_13(MemoryStream msdata, ulong id, uint treasureTid)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUInt(msdata, treasureTid);
        }

        public static void write_36_2(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_36_3(MemoryStream msdata, ulong id, uint number)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUInt(msdata, number);
        }

        public static void write_36_5(MemoryStream msdata, ulong id, ushort count)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUShort(msdata, count);
        }

        public static void write_36_6(MemoryStream msdata, ulong id, List<PMaterial> materialIds)
        {
            proto_util.writeULong(msdata, id);
            PMaterial.writeLoop(msdata, materialIds);
        }

        public static void write_36_7(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_36_8(MemoryStream msdata, ulong id, List<ulong> materialIds)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeLoopULong(msdata, materialIds);
        }

        public static void write_36_9(MemoryStream msdata, uint tid, uint num)
        {
            proto_util.writeUInt(msdata, tid);
            proto_util.writeUInt(msdata, num);
        }
    }
}

