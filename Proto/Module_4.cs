﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_4
    {
        public static void write_4_1(MemoryStream msdata, uint mapid, int x, int y)
        {
            proto_util.writeUInt(msdata, mapid);
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
        }

        public static void write_4_12(MemoryStream msdata, ulong id, uint lastKill, uint hp, uint mp)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUInt(msdata, lastKill);
            proto_util.writeUInt(msdata, hp);
            proto_util.writeUInt(msdata, mp);
        }

        public static void write_4_13(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_4_18(MemoryStream msdata, uint id)
        {
            proto_util.writeUInt(msdata, id);
        }

        public static void write_4_19(MemoryStream msdata, uint attackEnergy, uint fixTime)
        {
            proto_util.writeUInt(msdata, attackEnergy);
            proto_util.writeUInt(msdata, fixTime);
        }

        public static void write_4_2(MemoryStream msdata)
        {
        }

        public static void write_4_20(MemoryStream msdata, byte rebornCount, ulong parentMonId, int parentMonTemplateId, int monTemplateId, int rebornX, int rebornY, int rebornZ)
        {
            proto_util.writeUByte(msdata, rebornCount);
            proto_util.writeULong(msdata, parentMonId);
            proto_util.writeInt(msdata, parentMonTemplateId);
            proto_util.writeInt(msdata, monTemplateId);
            proto_util.writeInt(msdata, rebornX);
            proto_util.writeInt(msdata, rebornY);
            proto_util.writeInt(msdata, rebornZ);
        }

        public static void write_4_22(MemoryStream msdata)
        {
        }

        public static void write_4_23(MemoryStream msdata, ulong summorId, byte syncType, List<PMapMon> monList)
        {
            proto_util.writeULong(msdata, summorId);
            proto_util.writeUByte(msdata, syncType);
            PMapMon.writeLoop(msdata, monList);
        }

        public static void write_4_24(MemoryStream msdata, ulong summorId)
        {
            proto_util.writeULong(msdata, summorId);
        }

        public static void write_4_6(MemoryStream msdata, int x, int y, ushort moveStatus)
        {
            proto_util.writeInt(msdata, x);
            proto_util.writeInt(msdata, y);
            proto_util.writeUShort(msdata, moveStatus);
        }
    }
}

