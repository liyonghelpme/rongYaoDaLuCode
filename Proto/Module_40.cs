﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_40
    {
        public static void write_40_1(MemoryStream msdata, byte triggerType)
        {
            proto_util.writeUByte(msdata, triggerType);
        }

        public static void write_40_2(MemoryStream msdata, byte guideId)
        {
            proto_util.writeUByte(msdata, guideId);
        }
    }
}

