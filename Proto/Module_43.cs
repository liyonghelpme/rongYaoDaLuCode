﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_43
    {
        public static void write_43_1(MemoryStream msdata)
        {
        }

        public static void write_43_10(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_43_2(MemoryStream msdata)
        {
        }

        public static void write_43_3(MemoryStream msdata, ulong id, uint pos)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUInt(msdata, pos);
        }

        public static void write_43_4(MemoryStream msdata, string checkKey, uint time, byte result)
        {
            proto_util.writeString(msdata, checkKey);
            proto_util.writeUInt(msdata, time);
            proto_util.writeUByte(msdata, result);
        }

        public static void write_43_5(MemoryStream msdata)
        {
        }

        public static void write_43_6(MemoryStream msdata)
        {
        }

        public static void write_43_7(MemoryStream msdata)
        {
        }

        public static void write_43_8(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_43_9(MemoryStream msdata)
        {
        }
    }
}

