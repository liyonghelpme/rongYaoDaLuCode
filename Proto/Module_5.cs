﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_5
    {
        public static void write_5_1(MemoryStream msdata, byte repos)
        {
            proto_util.writeUByte(msdata, repos);
        }

        public static void write_5_10(MemoryStream msdata, ulong id, byte repos)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, repos);
        }

        public static void write_5_11(MemoryStream msdata, ulong id, byte pos)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, pos);
        }

        public static void write_5_12(MemoryStream msdata, ulong id, byte pos)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUByte(msdata, pos);
        }

        public static void write_5_13(MemoryStream msdata, ulong id, ushort count)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeUShort(msdata, count);
        }

        public static void write_5_14(MemoryStream msdata, byte repos, ulong id)
        {
            proto_util.writeUByte(msdata, repos);
            proto_util.writeULong(msdata, id);
        }

        public static void write_5_15(MemoryStream msdata, ulong id)
        {
            proto_util.writeULong(msdata, id);
        }

        public static void write_5_16(MemoryStream msdata, uint npcId, uint shopId, uint templateId, ushort count)
        {
            proto_util.writeUInt(msdata, npcId);
            proto_util.writeUInt(msdata, shopId);
            proto_util.writeUInt(msdata, templateId);
            proto_util.writeUShort(msdata, count);
        }

        public static void write_5_17(MemoryStream msdata, ulong gemId)
        {
            proto_util.writeULong(msdata, gemId);
        }

        public static void write_5_5(MemoryStream msdata, byte repos)
        {
            proto_util.writeUByte(msdata, repos);
        }

        public static void write_5_6(MemoryStream msdata, byte repos, ushort from, ushort to)
        {
            proto_util.writeUByte(msdata, repos);
            proto_util.writeUShort(msdata, from);
            proto_util.writeUShort(msdata, to);
        }

        public static void write_5_7(MemoryStream msdata, byte repos, ulong id, ushort count)
        {
            proto_util.writeUByte(msdata, repos);
            proto_util.writeULong(msdata, id);
            proto_util.writeUShort(msdata, count);
        }

        public static void write_5_9(MemoryStream msdata, ulong id, string name)
        {
            proto_util.writeULong(msdata, id);
            proto_util.writeString(msdata, name);
        }
    }
}

