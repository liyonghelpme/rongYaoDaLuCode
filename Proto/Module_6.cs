﻿namespace Proto
{
    using System;
    using System.IO;

    public class Module_6
    {
        public static void write_6_1(MemoryStream msdata)
        {
        }

        public static void write_6_10(MemoryStream msdata, ulong taskId, uint npcId)
        {
            proto_util.writeULong(msdata, taskId);
            proto_util.writeUInt(msdata, npcId);
        }

        public static void write_6_11(MemoryStream msdata, ushort type)
        {
            proto_util.writeUShort(msdata, type);
        }

        public static void write_6_13(MemoryStream msdata, ushort type)
        {
            proto_util.writeUShort(msdata, type);
        }

        public static void write_6_14(MemoryStream msdata)
        {
        }

        public static void write_6_15(MemoryStream msdata)
        {
        }

        public static void write_6_2(MemoryStream msdata, ulong taskId, uint npcId)
        {
            proto_util.writeULong(msdata, taskId);
            proto_util.writeUInt(msdata, npcId);
        }

        public static void write_6_3(MemoryStream msdata, ulong taskId)
        {
            proto_util.writeULong(msdata, taskId);
        }

        public static void write_6_4(MemoryStream msdata, ulong taskId, uint npcId)
        {
            proto_util.writeULong(msdata, taskId);
            proto_util.writeUInt(msdata, npcId);
        }

        public static void write_6_6(MemoryStream msdata)
        {
        }

        public static void write_6_7(MemoryStream msdata)
        {
        }

        public static void write_6_8(MemoryStream msdata, ulong taskId)
        {
            proto_util.writeULong(msdata, taskId);
        }

        public static void write_6_9(MemoryStream msdata)
        {
        }
    }
}

