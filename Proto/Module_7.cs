﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class Module_7
    {
        public static void write_7_1(MemoryStream msdata)
        {
        }

        public static void write_7_10(MemoryStream msdata, string name)
        {
            proto_util.writeString(msdata, name);
        }

        public static void write_7_11(MemoryStream msdata, ulong roleId)
        {
            proto_util.writeULong(msdata, roleId);
        }

        public static void write_7_13(MemoryStream msdata, ulong roleId)
        {
            proto_util.writeULong(msdata, roleId);
        }

        public static void write_7_16(MemoryStream msdata, List<ulong> ids)
        {
            proto_util.writeLoopULong(msdata, ids);
        }

        public static void write_7_18(MemoryStream msdata)
        {
        }

        public static void write_7_2(MemoryStream msdata, ulong roleId)
        {
            proto_util.writeULong(msdata, roleId);
        }

        public static void write_7_4(MemoryStream msdata, ulong roleId, byte answer)
        {
            proto_util.writeULong(msdata, roleId);
            proto_util.writeUByte(msdata, answer);
        }

        public static void write_7_6(MemoryStream msdata, ulong roleId)
        {
            proto_util.writeULong(msdata, roleId);
        }

        public static void write_7_7(MemoryStream msdata, ulong roleId)
        {
            proto_util.writeULong(msdata, roleId);
        }
    }
}

