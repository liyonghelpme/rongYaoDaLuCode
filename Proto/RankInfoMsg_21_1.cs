﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class RankInfoMsg_21_1
    {
        public ushort code;
        public ushort counter;
        public List<PRankNew> info = new List<PRankNew>();
        public PRankNew me = new PRankNew();
        public ushort type;

        public static int getCode()
        {
            return 0x1501;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUShort(msdata);
            this.counter = proto_util.readUShort(msdata);
            PRankNew.readLoop(msdata, this.info);
            this.me.read(msdata);
        }
    }
}

