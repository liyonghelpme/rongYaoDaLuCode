﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationAcceptMsg_7_2
    {
        public ushort code;

        public static int getCode()
        {
            return 0x702;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

