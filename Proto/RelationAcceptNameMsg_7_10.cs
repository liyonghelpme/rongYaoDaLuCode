﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationAcceptNameMsg_7_10
    {
        public ushort code;

        public static int getCode()
        {
            return 0x70a;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

