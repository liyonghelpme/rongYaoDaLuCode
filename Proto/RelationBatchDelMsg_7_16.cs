﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationBatchDelMsg_7_16
    {
        public ushort code;

        public static int getCode()
        {
            return 0x710;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

