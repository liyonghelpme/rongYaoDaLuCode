﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationDeleteBlackMsg_7_11
    {
        public ushort code;
        public ulong roleId;

        public static int getCode()
        {
            return 0x70b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.roleId = proto_util.readULong(msdata);
        }
    }
}

