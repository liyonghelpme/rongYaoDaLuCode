﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationFriendsCountMsg_7_17
    {
        public ushort maxCount;
        public ushort nowCount;

        public static int getCode()
        {
            return 0x711;
        }

        public void read(MemoryStream msdata)
        {
            this.nowCount = proto_util.readUShort(msdata);
            this.maxCount = proto_util.readUShort(msdata);
        }
    }
}

