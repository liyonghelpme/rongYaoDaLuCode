﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationFriendsLvlMsg_7_12
    {
        public byte lvl;
        public ulong roleId;

        public static int getCode()
        {
            return 0x70c;
        }

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
            this.lvl = proto_util.readUByte(msdata);
        }
    }
}

