﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class RelationInfoMsg_7_1
    {
        public List<PRelationInfo> blacks = new List<PRelationInfo>();
        public List<PRelationInfo> friends = new List<PRelationInfo>();

        public static int getCode()
        {
            return 0x701;
        }

        public void read(MemoryStream msdata)
        {
            PRelationInfo.readLoop(msdata, this.friends);
            PRelationInfo.readLoop(msdata, this.blacks);
        }
    }
}

