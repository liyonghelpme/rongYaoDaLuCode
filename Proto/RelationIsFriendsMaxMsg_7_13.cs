﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationIsFriendsMaxMsg_7_13
    {
        public ushort code;
        public ulong roleId;

        public static int getCode()
        {
            return 0x70d;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.roleId = proto_util.readULong(msdata);
        }
    }
}

