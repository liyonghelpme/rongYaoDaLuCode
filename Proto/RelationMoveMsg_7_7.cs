﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationMoveMsg_7_7
    {
        public ushort code;
        public ulong roleId;

        public static int getCode()
        {
            return 0x707;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.roleId = proto_util.readULong(msdata);
        }
    }
}

