﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class RelationNearMsg_7_18
    {
        public List<PRelationNear> near = new List<PRelationNear>();

        public static int getCode()
        {
            return 0x712;
        }

        public void read(MemoryStream msdata)
        {
            PRelationNear.readLoop(msdata, this.near);
        }
    }
}

