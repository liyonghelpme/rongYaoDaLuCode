﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationPushAcceptMsg_7_3
    {
        public uint fightpoint;
        public byte job;
        public byte lvl;
        public string name = string.Empty;
        public ulong roleId;
        public byte sex;
        public byte vip;

        public static int getCode()
        {
            return 0x703;
        }

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
            this.name = proto_util.readString(msdata);
            this.job = proto_util.readUByte(msdata);
            this.lvl = proto_util.readUByte(msdata);
            this.sex = proto_util.readUByte(msdata);
            this.vip = proto_util.readUByte(msdata);
            this.fightpoint = proto_util.readUInt(msdata);
        }
    }
}

