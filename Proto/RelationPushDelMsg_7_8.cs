﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationPushDelMsg_7_8
    {
        public ulong roleId;

        public static int getCode()
        {
            return 0x708;
        }

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
        }
    }
}

