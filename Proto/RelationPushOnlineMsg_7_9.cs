﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationPushOnlineMsg_7_9
    {
        public byte isOnline;
        public ulong roleId;

        public static int getCode()
        {
            return 0x709;
        }

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
            this.isOnline = proto_util.readUByte(msdata);
        }
    }
}

