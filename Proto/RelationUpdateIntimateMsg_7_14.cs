﻿namespace Proto
{
    using System;
    using System.IO;

    public class RelationUpdateIntimateMsg_7_14
    {
        public uint intimate;
        public ulong roleId;

        public static int getCode()
        {
            return 0x70e;
        }

        public void read(MemoryStream msdata)
        {
            this.roleId = proto_util.readULong(msdata);
            this.intimate = proto_util.readUInt(msdata);
        }
    }
}

