﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class RoleAttrMsg_3_4
    {
        public PNewAttr attr = new PNewAttr();

        public static int getCode()
        {
            return 0x304;
        }

        public void read(MemoryStream msdata)
        {
            this.attr.read(msdata);
        }
    }
}

