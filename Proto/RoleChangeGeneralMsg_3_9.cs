﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class RoleChangeGeneralMsg_3_9
    {
        public ushort code;
        public ulong generalId;
        public ulong roleId;
        public PStylebin styleBin = new PStylebin();

        public static int getCode()
        {
            return 0x309;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.roleId = proto_util.readULong(msdata);
            this.generalId = proto_util.readULong(msdata);
            this.styleBin.read(msdata);
        }
    }
}

