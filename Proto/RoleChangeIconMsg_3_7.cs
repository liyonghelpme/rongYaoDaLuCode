﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleChangeIconMsg_3_7
    {
        public ushort code;
        public uint iconId;

        public static int getCode()
        {
            return 0x307;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.iconId = proto_util.readUInt(msdata);
        }
    }
}

