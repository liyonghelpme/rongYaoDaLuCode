﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleExpIncMsg_3_10
    {
        public byte hasWorldExp;
        public uint inc;

        public static int getCode()
        {
            return 0x30a;
        }

        public void read(MemoryStream msdata)
        {
            this.inc = proto_util.readUInt(msdata);
            this.hasWorldExp = proto_util.readUByte(msdata);
        }
    }
}

