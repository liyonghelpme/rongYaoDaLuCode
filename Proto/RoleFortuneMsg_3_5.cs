﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleFortuneMsg_3_5
    {
        public uint diam;
        public uint diamBind;
        public uint gold;
        public uint repu;

        public static int getCode()
        {
            return 0x305;
        }

        public void read(MemoryStream msdata)
        {
            this.gold = proto_util.readUInt(msdata);
            this.diam = proto_util.readUInt(msdata);
            this.diamBind = proto_util.readUInt(msdata);
            this.repu = proto_util.readUInt(msdata);
        }
    }
}

