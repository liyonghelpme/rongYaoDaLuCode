﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleGetPlotMsg_3_13
    {
        public uint plot;

        public static int getCode()
        {
            return 0x30d;
        }

        public void read(MemoryStream msdata)
        {
            this.plot = proto_util.readUInt(msdata);
        }
    }
}

