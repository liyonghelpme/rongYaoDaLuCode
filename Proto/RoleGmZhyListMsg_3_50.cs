﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class RoleGmZhyListMsg_3_50
    {
        public List<PGmZhy> zhyList = new List<PGmZhy>();

        public static int getCode()
        {
            return 0x332;
        }

        public void read(MemoryStream msdata)
        {
            PGmZhy.readLoop(msdata, this.zhyList);
        }
    }
}

