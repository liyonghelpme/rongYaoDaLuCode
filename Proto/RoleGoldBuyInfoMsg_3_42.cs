﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleGoldBuyInfoMsg_3_42
    {
        public ushort batchDiam;
        public ushort code;
        public ushort diam;
        public uint gold;
        public ushort remainTimes;
        public ushort times;

        public static int getCode()
        {
            return 810;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.times = proto_util.readUShort(msdata);
            this.remainTimes = proto_util.readUShort(msdata);
            this.diam = proto_util.readUShort(msdata);
            this.gold = proto_util.readUInt(msdata);
            this.batchDiam = proto_util.readUShort(msdata);
        }
    }
}

