﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleGoldBuyMsg_3_43
    {
        public ushort code;
        public byte type;

        public static int getCode()
        {
            return 0x32b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
        }
    }
}

