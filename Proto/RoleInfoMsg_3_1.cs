﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class RoleInfoMsg_3_1
    {
        public PRoleAttr role = new PRoleAttr();

        public static int getCode()
        {
            return 0x301;
        }

        public void read(MemoryStream msdata)
        {
            this.role.read(msdata);
        }
    }
}

