﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class RoleInfoOtherMsg_3_3
    {
        public ushort code;
        public POtherRoleInfo info = new POtherRoleInfo();

        public static int getCode()
        {
            return 0x303;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.info.read(msdata);
        }
    }
}

