﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class RoleListMsg_3_2
    {
        public List<PBuff> list = new List<PBuff>();

        public static int getCode()
        {
            return 770;
        }

        public void read(MemoryStream msdata)
        {
            PBuff.readLoop(msdata, this.list);
        }
    }
}

