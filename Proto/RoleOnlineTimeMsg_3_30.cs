﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleOnlineTimeMsg_3_30
    {
        public byte cm;
        public uint time;

        public static int getCode()
        {
            return 0x31e;
        }

        public void read(MemoryStream msdata)
        {
            this.time = proto_util.readUInt(msdata);
            this.cm = proto_util.readUByte(msdata);
        }
    }
}

