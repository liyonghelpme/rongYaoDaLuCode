﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleReviveMsg_3_20
    {
        public ushort code;
        public byte type;

        public static int getCode()
        {
            return 0x314;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
        }
    }
}

