﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleSetPlotMsg_3_14
    {
        public uint code;

        public static int getCode()
        {
            return 0x30e;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUInt(msdata);
        }
    }
}

