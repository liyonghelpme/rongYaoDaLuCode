﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleSyncMsg_3_44
    {
        public int dir;
        public ulong id;
        public byte state;
        public int x;
        public int y;
        public int z;

        public static int getCode()
        {
            return 0x32c;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.state = proto_util.readUByte(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.dir = proto_util.readInt(msdata);
        }
    }
}

