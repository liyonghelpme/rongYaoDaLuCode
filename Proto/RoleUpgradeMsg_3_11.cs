﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleUpgradeMsg_3_11
    {
        public ulong exp;
        public ulong expFull;
        public byte lvl;

        public static int getCode()
        {
            return 0x30b;
        }

        public void read(MemoryStream msdata)
        {
            this.lvl = proto_util.readUByte(msdata);
            this.exp = proto_util.readULong(msdata);
            this.expFull = proto_util.readULong(msdata);
        }
    }
}

