﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleVigourBuyMsg_3_40
    {
        public ushort code;
        public byte type;

        public static int getCode()
        {
            return 0x328;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
        }
    }
}

