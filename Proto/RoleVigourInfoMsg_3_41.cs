﻿namespace Proto
{
    using System;
    using System.IO;

    public class RoleVigourInfoMsg_3_41
    {
        public ushort dailyBuyTimes;
        public ushort diam;
        public uint lastRecoverTime;
        public ushort vigour;
        public ushort vigourFull;

        public static int getCode()
        {
            return 0x329;
        }

        public void read(MemoryStream msdata)
        {
            this.vigour = proto_util.readUShort(msdata);
            this.vigourFull = proto_util.readUShort(msdata);
            this.diam = proto_util.readUShort(msdata);
            this.dailyBuyTimes = proto_util.readUShort(msdata);
            this.lastRecoverTime = proto_util.readUInt(msdata);
        }
    }
}

