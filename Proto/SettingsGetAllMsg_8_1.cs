﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SettingsGetAllMsg_8_1
    {
        public List<byte> key = new List<byte>();
        public List<byte> val = new List<byte>();

        public static int getCode()
        {
            return 0x801;
        }

        public void read(MemoryStream msdata)
        {
            proto_util.readLoopUByte(msdata, this.key);
            proto_util.readLoopUByte(msdata, this.val);
        }
    }
}

