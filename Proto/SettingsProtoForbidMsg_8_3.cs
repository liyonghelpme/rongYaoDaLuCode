﻿namespace Proto
{
    using System;
    using System.IO;

    public class SettingsProtoForbidMsg_8_3
    {
        public byte method;
        public byte section;

        public static int getCode()
        {
            return 0x803;
        }

        public void read(MemoryStream msdata)
        {
            this.section = proto_util.readUByte(msdata);
            this.method = proto_util.readUByte(msdata);
        }
    }
}

