﻿namespace Proto
{
    using System;
    using System.IO;

    public class SettingsSaveMsg_8_2
    {
        public uint code;

        public static int getCode()
        {
            return 0x802;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUInt(msdata);
        }
    }
}

