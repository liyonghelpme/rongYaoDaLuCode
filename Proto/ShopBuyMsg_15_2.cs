﻿namespace Proto
{
    using System;
    using System.IO;

    public class ShopBuyMsg_15_2
    {
        public int amount;
        public ushort code;
        public ulong shopId;

        public static int getCode()
        {
            return 0xf02;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.shopId = proto_util.readULong(msdata);
            this.amount = proto_util.readInt(msdata);
        }
    }
}

