﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ShopInfoMsg_15_1
    {
        public List<PShopList> shopList = new List<PShopList>();

        public static int getCode()
        {
            return 0xf01;
        }

        public void read(MemoryStream msdata)
        {
            PShopList.readLoop(msdata, this.shopList);
        }
    }
}

