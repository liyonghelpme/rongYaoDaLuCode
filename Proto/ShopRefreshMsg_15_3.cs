﻿namespace Proto
{
    using System;
    using System.IO;

    public class ShopRefreshMsg_15_3
    {
        public ushort code;

        public static int getCode()
        {
            return 0xf03;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

