﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SkillAllStudyMsg_13_6
    {
        public ushort code;
        public List<uint> skillids = new List<uint>();

        public static int getCode()
        {
            return 0xd06;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            proto_util.readLoopUInt(msdata, this.skillids);
        }
    }
}

