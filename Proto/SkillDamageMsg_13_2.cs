﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SkillDamageMsg_13_2
    {
        public ulong actId;
        public List<PDamage> damage = new List<PDamage>();
        public byte seq;
        public uint skillId;
        public byte type;

        public static int getCode()
        {
            return 0xd02;
        }

        public void read(MemoryStream msdata)
        {
            this.seq = proto_util.readUByte(msdata);
            this.skillId = proto_util.readUInt(msdata);
            this.type = proto_util.readUByte(msdata);
            this.actId = proto_util.readULong(msdata);
            PDamage.readLoop(msdata, this.damage);
        }
    }
}

