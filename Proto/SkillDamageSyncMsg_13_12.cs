﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SkillDamageSyncMsg_13_12
    {
        public ulong actorId;
        public int actorType;
        public List<PDamage> damageList = new List<PDamage>();
        public ushort seq;
        public uint skillId;

        public static int getCode()
        {
            return 0xd0c;
        }

        public void read(MemoryStream msdata)
        {
            this.seq = proto_util.readUShort(msdata);
            this.actorId = proto_util.readULong(msdata);
            this.actorType = proto_util.readInt(msdata);
            this.skillId = proto_util.readUInt(msdata);
            PDamage.readLoop(msdata, this.damageList);
        }
    }
}

