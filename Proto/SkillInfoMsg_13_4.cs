﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SkillInfoMsg_13_4
    {
        public uint restPoint;
        public List<uint> skillids = new List<uint>();

        public static int getCode()
        {
            return 0xd04;
        }

        public void read(MemoryStream msdata)
        {
            proto_util.readLoopUInt(msdata, this.skillids);
            this.restPoint = proto_util.readUInt(msdata);
        }
    }
}

