﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SkillLearningAllInfoMsg_19_1
    {
        public byte currentSkillPoint;
        public List<PGeneralInfo> generalList = new List<PGeneralInfo>();
        public uint nextRefreshTime;

        public static int getCode()
        {
            return 0x1301;
        }

        public void read(MemoryStream msdata)
        {
            this.currentSkillPoint = proto_util.readUByte(msdata);
            this.nextRefreshTime = proto_util.readUInt(msdata);
            PGeneralInfo.readLoop(msdata, this.generalList);
        }
    }
}

