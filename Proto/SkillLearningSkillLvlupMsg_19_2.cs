﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillLearningSkillLvlupMsg_19_2
    {
        public ushort code;
        public byte currentSkillPoint;
        public ulong generalId;
        public uint groupId;
        public uint nextRefreshTime;
        public ushort skillLevel;

        public static int getCode()
        {
            return 0x1302;
        }

        public void read(MemoryStream msdata)
        {
            this.currentSkillPoint = proto_util.readUByte(msdata);
            this.nextRefreshTime = proto_util.readUInt(msdata);
            this.generalId = proto_util.readULong(msdata);
            this.groupId = proto_util.readUInt(msdata);
            this.skillLevel = proto_util.readUShort(msdata);
            this.code = proto_util.readUShort(msdata);
        }
    }
}

