﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillLearningSkillPointMsg_19_4
    {
        public ushort code;
        public byte currentSkillPoint;
        public uint nextRefreshTime;

        public static int getCode()
        {
            return 0x1304;
        }

        public void read(MemoryStream msdata)
        {
            this.currentSkillPoint = proto_util.readUByte(msdata);
            this.nextRefreshTime = proto_util.readUInt(msdata);
            this.code = proto_util.readUShort(msdata);
        }
    }
}

