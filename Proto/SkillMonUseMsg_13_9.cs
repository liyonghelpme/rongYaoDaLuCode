﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillMonUseMsg_13_9
    {
        public ulong actId;
        public byte dir;
        public ulong objId;
        public byte seq;
        public uint skillId;
        public byte type;

        public static int getCode()
        {
            return 0xd09;
        }

        public void read(MemoryStream msdata)
        {
            this.seq = proto_util.readUByte(msdata);
            this.skillId = proto_util.readUInt(msdata);
            this.dir = proto_util.readUByte(msdata);
            this.type = proto_util.readUByte(msdata);
            this.actId = proto_util.readULong(msdata);
            this.objId = proto_util.readULong(msdata);
        }
    }
}

