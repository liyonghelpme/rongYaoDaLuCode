﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SkillMonWorkMsg_13_10
    {
        public byte dir;
        public byte seq;
        public uint skillId;
        public List<PTarget> target = new List<PTarget>();

        public static int getCode()
        {
            return 0xd0a;
        }

        public void read(MemoryStream msdata)
        {
            this.seq = proto_util.readUByte(msdata);
            this.skillId = proto_util.readUInt(msdata);
            this.dir = proto_util.readUByte(msdata);
            PTarget.readLoop(msdata, this.target);
        }
    }
}

