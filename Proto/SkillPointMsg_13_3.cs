﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillPointMsg_13_3
    {
        public uint restPoint;

        public static int getCode()
        {
            return 0xd03;
        }

        public void read(MemoryStream msdata)
        {
            this.restPoint = proto_util.readUInt(msdata);
        }
    }
}

