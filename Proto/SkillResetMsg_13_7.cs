﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SkillResetMsg_13_7
    {
        public ushort code;
        public List<uint> skillids = new List<uint>();

        public static int getCode()
        {
            return 0xd07;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            proto_util.readLoopUInt(msdata, this.skillids);
        }
    }
}

