﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillStudyMsg_13_5
    {
        public ushort code;
        public uint nextId;
        public uint skillId;

        public static int getCode()
        {
            return 0xd05;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.skillId = proto_util.readUInt(msdata);
            this.nextId = proto_util.readUInt(msdata);
        }
    }
}

