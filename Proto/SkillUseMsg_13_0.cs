﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillUseMsg_13_0
    {
        public ushort code;

        public static int getCode()
        {
            return 0xd00;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

