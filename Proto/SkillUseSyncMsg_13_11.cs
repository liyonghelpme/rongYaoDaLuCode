﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillUseSyncMsg_13_11
    {
        public ushort code;
        public int eularY;
        public ulong id;
        public uint skillId;
        public byte type;

        public static int getCode()
        {
            return 0xd0b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            this.skillId = proto_util.readUInt(msdata);
            this.eularY = proto_util.readInt(msdata);
        }
    }
}

