﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillWarnMsg_13_8
    {
        public ulong actId;
        public uint skillId;
        public byte type;

        public static int getCode()
        {
            return 0xd08;
        }

        public void read(MemoryStream msdata)
        {
            this.type = proto_util.readUByte(msdata);
            this.actId = proto_util.readULong(msdata);
            this.skillId = proto_util.readUInt(msdata);
        }
    }
}

