﻿namespace Proto
{
    using System;
    using System.IO;

    public class SkillWorkMsg_13_1
    {
        public ushort code;

        public static int getCode()
        {
            return 0xd01;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

