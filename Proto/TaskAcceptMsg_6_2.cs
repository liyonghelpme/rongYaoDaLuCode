﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskAcceptMsg_6_2
    {
        public ushort code;
        public byte state;
        public ulong taskId;

        public static int getCode()
        {
            return 0x602;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.taskId = proto_util.readULong(msdata);
            this.state = proto_util.readUByte(msdata);
        }
    }
}

