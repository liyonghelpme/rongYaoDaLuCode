﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskCommitMsg_6_4
    {
        public ushort code;
        public ulong nextId;
        public ulong taskId;
        public List<ulong> visible = new List<ulong>();

        public static int getCode()
        {
            return 0x604;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.taskId = proto_util.readULong(msdata);
            this.nextId = proto_util.readULong(msdata);
            proto_util.readLoopULong(msdata, this.visible);
        }
    }
}

