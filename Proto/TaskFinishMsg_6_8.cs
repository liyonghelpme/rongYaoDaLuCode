﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskFinishMsg_6_8
    {
        public ushort code;
        public byte type;

        public static int getCode()
        {
            return 0x608;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
        }
    }
}

