﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskGiveUpMsg_6_3
    {
        public ushort code;
        public ulong taskId;

        public static int getCode()
        {
            return 0x603;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.taskId = proto_util.readULong(msdata);
        }
    }
}

