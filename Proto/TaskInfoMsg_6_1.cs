﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskInfoMsg_6_1
    {
        public ushort code;
        public ulong nextId;
        public List<PTaskDoing> taskDoing = new List<PTaskDoing>();
        public List<ulong> visible = new List<ulong>();

        public static int getCode()
        {
            return 0x601;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.nextId = proto_util.readULong(msdata);
            PTaskDoing.readLoop(msdata, this.taskDoing);
            proto_util.readLoopULong(msdata, this.visible);
        }
    }
}

