﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskLastMainIdMsg_6_7
    {
        public ulong taskId;

        public static int getCode()
        {
            return 0x607;
        }

        public void read(MemoryStream msdata)
        {
            this.taskId = proto_util.readULong(msdata);
        }
    }
}

