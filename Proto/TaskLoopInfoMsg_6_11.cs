﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskLoopInfoMsg_6_11
    {
        public ushort code;
        public ushort ratio;
        public ulong taskId;
        public byte type;

        public static int getCode()
        {
            return 0x60b;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
            this.taskId = proto_util.readULong(msdata);
            this.ratio = proto_util.readUShort(msdata);
        }
    }
}

