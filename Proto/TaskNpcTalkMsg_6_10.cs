﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskNpcTalkMsg_6_10
    {
        public ushort code;

        public static int getCode()
        {
            return 0x60a;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

