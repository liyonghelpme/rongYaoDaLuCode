﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskRandomRatioMsg_6_13
    {
        public ushort code;
        public byte type;

        public static int getCode()
        {
            return 0x60d;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.type = proto_util.readUByte(msdata);
        }
    }
}

