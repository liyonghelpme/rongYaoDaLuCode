﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskRecruitListPushMsg_6_17
    {
        public ushort code;
        public List<PRecruit> tasklist = new List<PRecruit>();
        public uint time;

        public static int getCode()
        {
            return 0x611;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PRecruit.readLoop(msdata, this.tasklist);
            this.time = proto_util.readUInt(msdata);
        }
    }
}

