﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskRecruitMsg_6_14
    {
        public ushort code;
        public ushort count;
        public List<PRecruit> tasklist = new List<PRecruit>();
        public uint time;

        public static int getCode()
        {
            return 0x60e;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PRecruit.readLoop(msdata, this.tasklist);
            this.count = proto_util.readUShort(msdata);
            this.time = proto_util.readUInt(msdata);
        }
    }
}

