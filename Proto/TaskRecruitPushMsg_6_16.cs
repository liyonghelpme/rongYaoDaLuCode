﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskRecruitPushMsg_6_16
    {
        public ushort code;
        public ushort count;
        public List<PRecruit> tasklist = new List<PRecruit>();

        public static int getCode()
        {
            return 0x610;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            this.count = proto_util.readUShort(msdata);
            PRecruit.readLoop(msdata, this.tasklist);
        }
    }
}

