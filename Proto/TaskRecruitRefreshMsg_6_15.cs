﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskRecruitRefreshMsg_6_15
    {
        public ushort code;
        public List<PRecruit> tasklist = new List<PRecruit>();

        public static int getCode()
        {
            return 0x60f;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PRecruit.readLoop(msdata, this.tasklist);
        }
    }
}

