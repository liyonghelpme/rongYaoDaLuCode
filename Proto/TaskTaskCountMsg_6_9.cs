﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskTaskCountMsg_6_9
    {
        public ushort dLoopAccept;
        public ushort dLoopFinish;
        public ushort wLoopAccept;
        public ushort wLoopFinish;

        public static int getCode()
        {
            return 0x609;
        }

        public void read(MemoryStream msdata)
        {
            this.dLoopAccept = proto_util.readUShort(msdata);
            this.dLoopFinish = proto_util.readUShort(msdata);
            this.wLoopAccept = proto_util.readUShort(msdata);
            this.wLoopFinish = proto_util.readUShort(msdata);
        }
    }
}

