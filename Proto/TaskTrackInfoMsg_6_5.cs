﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskTrackInfoMsg_6_5
    {
        public byte state;
        public ulong taskId;
        public List<PTaskTrack> track = new List<PTaskTrack>();

        public static int getCode()
        {
            return 0x605;
        }

        public void read(MemoryStream msdata)
        {
            this.taskId = proto_util.readULong(msdata);
            this.state = proto_util.readUByte(msdata);
            PTaskTrack.readLoop(msdata, this.track);
        }
    }
}

