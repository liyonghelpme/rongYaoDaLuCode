﻿namespace Proto
{
    using System;
    using System.IO;

    public class TaskUpdateCountMsg_6_12
    {
        public ushort count;
        public byte state;
        public byte type;

        public static int getCode()
        {
            return 0x60c;
        }

        public void read(MemoryStream msdata)
        {
            this.state = proto_util.readUByte(msdata);
            this.type = proto_util.readUByte(msdata);
            this.count = proto_util.readUShort(msdata);
        }
    }
}

