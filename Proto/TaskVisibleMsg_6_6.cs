﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TaskVisibleMsg_6_6
    {
        public List<ulong> visible = new List<ulong>();

        public static int getCode()
        {
            return 0x606;
        }

        public void read(MemoryStream msdata)
        {
            proto_util.readLoopULong(msdata, this.visible);
        }
    }
}

