﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class TitleCloseTitleMsg_44_3
    {
        public PTitle closeTitle = new PTitle();

        public static int getCode()
        {
            return 0x2c03;
        }

        public void read(MemoryStream msdata)
        {
            this.closeTitle.read(msdata);
        }
    }
}

