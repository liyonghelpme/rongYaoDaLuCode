﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class TitleOpenTitleMsg_44_2
    {
        public PTitle openTitle = new PTitle();

        public static int getCode()
        {
            return 0x2c02;
        }

        public void read(MemoryStream msdata)
        {
            this.openTitle.read(msdata);
        }
    }
}

