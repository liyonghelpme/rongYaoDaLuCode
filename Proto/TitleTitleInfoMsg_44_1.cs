﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TitleTitleInfoMsg_44_1
    {
        public List<PTitle> titleList = new List<PTitle>();

        public static int getCode()
        {
            return 0x2c01;
        }

        public void read(MemoryStream msdata)
        {
            PTitle.readLoop(msdata, this.titleList);
        }
    }
}

