﻿namespace Proto
{
    using System;
    using System.IO;

    public class TitleUseTitleMsg_44_4
    {
        public uint code;
        public byte opType;
        public uint titleId;

        public static int getCode()
        {
            return 0x2c04;
        }

        public void read(MemoryStream msdata)
        {
            this.opType = proto_util.readUByte(msdata);
            this.titleId = proto_util.readUInt(msdata);
            this.code = proto_util.readUInt(msdata);
        }
    }
}

