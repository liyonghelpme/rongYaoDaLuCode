﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class TreasureBuyMsg_37_3
    {
        public ushort code;
        public List<PTreasure> treasures = new List<PTreasure>();

        public static int getCode()
        {
            return 0x2503;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
            PTreasure.readLoop(msdata, this.treasures);
        }
    }
}

