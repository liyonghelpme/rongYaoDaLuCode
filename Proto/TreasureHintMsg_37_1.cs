﻿namespace Proto
{
    using System;
    using System.IO;

    public class TreasureHintMsg_37_1
    {
        public byte code;

        public static int getCode()
        {
            return 0x2501;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUByte(msdata);
        }
    }
}

