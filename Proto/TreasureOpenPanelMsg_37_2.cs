﻿namespace Proto
{
    using System;
    using System.IO;

    public class TreasureOpenPanelMsg_37_2
    {
        public byte diamondFirstFlag;
        public uint diamondFreeCd;
        public byte diamondFreeTimes;
        public uint diamondPrice;
        public uint diamondPrices;
        public byte goldFirstFlag;
        public uint goldFreeCd;
        public byte goldFreeTimes;
        public uint goldPrice;
        public uint goldPrices;

        public static int getCode()
        {
            return 0x2502;
        }

        public void read(MemoryStream msdata)
        {
            this.goldFirstFlag = proto_util.readUByte(msdata);
            this.goldFreeTimes = proto_util.readUByte(msdata);
            this.goldFreeCd = proto_util.readUInt(msdata);
            this.goldPrice = proto_util.readUInt(msdata);
            this.goldPrices = proto_util.readUInt(msdata);
            this.diamondFirstFlag = proto_util.readUByte(msdata);
            this.diamondFreeTimes = proto_util.readUByte(msdata);
            this.diamondFreeCd = proto_util.readUInt(msdata);
            this.diamondPrice = proto_util.readUInt(msdata);
            this.diamondPrices = proto_util.readUInt(msdata);
        }
    }
}

