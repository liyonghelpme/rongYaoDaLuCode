﻿namespace Proto
{
    using System;
    using System.IO;

    public class VipGetRewardMsg_32_2
    {
        public ushort code;

        public static int getCode()
        {
            return 0x2002;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

