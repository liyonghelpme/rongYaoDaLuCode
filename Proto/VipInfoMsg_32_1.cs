﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class VipInfoMsg_32_1
    {
        public List<PVipRewardGetInfo> rewardInfoList = new List<PVipRewardGetInfo>();
        public uint totalDiam;
        public byte vip;

        public static int getCode()
        {
            return 0x2001;
        }

        public void read(MemoryStream msdata)
        {
            this.vip = proto_util.readUByte(msdata);
            this.totalDiam = proto_util.readUInt(msdata);
            PVipRewardGetInfo.readLoop(msdata, this.rewardInfoList);
        }
    }
}

