﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpAddBattleObjectMsg_14_19
    {
        public List<PBattleObject> battleObjectList = new List<PBattleObject>();

        public static int getCode()
        {
            return 0xe13;
        }

        public void read(MemoryStream msdata)
        {
            PBattleObject.readLoop(msdata, this.battleObjectList);
        }
    }
}

