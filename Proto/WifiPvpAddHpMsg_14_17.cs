﻿namespace Proto
{
    using System;
    using System.IO;

    public class WifiPvpAddHpMsg_14_17
    {
        public uint delta;
        public uint endHp;
        public ulong id;
        public byte type;

        public static int getCode()
        {
            return 0xe11;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            this.delta = proto_util.readUInt(msdata);
            this.endHp = proto_util.readUInt(msdata);
        }
    }
}

