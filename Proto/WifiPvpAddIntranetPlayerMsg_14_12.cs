﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class WifiPvpAddIntranetPlayerMsg_14_12
    {
        public PIntranetPlayer player = new PIntranetPlayer();

        public static int getCode()
        {
            return 0xe0c;
        }

        public void read(MemoryStream msdata)
        {
            this.player.read(msdata);
        }
    }
}

