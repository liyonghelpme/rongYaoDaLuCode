﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpAddMonMsg_14_14
    {
        public List<PMapMon> monList = new List<PMapMon>();

        public static int getCode()
        {
            return 0xe0e;
        }

        public void read(MemoryStream msdata)
        {
            PMapMon.readLoop(msdata, this.monList);
        }
    }
}

