﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpAddPrivateMsg_14_15
    {
        public List<PMapMon> privateList = new List<PMapMon>();

        public static int getCode()
        {
            return 0xe0f;
        }

        public void read(MemoryStream msdata)
        {
            PMapMon.readLoop(msdata, this.privateList);
        }
    }
}

