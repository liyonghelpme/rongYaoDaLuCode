﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpAddSummonMsg_14_22
    {
        public List<PMapMon> monList = new List<PMapMon>();
        public ulong summorId;
        public byte syncType;

        public static int getCode()
        {
            return 0xe16;
        }

        public void read(MemoryStream msdata)
        {
            this.summorId = proto_util.readULong(msdata);
            this.syncType = proto_util.readUByte(msdata);
            PMapMon.readLoop(msdata, this.monList);
        }
    }
}

