﻿namespace Proto
{
    using System;
    using System.IO;

    public class WifiPvpBattleObjectPickedMsg_14_21
    {
        public ulong battleObjectId;
        public ulong generalId;
        public int rotateY;
        public int x;
        public int y;
        public int z;

        public static int getCode()
        {
            return 0xe15;
        }

        public void read(MemoryStream msdata)
        {
            this.generalId = proto_util.readULong(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
            this.rotateY = proto_util.readInt(msdata);
            this.battleObjectId = proto_util.readULong(msdata);
        }
    }
}

