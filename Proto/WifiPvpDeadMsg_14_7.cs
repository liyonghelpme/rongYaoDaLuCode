﻿namespace Proto
{
    using System;
    using System.IO;

    public class WifiPvpDeadMsg_14_7
    {
        public ulong id;
        public int x;
        public int y;
        public int z;

        public static int getCode()
        {
            return 0xe07;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.x = proto_util.readInt(msdata);
            this.y = proto_util.readInt(msdata);
            this.z = proto_util.readInt(msdata);
        }
    }
}

