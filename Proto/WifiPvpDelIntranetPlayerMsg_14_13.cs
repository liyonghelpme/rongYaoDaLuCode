﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.IO;

    public class WifiPvpDelIntranetPlayerMsg_14_13
    {
        public PIntranetPlayer player = new PIntranetPlayer();

        public static int getCode()
        {
            return 0xe0d;
        }

        public void read(MemoryStream msdata)
        {
            this.player.read(msdata);
        }
    }
}

