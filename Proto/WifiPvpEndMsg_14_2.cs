﻿namespace Proto
{
    using System;
    using System.IO;

    public class WifiPvpEndMsg_14_2
    {
        public ushort code;

        public static int getCode()
        {
            return 0xe02;
        }

        public void read(MemoryStream msdata)
        {
            this.code = proto_util.readUShort(msdata);
        }
    }
}

