﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpForceMoveSyncMsg_14_4
    {
        public List<PCoordinator> coordinator = new List<PCoordinator>();
        public bool isForce;

        public static int getCode()
        {
            return 0xe04;
        }

        public void read(MemoryStream msdata)
        {
            this.isForce = proto_util.readBool(msdata);
            PCoordinator.readLoop(msdata, this.coordinator);
        }
    }
}

