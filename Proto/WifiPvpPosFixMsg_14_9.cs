﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpPosFixMsg_14_9
    {
        public List<PCoordinator> coordinatorList = new List<PCoordinator>();

        public static int getCode()
        {
            return 0xe09;
        }

        public void read(MemoryStream msdata)
        {
            PCoordinator.readLoop(msdata, this.coordinatorList);
        }
    }
}

