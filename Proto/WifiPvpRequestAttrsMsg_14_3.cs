﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpRequestAttrsMsg_14_3
    {
        public List<PCoordinator> coordinator = new List<PCoordinator>();
        public List<PNewAttr> generalAttr = new List<PNewAttr>();
        public List<PNewAttr> generalEquipAttr = new List<PNewAttr>();
        public byte sender;

        public static int getCode()
        {
            return 0xe03;
        }

        public void read(MemoryStream msdata)
        {
            this.sender = proto_util.readUByte(msdata);
            PCoordinator.readLoop(msdata, this.coordinator);
            PNewAttr.readLoop(msdata, this.generalAttr);
            PNewAttr.readLoop(msdata, this.generalEquipAttr);
        }
    }
}

