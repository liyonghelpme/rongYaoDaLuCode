﻿namespace Proto
{
    using System;
    using System.IO;

    public class WifiPvpRequestEnterMsg_14_1
    {
        public ushort code;
        public bool isMaster;
        public uint pvpId;

        public static int getCode()
        {
            return 0xe01;
        }

        public void read(MemoryStream msdata)
        {
            this.pvpId = proto_util.readUInt(msdata);
            this.isMaster = proto_util.readBool(msdata);
            this.code = proto_util.readUShort(msdata);
        }
    }
}

