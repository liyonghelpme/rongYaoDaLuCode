﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpStartFlushMsg_14_10
    {
        public List<PIntranetPlayer> playerList = new List<PIntranetPlayer>();

        public static int getCode()
        {
            return 0xe0a;
        }

        public void read(MemoryStream msdata)
        {
            PIntranetPlayer.readLoop(msdata, this.playerList);
        }
    }
}

