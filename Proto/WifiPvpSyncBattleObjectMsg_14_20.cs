﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpSyncBattleObjectMsg_14_20
    {
        public List<PBattleObject> battleObjectList = new List<PBattleObject>();

        public static int getCode()
        {
            return 0xe14;
        }

        public void read(MemoryStream msdata)
        {
            PBattleObject.readLoop(msdata, this.battleObjectList);
        }
    }
}

