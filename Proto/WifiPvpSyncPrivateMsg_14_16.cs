﻿namespace Proto
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class WifiPvpSyncPrivateMsg_14_16
    {
        public List<PMapMon> privateList = new List<PMapMon>();

        public static int getCode()
        {
            return 0xe10;
        }

        public void read(MemoryStream msdata)
        {
            PMapMon.readLoop(msdata, this.privateList);
        }
    }
}

