﻿namespace Proto
{
    using System;
    using System.IO;

    public class WifiPvpSyncSoulMsg_14_18
    {
        public byte soul;

        public static int getCode()
        {
            return 0xe12;
        }

        public void read(MemoryStream msdata)
        {
            this.soul = proto_util.readUByte(msdata);
        }
    }
}

