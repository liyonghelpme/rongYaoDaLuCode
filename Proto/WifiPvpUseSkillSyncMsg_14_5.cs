﻿namespace Proto
{
    using System;
    using System.IO;

    public class WifiPvpUseSkillSyncMsg_14_5
    {
        public int eularY;
        public ulong id;
        public uint skillId;
        public ulong targetId;
        public byte targetType;
        public byte type;

        public static int getCode()
        {
            return 0xe05;
        }

        public void read(MemoryStream msdata)
        {
            this.id = proto_util.readULong(msdata);
            this.type = proto_util.readUByte(msdata);
            this.skillId = proto_util.readUInt(msdata);
            this.eularY = proto_util.readInt(msdata);
            this.targetId = proto_util.readULong(msdata);
            this.targetType = proto_util.readUByte(msdata);
        }
    }
}

