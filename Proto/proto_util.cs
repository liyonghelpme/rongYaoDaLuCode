﻿namespace Proto
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;

    public class proto_util
    {
        public static MemoryStream newStream = new MemoryStream();

        public static bool readBool(MemoryStream data)
        {
            return (readUByte(data) > 0);
        }

        public static sbyte readByte(MemoryStream data)
        {
            sbyte num = (sbyte) data.GetBuffer()[(int) ((IntPtr) data.Position)];
            readStream(data, 1);
            return num;
        }

        public static int readInt(MemoryStream data)
        {
            int num = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(data.GetBuffer(), (int) data.Position));
            readStream(data, 4);
            return num;
        }

        public static long readLong(MemoryStream data)
        {
            long num = 0L;
            num = IPAddress.NetworkToHostOrder(BitConverter.ToInt64(data.GetBuffer(), (int) data.Position));
            readStream(data, 8);
            return num;
        }

        public static void readLoopBool(MemoryStream data, List<bool> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readBool(data));
            }
        }

        public static void readLoopByte(MemoryStream data, List<sbyte> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readByte(data));
            }
        }

        public static void readLoopInt(MemoryStream data, List<int> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readInt(data));
            }
        }

        public static void readLoopLong(MemoryStream data, List<long> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readLong(data));
            }
        }

        public static void readLoopShort(MemoryStream data, List<short> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readShort(data));
            }
        }

        public static void readLoopString(MemoryStream data, List<string> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readString(data));
            }
        }

        public static void readLoopUByte(MemoryStream data, List<byte> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readUByte(data));
            }
        }

        public static void readLoopUInt(MemoryStream data, List<uint> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readUInt(data));
            }
        }

        public static void readLoopULong(MemoryStream data, List<ulong> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readULong(data));
            }
        }

        public static void readLoopUShort(MemoryStream data, List<ushort> _list)
        {
            int num = readShort(data);
            for (int i = 0; i < num; i++)
            {
                _list.Add(readUShort(data));
            }
        }

        public static short readShort(MemoryStream data)
        {
            short num = IPAddress.NetworkToHostOrder(BitConverter.ToInt16(data.GetBuffer(), (int) data.Position));
            readStream(data, 2);
            return num;
        }

        private static void readStream(MemoryStream oldStream, uint offset)
        {
            oldStream.Position += offset;
        }

        public static string readString(MemoryStream data)
        {
            int count = readShort(data);
            string str = Encoding.UTF8.GetString(data.GetBuffer(), (int) data.Position, count);
            readStream(data, (uint) count);
            return str;
        }

        public static byte readUByte(MemoryStream data)
        {
            byte num = data.GetBuffer()[(int) ((IntPtr) data.Position)];
            readStream(data, 1);
            return num;
        }

        public static uint readUInt(MemoryStream data)
        {
            uint num = (uint) IPAddress.NetworkToHostOrder((int) BitConverter.ToUInt32(data.GetBuffer(), (int) data.Position));
            readStream(data, 4);
            return num;
        }

        public static ulong readULong(MemoryStream data)
        {
            ulong num = 0L;
            byte[] buffer = data.GetBuffer();
            int position = (int) data.Position;
            int num3 = ((int) data.Position) + 8;
            int index = position;
            for (int i = 7; index < num3; i--)
            {
                num += (ulong) (buffer[index] * Math.Pow(256.0, (double) i));
                index++;
            }
            readStream(data, 8);
            return num;
        }

        public static ushort readUShort(MemoryStream data)
        {
            ushort num = (ushort) IPAddress.NetworkToHostOrder((short) BitConverter.ToUInt16(data.GetBuffer(), (int) data.Position));
            readStream(data, 2);
            return num;
        }

        public static void writeBool(MemoryStream data, bool tmpvalue)
        {
            byte num = 0;
            if (tmpvalue)
            {
                num = 1;
            }
            writeUByte(data, num);
        }

        public static void writeByte(MemoryStream data, sbyte tmpvalue)
        {
            byte[] buffer = new byte[] { (byte) tmpvalue };
            data.Write(buffer, 0, 1);
        }

        public static void writeInt(MemoryStream data, int tmpvalue)
        {
            tmpvalue = IPAddress.HostToNetworkOrder(tmpvalue);
            data.Write(BitConverter.GetBytes(tmpvalue), 0, 4);
        }

        public static void writeLong(MemoryStream data, long tmpvalue)
        {
            tmpvalue = IPAddress.HostToNetworkOrder(tmpvalue);
            data.Write(BitConverter.GetBytes(tmpvalue), 0, 8);
        }

        public static void writeLoopBool(MemoryStream data, List<bool> list)
        {
            writeShort(data, (short) list.Count);
            foreach (bool flag in list)
            {
                writeBool(data, flag);
            }
        }

        public static void writeLoopByte(MemoryStream data, List<sbyte> list)
        {
            writeShort(data, (short) list.Count);
            foreach (sbyte num in list)
            {
                writeByte(data, num);
            }
        }

        public static void writeLoopInt(MemoryStream data, List<int> list)
        {
            writeShort(data, (short) list.Count);
            foreach (int num in list)
            {
                writeInt(data, num);
            }
        }

        public static void writeLoopLong(MemoryStream data, List<long> list)
        {
            writeShort(data, (short) list.Count);
            foreach (long num in list)
            {
                writeLong(data, num);
            }
        }

        public static void writeLoopShort(MemoryStream data, List<short> list)
        {
            writeShort(data, (short) list.Count);
            foreach (short num in list)
            {
                writeShort(data, num);
            }
        }

        public static void writeLoopString(MemoryStream data, List<string> list)
        {
            writeShort(data, (short) list.Count);
            foreach (string str in list)
            {
                writeString(data, str);
            }
        }

        public static void writeLoopUByte(MemoryStream data, List<byte> list)
        {
            writeShort(data, (short) list.Count);
            foreach (byte num in list)
            {
                writeUByte(data, num);
            }
        }

        public static void writeLoopUInt(MemoryStream data, List<uint> list)
        {
            if (list == null)
            {
                writeShort(data, 0);
            }
            else
            {
                writeShort(data, (short) list.Count);
                foreach (uint num in list)
                {
                    writeUInt(data, num);
                }
            }
        }

        public static void writeLoopULong(MemoryStream data, List<ulong> list)
        {
            if (list == null)
            {
                writeShort(data, 0);
            }
            else
            {
                writeShort(data, (short) list.Count);
                foreach (ulong num in list)
                {
                    writeULong(data, num);
                }
            }
        }

        public static void writeLoopUShort(MemoryStream data, List<ushort> list)
        {
            writeShort(data, (short) list.Count);
            foreach (ushort num in list)
            {
                writeUShort(data, num);
            }
        }

        public static void writeShort(MemoryStream data, short tmpvalue)
        {
            tmpvalue = IPAddress.HostToNetworkOrder(tmpvalue);
            byte[] bytes = BitConverter.GetBytes(tmpvalue);
            data.Write(bytes, 0, 2);
        }

        public static void writeString(MemoryStream data, string _src)
        {
            MemoryStream stream = new MemoryStream();
            byte[] bytes = Encoding.UTF8.GetBytes(_src);
            stream.Write(bytes, 0, bytes.Length);
            writeShort(data, (short) bytes.Length);
            data.Write(stream.GetBuffer(), 0, (int) stream.Length);
        }

        public static void writeUByte(MemoryStream data, byte tmpvalue)
        {
            byte[] bytes = BitConverter.GetBytes((short) tmpvalue);
            data.Write(bytes, 0, 1);
        }

        public static void writeUInt(MemoryStream data, uint tmpvalue)
        {
            tmpvalue = (uint) IPAddress.HostToNetworkOrder((int) tmpvalue);
            byte[] bytes = BitConverter.GetBytes(tmpvalue);
            data.Write(bytes, 0, 4);
        }

        public static void writeULong(MemoryStream data, ulong tmpvalue)
        {
            byte[] bytes = BitConverter.GetBytes((ulong) IPAddress.HostToNetworkOrder((long) tmpvalue));
            data.Write(bytes, 0, 8);
        }

        public static void writeUShort(MemoryStream data, ushort tmpvalue)
        {
            tmpvalue = (ushort) IPAddress.HostToNetworkOrder((short) tmpvalue);
            byte[] bytes = BitConverter.GetBytes(tmpvalue);
            data.Write(bytes, 0, 2);
        }
    }
}

