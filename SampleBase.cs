﻿using System;
using UnityEngine;

[RequireComponent(typeof(SampleUI))]
public class SampleBase : MonoBehaviour
{
    private SampleUI ui;

    protected virtual void Awake()
    {
        this.ui = base.GetComponent<SampleUI>();
    }

    protected virtual string GetHelpText()
    {
        return string.Empty;
    }

    public static Vector3 GetWorldPos(Vector2 screenPos)
    {
        Ray ray = Camera.main.ScreenPointToRay((Vector3) screenPos);
        float distance = -ray.origin.z / ray.direction.z;
        return ray.GetPoint(distance);
    }

    public static GameObject PickObject(Vector2 screenPos)
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay((Vector3) screenPos), out hit))
        {
            return hit.collider.gameObject;
        }
        return null;
    }

    protected virtual void Start()
    {
        this.ui.helpText = this.GetHelpText();
    }

    public SampleUI UI
    {
        get
        {
            return this.ui;
        }
    }
}

