﻿using System;
using UnityEngine;

public class SampleUI : MonoBehaviour
{
    private Rect backButtonRect = new Rect(5f, 2f, 80f, 46f);
    private Rect helpButtonRect = new Rect(515f, 2f, 80f, 46f);
    private Rect helpRect = new Rect(50f, 60f, 500f, 300f);
    private GUIStyle helpStyle;
    public string helpText = string.Empty;
    public bool showHelp;
    public bool showHelpButton = true;
    public bool showStatusText = true;
    public GUISkin skin;
    private GUIStyle statusStyle;
    private string statusText = string.Empty;
    private Rect statusTextRect = new Rect(30f, 336f, 540f, 60f);
    public Color titleColor = Color.white;
    private Rect titleRect = new Rect(100f, 2f, 400f, 46f);
    private GUIStyle titleStyle;
    private Rect topBarRect = new Rect(0f, -4f, 600f, 56f);
    public static readonly float VirtualScreenHeight = 400f;
    public static readonly float VirtualScreenWidth = 600f;

    public static void ApplyVirtualScreen()
    {
        GUI.matrix = Matrix4x4.Scale(new Vector3(((float) Screen.width) / VirtualScreenWidth, ((float) Screen.height) / VirtualScreenHeight, 1f));
    }

    private void Awake()
    {
        this.titleStyle = new GUIStyle(this.skin.label);
        this.titleStyle.alignment = TextAnchor.MiddleCenter;
        this.titleStyle.normal.textColor = this.titleColor;
        this.statusStyle = new GUIStyle(this.skin.label);
        this.statusStyle.alignment = TextAnchor.LowerCenter;
        this.helpStyle = new GUIStyle(this.skin.label);
        this.helpStyle.alignment = TextAnchor.UpperLeft;
        this.helpStyle.padding.left = 5;
        this.helpStyle.padding.right = 5;
    }

    protected virtual void OnGUI()
    {
        if (this.skin != null)
        {
            GUI.skin = this.skin;
        }
        ApplyVirtualScreen();
        GUI.Box(this.topBarRect, string.Empty);
        if (GUI.Button(this.backButtonRect, "Back"))
        {
            Application.LoadLevel("start");
        }
        GUI.Label(this.titleRect, "FingerGestures - " + base.name, this.titleStyle);
        if (this.showStatusText)
        {
            GUI.Label(this.statusTextRect, this.statusText, this.statusStyle);
        }
        if (((this.helpText.Length > 0) && this.showHelpButton) && (!this.showHelp && GUI.Button(this.helpButtonRect, "Help")))
        {
            this.showHelp = true;
        }
        if (this.showHelp)
        {
            GUI.Box(this.helpRect, "Help");
            GUILayout.BeginArea(this.helpRect);
            GUILayout.BeginVertical(new GUILayoutOption[0]);
            GUILayout.Space(25f);
            GUILayout.Label(this.helpText, this.helpStyle, new GUILayoutOption[0]);
            GUILayout.FlexibleSpace();
            GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Height(40f) };
            if (GUILayout.Button("Close", options))
            {
                this.showHelp = false;
            }
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
    }

    public string StatusText
    {
        get
        {
            return this.statusText;
        }
        set
        {
            this.statusText = value;
        }
    }
}

