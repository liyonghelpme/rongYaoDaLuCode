﻿using System;
using UnityEngine;

[RequireComponent(typeof(Renderer)), AddComponentMenu("Effects/SetRenderQueue")]
public class SetRenderQueue : MonoBehaviour
{
    public int queue = 1;
    public int[] queues;

    private void OnEnble()
    {
        this.Start();
    }

    protected void Start()
    {
        if (((base.renderer != null) && (base.renderer.sharedMaterial != null)) && (this.queues != null))
        {
            base.renderer.sharedMaterial.renderQueue = this.queue;
            for (int i = 0; (i < this.queues.Length) && (i < base.renderer.sharedMaterials.Length); i++)
            {
                base.renderer.sharedMaterials[i].renderQueue = this.queues[i];
            }
        }
    }
}

