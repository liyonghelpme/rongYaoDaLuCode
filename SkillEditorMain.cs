﻿using com.game;
using com.game.manager;
using com.game.module.effect;
using com.u3d.bases.joystick;
using Skill_ActionEditor.Scripts.Manager;
using System;
using UnityEngine;

public class SkillEditorMain : MonoBehaviour
{
    public NGUIJoystick Joystick;

    private void Start()
    {
        AppMap.Instance.IsEditor = true;
        EditorAssetManager.Instance.init(new LoadAssetFinish<UnityEngine.Object>(this.startLoadingFinish));
    }

    private void startLoadingFinish(UnityEngine.Object obj)
    {
        Debug.Log("资源包加载完成,等待手动初始化设置!");
        EffectActionManager.Instance.InitDic();
    }

    private void Update()
    {
        JoystickController.instance.Update();
    }
}

