﻿namespace Skill_ActionEditor.Scripts.Manager
{
    using com.game.manager;
    using com.game.utils;
    using com.u3d.bases.debug;
    using com.u3d.bases.loader;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class EditorAssetManager : MonoBehaviour
    {
        private Dictionary<string, UnityEngine.Object> cachedAssets = new Dictionary<string, UnityEngine.Object>();
        private Dictionary<string, Delegate> cachedAssetsCallBack = new Dictionary<string, Delegate>();
        private volatile int callCount;
        private LoadAssetFinish<UnityEngine.Object> initCall;
        private static volatile EditorAssetManager instance = null;
        private bool isAssetInit;
        private const bool loadAssetbundle = true;
        private Dictionary<string, AssetBundleLoader> loadingAssetsBundles = new Dictionary<string, AssetBundleLoader>();
        public static string persistentDataPath = ("file:///" + Application.persistentDataPath + "/");
        private Dictionary<string, AssetBundleLoader> sharedAssets = new Dictionary<string, AssetBundleLoader>();
        private List<AssetBundleLoader> WaitingAssetsBundles = new List<AssetBundleLoader>();

        private void addCallBack<T>(string assetKey, LoadAssetFinish<T> callBack) where T: UnityEngine.Object
        {
            Dictionary<string, Delegate> cachedAssetsCallBack = this.cachedAssetsCallBack;
            lock (cachedAssetsCallBack)
            {
                if (!this.cachedAssetsCallBack.ContainsKey(assetKey))
                {
                    this.cachedAssetsCallBack[assetKey] = callBack;
                }
                else
                {
                    LoadAssetFinish<T> a = this.cachedAssetsCallBack[assetKey] as LoadAssetFinish<T>;
                    a = (LoadAssetFinish<T>) Delegate.Combine(a, callBack);
                    this.cachedAssetsCallBack[assetKey] = a;
                }
            }
        }

        private void addWaitAssetBundleLoader(AssetBundleLoader assetBundleLoader)
        {
            List<AssetBundleLoader> waitingAssetsBundles = this.WaitingAssetsBundles;
            lock (waitingAssetsBundles)
            {
                for (int i = 0; i < this.WaitingAssetsBundles.Count; i++)
                {
                    if (this.WaitingAssetsBundles[i].fileName.Equals(assetBundleLoader.fileName))
                    {
                        goto Label_0067;
                    }
                }
                this.WaitingAssetsBundles.Add(assetBundleLoader);
            Label_0067:;
            }
        }

        private void callBack(string assetKey, UnityEngine.Object param)
        {
            Delegate delegate2 = null;
            Dictionary<string, Delegate> cachedAssetsCallBack = this.cachedAssetsCallBack;
            lock (cachedAssetsCallBack)
            {
                if (this.cachedAssetsCallBack.ContainsKey(assetKey))
                {
                    delegate2 = this.cachedAssetsCallBack[assetKey];
                    this.cachedAssetsCallBack.Remove(assetKey);
                }
            }
            if (delegate2 != null)
            {
                foreach (Delegate delegate3 in delegate2.GetInvocationList())
                {
                    object[] args = new object[] { param };
                    delegate3.DynamicInvoke(args);
                }
            }
        }

        [DebuggerHidden]
        private IEnumerator DownloadAndStoreAssetBundle(AssetBundleLoader assetBundleLoader, LoadAssetBundleFinish callBack)
        {
            return new <DownloadAndStoreAssetBundle>c__Iterator63 { assetBundleLoader = assetBundleLoader, callBack = callBack, <$>assetBundleLoader = assetBundleLoader, <$>callBack = callBack, <>f__this = this };
        }

        public AssetBundleLoader DownLoadAssetBundle(string url, string fileName, LoadAssetBundleFinish callBack = null)
        {
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(url, fileName, null, null, false, DownLoadState.Init, false, false);
            base.StartCoroutine(this.DownloadAndStoreAssetBundle(assetBundleLoader, callBack));
            return assetBundleLoader;
        }

        private string getAssetBundleLoaderPath(string fileName)
        {
            if (ClientUpdate.updateFinish && File.Exists(Application.persistentDataPath + "/" + fileName))
            {
                return persistentDataPath;
            }
            return dataPathURL;
        }

        private string getAssetKey(string fileName, string assetName, System.Type assetType)
        {
            string str;
            if (assetName != null)
            {
                str = fileName + assetName;
            }
            else
            {
                str = fileName + "main";
            }
            return (str + assetType.Name);
        }

        private string getDepAssetFileName(string assetFileName)
        {
            char[] separator = new char[] { '.' };
            string[] strArray = assetFileName.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return (strArray[0] + "_dep." + strArray[1]);
        }

        private string[] getDependeAssetFilesName(TextAsset text)
        {
            string str = text.text;
            char[] separator = new char[] { '\r', '\n' };
            return str.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }

        public void init(LoadAssetFinish<UnityEngine.Object> callback)
        {
            this.initCall = callback;
        }

        [DebuggerHidden]
        private IEnumerator LoadAndCachedAllAssetInAssetBundle(AssetBundleLoader assetBundleLoader, LoadAssetFinish<UnityEngine.Object> callBack)
        {
            return new <LoadAndCachedAllAssetInAssetBundle>c__Iterator66 { assetBundleLoader = assetBundleLoader, callBack = callBack, <$>assetBundleLoader = assetBundleLoader, <$>callBack = callBack, <>f__this = this };
        }

        public AssetBundleLoader LoadAndCacheSharedAssets(string fileName, LoadAssetFinish<UnityEngine.Object> callBack)
        {
            string path = this.getAssetBundleLoaderPath(fileName);
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(path, fileName, null, null, false, DownLoadState.Init, false, false);
            if (!this.sharedAssets.ContainsKey(fileName))
            {
                Log.info(this, "加载" + path + fileName);
                base.StartCoroutine(this.LoadAndCachedAllAssetInAssetBundle(assetBundleLoader, callBack));
                return assetBundleLoader;
            }
            if (callBack != null)
            {
                callBack(null);
            }
            return assetBundleLoader;
        }

        private UnityEngine.Object LoadAsset(AssetBundleLoader assetBundleLoader, string assetName, System.Type assettype)
        {
            UnityEngine.Object obj2 = null;
            if (assetBundleLoader.state != DownLoadState.Loaded)
            {
                return obj2;
            }
            if ((assetName != null) && !assetName.Equals(string.Empty))
            {
                UnityEngine.Object obj3 = assetBundleLoader.assetBundle.Load(assetName, assettype);
                if (obj3 == null)
                {
                    object[] objArray1 = new object[] { assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name, assetName };
                    Log.error(this, string.Format("资源包{0}{1}中类型为{2}的资源 {3} 不存在", objArray1));
                    return obj2;
                }
                object[] args = new object[] { assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name, assetName };
                Log.info(this, string.Format("成功在资源包{0}{1}中加载类型为{2}的资源 {3}", args));
                return obj3;
            }
            Log.info(this, assetBundleLoader.fileName + " " + assetBundleLoader.path);
            UnityEngine.Object mainAsset = assetBundleLoader.assetBundle.mainAsset;
            if (mainAsset == null)
            {
                Log.error(this, string.Format("资源包{0}{1}中主资源不存在", assetBundleLoader.path, assetBundleLoader.fileName));
                return obj2;
            }
            if (assettype != typeof(UnityEngine.Object))
            {
                if (mainAsset.GetType() != assettype)
                {
                    Log.error(this, string.Format("资源包{0}{1}中主资源不是{2}类型", assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name));
                    return obj2;
                }
                Log.info(this, string.Format("成功在资源包{0}{1}中加载类型为{2}的主资源", assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name));
            }
            return mainAsset;
        }

        public AssetBundleLoader LoadAsset<T>(string fileName, LoadAssetFinish<T> callBack, string assetName = null, bool cache = false, bool asynLoad = true) where T: UnityEngine.Object
        {
            string path = this.getAssetBundleLoaderPath(fileName);
            return this.LoadAsset<T>(path, fileName, callBack, assetName, cache, false, asynLoad);
        }

        public AssetBundleLoader LoadAsset<T>(string path, string fileName, LoadAssetFinish<T> callBack, string assetName = null, bool cache = false, bool scene = false, bool asynload = false) where T: UnityEngine.Object
        {
            if (this.sharedAssets.ContainsKey(fileName))
            {
                UnityEngine.Object obj2 = this.LoadAsset(this.sharedAssets[fileName], assetName, typeof(T));
                if (callBack != null)
                {
                    T local = obj2 as T;
                    callBack(local);
                }
                return this.sharedAssets[fileName];
            }
            string key = this.getAssetKey(fileName, assetName, typeof(T));
            if (this.cachedAssets.ContainsKey(key))
            {
                if (callBack != null)
                {
                    T local2 = this.cachedAssets[key] as T;
                    callBack(local2);
                }
                return new AssetBundleLoader(path, fileName, typeof(T), assetName, cache, DownLoadState.Cached, scene, asynload);
            }
            if (callBack != null)
            {
                this.addCallBack<T>(key, callBack);
            }
            Dictionary<string, AssetBundleLoader> loadingAssetsBundles = this.loadingAssetsBundles;
            lock (loadingAssetsBundles)
            {
                if (this.loadingAssetsBundles.ContainsKey(fileName))
                {
                    return this.loadingAssetsBundles[fileName];
                }
            }
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(path, fileName, typeof(T), assetName, cache, DownLoadState.Init, scene, asynload);
            this.addWaitAssetBundleLoader(assetBundleLoader);
            return assetBundleLoader;
        }

        private AssetBundleRequest LoadAssetAsync(AssetBundleLoader assetBundleLoader, string assetName, System.Type assettype)
        {
            AssetBundleRequest request = null;
            if (assetBundleLoader.state != DownLoadState.Loaded)
            {
                return request;
            }
            if ((assetName == null) || assetName.Equals(string.Empty))
            {
                int num = assetBundleLoader.fileName.LastIndexOf('/');
                int num2 = assetBundleLoader.fileName.LastIndexOf('.');
                assetName = assetBundleLoader.fileName.Substring(num + 1, (num2 - num) - 1);
            }
            return assetBundleLoader.assetBundle.LoadAsync(assetName, assettype);
        }

        [DebuggerHidden]
        private IEnumerator LoadAssetBundle(AssetBundleLoader assetBundleLoader, LoadAssetBundleFinish callBack)
        {
            return new <LoadAssetBundle>c__Iterator64 { assetBundleLoader = assetBundleLoader, callBack = callBack, <$>assetBundleLoader = assetBundleLoader, <$>callBack = callBack, <>f__this = this };
        }

        public AssetBundleLoader LoadAssetBundle(string path, string fileName, LoadAssetBundleFinish callBack = null)
        {
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(path, fileName, null, null, false, DownLoadState.Init, false, false);
            base.StartCoroutine(this.LoadAssetBundle(assetBundleLoader, callBack));
            return assetBundleLoader;
        }

        public AssetBundleLoader LoadAssetFromResources<T>(string fileName, LoadAssetFinish<T> callBack, string assetName = null, bool cache = false) where T: UnityEngine.Object
        {
            fileName = fileName.Replace(".assetbundle", string.Empty);
            callBack(ResMgr.instance.load(fileName, typeof(T)) as T);
            return new AssetBundleLoader(fileName, assetName, typeof(T), null, false, DownLoadState.Init, false, false);
        }

        [DebuggerHidden]
        private IEnumerator LoadAssetInAssetBundle(AssetBundleLoader assetBundleLoader)
        {
            return new <LoadAssetInAssetBundle>c__Iterator65 { assetBundleLoader = assetBundleLoader, <$>assetBundleLoader = assetBundleLoader, <>f__this = this };
        }

        public void RemoveAllSharedAssetBundle(bool isAll = true)
        {
            Dictionary<string, AssetBundleLoader> sharedAssets = this.sharedAssets;
            lock (sharedAssets)
            {
                foreach (AssetBundleLoader loader in this.sharedAssets.Values)
                {
                    loader.UnloadAssetBundle(isAll);
                }
                this.sharedAssets.Clear();
                Resources.UnloadUnusedAssets();
            }
        }

        public bool RemoveCachedAsset<T>(string fileName, string assetName) where T: UnityEngine.Object
        {
            string key = this.getAssetKey(fileName, assetName, typeof(T));
            Dictionary<string, UnityEngine.Object> cachedAssets = this.cachedAssets;
            lock (cachedAssets)
            {
                return this.cachedAssets.Remove(key);
            }
        }

        public void RemoveSharedAssetBundle(string assetBundleName, bool isAll = true)
        {
            Dictionary<string, AssetBundleLoader> sharedAssets = this.sharedAssets;
            lock (sharedAssets)
            {
                if (this.sharedAssets.ContainsKey(assetBundleName))
                {
                    this.sharedAssets[assetBundleName].UnloadAssetBundle(isAll);
                    this.sharedAssets.Remove(assetBundleName);
                    Resources.UnloadUnusedAssets();
                }
            }
        }

        private void Update()
        {
            if (!this.isAssetInit)
            {
                this.initCall(null);
                this.isAssetInit = true;
            }
            List<AssetBundleLoader> waitingAssetsBundles = this.WaitingAssetsBundles;
            lock (waitingAssetsBundles)
            {
                Dictionary<string, AssetBundleLoader> loadingAssetsBundles = this.loadingAssetsBundles;
                lock (loadingAssetsBundles)
                {
                    if ((this.WaitingAssetsBundles.Count > 0) && (this.loadingAssetsBundles.Count < this.MaxLoadingNum))
                    {
                        AssetBundleLoader loader = this.WaitingAssetsBundles[0];
                        if (!this.loadingAssetsBundles.ContainsKey(loader.fileName))
                        {
                            this.loadingAssetsBundles.Add(loader.fileName, loader);
                            base.StartCoroutine(this.LoadAssetInAssetBundle(loader));
                        }
                        this.WaitingAssetsBundles.RemoveAt(0);
                    }
                }
            }
        }

        public static string dataPathURL
        {
            get
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    return (Application.streamingAssetsPath + "/");
                }
                return ("file:///" + Application.streamingAssetsPath + "/");
            }
        }

        public static EditorAssetManager Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject target = new GameObject("AssetManagerObj");
                    UnityEngine.Object.DontDestroyOnLoad(target);
                    instance = target.AddComponent<EditorAssetManager>();
                    instance.MaxLoadingNum = 1;
                }
                return instance;
            }
        }

        private int MaxLoadingNum { get; set; }

        [CompilerGenerated]
        private sealed class <DownloadAndStoreAssetBundle>c__Iterator63 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal LoadAssetBundleFinish <$>callBack;
            internal EditorAssetManager <>f__this;
            internal AssetBundleLoader assetBundleLoader;
            internal LoadAssetBundleFinish callBack;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(true, true));
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.callBack != null)
                        {
                            this.callBack(this.assetBundleLoader);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAndCachedAllAssetInAssetBundle>c__Iterator66 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal LoadAssetFinish<UnityEngine.Object> <$>callBack;
            internal Dictionary<string, AssetBundleLoader> <$s_884>__0;
            internal EditorAssetManager <>f__this;
            internal AssetBundleLoader assetBundleLoader;
            internal LoadAssetFinish<UnityEngine.Object> callBack;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(false, false));
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.assetBundleLoader.state == DownLoadState.Loaded)
                        {
                            this.<$s_884>__0 = this.<>f__this.sharedAssets;
                            lock (this.<$s_884>__0)
                            {
                                if (!this.<>f__this.sharedAssets.ContainsKey(this.assetBundleLoader.fileName))
                                {
                                    this.<>f__this.sharedAssets.Add(this.assetBundleLoader.fileName, this.assetBundleLoader);
                                }
                            }
                        }
                        if (this.callBack != null)
                        {
                            this.callBack(null);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetBundle>c__Iterator64 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal LoadAssetBundleFinish <$>callBack;
            internal EditorAssetManager <>f__this;
            internal AssetBundleLoader assetBundleLoader;
            internal LoadAssetBundleFinish callBack;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(false, false));
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.callBack != null)
                        {
                            this.callBack(this.assetBundleLoader);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetInAssetBundle>c__Iterator65 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal string[] <$s_876>__9;
            internal int <$s_877>__10;
            internal List<AssetBundleLoader>.Enumerator <$s_878>__13;
            internal List<AssetBundleLoader>.Enumerator <$s_879>__16;
            internal Dictionary<string, AssetBundleLoader> <$s_880>__18;
            internal Dictionary<string, AssetBundleLoader> <$s_881>__21;
            internal EditorAssetManager <>f__this;
            internal UnityEngine.Object <asset>__19;
            internal string <assetKey>__15;
            internal string <assetName>__0;
            internal bool <cache>__1;
            internal string[] <depAssetBundles>__3;
            internal string <depAssetFileName>__11;
            internal List<AssetBundleLoader> <dependAssetBundles>__4;
            internal AssetBundleLoader <dependFileLoader>__7;
            internal string <depFileAssetName>__5;
            internal string <depPath>__12;
            internal TextAsset <depTxt>__8;
            internal AssetBundleLoader <loader>__14;
            internal AssetBundleLoader <loader>__17;
            internal string <path>__6;
            internal AssetBundleRequest <request>__20;
            internal bool <scene>__2;
            internal AssetBundleLoader assetBundleLoader;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 2:
                        try
                        {
                        }
                        finally
                        {
                            this.<$s_878>__13.Dispose();
                        }
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<assetName>__0 = this.assetBundleLoader.assetName;
                        this.<cache>__1 = this.assetBundleLoader.cache;
                        this.<scene>__2 = this.assetBundleLoader.isScene;
                        this.<depAssetBundles>__3 = null;
                        this.<dependAssetBundles>__4 = new List<AssetBundleLoader>();
                        if (!this.<scene>__2)
                        {
                            goto Label_02CB;
                        }
                        this.<depFileAssetName>__5 = this.<>f__this.getDepAssetFileName(this.assetBundleLoader.fileName);
                        this.<path>__6 = this.<>f__this.getAssetBundleLoaderPath(this.<depFileAssetName>__5);
                        this.<dependFileLoader>__7 = new AssetBundleLoader(this.<path>__6, this.<depFileAssetName>__5, null, null, false, DownLoadState.Init, false, false);
                        this.$current = this.<>f__this.StartCoroutine(this.<dependFileLoader>__7.LoadAssetBundle(false, false));
                        this.$PC = 1;
                        goto Label_0598;

                    case 1:
                        if (this.<dependFileLoader>__7.state == DownLoadState.Loaded)
                        {
                            this.<depTxt>__8 = this.<dependFileLoader>__7.assetBundle.mainAsset as TextAsset;
                            this.<depAssetBundles>__3 = this.<>f__this.getDependeAssetFilesName(this.<depTxt>__8);
                            this.<dependFileLoader>__7.UnloadAssetBundle(false);
                        }
                        if (this.<depAssetBundles>__3 != null)
                        {
                            this.<$s_876>__9 = this.<depAssetBundles>__3;
                            this.<$s_877>__10 = 0;
                            while (this.<$s_877>__10 < this.<$s_876>__9.Length)
                            {
                                this.<depAssetFileName>__11 = this.<$s_876>__9[this.<$s_877>__10];
                                Log.info(this.<>f__this, "加载" + this.assetBundleLoader.fileName + "的依赖" + this.<depAssetFileName>__11);
                                this.<depPath>__12 = this.<>f__this.getAssetBundleLoaderPath(this.<depAssetFileName>__11);
                                this.<dependAssetBundles>__4.Add(new AssetBundleLoader(this.<depPath>__12, this.<depAssetFileName>__11, null, null, false, DownLoadState.Init, false, false));
                                this.<$s_877>__10++;
                            }
                        }
                        this.<$s_878>__13 = this.<dependAssetBundles>__4.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 2:
                        break;

                    case 3:
                        this.<assetKey>__15 = this.<>f__this.getAssetKey(this.assetBundleLoader.fileName, this.<assetName>__0, this.assetBundleLoader.assetType);
                        if (!this.<scene>__2)
                        {
                            this.<asset>__19 = null;
                            if (this.assetBundleLoader.asynLoad)
                            {
                                this.<request>__20 = this.<>f__this.LoadAssetAsync(this.assetBundleLoader, this.<assetName>__0, this.assetBundleLoader.assetType);
                                if (this.<request>__20 == null)
                                {
                                    goto Label_0485;
                                }
                                this.$current = this.<request>__20;
                                this.$PC = 4;
                                goto Label_0598;
                            }
                            this.<asset>__19 = this.<>f__this.LoadAsset(this.assetBundleLoader, this.<assetName>__0, this.assetBundleLoader.assetType);
                            goto Label_0485;
                        }
                        this.assetBundleLoader.UnloadAssetBundle(false);
                        this.<$s_879>__16 = this.<dependAssetBundles>__4.GetEnumerator();
                        try
                        {
                            while (this.<$s_879>__16.MoveNext())
                            {
                                this.<loader>__17 = this.<$s_879>__16.Current;
                                this.<loader>__17.UnloadAssetBundle(false);
                            }
                        }
                        finally
                        {
                            this.<$s_879>__16.Dispose();
                        }
                        this.<$s_880>__18 = this.<>f__this.loadingAssetsBundles;
                        lock (this.<$s_880>__18)
                        {
                            this.<>f__this.loadingAssetsBundles.Remove(this.assetBundleLoader.fileName);
                        }
                        GC.Collect();
                        goto Label_058F;

                    case 4:
                        this.<asset>__19 = this.<request>__20.asset;
                        goto Label_0485;

                    default:
                        goto Label_0596;
                }
                try
                {
                    switch (num)
                    {
                        case 2:
                            if (this.<loader>__14.state == DownLoadState.Loaded)
                            {
                                Log.info(this.<>f__this, "成功加载" + this.<loader>__14.path + this.<loader>__14.fileName);
                            }
                            break;
                    }
                    while (this.<$s_878>__13.MoveNext())
                    {
                        this.<loader>__14 = this.<$s_878>__13.Current;
                        this.$current = this.<>f__this.StartCoroutine(this.<loader>__14.LoadAssetBundle(false, false));
                        this.$PC = 2;
                        flag = true;
                        goto Label_0598;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                    }
                    this.<$s_878>__13.Dispose();
                }
            Label_02CB:
                this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(false, false));
                this.$PC = 3;
                goto Label_0598;
            Label_0485:
                if ((this.<asset>__19 == null) && !this.assetBundleLoader.fileName.Contains("bust"))
                {
                    Log.error(this.<>f__this, "asset==null" + this.assetBundleLoader.fileName);
                }
                if (this.<cache>__1)
                {
                    this.<>f__this.cachedAssets[this.<assetKey>__15] = this.<asset>__19;
                }
                if ((this.assetBundleLoader.state == DownLoadState.Loaded) && !this.assetBundleLoader.delayUnload)
                {
                    this.assetBundleLoader.UnloadAssetBundle(false);
                }
                this.<$s_881>__21 = this.<>f__this.loadingAssetsBundles;
                lock (this.<$s_881>__21)
                {
                    this.<>f__this.loadingAssetsBundles.Remove(this.assetBundleLoader.fileName);
                }
                ShaderUtils.ResetObjectShader(this.<asset>__19);
                this.<>f__this.callBack(this.<assetKey>__15, this.<asset>__19);
            Label_058F:
                this.$PC = -1;
            Label_0596:
                return false;
            Label_0598:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

