﻿using System;
using UnityEngine;

public class StartMenu : MonoBehaviour
{
    public float buttonHeight = 80f;
    public GUIStyle buttonStyle;
    private Transform currentMenuRoot;
    public Transform itemsTree;
    public float menuWidth = 450f;
    private Rect screenRect = new Rect(0f, 0f, SampleUI.VirtualScreenWidth, SampleUI.VirtualScreenHeight);
    public float sideBorder = 30f;
    public GUIStyle titleStyle;

    private void OnGUI()
    {
        SampleUI.ApplyVirtualScreen();
        GUILayout.BeginArea(this.screenRect);
        GUILayout.BeginHorizontal(new GUILayoutOption[0]);
        GUILayout.Space(this.sideBorder);
        if (this.CurrentMenuRoot != null)
        {
            GUILayout.BeginVertical(new GUILayoutOption[0]);
            GUILayout.Space(15f);
            GUILayout.Label(this.CurrentMenuRoot.name, this.titleStyle, new GUILayoutOption[0]);
            for (int i = 0; i < this.CurrentMenuRoot.childCount; i++)
            {
                Transform child = this.CurrentMenuRoot.GetChild(i);
                GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Height(this.buttonHeight) };
                if (GUILayout.Button(child.name, options))
                {
                    MenuNode component = child.GetComponent<MenuNode>();
                    if (((component != null) && (component.sceneName != null)) && (component.sceneName.Length > 0))
                    {
                        Application.LoadLevel(component.sceneName);
                    }
                    else if (child.childCount > 0)
                    {
                        this.CurrentMenuRoot = child;
                    }
                }
                GUILayout.Space(5f);
            }
            GUILayout.FlexibleSpace();
            if ((this.CurrentMenuRoot != this.itemsTree) && (this.CurrentMenuRoot.parent != null))
            {
                GUILayoutOption[] optionArray2 = new GUILayoutOption[] { GUILayout.Height(this.buttonHeight) };
                if (GUILayout.Button("<< BACK <<", optionArray2))
                {
                    this.CurrentMenuRoot = this.CurrentMenuRoot.parent;
                }
                GUILayout.Space(15f);
            }
            GUILayout.EndVertical();
        }
        GUILayout.Space(this.sideBorder);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private void Start()
    {
        this.CurrentMenuRoot = this.itemsTree;
    }

    public Transform CurrentMenuRoot
    {
        get
        {
            return this.currentMenuRoot;
        }
        set
        {
            this.currentMenuRoot = value;
        }
    }
}

