﻿using System;
using UnityEngine;

[Serializable]
public class StateColour
{
    [SerializeField]
    public Color normal;
    [SerializeField]
    public Color selected;
}

