﻿using System;
using UnityEngine;

public class SwipeParticlesEmitter : MonoBehaviour
{
    public float baseSpeed = 4f;
    public ParticleEmitter emitter;
    public float swipeVelocityScale = 0.001f;

    public void Emit(FingerGestures.SwipeDirection direction, float swipeVelocity)
    {
        Vector3 up;
        if (direction == FingerGestures.SwipeDirection.Up)
        {
            up = Vector3.up;
        }
        else if (direction == FingerGestures.SwipeDirection.Down)
        {
            up = Vector3.down;
        }
        else if (direction == FingerGestures.SwipeDirection.Right)
        {
            up = Vector3.right;
        }
        else
        {
            up = Vector3.left;
        }
        this.emitter.transform.rotation = Quaternion.LookRotation(up);
        Vector3 localVelocity = this.emitter.localVelocity;
        localVelocity.z = (this.baseSpeed * this.swipeVelocityScale) * swipeVelocity;
        this.emitter.localVelocity = localVelocity;
        this.emitter.Emit();
    }

    private void Start()
    {
        if (this.emitter == null)
        {
            this.emitter = base.particleEmitter;
        }
        this.emitter.emit = false;
    }
}

