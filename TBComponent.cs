﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public abstract class TBComponent : MonoBehaviour
{
    private int fingerIndex = -1;
    private Vector2 fingerPos;

    protected TBComponent()
    {
    }

    protected bool Send(Message msg)
    {
        if (!msg.enabled)
        {
            return false;
        }
        GameObject target = msg.target;
        if (target == null)
        {
            target = base.gameObject;
        }
        target.SendMessage(msg.methodName, SendMessageOptions.DontRequireReceiver);
        return true;
    }

    protected virtual void Start()
    {
        if (base.collider == null)
        {
            Debug.LogError(base.name + " must have a valid collider.");
            base.enabled = false;
        }
    }

    public int FingerIndex
    {
        get
        {
            return this.fingerIndex;
        }
        protected set
        {
            this.fingerIndex = value;
        }
    }

    public Vector2 FingerPos
    {
        get
        {
            return this.fingerPos;
        }
        protected set
        {
            this.fingerPos = value;
        }
    }

    public delegate void EventHandler<T>(T sender) where T: TBComponent;

    [Serializable]
    public class Message
    {
        public bool enabled;
        public string methodName;
        public GameObject target;

        public Message()
        {
            this.enabled = true;
            this.methodName = "MethodToCall";
        }

        public Message(string methodName)
        {
            this.enabled = true;
            this.methodName = "MethodToCall";
            this.methodName = methodName;
        }

        public Message(string methodName, bool enabled)
        {
            this.enabled = true;
            this.methodName = "MethodToCall";
            this.enabled = enabled;
            this.methodName = methodName;
        }
    }
}

