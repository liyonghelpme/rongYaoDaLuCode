﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[AddComponentMenu("FingerGestures/Toolbox/Drag")]
public class TBDrag : TBComponent
{
    public TBComponent.Message dragBeginMessage = new TBComponent.Message("OnDragBegin");
    public TBComponent.Message dragEndMessage = new TBComponent.Message("OnDragEnd");
    private bool dragging;
    public TBComponent.Message dragMoveMessage = new TBComponent.Message("OnDragMove", false);
    private Vector2 moveDelta;

    public event TBComponent.EventHandler<TBDrag> OnDragBegin;

    public event TBComponent.EventHandler<TBDrag> OnDragEnd;

    public event TBComponent.EventHandler<TBDrag> OnDragMove;

    public bool BeginDrag(int fingerIndex, Vector2 fingerPos)
    {
        if (this.Dragging)
        {
            return false;
        }
        base.FingerIndex = fingerIndex;
        base.FingerPos = fingerPos;
        this.Dragging = true;
        if (this.OnDragBegin != null)
        {
            this.OnDragBegin(this);
        }
        base.Send(this.dragBeginMessage);
        return true;
    }

    public bool EndDrag()
    {
        if (!this.Dragging)
        {
            return false;
        }
        if (this.OnDragEnd != null)
        {
            this.OnDragEnd(this);
        }
        base.Send(this.dragEndMessage);
        this.Dragging = false;
        base.FingerIndex = -1;
        return true;
    }

    private void FingerGestures_OnDragEnd(int fingerIndex, Vector2 fingerPos)
    {
        if (this.Dragging && (base.FingerIndex == fingerIndex))
        {
            base.FingerPos = fingerPos;
            this.EndDrag();
        }
    }

    private void FingerGestures_OnDragMove(int fingerIndex, Vector2 fingerPos, Vector2 delta)
    {
        if (this.Dragging && (base.FingerIndex == fingerIndex))
        {
            base.FingerPos = fingerPos;
            this.MoveDelta = delta;
            if (this.OnDragMove != null)
            {
                this.OnDragMove(this);
            }
            base.Send(this.dragMoveMessage);
        }
    }

    private void OnDisable()
    {
        if (this.Dragging)
        {
            this.EndDrag();
        }
    }

    public bool Dragging
    {
        get
        {
            return this.dragging;
        }
        private set
        {
            if (this.dragging != value)
            {
                this.dragging = value;
                if (this.dragging)
                {
                    FingerGestures.OnFingerDragMove += new FingerGestures.FingerDragMoveEventHandler(this.FingerGestures_OnDragMove);
                    FingerGestures.OnFingerDragEnd += new FingerGestures.FingerDragEndEventHandler(this.FingerGestures_OnDragEnd);
                }
                else
                {
                    FingerGestures.OnFingerDragMove -= new FingerGestures.FingerDragMoveEventHandler(this.FingerGestures_OnDragMove);
                    FingerGestures.OnFingerDragEnd -= new FingerGestures.FingerDragEndEventHandler(this.FingerGestures_OnDragEnd);
                }
            }
        }
    }

    public Vector2 MoveDelta
    {
        get
        {
            return this.moveDelta;
        }
        private set
        {
            this.moveDelta = value;
        }
    }
}

