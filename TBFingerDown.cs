﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[AddComponentMenu("FingerGestures/Toolbox/FingerDown")]
public class TBFingerDown : TBComponent
{
    public TBComponent.Message message = new TBComponent.Message("OnFingerDown");

    public event TBComponent.EventHandler<TBFingerDown> OnFingerDown;

    public bool RaiseFingerDown(int fingerIndex, Vector2 fingerPos)
    {
        base.FingerIndex = fingerIndex;
        base.FingerPos = fingerPos;
        if (this.OnFingerDown != null)
        {
            this.OnFingerDown(this);
        }
        base.Send(this.message);
        return true;
    }
}

