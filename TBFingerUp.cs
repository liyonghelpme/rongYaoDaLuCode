﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[AddComponentMenu("FingerGestures/Toolbox/FingerUp")]
public class TBFingerUp : TBComponent
{
    public TBComponent.Message message = new TBComponent.Message("OnFingerUp");
    private float timeHeldDown;

    public event TBComponent.EventHandler<TBFingerUp> OnFingerUp;

    public bool RaiseFingerUp(int fingerIndex, Vector2 fingerPos, float timeHeldDown)
    {
        base.FingerIndex = fingerIndex;
        base.FingerPos = fingerPos;
        this.TimeHeldDown = timeHeldDown;
        if (this.OnFingerUp != null)
        {
            this.OnFingerUp(this);
        }
        base.Send(this.message);
        return true;
    }

    public float TimeHeldDown
    {
        get
        {
            return this.timeHeldDown;
        }
        private set
        {
            this.timeHeldDown = value;
        }
    }
}

