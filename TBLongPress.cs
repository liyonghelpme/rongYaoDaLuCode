﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[AddComponentMenu("FingerGestures/Toolbox/LongPress")]
public class TBLongPress : TBComponent
{
    public TBComponent.Message message = new TBComponent.Message("OnLongPress");

    public event TBComponent.EventHandler<TBLongPress> OnLongPress;

    public bool RaiseLongPress(int fingerIndex, Vector2 fingerPos)
    {
        base.FingerIndex = fingerIndex;
        base.FingerPos = fingerPos;
        if (this.OnLongPress != null)
        {
            this.OnLongPress(this);
        }
        base.Send(this.message);
        return true;
    }
}

