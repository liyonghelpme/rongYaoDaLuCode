﻿using System;
using UnityEngine;

[AddComponentMenu("FingerGestures/Toolbox/Misc/Pinch-Zoom"), RequireComponent(typeof(Camera))]
public class TBPinchZoom : MonoBehaviour
{
    private float defaultFov;
    private float defaultOrthoSize;
    private Vector3 defaultPos = Vector3.zero;
    public float maxZoomAmount = 50f;
    public float minZoomAmount;
    private float zoomAmount;
    public ZoomMethod zoomMethod;
    public float zoomSpeed = 1.5f;

    private void FingerGestures_OnPinchMove(Vector2 fingerPos1, Vector2 fingerPos2, float delta)
    {
        this.ZoomAmount += this.zoomSpeed * delta;
    }

    private void OnDisable()
    {
        FingerGestures.OnPinchMove -= new FingerGestures.PinchMoveEventHandler(this.FingerGestures_OnPinchMove);
    }

    private void OnEnable()
    {
        FingerGestures.OnPinchMove += new FingerGestures.PinchMoveEventHandler(this.FingerGestures_OnPinchMove);
    }

    public void SetDefaults()
    {
        this.DefaultPos = base.transform.position;
        this.DefaultFov = base.camera.fov;
        this.DefaultOrthoSize = base.camera.orthographicSize;
    }

    private void Start()
    {
        this.SetDefaults();
    }

    public float DefaultFov
    {
        get
        {
            return this.defaultFov;
        }
        set
        {
            this.defaultFov = value;
        }
    }

    public float DefaultOrthoSize
    {
        get
        {
            return this.defaultOrthoSize;
        }
        set
        {
            this.defaultOrthoSize = value;
        }
    }

    public Vector3 DefaultPos
    {
        get
        {
            return this.defaultPos;
        }
        set
        {
            this.defaultPos = value;
        }
    }

    public float ZoomAmount
    {
        get
        {
            return this.zoomAmount;
        }
        set
        {
            this.zoomAmount = Mathf.Clamp(value, this.minZoomAmount, this.maxZoomAmount);
            switch (this.zoomMethod)
            {
                case ZoomMethod.Position:
                    base.transform.position = this.defaultPos + ((Vector3) (this.zoomAmount * base.transform.forward));
                    break;

                case ZoomMethod.FOV:
                    if (base.camera.orthographic)
                    {
                        base.camera.orthographicSize = Mathf.Max((float) (this.defaultOrthoSize - this.zoomAmount), (float) 0.1f);
                    }
                    else
                    {
                        base.camera.fov = Mathf.Max((float) (this.defaultFov - this.zoomAmount), (float) 0.1f);
                    }
                    break;
            }
        }
    }

    public enum ZoomMethod
    {
        Position,
        FOV
    }
}

