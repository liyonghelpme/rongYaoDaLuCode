﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[AddComponentMenu("FingerGestures/Toolbox/Swipe")]
public class TBSwipe : TBComponent
{
    private FingerGestures.SwipeDirection direction;
    public float minVelocity;
    public bool swipeDown = true;
    public TBComponent.Message swipeDownMessage = new TBComponent.Message("OnSwipeDown", false);
    public bool swipeLeft = true;
    public TBComponent.Message swipeLeftMessage = new TBComponent.Message("OnSwipeLeft", false);
    public TBComponent.Message swipeMessage = new TBComponent.Message("OnSwipe");
    public bool swipeRight = true;
    public TBComponent.Message swipeRightMessage = new TBComponent.Message("OnSwipeRight", false);
    public bool swipeUp = true;
    public TBComponent.Message swipeUpMessage = new TBComponent.Message("OnSwipeUp", false);
    private float velocity;

    public event TBComponent.EventHandler<TBSwipe> OnSwipe;

    private TBComponent.Message GetMessageForSwipeDirection(FingerGestures.SwipeDirection direction)
    {
        if (direction == FingerGestures.SwipeDirection.Left)
        {
            return this.swipeLeftMessage;
        }
        if (direction == FingerGestures.SwipeDirection.Right)
        {
            return this.swipeRightMessage;
        }
        if (direction == FingerGestures.SwipeDirection.Up)
        {
            return this.swipeUpMessage;
        }
        return this.swipeDownMessage;
    }

    public bool IsValid(FingerGestures.SwipeDirection direction)
    {
        if (direction == FingerGestures.SwipeDirection.Left)
        {
            return this.swipeLeft;
        }
        if (direction == FingerGestures.SwipeDirection.Right)
        {
            return this.swipeRight;
        }
        if (direction == FingerGestures.SwipeDirection.Up)
        {
            return this.swipeUp;
        }
        return ((direction == FingerGestures.SwipeDirection.Down) && this.swipeDown);
    }

    public bool RaiseSwipe(int fingerIndex, Vector2 fingerPos, FingerGestures.SwipeDirection direction, float velocity)
    {
        if (velocity < this.minVelocity)
        {
            return false;
        }
        if (!this.IsValid(direction))
        {
            return false;
        }
        base.FingerIndex = fingerIndex;
        base.FingerPos = fingerPos;
        this.Direction = direction;
        this.Velocity = velocity;
        if (this.OnSwipe != null)
        {
            this.OnSwipe(this);
        }
        base.Send(this.swipeMessage);
        base.Send(this.GetMessageForSwipeDirection(direction));
        return true;
    }

    public FingerGestures.SwipeDirection Direction
    {
        get
        {
            return this.direction;
        }
        protected set
        {
            this.direction = value;
        }
    }

    public float Velocity
    {
        get
        {
            return this.velocity;
        }
        protected set
        {
            this.velocity = value;
        }
    }
}

