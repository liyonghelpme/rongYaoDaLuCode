﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[AddComponentMenu("FingerGestures/Toolbox/Tap")]
public class TBTap : TBComponent
{
    public TBComponent.Message message = new TBComponent.Message("OnTap");
    public int tapCount = 1;

    public event TBComponent.EventHandler<TBTap> OnTap;

    public bool RaiseTap(int fingerIndex, Vector2 fingerPos, int tapCount)
    {
        if (tapCount != this.tapCount)
        {
            return false;
        }
        base.FingerIndex = fingerIndex;
        base.FingerPos = fingerPos;
        if (this.OnTap != null)
        {
            this.OnTap(this);
        }
        base.Send(this.message);
        return true;
    }
}

