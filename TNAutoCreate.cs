﻿using System;
using UnityEngine;

public class TNAutoCreate : MonoBehaviour
{
    public bool persistent;
    public GameObject prefab;

    private void Start()
    {
        TNManager.Create(this.prefab, base.transform.position, base.transform.rotation, this.persistent);
        UnityEngine.Object.Destroy(base.gameObject);
    }
}

