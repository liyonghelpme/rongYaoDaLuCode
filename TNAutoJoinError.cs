﻿using System;
using UnityEngine;

[RequireComponent(typeof(TextMesh))]
public class TNAutoJoinError : MonoBehaviour
{
    private void AutoJoinError(string message)
    {
        base.GetComponent<TextMesh>().text = message;
    }
}

