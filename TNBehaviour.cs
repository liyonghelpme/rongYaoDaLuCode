﻿using System;
using UnityEngine;

[RequireComponent(typeof(TNObject))]
public abstract class TNBehaviour : MonoBehaviour
{
    private TNObject mTNO;

    protected TNBehaviour()
    {
    }

    public virtual void DestroySelf()
    {
        this.tno.DestroySelf();
    }

    protected virtual void OnEnable()
    {
        if (Application.isPlaying)
        {
            this.tno.rebuildMethodList = true;
        }
    }

    public TNObject tno
    {
        get
        {
            if (this.mTNO == null)
            {
                this.mTNO = base.GetComponent<TNObject>();
            }
            return this.mTNO;
        }
    }
}

