﻿using System;
using System.Runtime.CompilerServices;
using TNet;
using UnityEngine;

public abstract class TNLobbyClient : MonoBehaviour
{
    public static string errorString = string.Empty;
    public static bool isActive = false;
    public static ServerList knownServers = new ServerList();
    public static OnListChange onChange;
    public string remoteAddress;
    public int remotePort = 0x1409;

    protected TNLobbyClient()
    {
    }

    protected virtual void OnDisable()
    {
        errorString = string.Empty;
        knownServers.Clear();
    }

    public delegate void OnListChange();
}

