﻿using System;
using TNet;
using UnityEngine;

[AddComponentMenu("TNet/Sync Rigidbody"), RequireComponent(typeof(Rigidbody))]
public class TNSyncRigidbody : TNBehaviour
{
    public bool isImportant;
    private Vector3 mLastPos;
    private Vector3 mLastRot;
    private float mNext;
    private Rigidbody mRb;
    private Transform mTrans;
    private bool mWasSleeping;
    public float updatesPerSecond = 10f;

    private void Awake()
    {
        this.mTrans = base.transform;
        this.mRb = base.GetComponent<Rigidbody>();
        this.mLastPos = this.mTrans.position;
        this.mLastRot = this.mTrans.rotation.eulerAngles;
        this.UpdateInterval();
    }

    private void FixedUpdate()
    {
        if ((this.updatesPerSecond >= 0.001f) && (base.tno.isMine && TNManager.isInChannel))
        {
            bool flag = this.mRb.IsSleeping();
            if (!flag || !this.mWasSleeping)
            {
                this.mNext -= Time.deltaTime;
                if (this.mNext <= 0f)
                {
                    this.UpdateInterval();
                    Vector3 position = this.mTrans.position;
                    Vector3 eulerAngles = this.mTrans.rotation.eulerAngles;
                    if ((this.mWasSleeping || (position != this.mLastPos)) || (eulerAngles != this.mLastRot))
                    {
                        this.mLastPos = position;
                        this.mLastRot = eulerAngles;
                        if (this.isImportant)
                        {
                            object[] objs = new object[] { position, eulerAngles, this.mRb.velocity, this.mRb.angularVelocity };
                            base.tno.Send((byte) 1, Target.OthersSaved, objs);
                        }
                        else
                        {
                            object[] objArray2 = new object[] { position, eulerAngles, this.mRb.velocity, this.mRb.angularVelocity };
                            base.tno.SendQuickly((byte) 1, Target.OthersSaved, objArray2);
                        }
                    }
                    this.mWasSleeping = flag;
                }
            }
        }
    }

    private void OnCollisionEnter()
    {
        if (TNManager.isHosting)
        {
            this.Sync();
        }
    }

    [TNet.RFC(1)]
    private void OnSync(Vector3 pos, Vector3 rot, Vector3 vel, Vector3 ang)
    {
        this.mTrans.position = pos;
        this.mTrans.rotation = Quaternion.Euler(rot);
        this.mRb.velocity = vel;
        this.mRb.angularVelocity = ang;
        this.UpdateInterval();
    }

    public void Sync()
    {
        if (TNManager.isInChannel)
        {
            this.UpdateInterval();
            this.mWasSleeping = false;
            this.mLastPos = this.mTrans.position;
            this.mLastRot = this.mTrans.rotation.eulerAngles;
            object[] objs = new object[] { this.mLastPos, this.mLastRot, this.mRb.velocity, this.mRb.angularVelocity };
            base.tno.Send((byte) 1, Target.OthersSaved, objs);
        }
    }

    private void UpdateInterval()
    {
        this.mNext = UnityEngine.Random.Range((float) 0.85f, (float) 1.15f) * ((this.updatesPerSecond <= 0f) ? 0f : (1f / this.updatesPerSecond));
    }
}

