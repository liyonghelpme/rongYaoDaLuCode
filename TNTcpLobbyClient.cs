﻿using System;
using System.IO;
using System.Net;
using TNet;
using UnityEngine;

public class TNTcpLobbyClient : TNLobbyClient
{
    private long mNextConnect;
    private IPEndPoint mRemoteAddress;
    private TcpProtocol mTcp = new TcpProtocol();

    protected override void OnDisable()
    {
        TNLobbyClient.isActive = false;
        this.mTcp.Disconnect();
        base.OnDisable();
        if (TNLobbyClient.onChange != null)
        {
            TNLobbyClient.onChange();
        }
    }

    private void OnEnable()
    {
        if (this.mRemoteAddress == null)
        {
            this.mRemoteAddress = !string.IsNullOrEmpty(base.remoteAddress) ? Tools.ResolveEndPoint(base.remoteAddress, base.remotePort) : new IPEndPoint(IPAddress.Broadcast, base.remotePort);
            if (this.mRemoteAddress == null)
            {
                this.mTcp.Error(string.Concat(new object[] { "Invalid address: ", base.remoteAddress, ":", base.remotePort }));
            }
        }
    }

    private void Update()
    {
        TNet.Buffer buffer;
        bool flag = false;
        long time = DateTime.UtcNow.Ticks / 0x2710L;
        if (((this.mRemoteAddress != null) && (this.mTcp.stage == TcpProtocol.Stage.NotConnected)) && (this.mNextConnect < time))
        {
            this.mNextConnect = time + 0x1388L;
            this.mTcp.Connect(this.mRemoteAddress);
        }
        while (this.mTcp.ReceivePacket(out buffer))
        {
            if (buffer.size > 0)
            {
                try
                {
                    BinaryReader reader = buffer.BeginReading();
                    Packet packet = (Packet) reader.ReadByte();
                    switch (packet)
                    {
                        case Packet.ResponseID:
                            if (this.mTcp.VerifyResponseID(packet, reader))
                            {
                                TNLobbyClient.isActive = true;
                                this.mTcp.BeginSend(Packet.RequestServerList).Write((ushort) 1);
                                this.mTcp.EndSend();
                            }
                            goto Label_014D;

                        case Packet.Disconnect:
                            TNLobbyClient.knownServers.Clear();
                            TNLobbyClient.isActive = false;
                            flag = true;
                            goto Label_014D;

                        case Packet.ResponseServerList:
                            TNLobbyClient.knownServers.ReadFrom(reader, time);
                            flag = true;
                            goto Label_014D;

                        case Packet.Error:
                            TNLobbyClient.errorString = reader.ReadString();
                            Debug.LogWarning(TNLobbyClient.errorString);
                            flag = true;
                            goto Label_014D;
                    }
                }
                catch (Exception exception)
                {
                    TNLobbyClient.errorString = exception.Message;
                    Debug.LogWarning(exception.Message);
                    this.mTcp.Close(false);
                }
            }
        Label_014D:
            buffer.Recycle();
        }
        if (flag && (TNLobbyClient.onChange != null))
        {
            TNLobbyClient.onChange();
        }
    }
}

