﻿using System;
using System.IO;
using System.Net;
using TNet;
using UnityEngine;

public class TNUdpMultiLobbyClient : TNLobbyClient
{
    private long mNextSend;
    private bool mReEnable;
    private List<IPEndPoint> mRemoteAddressList = new List<IPEndPoint>();
    private TNet.Buffer mRequest;
    private UdpProtocol mUdp = new UdpProtocol();
    public int remoteEndPort = 0x1139;
    public int remoteStartPort = 0x112f;

    private void Awake()
    {
    }

    private void OnApplicationPause(bool paused)
    {
        if (paused)
        {
            if (TNLobbyClient.isActive)
            {
                this.mReEnable = true;
                this.OnDisable();
            }
        }
        else if (this.mReEnable)
        {
            this.mReEnable = false;
            this.OnEnable();
        }
    }

    protected override void OnDisable()
    {
        TNLobbyClient.isActive = false;
        base.OnDisable();
        try
        {
            this.mUdp.Stop();
            if (this.mRequest != null)
            {
                this.mRequest.Recycle();
                this.mRequest = null;
            }
            if (TNLobbyClient.onChange != null)
            {
                TNLobbyClient.onChange();
            }
        }
        catch (Exception)
        {
        }
    }

    private void OnEnable()
    {
        if (this.mRequest == null)
        {
            this.mRequest = TNet.Buffer.Create();
            this.mRequest.BeginPacket(Packet.RequestServerList).Write((ushort) 1);
            this.mRequest.EndPacket();
        }
        if (this.mRemoteAddressList.Count == 0)
        {
            int num = (this.remoteEndPort - this.remoteStartPort) + 1;
            for (int j = 0; j < num; j++)
            {
                int port = this.remoteStartPort + j;
                IPEndPoint item = !string.IsNullOrEmpty(base.remoteAddress) ? Tools.ResolveEndPoint(base.remoteAddress, port) : new IPEndPoint(IPAddress.Broadcast, port);
                if (item == null)
                {
                    this.mUdp.Error(new IPEndPoint(IPAddress.Loopback, this.mUdp.listeningPort), string.Concat(new object[] { "Invalid address: ", base.remoteAddress, ":", port }));
                }
                else
                {
                    this.mRemoteAddressList.Add(item);
                }
            }
        }
        int num4 = 3;
        for (int i = 0; i < num4; i++)
        {
            if (this.mUdp.Start(Tools.randomPort))
            {
                break;
            }
        }
    }

    private void Update()
    {
        TNet.Buffer buffer;
        IPEndPoint point;
        bool flag = false;
        long time = DateTime.UtcNow.Ticks / 0x2710L;
        while ((this.mUdp != null) && this.mUdp.ReceivePacket(out buffer, out point))
        {
            if (buffer.size > 0)
            {
                try
                {
                    BinaryReader reader = buffer.BeginReading();
                    switch (((Packet) reader.ReadByte()))
                    {
                        case Packet.ResponseServerList:
                            TNLobbyClient.isActive = true;
                            this.mNextSend = time + 0xbb8L;
                            TNLobbyClient.knownServers.ReadFrom(reader, time);
                            TNLobbyClient.knownServers.Cleanup(time);
                            flag = true;
                            goto Label_00A2;

                        case Packet.Error:
                            TNLobbyClient.errorString = reader.ReadString();
                            Debug.LogWarning(TNLobbyClient.errorString);
                            flag = true;
                            goto Label_00A2;
                    }
                }
                catch (Exception)
                {
                }
            }
        Label_00A2:
            buffer.Recycle();
        }
        if (TNLobbyClient.knownServers.Cleanup(time))
        {
            flag = true;
        }
        if (flag && (TNLobbyClient.onChange != null))
        {
            TNLobbyClient.onChange();
        }
        else if ((this.mNextSend < time) && (this.mUdp != null))
        {
            this.mNextSend = time + 0xbb8L;
            int count = this.mRemoteAddressList.Count;
            for (int i = 0; i < count; i++)
            {
                this.mUdp.Send(this.mRequest, this.mRemoteAddressList[i]);
            }
        }
    }
}

