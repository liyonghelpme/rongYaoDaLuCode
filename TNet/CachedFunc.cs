﻿namespace TNet
{
    using System;
    using System.Reflection;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct CachedFunc
    {
        public byte id;
        public object obj;
        public MethodInfo func;
        public ParameterInfo[] parameters;
    }
}

