﻿namespace TNet
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using UnityEngine;

    [Serializable]
    public class DataNode
    {
        public List<DataNode> children;
        [NonSerialized]
        private bool mResolved;
        private object mValue;
        public string name;

        public DataNode()
        {
            this.mResolved = true;
            this.children = new List<DataNode>();
        }

        public DataNode(string name)
        {
            this.mResolved = true;
            this.children = new List<DataNode>();
            this.name = name;
        }

        public DataNode(string name, object value)
        {
            this.mResolved = true;
            this.children = new List<DataNode>();
            this.name = name;
            this.value = value;
        }

        public DataNode AddChild()
        {
            DataNode item = new DataNode();
            this.children.Add(item);
            return item;
        }

        public DataNode AddChild(string name)
        {
            DataNode node = this.AddChild();
            node.name = name;
            return node;
        }

        public DataNode AddChild(string name, object value)
        {
            DataNode node = this.AddChild();
            node.name = name;
            node.value = !(value is Enum) ? value : value.ToString();
            return node;
        }

        private static int CalculateTabs(string line)
        {
            if (line != null)
            {
                for (int i = 0; i < line.Length; i++)
                {
                    if (line[i] != '\t')
                    {
                        return i;
                    }
                }
            }
            return 0;
        }

        public void Clear()
        {
            this.value = null;
            this.children.Clear();
        }

        public DataNode Clone()
        {
            DataNode node = new DataNode(this.name) {
                mValue = this.mValue,
                mResolved = this.mResolved
            };
            for (int i = 0; i < this.children.size; i++)
            {
                node.children.Add(this.children[i].Clone());
            }
            return node;
        }

        private static string Escape(string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                val = val.Replace("\n", @"\n");
                val = val.Replace("\t", @"\t");
            }
            return val;
        }

        public T Get<T>()
        {
            if (this.value is T)
            {
                return (T) this.mValue;
            }
            object obj2 = this.Get(typeof(T));
            return ((this.mValue == null) ? default(T) : ((T) obj2));
        }

        public object Get(System.Type type)
        {
            return Serialization.ConvertValue(this.value, type);
        }

        public T Get<T>(T defaultVal)
        {
            if (this.value is T)
            {
                return (T) this.mValue;
            }
            object obj2 = this.Get(typeof(T));
            return ((this.mValue == null) ? defaultVal : ((T) obj2));
        }

        public DataNode GetChild(string name)
        {
            for (int i = 0; i < this.children.size; i++)
            {
                if (this.children[i].name == name)
                {
                    return this.children[i];
                }
            }
            return null;
        }

        public T GetChild<T>(string name)
        {
            DataNode child = this.GetChild(name);
            if (child == null)
            {
                return default(T);
            }
            return child.Get<T>();
        }

        public DataNode GetChild(string name, bool createIfMissing)
        {
            for (int i = 0; i < this.children.size; i++)
            {
                if (this.children[i].name == name)
                {
                    return this.children[i];
                }
            }
            if (createIfMissing)
            {
                return this.AddChild(name);
            }
            return null;
        }

        public T GetChild<T>(string name, T defaultValue)
        {
            DataNode child = this.GetChild(name);
            if (child == null)
            {
                return defaultValue;
            }
            return child.Get<T>();
        }

        private static string GetNextLine(StreamReader reader)
        {
            string str = reader.ReadLine();
            while ((str != null) && str.Trim().StartsWith("//"))
            {
                str = reader.ReadLine();
                if (str == null)
                {
                    return null;
                }
            }
            return str;
        }

        [DebuggerHidden, DebuggerStepThrough]
        private static int HexToDecimal(char ch)
        {
            char ch2 = ch;
            switch (ch2)
            {
                case '0':
                    return 0;

                case '1':
                    return 1;

                case '2':
                    return 2;

                case '3':
                    return 3;

                case '4':
                    return 4;

                case '5':
                    return 5;

                case '6':
                    return 6;

                case '7':
                    return 7;

                case '8':
                    return 8;

                case '9':
                    return 9;

                case 'A':
                    break;

                case 'B':
                    goto Label_00A5;

                case 'C':
                    goto Label_00A8;

                case 'D':
                    goto Label_00AB;

                case 'E':
                    goto Label_00AE;

                case 'F':
                    goto Label_00B1;

                default:
                    switch (ch2)
                    {
                        case 'a':
                            break;

                        case 'b':
                            goto Label_00A5;

                        case 'c':
                            goto Label_00A8;

                        case 'd':
                            goto Label_00AB;

                        case 'e':
                            goto Label_00AE;

                        case 'f':
                            goto Label_00B1;

                        default:
                            return 15;
                    }
                    break;
            }
            return 10;
        Label_00A5:
            return 11;
        Label_00A8:
            return 12;
        Label_00AB:
            return 13;
        Label_00AE:
            return 14;
        Label_00B1:
            return 15;
        }

        private static Color32 ParseColor32(string text, int offset)
        {
            byte r = (byte) ((HexToDecimal(text[offset]) << 4) | HexToDecimal(text[offset + 1]));
            byte g = (byte) ((HexToDecimal(text[offset + 2]) << 4) | HexToDecimal(text[offset + 3]));
            byte b = (byte) ((HexToDecimal(text[offset + 4]) << 4) | HexToDecimal(text[offset + 5]));
            return new Color32(r, g, b, ((offset + 8) > text.Length) ? ((byte) 0xff) : ((byte) ((HexToDecimal(text[offset + 6]) << 4) | HexToDecimal(text[offset + 7]))));
        }

        public static DataNode Read(StreamReader reader)
        {
            string nextLine = GetNextLine(reader);
            int offset = CalculateTabs(nextLine);
            DataNode node = new DataNode();
            node.Read(reader, nextLine, ref offset);
            return node;
        }

        public static DataNode Read(string path)
        {
            return Read(path, false);
        }

        public static DataNode Read(string path, bool binary)
        {
            if (binary)
            {
                BinaryReader reader = new BinaryReader(File.OpenRead(path));
                DataNode node = reader.ReadObject<DataNode>();
                reader.Close();
                return node;
            }
            StreamReader reader2 = new StreamReader(path);
            DataNode node2 = Read(reader2);
            reader2.Close();
            return node2;
        }

        public static DataNode Read(byte[] bytes, bool binary)
        {
            if ((bytes == null) || (bytes.Length == 0))
            {
                return null;
            }
            MemoryStream input = new MemoryStream(bytes);
            if (binary)
            {
                BinaryReader reader = new BinaryReader(input);
                DataNode node = reader.ReadObject<DataNode>();
                reader.Close();
                return node;
            }
            StreamReader reader2 = new StreamReader(input);
            DataNode node2 = Read(reader2);
            reader2.Close();
            return node2;
        }

        private string Read(StreamReader reader, string line, ref int offset)
        {
            if (line != null)
            {
                int startIndex = offset;
                int index = line.IndexOf("=", startIndex);
                if (index == -1)
                {
                    this.name = Unescape(line.Substring(offset)).Trim();
                    this.value = null;
                }
                else
                {
                    this.name = Unescape(line.Substring(offset, index - offset)).Trim();
                    this.mValue = line.Substring(index + 1).Trim();
                    this.mResolved = false;
                }
                line = GetNextLine(reader);
                offset = CalculateTabs(line);
                while (line != null)
                {
                    if (offset != (startIndex + 1))
                    {
                        return line;
                    }
                    line = this.AddChild().Read(reader, line, ref offset);
                }
            }
            return line;
        }

        public void RemoveChild(string name)
        {
            for (int i = 0; i < this.children.size; i++)
            {
                if (this.children[i].name == name)
                {
                    this.children.RemoveAt(i);
                    return;
                }
            }
        }

        private bool ResolveValue(System.Type type)
        {
            this.mResolved = true;
            string mValue = this.mValue as string;
            if (!string.IsNullOrEmpty(mValue))
            {
                if (mValue.Length > 2)
                {
                    bool flag;
                    if ((mValue[0] == '"') && (mValue[mValue.Length - 1] == '"'))
                    {
                        this.mValue = mValue.Substring(1, mValue.Length - 2);
                        return true;
                    }
                    if (((mValue[0] == '0') && (mValue[1] == 'x')) && (mValue.Length > 7))
                    {
                        this.mValue = ParseColor32(mValue, 2);
                        return true;
                    }
                    if ((mValue[0] == '(') && (mValue[mValue.Length - 1] == ')'))
                    {
                        mValue = mValue.Substring(1, mValue.Length - 2);
                        char[] separator = new char[] { ',' };
                        string[] parts = mValue.Split(separator);
                        if (parts.Length == 1)
                        {
                            return this.SetValue(mValue, typeof(float), null);
                        }
                        if (parts.Length == 2)
                        {
                            return this.SetValue(mValue, typeof(Vector2), parts);
                        }
                        if (parts.Length == 3)
                        {
                            return this.SetValue(mValue, typeof(Vector3), parts);
                        }
                        if (parts.Length == 4)
                        {
                            return this.SetValue(mValue, typeof(Color), parts);
                        }
                        this.mValue = mValue;
                        return true;
                    }
                    if (bool.TryParse(mValue, out flag))
                    {
                        this.mValue = flag;
                        return true;
                    }
                }
                int index = mValue.IndexOf('(');
                if (index == -1)
                {
                    if ((mValue[0] == '-') || ((mValue[0] >= '0') && (mValue[0] <= '9')))
                    {
                        if (mValue.IndexOf('.') != -1)
                        {
                            float num2;
                            if (float.TryParse(mValue, out num2))
                            {
                                this.mValue = num2;
                                return true;
                            }
                        }
                        else
                        {
                            int num3;
                            if (int.TryParse(mValue, out num3))
                            {
                                this.mValue = num3;
                                return true;
                            }
                        }
                    }
                }
                else
                {
                    int num4 = (mValue[mValue.Length - 1] != ')') ? mValue.LastIndexOf(')', index) : (mValue.Length - 1);
                    if ((num4 != -1) && (mValue.Length > 2))
                    {
                        type = Serialization.NameToType(mValue.Substring(0, index));
                        mValue = mValue.Substring(index + 1, (num4 - index) - 1);
                    }
                    else if (type == null)
                    {
                        type = typeof(string);
                        this.mValue = mValue;
                        return true;
                    }
                }
                if (type == null)
                {
                    type = Serialization.NameToType(mValue);
                }
            }
            return this.SetValue(mValue, type, null);
        }

        public DataNode SetChild(string name, object value)
        {
            DataNode child = this.GetChild(name);
            if (child == null)
            {
                child = this.AddChild();
            }
            child.name = name;
            child.value = !(value is Enum) ? value : value.ToString();
            return child;
        }

        private bool SetValue(string text, System.Type type, string[] parts)
        {
            if ((type == null) || (type == typeof(void)))
            {
                this.mValue = null;
            }
            else if (type == typeof(string))
            {
                this.mValue = text;
            }
            else if (type == typeof(bool))
            {
                bool result = false;
                if (bool.TryParse(text, out result))
                {
                    this.mValue = result;
                }
            }
            else if (type == typeof(byte))
            {
                byte num;
                if (byte.TryParse(text, out num))
                {
                    this.mValue = num;
                }
            }
            else if (type == typeof(short))
            {
                short num2;
                if (short.TryParse(text, out num2))
                {
                    this.mValue = num2;
                }
            }
            else if (type == typeof(ushort))
            {
                ushort num3;
                if (ushort.TryParse(text, out num3))
                {
                    this.mValue = num3;
                }
            }
            else if (type == typeof(int))
            {
                int num4;
                if (int.TryParse(text, out num4))
                {
                    this.mValue = num4;
                }
            }
            else if (type == typeof(uint))
            {
                uint num5;
                if (uint.TryParse(text, out num5))
                {
                    this.mValue = num5;
                }
            }
            else if (type == typeof(float))
            {
                float num6;
                if (float.TryParse(text, NumberStyles.Float, CultureInfo.InvariantCulture, out num6))
                {
                    this.mValue = num6;
                }
            }
            else if (type == typeof(double))
            {
                double num7;
                if (double.TryParse(text, NumberStyles.Float, CultureInfo.InvariantCulture, out num7))
                {
                    this.mValue = num7;
                }
            }
            else if (type == typeof(Vector2))
            {
                Vector2 vector;
                if (parts == null)
                {
                    char[] separator = new char[] { ',' };
                    parts = text.Split(separator);
                }
                if (((parts.Length == 2) && float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out vector.x)) && float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out vector.y))
                {
                    this.mValue = vector;
                }
            }
            else if (type == typeof(Vector3))
            {
                Vector3 vector2;
                if (parts == null)
                {
                    char[] chArray2 = new char[] { ',' };
                    parts = text.Split(chArray2);
                }
                if (((parts.Length == 3) && float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out vector2.x)) && (float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out vector2.y) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out vector2.z)))
                {
                    this.mValue = vector2;
                }
            }
            else if (type == typeof(Vector4))
            {
                Vector4 vector3;
                if (parts == null)
                {
                    char[] chArray3 = new char[] { ',' };
                    parts = text.Split(chArray3);
                }
                if ((((parts.Length == 4) && float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out vector3.x)) && (float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out vector3.y) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out vector3.z))) && float.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out vector3.w))
                {
                    this.mValue = vector3;
                }
            }
            else if (type == typeof(Quaternion))
            {
                if (parts == null)
                {
                    char[] chArray4 = new char[] { ',' };
                    parts = text.Split(chArray4);
                }
                if (parts.Length == 3)
                {
                    Vector3 vector4;
                    if ((float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out vector4.x) && float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out vector4.y)) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out vector4.z))
                    {
                        this.mValue = Quaternion.Euler(vector4);
                    }
                }
                else
                {
                    Quaternion quaternion;
                    if ((((parts.Length == 4) && float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out quaternion.x)) && (float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out quaternion.y) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out quaternion.z))) && float.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out quaternion.w))
                    {
                        this.mValue = quaternion;
                    }
                }
            }
            else if (type == typeof(Color))
            {
                Color color;
                if (parts == null)
                {
                    char[] chArray5 = new char[] { ',' };
                    parts = text.Split(chArray5);
                }
                if ((((parts.Length == 4) && float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out color.r)) && (float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out color.g) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out color.b))) && float.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out color.a))
                {
                    this.mValue = color;
                }
            }
            else if (type == typeof(Rect))
            {
                Vector4 vector5;
                if (parts == null)
                {
                    char[] chArray6 = new char[] { ',' };
                    parts = text.Split(chArray6);
                }
                if ((((parts.Length == 4) && float.TryParse(parts[0], NumberStyles.Float, CultureInfo.InvariantCulture, out vector5.x)) && (float.TryParse(parts[1], NumberStyles.Float, CultureInfo.InvariantCulture, out vector5.y) && float.TryParse(parts[2], NumberStyles.Float, CultureInfo.InvariantCulture, out vector5.z))) && float.TryParse(parts[3], NumberStyles.Float, CultureInfo.InvariantCulture, out vector5.w))
                {
                    this.mValue = new Rect(vector5.x, vector5.y, vector5.z, vector5.w);
                }
            }
            else
            {
                if (type.Implements(typeof(IDataNodeSerializable)))
                {
                    IDataNodeSerializable serializable = (IDataNodeSerializable) type.Create();
                    serializable.Deserialize(this);
                    this.mValue = serializable;
                    return false;
                }
                if (!type.IsSubclassOf(typeof(Component)))
                {
                    bool flag2 = type.Implements(typeof(IList));
                    bool flag3 = !flag2 && type.Implements(typeof(TList));
                    this.mValue = (!flag3 && !flag2) ? type.Create() : type.Create(this.children.size);
                    if (this.mValue == null)
                    {
                        UnityEngine.Debug.LogError("Unable to create a " + type);
                        return true;
                    }
                    if (flag3)
                    {
                        TList mValue = this.mValue as TList;
                        System.Type genericArgument = type.GetGenericArgument();
                        if (genericArgument != null)
                        {
                            for (int i = 0; i < this.children.size; i++)
                            {
                                DataNode node = this.children[i];
                                if (node.value == null)
                                {
                                    node.mValue = node.name;
                                    node.mResolved = false;
                                    node.ResolveValue(genericArgument);
                                    mValue.Add(node.mValue);
                                }
                                else if (node.name == "Add")
                                {
                                    node.ResolveValue(genericArgument);
                                    mValue.Add(node.mValue);
                                }
                                else
                                {
                                    UnityEngine.Debug.LogWarning("Unexpected node in an array: " + node.name);
                                }
                            }
                            return false;
                        }
                        UnityEngine.Debug.LogError("Unable to determine the element type of " + type);
                    }
                    else if (flag2)
                    {
                        IList list2 = this.mValue as IList;
                        System.Type elementType = type.GetGenericArgument();
                        if (elementType == null)
                        {
                            elementType = type.GetElementType();
                        }
                        bool flag4 = list2.Count == this.children.size;
                        if (elementType != null)
                        {
                            for (int j = 0; j < this.children.size; j++)
                            {
                                DataNode node2 = this.children[j];
                                if (node2.value == null)
                                {
                                    node2.mValue = node2.name;
                                    node2.mResolved = false;
                                    node2.ResolveValue(elementType);
                                    if (flag4)
                                    {
                                        list2[j] = node2.mValue;
                                    }
                                    else
                                    {
                                        list2.Add(node2.mValue);
                                    }
                                }
                                else if (node2.name == "Add")
                                {
                                    node2.ResolveValue(elementType);
                                    if (flag4)
                                    {
                                        list2[j] = node2.mValue;
                                    }
                                    else
                                    {
                                        list2.Add(node2.mValue);
                                    }
                                }
                                else
                                {
                                    UnityEngine.Debug.LogWarning("Unexpected node in an array: " + node2.name);
                                }
                            }
                            return false;
                        }
                        UnityEngine.Debug.LogError("Unable to determine the element type of " + type);
                    }
                    else
                    {
                        if (type.IsClass)
                        {
                            for (int k = 0; k < this.children.size; k++)
                            {
                                DataNode node3 = this.children[k];
                                this.mValue.SetSerializableField(node3.name, node3.value);
                            }
                            return false;
                        }
                        UnityEngine.Debug.LogError("Unhandled type: " + type);
                    }
                }
            }
            return true;
        }

        public override string ToString()
        {
            if (!this.isSerializable)
            {
                return string.Empty;
            }
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            this.Write(writer, 0);
            stream.Seek(0L, SeekOrigin.Begin);
            string str = new StreamReader(stream).ReadToEnd();
            stream.Close();
            return str;
        }

        private static string Unescape(string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                val = val.Replace(@"\n", "\n");
                val = val.Replace(@"\t", "\t");
            }
            return val;
        }

        public void Write(StreamWriter writer)
        {
            this.Write(writer, 0);
        }

        public void Write(string path)
        {
            this.Write(path, false);
        }

        private void Write(StreamWriter writer, int tab)
        {
            if (this.isSerializable)
            {
                Write(writer, tab, this.name, this.value, true);
                for (int i = 0; i < this.children.size; i++)
                {
                    this.children[i].Write(writer, tab + 1);
                }
            }
            if (tab == 0)
            {
                writer.Flush();
            }
        }

        public void Write(string path, bool binary)
        {
            if (binary)
            {
                BinaryWriter bw = new BinaryWriter(File.Create(path));
                bw.WriteObject(this);
                bw.Close();
            }
            else
            {
                StreamWriter writer = new StreamWriter(path, false);
                this.Write(writer, 0);
                writer.Close();
            }
        }

        private static void Write(StreamWriter writer, int tab, string name, object value, bool writeType)
        {
            if (!string.IsNullOrEmpty(name) || (value != null))
            {
                WriteTabs(writer, tab);
                if (name != null)
                {
                    writer.Write(Escape(name));
                    if (value == null)
                    {
                        writer.Write('\n');
                        return;
                    }
                }
                System.Type type = value.GetType();
                if (type == typeof(string))
                {
                    if (name != null)
                    {
                        writer.Write(" = \"");
                    }
                    writer.Write((string) value);
                    if (name != null)
                    {
                        writer.Write('"');
                    }
                    writer.Write('\n');
                }
                else if (type == typeof(bool))
                {
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(!((bool) value) ? "false" : "true");
                    writer.Write('\n');
                }
                else if ((((type == typeof(int)) || (type == typeof(float))) || ((type == typeof(uint)) || (type == typeof(byte)))) || ((type == typeof(short)) || (type == typeof(ushort))))
                {
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(value.ToString());
                    writer.Write('\n');
                }
                else if (type == typeof(Vector2))
                {
                    Vector2 vector = (Vector2) value;
                    writer.Write((name == null) ? "(" : " = (");
                    writer.Write(vector.x.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(vector.y.ToString(CultureInfo.InvariantCulture));
                    writer.Write(")\n");
                }
                else if (type == typeof(Vector3))
                {
                    Vector3 vector2 = (Vector3) value;
                    writer.Write((name == null) ? "(" : " = (");
                    writer.Write(vector2.x.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(vector2.y.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(vector2.z.ToString(CultureInfo.InvariantCulture));
                    writer.Write(")\n");
                }
                else if (type == typeof(Color))
                {
                    Color color = (Color) value;
                    writer.Write((name == null) ? "(" : " = (");
                    writer.Write(color.r.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(color.g.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(color.b.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(color.a.ToString(CultureInfo.InvariantCulture));
                    writer.Write(")\n");
                }
                else if (type == typeof(Color32))
                {
                    Color32 color2 = (Color32) value;
                    writer.Write((name == null) ? "0x" : " = 0x");
                    if (color2.a == 0xff)
                    {
                        writer.Write((((color2.r << 0x10) | (color2.g << 8)) | color2.b).ToString("X6"));
                    }
                    else
                    {
                        writer.Write(((((color2.r << 0x18) | (color2.g << 0x10)) | (color2.b << 8)) | color2.a).ToString("X8"));
                    }
                    writer.Write('\n');
                }
                else if (type == typeof(Vector4))
                {
                    Vector4 vector3 = (Vector4) value;
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(Serialization.TypeToName(type));
                    writer.Write('(');
                    writer.Write(vector3.x.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(vector3.y.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(vector3.z.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(vector3.w.ToString(CultureInfo.InvariantCulture));
                    writer.Write(")\n");
                }
                else if (type == typeof(Quaternion))
                {
                    Quaternion quaternion = (Quaternion) value;
                    Vector3 eulerAngles = quaternion.eulerAngles;
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(Serialization.TypeToName(type));
                    writer.Write('(');
                    writer.Write(eulerAngles.x.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(eulerAngles.y.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(eulerAngles.z.ToString(CultureInfo.InvariantCulture));
                    writer.Write(")\n");
                }
                else if (type == typeof(Rect))
                {
                    Rect rect = (Rect) value;
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(Serialization.TypeToName(type));
                    writer.Write('(');
                    writer.Write(rect.x.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(rect.y.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(rect.width.ToString(CultureInfo.InvariantCulture));
                    writer.Write(", ");
                    writer.Write(rect.height.ToString(CultureInfo.InvariantCulture));
                    writer.Write(")\n");
                }
                else if (value is TList)
                {
                    TList list = value as TList;
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(Serialization.TypeToName(type));
                    writer.Write('\n');
                    if (list.Count > 0)
                    {
                        int index = 0;
                        int count = list.Count;
                        while (index < count)
                        {
                            Write(writer, tab + 1, null, list.Get(index), false);
                            index++;
                        }
                    }
                }
                else if (value is IList)
                {
                    IList list2 = value as IList;
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(Serialization.TypeToName(type));
                    writer.Write('\n');
                    if (list2.Count > 0)
                    {
                        int num5 = 0;
                        int num6 = list2.Count;
                        while (num5 < num6)
                        {
                            Write(writer, tab + 1, null, list2[num5], false);
                            num5++;
                        }
                    }
                }
                else if (value is IDataNodeSerializable)
                {
                    IDataNodeSerializable serializable = value as IDataNodeSerializable;
                    DataNode node = new DataNode();
                    serializable.Serialize(node);
                    if (name != null)
                    {
                        writer.Write(" = ");
                    }
                    writer.Write(Serialization.TypeToName(type));
                    writer.Write('\n');
                    for (int i = 0; i < node.children.size; i++)
                    {
                        node.children[i].Write(writer, tab + 1);
                    }
                }
                else if (value is GameObject)
                {
                    UnityEngine.Debug.LogError("It's not possible to save game objects.");
                    writer.Write('\n');
                }
                else if (value is Component)
                {
                    UnityEngine.Debug.LogError("It's not possible to save components.");
                    writer.Write('\n');
                }
                else
                {
                    if (writeType)
                    {
                        if (name != null)
                        {
                            writer.Write(" = ");
                        }
                        writer.Write(Serialization.TypeToName(type));
                    }
                    writer.Write('\n');
                    List<FieldInfo> serializableFields = type.GetSerializableFields();
                    if (serializableFields.size > 0)
                    {
                        for (int j = 0; j < serializableFields.size; j++)
                        {
                            FieldInfo info = serializableFields[j];
                            object obj2 = info.GetValue(value);
                            if (obj2 != null)
                            {
                                Write(writer, tab + 1, info.Name, obj2, true);
                            }
                        }
                    }
                }
            }
        }

        private static void WriteTabs(StreamWriter writer, int count)
        {
            for (int i = 0; i < count; i++)
            {
                writer.Write('\t');
            }
        }

        public bool isSerializable
        {
            get
            {
                return ((this.value != null) || (this.children.size > 0));
            }
        }

        public System.Type type
        {
            get
            {
                return ((this.value == null) ? typeof(void) : this.mValue.GetType());
            }
        }

        public object value
        {
            get
            {
                if (!this.mResolved && !this.ResolveValue(null))
                {
                    this.children.Clear();
                }
                return this.mValue;
            }
            set
            {
                this.mValue = value;
                this.mResolved = true;
            }
        }
    }
}

