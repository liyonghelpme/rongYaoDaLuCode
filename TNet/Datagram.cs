﻿namespace TNet
{
    using System;
    using System.Net;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Datagram
    {
        public TNet.Buffer buffer;
        public IPEndPoint ip;
    }
}

