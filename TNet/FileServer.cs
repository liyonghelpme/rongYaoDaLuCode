﻿namespace TNet
{
    using System;
    using System.Collections.Generic;

    public class FileServer
    {
        private Dictionary<string, byte[]> mSavedFiles = new Dictionary<string, byte[]>();

        public void DeleteFile(string fileName)
        {
            this.mSavedFiles.Remove(fileName);
            Tools.DeleteFile(fileName);
        }

        protected void Error(string error)
        {
        }

        public byte[] LoadFile(string fileName)
        {
            byte[] buffer;
            if (!this.mSavedFiles.TryGetValue(fileName, out buffer))
            {
                buffer = Tools.ReadFile(fileName);
                this.mSavedFiles[fileName] = buffer;
            }
            return buffer;
        }

        public void SaveFile(string fileName, byte[] data)
        {
            this.mSavedFiles[fileName] = data;
            Tools.WriteFile(fileName, data);
        }
    }
}

