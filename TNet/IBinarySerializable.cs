﻿namespace TNet
{
    using System;
    using System.IO;

    public interface IBinarySerializable
    {
        void Deserialize(BinaryReader reader);
        void Serialize(BinaryWriter writer);
    }
}

