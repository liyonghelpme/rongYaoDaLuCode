﻿namespace TNet
{
    using System;

    public interface IDataNodeSerializable
    {
        void Deserialize(DataNode node);
        void Serialize(DataNode node);
    }
}

