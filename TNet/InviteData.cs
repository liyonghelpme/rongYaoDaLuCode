﻿namespace TNet
{
    using System;

    public class InviteData
    {
        public ServerList.Entry ipEnt;
        public string key;

        public InviteData(ServerList.Entry e, string k)
        {
            this.ipEnt = e;
            this.key = k;
        }
    }
}

