﻿namespace TNet
{
    using System;
    using System.Net;

    public abstract class LobbyServer : FileServer
    {
        protected LobbyServer()
        {
        }

        public abstract void AddServer(string name, int playerCount, IPEndPoint internalAddress, IPEndPoint externalAddress);
        public abstract void RemoveServer(IPEndPoint internalAddress, IPEndPoint externalAddress);
        public abstract bool Start(int listenPort);
        public abstract void Stop();

        public abstract bool isActive { get; }

        public abstract int port { get; }
    }
}

