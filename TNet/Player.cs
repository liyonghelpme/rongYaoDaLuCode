﻿namespace TNet
{
    using System;

    public class Player
    {
        public object data;
        public int id;
        private static DataNode mDummy = new DataNode("Version", 12);
        public static object mLock = 0;
        protected static int mPlayerCounter = 0;
        public string name;
        public const int version = 12;

        public Player()
        {
            this.id = 1;
            this.name = "Guest";
        }

        public Player(string playerName)
        {
            this.id = 1;
            this.name = "Guest";
            this.name = playerName;
        }

        public DataNode dataNode
        {
            get
            {
                DataNode data = this.data as DataNode;
                if (data != null)
                {
                    return data;
                }
                return mDummy;
            }
        }
    }
}

