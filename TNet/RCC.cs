﻿namespace TNet
{
    using System;

    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
    public sealed class RCC : Attribute
    {
        public byte id;

        public RCC(byte rid)
        {
            this.id = rid;
        }
    }
}

