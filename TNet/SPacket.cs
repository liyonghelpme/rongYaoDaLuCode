﻿namespace TNet
{
    using System;

    public enum SPacket
    {
        BeginIdx = 0x40,
        EndIdx = 0x47,
        InviteJoin = 0x43,
        LookupIp = 0x41,
        ResponseIp = 0x42,
        ResponseJoin = 0x44,
        ResponseUserResponseJoin = 70,
        UserResponseJoin = 0x45
    }
}

