﻿namespace TNet
{
    using System;

    public interface TList
    {
        void Add(object obj);
        void Clear();
        object Get(int index);
        void RemoveAt(int index);

        int Count { get; }
    }
}

