﻿namespace TNet
{
    using System;

    public enum Target
    {
        All,
        AllSaved,
        Others,
        OthersSaved,
        Host,
        Broadcast
    }
}

