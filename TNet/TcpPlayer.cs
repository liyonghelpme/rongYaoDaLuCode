﻿namespace TNet
{
    using System;
    using System.IO;
    using System.Net;

    public class TcpPlayer : TcpProtocol
    {
        public Channel channel;
        public IPEndPoint udpEndPoint;
        public bool udpIsUsable;

        public void FinishJoiningChannel()
        {
            TNet.Buffer origBuffer = TNet.Buffer.Create();
            BinaryWriter bw = origBuffer.BeginPacket(Packet.ResponseJoiningChannel);
            bw.Write(this.channel.id);
            bw.Write((short) this.channel.players.size);
            for (int i = 0; i < this.channel.players.size; i++)
            {
                TcpPlayer player = this.channel.players[i];
                bw.Write(player.id);
                bw.Write(!string.IsNullOrEmpty(player.name) ? player.name : "Guest");
                bw.WriteObject(player.data);
            }
            int startOffset = origBuffer.EndPacket();
            if (this.channel.host == null)
            {
                this.channel.host = this;
            }
            origBuffer.BeginPacket(Packet.ResponseSetHost, startOffset);
            bw.Write(this.channel.host.id);
            startOffset = origBuffer.EndTcpPacketStartingAt(startOffset);
            if (!string.IsNullOrEmpty(this.channel.data))
            {
                origBuffer.BeginPacket(Packet.ResponseSetChannelData, startOffset);
                bw.Write(this.channel.data);
                startOffset = origBuffer.EndTcpPacketStartingAt(startOffset);
            }
            origBuffer.BeginPacket(Packet.ResponseLoadLevel, startOffset);
            bw.Write(!string.IsNullOrEmpty(this.channel.level) ? this.channel.level : string.Empty);
            startOffset = origBuffer.EndTcpPacketStartingAt(startOffset);
            for (int j = 0; j < this.channel.created.size; j++)
            {
                Channel.CreatedObject obj2 = this.channel.created.buffer[j];
                bool flag = false;
                for (int m = 0; m < this.channel.players.size; m++)
                {
                    if (this.channel.players[m].id == obj2.playerID)
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    obj2.playerID = this.channel.host.id;
                }
                origBuffer.BeginPacket(Packet.ResponseCreate, startOffset);
                bw.Write(obj2.playerID);
                bw.Write(obj2.objectID);
                bw.Write(obj2.uniqueID);
                bw.Write(obj2.buffer.buffer, obj2.buffer.position, obj2.buffer.size);
                startOffset = origBuffer.EndTcpPacketStartingAt(startOffset);
            }
            if (this.channel.destroyed.size != 0)
            {
                origBuffer.BeginPacket(Packet.ResponseDestroy, startOffset);
                bw.Write((ushort) this.channel.destroyed.size);
                for (int n = 0; n < this.channel.destroyed.size; n++)
                {
                    bw.Write(this.channel.destroyed.buffer[n]);
                }
                startOffset = origBuffer.EndTcpPacketStartingAt(startOffset);
            }
            for (int k = 0; k < this.channel.rfcs.size; k++)
            {
                TNet.Buffer buffer = this.channel.rfcs[k].buffer;
                buffer.BeginReading();
                origBuffer.BeginWriting(startOffset);
                bw.Write(buffer.buffer, 0, buffer.size);
                startOffset = origBuffer.EndWriting();
            }
            origBuffer.BeginPacket(Packet.ResponseJoinChannel, startOffset);
            bw.Write(true);
            startOffset = origBuffer.EndTcpPacketStartingAt(startOffset);
            base.SendTcpPacket(origBuffer);
            origBuffer.Recycle();
        }
    }
}

