﻿namespace TNet
{
    using System;
    using System.IO;
    using System.Net;
    using System.Threading;

    public class UdpLobbyServerLink : LobbyServerLink
    {
        private long mNextSend;
        private IPEndPoint mRemoteAddress;
        private UdpProtocol mUdp;

        public UdpLobbyServerLink(IPEndPoint address) : base(null)
        {
            this.mRemoteAddress = address;
        }

        ~UdpLobbyServerLink()
        {
            if (this.mUdp != null)
            {
                this.mUdp.Stop();
                this.mUdp = null;
            }
        }

        public override void SendUpdate(GameServer server)
        {
            if (!base.mShutdown)
            {
                this.mNextSend = 0L;
                base.mGameServer = server;
                if (base.mThread == null)
                {
                    base.mThread = new Thread(new ThreadStart(this.ThreadFunction));
                    base.mThread.Start();
                }
            }
        }

        public override void Start()
        {
            base.Start();
            if (this.mUdp == null)
            {
                this.mUdp = new UdpProtocol();
                this.mUdp.Start();
            }
        }

        private void ThreadFunction()
        {
            long num;
            base.mInternal = new IPEndPoint(Tools.localAddress, base.mGameServer.tcpPort);
            base.mExternal = new IPEndPoint(Tools.localAddress, base.mGameServer.tcpPort);
        Label_003B:
            num = DateTime.UtcNow.Ticks / 0x2710L;
            if (base.mShutdown)
            {
                TNet.Buffer buffer = TNet.Buffer.Create();
                BinaryWriter writer = buffer.BeginPacket(Packet.RequestRemoveServer);
                writer.Write((ushort) 1);
                Tools.Serialize(writer, base.mInternal);
                Tools.Serialize(writer, base.mExternal);
                buffer.EndPacket();
                this.mUdp.Send(buffer, this.mRemoteAddress);
                buffer.Recycle();
                base.mThread = null;
            }
            else
            {
                if ((this.mNextSend < num) && (base.mGameServer != null))
                {
                    this.mNextSend = num + 0xbb8L;
                    TNet.Buffer buffer2 = TNet.Buffer.Create();
                    BinaryWriter writer2 = buffer2.BeginPacket(Packet.RequestAddServer);
                    writer2.Write((ushort) 1);
                    writer2.Write(base.mGameServer.name);
                    writer2.Write((short) base.mGameServer.playerCount);
                    Tools.Serialize(writer2, base.mInternal);
                    Tools.Serialize(writer2, base.mExternal);
                    buffer2.EndPacket();
                    this.mUdp.Send(buffer2, this.mRemoteAddress);
                    buffer2.Recycle();
                }
                Thread.Sleep(10);
                goto Label_003B;
            }
        }

        public override bool isActive
        {
            get
            {
                return ((this.mUdp != null) && this.mUdp.isActive);
            }
        }
    }
}

