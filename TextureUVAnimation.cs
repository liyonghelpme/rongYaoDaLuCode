﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class TextureUVAnimation : MonoBehaviour
{
    public float changeInterval = 1f;
    public bool isDestroyMyself = true;
    public int LoopTimes = 1;
    private int mLoopCount;
    private Material mMat;
    private MeshRenderer mMR;
    public float mStartTime;
    private Texture2D mTargetTex;
    public Texture2D mTex;
    private int mTilingHeightPix;
    private int mTilingsClick;
    private int mTilingsCount;
    public int mTilingsX = 1;
    public int mTilingsY = 1;
    private int mTilingWithPix;

    private void Awake()
    {
        this.mMR = base.GetComponent<MeshRenderer>();
        this.mMat = this.mMR.sharedMaterial;
        if (this.mMat == null)
        {
            UnityEngine.Debug.LogError("mMat == null  fuckin fatal error !!!");
            base.enabled = false;
        }
        this.SetTilingTex(0);
        this.mMR.enabled = false;
    }

    private void OnDestroy()
    {
        if ((this.mMat != null) && this.mMat.name.Contains("(Instance)"))
        {
            UnityEngine.Object.Destroy(this.mMat);
        }
    }

    private void SetTilingTex(int index)
    {
        float x = 1f / ((float) this.mTilingsX);
        float y = 1f / ((float) this.mTilingsY);
        int num3 = index / this.mTilingsX;
        int num4 = index % this.mTilingsX;
        this.mMat.SetTextureScale("_MainTex", new Vector2(x, y));
        this.mMat.SetTextureOffset("_MainTex", new Vector2(x * num4, ((this.mTilingsY - 1) - num3) * y));
    }

    [DebuggerHidden]
    private IEnumerator Start()
    {
        return new <Start>c__Iterator10 { <>f__this = this };
    }

    [CompilerGenerated]
    private sealed class <Start>c__Iterator10 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal TextureUVAnimation <>f__this;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.$current = new WaitForSeconds(this.<>f__this.mStartTime);
                    this.$PC = 1;
                    goto Label_020C;

                case 1:
                    this.<>f__this.mMR.enabled = true;
                    this.<>f__this.mTilingsCount = (this.<>f__this.mTilingsX * this.<>f__this.mTilingsY) - 1;
                    this.<>f__this.mMat.mainTexture = this.<>f__this.mTex;
                    if (this.<>f__this.LoopTimes >= 0)
                    {
                        while (this.<>f__this.mLoopCount < this.<>f__this.LoopTimes)
                        {
                            this.$current = new WaitForSeconds(this.<>f__this.changeInterval);
                            this.$PC = 3;
                            goto Label_020C;
                        Label_014A:
                            this.<>f__this.SetTilingTex(this.<>f__this.mTilingsClick);
                            this.<>f__this.mTilingsClick++;
                            if (this.<>f__this.mTilingsClick > this.<>f__this.mTilingsCount)
                            {
                                this.<>f__this.mTilingsClick = 0;
                                this.<>f__this.mLoopCount++;
                                if ((this.<>f__this.mLoopCount >= this.<>f__this.LoopTimes) && this.<>f__this.isDestroyMyself)
                                {
                                    UnityEngine.Object.Destroy(this.<>f__this.gameObject);
                                }
                            }
                        }
                        this.$PC = -1;
                        goto Label_020A;
                    }
                    break;

                case 2:
                    this.<>f__this.SetTilingTex(this.<>f__this.mTilingsClick);
                    this.<>f__this.mTilingsClick++;
                    if (this.<>f__this.mTilingsClick > this.<>f__this.mTilingsCount)
                    {
                        this.<>f__this.mTilingsClick = 0;
                    }
                    break;

                case 3:
                    goto Label_014A;

                default:
                    goto Label_020A;
            }
            this.$current = new WaitForSeconds(this.<>f__this.changeInterval);
            this.$PC = 2;
            goto Label_020C;
        Label_020A:
            return false;
        Label_020C:
            return true;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }
}

