﻿using System;
using UnityEngine;

internal class TimedObjectDestructor : MonoBehaviour
{
    public bool detachChildren;
    public float timeOut = 1f;

    private void Awake()
    {
        base.Invoke("DestroyNow", this.timeOut);
    }

    private void DestroyNow()
    {
        if (this.detachChildren)
        {
            base.transform.DetachChildren();
        }
        UnityEngine.Object.DestroyObject(base.gameObject);
    }

    private void Start()
    {
    }

    private void Update()
    {
    }
}

