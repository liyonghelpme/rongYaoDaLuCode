﻿using System;
using UnityEngine;

public class ToolboxDragDropSample : SampleBase
{
    public Rect dragModeButtonRect;
    public Transform[] dragObjects;
    public Collider dragPlane;
    private DragPlaneMode dragPlaneMode;
    public Collider dragSphere;
    private Vector3[] initialPositions;
    public TBInputManager inputMgr;
    public Light pointlight;

    protected override string GetHelpText()
    {
        return "This sample demonstrates the use of the Toolbox's Drag & Drop scripts";
    }

    private void OnGUI()
    {
        if (!base.UI.showHelp)
        {
            string str;
            DragPlaneMode sphere;
            SampleUI.ApplyVirtualScreen();
            DragPlaneMode dragPlaneMode = this.dragPlaneMode;
            if (dragPlaneMode == DragPlaneMode.Plane)
            {
                str = "Drag On Plane";
                sphere = DragPlaneMode.Sphere;
            }
            else if (dragPlaneMode == DragPlaneMode.Sphere)
            {
                str = "Drag On Sphere";
                sphere = DragPlaneMode.XY;
            }
            else
            {
                str = "Drag On XZ";
                sphere = DragPlaneMode.Plane;
            }
            if (GUI.Button(this.dragModeButtonRect, str))
            {
                this.SetDragPlaneMode(sphere);
            }
        }
    }

    private void RestoreInitialPositions()
    {
        for (int i = 0; i < this.initialPositions.Length; i++)
        {
            this.dragObjects[i].position = this.initialPositions[i];
        }
    }

    private void SaveInitialPositions()
    {
        this.initialPositions = new Vector3[this.dragObjects.Length];
        for (int i = 0; i < this.initialPositions.Length; i++)
        {
            this.initialPositions[i] = this.dragObjects[i].position;
        }
    }

    private void SetDragPlaneMode(DragPlaneMode mode)
    {
        switch (mode)
        {
            case DragPlaneMode.XY:
                this.RestoreInitialPositions();
                this.dragSphere.gameObject.active = false;
                this.dragPlane.gameObject.active = false;
                this.inputMgr.dragPlaneType = TBInputManager.DragPlaneType.XY;
                break;

            case DragPlaneMode.Plane:
                this.RestoreInitialPositions();
                this.dragSphere.gameObject.active = false;
                this.dragPlane.gameObject.active = true;
                this.inputMgr.dragPlaneCollider = this.dragPlane;
                this.inputMgr.dragPlaneType = TBInputManager.DragPlaneType.UseCollider;
                break;

            case DragPlaneMode.Sphere:
                this.RestoreInitialPositions();
                this.dragSphere.gameObject.active = true;
                this.dragPlane.gameObject.active = false;
                this.inputMgr.dragPlaneCollider = this.dragSphere;
                this.inputMgr.dragPlaneType = TBInputManager.DragPlaneType.UseCollider;
                break;
        }
        this.dragPlaneMode = mode;
    }

    protected override void Start()
    {
        base.Start();
        this.SaveInitialPositions();
        this.SetDragPlaneMode(DragPlaneMode.XY);
    }

    private void ToggleLight()
    {
        this.pointlight.enabled = !this.pointlight.enabled;
    }

    private enum DragPlaneMode
    {
        XY,
        Plane,
        Sphere
    }
}

