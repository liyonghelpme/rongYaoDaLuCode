﻿using System;
using UnityEngine;

public class ToolboxFingerEventsSample : SampleBase
{
    public Light light1;
    public Light light2;

    protected override string GetHelpText()
    {
        return "This sample demonstrates the use of the toolbox scripts TBFingerDown and TBFingerUp. It also shows how you can use the message target property to turn the light on & off.";
    }

    private void ToggleLight1()
    {
        this.light1.enabled = !this.light1.enabled;
    }

    private void ToggleLight2()
    {
        this.light2.enabled = !this.light2.enabled;
    }
}

