﻿using System;
using UnityEngine;

public class TouchPhaseVisualizer : MonoBehaviour
{
    private TouchPhase phase = TouchPhase.Canceled;
    public Rect rectLabel = new Rect(50f, 50f, 200f, 200f);
    private bool touchDown;

    private void OnGUI()
    {
        if (this.touchDown)
        {
            GUI.Label(this.rectLabel, this.Phase.ToString());
        }
        else
        {
            GUI.Label(this.rectLabel, "N/A");
        }
    }

    private void Update()
    {
        this.touchDown = Input.touchCount > 0;
        if (this.touchDown)
        {
            this.Phase = Input.touches[0].phase;
        }
    }

    public TouchPhase Phase
    {
        get
        {
            return this.phase;
        }
        set
        {
            if (this.phase != value)
            {
                Debug.Log(string.Concat(new object[] { "Phase transition: ", this.phase, " -> ", value }));
                this.phase = value;
            }
        }
    }
}

