﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class TweenUtils
{
    public static TweenPlay AddClickTween(GameObject go, out TweenPosition tweenPosition)
    {
        tweenPosition = go.AddMissingComponent<TweenPosition>();
        tweenPosition.duration = 0.3f;
        tweenPosition.enabled = false;
        TweenAlphas alphas = go.AddMissingComponent<TweenAlphas>();
        alphas.duration = 0.3f;
        alphas.from = 0f;
        alphas.enabled = false;
        TweenScale scale = go.AddMissingComponent<TweenScale>();
        scale.duration = 0.3f;
        scale.from = new Vector3(0.2f, 0.2f, 0.2f);
        scale.enabled = false;
        return go.AddMissingComponent<TweenPlay>();
    }

    public static void AdjustTransformToClick(TweenPosition tweenPosition)
    {
        NGUITools.SetLocalPositionToRayHit(tweenPosition.transform);
        tweenPosition.from = tweenPosition.value;
    }
}

