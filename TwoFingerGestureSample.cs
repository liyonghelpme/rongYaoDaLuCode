﻿using System;
using UnityEngine;

public class TwoFingerGestureSample : SampleBase
{
    private bool dragging;
    public GameObject dragObject;
    public GameObject longPressObject;
    public int requiredTapCount = 2;
    public GameObject swipeObject;
    public GameObject tapObject;

    private bool CheckSpawnParticles(Vector2 fingerPos, GameObject requiredObject)
    {
        GameObject obj2 = SampleBase.PickObject(fingerPos);
        if ((obj2 == null) || (obj2 != requiredObject))
        {
            return false;
        }
        this.SpawnParticles(obj2);
        return true;
    }

    private void FingerGestures_OnTwoFingerDragBegin(Vector2 fingerPos, Vector2 startPos)
    {
        GameObject obj2 = SampleBase.PickObject(startPos);
        if (obj2 == this.dragObject)
        {
            this.dragging = true;
            base.UI.StatusText = "Started dragging with two fingers";
            this.SpawnParticles(obj2);
        }
    }

    private void FingerGestures_OnTwoFingerDragEnd(Vector2 fingerPos)
    {
        if (this.dragging)
        {
            base.UI.StatusText = "Stopped dragging with two fingers";
            this.SpawnParticles(this.dragObject);
            this.dragging = false;
        }
    }

    private void FingerGestures_OnTwoFingerDragMove(Vector2 fingerPos, Vector2 delta)
    {
        if (this.dragging)
        {
            this.dragObject.transform.position = SampleBase.GetWorldPos(fingerPos);
        }
    }

    private void FingerGestures_OnTwoFingerLongPress(Vector2 fingerPos)
    {
        if (this.CheckSpawnParticles(fingerPos, this.longPressObject))
        {
            base.UI.StatusText = "Performed a two-finger long-press";
        }
    }

    private void FingerGestures_OnTwoFingerSwipe(Vector2 startPos, FingerGestures.SwipeDirection direction, float velocity)
    {
        GameObject obj2 = SampleBase.PickObject(startPos);
        if (obj2 == this.swipeObject)
        {
            base.UI.StatusText = "Swiped " + direction + " with two fingers";
            SwipeParticlesEmitter componentInChildren = obj2.GetComponentInChildren<SwipeParticlesEmitter>();
            if (componentInChildren != null)
            {
                componentInChildren.Emit(direction, velocity);
            }
        }
    }

    private void FingerGestures_OnTwoFingerTap(Vector2 fingerPos, int tapCount)
    {
        if ((tapCount == this.requiredTapCount) && this.CheckSpawnParticles(fingerPos, this.tapObject))
        {
            base.UI.StatusText = "Tapped " + this.requiredTapCount + " times with two fingers";
        }
    }

    protected override string GetHelpText()
    {
        object[] objArray1 = new object[] { "This sample demonstrates some of the supported two-finger gestures:\r\n\r\n- Drag: press the red sphere with two fingers and move them to drag the sphere around  \r\n\r\n- LongPress: keep your two fingers pressed on the cyan sphere for at least ", FingerGestures.Defaults.Fingers[0].LongPress.Duration, " seconds\r\n\r\n- Tap: rapidly press & release the purple sphere ", this.requiredTapCount, " times with two fingers\r\n\r\n- Swipe: press the yellow sphere with two fingers and move them in one of the four cardinal directions, then release your fingers. The speed of the motion is taken into account." };
        return string.Concat(objArray1);
    }

    private void OnDisable()
    {
        FingerGestures.OnTwoFingerLongPress -= new FingerGestures.LongPressEventHandler(this.FingerGestures_OnTwoFingerLongPress);
        FingerGestures.OnTwoFingerTap -= new FingerGestures.TapEventHandler(this.FingerGestures_OnTwoFingerTap);
        FingerGestures.OnTwoFingerSwipe -= new FingerGestures.SwipeEventHandler(this.FingerGestures_OnTwoFingerSwipe);
        FingerGestures.OnTwoFingerDragBegin -= new FingerGestures.DragBeginEventHandler(this.FingerGestures_OnTwoFingerDragBegin);
        FingerGestures.OnTwoFingerDragMove -= new FingerGestures.DragMoveEventHandler(this.FingerGestures_OnTwoFingerDragMove);
        FingerGestures.OnTwoFingerDragEnd -= new FingerGestures.DragEndEventHandler(this.FingerGestures_OnTwoFingerDragEnd);
    }

    private void OnEnable()
    {
        Debug.Log("Registering finger gesture events from C# script");
        FingerGestures.OnTwoFingerLongPress += new FingerGestures.LongPressEventHandler(this.FingerGestures_OnTwoFingerLongPress);
        FingerGestures.OnTwoFingerTap += new FingerGestures.TapEventHandler(this.FingerGestures_OnTwoFingerTap);
        FingerGestures.OnTwoFingerSwipe += new FingerGestures.SwipeEventHandler(this.FingerGestures_OnTwoFingerSwipe);
        FingerGestures.OnTwoFingerDragBegin += new FingerGestures.DragBeginEventHandler(this.FingerGestures_OnTwoFingerDragBegin);
        FingerGestures.OnTwoFingerDragMove += new FingerGestures.DragMoveEventHandler(this.FingerGestures_OnTwoFingerDragMove);
        FingerGestures.OnTwoFingerDragEnd += new FingerGestures.DragEndEventHandler(this.FingerGestures_OnTwoFingerDragEnd);
    }

    private void SpawnParticles(GameObject obj)
    {
        ParticleEmitter componentInChildren = obj.GetComponentInChildren<ParticleEmitter>();
        if (componentInChildren != null)
        {
            componentInChildren.Emit();
        }
    }
}

