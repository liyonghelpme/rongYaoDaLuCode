﻿using System;
using UnityEngine;

public class UIEffectRenderQueue : MonoBehaviour
{
    public bool beforeWidget;
    private bool hasUpdate;
    private Material mat;
    public Renderer r;
    public bool updateAtStart;
    public UIWidget widget;

    private void LateUpdate()
    {
        if ((!this.updateAtStart && (null != this.mat)) && (null != this.widget))
        {
            UIDrawCall drawCall = this.widget.drawCall;
            if (null != drawCall)
            {
                if (this.beforeWidget)
                {
                    this.mat.renderQueue = drawCall.renderQueue + 1;
                }
                else
                {
                    this.mat.renderQueue = drawCall.renderQueue - 1;
                }
            }
        }
    }

    private void Start()
    {
        if (null == this.mat)
        {
            if (null == this.r)
            {
                this.r = base.renderer;
            }
            if (null != this.r)
            {
                this.mat = this.r.sharedMaterial;
                if (null == this.mat)
                {
                    this.mat = this.r.material;
                }
            }
        }
        if (this.updateAtStart)
        {
            this.LateUpdate();
        }
    }
}

