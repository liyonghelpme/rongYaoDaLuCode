﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class USpeakOwnerInfo : MonoBehaviour
{
    private USpeakPlayer m_Owner;
    private USpeaker m_speaker;
    public static Dictionary<USpeakOwnerInfo, USpeaker> USpeakerMap = new Dictionary<USpeakOwnerInfo, USpeaker>();
    public static Dictionary<string, USpeakOwnerInfo> USpeakPlayerMap = new Dictionary<string, USpeakOwnerInfo>();

    public void DeInit()
    {
        USpeakPlayerMap.Remove(this.m_Owner.PlayerID);
        USpeakerMap.Remove(this);
        UnityEngine.Object.Destroy(base.gameObject);
    }

    public static USpeakOwnerInfo FindPlayerByID(string PlayerID)
    {
        return USpeakPlayerMap[PlayerID];
    }

    public void Init(USpeakPlayer owner)
    {
        this.m_Owner = owner;
        USpeakPlayerMap.Add(owner.PlayerID, this);
        USpeakerMap.Add(this, base.GetComponent<USpeaker>());
        UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
    }

    public USpeakPlayer Owner
    {
        get
        {
            return this.m_Owner;
        }
    }

    public USpeaker Speaker
    {
        get
        {
            if (this.m_speaker == null)
            {
                this.m_speaker = USpeaker.Get(this);
            }
            return this.m_speaker;
        }
    }
}

