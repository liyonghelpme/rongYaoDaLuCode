﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class USpeakUtilities
{
    public static string USpeakerPrefabPath = "USpeakerPrefab";

    public static void Clear()
    {
        foreach (string str in USpeakOwnerInfo.USpeakPlayerMap.Keys)
        {
            USpeakOwnerInfo.USpeakPlayerMap[str].DeInit();
        }
    }

    public static void ListPlayers(IEnumerable<string> PlayerIDs)
    {
        IEnumerator<string> enumerator = PlayerIDs.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                string current = enumerator.Current;
                PlayerJoined(current);
            }
        }
        finally
        {
            if (enumerator == null)
            {
            }
            enumerator.Dispose();
        }
    }

    public static void PlayerJoined(string PlayerID)
    {
        USpeakOwnerInfo info = ((GameObject) UnityEngine.Object.Instantiate(Resources.Load("USpeakerPrefab"))).AddComponent<USpeakOwnerInfo>();
        USpeakPlayer owner = new USpeakPlayer {
            PlayerID = PlayerID
        };
        info.Init(owner);
    }

    public static void PlayerLeft(string PlayerID)
    {
        USpeakOwnerInfo.FindPlayerByID(PlayerID).DeInit();
    }
}

