﻿using System;
using UnityEngine;

public class UnityNetworkExample : MonoBehaviour
{
    private int listenPort = 0x61a8;
    public GameObject PlayerObject;
    private string remoteIP = "127.0.0.1";
    private int remotePort = 0x61a8;

    private void OnConnectedToServer()
    {
        Network.Instantiate(this.PlayerObject, (Vector3) (UnityEngine.Random.insideUnitSphere * 5f), Quaternion.identity, 0);
    }

    private void OnGUI()
    {
        if (Network.peerType == NetworkPeerType.Disconnected)
        {
            if (GUI.Button(new Rect(10f, 10f, 100f, 30f), "Connect"))
            {
                Network.Connect(this.remoteIP, this.remotePort);
            }
            if (GUI.Button(new Rect(10f, 50f, 100f, 30f), "Start Server"))
            {
                Network.InitializeServer(0x20, this.listenPort, true);
            }
            this.remoteIP = GUI.TextField(new Rect(120f, 10f, 100f, 20f), this.remoteIP);
            this.remotePort = int.Parse(GUI.TextField(new Rect(230f, 10f, 40f, 20f), this.remotePort.ToString()));
        }
        else
        {
            if (GUI.Button(new Rect(10f, 10f, 100f, 50f), "Disconnect"))
            {
                Network.Disconnect(200);
            }
            GUILayout.BeginArea(new Rect(10f, 60f, 200f, 500f));
            int deviceID = 0;
            foreach (string str in Microphone.devices)
            {
                GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(200f) };
                if (GUILayout.Button(str, options))
                {
                    USpeaker.SetInputDevice(deviceID);
                }
                deviceID++;
            }
            GUILayout.EndArea();
        }
    }

    private void OnServerInitialized()
    {
        Network.Instantiate(this.PlayerObject, (Vector3) (UnityEngine.Random.insideUnitSphere * 5f), Quaternion.identity, 0);
    }
}

