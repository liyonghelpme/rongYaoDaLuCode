﻿using MoPhoGames.USpeak.Interface;
using System;
using UnityEngine;

public class UnityNetworkUSpeakSender : MonoBehaviour, ISpeechDataHandler
{
    [RPC]
    private void init(int data)
    {
        USpeaker.Get(this).InitializeSettings(data);
    }

    private void OnDisconnectedFromServer(NetworkDisconnection cause)
    {
        UnityEngine.Object.Destroy(base.gameObject);
    }

    private void Start()
    {
        if (!base.networkView.isMine)
        {
            USpeaker.Get(this).SpeakerMode = SpeakerMode.Remote;
        }
    }

    public void USpeakInitializeSettings(int data)
    {
        object[] args = new object[] { data };
        base.networkView.RPC("init", RPCMode.AllBuffered, args);
    }

    public void USpeakOnSerializeAudio(byte[] data)
    {
        object[] args = new object[] { data };
        base.networkView.RPC("vc", RPCMode.All, args);
    }

    [RPC]
    private void vc(byte[] data)
    {
        USpeaker.Get(this).ReceiveAudio(data);
    }
}

