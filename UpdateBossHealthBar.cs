﻿using com.game.basic;
using com.game.basic.events;
using com.liyong;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityLog;

public class UpdateBossHealthBar : MonoBehaviour
{
    private UISprite background;
    private UILabel countLabel;
    private UISprite foreground;
    private float lastTotalScale = 1f;
    private int maxId;
    private UISlider slider;
    private float totalScale = 1f;
    private UISprite tweenForeGround;

    private void Awake()
    {
        this.slider = base.GetComponent<UISlider>();
        this.tweenForeGround = Util.FindChildRecursive(base.transform, "tweenGround").GetComponent<UISprite>();
        this.foreground = Util.FindChildRecursive(base.transform, "foreground").GetComponent<UISprite>();
        this.background = Util.FindChildRecursive(base.transform, "background").GetComponent<UISprite>();
        this.countLabel = Util.FindChildRecursive(base.transform, "countLabel").GetComponent<UILabel>();
    }

    [DebuggerHidden]
    private IEnumerator FadeOutWhite(UISprite tween, int curId)
    {
        return new <FadeOutWhite>c__Iterator5E { curId = curId, <$>curId = curId, <>f__this = this };
    }

    [DebuggerHidden]
    private IEnumerator FlashTween(float realScale, int curId)
    {
        return new <FlashTween>c__Iterator5F { curId = curId, realScale = realScale, <$>curId = curId, <$>realScale = realScale, <>f__this = this };
    }

    private void InitBossBar(int type, int v1, int v2, object data)
    {
    }

    private void OnDisable()
    {
        GlobalAPI.facade.Remove(0x191, new NoticeListener(this.InitBossBar));
    }

    private void OnEnable()
    {
        GlobalAPI.facade.Add(0x191, new NoticeListener(this.InitBossBar));
    }

    private void OnUpdateHP(CommandHandler.Command cmd)
    {
        int curHp = Convert.ToInt32(cmd.cmd[1]);
        int hp = Convert.ToInt32(cmd.cmd[2]);
        this.SetHp(hp, curHp);
    }

    public void Reset()
    {
        Log.AI(null, "reset boss health bar " + base.gameObject);
        this.slider = base.GetComponent<UISlider>();
        this.tweenForeGround = Util.FindChildRecursive(base.transform, "tweenGround").GetComponent<UISprite>();
        this.foreground = Util.FindChildRecursive(base.transform, "foreground").GetComponent<UISprite>();
        this.background = Util.FindChildRecursive(base.transform, "background").GetComponent<UISprite>();
        this.countLabel = Util.FindChildRecursive(base.transform, "countLabel").GetComponent<UILabel>();
        this.lastTotalScale = 1f;
        this.totalScale = 1f;
        this.SwitchColor();
        this.slider.value = 1f;
        if (base.gameObject.activeInHierarchy)
        {
            base.StartCoroutine(this.ResetSlider());
        }
    }

    [DebuggerHidden]
    private IEnumerator ResetSlider()
    {
        return new <ResetSlider>c__Iterator5D { <>f__this = this };
    }

    public void SetHp(int hp, int curHp)
    {
        this.totalScale = (curHp * 1f) / ((float) hp);
        if (base.gameObject.activeInHierarchy)
        {
            int curId = ++this.maxId;
            base.StartCoroutine(this.FadeOutWhite(this.tweenForeGround, curId));
        }
    }

    private void ShrinkSlider()
    {
        this.lastTotalScale = Mathf.Lerp(this.lastTotalScale, this.totalScale, Time.deltaTime * 3f);
        float num = this.lastTotalScale % 0.2f;
        this.slider.value = num / 0.2f;
    }

    private void Start()
    {
        UnityEngine.Debug.Log("Init Count Label " + this.countLabel);
        base.gameObject.AddMissingComponent<CommandHandler>().AddHandler("updatehp", new CommandHandler.CmdHandler(this.OnUpdateHP));
    }

    private void SwitchColor()
    {
        int num = ((int) (this.lastTotalScale / 0.2f)) + 1;
        num = Mathf.Clamp(num, 1, 5) - 1;
        this.foreground.spriteName = "blood" + num.ToString();
        this.foreground.MakePixelPerfect();
        if (num >= 1)
        {
            this.background.spriteName = "blood" + ((num - 1)).ToString();
            this.background.SetActive(true);
        }
        else
        {
            this.background.spriteName = string.Empty;
            this.background.SetActive(false);
        }
        this.background.MakePixelPerfect();
        this.countLabel.text = num.ToString();
    }

    [CompilerGenerated]
    private sealed class <FadeOutWhite>c__Iterator5E : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal int <$>curId;
        internal UpdateBossHealthBar <>f__this;
        internal float <diff>__0;
        internal float <difScale>__1;
        internal float <realScale>__2;
        internal float <sign>__3;
        internal int curId;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.<diff>__0 = this.<>f__this.lastTotalScale - this.<>f__this.totalScale;
                    this.<difScale>__1 = Mathf.Abs(this.<diff>__0);
                    this.<realScale>__2 = this.<difScale>__1 % 0.2f;
                    this.<sign>__3 = Mathf.Sign(this.<diff>__0);
                    this.$current = this.<>f__this.StartCoroutine(this.<>f__this.FlashTween(1f, this.curId));
                    this.$PC = 1;
                    goto Label_0146;

                case 1:
                case 2:
                    if ((Mathf.Abs((float) (this.<>f__this.lastTotalScale - this.<>f__this.totalScale)) > 0.005f) && (this.<>f__this.maxId == this.curId))
                    {
                        this.<>f__this.ShrinkSlider();
                        this.<>f__this.SwitchColor();
                        this.$current = null;
                        this.$PC = 2;
                        goto Label_0146;
                    }
                    if (this.<>f__this.totalScale <= 0.005f)
                    {
                        this.<>f__this.slider.value = 0f;
                    }
                    this.$PC = -1;
                    break;
            }
            return false;
        Label_0146:
            return true;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class <FlashTween>c__Iterator5F : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal int <$>curId;
        internal float <$>realScale;
        internal UpdateBossHealthBar <>f__this;
        internal UISprite <sp>__1;
        internal float <spCurWidth>__4;
        internal Vector3 <spPos>__2;
        internal float <spRealWidth>__3;
        internal UISprite <tween>__0;
        internal float <tweenStartX>__5;
        internal float <tweenWidth>__6;
        internal int curId;
        internal float realScale;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.<tween>__0 = this.<>f__this.tweenForeGround;
                    if (this.curId == this.<>f__this.maxId)
                    {
                        this.<tween>__0.alpha = 1f;
                        this.<tween>__0.SetActive(true);
                        this.<sp>__1 = this.<>f__this.slider.foregroundWidget as UISprite;
                        this.<spPos>__2 = this.<sp>__1.transform.localPosition;
                        this.<spRealWidth>__3 = this.<sp>__1.width * this.realScale;
                        this.<spCurWidth>__4 = this.<>f__this.slider.value * this.<sp>__1.width;
                        if (this.realScale >= 1f)
                        {
                            this.<spCurWidth>__4 = this.<sp>__1.width;
                        }
                        this.<tweenStartX>__5 = 0f;
                        if (this.<spRealWidth>__3 > 0f)
                        {
                            this.<tweenStartX>__5 = this.<spCurWidth>__4 - this.<spRealWidth>__3;
                        }
                        else
                        {
                            this.<tweenStartX>__5 = this.<spCurWidth>__4;
                        }
                        this.<tweenWidth>__6 = Mathf.Abs(this.<spRealWidth>__3);
                        this.<tween>__0.transform.localPosition = this.<spPos>__2 + new Vector3(this.<tweenStartX>__5, 0f, 0f);
                        this.<tween>__0.transform.localScale = new Vector3(this.<tweenWidth>__6 / ((float) this.<tween>__0.width), 1f, 1f);
                        this.<tween>__0.depth = 0x71;
                    }
                    break;

                case 1:
                    break;

                default:
                    goto Label_023B;
            }
            while ((this.<tween>__0.alpha > 0f) && (this.curId == this.<>f__this.maxId))
            {
                this.<tween>__0.alpha -= Time.deltaTime * 4f;
                this.$current = null;
                this.$PC = 1;
                return true;
            }
            if (this.curId == this.<>f__this.maxId)
            {
                this.<tween>__0.SetActive(false);
            }
            this.$PC = -1;
        Label_023B:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class <ResetSlider>c__Iterator5D : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal UpdateBossHealthBar <>f__this;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.$current = new WaitForSeconds(0.2f);
                    this.$PC = 1;
                    return true;

                case 1:
                    this.<>f__this.slider.value = 1f;
                    this.$PC = -1;
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }
}

