﻿using com.liyong;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityLog;

public class UpdateHpBar : MonoBehaviour
{
    private int maxUpdateId;
    private float realScale = 1f;
    private UISlider slider;
    private uint targetCurHp;
    private uint targetHp;
    private UISprite tweenForeGround;

    private void Awake()
    {
        this.targetHp = 1;
        this.targetCurHp = 1;
        this.slider = base.GetComponent<UISlider>();
        Transform transform = base.transform.FindChild("foregroundRight");
        if (transform != null)
        {
            transform.gameObject.SetActive(false);
        }
        Transform transform2 = base.transform.FindChild("foregroundLeft");
        if (transform2 != null)
        {
            transform2.gameObject.SetActive(false);
        }
        this.tweenForeGround = base.transform.FindChild("tweenGround").GetComponent<UISprite>();
        this.tweenForeGround.transform.localPosition = new Vector3(-49f, 0f, 0f);
        this.tweenForeGround.transform.localScale = new Vector3(1f, 1f, 1f);
        this.tweenForeGround.SetActive(false);
    }

    [DebuggerHidden]
    private IEnumerator FadeOut(UISlider slider)
    {
        return new <FadeOut>c__Iterator61 { slider = slider, <$>slider = slider };
    }

    [DebuggerHidden]
    private IEnumerator FadeOutWhite(UISprite tween, int curId)
    {
        return new <FadeOutWhite>c__Iterator60 { curId = curId, tween = tween, <$>curId = curId, <$>tween = tween, <>f__this = this };
    }

    public void HideSelf()
    {
        Log.AI(base.gameObject, " bloodBar hide self");
    }

    private void OnEnable()
    {
        this.SetHp(this.targetHp, this.targetCurHp, true);
    }

    private void OnUpdateHP(CommandHandler.Command cmd)
    {
        int num = Convert.ToInt32(cmd.cmd[1]);
        int num2 = Convert.ToInt32(cmd.cmd[2]);
        this.SetHp((uint) num2, (uint) num, false);
    }

    [DebuggerHidden]
    private IEnumerator Remove()
    {
        return new <Remove>c__Iterator62 { <>f__this = this };
    }

    public void RemoveSelf()
    {
        base.gameObject.SetActive(true);
        if (base.gameObject.activeInHierarchy)
        {
            base.StartCoroutine(this.Remove());
        }
    }

    public void SetHp(uint hp, uint curHp, bool force = false)
    {
        if ((force || (curHp != this.targetCurHp)) || (hp != this.targetHp))
        {
            int curId = ++this.maxUpdateId;
            Log.AI(base.gameObject, string.Concat(new object[] { " SetHp For Who ", curId, " maxUpdateId ", this.maxUpdateId, " hp Hp ", hp, " curHp ", curHp }));
            this.targetHp = hp;
            this.targetCurHp = curHp;
            this.realScale = Mathf.Min(1f, Mathf.Max((float) ((this.targetCurHp * 1f) / ((float) this.targetHp)), (float) 0f));
            Log.AI(base.gameObject, " realScale is " + this.realScale);
            if (curHp <= 0)
            {
                if (base.gameObject.activeInHierarchy)
                {
                    base.StartCoroutine(this.FadeOut(this.slider));
                }
            }
            else
            {
                this.slider.alpha = 1f;
            }
            if (base.gameObject.activeInHierarchy)
            {
                base.StartCoroutine(this.FadeOutWhite(this.tweenForeGround, curId));
            }
        }
    }

    private void Start()
    {
        base.gameObject.AddMissingComponent<CommandHandler>().AddHandler("updatehp", new CommandHandler.CmdHandler(this.OnUpdateHP));
    }

    [CompilerGenerated]
    private sealed class <FadeOut>c__Iterator61 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal UISlider <$>slider;
        internal UISlider slider;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                case 1:
                    if (this.slider.alpha > 0f)
                    {
                        this.slider.alpha -= 3f * Time.deltaTime;
                        this.$current = null;
                        this.$PC = 1;
                        return true;
                    }
                    this.$PC = -1;
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class <FadeOutWhite>c__Iterator60 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal int <$>curId;
        internal UISprite <$>tween;
        internal UpdateHpBar <>f__this;
        internal int <count>__8;
        internal float <localRealScale>__0;
        internal float <realSliderValue>__3;
        internal int <slideCount>__9;
        internal UISprite <sp>__1;
        internal float <spCurWidth>__5;
        internal Vector3 <spPos>__2;
        internal float <spRealWidth>__4;
        internal float <tweenStartX>__6;
        internal float <tweenWidth>__7;
        internal int curId;
        internal UISprite tween;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { "FadeOutWhite hp bar ", this.curId, " maxId ", this.<>f__this.maxUpdateId }));
                    this.<localRealScale>__0 = this.<>f__this.realScale;
                    Log.AI(this.<>f__this.gameObject, " localRealScale is " + this.<>f__this.realScale);
                    if (this.curId == this.<>f__this.maxUpdateId)
                    {
                        this.tween.alpha = 1f;
                        this.tween.SetActive(true);
                        this.<sp>__1 = this.<>f__this.slider.foregroundWidget as UISprite;
                        this.<spPos>__2 = this.<sp>__1.transform.localPosition;
                        this.<realSliderValue>__3 = this.<>f__this.slider.value;
                        if (this.<>f__this.slider.alpha == 0f)
                        {
                            this.<realSliderValue>__3 = 0f;
                        }
                        this.<spRealWidth>__4 = this.<sp>__1.width * (this.<realSliderValue>__3 - this.<>f__this.realScale);
                        Log.AI(this.<>f__this.gameObject, " blood current value " + this.<>f__this.slider.value);
                        this.<spCurWidth>__5 = this.<>f__this.slider.value * this.<sp>__1.width;
                        this.<tweenStartX>__6 = 0f;
                        if (this.<spRealWidth>__4 > 0f)
                        {
                            this.<tweenStartX>__6 = this.<spCurWidth>__5 - this.<spRealWidth>__4;
                        }
                        else
                        {
                            this.<tweenStartX>__6 = this.<spCurWidth>__5;
                        }
                        this.<tweenWidth>__7 = Mathf.Max(Mathf.Abs(this.<spRealWidth>__4), 3f);
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " tweenWidth ", this.<tweenWidth>__7, " startX ", this.<tweenStartX>__6 }));
                        this.tween.transform.localPosition = this.<spPos>__2 + new Vector3(this.<tweenStartX>__6, 0f, 0f);
                        this.tween.transform.localScale = new Vector3(this.<tweenWidth>__7 / ((float) this.tween.width), 1f, 1f);
                        this.tween.depth = 2;
                    }
                    Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { "Tween Alpha ", this.curId, " max ", this.<>f__this.maxUpdateId, " alpha ", this.tween.alpha }));
                    this.<count>__8 = 0;
                    break;

                case 1:
                    break;

                case 2:
                    goto Label_04D6;

                default:
                    goto Label_05E4;
            }
            while (((this.tween.alpha > 0f) && (this.curId == this.<>f__this.maxUpdateId)) && (this.<count>__8 < 20))
            {
                Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " alpha Change ", this.curId, " maxUpdateId ", this.<>f__this.maxUpdateId }));
                this.tween.alpha -= Time.deltaTime * 2f;
                this.<count>__8++;
                this.$current = null;
                this.$PC = 1;
                goto Label_05E6;
            }
            Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " Set Tween Active False ", this.curId, " max ", this.<>f__this.maxUpdateId }));
            if (this.curId == this.<>f__this.maxUpdateId)
            {
                this.tween.SetActive(false);
            }
            this.<slideCount>__9 = 0;
        Label_04D6:
            while ((Mathf.Abs((float) (this.<>f__this.slider.value - this.<>f__this.realScale)) > 0.005f) && ((this.curId == this.<>f__this.maxUpdateId) || (this.<slideCount>__9 < 3)))
            {
                this.<>f__this.slider.value = Mathf.Lerp(this.<>f__this.slider.value, this.<localRealScale>__0, Time.deltaTime * 5f);
                this.<slideCount>__9++;
                this.$current = null;
                this.$PC = 2;
                goto Label_05E6;
            }
            Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " finish setHP ", this.<>f__this.slider.value, " curId ", this.curId, " maxUpdateId ", this.<>f__this.maxUpdateId, " localRealScale ", this.<localRealScale>__0 }));
            if (this.curId == this.<>f__this.maxUpdateId)
            {
                this.<>f__this.slider.value = this.<localRealScale>__0;
                this.tween.SetActive(false);
            }
            this.$PC = -1;
        Label_05E4:
            return false;
        Label_05E6:
            return true;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class <Remove>c__Iterator62 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object $current;
        internal int $PC;
        internal UpdateHpBar <>f__this;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.$current = new WaitForSeconds(0.5f);
                    this.$PC = 1;
                    return true;

                case 1:
                    UnityEngine.Object.Destroy(this.<>f__this.gameObject);
                    this.$PC = -1;
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return this.$current;
            }
        }
    }
}

