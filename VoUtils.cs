﻿using com.game.basic;
using com.game.manager;
using com.game.utils;
using com.game.vo;
using com.u3d.bases.debug;
using PCustomDataType;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityLog;

public static class VoUtils
{
    private static void AddSuitAttrValue(List<PSuitAttr> suitList, int type, int value)
    {
        foreach (PSuitAttr attr in suitList)
        {
            if (attr.id == type)
            {
                attr.attr = (ushort) (attr.attr + ((ushort) value));
                return;
            }
        }
        PSuitAttr item = new PSuitAttr {
            id = (byte) type,
            attr = (ushort) value
        };
        suitList.Add(item);
    }

    public static T CloneObject<T>(T obj)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        MemoryStream serializationStream = new MemoryStream();
        formatter.Serialize(serializationStream, obj);
        serializationStream.Seek(0L, SeekOrigin.Begin);
        return (T) formatter.Deserialize(serializationStream);
    }

    public static float FromProtocol(this int val)
    {
        return (val * 0.001f);
    }

    public static float FromProtocol(this uint val)
    {
        return (val * 0.001f);
    }

    public static Vector3 FromProtocolInt(this Vector3 vec)
    {
        return StringUtils.DivideWith1000(vec);
    }

    public static string GetColorTips(int type)
    {
        return LanguageManager.GetWord("Equip.ColorTips" + type);
    }

    public static string GetInlayTips(int type)
    {
        return string.Format(LanguageManager.GetWord("Smelt.InlayTips"), string.Format("[FFE400]{0}[-]", LanguageManager.GetWord("Smelt.Type" + type)));
    }

    public static string GetMergeTips(int type)
    {
        return string.Format(LanguageManager.GetWord("Smelt.MergeTips"), string.Format("[FFE400]{0}[-]", LanguageManager.GetWord("Smelt.Type" + type)));
    }

    public static T GetPItemsVo<T>(this PItems gooods) where T: class
    {
        return BaseDataMgr.instance.GetDataById<T>(gooods.templateId);
    }

    public static string GetRoleAttr(int type)
    {
        return LanguageManager.GetWord("Role.Attr" + type);
    }

    public static string GetWordWithLine(string key)
    {
        return LanguageManager.GetWord(key).Replace(@"\n", "\n");
    }

    public static string LanguagerFormat(string language, params object[] value)
    {
        string word = LanguageManager.GetWord(language);
        if (string.IsNullOrEmpty(word))
        {
            com.u3d.bases.debug.Log.info("VoUtils", "没有找到语言包： " + language);
            return null;
        }
        return string.Format(word, value);
    }

    public static void Merge(this ICollection<PDamageBuff> list, ICollection<PDamageBuff> anotherList)
    {
        <Merge>c__AnonStoreyCE yce = new <Merge>c__AnonStoreyCE {
            list = list
        };
        if ((anotherList != null) && (anotherList.Count != 0))
        {
            IEnumerator<PDamageBuff> enumerator = anotherList.Where<PDamageBuff>(new Func<PDamageBuff, bool>(yce.<>m__36)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PDamageBuff current = enumerator.Current;
                    yce.list.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }
    }

    public static string RoleAttrFormat1(int type, string value)
    {
        return string.Format(LanguageManager.GetWord("Language.Format1"), GetRoleAttr(type), value);
    }

    public static string RoleAttrFormat2(int type, string value, string addValue)
    {
        return string.Format(LanguageManager.GetWord("Language.Format2"), GetRoleAttr(type), value, addValue);
    }

    public static string RoleAttrFormat3(int type, string value, string addValue1, string addValue2)
    {
        object[] args = new object[] { GetRoleAttr(type), value, addValue1, addValue2 };
        return string.Format(LanguageManager.GetWord("Language.Format3"), args);
    }

    public static string RoleAttrFormat4(int type, string value, string addValue1, string addValue2)
    {
        object[] args = new object[] { GetRoleAttr(type), value, addValue1, addValue2 };
        return string.Format(LanguageManager.GetWord("Language.Format4"), args);
    }

    public static void SetVoAllAttribute(BaseRoleVo vo, PNewAttr baseAttr, PNewAttr equipAttr)
    {
        UnityLog.Log.AI(null, string.Concat(new object[] { "Initial Vo Attribute ", baseAttr, " equipAttr ", equipAttr }));
        vo.SetEquipAttr(equipAttr);
        vo.Hp = baseAttr.hpFull;
        vo.SetHp(baseAttr.hp);
        vo.AttrPhy = baseAttr.attP;
        vo.AttrMag = baseAttr.attM;
        vo.DefP = baseAttr.defP;
        vo.DefM = baseAttr.defM;
        vo.Hit = baseAttr.hit;
        vo.Dodge = baseAttr.dodge;
        vo.Crit = baseAttr.crit;
        vo.Flex = baseAttr.flex;
        vo.AbDef = baseAttr.abDef;
        vo.AbHurt = baseAttr.abHurt;
        vo.MoveSpeed = baseAttr.moveSpeed;
        vo.AttPPen = baseAttr.attPPen;
        vo.AttMPen = baseAttr.attMPen;
        vo.AttPVam = baseAttr.attPVam;
        vo.AttMVam = baseAttr.attMVam;
        vo.CritAdd = baseAttr.critAdd;
        vo.AtkSpeedAdd = baseAttr.atkSpeedAdd;
        vo.AtkCdDec = baseAttr.atkCdDec;
        vo.AtkNormalAdd = baseAttr.atkNormalAdd;
        vo.DefPPen = baseAttr.defPPen;
        vo.DefMPen = baseAttr.defMPen;
        vo.DmgBackP = baseAttr.dmgBackP;
        vo.DmgBackM = baseAttr.dmgBackM;
        vo.FightGeneral = (uint) baseAttr.id;
    }

    public static void SetVoAllAttributeByInfo(BaseRoleVo vo, AttributeInfo baseAttr)
    {
        vo.Hp = baseAttr.hpFull;
        vo.SetHp(baseAttr.hpFull);
        vo.AttrPhy = baseAttr.attP;
        vo.AttrMag = baseAttr.attM;
        vo.DefP = baseAttr.defP;
        vo.DefM = baseAttr.defM;
        vo.Hit = baseAttr.hit;
        vo.Dodge = baseAttr.dodge;
        vo.Crit = baseAttr.crit;
        vo.Flex = baseAttr.flex;
        vo.AbDef = baseAttr.abDef;
        vo.AbHurt = baseAttr.abHurt;
        vo.MoveSpeed = baseAttr.moveSpeed;
        vo.AttPPen = baseAttr.attPPen;
        vo.AttMPen = baseAttr.attMPen;
        vo.AttPVam = baseAttr.attPVam;
        vo.AttMVam = baseAttr.attMVam;
        vo.CritAdd = baseAttr.critAdd;
        vo.AtkSpeedAdd = baseAttr.atkSpeedAdd;
        vo.AtkCdDec = baseAttr.atkCdDec;
        vo.AtkNormalAdd = baseAttr.atkNormalAdd;
        vo.DefPPen = baseAttr.defPPen;
        vo.DefMPen = baseAttr.defMPen;
        vo.DmgBackP = baseAttr.dmgBackP;
        vo.DmgBackM = baseAttr.dmgBackM;
    }

    public static void SetVoEquipAttribute(BaseRoleVo vo, PNewAttr baseAttr)
    {
    }

    public static int Square(this int val)
    {
        return (val * val);
    }

    public static float Square(this float val)
    {
        return (val * val);
    }

    public static uint Square(this uint val)
    {
        return (val * val);
    }

    public static int ToProtocolInt(this int val)
    {
        return Convert.ToInt32((float) (val * 1000f));
    }

    public static int ToProtocolInt(this float val)
    {
        return Convert.ToInt32((float) (val * 1000f));
    }

    public static int[] ToProtocolInt(this Vector3 vec)
    {
        int num = vec.x.ToProtocolInt();
        int num2 = vec.y.ToProtocolInt();
        int num3 = vec.z.ToProtocolInt();
        return new int[] { num, num2, num3 };
    }

    public static uint ToProtocolUInt(this float val)
    {
        return Convert.ToUInt32((float) (val * 1000f));
    }

    public static uint ToProtocolUInt(this uint val)
    {
        return Convert.ToUInt32((float) (val * 1000f));
    }

    [CompilerGenerated]
    private sealed class <Merge>c__AnonStoreyCE
    {
        internal ICollection<PDamageBuff> list;

        internal bool <>m__36(PDamageBuff pDamageBuff)
        {
            return !this.list.Contains(pDamageBuff);
        }
    }
}

