﻿namespace com.bases.utils
{
    using System;
    using UnityEngine;

    internal class StringUtils
    {
        internal static void addChild(GameObject parent, GameObject child)
        {
            Transform transform = child.transform;
            transform.parent = parent.transform;
            transform.localPosition = new Vector3(0f, 0f, 0f);
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
            child.layer = parent.layer;
        }

        internal static bool isEmpty(string param)
        {
            return ((param == null) || (param.Trim().Length <= 0));
        }

        internal static bool isEquals(string param1, string param2)
        {
            return (((param1 != null) && (param2 != null)) && param1.Equals(param2));
        }

        internal static bool isEqualsExcFree(string param1, string param2)
        {
            return ((!isEmpty(param1) && !isEmpty(param2)) && param1.Equals(param2));
        }
    }
}

