﻿namespace com.game
{
    using com.game.module.core;
    using System;

    public class AppFacde
    {
        public static readonly AppFacde instance = new AppFacde();

        private AppFacde()
        {
        }

        public void Init()
        {
            Singleton<MapControlII>.Instance.ToString();
            Singleton<DungeonControl>.Instance.ToString();
            Singleton<LoginControl>.Instance.ToString();
            Singleton<BagControl>.Instance.ToString();
            Singleton<RoleControl>.Instance.ToString();
            Singleton<ChatIIControl>.Instance.ToString();
            Singleton<MailControl>.Instance.ToString();
            Singleton<SystemSettingsControl>.Instance.ToString();
            Singleton<GoldBoxControl>.Instance.ToString();
            Singleton<VIPControl>.Instance.ToString();
            Singleton<GeneralControl>.Instance.ToString();
            Singleton<CorpsControl>.Instance.ToString();
            Singleton<GuideControl>.Instance.ToString();
            Singleton<BaoxiangControl>.Instance.ToString();
            Singleton<ActivityControl>.Instance.ToString();
            Singleton<FreshmanGuideController>.Instance.ToString();
            Singleton<RankControl>.Instance.ToString();
            Singleton<EquipmentController>.Instance.ToString();
            Singleton<ShopControl>.Instance.ToString();
            Singleton<CommandController>.Instance.ToString();
            Singleton<AchievementController>.Instance.ToString();
            Singleton<DailyTaskController>.Instance.ToString();
            Singleton<ExpDrugController>.Instance.ToString();
        }

        public void InitAfterIntoScene()
        {
            Singleton<TaskControl>.Instance.ToString();
            Singleton<SkillControl>.Instance.ToString();
            Singleton<BuffControl>.Instance.ToString();
            Singleton<WifiPvpControl>.Instance.ToString();
            Singleton<FriendControl>.Instance.ToString();
            Singleton<SkillUpgradControl>.Instance.ToString();
        }
    }
}

