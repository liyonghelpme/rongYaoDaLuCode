﻿namespace com.game
{
    using com.game.autoroad;
    using com.game.module.core;
    using com.net.wifi_pvp;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.map;
    using System;
    using System.Runtime.CompilerServices;

    public class AppMap : GameMap
    {
        public int CurCopyGroupId;
        public static AppMap Instance;
        private MapPointHitCallback mapPointHitCallBack;

        static AppMap()
        {
            if (Instance == null)
            {
            }
            Instance = new AppMap();
        }

        private AppMap()
        {
        }

        public com.u3d.bases.display.controler.MeControler MeControler()
        {
            return (base.me.Controller as com.u3d.bases.display.controler.MeControler);
        }

        public override bool monseClickEnable()
        {
            return (UICamera.hoveredObject != null);
        }

        private void openCopyPoint()
        {
            if (this.mapPointHitCallBack != null)
            {
                this.mapPointHitCallBack();
            }
        }

        private void openWorldMapPoint()
        {
            if (this.mapPointHitCallBack != null)
            {
                this.mapPointHitCallBack();
            }
        }

        public override void stopAutoRoad()
        {
            if (AutoRoad.intance.isRoading)
            {
                AutoRoad.intance.isRoading = false;
                com.u3d.bases.display.controler.MeControler controller = base.me.Controller as com.u3d.bases.display.controler.MeControler;
                if (controller != null)
                {
                    controller.StopWalk();
                }
                AutoRoad.intance.autoWalk();
            }
        }

        public override void tellServer(float x, float y)
        {
        }

        public bool IsInArena
        {
            get
            {
                return (Singleton<MapMode>.Instance.CurrentSceneVo.type == 6);
            }
        }

        public bool IsInMainCity
        {
            get
            {
                return (Singleton<MapMode>.Instance.CurrentSceneVo.type == 1);
            }
        }

        public bool IsInWifiPVP
        {
            get
            {
                return WifiLAN.Instance.IsInPvp;
            }
        }

        public delegate void MapPointHitCallback();
    }
}

