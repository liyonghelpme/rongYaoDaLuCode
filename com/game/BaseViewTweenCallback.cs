﻿namespace com.game
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class BaseViewTweenCallback : MonoBehaviour
    {
        private IView _baseView;

        public void TweenCallback()
        {
            if (this.baseView != null)
            {
                this.baseView.TweenCallback();
            }
        }

        public void TweenCallbackParams(object oncompleteparams)
        {
            if (this.baseView != null)
            {
                this.baseView.TweenCallbackParams(oncompleteparams);
            }
        }

        public IView baseView
        {
            get
            {
                return this._baseView;
            }
            set
            {
                this._baseView = value;
            }
        }
    }
}

