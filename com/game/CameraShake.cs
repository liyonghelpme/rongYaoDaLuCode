﻿namespace com.game
{
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CameraShake : MonoBehaviour
    {
        private ShakeCurveInfo currentCurveInfo;
        private float currentTime;
        private bool isShaking;
        private float startTime;

        private ShakeCurveInfo GetShakeCurveInfo(uint shakeId)
        {
            SysShakeVo dataById = BaseDataMgr.instance.GetDataById<SysShakeVo>(shakeId);
            if ((dataById == null) || string.IsNullOrEmpty(dataById.keys))
            {
                Debug.LogError("震动配置错误");
                return null;
            }
            int[][] doubleDimensionalArrayStringToInt = StringUtils.GetDoubleDimensionalArrayStringToInt(dataById.keys);
            AnimationCurve curve = new AnimationCurve();
            for (int i = 0; i < doubleDimensionalArrayStringToInt.Length; i++)
            {
                curve.AddKey(doubleDimensionalArrayStringToInt[i][0] * 0.001f, doubleDimensionalArrayStringToInt[i][1] * 0.001f);
            }
            return new ShakeCurveInfo(curve, dataById.priority, dataById.type);
        }

        public void play(uint shakeId, uint type = 1)
        {
            ShakeCurveInfo shakeCurveInfo = this.GetShakeCurveInfo(shakeId);
            if ((shakeCurveInfo != null) && (type == shakeCurveInfo.type))
            {
                if (!this.isShaking)
                {
                    this.currentCurveInfo = shakeCurveInfo;
                    this.startTime = Time.realtimeSinceStartup;
                    this.isShaking = true;
                }
                else if (shakeCurveInfo.priority >= this.currentCurveInfo.priority)
                {
                    this.currentCurveInfo = shakeCurveInfo;
                    this.startTime = Time.realtimeSinceStartup;
                }
            }
        }

        public void Stop()
        {
            this.isShaking = false;
            this.startTime = 0f;
        }

        private void Update()
        {
            if (this.isShaking)
            {
                this.currentTime = Time.realtimeSinceStartup;
                float time = this.currentTime - this.startTime;
                if (time >= 0f)
                {
                    Keyframe[] keys = this.currentCurveInfo.shakeCurve.keys;
                    float num2 = keys[keys.Length - 1].time;
                    Vector3 localPosition = base.gameObject.transform.localPosition;
                    base.gameObject.transform.localPosition = new Vector3(localPosition.x, this.currentCurveInfo.shakeCurve.Evaluate(time) + localPosition.y, localPosition.z);
                    if (time > num2)
                    {
                        this.isShaking = false;
                    }
                }
            }
        }

        protected class ShakeCurveInfo
        {
            public uint priority;
            public AnimationCurve shakeCurve;
            public uint type;

            public ShakeCurveInfo(AnimationCurve curve, uint pri, uint ty)
            {
                this.shakeCurve = curve;
                this.priority = pri;
                this.type = ty;
            }
        }
    }
}

