﻿namespace com.game.Interface.Character
{
    using com.game.basic.events;
    using com.game.module.map.Data;
    using System;
    using UnityEngine;

    public interface ICharacter : IEventDispatcher
    {
        void ChangeCharacterModel(BaseSceneObjInfo roleInfo);
        void SetAction(int actionId);

        GameObject CharacterModel { get; set; }

        int CharacterModelId { get; }
    }
}

