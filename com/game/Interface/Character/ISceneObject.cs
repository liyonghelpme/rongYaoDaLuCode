﻿namespace com.game.Interface.Character
{
    using com.game.basic.components;
    using System;
    using UnityEngine;

    public interface ISceneObject
    {
        void DoMouseClick();
        Sprite3D GetFigure();
        void SetMouseAvoid(bool mouseAvoid);
        void SetPosition(Vector3 postition);
        void SetPosition(float x, float y, float z);
    }
}

