﻿namespace com.game.Interface.Tick
{
    using System;
    using System.Runtime.InteropServices;

    public interface ITick
    {
        void Update(int times, float dt = 0.33f);
    }
}

