﻿namespace com.game.Interface.Tick
{
    using System;

    public interface ITickManager
    {
        void addTick(ITick tick);
        float getClientTotalTime();
        float getSyncOffsetTime();
        bool inTick(ITick tick);
        void removeTick(ITick tick);
        void Update();
    }
}

