﻿namespace com.game
{
    using com.game.data;
    using com.game.module.core;
    using com.game.module.map;
    using com.game.vo;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MouseOrbit : MonoBehaviour
    {
        private MonsterDisplay _monsteDisplay;
        private Camera camera;
        public MapCameraScriptData cameraData;
        public Vector3 cameraDir = new Vector3(50f, 0f, 0f);
        private Vector3 focusPosition;
        private bool isFocusPosition;
        private bool isUpdate;
        public float offsetX;
        public float offsetY = 5f;
        public float offsetZ = -4f;
        public GameObject target;

        private void Awake()
        {
            this.cameraData = new MapCameraScriptData();
            this.camera = base.GetComponent<Camera>();
            this.setParms(this.cameraData);
        }

        private void moveComplete()
        {
            this.isUpdate = true;
        }

        [DebuggerHidden]
        private IEnumerator MoveTo(MapCameraScriptData cameraData)
        {
            return new <MoveTo>c__Iterator16 { cameraData = cameraData, <$>cameraData = cameraData, <>f__this = this };
        }

        public void ReSetTarget()
        {
            vp_Timer.CancelAll("ReSetTarget");
            GlobalData.IsPlayingCamera = false;
            Time.timeScale = 1f;
            if (!this.isFocusPosition)
            {
                Singleton<BattleTopView>.Instance.AddBossBloodBar(this._monsteDisplay);
                MonsterVo vo = this._monsteDisplay.GetVo() as MonsterVo;
                vo.IsUnbeatable = false;
                Singleton<BattleMode>.Instance.OpenBattleView();
            }
            this.isFocusPosition = false;
            this.setParms(this.cameraData);
            this.setTarget(AppMap.Instance.me.GoBase, true);
        }

        private void setParms(MapCameraScriptData cameraData)
        {
            this.offsetX = cameraData.offetX;
            this.offsetY = cameraData.offetY;
            this.offsetZ = cameraData.offetZ;
            base.transform.eulerAngles = cameraData.cameraDir;
            this.camera.fieldOfView = cameraData.fieldOfView;
        }

        public void SetPositionInTime(Vector3 vec, MapCameraScriptData cameraData)
        {
            this.isFocusPosition = true;
            this.focusPosition = vec;
            this.setParms(cameraData);
            vp_Timer.CancelAll("ReSetTarget");
            vp_Timer.In(cameraData.time, new vp_Timer.Callback(this.ReSetTarget), 1, 0.1f, null);
        }

        private void setPreParms(MapCameraScriptData cameraData)
        {
            this.offsetX = cameraData.offetX;
            this.offsetY = cameraData.offetY;
            this.offsetZ = cameraData.offetZ;
        }

        public void setTarget(GameObject go, bool isInit = false)
        {
            this.isUpdate = false;
            this.target = go;
            this.isFocusPosition = false;
            if (isInit)
            {
                base.transform.position = this.lastPosition;
                this.isUpdate = true;
            }
            else
            {
                this.updatePoint();
            }
        }

        public void SetTargetInTime(GameObject oj, MapCameraScriptData cameraData, MonsterDisplay display)
        {
            vp_Timer.CancelAll("ReSetTarget");
            GlobalData.IsPlayingCamera = true;
            this._monsteDisplay = display;
            this.setPreParms(cameraData);
            this.target = oj;
            base.StartCoroutine(this.MoveTo(cameraData));
            this.isUpdate = false;
            vp_Timer.In(cameraData.time, new vp_Timer.Callback(this.ReSetTarget), 1, 0.1f, null);
        }

        public void SetTraget(GameObject oj, MapCameraScriptData newCameraData)
        {
            this.setParms(newCameraData);
            this.setTarget(oj, false);
            this.cameraData = newCameraData;
        }

        public void ShakeCamera(Vector3 amount, float totalTime)
        {
            object[] args = new object[] { "amount", amount, "time", totalTime };
            iTween.ShakePosition(base.gameObject, iTween.Hash(args));
        }

        private void Update()
        {
            if (this.isUpdate)
            {
                base.transform.position = Vector3.Lerp(base.transform.position, this.lastPosition, Time.deltaTime * 5f);
            }
        }

        private void updatePoint()
        {
            object[] args = new object[] { "position", this.lastPosition, "time", 0.5f, "oncomplete", "moveComplete", "oncompletetarget", base.gameObject };
            iTween.MoveTo(base.gameObject, iTween.Hash(args));
        }

        private Vector3 lastPosition
        {
            get
            {
                if (this.isFocusPosition)
                {
                    return new Vector3(this.focusPosition.x + this.offsetX, this.focusPosition.y + this.offsetY, this.focusPosition.z + this.offsetZ);
                }
                if (this.target == null)
                {
                    return new Vector3(0f, 0f, 0f);
                }
                return new Vector3(this.target.transform.position.x + this.offsetX, this.target.transform.position.y + this.offsetY, this.target.transform.position.z + this.offsetZ);
            }
        }

        [CompilerGenerated]
        private sealed class <MoveTo>c__Iterator16 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MapCameraScriptData <$>cameraData;
            internal MouseOrbit <>f__this;
            internal float <passTime>__0;
            internal float <startField>__3;
            internal Vector3 <startPos>__1;
            internal Quaternion <startRot>__2;
            internal Quaternion <targetDir>__5;
            internal Vector3 <targetPos>__4;
            internal MapCameraScriptData cameraData;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<passTime>__0 = 0f;
                        this.<startPos>__1 = this.<>f__this.transform.position;
                        this.<startRot>__2 = this.<>f__this.transform.rotation;
                        this.<startField>__3 = this.<>f__this.camera.fieldOfView;
                        this.<targetPos>__4 = this.<>f__this.target.transform.position + new Vector3(this.<>f__this.offsetX, this.<>f__this.offsetY, this.<>f__this.offsetZ);
                        this.<targetDir>__5 = Quaternion.Euler(this.cameraData.cameraDir);
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_01A3;
                }
                if (this.<passTime>__0 < 0.5f)
                {
                    this.<>f__this.transform.position = Vector3.Lerp(this.<startPos>__1, this.<targetPos>__4, this.<passTime>__0 / 0.5f);
                    this.<>f__this.transform.rotation = Quaternion.Slerp(this.<startRot>__2, this.<targetDir>__5, this.<passTime>__0 / 0.5f);
                    this.<>f__this.camera.fieldOfView = Mathf.Lerp(this.<startField>__3, this.cameraData.fieldOfView, this.<passTime>__0 / 0.5f);
                    this.<passTime>__0 += Time.deltaTime;
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.<>f__this.isUpdate = true;
                this.$PC = -1;
            Label_01A3:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

