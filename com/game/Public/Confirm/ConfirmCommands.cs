﻿namespace com.game.Public.Confirm
{
    using System;

    public class ConfirmCommands
    {
        public const string BUY_VIGOUR = "BUY_VIGOUR";
        public const string CONNECT_SERVER_ERROR = "CONNECT_SERVER_ERROR";
        public const string DEAD = "DEAD";
        public const string DeedCost = "DeedCost";
        public const string OK = "OK";
        public const string OK_CANCEL = "OK_CANCEL";
        public const string SELECT_ONE = "SELECT_ONE";
        public const string TIPS = "TIPS";
        public const string WantedTaskRefresh = "WantedTaskRefresh";
    }
}

