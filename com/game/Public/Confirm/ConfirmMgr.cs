﻿namespace com.game.Public.Confirm
{
    using com.game.manager;
    using com.game.module;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ConfirmMgr
    {
        public ConfirmView CurrentConfirmView;
        public static ConfirmMgr Instance;
        private GameObject maskGo;
        private int topViewIndex;
        private IList<ConfirmView> viewList;

        static ConfirmMgr()
        {
            if (Instance == null)
            {
            }
            Instance = new ConfirmMgr();
        }

        private ConfirmMgr()
        {
            if (this.viewList == null)
            {
                this.viewList = new List<ConfirmView>();
                this.topViewIndex = -1;
            }
        }

        public void CancelTimerTipAlert()
        {
            if (this.CurrentConfirmView != null)
            {
                this.CurrentConfirmView.CancelTimerTipAlert();
                this.CurrentConfirmView.ClearTimetickAfterClick();
            }
        }

        private ConfirmView GetNextView(string cmd)
        {
            ConfirmView view;
            if (this.CurrentConfirmView != null)
            {
                this.CurrentConfirmView.ForceQuit();
            }
            for (int i = 0; i <= this.topViewIndex; i++)
            {
                view = this.viewList[i];
                if (view.Cmd == cmd)
                {
                    return view;
                }
            }
            if (((this.topViewIndex < (this.viewList.Count - 1)) && (this.viewList.Count > 0)) && (this.topViewIndex >= -1))
            {
                try
                {
                    this.topViewIndex++;
                    view = this.viewList[this.topViewIndex];
                }
                catch (ArgumentOutOfRangeException exception)
                {
                    Debug.LogError(string.Concat(new object[] { "数组越界! topViewIndex:", this.topViewIndex, " :", exception.Message }));
                    view = this.HandleNextViewUnfound();
                }
                catch (Exception exception2)
                {
                    Debug.LogError("报错:" + exception2.Message);
                    view = this.HandleNextViewUnfound();
                }
            }
            else
            {
                view = this.HandleNextViewUnfound();
            }
            if (!this.maskGo.activeSelf)
            {
                this.maskGo.SetActive(true);
            }
            this.SetOnlyTopViewVisible();
            this.CurrentConfirmView = view;
            return view;
        }

        private ConfirmView HandleNextViewUnfound()
        {
            ConfirmView item = new ConfirmView {
                ViewClosedCallback = new ClosedCallback(this.ViewCloseCallback)
            };
            this.viewList.Add(item);
            this.topViewIndex++;
            return item;
        }

        public void Init()
        {
            AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.LoadViewCallBack), null, false, true);
        }

        private void LoadViewCallBack(GameObject prefab)
        {
            if (null == this.maskGo)
            {
                this.maskGo = NGUITools.AddChild(com.game.module.ViewTree.go, prefab);
                this.maskGo.SetActive(false);
                this.maskGo.GetComponent<UIPanel>().depth = 0x3e8;
            }
        }

        private void SetOnlyTopViewVisible()
        {
            if (this.topViewIndex >= 0)
            {
                for (int i = 0; i < this.topViewIndex; i++)
                {
                    if (this.viewList[i].visible)
                    {
                        this.viewList[i].visible = false;
                    }
                }
                this.viewList[this.topViewIndex].visible = true;
            }
        }

        public void ShowCommonAlert(string tip = "", string cmd = "OK_CANCEL", ClickCallback okFun = null, string txtOk = "", ClickCallback cancelFun = null, string txtCancel = "")
        {
            this.GetNextView(cmd).ShowCommonAlert(tip, cmd, okFun, txtOk, cancelFun, txtCancel);
        }

        public void ShowOkAlert(string tip = "", string cmd = "OK", ClickCallback okFun = null, string txtOk = "")
        {
            this.GetNextView(cmd).ShowOkAlert(tip, cmd, okFun, txtOk);
        }

        public void ShowOkCancelAlert(string tip = "", string cmd = "OK_CANCEL", ClickCallback okFun = null)
        {
            this.GetNextView(cmd).ShowOkCancelAlert(tip, cmd, okFun);
        }

        public void ShowSelectOneAlert(string tip = "", string cmd = "SELECT_ONE", ClickCallback okFun = null, string txtOk = "", ClickCallback cancelFun = null, string txtCancel = "")
        {
            this.GetNextView(cmd).ShowSelectOneAlert(tip, cmd, okFun, txtOk, cancelFun, txtCancel);
        }

        public void ShowTimeOkCancelAlert(string tip = "", int showTimer = 0, string cmd = "OK_CANCEL", ClickCallback okFun = null, ClickCallback cancelFun = null)
        {
            this.GetNextView(cmd).ShowTimerOkCancelAlert(tip, showTimer, cmd, okFun, cancelFun);
        }

        public void ShowTimeTips(string tip = "", int showTimer = 0, ClickCallback timeoutFun = null)
        {
            this.GetNextView("TIPS").ShowTimerTipAlert(tip, showTimer, timeoutFun);
        }

        private void ViewCloseCallback()
        {
            this.topViewIndex--;
            this.SetOnlyTopViewVisible();
            if (this.topViewIndex < 0)
            {
                this.maskGo.SetActive(false);
            }
        }

        private static string url
        {
            get
            {
                return "UI/Mask/MaskView.assetbundle";
            }
        }
    }
}

