﻿namespace com.game.Public.Confirm
{
    using com.game.manager;
    using com.game.module;
    using com.game.module.core;
    using com.game.sound;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ConfirmView
    {
        public static OpenViewGuideDelegate AfterOpenViewGuideDelegate;
        private AlertTypeEnum alertType;
        private Button btnCancel;
        private Button btnClose;
        private Button btnMask;
        public Button btnOk;
        private ClickCallback cancelClickedCallback;
        private UILabel cancelLabel;
        private string cmd;
        private int currentTimer;
        private GameObject gameObject;
        private UILabel labTips;
        private ClickCallback okClickedCallback;
        private UILabel okLabel;
        private Vector3 okPosForOneBtn;
        private Vector3 okPosForTwoBtn;
        private UISprite sprBack;
        private vp_Timer.Handle timeHandle = new vp_Timer.Handle();
        private UILabel timerLabel;
        private string tips;
        private string txtCancel;
        private string txtOk;
        private static GameObject viewPrefab;

        private void AdjustButtons()
        {
            if (this.alertType == AlertTypeEnum.Ok)
            {
                this.timerLabel.gameObject.SetActive(false);
                this.btnCancel.gameObject.SetActive(false);
                this.btnClose.gameObject.SetActive(false);
                this.btnOk.gameObject.transform.position = this.okPosForOneBtn;
            }
            else if (this.alertType == AlertTypeEnum.SelectOne)
            {
                this.timerLabel.gameObject.SetActive(false);
                this.btnCancel.gameObject.SetActive(true);
                this.btnClose.gameObject.SetActive(false);
                this.btnOk.gameObject.transform.position = this.okPosForTwoBtn;
            }
            else if (this.alertType == AlertTypeEnum.SelectTimer)
            {
                this.timerLabel.gameObject.SetActive(true);
                this.btnOk.gameObject.SetActive(true);
                this.btnCancel.gameObject.SetActive(true);
                this.btnClose.gameObject.SetActive(false);
                this.btnOk.gameObject.transform.position = this.okPosForTwoBtn;
                this.timerLabel.text = LanguageManager.GetWord("ConfirmView.Timer", this.currentTimer.ToString());
                vp_Timer.In(0f, new vp_Timer.Callback(this.TimerCallbackInConfirm), null);
            }
            else if (this.alertType == AlertTypeEnum.TipTimer)
            {
                this.timerLabel.gameObject.SetActive(true);
                this.btnCancel.gameObject.SetActive(false);
                this.btnClose.gameObject.SetActive(false);
                this.btnOk.gameObject.SetActive(false);
                this.timerLabel.text = LanguageManager.GetWord("ConfirmView.Timer", this.currentTimer.ToString());
                vp_Timer.In(0f, new vp_Timer.Callback(this.TimerCallbackInConfirm), null);
            }
            else
            {
                this.timerLabel.gameObject.SetActive(false);
                this.btnOk.gameObject.SetActive(true);
                this.btnCancel.gameObject.SetActive(true);
                this.btnClose.gameObject.SetActive(true);
                this.btnOk.gameObject.transform.position = this.okPosForTwoBtn;
            }
        }

        private void CancelOnClick(GameObject go)
        {
            vp_Timer.CancelAll("TimerCallbackInConfirm");
            this.CloseView();
            if (this.cancelClickedCallback != null)
            {
                this.cancelClickedCallback();
            }
            SoundMgr.Instance.PlayUIAudio("3004", 0f);
        }

        public void CancelTimerTipAlert()
        {
            vp_Timer.CancelAll("TimerCallBackInConfirm");
            this.CloseView();
        }

        public void ClearTimetickAfterClick()
        {
            vp_Timer.CancelTimerByHandle(this.timeHandle);
            this.currentTimer = 0;
        }

        private void CloseOnClick(GameObject go)
        {
            this.CloseView();
            SoundMgr.Instance.PlayUIAudio("3004", 0f);
        }

        private void CloseView()
        {
            this.gameObject.SetActive(false);
            if (this.ViewClosedCallback != null)
            {
                this.ViewClosedCallback();
            }
        }

        private void CreateView()
        {
            this.gameObject = NGUITools.AddChild(com.game.module.ViewTree.go, viewPrefab);
            this.gameObject.SetActive(true);
            this.Init();
            this.UpdateInfo();
            this.AdjustButtons();
        }

        public void ForceQuit()
        {
            if (this.currentTimer > 0)
            {
                this.currentTimer = 0;
                vp_Timer.CancelTimerByHandle(this.timeHandle);
                if (this.cancelClickedCallback != null)
                {
                    this.cancelClickedCallback();
                }
                this.CloseView();
            }
        }

        private void Init()
        {
            this.gameObject.GetComponent<UIPanel>().depth = 0x3e9;
            this.btnMask = NGUITools.FindInChild<Button>(this.gameObject, "mask");
            this.btnOk = NGUITools.FindInChild<Button>(this.gameObject, "btn_ok");
            this.btnCancel = NGUITools.FindInChild<Button>(this.gameObject, "btn_cancel");
            this.btnClose = NGUITools.FindInChild<Button>(this.gameObject, "btn_close");
            this.labTips = NGUITools.FindInChild<UILabel>(this.gameObject, "tips");
            this.okLabel = NGUITools.FindInChild<UILabel>(this.gameObject, "btn_ok/label");
            this.cancelLabel = NGUITools.FindInChild<UILabel>(this.gameObject, "btn_cancel/label");
            this.sprBack = NGUITools.FindInChild<UISprite>(this.gameObject, "background");
            this.timerLabel = NGUITools.FindInChild<UILabel>(this.gameObject, "timer");
            this.timerLabel.SetActive(false);
            this.btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.CloseOnClick);
            this.btnOk.onClick = new UIWidgetContainer.VoidDelegate(this.OkOnClick);
            this.btnCancel.onClick = new UIWidgetContainer.VoidDelegate(this.CancelOnClick);
            this.btnMask.onClick = new UIWidgetContainer.VoidDelegate(this.CancelOnClick);
            this.labTips.text = this.tips;
            if (string.Empty == this.txtOk)
            {
                this.txtOk = LanguageManager.GetWord("ConfirmView.Ok");
            }
            this.okLabel.text = this.txtOk;
            if (string.Empty == this.txtCancel)
            {
                this.txtCancel = LanguageManager.GetWord("ConfirmView.Cancel");
            }
            this.cancelLabel.text = this.txtCancel;
            this.okPosForTwoBtn = this.btnOk.gameObject.transform.position;
            this.okPosForOneBtn = this.sprBack.gameObject.transform.position;
            this.okPosForOneBtn.y = this.okPosForTwoBtn.y;
            this.SetToLayerTopUI();
        }

        private void LoadView()
        {
            if (null == viewPrefab)
            {
                AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.LoadViewCallBack), null, false, true);
            }
            else
            {
                this.CreateView();
            }
        }

        private void LoadViewCallBack(GameObject prefab)
        {
            if (null == viewPrefab)
            {
                viewPrefab = prefab;
                this.CreateView();
            }
            if (AfterOpenViewGuideDelegate != null)
            {
                AfterOpenViewGuideDelegate();
                AfterOpenViewGuideDelegate = null;
            }
        }

        private void OkOnClick(GameObject go)
        {
            vp_Timer.CancelAll("TimerCallbackInConfirm");
            this.CloseView();
            if (this.okClickedCallback != null)
            {
                this.okClickedCallback();
            }
            SoundMgr.Instance.PlayUIAudio("3004", 0f);
        }

        private void OpenView()
        {
            if (null == this.gameObject)
            {
                this.LoadView();
            }
            else
            {
                this.gameObject.SetActive(true);
                this.UpdateInfo();
                this.AdjustButtons();
                if (AfterOpenViewGuideDelegate != null)
                {
                    AfterOpenViewGuideDelegate();
                    AfterOpenViewGuideDelegate = null;
                }
            }
        }

        private void SetToLayerTopUI()
        {
            NGUITools.SetLayer(this.gameObject, LayerMask.NameToLayer("TopUI"));
        }

        public void ShowCommonAlert(string tip, string cmd, ClickCallback okFun, string textOk, ClickCallback cancelFun, string textCancel)
        {
            this.alertType = AlertTypeEnum.Common;
            this.ShowWindow(tip, cmd, okFun, textOk, cancelFun, textCancel);
        }

        public void ShowOkAlert(string tip, string cmd, ClickCallback okFun, string textOk)
        {
            if (string.Empty == textOk)
            {
                textOk = LanguageManager.GetWord("ConfirmView.Ok");
            }
            this.alertType = AlertTypeEnum.Ok;
            this.ShowWindow(tip, cmd, okFun, textOk, null, string.Empty);
        }

        public void ShowOkCancelAlert(string tip, string cmd, ClickCallback okFun)
        {
            this.alertType = AlertTypeEnum.Common;
            this.ShowWindow(tip, cmd, okFun, LanguageManager.GetWord("ConfirmView.Ok"), null, LanguageManager.GetWord("ConfirmView.Cancel"));
        }

        public void ShowSelectOneAlert(string tip, string cmd, ClickCallback okFun, string txtOk, ClickCallback cancelFun, string txtCancel)
        {
            this.alertType = AlertTypeEnum.SelectOne;
            if (string.Empty == txtOk)
            {
                txtOk = LanguageManager.GetWord("ConfirmView.Ok");
            }
            if (string.Empty == txtCancel)
            {
                txtCancel = LanguageManager.GetWord("ConfirmView.Cancel");
            }
            this.ShowWindow(tip, cmd, okFun, txtOk, cancelFun, txtCancel);
        }

        public void ShowTimerOkCancelAlert(string tip, int timer, string cmd, ClickCallback okFun, ClickCallback cancelFun)
        {
            vp_Timer.CancelAll("TimerCallbackInConfirm");
            this.alertType = AlertTypeEnum.SelectTimer;
            this.currentTimer = timer;
            this.ShowWindow(tip, cmd, okFun, LanguageManager.GetWord("ConfirmView.Ok"), cancelFun, LanguageManager.GetWord("ConfirmView.Cancel"));
        }

        public void ShowTimerTipAlert(string tip, int timer, ClickCallback timeoutFun)
        {
            vp_Timer.CancelAll("TimerCallbackInConfirm");
            this.alertType = AlertTypeEnum.TipTimer;
            this.currentTimer = timer;
            this.ShowWindow(tip, this.cmd, null, null, timeoutFun, null);
        }

        private void ShowWindow(string tip, string cmd, ClickCallback okFun, string textOk, ClickCallback cancelFun, string textCancel)
        {
            this.tips = tip;
            this.cmd = cmd;
            this.txtOk = textOk;
            this.txtCancel = textCancel;
            this.okClickedCallback = okFun;
            this.cancelClickedCallback = cancelFun;
            this.okClickedCallback = (ClickCallback) Delegate.Combine(this.okClickedCallback, new ClickCallback(this.ClearTimetickAfterClick));
            this.cancelClickedCallback = (ClickCallback) Delegate.Combine(this.cancelClickedCallback, new ClickCallback(this.ClearTimetickAfterClick));
            this.OpenView();
        }

        private void TimerCallbackInConfirm()
        {
            if (this.gameObject.activeSelf)
            {
                this.currentTimer--;
                this.timerLabel.text = LanguageManager.GetWord("ConfirmView.Timer", this.currentTimer.ToString());
                if (this.currentTimer <= 0)
                {
                    if (this.cancelClickedCallback != null)
                    {
                        this.cancelClickedCallback();
                    }
                    this.currentTimer = 0;
                    vp_Timer.CancelTimerByHandle(this.timeHandle);
                    this.CloseView();
                }
                else
                {
                    vp_Timer.In(1f, new vp_Timer.Callback(this.TimerCallbackInConfirm), this.timeHandle);
                }
            }
        }

        private void UpdateInfo()
        {
            this.labTips.text = this.tips;
            this.okLabel.text = this.txtOk;
            this.cancelLabel.text = this.txtCancel;
        }

        public string Cmd
        {
            get
            {
                return this.cmd;
            }
        }

        private static string url
        {
            get
            {
                return "UI/Confirm/ConfirmView.assetbundle";
            }
        }

        public ClosedCallback ViewClosedCallback { get; set; }

        public bool visible
        {
            get
            {
                return ((null != this.gameObject) && this.gameObject.activeSelf);
            }
            set
            {
                if (null != this.gameObject)
                {
                    this.gameObject.SetActive(value);
                }
            }
        }

        public enum AlertTypeEnum
        {
            Common,
            Ok,
            SelectOne,
            SelectTimer,
            TipTimer
        }
    }
}

