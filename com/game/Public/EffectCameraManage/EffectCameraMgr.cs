﻿namespace com.game.Public.EffectCameraManage
{
    using System;
    using UnityEngine;

    public class EffectCameraMgr
    {
        private Camera effectCamera;
        private Camera uiCamera;

        public void CloseCamera()
        {
            this.effectCamera.enabled = false;
        }

        public void Init()
        {
            this.effectCamera = GameObject.FindGameObjectWithTag("EffectCamera").GetComponent<Camera>();
            this.uiCamera = GameObject.FindGameObjectWithTag("UICamera").GetComponent<Camera>();
            if (null != this.uiCamera)
            {
                Vector3 position = this.uiCamera.transform.position;
                position.z = this.effectCamera.transform.position.z;
                this.effectCamera.transform.position = position;
                this.effectCamera.orthographicSize = this.uiCamera.orthographicSize;
            }
            this.CloseCamera();
        }

        public void OpenCamera()
        {
            this.effectCamera.enabled = true;
        }

        public float ToEffectCameraSizeH(float orgH)
        {
            float num = this.effectCamera.orthographicSize * 2f;
            return ((orgH / ((float) Screen.height)) * num);
        }

        public float ToEffectCameraSizeW(float orgW)
        {
            float num = (this.effectCamera.orthographicSize * this.effectCamera.aspect) * 2f;
            return ((orgW / ((float) Screen.width)) * num);
        }

        public float ToMainCameraSizeH(float orgH)
        {
            float num = Camera.main.orthographicSize * 2f;
            return ((orgH / ((float) Screen.height)) * num);
        }

        public float ToMainCameraSizeW(float orgW)
        {
            float num = (Camera.main.orthographicSize * Camera.main.aspect) * 2f;
            return ((orgW / ((float) Screen.width)) * num);
        }

        public bool CameraEnabled
        {
            get
            {
                return this.effectCamera.enabled;
            }
        }

        public Vector3 CameraPosition
        {
            get
            {
                return this.effectCamera.transform.position;
            }
        }
    }
}

