﻿namespace com.game.Public.Hud
{
    using com.game.consts;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class HeadInfoItem : MonoBehaviour
    {
        private int _dir;
        private UILabel _label;
        private float _moveTime;
        private Transform _selfTransform;
        private float _showTime;
        private Vector3 _startPosition;
        private float _totalMoveTime;
        private float _totalShowTime;
        private HeadInfoItemType _type;
        private float _xSpeed;
        private float _ySpeed;
        private static readonly List<HeadInfoItemType> UpInfoItemTypes = new List<HeadInfoItemType> { 3, 4 };

        public void Dispose()
        {
            HeadInfoItemPool.Instance.DeSpawn(base.gameObject.transform);
        }

        public void Init()
        {
            this._label = base.gameObject.GetComponent<UILabel>();
            this._selfTransform = base.transform;
            this._totalMoveTime = 0.2f;
        }

        private bool IsUpHead()
        {
            return UpInfoItemTypes.Contains(this._type);
        }

        private void SetLabelEffectColor()
        {
            switch (this._type)
            {
                case HeadInfoItemType.TypeNormal:
                    this._label.gradientBottom = ColorConst.NormalGradientBottom;
                    this._label.gradientTop = ColorConst.NormalGradientTop;
                    break;

                case HeadInfoItemType.TypeCrit:
                    this._label.gradientBottom = ColorConst.CritGradientBottom;
                    this._label.gradientTop = ColorConst.CritGradientTop;
                    break;

                case HeadInfoItemType.TypeDodge:
                    this._label.gradientBottom = ColorConst.DodgeGradientBottom;
                    this._label.gradientTop = ColorConst.DodgeGradientTop;
                    break;

                case HeadInfoItemType.TypePetTalentHp:
                    this._label.gradientBottom = ColorConst.AddHpGradientBottom;
                    this._label.gradientTop = ColorConst.AddHpGradientTop;
                    break;

                case HeadInfoItemType.TypePetTalentUnbeatable:
                    this._label.gradientBottom = ColorConst.UnbeatableGradientBottom;
                    this._label.gradientTop = ColorConst.UnbeatableGradientTop;
                    break;

                default:
                    this._label.gradientBottom = ColorConst.NormalGradientBottom;
                    this._label.gradientTop = ColorConst.NormalGradientTop;
                    break;
            }
        }

        private bool SetPosition(Vector3 startPosition)
        {
            return true;
        }

        private void SetSpeed()
        {
            this._showTime = 0f;
            this._moveTime = 0f;
            if (this.IsUpHead())
            {
                this._xSpeed = 0f;
                this._ySpeed = 0.5f;
            }
            else
            {
                this._xSpeed = UnityEngine.Random.Range((float) 0.5f, (float) 2.4f);
                this._ySpeed = UnityEngine.Random.Range((float) -1f, (float) 1f);
            }
        }

        public void SetValue(string value, Color color, Vector3 startPosition, int dir, HeadInfoItemType type, float totalShowTime, int labelSize)
        {
            this._type = type;
            this._label.text = value;
            this._label.fontSize = labelSize;
            this._dir = (dir != 4) ? -1 : 1;
            this._totalShowTime = totalShowTime;
            this.SetLabelEffectColor();
            if (this.SetPosition(startPosition))
            {
                this.SetSpeed();
            }
        }

        private void Update()
        {
            if (this._showTime < this._totalShowTime)
            {
                this._showTime += Time.deltaTime;
                this._moveTime += Time.deltaTime;
                if (this._moveTime < this._totalMoveTime)
                {
                    this._startPosition.x += (Time.deltaTime * this._xSpeed) * this._dir;
                    this._startPosition.y += Time.deltaTime * this._ySpeed;
                }
                this._selfTransform.position = this._startPosition;
            }
            else
            {
                this.Dispose();
            }
        }
    }
}

