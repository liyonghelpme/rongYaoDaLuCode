﻿namespace com.game.Public.Hud
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class HeadInfoItemManager
    {
        public static HeadInfoItemManager Instance = new HeadInfoItemManager();

        public void ShowHeadInfo(string value, Color color, Vector3 startPosition, int dir, HeadInfoItemType type, float totalShowTime = 0.8f, int labelSize = 0x1a)
        {
            GameObject gameObject = HeadInfoItemPool.Instance.SpawnHeadInfoItem().gameObject;
            HeadInfoItem component = gameObject.GetComponent<HeadInfoItem>();
            if (component == null)
            {
                component = gameObject.AddComponent<HeadInfoItem>();
                component.Init();
            }
            component.SetValue(value, color, startPosition, dir, type, totalShowTime, labelSize);
        }
    }
}

