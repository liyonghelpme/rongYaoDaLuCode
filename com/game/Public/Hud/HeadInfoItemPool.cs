﻿namespace com.game.Public.Hud
{
    using com.game.Public.PoolManager;
    using System;
    using UnityEngine;

    public class HeadInfoItemPool
    {
        private Transform _headInfoItemPrefab;
        private SpawnPool _pool;
        private readonly Vector3 _swapPos = new Vector3(0f, -2000f, 0f);
        public static HeadInfoItemPool Instance;
        private const string PoolName = "HeadInfoItem";

        static HeadInfoItemPool()
        {
            if (Instance == null)
            {
            }
            Instance = new HeadInfoItemPool();
        }

        public void DeSpawn(Transform headInfoItem)
        {
            headInfoItem.parent = this._pool.transform;
            this._pool.Despawn(headInfoItem);
        }

        public void Init(Transform headInfoItemPrefab)
        {
            this._headInfoItemPrefab = headInfoItemPrefab;
            this._pool = com.game.Public.PoolManager.PoolManager.Pools.Create("HeadInfoItem");
            this._pool.group.parent = com.game.Public.PoolManager.PoolManager.PoolParent;
            this._pool.group.localPosition = new Vector3(0f, 0f, 0f);
            this._pool.group.localRotation = Quaternion.identity;
            PrefabPool prefabPool = new PrefabPool(this._headInfoItemPrefab) {
                preloadAmount = 30
            };
            this._pool.CreatePrefabPool(prefabPool);
        }

        public Transform SpawnHeadInfoItem()
        {
            return this._pool.Spawn(this._headInfoItemPrefab, this._swapPos, Quaternion.identity);
        }
    }
}

