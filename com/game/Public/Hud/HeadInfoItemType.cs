﻿namespace com.game.Public.Hud
{
    using System;

    public enum HeadInfoItemType
    {
        TypeNormal,
        TypeCrit,
        TypeDodge,
        TypePetTalentHp,
        TypePetTalentUnbeatable
    }
}

