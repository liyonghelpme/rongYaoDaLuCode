﻿namespace com.game.Public.InitManager
{
    using com.game.manager;
    using Holoville.HOTween;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class InitManager : MonoBehaviour
    {
        [DebuggerHidden]
        private IEnumerator Init()
        {
            return new <Init>c__Iterator17 { <>f__this = this };
        }

        private void Start()
        {
            base.StartCoroutine(this.Init());
        }

        private void Update()
        {
        }

        [CompilerGenerated]
        private sealed class <Init>c__Iterator17 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal com.game.Public.InitManager.InitManager <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = 0;
                        this.$PC = 1;
                        goto Label_0098;

                    case 1:
                        ModelEffectManager.Init();
                        this.$current = 0;
                        this.$PC = 2;
                        goto Label_0098;

                    case 2:
                        Holoville.HOTween.HOTween.Init(true, true, true);
                        this.$current = 0;
                        this.$PC = 3;
                        goto Label_0098;

                    case 3:
                        this.<>f__this.gameObject.AddComponent<FPS>();
                        this.$PC = -1;
                        break;
                }
                return false;
            Label_0098:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

