﻿namespace com.game.Public.LocalVar
{
    using com.game.vo;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LocalVarManager
    {
        public const string CHAT_REC_SL_CHANNEL = "Chat_RecieveSiLiaoChannel";
        public const string CHAT_REC_ZH_CHANNEL = "Chat_RecieveZongHeChannel";
        public const string CHAT_REC_ZY_CHANNEL = "Chat_RecieveZhenYingChannel";
        public const string COPY_WORLD_ID = "Copy_WorldId";
        public const string LOGIN_SERVER = "Login_Server";

        public static float GetFloat(string key, float defaultValue = 0f)
        {
            key = MeVo.instance.Id + key;
            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        public static int GetInt(string key, int defaultValue = 0)
        {
            key = MeVo.instance.Id + key;
            return PlayerPrefs.GetInt(key, defaultValue);
        }

        public static string GetString(string key, string defaultValue)
        {
            key = MeVo.instance.Id + key;
            return PlayerPrefs.GetString(key, defaultValue);
        }

        public static void SetFloat(string key, float value)
        {
            key = MeVo.instance.Id + key;
            PlayerPrefs.SetFloat(key, value);
        }

        public static void SetInt(string key, int value)
        {
            key = MeVo.instance.Id + key;
            PlayerPrefs.SetInt(key, value);
        }

        public static void SetString(string key, string value)
        {
            key = MeVo.instance.Id + key;
            PlayerPrefs.SetString(key, value);
        }
    }
}

