﻿namespace com.game.Public.Message
{
    using com.game.data;
    using com.game.manager;
    using com.game.sound;
    using System;
    using System.Globalization;

    public class ErrorCodeManager
    {
        public static void ShowError(ushort errorCode)
        {
            SysErrorCodeVo errorCodeVo = BaseDataMgr.instance.GetErrorCodeVo(uint.Parse(errorCode.ToString(CultureInfo.InvariantCulture)));
            string message = string.Empty;
            if (errorCodeVo == null)
            {
                message = string.Format("Errorcode {0} is not defined", errorCode);
            }
            else
            {
                message = errorCodeVo.desc;
            }
            MessageManager.Show(message);
            SoundMgr.Instance.PlayUIAudio("3005", 0f);
        }
    }
}

