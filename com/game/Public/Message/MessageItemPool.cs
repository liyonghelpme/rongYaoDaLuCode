﻿namespace com.game.Public.Message
{
    using com.game.Public.PoolManager;
    using System;
    using UnityEngine;

    public class MessageItemPool
    {
        private Transform _messageItemPrefab;
        private SpawnPool _pool;
        public static MessageItemPool Instance;
        private const string PoolName = "MessageItem";

        static MessageItemPool()
        {
            if (Instance == null)
            {
            }
            Instance = new MessageItemPool();
        }

        public void DeSpawn(Transform messageItem)
        {
            messageItem.parent = this._pool.transform;
            this._pool.Despawn(messageItem);
        }

        public void Init(Transform mailItemPrefab)
        {
            this._messageItemPrefab = mailItemPrefab;
            this._pool = com.game.Public.PoolManager.PoolManager.Pools.Create("MessageItem");
            this._pool.group.parent = com.game.Public.PoolManager.PoolManager.PoolParent;
            this._pool.group.localPosition = new Vector3(0f, 0f, 0f);
            this._pool.group.localRotation = Quaternion.identity;
            PrefabPool prefabPool = new PrefabPool(this._messageItemPrefab) {
                preloadAmount = 3
            };
            this._pool.CreatePrefabPool(prefabPool);
        }

        public Transform SpawnMessageItem()
        {
            return this._pool.Spawn(this._messageItemPrefab, Vector3.zero, Quaternion.identity);
        }
    }
}

