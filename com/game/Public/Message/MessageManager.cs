﻿namespace com.game.Public.Message
{
    using com.game.manager;
    using com.game.module;
    using System;
    using UnityEngine;

    public class MessageManager
    {
        public static MessageView CurItem;
        public UISprite tanhao;
        private static string URL = "UI/Public/MessageView.assetbundle";

        public static void Init()
        {
            AssetManager.Instance.LoadAsset<GameObject>(URL, new LoadAssetFinish<GameObject>(MessageManager.LoadMessageViewBack), null, false, true);
        }

        private static void LoadMessageViewBack(GameObject gameObject)
        {
            MessageItemPool.Instance.Init(gameObject.transform);
        }

        public static void SetWarmFalse()
        {
            CurItem._warnIconSprite.gameObject.SetActive(false);
            CurItem._tipIconSprite.gameObject.SetActive(true);
        }

        public static void Show(string message)
        {
            GameObject gameObject = MessageItemPool.Instance.SpawnMessageItem().gameObject;
            GameObject go = com.game.module.Viewport.go;
            Transform transform = gameObject.transform;
            transform.parent = go.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
            MessageView component = gameObject.GetComponent<MessageView>();
            if (component == null)
            {
                component = gameObject.AddComponent<MessageView>();
                component.Init();
            }
            CurItem = component;
            component.Show(message);
        }

        public static void ShowLang(string wordKey)
        {
            Show(LanguageManager.GetWord(wordKey));
        }

        public static void ShowLang(string wordKey, string param)
        {
            Show(LanguageManager.GetWord(wordKey, param));
        }

        public static void ShowLang(string wordKey, string[] paramList)
        {
            Show(LanguageManager.GetWord(wordKey, paramList));
        }
    }
}

