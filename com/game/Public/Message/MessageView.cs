﻿namespace com.game.Public.Message
{
    using System;
    using System.Collections;
    using UnityEngine;

    public class MessageView : MonoBehaviour
    {
        private UISprite _backGroundSprite;
        private UILabel _content;
        private UIPanel _panel;
        public UISprite _tipIconSprite;
        public UISprite _warnIconSprite;
        public static int Index = 0x3e8;

        public void Init()
        {
            this._content = NGUITools.FindInChild<UILabel>(base.gameObject, "ContentLabel");
            this._backGroundSprite = NGUITools.FindInChild<UISprite>(base.gameObject, "Background");
            this._warnIconSprite = NGUITools.FindInChild<UISprite>(base.gameObject, "WarnIcon");
            this._tipIconSprite = NGUITools.FindInChild<UISprite>(base.gameObject, "TipIcon");
            this._panel = base.gameObject.GetComponent<UIPanel>();
            this.SetToLayerTopUi();
        }

        private void OnMessageShowComplete()
        {
            MessageItemPool.Instance.DeSpawn(base.transform);
        }

        private void SetDepth()
        {
            Index++;
            this._panel.depth = Index;
        }

        private void SetToLayerTopUi()
        {
            NGUITools.SetLayer(base.gameObject, LayerMask.NameToLayer("TopUI"));
        }

        public void Show(string message)
        {
            this.SetToLayerTopUi();
            this.SetDepth();
            this._warnIconSprite.gameObject.SetActive(true);
            this._tipIconSprite.gameObject.SetActive(false);
            this._content.text = message;
            Hashtable hashtable2 = new Hashtable();
            hashtable2.Add("y", 120);
            hashtable2.Add("time", 2.0);
            hashtable2.Add("oncomplete", "OnMessageShowComplete");
            hashtable2.Add("islocal", true);
            hashtable2.Add("ignoretimescale", true);
            Hashtable args = hashtable2;
            iTween.MoveTo(base.gameObject, args);
        }
    }
}

