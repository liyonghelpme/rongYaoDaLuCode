﻿namespace com.game.Public.PoolManager
{
    using System;
    using UnityEngine;

    public static class PoolManager
    {
        private static GameObject _poolParent;
        public static readonly SpawnPoolsDict Pools = new SpawnPoolsDict();

        public static Transform PoolParent
        {
            get
            {
                if (_poolParent == null)
                {
                    _poolParent = new GameObject("PoolParent");
                    UnityEngine.Object.DontDestroyOnLoad(_poolParent);
                }
                return _poolParent.transform;
            }
        }
    }
}

