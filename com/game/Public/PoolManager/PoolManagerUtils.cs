﻿namespace com.game.Public.PoolManager
{
    using System;
    using UnityEngine;

    public static class PoolManagerUtils
    {
        internal static bool activeInHierarchy(GameObject obj)
        {
            return obj.activeInHierarchy;
        }

        internal static void SetActive(GameObject obj, bool state)
        {
            obj.SetActive(state);
        }
    }
}

