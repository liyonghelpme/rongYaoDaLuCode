﻿namespace com.game.Public.PoolManager
{
    using System;
    using UnityEngine;

    [AddComponentMenu("Path-o-logical/PoolManager/Pre-Runtime Pool Item")]
    public class PreRuntimePoolItem : MonoBehaviour
    {
        public bool despawnOnStart = true;
        public bool doNotReparent;
        public string poolName = string.Empty;
        public string prefabName = string.Empty;

        private void Start()
        {
            SpawnPool pool;
            if (!com.game.Public.PoolManager.PoolManager.Pools.TryGetValue(this.poolName, out pool))
            {
                string format = "PreRuntimePoolItem Error ('{0}'): No pool with the name '{1}' exists! Create one using the PoolManager Inspector interface or PoolManager.CreatePool().See the online docs for more information at http://docs.poolmanager.path-o-logical.com";
                Debug.LogError(string.Format(format, base.name, this.poolName));
            }
            else
            {
                pool.Add(base.transform, this.prefabName, this.despawnOnStart, !this.doNotReparent);
            }
        }
    }
}

