﻿namespace com.game.Public.PoolManager
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class PrefabsDict : IEnumerable, IDictionary<string, Transform>, ICollection<KeyValuePair<string, Transform>>, IEnumerable<KeyValuePair<string, Transform>>
    {
        private Dictionary<string, Transform> _prefabs = new Dictionary<string, Transform>();

        internal void _Add(string prefabName, Transform prefab)
        {
            this._prefabs.Add(prefabName, prefab);
        }

        internal void _Clear()
        {
            this._prefabs.Clear();
        }

        internal bool _Remove(string prefabName)
        {
            return this._prefabs.Remove(prefabName);
        }

        public void Add(KeyValuePair<string, Transform> item)
        {
            throw new NotImplementedException("Read-only");
        }

        public void Add(string key, Transform value)
        {
            throw new NotImplementedException("Read-Only");
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<string, Transform> item)
        {
            string message = "Use Contains(string prefabName) instead.";
            throw new NotImplementedException(message);
        }

        public bool ContainsKey(string prefabName)
        {
            return this._prefabs.ContainsKey(prefabName);
        }

        private void CopyTo(KeyValuePair<string, Transform>[] array, int arrayIndex)
        {
            string message = "Cannot be copied";
            throw new NotImplementedException(message);
        }

        public IEnumerator<KeyValuePair<string, Transform>> GetEnumerator()
        {
            return this._prefabs.GetEnumerator();
        }

        public bool Remove(KeyValuePair<string, Transform> item)
        {
            throw new NotImplementedException("Read-only");
        }

        public bool Remove(string prefabName)
        {
            throw new NotImplementedException("Read-Only");
        }

        void ICollection<KeyValuePair<string, Transform>>.CopyTo(KeyValuePair<string, Transform>[] array, int arrayIndex)
        {
            string message = "Cannot be copied";
            throw new NotImplementedException(message);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._prefabs.GetEnumerator();
        }

        public override string ToString()
        {
            string[] array = new string[this._prefabs.Count];
            this._prefabs.Keys.CopyTo(array, 0);
            return string.Format("[{0}]", string.Join(", ", array));
        }

        public bool TryGetValue(string prefabName, out Transform prefab)
        {
            return this._prefabs.TryGetValue(prefabName, out prefab);
        }

        public int Count
        {
            get
            {
                return this._prefabs.Count;
            }
        }

        private bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public Transform this[string key]
        {
            get
            {
                Transform transform;
                try
                {
                    transform = this._prefabs[key];
                }
                catch (KeyNotFoundException)
                {
                    throw new KeyNotFoundException(string.Format("A Prefab with the name '{0}' not found. \nPrefabs={1}", key, this.ToString()));
                }
                return transform;
            }
            set
            {
                throw new NotImplementedException("Read-only.");
            }
        }

        public ICollection<string> Keys
        {
            get
            {
                return this._prefabs.Keys;
            }
        }

        bool ICollection<KeyValuePair<string, Transform>>.IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public ICollection<Transform> Values
        {
            get
            {
                return this._prefabs.Values;
            }
        }
    }
}

