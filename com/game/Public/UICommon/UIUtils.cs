﻿namespace com.game.Public.UICommon
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class UIUtils
    {
        public static string ChangeAttrTypeToString(byte type)
        {
            switch (type)
            {
                case 1:
                    return "力量";

                case 2:
                    return "敏捷";

                case 3:
                    return "体力";

                case 4:
                    return "智力";

                case 5:
                    return "生命";

                case 6:
                    return "魔法";

                case 7:
                    return "最小物攻";

                case 8:
                    return "最大物攻";

                case 9:
                    return "最小魔攻";

                case 10:
                    return "最大魔攻";

                case 11:
                    return "物防";

                case 12:
                    return "魔防";

                case 13:
                    return "命中";

                case 14:
                    return "闪避";

                case 15:
                    return "暴击";

                case 0x10:
                    return "暴伤";

                case 0x11:
                    return "韧性";

                case 0x12:
                    return "格挡";

                case 0x13:
                    return "速度";

                case 20:
                    return "幸运";

                case 0x15:
                    return "最小攻击";

                case 0x16:
                    return "攻击";
            }
            return string.Empty;
        }

        public static void ChangeGrayShader(UISprite spr, int depth = 0)
        {
        }

        public static void ChangeNormalShader(UISprite spr, int depth = 0)
        {
        }

        public static GameObject CloneObj(Transform obj)
        {
            GameObject obj2 = UnityEngine.Object.Instantiate(obj.gameObject, obj.localPosition, obj.localRotation) as GameObject;
            obj2.transform.parent = obj.parent;
            obj2.transform.localPosition = obj.localPosition;
            obj2.transform.localRotation = obj.localRotation;
            obj2.transform.localScale = obj.localScale;
            return obj2;
        }

        public static void SetButtonState(Transform btn, bool state, int depth)
        {
            if (state)
            {
                ChangeNormalShader(btn.FindChild("background").GetComponent<UISprite>(), depth);
                btn.GetComponent<BoxCollider>().enabled = true;
            }
            else
            {
                ChangeGrayShader(btn.FindChild("background").GetComponent<UISprite>(), depth);
                btn.GetComponent<BoxCollider>().enabled = false;
            }
        }

        public static void ShowTimeToHMS(int time, UILabel cd)
        {
            int num3 = time / 0xe10;
            time -= num3 * 0xe10;
            int num = time / 60;
            int num2 = time - (num * 60);
            object[] objArray1 = new object[] { (num3 >= 10) ? string.Empty : "0", num3, ":", (num >= 10) ? string.Empty : "0", num, ":", (num2 >= 10) ? string.Empty : "0", num2 };
            cd.text = string.Concat(objArray1);
        }

        public static void ShowTimeToMS(int time, UILabel cd)
        {
            int num = time / 60;
            int num2 = time - (num * 60);
            object[] objArray1 = new object[] { (num >= 10) ? string.Empty : "0", num, ":", (num2 >= 10) ? string.Empty : "0", num2 };
            cd.text = string.Concat(objArray1);
        }

        public static void TogglePress(GameObject go, bool isPress)
        {
            UISprite sprite = NGUITools.FindInChild<UISprite>(go, "highlight");
            if (sprite != null)
            {
                TweenAlpha.Begin(sprite.gameObject, 0.15f, !isPress ? 0f : 1f);
            }
        }

        public static void UpdateItemAndPoint(int itemNum, Transform itemGroups, Transform fanye, int itemNumPerGroup)
        {
            int num = (itemNum / itemNumPerGroup) + (((itemNum % itemNumPerGroup) <= 0) ? 0 : 1);
            int childCount = itemGroups.childCount;
            Transform transform = itemGroups.FindChild("1");
            Transform transform2 = fanye.FindChild("1");
            while (childCount < num)
            {
                childCount++;
                CloneObj(transform).name = childCount.ToString();
                CloneObj(transform2).name = childCount.ToString();
            }
            for (int i = 1; i <= childCount; i++)
            {
                if (i <= num)
                {
                    itemGroups.FindChild(i.ToString()).gameObject.SetActive(true);
                    fanye.FindChild(i.ToString()).gameObject.SetActive(true);
                }
                else
                {
                    itemGroups.FindChild(i.ToString()).gameObject.SetActive(false);
                    fanye.FindChild(i.ToString()).gameObject.SetActive(false);
                }
            }
            Transform transform3 = itemGroups.FindChild(num.ToString());
            int num4 = ((itemNum % itemNumPerGroup) <= 0) ? itemNumPerGroup : (itemNum % itemNumPerGroup);
            for (int j = num4 + 1; j <= itemNumPerGroup; j++)
            {
                transform3.FindChild("Items/" + j).gameObject.SetActive(false);
            }
            itemGroups.GetComponent<UIGrid>().Reposition();
            itemGroups.GetComponent<UICenterOnChild>().CenterOn(itemGroups.FindChild("1"));
            fanye.localPosition = new Vector3(fanye.localPosition.x - (((childCount - 1) * fanye.GetComponent<UIGrid>().cellWidth) / 2f), fanye.localPosition.y, 0f);
            fanye.GetComponent<UIGrid>().Reposition();
            fanye.FindChild("1").GetComponent<UIToggle>().value = true;
        }
    }
}

