﻿namespace com.game.Speech
{
    using System;

    public class AssassinAttackedAndMonster0to4Command : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 30f;
            base.probability = 50;
            base.soundList.Add("30004");
            base.soundList.Add("30005");
        }
    }
}

