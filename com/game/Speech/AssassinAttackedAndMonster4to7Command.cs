﻿namespace com.game.Speech
{
    using System;

    public class AssassinAttackedAndMonster4to7Command : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 10f;
            base.probability = 20;
            base.soundList.Add("30006");
            base.soundList.Add("30007");
        }
    }
}

