﻿namespace com.game.Speech
{
    using System;

    public class AssassinClearanceFailedCommand : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("30014");
        }
    }
}

