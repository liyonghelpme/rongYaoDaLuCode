﻿namespace com.game.Speech
{
    using System;

    public class AssassinCommand : BaseCommand
    {
        protected override string roleType
        {
            get
            {
                return "Assassin";
            }
        }
    }
}

