﻿namespace com.game.Speech
{
    using System;

    public class AssassinDeadCommand : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("30018");
        }
    }
}

