﻿namespace com.game.Speech
{
    using System;

    public class AssassinEnterCopyCommand : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 60f;
            base.probability = 60;
            base.soundList.Add("30001");
            base.soundList.Add("30002");
            base.soundList.Add("30003");
        }
    }
}

