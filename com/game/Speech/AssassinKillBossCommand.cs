﻿namespace com.game.Speech
{
    using System;

    public class AssassinKillBossCommand : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("30008");
            base.soundList.Add("30009");
            base.soundList.Add("30010");
            base.soundList.Add("30011");
        }
    }
}

