﻿namespace com.game.Speech
{
    using System;

    public class AssassinKnockedDownCommand : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("30015");
            base.soundList.Add("30016");
            base.soundList.Add("30017");
        }
    }
}

