﻿namespace com.game.Speech
{
    using System;

    public class AssassinSkill1Command : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 30f;
            base.probability = 60;
            base.soundList.Add("30026");
        }
    }
}

