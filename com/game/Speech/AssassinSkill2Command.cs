﻿namespace com.game.Speech
{
    using System;

    public class AssassinSkill2Command : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 30f;
            base.probability = 80;
            base.soundList.Add("30027");
            base.soundList.Add("30028");
        }
    }
}

