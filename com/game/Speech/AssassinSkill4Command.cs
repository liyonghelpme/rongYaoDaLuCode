﻿namespace com.game.Speech
{
    using System;

    public class AssassinSkill4Command : AssassinCommand
    {
        protected override void InitData()
        {
            base.cd = 30f;
            base.probability = 50;
            base.soundList.Add("30029");
        }
    }
}

