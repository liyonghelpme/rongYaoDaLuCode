﻿namespace com.game.Speech
{
    using com.game.sound;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class BaseCommand
    {
        protected float cd = 10f;
        private string curSoundId = string.Empty;
        protected float lastRunTime = -1000f;
        protected int probability = 100;
        protected IList<string> soundList = new List<string>();

        public BaseCommand()
        {
            this.InitData();
        }

        private bool ConditionMatched()
        {
            bool flag = (Time.time - this.lastRunTime) >= this.cd;
            bool flag2 = false;
            if (this.soundList.Count > 0)
            {
                flag2 = UnityEngine.Random.Range(1, 0x65) <= this.probability;
            }
            return (flag && flag2);
        }

        protected virtual void InitData()
        {
        }

        public void Run()
        {
            if (this.ConditionMatched() && !this.IsPlaying)
            {
                int num = UnityEngine.Random.Range(0, this.soundList.Count);
                this.curSoundId = this.soundList[num];
                this.lastRunTime = Time.time;
                SoundMgr.Instance.PlaySpeechAudio(this.curSoundId, this.roleType, 0f);
            }
        }

        public bool IsPlaying
        {
            get
            {
                return SoundMgr.Instance.IsPlaying(this.curSoundId);
            }
        }

        protected virtual string roleType
        {
            get
            {
                return string.Empty;
            }
        }
    }
}

