﻿namespace com.game.Speech
{
    using System;

    public class MagicAttackedAndHurtOverPercent10Command : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 20f;
            base.probability = 40;
            base.soundList.Add("20008");
            base.soundList.Add("20009");
        }
    }
}

