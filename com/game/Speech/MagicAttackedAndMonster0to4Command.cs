﻿namespace com.game.Speech
{
    using System;

    public class MagicAttackedAndMonster0to4Command : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 30f;
            base.probability = 20;
            base.soundList.Add("20004");
            base.soundList.Add("20005");
        }
    }
}

