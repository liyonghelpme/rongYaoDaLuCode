﻿namespace com.game.Speech
{
    using System;

    public class MagicAttackedAndMonster4to7Command : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 10f;
            base.probability = 50;
            base.soundList.Add("20006");
            base.soundList.Add("20007");
        }
    }
}

