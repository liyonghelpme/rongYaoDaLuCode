﻿namespace com.game.Speech
{
    using System;

    public class MagicBeautiCryCommand : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("20003");
            base.soundList.Add("20004");
            base.soundList.Add("20005");
            base.soundList.Add("20006");
            base.soundList.Add("20007");
        }
    }
}

