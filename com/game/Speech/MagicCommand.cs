﻿namespace com.game.Speech
{
    using System;

    public class MagicCommand : BaseCommand
    {
        protected override string roleType
        {
            get
            {
                return "Magic";
            }
        }
    }
}

