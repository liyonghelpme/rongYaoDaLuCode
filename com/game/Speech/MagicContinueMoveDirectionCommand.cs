﻿namespace com.game.Speech
{
    using System;

    public class MagicContinueMoveDirectionCommand : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 60f;
            base.probability = 50;
            base.soundList.Add("20001");
            base.soundList.Add("20002");
        }
    }
}

