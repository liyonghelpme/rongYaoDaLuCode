﻿namespace com.game.Speech
{
    using System;

    public class MagicEnterCopyCommand : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("20018");
            base.soundList.Add("20019");
        }
    }
}

