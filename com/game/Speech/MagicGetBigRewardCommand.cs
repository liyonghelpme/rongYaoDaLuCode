﻿namespace com.game.Speech
{
    using System;

    public class MagicGetBigRewardCommand : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("20020");
            base.soundList.Add("20021");
        }
    }
}

