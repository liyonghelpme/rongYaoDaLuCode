﻿namespace com.game.Speech
{
    using System;

    public class MagicHPBelowPercent5Command : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 30f;
            base.probability = 100;
            base.soundList.Add("20010");
            base.soundList.Add("20011");
        }
    }
}

