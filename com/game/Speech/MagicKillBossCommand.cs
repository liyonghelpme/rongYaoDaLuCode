﻿namespace com.game.Speech
{
    using System;

    public class MagicKillBossCommand : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 0f;
            base.probability = 100;
            base.soundList.Add("20012");
            base.soundList.Add("20013");
            base.soundList.Add("20014");
            base.soundList.Add("20015");
        }
    }
}

