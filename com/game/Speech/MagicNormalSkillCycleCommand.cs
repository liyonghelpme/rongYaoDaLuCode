﻿namespace com.game.Speech
{
    using System;

    public class MagicNormalSkillCycleCommand : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 60f;
            base.probability = 100;
            base.soundList.Add("20016");
        }
    }
}

