﻿namespace com.game.Speech
{
    using System;

    public class MagicUseSkillAndMonsterOver7Command : MagicCommand
    {
        protected override void InitData()
        {
            base.cd = 20f;
            base.probability = 40;
            base.soundList.Add("20003");
        }
    }
}

