﻿namespace com.game.Speech
{
    using System;

    public class SpeechConst
    {
        public const int AssassinAttackedAndMonster0to4 = 15;
        public const int AssassinAttackedAndMonster4to7 = 0x10;
        public const int AssassinClearanceFailed = 0x13;
        public const int AssassinDead = 0x15;
        public const int AssassinEnterCopy = 14;
        public const int AssassinKillBoss = 0x11;
        public const int AssassinKnockedDown = 20;
        public const int AssassinNormalAttack1 = 0x16;
        public const int AssassinNormalAttack2 = 0x17;
        public const int AssassinNormalAttack3 = 0x18;
        public const int AssassinNormalAttack4 = 0x19;
        public const int AssassinSkill1 = 0x1a;
        public const int AssassinSkill2 = 0x1b;
        public const int AssassinSkill3 = 0x1c;
        public const int AssassinSkill4 = 0x1d;
        public const int AssassinWin = 0x12;
        public const int MagicAttackedAndHurtOverPercent10 = 5;
        public const int MagicAttackedAndMonster0to4 = 3;
        public const int MagicAttackedAndMonster4to7 = 4;
        public const int MagicBeautyCry = 12;
        public const int MagicClearanceFailed = 9;
        public const int MagicContinueMoveDirection = 1;
        public const int MagicEnterCopy = 10;
        public const int MagicGetBigReward = 11;
        public const int MagicHPBelowPercent5 = 6;
        public const int MagicKillBoss = 7;
        public const int MagicNormalSkillCycle = 8;
        public const int MagicUseSkillAndMonsterOver7 = 2;
        public const int MagicWin = 13;
    }
}

