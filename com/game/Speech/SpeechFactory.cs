﻿namespace com.game.Speech
{
    using System;

    public class SpeechFactory
    {
        private static BaseCommand GetAssassinCommand(int commandType)
        {
            switch (commandType)
            {
                case 14:
                    return new AssassinEnterCopyCommand();

                case 15:
                    return new AssassinAttackedAndMonster0to4Command();

                case 0x10:
                    return new AssassinAttackedAndMonster4to7Command();

                case 0x11:
                    return new AssassinKillBossCommand();

                case 0x12:
                    return new AssassinWinCommand();

                case 0x13:
                    return new AssassinClearanceFailedCommand();

                case 20:
                    return new AssassinKnockedDownCommand();

                case 0x15:
                    return new AssassinDeadCommand();

                case 0x16:
                    return new AssassinNormalAttack1Command();

                case 0x17:
                    return new AssassinNormalAttack2Command();

                case 0x18:
                    return new AssassinNormalAttack3Command();

                case 0x19:
                    return new AssassinNormalAttack4Command();

                case 0x1a:
                    return new AssassinSkill1Command();

                case 0x1b:
                    return new AssassinSkill2Command();

                case 0x1c:
                    return new AssassinSkill3Command();

                case 0x1d:
                    return new AssassinSkill4Command();
            }
            return null;
        }

        private static BaseCommand GetMagicCommand(int commandType)
        {
            switch (commandType)
            {
                case 1:
                    return new MagicContinueMoveDirectionCommand();

                case 2:
                    return new MagicUseSkillAndMonsterOver7Command();

                case 3:
                    return new MagicAttackedAndMonster0to4Command();

                case 4:
                    return new MagicAttackedAndMonster4to7Command();

                case 5:
                    return new MagicAttackedAndHurtOverPercent10Command();

                case 6:
                    return new MagicHPBelowPercent5Command();

                case 7:
                    return new MagicKillBossCommand();

                case 8:
                    return new MagicNormalSkillCycleCommand();

                case 9:
                    return new MagicClearanceFailedCommand();

                case 10:
                    return new MagicEnterCopyCommand();

                case 11:
                    return new MagicGetBigRewardCommand();

                case 12:
                    return new MagicBeautiCryCommand();

                case 13:
                    return new MagicWinCommand();
            }
            return null;
        }

        public static BaseCommand GetSpeechComand(int commandType)
        {
            if (commandType <= 13)
            {
                return GetMagicCommand(commandType);
            }
            if (commandType <= 0x1d)
            {
                return GetAssassinCommand(commandType);
            }
            return null;
        }
    }
}

