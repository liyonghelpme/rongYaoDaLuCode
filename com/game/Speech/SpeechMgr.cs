﻿namespace com.game.Speech
{
    using System;
    using System.Collections.Generic;

    public class SpeechMgr
    {
        private IDictionary<int, BaseCommand> commandDictionary = new Dictionary<int, BaseCommand>();
        public static SpeechMgr Instance = ((Instance != null) ? Instance : new SpeechMgr());

        private bool HasCommandRun()
        {
            IEnumerator<KeyValuePair<int, BaseCommand>> enumerator = this.commandDictionary.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    KeyValuePair<int, BaseCommand> current = enumerator.Current;
                    if (current.Value.IsPlaying)
                    {
                        return true;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return false;
        }

        public void PlayAttackedAndMonster0to4Speech()
        {
            if (this.IsMagicSpeech)
            {
                this.PlaySpeech(3);
            }
            else if (this.IsAssassinSpeech)
            {
                this.PlaySpeech(15);
            }
        }

        public void PlayAttackedAndMonster4to7Speech()
        {
            if (this.IsMagicSpeech)
            {
                this.PlaySpeech(4);
            }
            else if (this.IsAssassinSpeech)
            {
                this.PlaySpeech(0x10);
            }
        }

        public void PlayEnterCopySpeech()
        {
            if (this.IsMagicSpeech)
            {
                this.PlaySpeech(10);
            }
            else if (this.IsAssassinSpeech)
            {
                this.PlaySpeech(14);
            }
        }

        public void PlayFailedSpeech()
        {
            if (this.IsMagicSpeech)
            {
                this.PlaySpeech(9);
            }
            else if (this.IsAssassinSpeech)
            {
                this.PlaySpeech(0x13);
            }
        }

        public void PlayKillBossSpeech()
        {
            if (this.IsMagicSpeech)
            {
                this.PlaySpeech(7);
            }
            else if (this.IsAssassinSpeech)
            {
                this.PlaySpeech(0x11);
            }
        }

        public void PlaySpeech(int commandType)
        {
            if (!this.HasCommandRun())
            {
                BaseCommand speechComand = null;
                if (!this.commandDictionary.ContainsKey(commandType))
                {
                    speechComand = SpeechFactory.GetSpeechComand(commandType);
                    if (speechComand != null)
                    {
                        this.commandDictionary.Add(commandType, speechComand);
                    }
                }
                else
                {
                    speechComand = this.commandDictionary[commandType];
                }
                if (speechComand != null)
                {
                    speechComand.Run();
                }
            }
        }

        public void PlayWinSpeech()
        {
            if (this.IsMagicSpeech)
            {
                this.PlaySpeech(13);
            }
            else if (this.IsAssassinSpeech)
            {
                this.PlaySpeech(0x12);
            }
        }

        public bool IsAssassinSpeech
        {
            get
            {
                return false;
            }
        }

        public bool IsMagicSpeech
        {
            get
            {
                return false;
            }
        }
    }
}

