﻿namespace com.game.autoroad
{
    using com.game;
    using com.game.utils;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.task;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AutoRoad
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map6;
        private short curStep = -1;
        public static AutoRoad intance = new AutoRoad();
        private short pathLen;
        private IList<PathStepVo> pathList = new List<PathStepVo>();
        public const short TO_COPY = 2;
        public const short TO_NPC = 1;
        public const short TO_SCENE = 3;
        private const short TO_STEP_COPY = 2;
        private const short TO_STEP_NPC = 1;
        private const short TO_STEP_SCENE = 3;
        private const short TO_STEP_WORLDMAP = 4;

        private void add2PathList(int index, int stepType, string targetId)
        {
            if (this.pathList.Count < index)
            {
                this.pathList.Add(new PathStepVo());
            }
            PathStepVo vo = this.pathList[index - 1];
            vo.stepType = stepType;
            vo.targetId = targetId;
            this.pathLen = (short) (this.pathLen + 1);
        }

        public void autoWalk()
        {
            this.curStep = (short) (this.curStep + 1);
            if (!this.isRoading || (this.curStep >= this.pathLen))
            {
                this.curStep = -1;
                this.isRoading = false;
                Log.info(this, "-autoWalk() 自动寻路已结束！");
            }
            else
            {
                Log.info(this, "-autoWalk() 开始自动寻路！");
                PathStepVo vo = this.pathList[this.curStep];
                switch (vo.stepType)
                {
                    case 2:
                        this.moveTo(AppMap.Instance.GetCopyPoint(vo.targetId));
                        break;

                    case 3:
                        this.moveTo(AppMap.Instance.MapPointDisplay);
                        break;
                }
            }
        }

        public void cancelAutoWalk()
        {
            this.isRoading = false;
            (AppMap.Instance.me.Controller as MeControler).StopWalk();
        }

        private bool create2CopyPath(string copyId)
        {
            return true;
        }

        private bool create2MapPath(string mapId)
        {
            if (AppMap.Instance.mapParserII.MapId.ToString().Equals(mapId))
            {
                return false;
            }
            this.add2PathList(1, 3, null);
            this.add2PathList(2, 4, mapId);
            return true;
        }

        private bool create2NpcPath(string npcId)
        {
            return true;
        }

        public void createPath(short targetType, string targetId)
        {
            if (((targetType >= 1) && (targetType <= 3)) && !StringUtils.isEmpty(targetId))
            {
                this.pathLen = 0;
                this.curStep = -1;
                this.isRoading = false;
                bool flag = false;
                switch (targetType)
                {
                    case 1:
                        flag = this.create2NpcPath(targetId);
                        break;

                    case 2:
                        flag = this.create2CopyPath(targetId);
                        break;

                    case 3:
                        flag = this.create2MapPath(targetId);
                        break;
                }
                if (flag)
                {
                    this.isRoading = true;
                    this.autoWalk();
                }
            }
        }

        public void createTaskPath(string taskId, int index = 0)
        {
            Task task = TaskExecute.instance.get(taskId);
            if (task != null)
            {
                if (task.state != 1)
                {
                    this.createPath(1, task.curNpc());
                    Log.info(this, "-createTaskPath() 自动寻路到NPC：" + task.curNpc() + " 跟前！");
                }
                else
                {
                    TaskCondition condition = (index >= 1) ? task.getCondition(index) : task.curCondition();
                    if (condition != null)
                    {
                        string sort = condition.sort;
                        if (sort != null)
                        {
                            int num;
                            if (<>f__switch$map6 == null)
                            {
                                Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                                dictionary.Add("4", 0);
                                dictionary.Add("3", 1);
                                dictionary.Add("5", 1);
                                dictionary.Add("9", 1);
                                <>f__switch$map6 = dictionary;
                            }
                            if ((<>f__switch$map6.TryGetValue(sort, out num) && (num != 0)) && (num == 1))
                            {
                                this.createPath(2, condition.backupId);
                            }
                        }
                    }
                }
            }
        }

        private bool moveTo(BaseDisplay display)
        {
            if (display == null)
            {
                return false;
            }
            if (display.GoBase == null)
            {
                return false;
            }
            Transform transform = display.GoBase.transform;
            AppMap.Instance.clickVo.SaveClicker(display);
            AppMap.Instance.me.Controller.MoveTo(transform.position.x, transform.position.y, null);
            return true;
        }

        public bool isRoading { get; set; }
    }
}

