﻿namespace com.game.autoroad
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.task;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AutoRoad_old
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map7;
        private short curStep = -1;
        public static AutoRoad_old intance = new AutoRoad_old();
        private short pathLen;
        private IList<PathStepVo> pathList = new List<PathStepVo>();
        private IList<string> tempMapList = new List<string>();
        public const short TO_COPY = 2;
        public const short TO_MONSTER = 4;
        public const short TO_NPC = 1;
        public const short TO_SCENE = 3;
        private const short TO_STEP_COPY = 2;
        private const short TO_STEP_MONSTER = 5;
        private const short TO_STEP_NPC = 1;
        private const short TO_STEP_SCENE = 3;
        private const short TO_STEP_WORLDMAP = 4;

        private void add2PathList(int index, int stepType, string targetId)
        {
            if (this.pathList.Count < index)
            {
                this.pathList.Add(new PathStepVo());
            }
            PathStepVo vo = this.pathList[index - 1];
            vo.stepType = stepType;
            vo.targetId = targetId;
            this.pathLen = (short) (this.pathLen + 1);
        }

        public void autoWalk()
        {
            this.curStep = (short) (this.curStep + 1);
            if (!this.isRoading || (this.curStep >= this.pathLen))
            {
                this.curStep = -1;
                this.isRoading = false;
            }
            else
            {
                PathStepVo vo = this.pathList[this.curStep];
                switch (vo.stepType)
                {
                    case 3:
                        this.moveTo(AppMap.Instance.GetMapPoint(vo.targetId), null);
                        break;

                    case 5:
                    {
                        MonsterDisplay display = this.getMonsterWithUid(vo.targetId);
                        if (display != null)
                        {
                            this.moveTo(display, new MoveEndCallback(this.moveEndBack));
                        }
                        else
                        {
                            MonsterVo monster = Singleton<MonsterMgr>.Instance.GetMonster(vo.targetId);
                            if (monster != null)
                            {
                                (AppMap.Instance.me.Controller as MeControler).MoveToAndTellServer(monster.X, monster.Y, new MoveEndCallback(this.moveEndBack));
                            }
                        }
                        break;
                    }
                }
            }
        }

        private bool createCopyPath(string copyId)
        {
            return false;
        }

        private bool createMapPath(string mapId)
        {
            string str = AppMap.Instance.mapParserII.MapId.ToString();
            return (((str != null) && !str.Equals(mapId)) && false);
        }

        private bool createMonsterPath(string mapId, uint monsterId)
        {
            string str = AppMap.Instance.mapParserII.MapId.ToString();
            SysMonsterVo vo = BaseDataMgr.instance.getSysMonsterVo(monsterId);
            if (((str == null) || (mapId == null)) || (vo == null))
            {
                return false;
            }
            Log.info(this, string.Concat(new object[] { "-createMonsterPath() 追踪到场景ID:", mapId, ",怪物ID:", monsterId, ",怪物名称:", vo.name }));
            return (str.Equals(mapId) || true);
        }

        private bool createNpcPath(string npcId)
        {
            return true;
        }

        public void createPath(short targetType, string targetId, string monsterId = null)
        {
            if (((targetType >= 1) && (targetType <= 4)) && !StringUtils.isEmpty(targetId))
            {
                this.isRoading = false;
                this.pathLen = 0;
                this.curStep = -1;
                bool flag = false;
                switch (targetType)
                {
                    case 1:
                        flag = this.createNpcPath(targetId);
                        break;

                    case 2:
                        flag = this.createCopyPath(targetId);
                        break;

                    case 3:
                        flag = this.createMapPath(targetId);
                        break;
                }
                if (flag)
                {
                    this.isRoading = true;
                    this.autoWalk();
                }
            }
        }

        public void createTaskPath(string taskId, int index = 0)
        {
            com.u3d.bases.task.Task task = TaskExecute.instance.get(taskId);
            if (task != null)
            {
                if (task.state != 1)
                {
                    this.createPath(1, task.curNpc(), null);
                }
                else
                {
                    TaskCondition condition = (index >= 1) ? task.getCondition(index) : task.curCondition();
                    if (condition != null)
                    {
                        string sort = condition.sort;
                        if (sort != null)
                        {
                            int num;
                            if (<>f__switch$map7 == null)
                            {
                                Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                                dictionary.Add("3", 0);
                                dictionary.Add("4", 1);
                                dictionary.Add("5", 1);
                                <>f__switch$map7 = dictionary;
                            }
                            if (<>f__switch$map7.TryGetValue(sort, out num))
                            {
                                if (num == 0)
                                {
                                    this.createPath(4, condition.mapId, condition.targetId);
                                }
                                else if (num == 1)
                                {
                                    this.createPath(3, condition.mapId, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        private string findMainMapId(string mapId)
        {
            return string.Empty;
        }

        private void findMapLinks(string startMapId, string targetMapId, bool isUp)
        {
            this.tempMapList.Clear();
            bool flag = false;
            SysMapVo mapVo = BaseDataMgr.instance.GetMapVo(uint.Parse(startMapId));
            do
            {
                this.tempMapList.Add(mapVo.id.ToString());
                if (mapVo.id.Equals(targetMapId))
                {
                    flag = true;
                    break;
                }
            }
            while (mapVo.type != 1);
            if (!flag)
            {
                this.tempMapList.Clear();
            }
        }

        public MonsterDisplay getMonsterWithUid(string uid)
        {
            if (!StringUtils.isEmpty(uid))
            {
                MonsterVo vo = null;
                IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        MonsterDisplay current = enumerator.Current;
                        vo = (MonsterVo) current.GetVo();
                        if (vo.monsterId.Equals(uid))
                        {
                            return current;
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
            return null;
        }

        private void moveEndBack(BaseControler controler)
        {
            Log.info(this, "-moveEndBack() 移到怪物跟前结束！");
            this.autoWalk();
        }

        private bool moveTo(BaseDisplay display, MoveEndCallback callback = null)
        {
            if (display == null)
            {
                return false;
            }
            Transform transform = display.GoBase.transform;
            AppMap.Instance.clickVo.SaveClicker(display);
            (AppMap.Instance.me.Controller as MeControler).MoveToAndTellServer(transform.position.x, transform.position.y, callback);
            transform = null;
            return true;
        }

        private void println()
        {
            Log.info(this, "=========================================\n");
            PathStepVo vo = null;
            for (int i = 0; i < this.pathLen; i++)
            {
                vo = this.pathList[i];
                Log.info(this, "[" + vo.stepDesc() + "] targetId:" + vo.targetId);
            }
            Log.info(this, "=========================================\n");
        }

        public bool isRoading { get; set; }
    }
}

