﻿namespace com.game.autoroad
{
    using System;

    public class PathStepVo
    {
        public int stepType;
        public string targetId;

        internal string stepDesc()
        {
            if (this.stepType == 1)
            {
                return "到NPC前";
            }
            if (this.stepType == 2)
            {
                return "到副本传送点前";
            }
            if (this.stepType == 3)
            {
                return "到场景传送点前";
            }
            if (this.stepType == 4)
            {
                return "在世界地图行走";
            }
            if (this.stepType == 5)
            {
                return "到怪物前";
            }
            return string.Empty;
        }
    }
}

