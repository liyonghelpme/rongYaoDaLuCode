﻿namespace com.game.basic
{
    using System;

    public class AttributeInfo
    {
        public uint abDef;
        public float abDefPercent = 1f;
        public uint abHurt;
        public float abHurtPercent = 1f;
        public uint antiKnockBack;
        public float antiKnockBackPercent = 1f;
        public uint antiKnockDown;
        public float antiKnockDownPercent = 1f;
        public uint atkCdDec;
        public uint atkNormalAdd;
        public uint atkSpeedAdd;
        public uint attM;
        public uint attMPen;
        public float attMPercent = 1f;
        public uint attMVam;
        public uint attP;
        public uint attPPen;
        public float attPPercent = 1f;
        public uint attPVam;
        public uint crit;
        public uint critAdd;
        public float critPercent = 1f;
        public uint defM;
        public uint defMPen;
        public float defMPercent = 1f;
        public uint defP;
        public uint defPPen;
        public float defPPercent = 1f;
        public uint dmgBackM;
        public uint dmgBackP;
        public uint dodge;
        public float dodgePercent = 1f;
        public uint flex;
        public float flexPercent = 1f;
        public uint hit;
        public float hitPercent = 1f;
        public uint hp;
        public uint hpFull;
        public float hpFullPercent = 1f;
        public float hpPercent = 1f;
        public uint hpRe;
        public float hpRePercent = 1f;
        public uint moveSpeed;
        public uint moveSpeedAdd;
    }
}

