﻿namespace com.game.basic
{
    using com.game.manager;
    using System;
    using System.Collections.Generic;

    public class CategoryType
    {
        public const int AbDef = 0x15;
        public const int AbHurt = 20;
        public const int AntiKnockDown = 0x25;
        public const int AtkSpeedAdd = 30;
        public static List<int> AttackStone = new List<int> { 12, 13, 0x19, 0x1a, 0x1b, 0x1c, 0x12, 0x1d, 30, 0x1f, 0x20 };
        public const int AttM = 13;
        public const int AttP = 12;
        public const int CdDec = 0x1f;
        public const int Crit = 0x12;
        public const int CritAdd = 0x1d;
        public static List<int> DefendStone = new List<int> { 11, 14, 15, 0x21, 0x22, 0x23, 0x24, 0x13, 0x16 };
        public const int DefM = 15;
        public const int DefMagPen = 0x22;
        public const int DefP = 14;
        public const int DefPhyPen = 0x21;
        public const int DmgBackMag = 0x24;
        public const int DmgBackPhy = 0x23;
        public const int Dodge = 0x11;
        public const int Flex = 0x13;
        public const int Hit = 0x10;
        public const int HP = 10;
        public const int HpFull = 11;
        public const int HpRe = 0x16;
        public const int MagPen = 0x1a;
        public const int MagVam = 0x1c;
        public const int MoveSpeed = 0x17;
        public const int MoveSpeedAdd = 0x18;
        public const int NormalAtkDmg = 0x20;
        public const int PhyPen = 0x19;
        public const int PhyVam = 0x1b;

        public static string GetAttName(int id)
        {
            return LanguageManager.GetWord("Common.Attri" + id.ToString());
        }
    }
}

