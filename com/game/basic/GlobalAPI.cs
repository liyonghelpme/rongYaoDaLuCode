﻿namespace com.game.basic
{
    using com.game.basic.events;
    using com.game.Interface.Tick;
    using System;
    using UnityEngine;

    public class GlobalAPI
    {
        public static readonly Notifier facade;
        public static readonly ITickManager tickManager = TickManager.instance;

        static GlobalAPI()
        {
            if (facade == null)
            {
            }
            facade = new Notifier();
        }

        public static void GC()
        {
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }
    }
}

