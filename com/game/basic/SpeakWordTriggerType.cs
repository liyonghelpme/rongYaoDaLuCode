﻿namespace com.game.basic
{
    using System;

    public class SpeakWordTriggerType
    {
        public const int BloodPercent = 0x3ea;
        public const int BossSkill = 0x3eb;
        public const int GotoPoint = 0x3e9;
        public const int MonsterBatch = 0x3ec;
    }
}

