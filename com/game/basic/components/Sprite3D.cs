﻿namespace com.game.basic.components
{
    using com.game.basic.events;
    using System;
    using System.Collections;
    using UnityEngine;

    public class Sprite3D : EventDispatcher
    {
        private GameObject gameObject;

        public Sprite3D()
        {
            this.gameObject = new GameObject(base.GetType().Name);
        }

        public void AddChild(GameObject child)
        {
            if (child == null)
            {
                throw new ArgumentException("Parameter child must be non-null");
            }
            child.transform.parent = this.Container.transform;
            child.transform.localPosition = Vector3.zero;
            child.transform.localRotation = Quaternion.identity;
            child.transform.localScale = Vector3.one;
        }

        protected virtual void ConfigureSprite()
        {
        }

        public void RemoveAllChild()
        {
            if (this.Container == null)
            {
                throw new ArgumentException("The parent must be non-null");
            }
            this.Container.transform.DetachChildren();
        }

        public void RemoveChild(GameObject child)
        {
            IEnumerator enumerator = this.Container.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    UnityEngine.Transform current = (UnityEngine.Transform) enumerator.Current;
                    if (current == child.transform)
                    {
                        current.parent = null;
                        UnityEngine.Object.Destroy(current.gameObject);
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }

        public GameObject Container
        {
            get
            {
                return this.gameObject;
            }
        }

        public Vector3 Position
        {
            get
            {
                return this.Container.transform.position;
            }
            set
            {
                this.Container.transform.position = value;
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return this.Container.transform.rotation;
            }
            set
            {
                this.Container.transform.rotation = value;
            }
        }

        public Vector3 Scale
        {
            get
            {
                return this.Container.transform.localScale;
            }
            set
            {
                this.Container.transform.localScale = value;
            }
        }

        public UnityEngine.Transform Transform
        {
            get
            {
                return this.gameObject.transform;
            }
        }
    }
}

