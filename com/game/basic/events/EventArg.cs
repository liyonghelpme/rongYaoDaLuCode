﻿namespace com.game.basic.events
{
    using System;
    using UnityEngine.Events;

    public class EventArg : UnityEvent<EventArg>
    {
        public object arg;
        public string eventType;
        public EasyEventDispatcher notifier;

        public EventArg(string type)
        {
            this.eventType = type;
        }

        public EventArg(string type, object arg)
        {
            this.eventType = type;
            this.arg = arg;
        }
    }
}

