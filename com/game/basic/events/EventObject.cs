﻿namespace com.game.basic.events
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine.Events;

    public class EventObject : UnityEvent<EventObject>
    {
        private EventDispatcher currentTarget;
        public object data;
        private string eventType;
        private EventDispatcher target;

        public EventObject(string eventType, object data = null)
        {
            this.eventType = eventType;
            this.data = data;
        }

        public void Invoke(EventObject evtObject)
        {
            base.Invoke(evtObject);
        }

        public EventDispatcher CurrentTarget
        {
            get
            {
                return this.currentTarget;
            }
            set
            {
                this.currentTarget = value;
            }
        }

        public string EventType
        {
            get
            {
                return this.eventType;
            }
        }

        public EventDispatcher Target
        {
            get
            {
                return this.target;
            }
            set
            {
                this.target = value;
            }
        }
    }
}

