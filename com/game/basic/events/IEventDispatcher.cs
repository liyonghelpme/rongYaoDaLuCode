﻿namespace com.game.basic.events
{
    using System;
    using UnityEngine.Events;

    public interface IEventDispatcher
    {
        void AddEventListener(string type, UnityAction<EventObject> listener);
        void Dispatcher(EventObject evt);
        bool HasEventListener(string type);
        void RemoveEventListener(string type, UnityAction<EventObject> listener);
    }
}

