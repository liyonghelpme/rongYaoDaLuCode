﻿namespace com.game.basic.events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;

    public class Notifier
    {
        private Dictionary<int, NoticeListener> _listernDis = new Dictionary<int, NoticeListener>();
        public NoticeListener postNotify;
        public NoticeListener prevNotify;

        public void Add(int type, NoticeListener listener)
        {
            if (listener != null)
            {
                if (!this._listernDis.ContainsKey(type))
                {
                    this._listernDis.Add(type, listener);
                }
                else
                {
                    Dictionary<int, NoticeListener> dictionary;
                    int num;
                    Dictionary<int, NoticeListener> dictionary2;
                    NoticeListener source = dictionary[num];
                    (dictionary = this._listernDis)[num = type] = (NoticeListener) Delegate.Remove(source, listener);
                    source = dictionary2[num];
                    (dictionary2 = this._listernDis)[num = type] = (NoticeListener) Delegate.Combine(source, listener);
                }
            }
        }

        public virtual void Clear()
        {
            this._listernDis.Clear();
        }

        public void Clear(int type)
        {
            this._listernDis.Remove(type);
        }

        public bool Contains(int type)
        {
            return ((this._listernDis != null) && this._listernDis.ContainsKey(type));
        }

        public bool Contains(int type, NoticeListener listener)
        {
            if (!this.Contains(type))
            {
                return false;
            }
            NoticeListener listener2 = this._listernDis[type];
            return listener2.GetInvocationList().Contains<Delegate>(listener);
        }

        private void Invoke(int type, int v1 = 0, int v2 = 0, object data = null)
        {
            if (this.Contains(type))
            {
                NoticeListener listener = this._listernDis[type];
                foreach (NoticeListener listener2 in listener.GetInvocationList())
                {
                    listener2(type, v1, v2, data);
                }
            }
        }

        private void Invoke(NoticeListener call, int type, int v1 = 0, int v2 = 0, object data = null)
        {
            if (call != null)
            {
                foreach (NoticeListener listener in call.GetInvocationList())
                {
                    listener(type, v1, v2, data);
                }
            }
        }

        public virtual void Notify(int type, int v1 = 0, int v2 = 0, object data = null)
        {
            this.Invoke(this.prevNotify, type, v1, v2, data);
            this.Invoke(type, v1, v2, data);
            this.Invoke(this.postNotify, type, v1, v2, data);
        }

        public void Remove(int type, NoticeListener listener)
        {
            if ((listener != null) && this._listernDis.ContainsKey(type))
            {
                Dictionary<int, NoticeListener> dictionary;
                int num;
                NoticeListener source = dictionary[num];
                (dictionary = this._listernDis)[num = type] = (NoticeListener) Delegate.Remove(source, listener);
                if (this._listernDis[type] == null)
                {
                    this._listernDis.Remove(type);
                }
            }
        }
    }
}

