﻿namespace com.game.cmd
{
    using System;

    public class CMD
    {
        public const string CMD_1_0 = "256";
        public const string CMD_1_1 = "257";
        public const string CMD_1_2 = "258";
        public const string CMD_1_3 = "259";
        public const string CMD_1_4 = "260";
        public const string CMD_1_5 = "261";
        public const string CMD_1_6 = "262";
        public const string CMD_1_7 = "263";
        public const string CMD_1_8 = "264";
        public const string CMD_1_9 = "265";
        public const string CMD_10_1 = "2561";
        public const string CMD_10_10 = "2570";
        public const string CMD_10_11 = "2571";
        public const string CMD_10_12 = "2572";
        public const string CMD_10_13 = "2573";
        public const string CMD_10_14 = "2574";
        public const string CMD_10_2 = "2562";
        public const string CMD_10_3 = "2563";
        public const string CMD_10_4 = "2564";
        public const string CMD_10_5 = "2565";
        public const string CMD_10_6 = "2566";
        public const string CMD_10_7 = "2567";
        public const string CMD_10_8 = "2568";
        public const string CMD_10_9 = "2569";
        public const string CMD_12_1 = "3073";
        public const string CMD_12_2 = "3074";
        public const string CMD_12_3 = "3075";
        public const string CMD_12_4 = "3076";
        public const string CMD_12_6 = "3078";
        public const string CMD_12_7 = "3079";
        public const string CMD_12_8 = "3080";
        public const string CMD_12_9 = "3081";
        public const string CMD_13_0 = "3328";
        public const string CMD_13_1 = "3329";
        public const string CMD_13_10 = "3338";
        public const string CMD_13_11 = "3339";
        public const string CMD_13_12 = "3340";
        public const string CMD_13_2 = "3330";
        public const string CMD_13_3 = "3331";
        public const string CMD_13_4 = "3332";
        public const string CMD_13_5 = "3333";
        public const string CMD_13_6 = "3334";
        public const string CMD_13_7 = "3335";
        public const string CMD_13_8 = "3336";
        public const string CMD_13_9 = "3337";
        public const string CMD_14_1 = "3585";
        public const string CMD_14_10 = "3594";
        public const string CMD_14_11 = "3595";
        public const string CMD_14_12 = "3596";
        public const string CMD_14_13 = "3597";
        public const string CMD_14_14 = "3598";
        public const string CMD_14_15 = "3599";
        public const string CMD_14_16 = "3600";
        public const string CMD_14_17 = "3601";
        public const string CMD_14_18 = "3602";
        public const string CMD_14_19 = "3603";
        public const string CMD_14_2 = "3586";
        public const string CMD_14_20 = "3604";
        public const string CMD_14_21 = "3605";
        public const string CMD_14_22 = "3606";
        public const string CMD_14_3 = "3587";
        public const string CMD_14_4 = "3588";
        public const string CMD_14_5 = "3589";
        public const string CMD_14_6 = "3590";
        public const string CMD_14_7 = "3591";
        public const string CMD_14_8 = "3592";
        public const string CMD_14_9 = "3593";
        public const string CMD_15_1 = "3841";
        public const string CMD_15_2 = "3842";
        public const string CMD_15_3 = "3843";
        public const string CMD_19_1 = "4865";
        public const string CMD_19_2 = "4866";
        public const string CMD_19_4 = "4868";
        public const string CMD_2_1 = "513";
        public const string CMD_2_2 = "514";
        public const string CMD_2_3 = "515";
        public const string CMD_2_4 = "516";
        public const string CMD_2_5 = "517";
        public const string CMD_2_6 = "518";
        public const string CMD_2_7 = "519";
        public const string CMD_2_8 = "520";
        public const string CMD_2_9 = "521";
        public const string CMD_21_1 = "5377";
        public const string CMD_23_1 = "5889";
        public const string CMD_23_10 = "5898";
        public const string CMD_23_11 = "5899";
        public const string CMD_23_12 = "5900";
        public const string CMD_23_13 = "5901";
        public const string CMD_23_2 = "5890";
        public const string CMD_23_3 = "5891";
        public const string CMD_23_4 = "5892";
        public const string CMD_23_5 = "5893";
        public const string CMD_23_6 = "5894";
        public const string CMD_23_7 = "5895";
        public const string CMD_23_8 = "5896";
        public const string CMD_23_9 = "5897";
        public const string CMD_3_1 = "769";
        public const string CMD_3_10 = "778";
        public const string CMD_3_11 = "779";
        public const string CMD_3_12 = "780";
        public const string CMD_3_13 = "781";
        public const string CMD_3_14 = "782";
        public const string CMD_3_2 = "770";
        public const string CMD_3_20 = "788";
        public const string CMD_3_3 = "771";
        public const string CMD_3_30 = "798";
        public const string CMD_3_4 = "772";
        public const string CMD_3_40 = "808";
        public const string CMD_3_41 = "809";
        public const string CMD_3_42 = "810";
        public const string CMD_3_43 = "811";
        public const string CMD_3_44 = "812";
        public const string CMD_3_5 = "773";
        public const string CMD_3_50 = "818";
        public const string CMD_3_6 = "774";
        public const string CMD_3_7 = "775";
        public const string CMD_3_8 = "776";
        public const string CMD_3_9 = "777";
        public const string CMD_32_1 = "8193";
        public const string CMD_32_2 = "8194";
        public const string CMD_35_1 = "8961";
        public const string CMD_35_2 = "8962";
        public const string CMD_35_3 = "8963";
        public const string CMD_35_4 = "8964";
        public const string CMD_35_5 = "8965";
        public const string CMD_36_1 = "9217";
        public const string CMD_36_10 = "9226";
        public const string CMD_36_11 = "9227";
        public const string CMD_36_12 = "9228";
        public const string CMD_36_13 = "9229";
        public const string CMD_36_2 = "9218";
        public const string CMD_36_3 = "9219";
        public const string CMD_36_4 = "9220";
        public const string CMD_36_5 = "9221";
        public const string CMD_36_6 = "9222";
        public const string CMD_36_7 = "9223";
        public const string CMD_36_8 = "9224";
        public const string CMD_36_9 = "9225";
        public const string CMD_37_1 = "9473";
        public const string CMD_37_2 = "9474";
        public const string CMD_37_3 = "9475";
        public const string CMD_4_1 = "1025";
        public const string CMD_4_10 = "1034";
        public const string CMD_4_11 = "1035";
        public const string CMD_4_12 = "1036";
        public const string CMD_4_13 = "1037";
        public const string CMD_4_14 = "1038";
        public const string CMD_4_15 = "1039";
        public const string CMD_4_17 = "1041";
        public const string CMD_4_18 = "1042";
        public const string CMD_4_19 = "1043";
        public const string CMD_4_2 = "1026";
        public const string CMD_4_20 = "1044";
        public const string CMD_4_21 = "1045";
        public const string CMD_4_22 = "1046";
        public const string CMD_4_23 = "1047";
        public const string CMD_4_24 = "1048";
        public const string CMD_4_4 = "1028";
        public const string CMD_4_5 = "1029";
        public const string CMD_4_6 = "1030";
        public const string CMD_4_7 = "1031";
        public const string CMD_4_8 = "1032";
        public const string CMD_4_9 = "1033";
        public const string CMD_40_1 = "10241";
        public const string CMD_40_2 = "10242";
        public const string CMD_41_1 = "10497";
        public const string CMD_41_2 = "10498";
        public const string CMD_41_3 = "10499";
        public const string CMD_42_1 = "10753";
        public const string CMD_42_2 = "10754";
        public const string CMD_42_3 = "10755";
        public const string CMD_43_1 = "11009";
        public const string CMD_43_10 = "11018";
        public const string CMD_43_2 = "11010";
        public const string CMD_43_3 = "11011";
        public const string CMD_43_4 = "11012";
        public const string CMD_43_5 = "11013";
        public const string CMD_43_6 = "11014";
        public const string CMD_43_7 = "11015";
        public const string CMD_43_8 = "11016";
        public const string CMD_43_9 = "11017";
        public const string CMD_44_1 = "11265";
        public const string CMD_44_2 = "11266";
        public const string CMD_44_3 = "11267";
        public const string CMD_44_4 = "11268";
        public const string CMD_5_1 = "1281";
        public const string CMD_5_10 = "1290";
        public const string CMD_5_11 = "1291";
        public const string CMD_5_12 = "1292";
        public const string CMD_5_13 = "1293";
        public const string CMD_5_14 = "1294";
        public const string CMD_5_15 = "1295";
        public const string CMD_5_16 = "1296";
        public const string CMD_5_17 = "1297";
        public const string CMD_5_2 = "1282";
        public const string CMD_5_5 = "1285";
        public const string CMD_5_6 = "1286";
        public const string CMD_5_7 = "1287";
        public const string CMD_5_9 = "1289";
        public const string CMD_6_1 = "1537";
        public const string CMD_6_10 = "1546";
        public const string CMD_6_11 = "1547";
        public const string CMD_6_12 = "1548";
        public const string CMD_6_13 = "1549";
        public const string CMD_6_14 = "1550";
        public const string CMD_6_15 = "1551";
        public const string CMD_6_16 = "1552";
        public const string CMD_6_17 = "1553";
        public const string CMD_6_2 = "1538";
        public const string CMD_6_3 = "1539";
        public const string CMD_6_4 = "1540";
        public const string CMD_6_5 = "1541";
        public const string CMD_6_6 = "1542";
        public const string CMD_6_7 = "1543";
        public const string CMD_6_8 = "1544";
        public const string CMD_6_9 = "1545";
        public const string CMD_7_1 = "1793";
        public const string CMD_7_10 = "1802";
        public const string CMD_7_11 = "1803";
        public const string CMD_7_12 = "1804";
        public const string CMD_7_13 = "1805";
        public const string CMD_7_14 = "1806";
        public const string CMD_7_16 = "1808";
        public const string CMD_7_17 = "1809";
        public const string CMD_7_18 = "1810";
        public const string CMD_7_2 = "1794";
        public const string CMD_7_3 = "1795";
        public const string CMD_7_4 = "1796";
        public const string CMD_7_5 = "1797";
        public const string CMD_7_6 = "1798";
        public const string CMD_7_7 = "1799";
        public const string CMD_7_8 = "1800";
        public const string CMD_7_9 = "1801";
        public const string CMD_8_1 = "2049";
        public const string CMD_8_2 = "2050";
        public const string CMD_8_3 = "2051";
        public const string CMD_9_1 = "2305";
        public const string CMD_9_2 = "2306";
        public const string CMD_9_3 = "2307";
    }
}

