﻿namespace com.game.cmd
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class Command
    {
        private static Dictionary<int, ICommandHandler> _handlers;
        private int[] _params = new int[4];
        public object data;
        private const int NUM_PARAM_MAX = 4;
        public string text;
        public int type;
        public int value;

        public void Exe()
        {
            if ((_handlers != null) && _handlers.ContainsKey(this.type))
            {
                _handlers[this.type].Handle(this);
            }
        }

        public static Command Parse(string str)
        {
            Command cmd = new Command();
            Parse(cmd, str, false);
            return cmd;
        }

        public static void Parse(Command cmd, string str, bool resetCommand = true)
        {
            if (resetCommand)
            {
                cmd.type = 0;
                cmd.text = string.Empty;
                cmd.value = 0;
                int num3 = 0;
                cmd.param4 = num3;
                cmd.param3 = num3;
                cmd.param2 = num3;
                cmd.param1 = num3;
            }
            if (!string.IsNullOrEmpty(str))
            {
                char[] separator = new char[] { ',' };
                string[] strArray = str.Split(separator);
                int num = Math.Min(strArray.Length, 7);
                for (int i = 0; i < num; i++)
                {
                    switch (i)
                    {
                        case 0:
                            cmd.type = int.Parse(strArray[i]);
                            break;

                        case 1:
                            cmd.value = int.Parse(strArray[i]);
                            break;

                        case 2:
                            cmd.text = strArray[i];
                            break;

                        default:
                            cmd._params[i - 3] = int.Parse(strArray[i]);
                            break;
                    }
                }
            }
        }

        public static ICommandHandler Register(ICommandHandler handler)
        {
            if (_handlers == null)
            {
                _handlers = new Dictionary<int, ICommandHandler>();
            }
            ICommandHandler handler2 = null;
            if (_handlers.ContainsKey(handler.type))
            {
                handler2 = _handlers[handler.type];
                _handlers[handler.type] = handler;
                return handler2;
            }
            _handlers.Add(handler.type, handler);
            return handler2;
        }

        public static ICommandHandler Unregister(int type)
        {
            if (_handlers == null)
            {
                return null;
            }
            if (!_handlers.ContainsKey(type))
            {
                return null;
            }
            ICommandHandler handler = _handlers[type];
            _handlers.Remove(type);
            return handler;
        }

        public int param1
        {
            get
            {
                return this._params[0];
            }
            set
            {
                this._params[0] = value;
            }
        }

        public int param2
        {
            get
            {
                return this._params[1];
            }
            set
            {
                this._params[1] = value;
            }
        }

        public int param3
        {
            get
            {
                return this._params[2];
            }
            set
            {
                this._params[2] = value;
            }
        }

        public int param4
        {
            get
            {
                return this._params[3];
            }
            set
            {
                this._params[3] = value;
            }
        }

        public int[] paramList
        {
            get
            {
                return this._params;
            }
        }
    }
}

