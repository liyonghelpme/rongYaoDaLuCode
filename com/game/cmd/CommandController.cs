﻿namespace com.game.cmd
{
    using com.game.module.core;
    using System;

    public class CommandController : BaseControl<CommandController>
    {
        protected override void NetListener()
        {
            Command.Register(new PopUIHandler());
            Command.Register(new TipHandler());
        }
    }
}

