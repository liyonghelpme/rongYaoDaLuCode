﻿namespace com.game.cmd
{
    using System;

    public interface ICommandHandler
    {
        void Handle(Command cmd);

        int type { get; }
    }
}

