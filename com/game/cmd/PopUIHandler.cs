﻿namespace com.game.cmd
{
    using com.game.module.core;
    using System;

    public class PopUIHandler : ICommandHandler
    {
        public void Handle(Command cmd)
        {
            int num = cmd.value;
            switch (num)
            {
                case 10:
                    Singleton<ChatIIPanel>.Instance.OpenView();
                    return;

                case 12:
                    Singleton<MailView>.Instance.OpenView();
                    return;

                case 14:
                    Singleton<WifiPvpView>.Instance.OpenView();
                    return;

                case 15:
                    Singleton<ShopView>.Instance.Show(cmd.param1);
                    return;

                case 0x13:
                    return;

                case 0x15:
                    Singleton<RankView>.Instance.OpenView();
                    return;

                case 0x17:
                    this.OpenDungeon(cmd);
                    return;

                case 0x24:
                    if (cmd.param1 != 0)
                    {
                        if (cmd.param1 == 1)
                        {
                            Singleton<EquipmentMainPanel>.Instance.OpenEquipment(null, cmd.param2);
                        }
                        return;
                    }
                    Singleton<BagView>.Instance.OpenView();
                    return;

                case 0x25:
                    Singleton<BaoxiangSystem>.Instance.OpenView();
                    return;

                case 0x29:
                    Singleton<DailyTaskView>.Instance.OpenView();
                    return;

                case 0x2a:
                    Singleton<EquipmentMainPanel>.Instance.OpenView();
                    return;
            }
            if (num == 2)
            {
                Singleton<GeneralPanel>.Instance.OpenView();
            }
        }

        private void OpenDungeon(Command cmd)
        {
            switch (cmd.param1)
            {
                case 1:
                    Singleton<DungeonChapterView>.Instance.OpenView();
                    break;

                case 2:
                    Singleton<DungeonInfoView>.Instance.Show(cmd.param2);
                    break;
            }
        }

        public int type
        {
            get
            {
                return 1;
            }
        }
    }
}

