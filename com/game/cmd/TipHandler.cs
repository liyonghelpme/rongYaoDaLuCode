﻿namespace com.game.cmd
{
    using com.game.manager;
    using com.game.Public.Message;
    using System;

    public class TipHandler : ICommandHandler
    {
        public void Handle(Command cmd)
        {
            if ((cmd.value != 0) && !string.IsNullOrEmpty(cmd.text))
            {
                MessageManager.Show(cmd.text);
            }
            else
            {
                MessageManager.Show(DescriptManager.GetText((uint) cmd.param1));
            }
        }

        public int type
        {
            get
            {
                return 2;
            }
        }
    }
}

