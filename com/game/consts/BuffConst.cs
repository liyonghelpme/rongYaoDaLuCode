﻿namespace com.game.consts
{
    using System;

    internal class BuffConst
    {
        public const int BUFF_EFFECT_DEBUFF = 1;
        public const int BUFF_EFFECT_NOTDEBUFF = 0;
        public const int BUFF_LOGIC_TYPE_CONTROL = 1;
        public const int BUFF_LOGIC_TYPE_NONE = 0;
        public const int BUFF_POS_BODY = 3;
        public const int BUFF_POS_FOOT = 1;
        public const int BUFF_POS_HEAD = 2;
        public const float ONCE_LOOP_TIME = 0.1f;
        public const uint OVERLYING_CANT_ADD = 0;
        public const uint OVERLYING_DUPLICATE = 1;
        public const uint OVERLYING_REPLACE = 2;
        public const int REQ_ADD = 3;
        public const int REQ_REMOVE = 4;
        public const int SYNC_ADD = 1;
        public const int SYNC_REMOVE = 2;
        public const uint TIME_DENOMINATOR = 0x3e8;
        public const uint TYPE_ATTR = 3;
        public const uint TYPE_ATTR_NORMAL = 1;
        public const uint TYPE_ATTR_VAL = 2;
        public const uint TYPE_CONTROL = 1;
        public const uint TYPE_CONTROL_ATTACK_DISTANCE = 12;
        public const uint TYPE_CONTROL_BATI = 13;
        public const uint TYPE_CONTROL_DAZY = 1;
        public const uint TYPE_CONTROL_FREEZING = 9;
        public const uint TYPE_CONTROL_GIVE_UP = 8;
        public const uint TYPE_CONTROL_GOD = 6;
        public const uint TYPE_CONTROL_HIT = 2;
        public const uint TYPE_CONTROL_LIFE_PROTECT = 11;
        public const uint TYPE_CONTROL_NO_MAGIC = 7;
        public const uint TYPE_CONTROL_SILENT = 4;
        public const uint TYPE_CONTROL_STAND = 5;
        public const uint TYPE_CONTROL_TAUNT = 3;
        public const uint TYPE_CONTROL_TEMPTATION = 10;
        public const uint TYPE_DAMAGE = 4;
        public const uint TYPE_DAMAGE_ABSOLUTE = 3;
        public const uint TYPE_DAMAGE_MAGIC = 2;
        public const uint TYPE_DAMAGE_PHYSIC = 1;
        public const uint TYPE_DAMAGE_VAL_ABSOLUTE = 4;
        public const uint TYPE_RECOVER = 5;
        public const uint TYPE_RECOVER_HP = 1;
        public const uint TYPE_RECOVER_MP = 2;
        public const uint TYPE_SHAPE = 2;
        public const uint TYPE_SHAPE_LONGQI = 2;
        public const uint TYPE_SHAPE_ORGIGE = 0;
        public const uint TYPE_SHAPE_SHEEP = 1;
    }
}

