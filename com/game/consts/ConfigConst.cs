﻿namespace com.game.consts
{
    using com.game.data;
    using com.game.module.core;
    using com.u3d.bases.debug;
    using System;
    using System.Collections.Generic;

    public class ConfigConst : Singleton<ConfigConst>
    {
        private Dictionary<string, string> configDatas = new Dictionary<string, string>();

        public int GetConfigData(string name)
        {
            int result = 0;
            if (this.configDatas.ContainsKey(name))
            {
                int.TryParse(this.configDatas[name], out result);
                return result;
            }
            Log.error(this, "配置项不存在:" + name);
            return result;
        }

        public List<int> GetConfigDatas(string name)
        {
            List<int> list = new List<int>();
            if (this.configDatas.ContainsKey(name))
            {
                string str = this.configDatas[name];
                char[] separator = new char[] { ',' };
                foreach (string str2 in str.Substring(1, str.Length - 2).Split(separator))
                {
                    list.Add(int.Parse(str2));
                }
                return list;
            }
            Log.error(this, "配置项不存在:" + name);
            return list;
        }

        public string GetConfigDataString(string name)
        {
            if (this.configDatas.ContainsKey(name))
            {
                return this.configDatas[name];
            }
            Log.error(this, "配置项不存在:" + name);
            return "[]";
        }

        public void SetConfigData(Dictionary<uint, object> data)
        {
            foreach (object obj2 in data.Values)
            {
                SysConfigVo vo = (SysConfigVo) obj2;
                this.configDatas.Add(vo.name, vo.value);
            }
        }
    }
}

