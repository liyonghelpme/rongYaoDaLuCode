﻿namespace com.game.consts
{
    using System;

    public class DungeonTaskConst
    {
        public const int DTT_GENERAL_DEATH = 2;
        public const int DTT_GENERAL_HP = 3;
        public const int DTT_KILL_MON = 1;
    }
}

