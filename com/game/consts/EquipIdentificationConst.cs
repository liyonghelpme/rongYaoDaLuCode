﻿namespace com.game.consts
{
    using System;

    public class EquipIdentificationConst
    {
        public const int COPYSOURCE_3 = 3;
        public const int EQUIPMAX_12 = 12;
        public const int PAGEMIN_1 = 1;
        public const int POOR_GRADES_10 = 10;
    }
}

