﻿namespace com.game.consts
{
    using System;

    public class FormulaConst
    {
        public const float BASE_CRIT_DMG_RATE = 1.5f;
        public const float BASE_CRIT_LEVEL_RATIO = 0.002f;
        public const float BASE_CRIT_RATE = 0.1f;
        public const float BASE_DODGE_RATE = 0f;
        public const float BASE_HIT_LEVEL_RATIO = 0.002f;
        public const float BASE_HIT_RATE = 0.9f;
        public const float BASE_MAG_HURT_RATIO = 0.15f;
        public const float BASE_PHY_HURT_RATIO = 0.15f;
        public const float CRIT_MAX_RATE = 1f;
        public const float CRIT_MIN_RATE = 0.1f;
        public const float HIT_MAX_RATE = 0.4f;
        public const float HIT_MIN_RATE = 0f;
        public const float MAG_HURT_RATIO = 1f;
        public const float MAX_ATT_MAG_PEN = 0.4f;
        public const float MAX_ATT_MAG_VAM = 0.4f;
        public const float MAX_ATT_PHY_PEN = 0.4f;
        public const float MAX_ATT_PHY_VAM = 0.4f;
        public const float MAX_ATT_SPEED_ADD = 0.4f;
        public const float MAX_CD_DEC = 0.4f;
        public const float MAX_CRIT_DMG_ADD = 0.5f;
        public const float MAX_DEF_MAG_PEN = 0.4f;
        public const float MAX_DEF_PHY_PEN = 0.4f;
        public const float MAX_DMG_BACK_MAG = 0.4f;
        public const float MAX_DMG_BACK_PHY = 0.4f;
        public const float MAX_DODGE_RATE = 0.4f;
        public const float MAX_FLEX_RATE = 1f;
        public const float MAX_HP_RECOVER = 0.4f;
        public const float MAX_MOVE_SPEED_ADD = 0.2f;
        public const float MAX_NORMAL_ATT_DMG = 0.3f;
        public const float PHY_HURT_RATIO = 1f;
        public const int RAND_MAX_NUM = 0x2710;
        public const int RAND_MIN_NUM = -10000;
        public const double RAND_RATE = 1E-06;
    }
}

