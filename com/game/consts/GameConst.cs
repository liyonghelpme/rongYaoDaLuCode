﻿namespace com.game.consts
{
    using System;

    public class GameConst
    {
        public const int AREA_DAMAGE_TYPE = 10;
        public const float ARRIVE_TARGET_GAP_DISTANCE = 0.3f;
        public const float ARRIVE_TARGET_GAP_FOR_BULLET = 0.5f;
        public const uint BOSS_DAMAGE_SHAKE = 3;
        public const int BULLET_CATAPULT_TYPE = 4;
        public const int BULLET_PENETRATE_TYPE = 3;
        public const int BULLET_STRIGHT_BOOM_TYPE = 2;
        public const int BULLET_TRACK_TYPE = 0;
        public const byte BUY_VIGOUR_BY_MONEY = 0;
        public const byte BUY_VIGOUR_BY_TOOL = 1;
        public const float CHECK_INTERVAL_WHEN_CHASE = 0.1f;
        public const float CHECK_NOT_MAX_TIME = 0.9f;
        public const int ConfirmMaskDepth = 0x3e8;
        public const uint CRITICAL_SHAKE = 1;
        public const uint CRITICAL_SHAKE_ID = 1;
        public const uint CritShakeKey = 0x2711;
        public const uint DAMAGE_SHAKE = 2;
        public const uint DefaultEffectVolumn = 100;
        public const uint DefaultSceneVolumn = 40;
        public const int EFFECT_TYPE_BUFF = 3;
        public const int EFFECT_TYPE_BULLET = 2;
        public const int EFFECT_TYPE_NORMAL = 1;
        public const uint EffectVolumnKey = 0x2715;
        public const string ExteriorWall = "ExteriorWall";
        public const int FEMALE = 0;
        public const string Floor = "Floor";
        public const int FreshCopyId = 0x986f71;
        public const uint HidePlayerKey = 0x2712;
        public const float HitPointEffectOffH = 0.45f;
        public const float HitPointRadius = 0.5f;
        public const string InteriorWall = "InteriorWall";
        public const float INTERVAL_SYNC_STATUS = 1f;
        public const int JOB_ASSIST = 4;
        public const int JOB_MAGIC_ATK = 3;
        public const int JOB_NONE = 0;
        public const int JOB_PHY_ATK = 1;
        public const int JOB_SHIELD = 2;
        public const float MagicBustX = -448.3f;
        public const int MALE = 1;
        public const ushort MaxAssistTimes = 5;
        public const ushort MaxRobTimes = 2;
        public const string MeDisplay = "MeDisplay";
        public const int MONEY_BINGING_DIAMOND = 2;
        public const int MONEY_DIAM = 3;
        public const int MONEY_DIAMOND = 1;
        public const string MonsterDisplay = "MonsterDisplay";
        public const uint MuteKey = 0x2713;
        public const uint No_SHAKE = 0;
        public const float PercentUnit = 0.0001f;
        public const int PIXEL_TO_UNITY = 80;
        public const string PlayerDisplay = "PlayerDisplay";
        public const float PositionToProtocal = 1000f;
        public const float PositionUnit = 0.001f;
        public const double PROB_FULL_D = 10000.0;
        public const int PROB_FULL_I = 0x2710;
        public const char REGX_1 = '|';
        public const char REGX_2 = ',';
        public const char REGX_3 = '@';
        public const char REGX_4 = '#';
        public const char REGX_5 = ';';
        public const int ROLE_SUBTYPE_ME = 1;
        public const int ROLE_SUBTYPE_PLAYER = 2;
        public const uint SceneVolumnKey = 0x2714;
        public const int SELF_CENTER_BULLET_TYPE = 12;
        public const int SHIP_SKILL_TYPE = 13;
        public const uint SkillPos1 = 0x32c9;
        public const uint SkillPos2 = 0x32ca;
        public const uint SkillPos3 = 0x32cb;
        public const uint SkillPos4 = 0x32cc;
        public const uint SkillPos5 = 0x32cd;
        public const uint SystemNoticeId = 1;
        public const int TEAM_SKILL_TYPE = 20;
        public const int THROW_STONE_TYPE = 11;
        public const int TIANHUI_BASE_TEMPLATE_ID = 0x3e9008;
        public const int TIANHUI_TOWER_TEMPLATE_ID = 0xf4259;
        public const string Tree = "Tree";
        public const uint USE_SKILL_SHAKE = 4;
        public const int YEYAN_BASE_TEMPLATE_ID = 0x3e9009;
        public const int YEYAN_TOWER_TEMPLATE_ID = 0xf425a;

        public enum AttrType
        {
            Float = 3,
            Integer = 2,
            Percentage = 1
        }

        public enum CampType : uint
        {
            enemyside = 1,
            myside = 0,
            pve_monster = 100,
            pvp_3rd_party_monster = 0x65,
            unavailable = 0x3e7
        }
    }
}

