﻿namespace com.game.consts
{
    using System;

    public class GoodsConst
    {
        public const int ATTR_ID_AGI = 2;
        public const int ATTR_ID_ATT_DEF_M = 12;
        public const int ATTR_ID_ATT_DEF_P = 11;
        public const int ATTR_ID_ATT_M_MAX = 10;
        public const int ATTR_ID_ATT_M_MIN = 9;
        public const int ATTR_ID_ATT_MAX = 0x16;
        public const int ATTR_ID_ATT_MIN = 0x15;
        public const int ATTR_ID_ATT_P_MAX = 8;
        public const int ATTR_ID_ATT_P_MIN = 7;
        public const int ATTR_ID_CRIT = 15;
        public const int ATTR_ID_CRIT_RATIO = 0x10;
        public const int ATTR_ID_DODGE = 14;
        public const int ATTR_ID_FLEX = 0x11;
        public const int ATTR_ID_HIT = 13;
        public const int ATTR_ID_HP = 5;
        public const int ATTR_ID_HURT_RE = 0x12;
        public const int ATTR_ID_LUCK = 20;
        public const int ATTR_ID_MP = 6;
        public const int ATTR_ID_PHY = 3;
        public const int ATTR_ID_SPEED = 0x13;
        public const int ATTR_ID_STR = 1;
        public const int ATTR_ID_WIT = 4;
        public const int BLUE = 3;
        public const int GREEN = 2;
        public const int ORANGE = 5;
        public const int PET_EQUIP = 3;
        public const int PET_GOODS = 5;
        public const int POS_BAGGES = 10;
        public const int POS_BRACELET = 7;
        public const int POS_CLOTHE = 3;
        public const int POS_GLOVE = 9;
        public const int POS_HELMET = 2;
        public const int POS_NECKLACE = 6;
        public const int POS_RING = 8;
        public const int POS_SHOE = 5;
        public const int POS_TROUSERS = 4;
        public const int POS_WEAPON = 1;
        public const int PURPLE = 4;
        public const int RED = 6;
        public const int ROLE_EQUIP = 0;
        public const int SMELT_GOODS = 3;
        public const int SUBTYPE_COMMON = 0;
        public const int TYPE_COMMON = 0;
        public const int TYPE_PET = 3;
        public const int WHITE = 1;
    }
}

