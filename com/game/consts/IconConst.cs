﻿namespace com.game.consts
{
    using System;
    using System.Runtime.InteropServices;

    public class IconConst
    {
        public const string diamond_big = "zuanshi";
        public const string diamond_big_binding = "zhuanshi";
        public const string diamond_normal = "zuanshi-xiao";
        public const string diamond_normal_binding = "zhuanshi-xiao";
        public const string diamond_small = "zhuanshi-xiao";
        public const string diamond_small_binding = "zhuanshi-xiao";
        public const string gold_big = "jinbi";
        public const string gold_big_binding = "jinbi";
        public const string gold_normal = "jinbi-xiao";
        public const string gold_normal_binding = "jinbi-xiao";
        public const string gold_small = "jinbi-xiao";
        public const string gold_small_binding = "jinbi-xiao";
        public const string spirit_big = "shandian";
        public const string spirit_big_binding = "shandian";
        public const string spirit_normal = "shandian-xiao";
        public const string spirit_normal_binding = "shandian-xiao";
        public const string spirit_small = "shandian-xiao";
        public const string spirit_small_binding = "shandian-xiao";
        public const string star_big = "xingxing-da";
        public const string star_normal = "xingxing-xiao";
        public const string star_small = "xingxing-yingxiongdengji";

        public static void GetGoldIcon(IconType type, bool isBinding, out string IconName)
        {
            IconName = string.Empty;
            switch (type)
            {
                case IconType.Normal:
                    IconName = !isBinding ? "jinbi-xiao" : "jinbi-xiao";
                    break;

                case IconType.Big:
                    IconName = !isBinding ? "jinbi" : "jinbi";
                    break;

                case IconType.Small:
                    IconName = !isBinding ? "jinbi-xiao" : "jinbi-xiao";
                    break;
            }
        }

        public enum IconType
        {
            Big = 2,
            Normal = 1,
            Small = 3
        }
    }
}

