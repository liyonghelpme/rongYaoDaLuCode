﻿namespace com.game.consts
{
    using System;

    public class MapTypeConst
    {
        public const uint ARENA_MAP = 0xea61;
        public const int CITY_MAP = 1;
        public const int CLUB_CAMPSITE = 4;
        public const int COPY_MAP = 2;
        public const int COPY_POINT = 2;
        public const int DAEMONISLAND_COPY = 7;
        public const int FIRST_BATTLE_COPY = 8;
        public const uint FirstCopy = 0x5209;
        public const uint FirstFightMap = 0x2713;
        public const uint GoldHit_MAP = 0x9c42;
        public const uint GoldSilverIsland_MAP = 0xea62;
        public const int MAIN_COPY = 1;
        public const uint MajorCity = 0x1869e;
        public const int MAP_POINT = 3;
        public const uint MAPID_DUNGEON_GIANT = 0x30d41;
        public const byte Role_BUFF_LIST_CHANGE_KEY = 6;
        public const byte ROLE_HP_CHANGE_KEY = 1;
        public const byte ROLE_HP_FULL_CHANGE_KEY = 2;
        public const byte ROLE_LV_CHANGE_KEY = 5;
        public const byte ROLE_MP_CHANGE_KEY = 3;
        public const byte ROLE_MP_FULL_CHANGE_KEY = 4;
        public const byte ROLE_REVIVE_NO_MONEY = 4;
        public const byte ROLE_REVIVE_RETURN_CITY = 3;
        public const byte ROLE_REVIVE_USE_DIAM = 1;
        public const byte ROLE_REVIVE_USE_ITEM = 2;
        public const byte RolePetChange = 7;
        public const int SPECIAL_MAP = 3;
        public const uint WORLD_BOSS = 0x9c41;
        public const int WORLDMAP_POINT = 1;
    }
}

