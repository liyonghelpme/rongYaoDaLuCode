﻿namespace com.game.consts
{
    using System;

    public class ModuleConst
    {
        public const int ACHIEVE = 0x2b;
        public const int BAG = 0x24;
        public const int BAOXIANG = 0x25;
        public const int BUFF = 0x23;
        public const int CHAT = 10;
        public const int DAILY_TASK = 0x29;
        public const int DUNGEON = 0x17;
        public const int EQUIPMENT = 0x2a;
        public const int GENERAL = 2;
        public const int GUIDE = 40;
        public const int LOGIN = 1;
        public const int MAIL = 12;
        public const int MAP = 4;
        public const int RANK = 0x15;
        public const int ROLE = 3;
        public const int SHOP = 15;
        public const int SKILL = 13;
        public const int SKILL_TALENT = 0x13;
        public const int SYSTEM_SETTING = 8;
        public const int WIFI_PVP = 14;
    }
}

