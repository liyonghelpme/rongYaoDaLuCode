﻿namespace com.game.consts
{
    using System;

    public class MonsterCategory
    {
        public const int friend = 6;
        public const int normalMonster = 1;
        public const int pvp_base = 5;
        public const int pvp_private = 4;
        public const int pvp_tower = 3;
        public const int staticBox = 2;
    }
}

