﻿namespace com.game.consts
{
    using System;

    public class MonsterQuality
    {
        public const int PrivateNormal = 1;
        public const int TowerNormal = 1;
        public const int TypeBoss = 3;
        public const int TypeElite = 2;
        public const int TypeMonster = 1;
    }
}

