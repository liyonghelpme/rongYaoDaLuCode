﻿namespace com.game.consts
{
    using System;

    public class MonsterType
    {
        public const int PrivateNormal = 0xc9;
        public const int TowerNormal = 0x65;
        public const int TypeBoss = 3;
        public const int TypeElite = 2;
        public const int TypeMonster = 1;
    }
}

