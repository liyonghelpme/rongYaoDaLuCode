﻿namespace com.game.consts
{
    using System;

    public class PRTypeConst
    {
        public const int MT_MODULE = 3;
        public const int MT_SCENE = 1;
        public const int MT_TASK = 2;
        public const int ST_ICON = 4;
        public const int ST_MODULE = 2;
        public const int ST_MUL = 3;
        public const int ST_SKILL = 1;
        public const int ST_SOUND = 5;
    }
}

