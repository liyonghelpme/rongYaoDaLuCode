﻿namespace com.game.consts
{
    using System;

    public enum PanelDepth
    {
        LoadingView = 400,
        RoleView = 200
    }
}

