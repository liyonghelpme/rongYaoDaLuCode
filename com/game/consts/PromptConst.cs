﻿namespace com.game.consts
{
    using System;

    public class PromptConst
    {
        public const string BUY_VIGOUR_TIPS = "购买体力需要消耗XXX钻石";
        public const string VIGOUR_NOT_ENOUGH = "体力不足，请购买体力再进入";
    }
}

