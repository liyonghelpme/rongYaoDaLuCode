﻿namespace com.game.consts
{
    using System;

    public class SortingLayerType
    {
        public const string BACKGROUND_LAYER = "Background";
        public const string Default = "Default";
        public const string FOREGROUND = "Foreground";
        public const string ROAD_BACKGROUND_LAYER = "RoadBackground";
        public const string ROAD_FOR_ROUND_LAYER = "RoadgroundUp";
        public const string ROAD_GROUND = "Roadground";
    }
}

