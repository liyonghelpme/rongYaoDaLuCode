﻿namespace com.game.consts
{
    using System;

    public class StoreShopConst
    {
        public static int BUY_MAX = 0x63;
        public static int INPUT_NUMBER = 10;
        public static int ITEMNUMBER_9 = 9;
        public static int LIMITTIME_3 = 3;

        public enum GoodType
        {
            Hot,
            Diamond,
            BindingDiamond,
            Gold,
            Limit
        }
    }
}

