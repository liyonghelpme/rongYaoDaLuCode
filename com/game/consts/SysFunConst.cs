﻿namespace com.game.consts
{
    using System;

    public class SysFunConst
    {
        public const short SF_10 = 10;
        public const short SF_11 = 11;
        public const short SF_12 = 12;
        public const short SF_13 = 13;
        public const short SF_14 = 14;
        public const short SF_15 = 15;
        public const short SF_16 = 0x10;
        public const short SF_17 = 0x11;
        public const short SF_18 = 0x12;
        public const short SF_19 = 0x13;
        public const short SF_20 = 20;
        public const short SF_21 = 0x15;
        public const short SF_22 = 0x16;
    }
}

