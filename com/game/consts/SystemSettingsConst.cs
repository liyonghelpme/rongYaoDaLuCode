﻿namespace com.game.consts
{
    using System;

    internal class SystemSettingsConst
    {
        public const byte BG_MUSIC_KEY = 250;
        public const byte DefaultParticleQuality = 100;
        public const byte DefaultScreenQuality = 0;
        public const byte EFFECT_SOUND_KEY = 0xfb;
        public const byte PARTICLE_QUALITY_KEY = 0xfd;
        public const byte SCREEN_QUALITY_KEY = 0xfc;
    }
}

