﻿namespace com.game.consts
{
    using System;

    public class UrlConst
    {
        public const string EFFECT_ADDHP = "effect/other/addhp/addhp";
        public const string EFFECT_ADDMP = "effect/other/addmp/addmp";
        public const string EFFECT_HIT = "effect/hit/hit";
        public const string EFFECT_LEVELUP = "effect/other/levelup/levelup";
        public const string UI_ROOT = "zh_CN/prefab/ui/UIRoot";
    }
}

