﻿namespace com.game.consts
{
    using System;

    public class VIPDroitConst
    {
        public const int BUY_ENTER_DUNGEON = 9;
        public const int NUM_BUY_ARENAL_LIMIT = 6;
        public const int NUM_BUY_GOLD = 3;
        public const int NUM_BUY_SKILL_POINT_LIMIT = 5;
        public const int NUM_BUY_VIRGOUR_LIMIT = 1;
        public const int SWEEP = 7;
        public const int SWEEP10 = 8;
    }
}

