﻿namespace com.game.consts
{
    using System;

    public class WifiPvpConst
    {
        public const int BROADCAST_PORT_MAX = 0x23b9;
        public const int BROADCAST_PORT_MIN = 0x23b2;
        public const int DELAY_AFTER_INVITATION = 9;
        public const int DELAY_FOR_CONFIRM = 10;
        public const float DELAY_FOR_SYNC_TOWER_BASE_TO_OTHER = 0.25f;
        public const int FORCE_SYNC_POS_COUNTER = 3;
        public const int MAX_HANDLING_NUM = 30;
        public const int PacketID = 0x7f;
        public const int PVP_PORT_MAX = 0xea60;
        public const int PVP_PORT_MIN = 0x4e20;
        public const float REFRESH_LATENCY = 3f;
        public const long SCAN_TIME = 0x9c4L;
        public const char SEPERATOR_ACC_GENERAL = '*';
        public const char SEPERATOR_BETWEEN_GENERALS = '$';
        public const char SEPERATOR_FIELDS = '#';
        public const char SEPERATOR_UNIQUE_KEY = '@';
        public static readonly string WAIT_PROMPT = LanguageManager.GetWord("WifiPvp.TipsWaiting");

        public enum MsgType : byte
        {
            C_CONNECTION_OK = 10,
            C_LOADING_OK = 50,
            C_REQUEST_ENTER = 12,
            GIVE_UP = 90,
            INFORM_END_PVP = 80,
            PROTOCAL = 100,
            S_ACK_ENTER = 13,
            S_LOADING_OK = 0x33,
            S_TO_C_BASIC_INFO = 11,
            TEST_MESSAGE = 0
        }

        public enum PVPEndState : byte
        {
            abort = 2,
            confirming = 0x7e,
            ended = 100,
            fail = 0,
            started = 0x7f,
            win = 1
        }

        public enum Sender : byte
        {
            ACTUAL_SERVER = 2,
            SGAME_CLIENT = 0,
            SGAME_SERVER = 1
        }

        public enum Signal : byte
        {
            acceptable = 2,
            good = 3,
            none = 0,
            unset = 0x7f,
            weak = 1
        }
    }
}

