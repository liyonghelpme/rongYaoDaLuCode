﻿namespace com.game.data
{
    using com.game;
    using com.game.basic;
    using com.game.manager;
    using com.game.module.General;
    using com.game.module.SystemData;
    using com.game.vo;
    using System;

    public class GlobalData
    {
        private static bool _isPlayingCamera;
        private static int _loginTime;
        private static uint _sign;
        public static GeneralMode generalInfo = Singleton<GeneralMode>.Instance;
        public static MeVo SelfPlayerInfo = MeVo.instance;

        public static uint GetGeneralUniKey(int generalTemplateId, byte quality)
        {
            return (uint) ((generalTemplateId * 100) + quality);
        }

        public static void SetSign(uint v)
        {
            _sign = v;
            _loginTime = ServerTime.Instance.Timestamp;
        }

        public static uint CurrentMapId
        {
            get
            {
                return SelfPlayerInfo.mapId;
            }
        }

        public static bool isInCopy
        {
            get
            {
                if (AppMap.Instance.IsEditor)
                {
                    return true;
                }
                SysSceneVo sceneVo = BaseDataMgr.instance.GetSceneVo(CurrentMapId);
                return ((sceneVo.type != 1) && (sceneVo.type != 4));
            }
        }

        public static bool isInHomeCity
        {
            get
            {
                return (AppMap.Instance.IsEditor || (BaseDataMgr.instance.GetSceneVo(CurrentMapId).type == 1));
            }
        }

        public static bool IsPlayingCamera
        {
            get
            {
                return _isPlayingCamera;
            }
            set
            {
                if (_isPlayingCamera != value)
                {
                    _isPlayingCamera = value;
                    GlobalAPI.facade.Notify(!_isPlayingCamera ? 0x18 : 0x17, 0, 0, null);
                }
            }
        }

        public static int loginTime
        {
            get
            {
                return _loginTime;
            }
        }

        public static uint sign
        {
            get
            {
                return _sign;
            }
        }
    }
}

