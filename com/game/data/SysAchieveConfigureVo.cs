﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysAchieveConfigureVo
    {
        public uint id;
        public string name;
        public int type;
        public uint unikey;
    }
}

