﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysAchievementVo
    {
        public int achievementpoint;
        public string award;
        public int count;
        public int data;
        public string descript;
        public int icon;
        public uint id;
        public int level;
        public string name;
        public int next;
        public int subtype;
        public int type;
        public uint unikey;
        public int value1;
    }
}

