﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysAnnouncementVo
    {
        public string descript;
        public uint id;
        public string name;
        public string title;
        public uint unikey;
    }
}

