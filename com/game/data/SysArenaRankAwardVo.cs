﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysArenaRankAwardVo
    {
        public string award_list;
        public int pos_end;
        public int pos_start;
        public uint unikey;
    }
}

