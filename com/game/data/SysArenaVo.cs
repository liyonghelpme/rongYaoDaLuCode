﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysArenaVo
    {
        public int arena_id;
        public int battle_mode;
        public string challenger_pos;
        public int counting_down;
        public string defender_pos;
        public int map_id;
        public int total_time;
        public uint unikey;
    }
}

