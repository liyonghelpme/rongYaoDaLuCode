﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysBattleObjectVo
    {
        public string buff_list;
        public uint id;
        public int model_id;
        public int type;
        public uint unikey;
    }
}

