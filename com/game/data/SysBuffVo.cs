﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysBuffVo
    {
        public string attackedAddBuffList;
        public string attrArray;
        public int buffEffectId;
        public string desc;
        public int effectPosition;
        public int firstEffectTime;
        public int id;
        public int initEffectId;
        public int intervalTime;
        public int isDebuff;
        public int isNotHoldOn;
        public int level;
        public int logic_type;
        public string name;
        public int overlying;
        public int removeEffectId;
        public int show;
        public int subtype;
        public int totalTime;
        public int type;
        public uint unikey;
        public int value1;
        public int value2;
        public int value3;
    }
}

