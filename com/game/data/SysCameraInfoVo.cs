﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysCameraInfoVo
    {
        public string cameraDir;
        public string cameraPos;
        public int check_dist;
        public string check_pos;
        public uint id;
        public string offset_pos;
        public int time;
        public int type;
        public uint unikey;
        public int view;
    }
}

