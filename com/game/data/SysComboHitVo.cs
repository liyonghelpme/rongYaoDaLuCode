﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysComboHitVo
    {
        public int combo_count;
        public int id;
        public int total_add_energy;
        public uint unikey;
    }
}

