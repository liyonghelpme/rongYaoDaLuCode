﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysCompose
    {
        public int charge;
        public uint frag_num;
        public uint frag_tid;
        public uint material_tid;
        public uint unikey;
    }
}

