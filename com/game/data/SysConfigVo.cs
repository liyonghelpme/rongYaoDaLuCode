﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysConfigVo
    {
        public string name;
        public int num;
        public uint unikey;
        public string value;
    }
}

