﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDailyTaskVo
    {
        public int count;
        public string desc;
        public int lvl;
        public string name;
        public string pos;
        public string reward;
        public int src;
        public int type;
        public uint unikey;
    }
}

