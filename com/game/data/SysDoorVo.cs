﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDoorVo
    {
        public string door_coord;
        public int id;
        public string map_pos;
        public int next_map;
        public int type;
        public uint unikey;
    }
}

