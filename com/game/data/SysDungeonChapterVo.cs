﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonChapterVo
    {
        public string dungeon_list;
        public string dungeon_list_point;
        public int id;
        public int max_level;
        public int min_level;
        public string name;
        public int next_chapter;
        public int prev_chapter;
        public string res_id_list;
        public string star_award_list;
        public int type;
        public uint unikey;
    }
}

