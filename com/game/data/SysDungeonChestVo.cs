﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonChestVo
    {
        public string award_list;
        public float deg;
        public int exp;
        public uint id;
        public float radius;
        public uint unikey;
        public float x;
        public float y;
        public float z;
    }
}

