﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonHelpVo
    {
        public string data_1;
        public string data_2;
        public string data_3;
        public int dun_type;
        public string goods;
        public string help;
        public int id;
        public string name;
        public string target;
        public int time;
        public uint unikey;
    }
}

