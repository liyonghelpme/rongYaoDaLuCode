﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonMissionVo
    {
        public string chest_list;
        public string descript;
        public string dungeon_task;
        public int fight_type;
        public string frozen_points;
        public int id;
        public string initial_coord;
        public int low_hp_warning;
        public string monster_ai;
        public string monster_list;
        public string name;
        public int need_time;
        public string role_ai;
        public int scene_id;
        public int succ_task;
        public uint unikey;
    }
}

