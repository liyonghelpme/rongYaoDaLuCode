﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonMon
    {
        public string list;
        public int map_id;
        public int phase;
        public uint unikey;
    }
}

