﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonMonsterVo
    {
        public int act_distance;
        public string act_type;
        public int acthurt_max;
        public int acthurt_min;
        public string ai_list;
        public bool can_attacked;
        public int id;
        public bool is_boss;
        public int level;
        public int max_hp;
        public int model_id;
        public string monster_attrs;
        public int move_speed;
        public string name;
        public int reborn_time;
        public int return_distance;
        public string skill_list;
        public int targethurt_max;
        public int targethurt_min;
        public int type;
        public uint unikey;
    }
}

