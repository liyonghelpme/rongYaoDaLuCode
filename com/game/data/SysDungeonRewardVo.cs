﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonRewardVo
    {
        public string attack;
        public string ext_goods;
        public int fix_diam;
        public int fix_gold;
        public int fix_soul;
        public string goods1;
        public string goods2;
        public string goods3;
        public string goods4;
        public int map_id;
        public string time;
        public uint unikey;
    }
}

