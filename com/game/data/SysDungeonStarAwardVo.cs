﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonStarAwardVo
    {
        public string award;
        public int chapter_id;
        public int count_id;
        public int star;
        public uint unikey;
    }
}

