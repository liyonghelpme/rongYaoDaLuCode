﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonTaskVo
    {
        public string condition;
        public string content;
        public uint id;
        public bool show_state;
        public int type;
        public uint unikey;
    }
}

