﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonTreeVo
    {
        public string anim;
        public string attrAdd;
        public string boss_icon;
        public string icon;
        public int id;
        public string list;
        public string monster_icon;
        public string name;
        public int parentId;
        public string remark;
        public int type;
        public uint unikey;
        public int x;
        public int y;
    }
}

