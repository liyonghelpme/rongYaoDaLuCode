﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysDungeonVo
    {
        public int award_exp;
        public int award_gold;
        public string award_list;
        public int chapter_id;
        public int day_times;
        public string descript;
        public int dungeon_cd;
        public int endDialog;
        public string enemy_info;
        public int icon_type;
        public int id;
        public int max_level;
        public int min_level;
        public string mission_list;
        public string name;
        public int need_time;
        public int need_vigour;
        public int next_id;
        public int pre_id;
        public int pre_vigour;
        public int subtype;
        public string sweep_extra_award;
        public int type;
        public uint unikey;
    }
}

