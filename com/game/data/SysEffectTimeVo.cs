﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysEffectTimeVo
    {
        public int duration;
        public uint id;
        public string mount_path;
        public int offset_x;
        public int offset_y;
        public int offset_z;
        public uint unikey;
    }
}

