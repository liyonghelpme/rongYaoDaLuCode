﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysEquipUp
    {
        public int charge;
        public string material_list;
        public uint new_tid;
        public uint old_tid;
        public uint unikey;
    }
}

