﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysEquipUpVo
    {
        public int charge;
        public string material_list;
        public uint new_tid;
        public uint old_tid;
        public uint unikey;
    }
}

