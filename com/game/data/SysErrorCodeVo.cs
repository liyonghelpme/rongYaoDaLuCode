﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysErrorCodeVo
    {
        public string desc;
        public int id;
        public uint unikey;
    }
}

