﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysGemComposeVo
    {
        public uint charge;
        public uint des_tid;
        public uint ori_tid;
        public uint unikey;
    }
}

