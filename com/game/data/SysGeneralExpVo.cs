﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysGeneralExpVo
    {
        public int exp_level;
        public int level;
        public int total_exp;
        public uint unikey;
    }
}

