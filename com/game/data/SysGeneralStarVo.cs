﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysGeneralStarVo
    {
        public int count;
        public int id;
        public int item_id;
        public int star;
        public string star_attr;
        public uint unikey;
    }
}

