﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysGeneralVo
    {
        public int ai_sight_range;
        public string attrs;
        public int big_icon_id;
        public int career;
        public string default_equip_id;
        public int general_id;
        public int icon_id;
        public int id;
        public bool is_display;
        public int model_id;
        public string name;
        public int pivot_localposition_y;
        public int quality;
        public int skill_chase_range;
        public string skill_list;
        public uint unikey;
    }
}

