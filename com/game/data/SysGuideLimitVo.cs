﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysGuideLimitVo
    {
        public int ID;
        public int lvl;
        public uint unikey;
    }
}

