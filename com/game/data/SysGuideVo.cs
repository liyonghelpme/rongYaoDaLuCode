﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysGuideVo
    {
        public string condition;
        public string guide_describe;
        public int guide_type;
        public int guideID;
        public int trigger_type;
        public uint unikey;
    }
}

