﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysItemsVo
    {
        public bool can_sell;
        public bool can_superpose;
        public int color;
        public string configure;
        public string desc;
        public int icon;
        public int id;
        public int lvl;
        public string name;
        public string property;
        public int sell_price;
        public int subtype;
        public int tagtype;
        public int type;
        public uint unikey;
        public string use_tips;
        public int use_type;
        public int value;
        public int value1;
        public int value2;
        public int value3;
        public int value4;
    }
}

