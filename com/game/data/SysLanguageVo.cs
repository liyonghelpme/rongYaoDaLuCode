﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysLanguageVo
    {
        public string text;
        public int type;
        public uint unikey;
    }
}

