﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysLogDataVo
    {
        public int data;
        public int param;
        public int type;
        public uint unikey;
    }
}

