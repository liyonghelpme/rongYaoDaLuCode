﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMapVo
    {
        public bool adapt;
        public string ai;
        public string bg_sound_effect;
        public string bgMusic;
        public bool can_revive;
        public string enter_condition;
        public int enter_count;
        public int group;
        public int icon_id;
        public int id;
        public int lvl;
        public string monster_words;
        public string name;
        public bool need_synchronization;
        public string npcList;
        public string open_condition;
        public int phase;
        public int pk_mode;
        public int resource_id;
        public int revive;
        public int revive_cost;
        public string scene_pos;
        public bool skill_limited;
        public int subtype;
        public int team_num_min;
        public string trans_pos;
        public string trapList;
        public int type;
        public uint unikey;
        public int vigour;
        public string world_trans_pos;
    }
}

