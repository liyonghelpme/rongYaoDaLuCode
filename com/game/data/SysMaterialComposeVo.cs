﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMaterialComposeVo
    {
        public int charge;
        public int frag_num;
        public uint frag_tid;
        public uint material_tid;
        public uint unikey;
    }
}

