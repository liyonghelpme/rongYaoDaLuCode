﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysModelVo
    {
        public int boundDistance;
        public uint id;
        public string scale;
        public int shadowZoomRate;
        public uint unikey;
    }
}

