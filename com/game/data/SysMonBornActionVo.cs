﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMonBornActionVo
    {
        public int animation;
        public string end_pos;
        public uint id;
        public string start_pos;
        public int type;
        public uint unikey;
    }
}

