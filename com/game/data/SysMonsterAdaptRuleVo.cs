﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMonsterAdaptRuleVo
    {
        public int att_m_max_ratio;
        public int att_m_min_ratio;
        public int att_p_max_ratio;
        public int att_p_min_ratio;
        public int crit_hurt_ratio;
        public int crit_lvl_ratio;
        public int dark_attr_ratio;
        public int dark_def_ratio;
        public int def_m_ratio;
        public int def_p_ratio;
        public int dodge_ratio;
        public int exp_ratio;
        public int fir_attr_ratio;
        public int fir_def_ratio;
        public int flex_ratio;
        public int gold_ratio;
        public int hit_ratio;
        public int hp_ratio;
        public int hurt_re_ratio;
        public int ice_attr_ratio;
        public int ice_def_ratio;
        public int light_attr_ratio;
        public int light_def_ratio;
        public int lvl;
        public int thunder_attr_ratio;
        public int thunder_def_ratio;
        public uint unikey;
    }
}

