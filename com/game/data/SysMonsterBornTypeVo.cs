﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMonsterBornTypeVo
    {
        public uint id;
        public string param1;
        public string param2;
        public int type;
        public uint unikey;
    }
}

