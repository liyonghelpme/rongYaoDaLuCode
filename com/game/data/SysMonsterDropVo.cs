﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMonsterDropVo
    {
        public int amount;
        public int id;
        public bool is_bind;
        public int item_id;
        public int monster_id;
        public int rate;
        public int type;
        public uint unikey;
    }
}

