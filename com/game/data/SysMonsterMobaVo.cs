﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMonsterMobaVo
    {
        public int id;
        public int lvl;
        public string mon_props;
        public uint unikey;
    }
}

