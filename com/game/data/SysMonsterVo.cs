﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysMonsterVo
    {
        public string action_script;
        public bool active;
        public string ai_list;
        public int anti_knock_back;
        public int anti_knock_down;
        public int attack_range;
        public int attackAccuracy;
        public int attackMovePro;
        public string blocked_skill_subtype;
        public int born_action;
        public int born_camera;
        public int born_show;
        public int born_show_time;
        public int born_type;
        public int camp;
        public int collider_radius;
        public int cruise_range;
        public int damage_cal;
        public int dead_camera;
        public int dead_model_keep_time;
        public string death_speak;
        public bool enable_hurt_fly;
        public int endurance_initval;
        public int endurance_threshold;
        public string fight_speak;
        public int hp_count;
        public int hurt_down_resist;
        public int hurt_resist;
        public int icon;
        public int id;
        public string idle_speak;
        public bool is_cruise_enable;
        public int lvl;
        public int max_range;
        public string mon_props;
        public string mon_rand_skills;
        public int move_ratio;
        public string name;
        public int pivot_localposition_y;
        public int quality;
        public int res;
        public int resType;
        public string scale;
        public int searchStop;
        public int sex;
        public int sight_range;
        public string skill_ids;
        public int sound_dead;
        public int speed;
        public int track_range;
        public int type;
        public int unconvent_skill;
        public uint unikey;
    }
}

