﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysNpcVo
    {
        public string descript;
        public int mapid;
        public int model_id;
        public string moduleId;
        public string name;
        public int npcId;
        public string position;
        public int speak_cd;
        public int speak_rate;
        public uint unikey;
        public int yRotation;
    }
}

