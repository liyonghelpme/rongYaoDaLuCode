﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysPriceVo
    {
        public int count;
        public string description;
        public int diam;
        public int gold;
        public int money;
        public int type;
        public uint unikey;
    }
}

