﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysPvpPrivateDelayVo
    {
        public string delay_list;
        public string description;
        public uint id;
        public uint unikey;
    }
}

