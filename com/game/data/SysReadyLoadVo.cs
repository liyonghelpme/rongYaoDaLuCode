﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysReadyLoadVo
    {
        public int id;
        public int job;
        public string mainid;
        public int maintype;
        public int priority;
        public string subid;
        public int subtype;
        public uint unikey;
    }
}

