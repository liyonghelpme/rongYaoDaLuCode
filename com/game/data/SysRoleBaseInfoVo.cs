﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysRoleBaseInfoVo
    {
        public int JobId;
        public string Model;
        public string Name;
        public string SkillList;
        public uint unikey;
    }
}

