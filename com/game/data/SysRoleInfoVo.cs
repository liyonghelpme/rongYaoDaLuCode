﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysRoleInfoVo
    {
        public int exp_level;
        public int general_lvl;
        public int level;
        public int re_vigour;
        public uint unikey;
        public int vigour;
    }
}

