﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysRumor
    {
        public string format;
        public int id;
        public int type;
        public uint unikey;
    }
}

