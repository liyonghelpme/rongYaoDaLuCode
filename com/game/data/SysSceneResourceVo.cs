﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysSceneResourceVo
    {
        public int find_path;
        public int fog_id;
        public string lightmap_id;
        public int resource_id;
        public bool scene_light;
        public string sky_box;
        public uint unikey;
    }
}

