﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysSceneVo
    {
        public string bgMusic;
        public string cameraDir;
        public string cameraPos;
        public bool can_pause;
        public string default_pos;
        public string door_list;
        public bool full_broadcast;
        public int id;
        public string map_bottom_left_pos;
        public string map_bottom_right_pos;
        public string map_top_left_pos;
        public string map_top_right_pos;
        public int mon_script;
        public string name;
        public bool need_sync;
        public string npcId;
        public int resource_id;
        public int subtype;
        public int type;
        public uint unikey;
    }
}

