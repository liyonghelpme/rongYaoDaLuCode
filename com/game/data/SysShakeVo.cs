﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysShakeVo
    {
        public uint id;
        public string keys;
        public uint priority;
        public uint type;
        public uint unikey;
    }
}

