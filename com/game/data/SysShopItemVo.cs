﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysShopItemVo
    {
        public uint item_id;
        public int limit_count;
        public int max_level;
        public int min_level;
        public int pay_type;
        public uint price;
        public int shop_type_id;
        public uint unikey;
    }
}

