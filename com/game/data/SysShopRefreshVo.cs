﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysShopRefreshVo
    {
        public uint count;
        public uint id;
        public int pay_count;
        public int pay_type;
        public uint unikey;
    }
}

