﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysShopVo
    {
        public int manualUpdate;
        public string name;
        public int payMax;
        public int showMoney;
        public uint unikey;
    }
}

