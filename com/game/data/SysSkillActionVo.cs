﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysSkillActionVo
    {
        public int action_id;
        public int bullet_speed;
        public int BulletTravelDistance;
        public int BulletType;
        public int CheckInterval;
        public int DetectTime;
        public string environ_eff;
        public int id;
        public bool IsBullet;
        public string mount_eff;
        public string name;
        public string process_eff;
        public string tar_eff;
        public uint unikey;
        public string use_eff;
    }
}

