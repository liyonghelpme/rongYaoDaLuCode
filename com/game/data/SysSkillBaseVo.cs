﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysSkillBaseVo
    {
        public bool active;
        public int att_type;
        public int available_atk_range;
        public int back_dis;
        public int beiley;
        public string buff_list;
        public string buff_list_to_self_after_damage;
        public string buff_list_when_use_skill;
        public bool can_break;
        public bool can_move_step;
        public int cd;
        public int cd_public;
        public int color;
        public int data_fixed;
        public int data_per;
        public string desc;
        public int dungeon_cd;
        public int effect_shape;
        public bool hurt_fly;
        public int hurt_fly_velocity_max;
        public int icon;
        public int id;
        public bool is_knock_back_for_player;
        public bool is_knock_down_for_player;
        public bool is_skill;
        public int job;
        public string killing_feedback;
        public int knock_back_factor;
        public int knock_back_opp;
        public int knock_down_factor;
        public int knock_down_opp;
        public int lvl;
        public string name;
        public int next;
        public string param_list;
        public int position;
        public int pre;
        public int restricted_factor;
        public int restricted_job_id;
        public bool shake;
        public uint shake_id;
        public int shape_x;
        public int shape_y;
        public uint skill_group;
        public int skill_lvl;
        public string sound_hit;
        public string sound_id;
        public int subtype;
        public int target_num;
        public int target_type;
        public int type;
        public uint unikey;
        public int warn_eff;
        public int warn_prolong_time;
        public int warn_time;
    }
}

