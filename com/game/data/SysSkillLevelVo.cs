﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysSkillLevelVo
    {
        public uint beiley;
        public string buff_list;
        public int data_fixed;
        public int data_per;
        public string desc;
        public uint id;
        public uint lvl;
        public uint skill_group;
        public uint skill_lvl;
        public uint unikey;
    }
}

