﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysSkillTalentVo
    {
        public string attribute;
        public string copper;
        public string description;
        public string icon_id;
        public uint id;
        public string name;
        public string need_item;
        public uint need_level;
        public uint unikey;
    }
}

