﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysSoulVo
    {
        public uint id;
        public int property_add_rate;
        public int pvp_relive_time;
        public uint unikey;
    }
}

