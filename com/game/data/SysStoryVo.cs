﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysStoryVo
    {
        public uint id;
        public int nextId;
        public string script;
        public int time;
        public int type;
        public uint unikey;
    }
}

