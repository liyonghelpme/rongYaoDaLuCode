﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysStrengthLv
    {
        public int charge;
        public string material_list;
        public uint rate;
        public uint strength_lv;
        public uint unikey;
    }
}

