﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysTask
    {
        public int accept_icon;
        public int com_icon;
        public int diam;
        public int exp;
        public uint give_goods;
        public int gold;
        public int guild_con;
        public int guild_exp;
        public int guild_mon_exp;
        public string ins_req;
        public bool ir_pro_limit;
        public string item_reward;
        public string kill_monster;
        public string level;
        public string name;
        public uint next_task;
        public uint npcEnd;
        public uint npcStart;
        public int order;
        public uint pre_task;
        public bool processShow;
        public int repu;
        public string req;
        public string sort;
        public int spirit;
        public int star;
        public uint talk;
        public string talk_accept;
        public string talk_com;
        public string talk_uncom;
        public int taskId;
        public int times;
        public string trace_accept;
        public string trace_com;
        public string trace_uncom;
        public int type;
        public int uncom_icon;
        public uint unikey;
    }
}

