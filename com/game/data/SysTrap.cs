﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysTrap
    {
        public int AttackInterval;
        public int AttackType;
        public int BuffId;
        public int BuffLvl;
        public bool HurtFly;
        public int Id;
        public int Model;
        public string SkillIds;
        public int Type;
        public uint unikey;
    }
}

