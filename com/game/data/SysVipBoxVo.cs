﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysVipBoxVo
    {
        public int diam;
        public int gold_ratio;
        public uint id;
        public uint unikey;
    }
}

