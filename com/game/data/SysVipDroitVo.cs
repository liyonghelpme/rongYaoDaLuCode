﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysVipDroitVo
    {
        public int type;
        public uint unikey;
        public int value;
        public int vip;
    }
}

