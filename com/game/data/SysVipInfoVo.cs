﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysVipInfoVo
    {
        public string awards;
        public string describe;
        public int diam;
        public int id;
        public uint unikey;
        public int vip;
    }
}

