﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysVipMallLimitVo
    {
        public string alternate;
        public string pos1;
        public string pos2;
        public string pos3;
        public uint unikey;
    }
}

