﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysVipMallVo
    {
        public string avatar;
        public string buy_group;
        public int buy_max;
        public int curr_price;
        public int former_price;
        public int giving;
        public int icon_big;
        public int id;
        public int mark;
        public int money;
        public string name;
        public int queue;
        public string show_time;
        public int small_type;
        public int type;
        public uint unikey;
    }
}

