﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysWifiPvpVo
    {
        public string battle_object_list;
        public string desc;
        public string enemy_ai_script;
        public uint id;
        public uint map_id;
        public string master_base_pos;
        public string master_born_pos;
        public string master_private_list;
        public string master_private_path_list;
        public string master_spring_pos;
        public string master_tower_list;
        public string my_ai_script;
        public string name;
        public string private_ai_script;
        public int pvp_cd;
        public string slave_base_pos;
        public string slave_born_pos;
        public string slave_private_list;
        public string slave_private_path_list;
        public string slave_spring_pos;
        public string slave_tower_list;
        public int spring_radius;
        public string tower_ai_script;
        public uint unikey;
        public int vigor_cost;
    }
}

