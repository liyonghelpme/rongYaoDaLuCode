﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SysmonsterPaoPaoVo
    {
        public string condition;
        public string descript;
        public int dungeon_id;
        public uint id;
        public int monster_id;
        public uint unikey;
    }
}

