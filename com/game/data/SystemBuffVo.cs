﻿namespace com.game.data
{
    using System;

    [Serializable]
    public class SystemBuffVo
    {
        public string desc;
        public int id;
        public string name;
        public int type;
        public uint unikey;
    }
}

