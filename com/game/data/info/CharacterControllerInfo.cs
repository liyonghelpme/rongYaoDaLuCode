﻿namespace com.game.data.info
{
    using System;
    using UnityEngine;

    public class CharacterControllerInfo
    {
        public Vector3 center;
        public float height;
        public float radius;
        public float slopeLimit;
        public float stepOffset;
        public Vector3 velocity;
    }
}

