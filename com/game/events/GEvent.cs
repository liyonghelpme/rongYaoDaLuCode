﻿namespace com.game.events
{
    using System;

    public class GEvent
    {
        public const int ARENA_COUNT_DOWN_OVER = 200;
        public const int ARRIVE_DEST = 5;
        public const int CHAT_AUTO_END_TALK = 0x67;
        public const int CHAT_PLAYED_END = 0x68;
        public const int CHAT_RECEIVE_MESSAGE = 0x65;
        public const int CHAT_VOICE_COMPLETE = 0x66;
        public const int CLOSE_VIEW = 20;
        public const int DUNGEON_END = 12;
        public const int DUNGEON_RESULT = 13;
        public const int DUNGEON_STATE_CHANGE = 10;
        public const int DUNGEON_UPDATE = 11;
        public const int END_ANIM = 0x12;
        public const int ENTER_DUNGEON = 9;
        public const int ENTER_SCENE = 15;
        public const int EXIT_DUNGEON = 14;
        public const int FIND_PARTNER = 4;
        public const int GENERAL_DEAD = 2;
        public const int KILL_MONSTER = 1;
        public const int MONSTER_CAUGHT = 7;
        public const int MONSTER_FLEED = 8;
        public const int OPEN_VIEW = 0x13;
        public const int PICK_BOX = 3;
        public const int PLAY_CAMERA = 0x17;
        public const int PLAYER_WAR_STATE_CHANGE = 0x15;
        public const int RESCUE_PARTNER = 6;
        public const int ROLE_ATTR_CHANGE = 0x12d;
        public const int ROLE_LVL_CHANGE = 300;
        public const int SCENE_COMPLETE = 0x10;
        public const int START_ANIM = 0x11;
        public const int STOP_CAMERA = 0x18;
        public const int SUMMON = 0x16;
        public const int UPDATE_BOSS_BAR = 0x191;
    }
}

