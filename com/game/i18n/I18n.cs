﻿namespace com.game.i18n
{
    using com.game.utils;
    using com.u3d.bases.loader;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class I18n
    {
        public static I18n instance = new I18n();
        private IDictionary<string, IDictionary<int, string>> moduleDict = new Dictionary<string, IDictionary<int, string>>();
        private IDictionary<string, IDictionary<int, string>> socketDict = new Dictionary<string, IDictionary<int, string>>();

        public string getModuleTip(string code, int key = 1)
        {
            if (StringUtils.isEmpty(code))
            {
                return string.Empty;
            }
            IDictionary<int, string> dictionary = !this.moduleDict.ContainsKey(code) ? null : this.moduleDict[code];
            return (((dictionary == null) || !dictionary.ContainsKey(key)) ? string.Empty : dictionary[key]);
        }

        public string getSocketTip(string code, int key = 1)
        {
            if (StringUtils.isEmpty(code))
            {
                return string.Empty;
            }
            IDictionary<int, string> dictionary = !this.socketDict.ContainsKey(code) ? null : this.socketDict[code];
            return (((dictionary == null) || !dictionary.ContainsKey(key)) ? string.Empty : dictionary[key]);
        }

        public void init()
        {
        }

        private void loadModuleTip()
        {
            XMLNode node = XMLParser.Parse(((TextAsset) ResMgr.instance.load("xml/module", null)).ToString());
            XMLNodeList nodeList = node.GetNodeList("i18n>0>businessTip>0>tip");
            XMLNodeList list2 = node.GetNodeList("i18n>0>uiTip>0>tip");
            this.parserMsgList(this.moduleDict, nodeList);
            this.parserMsgList(this.moduleDict, list2);
        }

        private void loadSocketTip()
        {
            TextAsset asset = (TextAsset) ResMgr.instance.load("xml/socket", null);
            XMLNodeList nodeList = XMLParser.Parse(asset.ToString()).GetNodeList("i18n>0>tip");
            this.parserMsgList(this.socketDict, nodeList);
        }

        private void parserMsgList(IDictionary<string, IDictionary<int, string>> dict, XMLNodeList nodeList)
        {
            string param = null;
            string str2 = null;
            XMLNodeList list = null;
            IDictionary<int, string> dictionary = null;
            IEnumerator enumerator = nodeList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    XMLNode current = (XMLNode) enumerator.Current;
                    param = current.GetValue("@key");
                    list = current.GetNodeList("msg");
                    if (!StringUtils.isEmpty(param))
                    {
                        dictionary = new Dictionary<int, string>();
                        IEnumerator enumerator2 = list.GetEnumerator();
                        try
                        {
                            while (enumerator2.MoveNext())
                            {
                                XMLNode node2 = (XMLNode) enumerator2.Current;
                                str2 = node2.GetValue("@key");
                                if (!StringUtils.isEmpty(str2))
                                {
                                    dictionary.Add(int.Parse(str2), node2.GetValue("_text"));
                                }
                            }
                        }
                        finally
                        {
                            IDisposable disposable = enumerator2 as IDisposable;
                            if (disposable == null)
                            {
                            }
                            disposable.Dispose();
                        }
                        dict.Add(param, dictionary);
                    }
                }
            }
            finally
            {
                IDisposable disposable2 = enumerator as IDisposable;
                if (disposable2 == null)
                {
                }
                disposable2.Dispose();
            }
        }
    }
}

