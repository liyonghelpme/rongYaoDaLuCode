﻿namespace com.game.manager
{
    using com.u3d.bases.debug;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AssetBundleLoader
    {
        private UnityEngine.Object[] allAsset;
        private WWW downloader;

        public AssetBundleLoader(string path, string fileName, System.Type type, string assetName = null, bool cache = false, DownLoadState state = 0, bool isScene = false, bool asynLoad = false)
        {
            this.path = path;
            this.fileName = fileName;
            this.assetName = assetName;
            this.state = state;
            this.isScene = isScene;
            this.assetType = type;
            this.asynLoad = asynLoad;
            this.cache = cache;
        }

        private bool CreateAssetBundleFile(string fileName, byte[] bytes)
        {
            return this.CreateFile(Application.persistentDataPath + "/", fileName, bytes);
        }

        private bool CreateFile(string path, string fileName, byte[] bytes)
        {
            Stream stream = null;
            string str = path + fileName;
            string str2 = str.Substring(0, str.LastIndexOf("/"));
            bool flag = false;
            try
            {
                DirectoryInfo info = new DirectoryInfo(str2);
                if (!info.Exists)
                {
                    com.u3d.bases.debug.Log.info(this, "创建文件夹:" + str2);
                    info.Create();
                }
                com.u3d.bases.debug.Log.info(this, "创建文件:" + str);
                FileInfo info2 = new FileInfo(str);
                stream = new FileStream(info2.FullName, FileMode.OpenOrCreate, FileAccess.Write);
                stream.Write(bytes, 0, bytes.Length);
                flag = true;
            }
            catch (IOException exception)
            {
                com.u3d.bases.debug.Log.error(this, exception.Message);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }
            return flag;
        }

        private void DeleteFile(string path, string fileName)
        {
            File.Delete(path + fileName);
        }

        private float getProgress()
        {
            float progress = 0f;
            if ((this.state == DownLoadState.Loaded) || (this.state == DownLoadState.Stored))
            {
                progress = 1f;
            }
            else if (((this.state == DownLoadState.Init) || (this.state == DownLoadState.LoadFailure)) || (this.state == DownLoadState.StoreFailure))
            {
                progress = 0f;
            }
            else if (this.state == DownLoadState.Loading)
            {
                progress = this.downloader.progress;
            }
            if (!this.isScene)
            {
                return progress;
            }
            if (this.loadSceneOperation != null)
            {
                progress += this.loadSceneOperation.progress;
            }
            return (progress / 2f);
        }

        public UnityEngine.Object[] LoadAllAssets()
        {
            if ((this.assetBundle != null) && (this.allAsset == null))
            {
                this.allAsset = this.assetBundle.LoadAll();
            }
            return this.allAsset;
        }

        [DebuggerHidden]
        public IEnumerator LoadAssetBundle(bool storeLocal = false, bool unload = false)
        {
            return new <LoadAssetBundle>c__Iterator26 { storeLocal = storeLocal, unload = unload, <$>storeLocal = storeLocal, <$>unload = unload, <>f__this = this };
        }

        [DebuggerHidden]
        public IEnumerator LoadAssetBundleCached(bool unload = false, int version = 1)
        {
            return new <LoadAssetBundleCached>c__Iterator27 { version = version, unload = unload, <$>version = version, <$>unload = unload, <>f__this = this };
        }

        public void UnloadAssetBundle(bool isAll = false)
        {
            if (this.assetBundle != null)
            {
                this.assetBundle.Unload(isAll);
            }
            this.allAsset = null;
        }

        public AssetBundle assetBundle { get; private set; }

        public string assetName { get; set; }

        public System.Type assetType { get; set; }

        public bool asynLoad { get; set; }

        public bool cache { get; set; }

        public bool delayUnload { get; set; }

        public string fileName { get; private set; }

        public bool isScene { get; private set; }

        public AsyncOperation loadSceneOperation { get; set; }

        public string path { get; private set; }

        public float progress
        {
            get
            {
                return this.getProgress();
            }
        }

        public int size { get; private set; }

        public DownLoadState state { get; private set; }

        [CompilerGenerated]
        private sealed class <LoadAssetBundle>c__Iterator26 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal bool <$>storeLocal;
            internal bool <$>unload;
            internal AssetBundleLoader <>f__this;
            internal bool storeLocal;
            internal bool unload;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<>f__this.state = DownLoadState.Init;
                        this.<>f__this.downloader = new WWW(this.<>f__this.path + this.<>f__this.fileName);
                        this.<>f__this.state = DownLoadState.Loading;
                        this.$current = this.<>f__this.downloader;
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.<>f__this.downloader.error == null)
                        {
                            this.<>f__this.size = this.<>f__this.downloader.bytesDownloaded;
                            this.<>f__this.assetBundle = this.<>f__this.downloader.assetBundle;
                            if (this.<>f__this.assetBundle != null)
                            {
                                this.<>f__this.state = DownLoadState.Loaded;
                                if (this.storeLocal)
                                {
                                    if (this.<>f__this.CreateAssetBundleFile(this.<>f__this.fileName, this.<>f__this.downloader.bytes))
                                    {
                                        this.<>f__this.state = DownLoadState.Stored;
                                    }
                                    else
                                    {
                                        this.<>f__this.state = DownLoadState.StoreFailure;
                                    }
                                    if (this.unload)
                                    {
                                        this.<>f__this.assetBundle.Unload(true);
                                        this.<>f__this.assetBundle = null;
                                    }
                                }
                            }
                            else
                            {
                                com.u3d.bases.debug.Log.error(this.<>f__this, "加载" + this.<>f__this.fileName + "异常，可能资源重复加载");
                                this.<>f__this.state = DownLoadState.LoadFailure;
                            }
                            this.<>f__this.downloader.Dispose();
                            this.$PC = -1;
                            break;
                        }
                        if (!this.<>f__this.downloader.error.Contains("bust"))
                        {
                            com.u3d.bases.debug.Log.error(this.<>f__this, this.<>f__this.downloader.error);
                        }
                        this.<>f__this.state = DownLoadState.LoadFailure;
                        this.<>f__this.downloader.Dispose();
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetBundleCached>c__Iterator27 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal bool <$>unload;
            internal int <$>version;
            internal AssetBundleLoader <>f__this;
            internal bool unload;
            internal int version;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<>f__this.state = DownLoadState.Init;
                        if (!Caching.enabled)
                        {
                            goto Label_008E;
                        }
                        break;

                    case 1:
                        break;

                    case 2:
                        if (this.<>f__this.downloader.error == null)
                        {
                            this.<>f__this.assetBundle = this.<>f__this.downloader.assetBundle;
                            if (this.<>f__this.assetBundle != null)
                            {
                                this.<>f__this.state = DownLoadState.Loaded;
                                if (this.unload)
                                {
                                    this.<>f__this.assetBundle.Unload(true);
                                    this.<>f__this.assetBundle = null;
                                }
                            }
                            else
                            {
                                com.u3d.bases.debug.Log.error(this.<>f__this, "加载" + this.<>f__this.fileName + "异常，可能资源重复加载");
                                this.<>f__this.state = DownLoadState.LoadFailure;
                            }
                            this.<>f__this.downloader.Dispose();
                            this.<>f__this.downloader = null;
                            this.$PC = -1;
                        }
                        else
                        {
                            com.u3d.bases.debug.Log.error(this.<>f__this, this.<>f__this.downloader.error);
                            this.<>f__this.state = DownLoadState.LoadFailure;
                            this.<>f__this.downloader.Dispose();
                        }
                        goto Label_01C6;

                    default:
                        goto Label_01C6;
                }
                if (!Caching.ready)
                {
                    this.$current = null;
                    this.$PC = 1;
                    goto Label_01C8;
                }
                this.<>f__this.downloader = WWW.LoadFromCacheOrDownload(this.<>f__this.path + this.<>f__this.fileName, this.version);
            Label_008E:
                this.<>f__this.state = DownLoadState.Loading;
                this.$current = this.<>f__this.downloader;
                this.$PC = 2;
                goto Label_01C8;
            Label_01C6:
                return false;
            Label_01C8:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

