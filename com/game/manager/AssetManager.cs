﻿namespace com.game.manager
{
    using com.game;
    using com.game.consts;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.effect;
    using com.game.preloader;
    using com.game.Public.Confirm;
    using com.game.Public.Message;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.debug;
    using com.u3d.bases.loader;
    using com.utils;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using UnityEngine;
    using UnityLog;

    internal class AssetManager : MonoBehaviour
    {
        private Dictionary<string, UnityEngine.Object> cachedAssets = new Dictionary<string, UnityEngine.Object>();
        private Dictionary<string, Delegate> cachedAssetsCallBack = new Dictionary<string, Delegate>();
        private volatile int callCount;
        private List<uint> generalPerloadHistory = new List<uint>();
        private LoadAssetFinish<UnityEngine.Object> initCall;
        private static volatile AssetManager instance = null;
        private bool isAssetInit;
        private bool loadAssetbundle = true;
        private Dictionary<string, AssetBundleLoader> loadingAssetsBundles = new Dictionary<string, AssetBundleLoader>();
        public static string persistentDataPath = ("file:///" + Application.persistentDataPath + "/");
        private List<uint> scenePreloadHistory = new List<uint>();
        private Dictionary<string, AssetBundleLoader> sharedAssets = new Dictionary<string, AssetBundleLoader>();
        private List<AssetBundleLoader> WaitingAssetsBundles = new List<AssetBundleLoader>();

        private void addCallBack<T>(string assetKey, LoadAssetFinish<T> callBack) where T: UnityEngine.Object
        {
            Dictionary<string, Delegate> cachedAssetsCallBack = this.cachedAssetsCallBack;
            lock (cachedAssetsCallBack)
            {
                if (!this.cachedAssetsCallBack.ContainsKey(assetKey))
                {
                    this.cachedAssetsCallBack[assetKey] = callBack;
                }
                else
                {
                    LoadAssetFinish<T> a = this.cachedAssetsCallBack[assetKey] as LoadAssetFinish<T>;
                    a = (LoadAssetFinish<T>) Delegate.Combine(a, callBack);
                    this.cachedAssetsCallBack[assetKey] = a;
                }
            }
        }

        private void addWaitAssetBundleLoader(AssetBundleLoader assetBundleLoader)
        {
            List<AssetBundleLoader> waitingAssetsBundles = this.WaitingAssetsBundles;
            lock (waitingAssetsBundles)
            {
                for (int i = 0; i < this.WaitingAssetsBundles.Count; i++)
                {
                    if (this.WaitingAssetsBundles[i].fileName.Equals(assetBundleLoader.fileName))
                    {
                        goto Label_0067;
                    }
                }
                this.WaitingAssetsBundles.Add(assetBundleLoader);
            Label_0067:;
            }
        }

        private void callBack(string assetKey, UnityEngine.Object param)
        {
            Delegate delegate2 = null;
            Dictionary<string, Delegate> cachedAssetsCallBack = this.cachedAssetsCallBack;
            lock (cachedAssetsCallBack)
            {
                if (this.cachedAssetsCallBack.ContainsKey(assetKey))
                {
                    delegate2 = this.cachedAssetsCallBack[assetKey];
                    this.cachedAssetsCallBack.Remove(assetKey);
                }
            }
            if (delegate2 != null)
            {
                foreach (Delegate delegate3 in delegate2.GetInvocationList())
                {
                    object[] args = new object[] { param };
                    delegate3.DynamicInvoke(args);
                }
            }
        }

        [DebuggerHidden]
        private IEnumerator DownloadAndStoreAssetBundle(AssetBundleLoader assetBundleLoader, LoadAssetBundleFinish callBack)
        {
            return new <DownloadAndStoreAssetBundle>c__Iterator29 { assetBundleLoader = assetBundleLoader, callBack = callBack, <$>assetBundleLoader = assetBundleLoader, <$>callBack = callBack, <>f__this = this };
        }

        public AssetBundleLoader DownLoadAssetBundle(string url, string fileName, LoadAssetBundleFinish callBack = null)
        {
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(url, fileName, null, null, false, DownLoadState.Init, false, false);
            base.StartCoroutine(this.DownloadAndStoreAssetBundle(assetBundleLoader, callBack));
            return assetBundleLoader;
        }

        private string getAssetBundleLoaderPath(string fileName)
        {
            if (ClientUpdate.updateFinish && File.Exists(Application.persistentDataPath + "/" + fileName))
            {
                return persistentDataPath;
            }
            return dataPathURL;
        }

        private string getAssetKey(string fileName, string assetName, System.Type assetType)
        {
            string str;
            if (assetName != null)
            {
                str = fileName + assetName;
            }
            else
            {
                str = fileName + "main";
            }
            return (str + assetType.Name);
        }

        private string getDepAssetFileName(string assetFileName)
        {
            char[] separator = new char[] { '.' };
            string[] strArray = assetFileName.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            return (strArray[0] + "_dep." + strArray[1]);
        }

        private string[] getDependeAssetFilesName(TextAsset text)
        {
            string str = text.text;
            char[] separator = new char[] { '\r', '\n' };
            return str.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }

        public void init(LoadAssetFinish<UnityEngine.Object> callback)
        {
            this.initCall = callback;
            this.initFont();
        }

        private void initBindataCallBack(TextAsset text)
        {
            if (text != null)
            {
                com.u3d.bases.debug.Log.info(this, "-Start() 4、基础数据长度:" + (text.bytes.Length / 0x400) + "KB");
                UnityEngine.Debug.Log("-Start() 4、基础数据长度:" + (text.bytes.Length / 0x400) + "KB");
                object data = SerializerUtils.binaryDerialize(text.bytes);
                BaseDataMgr.instance.init(data);
                Resources.UnloadAsset(text);
                Singleton<ConfigConst>.Instance.SetConfigData(BaseDataMgr.instance.GetDicByType<SysConfigVo>());
                com.u3d.bases.debug.Log.info(this, "-Start() 5、基础数据初始化【OK】");
            }
            this.initCountCallBack(text);
        }

        private void initCommon(UnityEngine.Object obj)
        {
            Instance.LoadAndCacheSharedAssets("UI/Common/Common.assetbundle", new LoadAssetFinish<UnityEngine.Object>(this.initCommonCallBack));
        }

        private void initCommonCallBack(UnityEngine.Object obj)
        {
            MessageManager.Init();
            ConfirmMgr.Instance.Init();
            Singleton<StartLoadingView>.Instance.OpenView();
            Singleton<WaitingView>.Instance.OpenView();
        }

        private void initCountCallBack(UnityEngine.Object obj)
        {
            AssetManager instance = AssetManager.instance;
            lock (instance)
            {
                this.callCount--;
                if (this.callCount == 0)
                {
                    this.initCall(null);
                }
            }
        }

        public void initFont()
        {
            Instance.LoadAndCacheSharedAssets("UI/Font/arialbd.assetbundle", new LoadAssetFinish<UnityEngine.Object>(this.InitHycyjCallBack));
        }

        private void InitFontCallBack(UnityEngine.Object obj)
        {
            this.initNewCommon();
        }

        private void InitHycyjCallBack(UnityEngine.Object obj)
        {
            Instance.LoadAndCacheSharedAssets("UI/Font/hycyj.assetbundle", new LoadAssetFinish<UnityEngine.Object>(this.InitMsyhFont));
        }

        private void InitLoadingLanguageCallBack(TextAsset text)
        {
            string data = Encoding.GetEncoding("UTF_8").GetString(text.bytes);
            base.StartCoroutine(LanguageManager.SetUp(data, new LanguageManager.SetUpCallBack(this.initCountCallBack)));
        }

        private void InitMsyhFont(UnityEngine.Object obj)
        {
            Instance.LoadAndCacheSharedAssets("UI/Font/msyh.assetbundle", new LoadAssetFinish<UnityEngine.Object>(this.InitWDCSFont));
        }

        private void initNewCommon()
        {
            Instance.LoadAndCacheSharedAssets("UI/Common/NewCommonAtlas.assetbundle", new LoadAssetFinish<UnityEngine.Object>(this.initCommon));
        }

        private List<AssetBundleLoader> initOther()
        {
            this.callCount = 2;
            List<AssetBundleLoader> list = new List<AssetBundleLoader>();
            if ((Application.platform == RuntimePlatform.WindowsEditor) && File.Exists(Application.dataPath + "/Xml/BinData.xml"))
            {
                UnityEngine.Debug.Log("从xml中加载数据");
                if (GameConfig.readBinDataFromServer)
                {
                    base.StartCoroutine(this.LoadBinDataXmlFromServer("http://172.16.11.54:9269/BinData.xml"));
                }
                else
                {
                    object data = SerializerUtils.binaryDerialize(File.ReadAllBytes(Application.dataPath + "/Xml/BinData.xml"));
                    BaseDataMgr.instance.init(data);
                    Singleton<ConfigConst>.Instance.SetConfigData(BaseDataMgr.instance.GetDicByType<SysConfigVo>());
                    this.initCountCallBack(null);
                }
            }
            else
            {
                list.Add(Instance.LoadAsset<TextAsset>("Xml/BinData.assetbundle", new LoadAssetFinish<TextAsset>(this.initBindataCallBack), null, false, true));
            }
            if ((Application.platform == RuntimePlatform.WindowsEditor) && File.Exists(LanguageManager.XML_URL))
            {
                byte[] bytes = File.ReadAllBytes(LanguageManager.XML_URL);
                LanguageManager.SetUp(Encoding.UTF8.GetString(bytes));
                this.initCountCallBack(null);
                return list;
            }
            list.Add(Instance.LoadAsset<TextAsset>(LanguageManager.URL, new LoadAssetFinish<TextAsset>(this.InitLoadingLanguageCallBack), null, false, true));
            return list;
        }

        private void InitWDCSFont(UnityEngine.Object obj)
        {
            Instance.LoadAndCacheSharedAssets("UI/Font/WDCS.assetbundle", new LoadAssetFinish<UnityEngine.Object>(this.InitFontCallBack));
        }

        [DebuggerHidden]
        private IEnumerator LoadAndCachedAllAssetInAssetBundle(AssetBundleLoader assetBundleLoader, LoadAssetFinish<UnityEngine.Object> callBack)
        {
            return new <LoadAndCachedAllAssetInAssetBundle>c__Iterator2E { assetBundleLoader = assetBundleLoader, callBack = callBack, <$>assetBundleLoader = assetBundleLoader, <$>callBack = callBack, <>f__this = this };
        }

        public AssetBundleLoader LoadAndCacheSharedAssets(string fileName, LoadAssetFinish<UnityEngine.Object> callBack)
        {
            if (!this.loadAssetbundle)
            {
                callBack(null);
                return new AssetBundleLoader(fileName, string.Empty, typeof(GameObject), null, false, DownLoadState.Init, false, false);
            }
            string path = this.getAssetBundleLoaderPath(fileName);
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(path, fileName, null, null, false, DownLoadState.Init, false, false);
            if (!this.sharedAssets.ContainsKey(fileName))
            {
                com.u3d.bases.debug.Log.info(this, "加载" + path + fileName);
                base.StartCoroutine(this.LoadAndCachedAllAssetInAssetBundle(assetBundleLoader, callBack));
                return assetBundleLoader;
            }
            if (callBack != null)
            {
                callBack(null);
            }
            return assetBundleLoader;
        }

        private UnityEngine.Object LoadAsset(AssetBundleLoader assetBundleLoader, string assetName, System.Type assettype)
        {
            UnityEngine.Object obj2 = null;
            if (assetBundleLoader.state != DownLoadState.Loaded)
            {
                return obj2;
            }
            if ((assetName != null) && !assetName.Equals(string.Empty))
            {
                UnityEngine.Object obj3 = assetBundleLoader.assetBundle.Load(assetName, assettype);
                if (obj3 == null)
                {
                    object[] objArray1 = new object[] { assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name, assetName };
                    com.u3d.bases.debug.Log.error(this, string.Format("资源包{0}{1}中类型为{2}的资源 {3} 不存在", objArray1));
                    return obj2;
                }
                object[] args = new object[] { assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name, assetName };
                com.u3d.bases.debug.Log.info(this, string.Format("成功在资源包{0}{1}中加载类型为{2}的资源 {3}", args));
                return obj3;
            }
            com.u3d.bases.debug.Log.info(this, assetBundleLoader.fileName + " " + assetBundleLoader.path);
            UnityEngine.Object mainAsset = assetBundleLoader.assetBundle.mainAsset;
            if (mainAsset == null)
            {
                com.u3d.bases.debug.Log.error(this, string.Format("资源包{0}{1}中主资源不存在", assetBundleLoader.path, assetBundleLoader.fileName));
                return obj2;
            }
            if (assettype == typeof(UnityEngine.Object))
            {
                return (obj2 = mainAsset);
            }
            if (mainAsset.GetType() != assettype)
            {
                com.u3d.bases.debug.Log.error(this, string.Format("资源包{0}{1}中主资源不是{2}类型", assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name));
                return obj2;
            }
            com.u3d.bases.debug.Log.info(this, string.Format("成功在资源包{0}{1}中加载类型为{2}的主资源", assetBundleLoader.path, assetBundleLoader.fileName, assettype.Name));
            return mainAsset;
        }

        public AssetBundleLoader LoadAsset<T>(string fileName, LoadAssetFinish<T> callBack, string assetName = null, bool cache = false, bool asynLoad = true) where T: UnityEngine.Object
        {
            string path = this.getAssetBundleLoaderPath(fileName);
            return this.LoadAsset<T>(path, fileName, callBack, assetName, cache, false, asynLoad);
        }

        public AssetBundleLoader LoadAsset<T>(string path, string fileName, LoadAssetFinish<T> callBack, string assetName = null, bool cache = false, bool scene = false, bool asynload = false) where T: UnityEngine.Object
        {
            UnityLog.Log.AI(null, string.Concat(new object[] { " LoadAsset ", path, " fileName ", fileName, " callBack ", callBack }));
            if (this.sharedAssets.ContainsKey(fileName))
            {
                UnityEngine.Object obj2 = this.LoadAsset(this.sharedAssets[fileName], assetName, typeof(T));
                if (callBack != null)
                {
                    T local = obj2 as T;
                    callBack(local);
                }
                return this.sharedAssets[fileName];
            }
            string key = this.getAssetKey(fileName, assetName, typeof(T));
            if (this.cachedAssets.ContainsKey(key))
            {
                if (callBack != null)
                {
                    T local2 = this.cachedAssets[key] as T;
                    callBack(local2);
                }
                return new AssetBundleLoader(path, fileName, typeof(T), assetName, cache, DownLoadState.Cached, scene, asynload);
            }
            if (callBack != null)
            {
                this.addCallBack<T>(key, callBack);
            }
            Dictionary<string, AssetBundleLoader> loadingAssetsBundles = this.loadingAssetsBundles;
            lock (loadingAssetsBundles)
            {
                if (this.loadingAssetsBundles.ContainsKey(fileName))
                {
                    return this.loadingAssetsBundles[fileName];
                }
            }
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(path, fileName, typeof(T), assetName, cache, DownLoadState.Init, scene, asynload);
            this.addWaitAssetBundleLoader(assetBundleLoader);
            return assetBundleLoader;
        }

        private AssetBundleRequest LoadAssetAsync(AssetBundleLoader assetBundleLoader, string assetName, System.Type assettype)
        {
            AssetBundleRequest request = null;
            if (assetBundleLoader.state != DownLoadState.Loaded)
            {
                return request;
            }
            if ((assetName == null) || assetName.Equals(string.Empty))
            {
                int num = assetBundleLoader.fileName.LastIndexOf('/');
                int num2 = assetBundleLoader.fileName.LastIndexOf('.');
                assetName = assetBundleLoader.fileName.Substring(num + 1, (num2 - num) - 1);
            }
            return assetBundleLoader.assetBundle.LoadAsync(assetName, assettype);
        }

        [DebuggerHidden]
        private IEnumerator LoadAssetBundle(AssetBundleLoader assetBundleLoader, LoadAssetBundleFinish callBack)
        {
            return new <LoadAssetBundle>c__Iterator2A { assetBundleLoader = assetBundleLoader, callBack = callBack, <$>assetBundleLoader = assetBundleLoader, <$>callBack = callBack, <>f__this = this };
        }

        public AssetBundleLoader LoadAssetBundle(string path, string fileName, LoadAssetBundleFinish callBack = null)
        {
            AssetBundleLoader assetBundleLoader = new AssetBundleLoader(path, fileName, null, null, false, DownLoadState.Init, false, false);
            base.StartCoroutine(this.LoadAssetBundle(assetBundleLoader, callBack));
            return assetBundleLoader;
        }

        public AssetBundleLoader LoadAssetByKeys<T>(string path, string keys, LoadAssetFinishByKeys<T> callBack) where T: UnityEngine.Object
        {
            return this.LoadAsset<T>(path, null, null, false, true);
        }

        public AssetBundleLoader LoadAssetFromResources<T>(string fileName, LoadAssetFinish<T> callBack, string assetName = null, bool cache = false) where T: UnityEngine.Object
        {
            fileName = fileName.Replace(".assetbundle", string.Empty);
            callBack(ResMgr.instance.load(fileName, typeof(T)) as T);
            return new AssetBundleLoader(fileName, assetName, typeof(T), null, false, DownLoadState.Init, false, false);
        }

        [DebuggerHidden]
        private IEnumerator LoadAssetInAssetBundle(AssetBundleLoader assetBundleLoader)
        {
            return new <LoadAssetInAssetBundle>c__Iterator2B { assetBundleLoader = assetBundleLoader, <$>assetBundleLoader = assetBundleLoader, <>f__this = this };
        }

        [DebuggerHidden]
        private IEnumerator LoadBinDataXmlFromServer(string path)
        {
            return new <LoadBinDataXmlFromServer>c__Iterator28 { path = path, <$>path = path, <>f__this = this };
        }

        public void LoadRenderSeting(string renderId)
        {
            if (renderId == "-1")
            {
                AppMap.Instance.mapParserII.LoadRenderSetingComplete(null);
            }
            else
            {
                Singleton<StartLoadingView>.Instance.loaderList.Add(Instance.LoadAsset<TextAsset>("Scene/RenderSeting/" + renderId + ".assetbundle", new LoadAssetFinish<TextAsset>(AppMap.Instance.mapParserII.LoadRenderSetingComplete), null, true, true));
            }
        }

        public AssetBundleLoader LoadScene(uint mapId, LoadAssetFinish<GameObject> callBack)
        {
            string sceneFileName = "Scene/" + mapId + ".assetbundle";
            string sceneName = mapId.ToString();
            return this.LoadSceneLevel(sceneFileName, sceneName, callBack);
        }

        [DebuggerHidden]
        private IEnumerator LoadSceneAsset(uint mapId, string key)
        {
            return new <LoadSceneAsset>c__Iterator2F { mapId = mapId, key = key, <$>mapId = mapId, <$>key = key, <>f__this = this };
        }

        [DebuggerHidden]
        private IEnumerator LoadSceneInResource(uint mapResourceId, LoadAssetFinish<GameObject> callBack)
        {
            return new <LoadSceneInResource>c__Iterator2D { mapResourceId = mapResourceId, callBack = callBack, <$>mapResourceId = mapResourceId, <$>callBack = callBack, <>f__this = this };
        }

        [DebuggerHidden]
        private IEnumerator LoadSceneLevel(AssetBundleLoader assetBundleLoader, string sceneName)
        {
            return new <LoadSceneLevel>c__Iterator2C { sceneName = sceneName, assetBundleLoader = assetBundleLoader, <$>sceneName = sceneName, <$>assetBundleLoader = assetBundleLoader };
        }

        public AssetBundleLoader LoadSceneLevel(uint mapResourceId, LoadAssetFinish<GameObject> callBack)
        {
            string sceneFileName = "Scene/" + mapResourceId + ".assetbundle";
            string sceneName = mapResourceId.ToString();
            if (!this.loadAssetbundle)
            {
                return new AssetBundleLoader(string.Empty, string.Empty, typeof(GameObject), null, false, DownLoadState.Init, false, false);
            }
            int[] numArray = new int[] { 1, 5 };
            for (int i = 0; i < numArray.Length; i++)
            {
                IList<SysReadyLoadVo> scenePreLoadList = BaseDataMgr.instance.GetScenePreLoadList(mapResourceId, numArray[i]);
                StartLoadingView instance = Singleton<StartLoadingView>.Instance;
                instance.PreLoadNum += scenePreLoadList.Count;
            }
            if (BaseDataMgr.instance.GetMapVo(MeVo.instance.mapId).type == 2)
            {
                Singleton<MonsterMgr>.Instance.PreMonloadList = BaseDataMgr.instance.GetMonPreLoadList(MeVo.instance.mapId);
                if (Singleton<MonsterMgr>.Instance.PreMonloadList.Count > 0)
                {
                    StartLoadingView local2 = Singleton<StartLoadingView>.Instance;
                    local2.PreLoadNum += Singleton<MonsterMgr>.Instance.PreMonloadList.Count;
                }
            }
            return this.LoadSceneLevel(sceneFileName, sceneName, callBack);
        }

        public AssetBundleLoader LoadSceneLevel(string sceneFileName, string sceneName, LoadAssetFinish<GameObject> callBack)
        {
            string path = this.getAssetBundleLoaderPath(sceneFileName);
            UnityEngine.Debug.Log("path = " + path);
            return this.LoadAsset<GameObject>(path, sceneFileName, callBack, sceneName, false, true, false);
        }

        public void LoadSceneLightMapFar(string[] lightMapList)
        {
            foreach (string str in lightMapList)
            {
                if (str == "-1")
                {
                    this.LoadScenePathFinding(AppMap.Instance.mapParserII.resourceVo.find_path.ToString());
                    return;
                }
                Singleton<StartLoadingView>.Instance.loaderList.Add(Instance.LoadAsset<Texture2D>("Scene/LightmapFar/" + str + ".assetbundle", new LoadAssetFinish<Texture2D>(AppMap.Instance.mapParserII.LightmapComplete), null, true, true));
            }
        }

        public void LoadScenePathFinding(string pathFinding)
        {
            if (pathFinding == "-1")
            {
                this.LoadRenderSeting(AppMap.Instance.mapParserII.resourceVo.fog_id.ToString());
            }
            else
            {
                Singleton<StartLoadingView>.Instance.loaderList.Add(Instance.LoadAsset<TextAsset>("Scene/Pathfinding/" + pathFinding + ".assetbundle", new LoadAssetFinish<TextAsset>(AppMap.Instance.mapParserII.LoadMapStarData), null, true, true));
            }
        }

        public void LoadScenePreAsset(uint mapId, string key)
        {
            base.StartCoroutine(this.LoadSceneAsset(mapId, key));
        }

        public void LoadSceneResource(string mapId)
        {
            Singleton<StartLoadingView>.Instance.loaderList.Add(Instance.LoadAsset<GameObject>("Scene/" + mapId + ".assetbundle", new LoadAssetFinish<GameObject>(AppMap.Instance.mapParserII.LoadSceneBack), null, true, true));
        }

        public void LoadSkyBox(string skyBoxId)
        {
            if (skyBoxId == "-1")
            {
                AppMap.Instance.mapParserII.LoadSkyBoxComplete(null);
            }
            else
            {
                Singleton<StartLoadingView>.Instance.loaderList.Add(Instance.LoadAsset<Material>("Scene/SkyBox/" + skyBoxId + ".assetbundle", new LoadAssetFinish<Material>(AppMap.Instance.mapParserII.LoadSkyBoxComplete), null, true, true));
            }
        }

        public void PreLoadEffect()
        {
            List<uint> fightGeneralIdList = Singleton<GeneralMode>.Instance.GetFightGeneralIdList();
            EffectMgr.Instance.RemoveUnusedEffect(fightGeneralIdList);
            List<string> needPreloadList = EffectMgr.Instance.GetNeedPreloadList(fightGeneralIdList);
            base.StartCoroutine(EffectMgr.Instance.PreloadResourceList(needPreloadList));
            EffectMgr.Instance.SetLoadedGeneralList(fightGeneralIdList);
        }

        public void RemoveAllSharedAssetBundle(bool isAll = true)
        {
            Dictionary<string, AssetBundleLoader> sharedAssets = this.sharedAssets;
            lock (sharedAssets)
            {
                foreach (AssetBundleLoader loader in this.sharedAssets.Values)
                {
                    loader.UnloadAssetBundle(isAll);
                }
                this.sharedAssets.Clear();
                Resources.UnloadUnusedAssets();
            }
        }

        public bool RemoveCachedAsset<T>(string fileName, string assetName) where T: UnityEngine.Object
        {
            string key = this.getAssetKey(fileName, assetName, typeof(T));
            Dictionary<string, UnityEngine.Object> cachedAssets = this.cachedAssets;
            lock (cachedAssets)
            {
                return this.cachedAssets.Remove(key);
            }
        }

        public void RemoveSharedAssetBundle(string assetBundleName, bool isAll = true)
        {
            Dictionary<string, AssetBundleLoader> sharedAssets = this.sharedAssets;
            lock (sharedAssets)
            {
                if (this.sharedAssets.ContainsKey(assetBundleName))
                {
                    this.sharedAssets[assetBundleName].UnloadAssetBundle(isAll);
                    this.sharedAssets.Remove(assetBundleName);
                    Resources.UnloadUnusedAssets();
                }
            }
        }

        private void Update()
        {
            if (!this.isAssetInit && (Singleton<StartLoadingView>.Instance.gameObject != null))
            {
                Singleton<StartLoadingView>.Instance.loaderList.AddRange(this.initOther());
                this.isAssetInit = true;
            }
            List<AssetBundleLoader> waitingAssetsBundles = this.WaitingAssetsBundles;
            lock (waitingAssetsBundles)
            {
                Dictionary<string, AssetBundleLoader> loadingAssetsBundles = this.loadingAssetsBundles;
                lock (loadingAssetsBundles)
                {
                    if ((this.WaitingAssetsBundles.Count > 0) && (this.loadingAssetsBundles.Count < this.MaxLoadingNum))
                    {
                        AssetBundleLoader loader = this.WaitingAssetsBundles[0];
                        if (!this.loadingAssetsBundles.ContainsKey(loader.fileName))
                        {
                            this.loadingAssetsBundles.Add(loader.fileName, loader);
                            base.StartCoroutine(this.LoadAssetInAssetBundle(loader));
                        }
                        this.WaitingAssetsBundles.RemoveAt(0);
                    }
                }
            }
        }

        public static string dataPathURL
        {
            get
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    return (Application.streamingAssetsPath + "/");
                }
                return ("file:///" + Application.streamingAssetsPath + "/");
            }
        }

        public static AssetManager Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject target = new GameObject("AssetManagerObj");
                    UnityEngine.Object.DontDestroyOnLoad(target);
                    instance = target.AddComponent<AssetManager>();
                    instance.MaxLoadingNum = 1;
                }
                return instance;
            }
        }

        private int MaxLoadingNum { get; set; }

        public static string serverURL
        {
            get
            {
                return "http://localhost/";
            }
        }

        [CompilerGenerated]
        private sealed class <DownloadAndStoreAssetBundle>c__Iterator29 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal LoadAssetBundleFinish <$>callBack;
            internal AssetManager <>f__this;
            internal AssetBundleLoader assetBundleLoader;
            internal LoadAssetBundleFinish callBack;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(true, true));
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.callBack != null)
                        {
                            this.callBack(this.assetBundleLoader);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAndCachedAllAssetInAssetBundle>c__Iterator2E : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal LoadAssetFinish<UnityEngine.Object> <$>callBack;
            internal Dictionary<string, AssetBundleLoader> <$s_153>__0;
            internal AssetManager <>f__this;
            internal AssetBundleLoader assetBundleLoader;
            internal LoadAssetFinish<UnityEngine.Object> callBack;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(false, false));
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.assetBundleLoader.state == DownLoadState.Loaded)
                        {
                            this.<$s_153>__0 = this.<>f__this.sharedAssets;
                            lock (this.<$s_153>__0)
                            {
                                if (!this.<>f__this.sharedAssets.ContainsKey(this.assetBundleLoader.fileName))
                                {
                                    this.<>f__this.sharedAssets.Add(this.assetBundleLoader.fileName, this.assetBundleLoader);
                                }
                            }
                        }
                        if (this.callBack != null)
                        {
                            this.callBack(null);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetBundle>c__Iterator2A : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal LoadAssetBundleFinish <$>callBack;
            internal AssetManager <>f__this;
            internal AssetBundleLoader assetBundleLoader;
            internal LoadAssetBundleFinish callBack;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(false, false));
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.callBack != null)
                        {
                            this.callBack(this.assetBundleLoader);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetInAssetBundle>c__Iterator2B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal string[] <$s_143>__9;
            internal int <$s_144>__10;
            internal List<AssetBundleLoader>.Enumerator <$s_145>__13;
            internal List<AssetBundleLoader>.Enumerator <$s_146>__16;
            internal Dictionary<string, AssetBundleLoader> <$s_147>__18;
            internal Dictionary<string, AssetBundleLoader> <$s_148>__21;
            internal AssetManager <>f__this;
            internal UnityEngine.Object <asset>__19;
            internal string <assetKey>__15;
            internal string <assetName>__0;
            internal bool <cache>__1;
            internal string[] <depAssetBundles>__3;
            internal string <depAssetFileName>__11;
            internal List<AssetBundleLoader> <dependAssetBundles>__4;
            internal AssetBundleLoader <dependFileLoader>__7;
            internal string <depFileAssetName>__5;
            internal string <depPath>__12;
            internal TextAsset <depTxt>__8;
            internal AssetBundleLoader <loader>__14;
            internal AssetBundleLoader <loader>__17;
            internal string <path>__6;
            internal AssetBundleRequest <request>__20;
            internal bool <scene>__2;
            internal AssetBundleLoader assetBundleLoader;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 2:
                        try
                        {
                        }
                        finally
                        {
                            this.<$s_145>__13.Dispose();
                        }
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<assetName>__0 = this.assetBundleLoader.assetName;
                        this.<cache>__1 = this.assetBundleLoader.cache;
                        this.<scene>__2 = this.assetBundleLoader.isScene;
                        this.<depAssetBundles>__3 = null;
                        this.<dependAssetBundles>__4 = new List<AssetBundleLoader>();
                        if (!this.<scene>__2)
                        {
                            goto Label_02D6;
                        }
                        this.<depFileAssetName>__5 = this.<>f__this.getDepAssetFileName(this.assetBundleLoader.fileName);
                        this.<path>__6 = this.<>f__this.getAssetBundleLoaderPath(this.<depFileAssetName>__5);
                        this.<dependFileLoader>__7 = new AssetBundleLoader(this.<path>__6, this.<depFileAssetName>__5, null, null, false, DownLoadState.Init, false, false);
                        this.$current = this.<>f__this.StartCoroutine(this.<dependFileLoader>__7.LoadAssetBundle(false, false));
                        this.$PC = 1;
                        goto Label_05BF;

                    case 1:
                        if (this.<dependFileLoader>__7.state == DownLoadState.Loaded)
                        {
                            this.<depTxt>__8 = this.<dependFileLoader>__7.assetBundle.mainAsset as TextAsset;
                            this.<depAssetBundles>__3 = this.<>f__this.getDependeAssetFilesName(this.<depTxt>__8);
                            this.<dependFileLoader>__7.UnloadAssetBundle(false);
                        }
                        if (this.<depAssetBundles>__3 != null)
                        {
                            this.<$s_143>__9 = this.<depAssetBundles>__3;
                            this.<$s_144>__10 = 0;
                            while (this.<$s_144>__10 < this.<$s_143>__9.Length)
                            {
                                this.<depAssetFileName>__11 = this.<$s_143>__9[this.<$s_144>__10];
                                com.u3d.bases.debug.Log.info(this.<>f__this, "加载" + this.assetBundleLoader.fileName + "的依赖" + this.<depAssetFileName>__11);
                                this.<depPath>__12 = this.<>f__this.getAssetBundleLoaderPath(this.<depAssetFileName>__11);
                                this.<dependAssetBundles>__4.Add(new AssetBundleLoader(this.<depPath>__12, this.<depAssetFileName>__11, null, null, false, DownLoadState.Init, false, false));
                                this.<$s_144>__10++;
                            }
                        }
                        if (this.<dependAssetBundles>__4 == null)
                        {
                            goto Label_02D6;
                        }
                        this.<$s_145>__13 = this.<dependAssetBundles>__4.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 2:
                        break;

                    case 3:
                        this.<assetKey>__15 = this.<>f__this.getAssetKey(this.assetBundleLoader.fileName, this.<assetName>__0, this.assetBundleLoader.assetType);
                        if (!this.<scene>__2)
                        {
                            this.<asset>__19 = null;
                            if (this.assetBundleLoader.asynLoad)
                            {
                                this.<request>__20 = this.<>f__this.LoadAssetAsync(this.assetBundleLoader, this.<assetName>__0, this.assetBundleLoader.assetType);
                                if (this.<request>__20 == null)
                                {
                                    goto Label_04AC;
                                }
                                this.$current = this.<request>__20;
                                this.$PC = 4;
                                goto Label_05BF;
                            }
                            this.<asset>__19 = this.<>f__this.LoadAsset(this.assetBundleLoader, this.<assetName>__0, this.assetBundleLoader.assetType);
                            goto Label_04AC;
                        }
                        this.assetBundleLoader.UnloadAssetBundle(false);
                        this.<$s_146>__16 = this.<dependAssetBundles>__4.GetEnumerator();
                        try
                        {
                            while (this.<$s_146>__16.MoveNext())
                            {
                                this.<loader>__17 = this.<$s_146>__16.Current;
                                this.<loader>__17.UnloadAssetBundle(false);
                            }
                        }
                        finally
                        {
                            this.<$s_146>__16.Dispose();
                        }
                        this.<>f__this.LoadScenePreAsset(uint.Parse(this.<assetName>__0), this.<assetKey>__15);
                        this.<$s_147>__18 = this.<>f__this.loadingAssetsBundles;
                        lock (this.<$s_147>__18)
                        {
                            this.<>f__this.loadingAssetsBundles.Remove(this.assetBundleLoader.fileName);
                        }
                        GC.Collect();
                        goto Label_05B6;

                    case 4:
                        this.<asset>__19 = this.<request>__20.asset;
                        goto Label_04AC;

                    default:
                        goto Label_05BD;
                }
                try
                {
                    switch (num)
                    {
                        case 2:
                            if (this.<loader>__14.state == DownLoadState.Loaded)
                            {
                                com.u3d.bases.debug.Log.info(this.<>f__this, "成功加载" + this.<loader>__14.path + this.<loader>__14.fileName);
                            }
                            break;
                    }
                    while (this.<$s_145>__13.MoveNext())
                    {
                        this.<loader>__14 = this.<$s_145>__13.Current;
                        this.$current = this.<>f__this.StartCoroutine(this.<loader>__14.LoadAssetBundle(false, false));
                        this.$PC = 2;
                        flag = true;
                        goto Label_05BF;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                    }
                    this.<$s_145>__13.Dispose();
                }
            Label_02D6:
                this.$current = this.<>f__this.StartCoroutine(this.assetBundleLoader.LoadAssetBundle(false, false));
                this.$PC = 3;
                goto Label_05BF;
            Label_04AC:
                if ((this.<asset>__19 == null) && !this.assetBundleLoader.fileName.Contains("bust"))
                {
                    com.u3d.bases.debug.Log.error(this.<>f__this, "asset==null" + this.assetBundleLoader.fileName);
                }
                if (this.<cache>__1)
                {
                    this.<>f__this.cachedAssets[this.<assetKey>__15] = this.<asset>__19;
                }
                if ((this.assetBundleLoader.state == DownLoadState.Loaded) && !this.assetBundleLoader.delayUnload)
                {
                    this.assetBundleLoader.UnloadAssetBundle(false);
                }
                this.<$s_148>__21 = this.<>f__this.loadingAssetsBundles;
                lock (this.<$s_148>__21)
                {
                    this.<>f__this.loadingAssetsBundles.Remove(this.assetBundleLoader.fileName);
                }
                ShaderUtils.ResetObjectShader(this.<asset>__19);
                this.<>f__this.callBack(this.<assetKey>__15, this.<asset>__19);
            Label_05B6:
                this.$PC = -1;
            Label_05BD:
                return false;
            Label_05BF:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadBinDataXmlFromServer>c__Iterator28 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal string <$>path;
            internal AssetManager <>f__this;
            internal byte[] <bytes>__1;
            internal object <dataVo>__2;
            internal WWW <w3>__0;
            internal string path;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<w3>__0 = new WWW(this.path);
                        this.$current = this.<w3>__0;
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (string.IsNullOrEmpty(this.<w3>__0.error))
                        {
                            this.<bytes>__1 = this.<w3>__0.bytes;
                            if (this.<bytes>__1 != null)
                            {
                                com.u3d.bases.debug.Log.info(this.<>f__this, "-Start() 4、基础数据长度:" + (this.<bytes>__1.Length / 0x400) + "KB");
                                UnityEngine.Debug.Log("-Start() 4、基础数据长度:" + (this.<bytes>__1.Length / 0x400) + "KB");
                                this.<dataVo>__2 = SerializerUtils.binaryDerialize(this.<bytes>__1);
                                BaseDataMgr.instance.init(this.<dataVo>__2);
                                Singleton<ConfigConst>.Instance.SetConfigData(BaseDataMgr.instance.GetDicByType<SysConfigVo>());
                                com.u3d.bases.debug.Log.info(this.<>f__this, "-Start() 5、基础数据初始化【OK】");
                            }
                            this.<>f__this.initCountCallBack(null);
                            this.$PC = -1;
                            break;
                        }
                        UnityEngine.Debug.Log(this.<w3>__0.error);
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadSceneAsset>c__Iterator2F : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal string <$>key;
            internal uint <$>mapId;
            internal IEnumerator<SysReadyLoadVo> <$s_157>__4;
            internal AssetManager <>f__this;
            internal int <i>__1;
            internal IList<SysReadyLoadVo> <list>__6;
            internal IPreloader <loader>__3;
            internal SysReadyLoadVo <preload>__5;
            internal IList<SysReadyLoadVo> <preloadList>__2;
            internal int[] <subTypeList>__0;
            internal string key;
            internal uint mapId;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            if (this.<$s_157>__4 == null)
                            {
                            }
                            this.<$s_157>__4.Dispose();
                        }
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        if (this.<>f__this.scenePreloadHistory.Contains(this.mapId))
                        {
                            StartLoadingView instance = Singleton<StartLoadingView>.Instance;
                            instance.PreLoadedNum += Singleton<StartLoadingView>.Instance.PreLoadNum;
                            goto Label_01F1;
                        }
                        com.u3d.bases.debug.Log.warin(this.<>f__this, "-startLoadScene() 异步加载场景特效资源" + this.mapId);
                        this.<>f__this.scenePreloadHistory.Add(this.mapId);
                        this.<subTypeList>__0 = new int[] { 1, 5 };
                        this.<i>__1 = 0;
                        goto Label_01BE;

                    case 1:
                        break;

                    default:
                        goto Label_020A;
                }
            Label_00F8:
                try
                {
                    switch (num)
                    {
                        case 1:
                        {
                            StartLoadingView local1 = Singleton<StartLoadingView>.Instance;
                            local1.PreLoadedNum += this.<list>__6.Count;
                            break;
                        }
                    }
                    while (this.<$s_157>__4.MoveNext())
                    {
                        this.<preload>__5 = this.<$s_157>__4.Current;
                        this.<list>__6 = new List<SysReadyLoadVo>();
                        this.<list>__6.Add(this.<preload>__5);
                        this.$current = this.<>f__this.StartCoroutine(this.<loader>__3.PreloadResourceList(this.<list>__6));
                        this.$PC = 1;
                        flag = true;
                        return true;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                    }
                    if (this.<$s_157>__4 == null)
                    {
                    }
                    this.<$s_157>__4.Dispose();
                }
            Label_01B0:
                this.<i>__1++;
            Label_01BE:
                if (this.<i>__1 < this.<subTypeList>__0.Length)
                {
                    this.<preloadList>__2 = BaseDataMgr.instance.GetScenePreLoadList(this.mapId, this.<subTypeList>__0[this.<i>__1]);
                    if (this.<preloadList>__2.Count <= 0)
                    {
                        goto Label_01B0;
                    }
                    this.<loader>__3 = PreloaderFactory.Instance.GetPreLoader(this.<preloadList>__2[0]);
                    this.<$s_157>__4 = this.<preloadList>__2.GetEnumerator();
                    num = 0xfffffffd;
                    goto Label_00F8;
                }
            Label_01F1:
                this.<>f__this.callBack(this.key, null);
                this.$PC = -1;
            Label_020A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadSceneInResource>c__Iterator2D : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal LoadAssetFinish<GameObject> <$>callBack;
            internal uint <$>mapResourceId;
            internal AssetManager <>f__this;
            internal AssetBundleLoader <s>__0;
            internal LoadAssetFinish<GameObject> callBack;
            internal uint mapResourceId;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<s>__0 = new AssetBundleLoader(string.Empty, string.Empty, typeof(GameObject), null, false, DownLoadState.Init, false, false);
                        this.$current = this.<>f__this.StartCoroutine(this.<>f__this.LoadSceneLevel(this.<s>__0, this.mapResourceId.ToString()));
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.callBack != null)
                        {
                            this.<>f__this.addCallBack<GameObject>(this.mapResourceId.ToString(), this.callBack);
                        }
                        this.<>f__this.LoadScenePreAsset(this.mapResourceId, this.mapResourceId.ToString());
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadSceneLevel>c__Iterator2C : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal AssetBundleLoader <$>assetBundleLoader;
            internal string <$>sceneName;
            internal AsyncOperation <loadLevel>__0;
            internal AssetBundleLoader assetBundleLoader;
            internal string sceneName;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<loadLevel>__0 = Application.LoadLevelAsync(this.sceneName);
                        this.assetBundleLoader.loadSceneOperation = this.<loadLevel>__0;
                        this.$current = this.<loadLevel>__0;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        public delegate void SetUpFinish();
    }
}

