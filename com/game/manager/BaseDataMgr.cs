﻿namespace com.game.manager
{
    using com.game;
    using com.game.data;
    using com.game.utils;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class BaseDataMgr
    {
        [CompilerGenerated]
        private static Func<object, SysGuideVo> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<object, SysArenaRankAwardVo> <>f__am$cache6;
        private Dictionary<string, System.Type> clzTypeData = BaseDataConst.registerClzType();
        private Dictionary<string, Dictionary<uint, object>> data;
        private Dictionary<string, Dictionary<uint, object>> editorData = new Dictionary<string, Dictionary<uint, object>>();
        public static BaseDataMgr instance = new BaseDataMgr();
        private IList<string> preLoadMonList = new List<string>();

        private bool ContainsType(string vipType, string type)
        {
            foreach (string str in StringUtils.GetValueListFromString(vipType, ','))
            {
                if (str.Equals(type))
                {
                    return true;
                }
            }
            return false;
        }

        public Dictionary<uint, object> GetAchieveConfigureDic()
        {
            return this.data["SysAchieveConfigureVo"];
        }

        public SysAchievementVo GetAchievement(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysAchievementVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysAchievementVo) dictionary[id]));
        }

        public Dictionary<uint, object> GetAchievementTemplateList()
        {
            return this.data["SysAchievementVo"];
        }

        public List<SysAchievementVo> GetAchievementTemplateListByType(int type)
        {
            List<SysAchievementVo> list = new List<SysAchievementVo>();
            Dictionary<uint, object> dictionary = this.data["SysAchievementVo"];
            foreach (object obj2 in dictionary.Values)
            {
                if ((obj2 as SysAchievementVo).type == type)
                {
                    list.Add(obj2 as SysAchievementVo);
                }
            }
            return list;
        }

        public Dictionary<uint, object> GetAllGeneralList()
        {
            return this.data["SysGeneralVo"];
        }

        public SysAnnouncementVo GetAnnouncementTemplate(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysAnnouncementVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysAnnouncementVo) dictionary[id]));
        }

        public Dictionary<uint, object> GetAnnouncementTemplateList()
        {
            return this.data["SysAnnouncementVo"];
        }

        public List<SysArenaRankAwardVo> GetArenaRankAwardTemplateList()
        {
            Dictionary<uint, object> dictionary = this.data["SysArenaRankAwardVo"];
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = val => val as SysArenaRankAwardVo;
            }
            return dictionary.Values.Select<object, SysArenaRankAwardVo>(<>f__am$cache6).ToList<SysArenaRankAwardVo>();
        }

        public SysArenaVo GetArenaVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysArenaVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysArenaVo) dictionary[id]));
        }

        public SysCameraInfoVo GetCameraInfoVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysCameraInfoVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysCameraInfoVo) dictionary[id]));
        }

        public System.Type getClzType(string clzType)
        {
            return (!this.clzTypeData.ContainsKey(clzType) ? null : this.clzTypeData[clzType]);
        }

        public Dictionary<uint, object> GetComboHitTemplates()
        {
            return this.data["SysComboHitVo"];
        }

        public SysDailyTaskVo GetDailyTask(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDailyTaskVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDailyTaskVo) dictionary[id]));
        }

        public T GetDataById<T>(uint id) where T: class
        {
            Dictionary<uint, object> dictionary;
            if (AppMap.Instance.IsEditor)
            {
                dictionary = this.editorData[typeof(T).Name];
            }
            else
            {
                dictionary = this.data[typeof(T).Name];
            }
            if (dictionary.ContainsKey(id))
            {
                return (dictionary[id] as T);
            }
            return null;
        }

        public T GetDataByTypeAndId<T>(string type, uint id)
        {
            Dictionary<uint, object> dictionary;
            if (AppMap.Instance.IsEditor)
            {
                dictionary = this.editorData[type];
            }
            else
            {
                dictionary = this.data[type];
            }
            if (dictionary.ContainsKey(id))
            {
                return (T) dictionary[id];
            }
            return default(T);
        }

        public Dictionary<uint, object> GetDicByType<T>() where T: class
        {
            try
            {
                Dictionary<uint, object> dictionary;
                if (AppMap.Instance.IsEditor)
                {
                    dictionary = this.editorData[typeof(T).Name];
                }
                else
                {
                    dictionary = this.data[typeof(T).Name];
                }
                return dictionary;
            }
            catch (Exception)
            {
                Debug.Log(typeof(T).Name);
                return null;
            }
        }

        public SysDoorVo getDoor(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDoorVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDoorVo) dictionary[id]));
        }

        public SysDungeonVo getDungeon(uint id)
        {
            if (AppMap.Instance.IsEditor)
            {
                Dictionary<uint, object> dictionary = this.editorData["SysDungeonVo"];
                return (!dictionary.ContainsKey(id) ? null : ((SysDungeonVo) dictionary[id]));
            }
            Dictionary<uint, object> dictionary2 = this.data["SysDungeonVo"];
            return (!dictionary2.ContainsKey(id) ? null : ((SysDungeonVo) dictionary2[id]));
        }

        public SysDungeonChapterVo getDungeonChapter(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonChapterVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonChapterVo) dictionary[id]));
        }

        public SysDungeonChestVo GetDungeonChestVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonChestVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonChestVo) dictionary[id]));
        }

        public SysDungeonMissionVo getDungeonMission(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonMissionVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonMissionVo) dictionary[id]));
        }

        public SysDungeonMonsterVo getDungeonMonster(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonMonsterVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonMonsterVo) dictionary[id]));
        }

        public SysDungeonStarAwardVo GetDungeonStarAwardVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonStarAwardVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonStarAwardVo) dictionary[id]));
        }

        public SysDungeonTaskVo GetDungeonTaskVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonTaskVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonTaskVo) dictionary[id]));
        }

        public SysEquipUpVo GetEquipUpgradeVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysEquipUpVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysEquipUpVo) dictionary[id]));
        }

        public SysErrorCodeVo GetErrorCodeVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysErrorCodeVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysErrorCodeVo) dictionary[id]));
        }

        public SysGemComposeVo GetGemComposeVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysGemComposeVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysGemComposeVo) dictionary[id]));
        }

        public SysGeneralExpVo GetGeneralExpVo(uint lv)
        {
            Dictionary<uint, object> dictionary = this.data["SysGeneralExpVo"];
            return (!dictionary.ContainsKey(lv) ? null : ((SysGeneralExpVo) dictionary[lv]));
        }

        public SysGeneralStarVo GetGeneralStarVo(uint id, int star)
        {
            Dictionary<uint, object> dictionary = this.data["SysGeneralStarVo"];
            uint key = (id * 100) + ((uint) star);
            return (!dictionary.ContainsKey(key) ? null : ((SysGeneralStarVo) dictionary[key]));
        }

        public SysGeneralVo GetGeneralVo(uint id, byte quality)
        {
            uint key = (id * 100) + quality;
            if (AppMap.Instance.IsEditor)
            {
                Dictionary<uint, object> dictionary = this.editorData["SysGeneralVo"];
                return (!dictionary.ContainsKey(key) ? null : ((SysGeneralVo) dictionary[key]));
            }
            Dictionary<uint, object> dictionary2 = this.data["SysGeneralVo"];
            return (!dictionary2.ContainsKey(key) ? null : ((SysGeneralVo) dictionary2[key]));
        }

        public SysItemsVo getGoodsVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysItemsVo"];
            return (!dictionary.ContainsKey(id) ? null : (dictionary[id] as SysItemsVo));
        }

        public List<SysGuideVo> GetGuideVoList()
        {
            Dictionary<uint, object> dictionary = this.data["SysGuideVo"];
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = guideVo => guideVo as SysGuideVo;
            }
            return dictionary.Values.Select<object, SysGuideVo>(<>f__am$cache5).ToList<SysGuideVo>();
        }

        public SysJobDamageRatioVo GetJobDamageRatio(uint job1, uint job2)
        {
            uint key = (job1 * 100) + job2;
            Dictionary<uint, object> dictionary = this.data["SysJobDamageRatioVo"];
            return (!dictionary.ContainsKey(key) ? null : ((SysJobDamageRatioVo) dictionary[key]));
        }

        public SysLanguageVo GetLangVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysLanguageVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysLanguageVo) dictionary[id]));
        }

        public SysMapVo GetMapVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysMapVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysMapVo) dictionary[id]));
        }

        public SysMaterialComposeVo GetMaterialCompiseVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysMaterialComposeVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysMaterialComposeVo) dictionary[id]));
        }

        public SysModelVo GetModelVo(uint type)
        {
            Dictionary<uint, object> dictionary = this.data["SysModelVo"];
            return (!dictionary.ContainsKey(type) ? null : ((SysModelVo) dictionary[type]));
        }

        public SysMonBornActionVo GetMonBornAtionVo(uint type)
        {
            Dictionary<uint, object> dictionary = this.data["SysMonBornActionVo"];
            return (!dictionary.ContainsKey(type) ? null : ((SysMonBornActionVo) dictionary[type]));
        }

        public SysMonsterBornTypeVo GetMonBornTypeVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysMonsterBornTypeVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysMonsterBornTypeVo) dictionary[id]));
        }

        public IList<string> GetMonPreLoadList(uint mapId)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonMon"];
            uint[] numArray = new uint[] { (mapId * 100) + 1, (mapId * 100) + 2, (mapId * 100) + 3 };
            for (int i = 0; i < numArray.Length; i++)
            {
                if (dictionary.ContainsKey(numArray[i]))
                {
                    SysDungeonMon mon = (SysDungeonMon) dictionary[numArray[i]];
                    string[] monsterList = StringUtils.GetMonsterList(mon.list);
                    int num = monsterList.Length / 3;
                    for (int j = 0; j < num; j++)
                    {
                        if (!this.preLoadMonList.Contains(monsterList[1 + (j * 3)]))
                        {
                            this.preLoadMonList.Add(monsterList[1 + (j * 3)]);
                        }
                    }
                }
            }
            return this.preLoadMonList;
        }

        public SysMonsterAdaptRuleVo GetMonsterAdapt(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysMonsterAdaptRuleVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysMonsterAdaptRuleVo) dictionary[id]));
        }

        public SysMonsterDropVo getMonsterDrop(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysMonsterDropVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysMonsterDropVo) dictionary[id]));
        }

        public SysmonsterPaoPaoVo GetMonsterPaoPao(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysmonsterPaoPaoVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysmonsterPaoPaoVo) dictionary[id]));
        }

        public SysmonsterPaoPaoVo GetMonsterPaoPaoTemplateByMonsterId(int id)
        {
            Dictionary<uint, object> dictionary = this.data["SysmonsterPaoPaoVo"];
            foreach (KeyValuePair<uint, object> pair in dictionary)
            {
                SysmonsterPaoPaoVo vo = (SysmonsterPaoPaoVo) pair.Value;
                if (vo.monster_id == id)
                {
                    return vo;
                }
            }
            return null;
        }

        public SysNpcVo GetNpcVoById(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysNpcVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysNpcVo) dictionary[id]));
        }

        public Dictionary<uint, object> GetRoleIconList()
        {
            return this.data["SysRoleIconVo"];
        }

        public SysRoleInfoVo GetRoleInfo(int level)
        {
            Dictionary<uint, object> dictionary = this.data["SysRoleInfoVo"];
            return (!dictionary.ContainsKey((uint) level) ? null : ((SysRoleInfoVo) dictionary[(uint) level]));
        }

        public IList<SysReadyLoadVo> GetScenePreLoadList(uint mapId, int subtype)
        {
            IList<SysReadyLoadVo> list = new List<SysReadyLoadVo>();
            Dictionary<uint, object> dictionary = this.data["SysReadyLoadVo"];
            foreach (object obj2 in dictionary.Values)
            {
                SysReadyLoadVo item = (SysReadyLoadVo) obj2;
                if ((((item.maintype == 1) && (subtype == item.subtype)) && ((mapId == Convert.ToUInt32(item.mainid)) || (Convert.ToUInt32(item.mainid) == 0))) && ((MeVo.instance.job == item.job) || (item.job == 0)))
                {
                    list.Add(item);
                }
            }
            return list;
        }

        public SysSceneResourceVo getSceneResourceVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysSceneResourceVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysSceneResourceVo) dictionary[id]));
        }

        public SysSceneVo GetSceneVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysSceneVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysSceneVo) dictionary[id]));
        }

        public SysShopVo GetShop(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysShopVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysShopVo) dictionary[id]));
        }

        public SysShopItemVo GetShopItem(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysShopItemVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysShopItemVo) dictionary[id]));
        }

        public SysShopRefreshVo GetShopRefresh(byte type, int count)
        {
            Dictionary<uint, object> dictionary = this.data["SysShopRefreshVo"];
            uint key = (uint) ((type * 100) + count);
            return (!dictionary.ContainsKey(key) ? null : ((SysShopRefreshVo) dictionary[key]));
        }

        public List<SysItemsVo> GetStoneTemplateList()
        {
            List<SysItemsVo> list = new List<SysItemsVo>();
            Dictionary<uint, object> dictionary = this.data["SysItemsVo"];
            foreach (object obj2 in dictionary.Values)
            {
                SysItemsVo item = obj2 as SysItemsVo;
                if (item.type == 4)
                {
                    list.Add(item);
                }
            }
            return list;
        }

        public SysStoryVo GetStoryVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysStoryVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysStoryVo) dictionary[id]));
        }

        public Dictionary<uint, object> GetStoryVoList()
        {
            return this.data["SysStoryVo"];
        }

        public SysStrengthVo GetStrenthVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysStrengthVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysStrengthVo) dictionary[id]));
        }

        public SysBuffVo GetSysBuffVo(uint id, uint level)
        {
            uint key = (id * 100) + level;
            if (AppMap.Instance.IsEditor)
            {
                Dictionary<uint, object> dictionary = this.editorData["SysBuffVo"];
                return (!dictionary.ContainsKey(key) ? null : ((SysBuffVo) dictionary[key]));
            }
            Dictionary<uint, object> dictionary2 = this.data["SysBuffVo"];
            return (!dictionary2.ContainsKey(key) ? null : ((SysBuffVo) dictionary2[key]));
        }

        public SysDungeonRewardVo GetSysDungeonReward(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonRewardVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonRewardVo) dictionary[id]));
        }

        public SysDungeonTreeVo GetSysDungeonTree(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysDungeonTreeVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysDungeonTreeVo) dictionary[id]));
        }

        public SysEffectTimeVo GetSysEffectTimeVo(uint id)
        {
            if (AppMap.Instance.IsEditor)
            {
                Dictionary<uint, object> dictionary = this.editorData["SysEffectTimeVo"];
                return (!dictionary.ContainsKey(id) ? null : ((SysEffectTimeVo) dictionary[id]));
            }
            Dictionary<uint, object> dictionary2 = this.data["SysEffectTimeVo"];
            return (!dictionary2.ContainsKey(id) ? null : ((SysEffectTimeVo) dictionary2[id]));
        }

        public SysGeneralVo GetSysGeneralVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysGeneralVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysGeneralVo) dictionary[id]));
        }

        public SysLogDataVo GetSysLogDataVo(uint id, uint type)
        {
            uint key = (type * 100) + id;
            Dictionary<uint, object> dictionary = this.data["SysLogDataVo"];
            return (!dictionary.ContainsKey(key) ? null : ((SysLogDataVo) dictionary[key]));
        }

        public SysMonsterVo getSysMonsterVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysMonsterVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysMonsterVo) dictionary[id]));
        }

        public SysPriceVo GetSysPriceVo(uint id, uint type)
        {
            uint key = (id * 100) + type;
            Dictionary<uint, object> dictionary = this.data["SysPriceVo"];
            return (!dictionary.ContainsKey(key) ? null : ((SysPriceVo) dictionary[key]));
        }

        public SysRoleBaseInfoVo GetSysRoleBaseInfo(uint jobId)
        {
            Dictionary<uint, object> dictionary = this.data["SysRoleBaseInfoVo"];
            return (!dictionary.ContainsKey(jobId) ? null : ((SysRoleBaseInfoVo) dictionary[jobId]));
        }

        public SysRumor GetSysRumor(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysRumor"];
            return (!dictionary.ContainsKey(id) ? null : ((SysRumor) dictionary[id]));
        }

        public SysSkillActionVo GetSysSkillActionVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysSkillActionVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysSkillActionVo) dictionary[id]));
        }

        public SysSkillBaseVo GetSysSkillBaseVo(uint id)
        {
            if (AppMap.Instance.IsEditor)
            {
                Dictionary<uint, object> dictionary = this.editorData["SysSkillBaseVo"];
                return (!dictionary.ContainsKey(id) ? null : ((SysSkillBaseVo) dictionary[id]));
            }
            Dictionary<uint, object> dictionary2 = this.data["SysSkillBaseVo"];
            return (!dictionary2.ContainsKey(id) ? null : ((SysSkillBaseVo) dictionary2[id]));
        }

        public SysSkillLevelVo GetSysSkillLevelVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysSkillLevelVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysSkillLevelVo) dictionary[id]));
        }

        public SysTask GetSysTaskVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysTask"];
            return (!dictionary.ContainsKey(id) ? null : ((SysTask) dictionary[id]));
        }

        public SysVipMallVo GetSysVipMallVo(uint id, uint type)
        {
            uint key = (id * 100) + type;
            Dictionary<uint, object> dictionary = this.data["SysVipMallVo"];
            return (!dictionary.ContainsKey(key) ? null : ((SysVipMallVo) dictionary[key]));
        }

        public List<SysVipMallVo> GetSysVipMallVoListByType(int type)
        {
            List<SysVipMallVo> list = new List<SysVipMallVo>();
            Dictionary<uint, object> dictionary = this.data["SysVipMallVo"];
            foreach (object obj2 in dictionary.Values)
            {
                if ((obj2 as SysVipMallVo).type == type)
                {
                    list.Add(obj2 as SysVipMallVo);
                }
            }
            return list;
        }

        public SysTrap GetTrapVoById(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysTrap"];
            return (!dictionary.ContainsKey(id) ? null : ((SysTrap) dictionary[id]));
        }

        public SysVipBoxVo GetVipBox(int count)
        {
            Dictionary<uint, object> dictionary = this.data["SysVipBoxVo"];
            return (!dictionary.ContainsKey((uint) count) ? null : ((SysVipBoxVo) dictionary[(uint) count]));
        }

        public List<SysVipDroitVo> GetVipDroits(uint vipLevel)
        {
            <GetVipDroits>c__AnonStoreyCF ycf = new <GetVipDroits>c__AnonStoreyCF();
            List<SysVipDroitVo> list = new List<SysVipDroitVo>();
            Dictionary<uint, object> dictionary = this.data["SysVipDroitVo"];
            foreach (uint num2 in dictionary.Keys)
            {
                ycf.vo = (SysVipDroitVo) dictionary[num2];
                if (ycf.vo.vip <= vipLevel)
                {
                    int num = list.FindIndex(new Predicate<SysVipDroitVo>(ycf.<>m__37));
                    if (num < 0)
                    {
                        list.Add(ycf.vo);
                    }
                    else if (list[num].vip < ycf.vo.vip)
                    {
                        list[num] = ycf.vo;
                    }
                }
            }
            return list;
        }

        public SysVipInfoVo GetVIPInfo(uint level)
        {
            Dictionary<uint, object> dictionary = this.data["SysVipInfoVo"];
            return (!dictionary.ContainsKey(level) ? null : ((SysVipInfoVo) dictionary[level]));
        }

        public int GetVIPLowestGrade(int type)
        {
            Dictionary<uint, object> dictionary = this.data["SysVipDroitVo"];
            int vip = 0x2710;
            foreach (object obj2 in dictionary.Values)
            {
                SysVipDroitVo vo = (SysVipDroitVo) obj2;
                if (this.ContainsType(vo.type.ToString(), type.ToString()) && (vo.vip < vip))
                {
                    vip = vo.vip;
                }
            }
            return vip;
        }

        public SysMonsterMobaVo GetWifiPvpMonsterVO(uint id, int level)
        {
            Dictionary<uint, object> dictionary = this.data["SysMonsterMobaVo"];
            uint key = (id * 100) + ((uint) level);
            Log.AI(null, string.Concat(new object[] { "GetWifiPvpMonsterVO id ", id, " level ", level, " unikey ", key }));
            if (Main.mainObj.TestNormalSoldier)
            {
                key = (id * 100) + 1;
                Log.AI(null, "Try Default Key " + key);
                return (!dictionary.ContainsKey(key) ? null : ((SysMonsterMobaVo) dictionary[key]));
            }
            if (!dictionary.ContainsKey(key))
            {
                Log.AI(null, " Not Find uniKey " + key);
                return null;
            }
            Log.AI(null, "GetData Ok " + key);
            return (SysMonsterMobaVo) dictionary[key];
        }

        public SysWifiPvpVo GetWifiPvpVo(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysWifiPvpVo"];
            return (!dictionary.ContainsKey(id) ? null : ((SysWifiPvpVo) dictionary[id]));
        }

        public void init(object data)
        {
            this.data = (Dictionary<string, Dictionary<uint, object>>) data;
        }

        public bool IsItem(uint id)
        {
            Dictionary<uint, object> dictionary = this.data["SysItemsVo"];
            return dictionary.ContainsKey(id);
        }

        public void RegisterDictData(string key, Dictionary<uint, object> dict)
        {
            if (this.editorData != null)
            {
                if (this.editorData.ContainsKey(key))
                {
                    this.editorData.Remove(key);
                }
                this.editorData.Add(key, dict);
            }
        }

        public void UpdateDictData(string key, uint id, object value)
        {
            if (this.editorData != null)
            {
                if (this.editorData.ContainsKey(key))
                {
                    if (this.editorData[key].ContainsKey(id))
                    {
                        this.editorData[key].Remove(id);
                    }
                    this.editorData[key].Add(id, value);
                }
                else
                {
                    Dictionary<uint, object> dictionary2 = new Dictionary<uint, object>();
                    dictionary2.Add(id, value);
                    Dictionary<uint, object> dictionary = dictionary2;
                    this.editorData.Add(key, dictionary);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetVipDroits>c__AnonStoreyCF
        {
            internal SysVipDroitVo vo;

            internal bool <>m__37(SysVipDroitVo item)
            {
                return (item.type == this.vo.type);
            }
        }
    }
}

