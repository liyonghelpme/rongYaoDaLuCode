﻿namespace com.game.manager
{
    using com.game.module.core;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CameraEffectManager : MonoBehaviour
    {
        private static Camera _mainCamera;
        private static GameObject _mainCameraGameObject;
        private static bool isShake;

        public static void NormalAttackShake()
        {
        }

        public static void ScaleInCamera(float stepSize, float toSize)
        {
            CoroutineManager.StartCoroutine(StepScaleOutCamera(stepSize, toSize), true);
        }

        public static void ScaleOutCamera(float stepSize, float toSize)
        {
            CoroutineManager.StartCoroutine(StepScaleOutCamera(stepSize, toSize), true);
        }

        public static void ShakeCamera(float delaySeconds, float shakeSeconds)
        {
        }

        [DebuggerHidden]
        private static IEnumerator StepScaleInCamera(float stepSize, float toSize)
        {
            return new <StepScaleInCamera>c__Iterator22();
        }

        [DebuggerHidden]
        private static IEnumerator StepScaleOutCamera(float stepSize, float toSize)
        {
            return new <StepScaleOutCamera>c__Iterator21();
        }

        public static void TweenShakeCamera(float delaySeconds, float shakeSeconds)
        {
        }

        [CompilerGenerated]
        private sealed class <StepScaleInCamera>c__Iterator22 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = 0;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StepScaleOutCamera>c__Iterator21 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = 0;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

