﻿namespace com.game.manager
{
    using com.game;
    using com.game.data;
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ComboHitMgr : MonoBehaviour
    {
        private int _attackNumberForCombo;
        private int _fixSecsAfterUseSuperSkill;
        private int _lastComboTofAttackNum = -1;
        [CompilerGenerated]
        private static Func<SysComboHitVo, int> <>f__am$cache14;
        private int attackEnergy;
        private bool attackPause;
        private float attackPauseTime;
        public List<SysComboHitVo> comboHitT = new List<SysComboHitVo>();
        private int curAttackCounter;
        private int curCutCounter;
        private int cutTimeGap = 30;
        private int elapsedSec;
        public const int ENERGY_PER_SEC = 0;
        private bool isComboHitTSorted;
        private bool isSuperSkillAvailable;
        private float lastAttackTime;
        private float lastCutTime;
        private float lastRecordTime;
        private int maxAttackCounter;
        private float maxEnergy = 50f;
        private const float SHOW_LEFT_INTERVAL = 0.1f;
        public const uint SUPER_SKILL_UNIKEY = 0x35a4ece9;
        private float timeEnergy;
        private float totalEnergy;

        public void AddAttackNum(int attackNum = 1)
        {
            this.curAttackCounter += attackNum;
            this._attackNumberForCombo = this.curAttackCounter;
            this.HandleAddAttackEnergy();
            if (this.maxAttackCounter < this.curAttackCounter)
            {
                this.maxAttackCounter = this.curAttackCounter;
            }
            this.lastAttackTime = Time.time;
            MeVo.instance.SeqAttack = (uint) (this.curAttackCounter / 100);
            base.StartCoroutine(this.ShowAttack());
        }

        public void ClearWhenQuitDup()
        {
            this.Reset();
            Singleton<MapMode>.Instance.SyncAttackEnergy();
        }

        private int GetAddEnergyByHit(int hit)
        {
            int count = this.comboHitT.Count;
            int num2 = 1;
            while (num2 < count)
            {
                if (num2 == (count - 1))
                {
                    if ((hit >= this.comboHitT[num2 - 1].combo_count) && (this._lastComboTofAttackNum != (num2 - 1)))
                    {
                        this._lastComboTofAttackNum = num2 - 1;
                        return this.comboHitT[num2 - 1].total_add_energy;
                    }
                    return 0;
                }
                if (((hit < this.comboHitT[num2].combo_count) && (hit >= this.comboHitT[num2 - 1].combo_count)) && (this._lastComboTofAttackNum != (num2 - 1)))
                {
                    this._lastComboTofAttackNum = num2 - 1;
                    return this.comboHitT[num2 - 1].total_add_energy;
                }
                return 0;
            }
            return 0;
        }

        private SysComboHitVo GetProperTemplateByHitRange(int hit)
        {
            int count = this.comboHitT.Count;
            if (count <= 0)
            {
                return null;
            }
            int idx = 1;
            return this.GetProperTemplateByHitRange(hit, count, idx);
        }

        private SysComboHitVo GetProperTemplateByHitRange(int hit, int Len, int idx)
        {
            if ((Len == idx) && (this.comboHitT[hit - 1].combo_count < hit))
            {
                return null;
            }
            if ((this.comboHitT[hit - 1].combo_count <= (hit - 1)) && (this.comboHitT[hit].combo_count > (hit - 1)))
            {
                return this.comboHitT[hit - 1];
            }
            return this.GetProperTemplateByHitRange(hit, Len, ++idx);
        }

        private void HandleAddAttackEnergy()
        {
            int addEnergyByHit = this.GetAddEnergyByHit(this._attackNumberForCombo);
            this.attackEnergy += addEnergyByHit;
            if (this.attackEnergy > this.maxEnergy)
            {
                this.attackEnergy = (int) this.maxEnergy;
            }
            this.totalEnergy = this.attackEnergy + this.timeEnergy;
        }

        private void InitView()
        {
            Singleton<ComboHitView>.Instance._maxEnergy = (int) this.maxEnergy;
            Singleton<ComboHitView>.Instance._energy = (int) this.totalEnergy;
            Singleton<ComboHitView>.Instance._btnGroupSkillAvailable = this.isSuperSkillAvailable;
        }

        public void PauseAttack()
        {
            if (Singleton<ComboHitView>.Instance.gameObject.activeSelf)
            {
                this.attackPause = true;
                this.attackPauseTime = Time.time;
            }
        }

        public void Reset()
        {
            this.totalEnergy = 0f;
            this.attackEnergy = 0;
            this.timeEnergy = 0f;
            this.curAttackCounter = 0;
            this._lastComboTofAttackNum = -1;
        }

        public void ResetAfterUseSuperSkill()
        {
            this._attackNumberForCombo = 0;
            this._fixSecsAfterUseSuperSkill += this.elapsedSec + 1;
            this.totalEnergy = 0f;
            this.attackEnergy = 0;
            this.timeEnergy = 0f;
            this._lastComboTofAttackNum = -1;
        }

        public void ResumeAttack()
        {
            if (this.attackPause)
            {
                this.lastAttackTime += Time.time - this.attackPauseTime;
                this.attackPause = false;
                this.UpdateAttackNumber();
            }
        }

        [DebuggerHidden]
        private IEnumerator ShowAttack()
        {
            return new <ShowAttack>c__Iterator23 { <>f__this = this };
        }

        private void SortTemplatesByComboCount()
        {
            if (!AppMap.Instance.IsEditor)
            {
                foreach (KeyValuePair<uint, object> pair in BaseDataMgr.instance.GetComboHitTemplates())
                {
                    object obj2 = pair.Value;
                    this.comboHitT.Add((SysComboHitVo) obj2);
                }
                if (<>f__am$cache14 == null)
                {
                    <>f__am$cache14 = i => i.combo_count;
                }
                this.comboHitT = this.comboHitT.OrderBy<SysComboHitVo, int>(<>f__am$cache14).ToList<SysComboHitVo>();
            }
        }

        public void Start()
        {
            this.InitView();
            if (!this.isComboHitTSorted)
            {
                this.SortTemplatesByComboCount();
                this.isComboHitTSorted = true;
            }
        }

        public void StopAll()
        {
            this.maxAttackCounter = 0;
            this.StopCut();
            this.StopAttack();
        }

        private void StopAttack()
        {
            this.curAttackCounter = 0;
            this._attackNumberForCombo = 0;
            this.attackPause = false;
            Singleton<ComboHitView>.Instance.CloseView();
        }

        private void StopCut()
        {
            this.curCutCounter = 0;
            UnityEngine.Debug.Log("********************停止连斩统计");
        }

        protected void Update()
        {
            this.UpdateCutNumber();
            this.UpdateAttackNumber();
            this.UpdateEnergy();
        }

        private void UpdateAttackNumber()
        {
            if (!this.attackPause && (this.curAttackCounter > 0))
            {
                if ((Time.time - this.lastAttackTime) > this.attackTimeGap)
                {
                    this.StopAttack();
                }
                else if ((Time.time - this.lastRecordTime) > 0.1f)
                {
                    this.lastRecordTime = Time.time;
                    float num = this.attackTimeGap - (Time.time - this.lastAttackTime);
                    Singleton<ComboHitView>.Instance.ShowLeftTime(num / this.attackTimeGap);
                }
            }
        }

        private void UpdateCutNumber()
        {
            if ((this.curCutCounter > 0) && ((Time.time - this.lastCutTime) > this.cutTimeGap))
            {
                this.StopCut();
            }
        }

        private void UpdateEnergy()
        {
            this.timeEnergy = (this.elapsedSec - this._fixSecsAfterUseSuperSkill) * 0;
            this.totalEnergy = this.timeEnergy + this.attackEnergy;
            if (this.totalEnergy > this.maxEnergy)
            {
                this.totalEnergy = this.maxEnergy;
            }
            Singleton<ComboHitView>.Instance._energy = (int) this.totalEnergy;
            this.isSuperSkillAvailable = this.totalEnergy >= this.maxEnergy;
            Singleton<ComboHitView>.Instance._btnGroupSkillAvailable = this.isSuperSkillAvailable;
        }

        public void UseSuperSkill()
        {
            if (!this.isSuperSkillAvailable)
            {
                UnityEngine.Debug.LogWarning(string.Concat(new object[] { "还不能使用战队技能!: ", this.totalEnergy, " / ", this.maxEnergy, " 连斩计算:　", this._attackNumberForCombo }));
            }
            else
            {
                this.isSuperSkillAvailable = false;
                AppMap.Instance.me.Controller.SkillController.RequestUseSuperSkill(0x35a4ece9);
            }
        }

        public int AttackCounter
        {
            get
            {
                return this.maxAttackCounter;
            }
        }

        public int AttackEnergy
        {
            get
            {
                return this.attackEnergy;
            }
            set
            {
                this.attackEnergy = (value <= ((int) this.maxEnergy)) ? value : ((int) this.maxEnergy);
            }
        }

        private float attackTimeGap
        {
            get
            {
                if (this.curAttackCounter < 50)
                {
                    return 5f;
                }
                if (this.curAttackCounter < 100)
                {
                    return 4f;
                }
                if (this.curAttackCounter < 150)
                {
                    return 3f;
                }
                if (this.curAttackCounter < 200)
                {
                    return 2f;
                }
                return 1f;
            }
        }

        public int ElapsedSec
        {
            get
            {
                return this.elapsedSec;
            }
            set
            {
                this.elapsedSec = value;
            }
        }

        public int FixTime
        {
            get
            {
                return this._fixSecsAfterUseSuperSkill;
            }
            set
            {
                this._fixSecsAfterUseSuperSkill = value;
            }
        }

        [CompilerGenerated]
        private sealed class <ShowAttack>c__Iterator23 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal ComboHitMgr <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = 0;
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.<>f__this.curAttackCounter >= 1)
                        {
                            Singleton<ComboHitView>.Instance.ShowNum(this.<>f__this.curAttackCounter);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

