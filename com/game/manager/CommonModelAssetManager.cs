﻿namespace com.game.manager
{
    using System;
    using UnityEngine;

    public class CommonModelAssetManager
    {
        private GameObject _commonGameObject;
        private GameObject _footShadowGameObject;
        public static CommonModelAssetManager Instance = new CommonModelAssetManager();
        private const string Url = "Common/Model/ModelCommon.assetbundle";

        private CommonModelAssetManager()
        {
        }

        public GameObject GetFootShadowGameObject()
        {
            if (this._footShadowGameObject == null)
            {
                this._footShadowGameObject = this._commonGameObject.transform.FindChild("FootShadow").gameObject;
            }
            return this._footShadowGameObject;
        }

        public void Init()
        {
            AssetManager.Instance.LoadAsset<GameObject>("Common/Model/ModelCommon.assetbundle", new LoadAssetFinish<GameObject>(this.LoadCommonModelCallback), null, false, true);
        }

        private void LoadCommonModelCallback(GameObject commonGameObject)
        {
            this._commonGameObject = commonGameObject;
        }
    }
}

