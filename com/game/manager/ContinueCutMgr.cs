﻿namespace com.game.manager
{
    using com.game.module.core;
    using com.game.vo;
    using com.u3d.bases.debug;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ContinueCutMgr : MonoBehaviour
    {
        private bool attackPause;
        private float attackPauseTime;
        private int curAttackCounter;
        private int curCutCounter;
        private int cutTimeGap = 30;
        private float lastAttackTime;
        private float lastCutTime;
        private float lastRecordTime;
        private int maxAttackCounter;
        private const float SHOW_LEFT_INTERVAL = 0.1f;

        public void AddAttackNum(int attackNum = 1)
        {
            this.curAttackCounter += attackNum;
            if (this.maxAttackCounter < this.curAttackCounter)
            {
                this.maxAttackCounter = this.curAttackCounter;
            }
            this.lastAttackTime = Time.time;
            MeVo.instance.SeqAttack = (uint) (this.curAttackCounter / 100);
            base.StartCoroutine(this.ShowAttack());
        }

        public void AddCutNum(int cutNum = 1)
        {
            this.curCutCounter += cutNum;
            this.lastCutTime = Time.time;
            this.ShowCut();
        }

        public float GetCutAddition()
        {
            return (this.curAttackCounter * 0.1f);
        }

        public void PauseAttack()
        {
            if (Singleton<ContinueCutView>.Instance.gameObject.activeSelf)
            {
                this.attackPause = true;
                this.attackPauseTime = Time.time;
            }
        }

        public void ResumeAttack()
        {
            if (this.attackPause)
            {
                this.lastAttackTime += Time.time - this.attackPauseTime;
                this.attackPause = false;
                this.UpdateAttackNumber();
            }
        }

        [DebuggerHidden]
        private IEnumerator ShowAttack()
        {
            return new <ShowAttack>c__Iterator24 { <>f__this = this };
        }

        private void ShowCut()
        {
            if (this.curCutCounter >= 1)
            {
                com.u3d.bases.debug.Log.info(this, "********************万夫莫敌，连斩" + this.curCutCounter);
            }
        }

        public void StopAll()
        {
            this.maxAttackCounter = 0;
            this.StopCut();
            this.StopAttack();
        }

        private void StopAttack()
        {
            this.curAttackCounter = 0;
            this.attackPause = false;
            Singleton<ContinueCutView>.Instance.CloseView();
        }

        private void StopCut()
        {
            this.curCutCounter = 0;
            com.u3d.bases.debug.Log.info(this, "********************停止连斩统计");
        }

        protected void Update()
        {
            this.UpdateCutNumber();
            this.UpdateAttackNumber();
        }

        private void UpdateAttackNumber()
        {
            if (!this.attackPause && (this.curAttackCounter > 0))
            {
                if ((Time.time - this.lastAttackTime) > this.attackTimeGap)
                {
                    this.StopAttack();
                }
                else if ((Time.time - this.lastRecordTime) > 0.1f)
                {
                    this.lastRecordTime = Time.time;
                    float num = this.attackTimeGap - (Time.time - this.lastAttackTime);
                    Singleton<ContinueCutView>.Instance.ShowLeftTime(num / this.attackTimeGap);
                }
            }
        }

        private void UpdateCutNumber()
        {
            if ((this.curCutCounter > 0) && ((Time.time - this.lastCutTime) > this.cutTimeGap))
            {
                this.StopCut();
            }
        }

        public int AttackCounter
        {
            get
            {
                return this.maxAttackCounter;
            }
        }

        private float attackTimeGap
        {
            get
            {
                if (this.curAttackCounter < 50)
                {
                    return 5f;
                }
                if (this.curAttackCounter < 100)
                {
                    return 4f;
                }
                if (this.curAttackCounter < 150)
                {
                    return 3f;
                }
                if (this.curAttackCounter < 200)
                {
                    return 2f;
                }
                return 1f;
            }
        }

        [CompilerGenerated]
        private sealed class <ShowAttack>c__Iterator24 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal ContinueCutMgr <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = 0;
                        this.$PC = 1;
                        return true;

                    case 1:
                        if (this.<>f__this.curAttackCounter >= 1)
                        {
                            Singleton<ContinueCutView>.Instance.ShowNum(this.<>f__this.curAttackCounter);
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

