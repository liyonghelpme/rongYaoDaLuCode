﻿namespace com.game.manager
{
    using com.game.data;
    using System;

    public class DescriptManager
    {
        public static string GetText(uint id)
        {
            SysLanguageVo langVo = BaseDataMgr.instance.GetLangVo(id);
            return ((langVo != null) ? (langVo.text = langVo.text.Replace(@"\n", "\n")) : null);
        }
    }
}

