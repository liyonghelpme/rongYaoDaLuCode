﻿namespace com.game.manager
{
    using System;

    public enum DownLoadState
    {
        Init,
        Loading,
        Loaded,
        Stored,
        LoadFailure,
        StoreFailure,
        Cached
    }
}

