﻿namespace com.game.manager
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void LoadAssetFinish<T>(T obj) where T: Object;
}

