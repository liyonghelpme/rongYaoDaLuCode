﻿namespace com.game.manager
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void LoadAssetFinishByKeys<T>(T obj, string key) where T: Object;
}

