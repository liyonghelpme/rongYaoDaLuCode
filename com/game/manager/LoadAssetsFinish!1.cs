﻿namespace com.game.manager
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public delegate void LoadAssetsFinish<T>(List<T> objList) where T: Object;
}

