﻿namespace com.game.manager
{
    using System;
    using UnityEngine;

    public class ModelEffectManager
    {
        private const string ModelShaderName = "Mobile/UnlitCullOff (Supports Lightmap)";
        private static readonly Color White = new Color(0.5882353f, 0.5882353f, 0.5882353f, 1f);

        public static void ChangeAlpha(GameObject model, float alpha)
        {
            if (model != null)
            {
                foreach (SpriteRenderer renderer in model.GetComponentsInChildren<SpriteRenderer>(true))
                {
                    Color color = renderer.color;
                    color.a = alpha;
                    renderer.color = color;
                }
            }
        }

        public static void Init()
        {
        }

        public static void RemovePuleColor(GameObject model)
        {
        }

        public static void RemoveSpriteColorEffect(GameObject model)
        {
            if (model != null)
            {
                foreach (SpriteRenderer renderer in model.GetComponentsInChildren<SpriteRenderer>(true))
                {
                    renderer.color = White;
                }
                foreach (SkinnedMeshRenderer renderer2 in model.GetComponentsInChildren<SkinnedMeshRenderer>(true))
                {
                    foreach (Material material in renderer2.materials)
                    {
                        if (material.shader.name == "Mobile/UnlitCullOff (Supports Lightmap)")
                        {
                            material.color = White;
                        }
                    }
                }
            }
        }

        public static void ShowPuleColor(GameObject model)
        {
        }

        public static void ShowSpriteColorEffect(GameObject model, Color color)
        {
            if (model != null)
            {
                foreach (SpriteRenderer renderer in model.GetComponentsInChildren<SpriteRenderer>(true))
                {
                    renderer.color = color;
                }
                foreach (SkinnedMeshRenderer renderer2 in model.GetComponentsInChildren<SkinnedMeshRenderer>(true))
                {
                    foreach (Material material in renderer2.materials)
                    {
                        if (material.shader.name == "Mobile/UnlitCullOff (Supports Lightmap)")
                        {
                            material.color = color;
                        }
                    }
                }
            }
        }
    }
}

