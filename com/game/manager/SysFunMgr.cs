﻿namespace com.game.manager
{
    using com.game;
    using System;

    public class SysFunMgr
    {
        public static SysFunMgr instance = new SysFunMgr();

        public bool operable(string sysFunId)
        {
            uint mapId = AppMap.Instance.mapParserII.MapId;
            int type = BaseDataMgr.instance.GetMapVo(mapId).type;
            return this.operable(type, sysFunId);
        }

        public bool operable(int mapType, string sysFunId)
        {
            return true;
        }
    }
}

