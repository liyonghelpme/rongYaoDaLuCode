﻿namespace com.game.module.Achievement
{
    using com.game;
    using com.game.module.core;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class AchievementController : BaseControl<AchievementController>
    {
        private void AchievementItemListUpdate(INetData data)
        {
            AchieveReqMsg_42_1 g__ = new AchieveReqMsg_42_1();
            g__.read(data.GetMemoryStream());
            Singleton<AchievementMode>.Instance.UpdateAchieve(g__.achieveStructList, g__.achieveGot, g__.achievePoint);
        }

        private void GetAchieveUpdate(INetData data)
        {
            AchieveGetMsg_42_2 g__ = new AchieveGetMsg_42_2();
            g__.read(data.GetMemoryStream());
            Singleton<AchievementMode>.Instance.achieveInfo.UpdateHadGetOneAchieve(g__.achieveId, g__.achievePoint);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("10753", new NetMsgCallback(this.AchievementItemListUpdate));
            AppNet.main.addCMD("10754", new NetMsgCallback(this.GetAchieveUpdate));
            AppNet.main.addCMD("10755", new NetMsgCallback(this.PushAchieveUpdate));
        }

        private void PushAchieveUpdate(INetData data)
        {
            AchievePushMsg_42_3 g__ = new AchievePushMsg_42_3();
            g__.read(data.GetMemoryStream());
            Singleton<AchievementMode>.Instance.achieveInfo.UpdateOneAchieveInfo(g__.achieveStruct);
        }
    }
}

