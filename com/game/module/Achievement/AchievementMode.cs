﻿namespace com.game.module.Achievement
{
    using com.game;
    using com.game.module.Achievement.data;
    using com.game.module.core;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class AchievementMode : BaseMode<AchievementMode>
    {
        public AchievementInfo achieveInfo = new AchievementInfo();
        public GuideAchieve guideAchieve;

        public void ApplyAchieveInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_42.write_42_1(msdata);
            AppNet.gameNet.send(msdata, 0x2a, 1);
        }

        public void SendGetAchievement(ulong achievementId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_42.write_42_2(msdata, achievementId);
            AppNet.gameNet.send(msdata, 0x2a, 2);
        }

        public void UpdateAchieve(List<PAchieve> pAchieveList, List<ulong> hadGetIdList, uint achievePoint)
        {
            this.achieveInfo.UpdateItemInfoList(pAchieveList, hadGetIdList, achievePoint);
        }
    }
}

