﻿namespace com.game.module.Achievement.Components
{
    using com.game.data;
    using com.game.module.Achievement.data;
    using com.game.module.bag;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class AchieveItemView
    {
        private UISprite canGetBg;
        private UISprite cannotGetBg;
        private SysItemVoCell cell;
        private UILabel conditionAndAwardLabel;
        private Button getBtn;
        private GameObject go;
        private AchievementItemInfo info;
        private UILabel progressLabel;
        private SysAchievementVo template;
        private UILabel titleLabel;

        private void GetBtnClickHandler(GameObject go)
        {
            PAchieve achieve = new PAchieve {
                type = (uint) this.info.subType,
                data = this.info.currentProgress
            };
            Singleton<AchievementMode>.Instance.SendGetAchievement((ulong) this.info.AchievementTemplate.id);
        }

        public GameObject GetObject()
        {
            return this.go;
        }

        public void Init(GameObject gameObject)
        {
            this.go = gameObject;
            this.titleLabel = NGUITools.FindInChild<UILabel>(this.go, "titleLabel");
            this.conditionAndAwardLabel = NGUITools.FindInChild<UILabel>(this.go, "conditionAndAwardLabel");
            this.progressLabel = NGUITools.FindInChild<UILabel>(this.go, "progressLabel");
            this.getBtn = NGUITools.FindInChild<Button>(this.go, "getBtn");
            this.cell = NGUITools.FindChild(this.go, "cell").AddMissingComponent<SysItemVoCell>();
            this.canGetBg = NGUITools.FindInChild<UISprite>(this.go, "canGetBg");
            this.canGetBg.SetActive(false);
            this.cannotGetBg = NGUITools.FindInChild<UISprite>(this.go, "cannotGetBg");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this.getBtn.onClick = new UIWidgetContainer.VoidDelegate(this.GetBtnClickHandler);
        }

        public void SetInfo(AchievementItemInfo info)
        {
            this.info = info;
            this.template = info.AchievementTemplate;
            this.titleLabel.text = info.AchievementTemplate.name;
            this.conditionAndAwardLabel.text = info.AchievementTemplate.descript;
            this.progressLabel.text = info.currentProgress + "/" + info.AchievementTemplate.count.ToString();
            this.cell.sysVoId = info.AchievementTemplate.icon;
            switch (info.getState)
            {
                case AchievementItemState.CannotGet:
                    this.getBtn.SetActive(false);
                    this.canGetBg.SetActive(false);
                    this.cannotGetBg.SetActive(true);
                    break;

                case AchievementItemState.CanGet:
                    this.getBtn.SetActive(true);
                    this.canGetBg.SetActive(true);
                    this.cannotGetBg.SetActive(false);
                    break;

                case AchievementItemState.HadGet:
                    this.getBtn.SetActive(false);
                    this.canGetBg.SetActive(true);
                    this.cannotGetBg.SetActive(false);
                    break;
            }
        }

        public Button GetBtn
        {
            get
            {
                return this.getBtn;
            }
        }
    }
}

