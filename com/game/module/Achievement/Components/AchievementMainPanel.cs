﻿namespace com.game.module.Achievement.Components
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class AchievementMainPanel : BaseView<AchievementMainPanel>
    {
        private GameObject achievementGo;
        private UIGrid achievementItemGrid;
        private UILabel achivevementCountLab;
        private Button closeBtn;
        private int curPageIndex;
        private UIToggle curToggle;
        private Dictionary<int, int> indexInfo = new Dictionary<int, int>();
        private List<AchieveItemView> itemViewList = new List<AchieveItemView>();
        private UIScrollView scrollView;
        private GameObject tabBtnGo;
        private UIGrid tabBtnGrid;
        private List<UIToggle> tabBtnList = new List<UIToggle>();

        private void ClearItemView()
        {
            for (int i = 0; i < this.itemViewList.Count; i++)
            {
                this.itemViewList[i].GetObject().SetActive(false);
            }
        }

        private void CloneAchievementItem()
        {
            if (null == this.achievementGo)
            {
                this.achievementGo = base.FindChild("achievermentPanel/panel/grid/item");
            }
            GameObject gameObject = NGUITools.AddChild(this.achievementItemGrid.gameObject, this.achievementGo);
            AchieveItemView item = new AchieveItemView();
            item.Init(gameObject);
            this.itemViewList.Add(item);
        }

        private void CloneTabBtn(string tabBtnName)
        {
            if (null == this.tabBtnGo)
            {
                this.tabBtnGo = base.FindChild("achievermentPanel/tabBtns/grid/item");
            }
            GameObject parent = NGUITools.AddChild(this.tabBtnGrid.gameObject, this.tabBtnGo);
            UIToggle component = parent.GetComponent<UIToggle>();
            component.SetActive(true);
            UILabel componentInChildren = parent.GetComponentInChildren<UILabel>();
            NGUITools.FindInChild<UILabel>(parent, "Label").text = tabBtnName;
            this.tabBtnList.Add(component);
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            this.tabBtnList[0].value = true;
            this.SetIndex(0);
            this.achivevementCountLab.text = Singleton<AchievementMode>.Instance.achieveInfo.achievePoint.ToString();
        }

        protected override void Init()
        {
            this.closeBtn = base.FindInChild<Button>("achievermentPanel/closeBtn");
            this.tabBtnGrid = base.FindInChild<UIGrid>("achievermentPanel/tabBtns/grid");
            Dictionary<uint, object> achieveConfigureDic = BaseDataMgr.instance.GetAchieveConfigureDic();
            int key = 0;
            foreach (KeyValuePair<uint, object> pair in achieveConfigureDic)
            {
                SysAchieveConfigureVo vo = (SysAchieveConfigureVo) pair.Value;
                this.CloneTabBtn(vo.name);
                this.indexInfo.Add(key, vo.type);
                key++;
            }
            UIToggle.current = this.curToggle = this.tabBtnList[0];
            this.tabBtnList[0].value = true;
            this.scrollView = base.FindInChild<UIScrollView>("achievermentPanel/panel");
            this.achievementItemGrid = base.FindInChild<UIGrid>("achievermentPanel/panel/grid");
            this.achivevementCountLab = base.FindInChild<UILabel>("achievermentPanel/achivevementCount/achievementLab");
            this.InitEvet();
        }

        private void InitEvet()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            for (int i = 0; i < this.tabBtnList.Count; i++)
            {
                EventDelegate.Add(this.tabBtnList[i].onChange, new EventDelegate.Callback(this.TabBtnClickHandler));
            }
            Singleton<AchievementMode>.Instance.achieveInfo.Add(1, new NoticeListener(this.UpdateAllAchieveItemView));
            Singleton<AchievementMode>.Instance.achieveInfo.Add(2, new NoticeListener(this.UpdateAllAchieveItemView));
            Singleton<AchievementMode>.Instance.achieveInfo.Add(3, new NoticeListener(this.UpdateAllAchieveItemView));
        }

        private void SetIndex(int index)
        {
            this.scrollView.ResetPosition();
            this.ClearItemView();
            List<AchievementItemInfo> list = Singleton<AchievementMode>.Instance.achieveInfo.groupInfoDic[index];
            for (int i = 0; i < list.Count; i++)
            {
                if (i >= this.itemViewList.Count)
                {
                    this.CloneAchievementItem();
                }
                this.itemViewList[i].SetInfo(list[i]);
                this.itemViewList[i].GetObject().SetActive(true);
            }
            this.achivevementCountLab.text = Singleton<AchievementMode>.Instance.achieveInfo.achievePoint.ToString();
            this.achievementItemGrid.Reposition();
        }

        private void TabBtnClickHandler()
        {
            UIToggle current = UIToggle.current;
            if (current.value && (current != this.curToggle))
            {
                this.curToggle = current;
                this.curPageIndex = this.tabBtnList.IndexOf(current);
                this.SetIndex(this.curPageIndex);
            }
        }

        private void UpdateAllAchieveItemView(int type, int v1, int v2, object data)
        {
            this.SetIndex(this.curPageIndex);
        }

        private void UpdateView()
        {
        }

        public List<AchieveItemView> ItemViewList
        {
            get
            {
                return this.itemViewList;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Achievement/Achievement.assetbundle";
            }
        }
    }
}

