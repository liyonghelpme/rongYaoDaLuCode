﻿namespace com.game.module.Achievement.data
{
    using System;
    using System.Collections.Generic;

    public class AchieveSubType
    {
        public const int Area = 6;
        public const int normalCopy = 1;
        public static List<int> reversedCanGetArray = new List<int> { 6 };
        public static List<int> SpecialArray = new List<int> { 1, 4, 5 };
        public const int threeStarEliteCopy = 5;
        public const int threeStarNormalCopy = 4;
    }
}

