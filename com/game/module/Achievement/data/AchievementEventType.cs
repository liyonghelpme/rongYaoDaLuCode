﻿namespace com.game.module.Achievement.data
{
    using System;

    public class AchievementEventType
    {
        public const int UPDATE_ACHIEVEMENT = 1;
        public const int UPDATE_HAD_GET_ACHIEVEMENT = 3;
        public const int UPDATE_ONE_ACHIEVEMENT_INFO = 2;
    }
}

