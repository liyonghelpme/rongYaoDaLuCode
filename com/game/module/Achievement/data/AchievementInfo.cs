﻿namespace com.game.module.Achievement.data
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class AchievementInfo : Notifier
    {
        [CompilerGenerated]
        private static Func<AchievementItemInfo, bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<AchievementItemInfo, bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<AchievementItemInfo, bool> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<AchievementItemInfo, bool> <>f__am$cache8;
        public uint achievePoint;
        public List<PAchieve> currentAchieveProressList = new List<PAchieve>();
        public Dictionary<int, List<AchievementItemInfo>> groupInfoDic = new Dictionary<int, List<AchievementItemInfo>>();
        public List<ulong> hadGetIdList = new List<ulong>();
        public List<SysAchievementVo> hadGetTemplateList = new List<SysAchievementVo>();

        private int SortHadCetAchieveList(AchievementItemInfo a, AchievementItemInfo b)
        {
            if (a.currentProgress > b.currentProgress)
            {
                return -1;
            }
            if (a.currentProgress < b.currentProgress)
            {
                return 1;
            }
            return 0;
        }

        private int SortItemInfoListByProgressPercent(AchievementItemInfo a, AchievementItemInfo b)
        {
            float num = ((float) a.currentProgress) / ((float) a.AchievementTemplate.count);
            float num2 = ((float) b.currentProgress) / ((float) b.AchievementTemplate.count);
            if (num > num2)
            {
                return -1;
            }
            if (num < num2)
            {
                return 1;
            }
            return (a.subType - b.subType);
        }

        public void UpdateHadGetOneAchieve(ulong hadGetId, uint curAchievePoint)
        {
            SysAchievementVo achievement = BaseDataMgr.instance.GetAchievement((uint) hadGetId);
            if ((achievement.id == 0x2775) && (Singleton<AchievementMode>.Instance.guideAchieve != null))
            {
                Singleton<AchievementMode>.Instance.guideAchieve();
                Singleton<AchievementMode>.Instance.guideAchieve = null;
            }
            int index = 0;
            for (int i = 0; i < this.groupInfoDic[0].Count; i++)
            {
                if (this.groupInfoDic[0][i].AchievementTemplate.id == hadGetId)
                {
                    index = i;
                }
            }
            this.groupInfoDic[0].RemoveAt(index);
            this.hadGetIdList.Add(hadGetId);
            AchievementItemInfo item = new AchievementItemInfo {
                subType = achievement.subtype,
                currentProgress = (uint) achievement.count,
                level = achievement.level,
                getState = AchievementItemState.HadGet
            };
            this.groupInfoDic[0].Add(item);
            List<AchievementItemInfo> list = this.groupInfoDic[achievement.type];
            uint currentProgress = 0;
            int level = 1;
            int num5 = 0;
            for (int j = 0; j < list.Count; j++)
            {
                if (list[j].subType == achievement.subtype)
                {
                    currentProgress = list[j].currentProgress;
                    if (AchieveSubType.SpecialArray.IndexOf(list[j].subType) != -1)
                    {
                        if (list[j].realProress > (list[j].level + 1))
                        {
                            currentProgress = 1;
                        }
                        else
                        {
                            currentProgress = 0;
                        }
                    }
                    level = list[j].level;
                    num5 = j;
                    break;
                }
            }
            SysAchievementVo achieveTemplateBySunTypeAndLevel = AchievementTemplateList.GetAchieveTemplateBySunTypeAndLevel(achievement.subtype, achievement.level + 1);
            AchievementItemInfo info2 = new AchievementItemInfo {
                subType = achievement.subtype,
                currentProgress = currentProgress,
                level = level + 1
            };
            if (achieveTemplateBySunTypeAndLevel != null)
            {
                if (AchieveSubType.reversedCanGetArray.IndexOf(info2.subType) == -1)
                {
                    if (info2.currentProgress >= achieveTemplateBySunTypeAndLevel.count)
                    {
                        info2.getState = AchievementItemState.CanGet;
                    }
                    else
                    {
                        info2.getState = AchievementItemState.CannotGet;
                    }
                }
                else if ((info2.currentProgress > 0) && (info2.currentProgress <= achieveTemplateBySunTypeAndLevel.count))
                {
                    info2.getState = AchievementItemState.CanGet;
                }
                else
                {
                    info2.getState = AchievementItemState.CannotGet;
                }
                list[num5] = info2;
            }
            else
            {
                list.RemoveAt(num5);
            }
            this.achievePoint += curAchievePoint;
            list.Sort(new Comparison<AchievementItemInfo>(this.SortItemInfoListByProgressPercent));
            this.Notify(3, 0, 0, null);
        }

        public void UpdateItemInfoList(List<PAchieve> pAchieveList, List<ulong> hadGetIdList, uint achievePoint)
        {
            if (!this.groupInfoDic.ContainsKey(0) || (this.groupInfoDic[0].Count <= 0))
            {
                this.currentAchieveProressList = pAchieveList;
                this.hadGetIdList = hadGetIdList;
                this.achievePoint = achievePoint;
                List<AchievementItemInfo> list = new List<AchievementItemInfo>();
                for (int i = 0; i < hadGetIdList.Count; i++)
                {
                    SysAchievementVo achievement = BaseDataMgr.instance.GetAchievement((uint) hadGetIdList[i]);
                    this.hadGetTemplateList.Add(achievement);
                    AchievementItemInfo item = new AchievementItemInfo {
                        subType = achievement.subtype,
                        level = achievement.level,
                        currentProgress = (uint) achievement.count,
                        getState = AchievementItemState.HadGet
                    };
                    list.Add(item);
                }
                list.Sort(new Comparison<AchievementItemInfo>(this.SortHadCetAchieveList));
                this.groupInfoDic.Add(0, list);
                for (int j = 1; j <= 6; j++)
                {
                    List<int> typeToSubTypeList = AchievementTemplateList.GetTypeToSubTypeList(j);
                    List<AchievementItemInfo> list3 = new List<AchievementItemInfo>();
                    foreach (int num3 in typeToSubTypeList)
                    {
                        int level = 1;
                        uint data = 0;
                        uint num7 = 0;
                        for (int k = 0; k < this.currentAchieveProressList.Count; k++)
                        {
                            if (num3 != this.currentAchieveProressList[k].type)
                            {
                                continue;
                            }
                            data = this.currentAchieveProressList[k].data;
                            if (AchieveSubType.SpecialArray.IndexOf(num3) != -1)
                            {
                                num7 = this.currentAchieveProressList[k].data;
                            }
                            level = (int) this.currentAchieveProressList[k].level;
                            if (level == 0)
                            {
                                level = 1;
                            }
                            int num9 = 0;
                            SysAchievementVo achieveTemplateBySunTypeAndLevel = AchievementTemplateList.GetAchieveTemplateBySunTypeAndLevel(num3, level);
                            if (hadGetIdList.IndexOf((ulong) achieveTemplateBySunTypeAndLevel.id) > -1)
                            {
                                if (num3 < 100)
                                {
                                    level++;
                                }
                            }
                            else
                            {
                                foreach (AchievementItemInfo info2 in list)
                                {
                                    if (info2.subType == num3)
                                    {
                                        level = info2.level + 1;
                                        break;
                                    }
                                    num9++;
                                }
                                if (num9 == this.hadGetTemplateList.Count)
                                {
                                    level = 1;
                                }
                            }
                            if ((achieveTemplateBySunTypeAndLevel != null) && (achieveTemplateBySunTypeAndLevel.count == 1))
                            {
                                if (level > this.currentAchieveProressList[k].level)
                                {
                                    data = 0;
                                }
                                else
                                {
                                    data = 1;
                                }
                            }
                            break;
                        }
                        int num5 = num3;
                        AchievementItemInfo info3 = new AchievementItemInfo {
                            subType = num5,
                            level = level,
                            currentProgress = data,
                            realProress = num7
                        };
                        if (info3.AchievementTemplate != null)
                        {
                            if (AchieveSubType.reversedCanGetArray.IndexOf(info3.subType) == -1)
                            {
                                if (info3.currentProgress < info3.AchievementTemplate.count)
                                {
                                    info3.getState = AchievementItemState.CannotGet;
                                }
                                else
                                {
                                    info3.getState = AchievementItemState.CanGet;
                                    this.groupInfoDic[0].Add(info3);
                                }
                            }
                            else if ((info3.currentProgress > 0) && (info3.currentProgress < info3.AchievementTemplate.count))
                            {
                                info3.getState = AchievementItemState.CanGet;
                                this.groupInfoDic[0].Add(info3);
                            }
                            else
                            {
                                info3.getState = AchievementItemState.CannotGet;
                            }
                            if (hadGetIdList.IndexOf((ulong) info3.AchievementTemplate.id) == -1)
                            {
                                list3.Add(info3);
                            }
                        }
                    }
                    list3.Sort(new Comparison<AchievementItemInfo>(this.SortItemInfoListByProgressPercent));
                    this.groupInfoDic.Add(j, list3);
                }
                if (<>f__am$cache5 == null)
                {
                    <>f__am$cache5 = info => info.getState == AchievementItemState.CanGet;
                }
                List<AchievementItemInfo> list4 = this.groupInfoDic[0].Where<AchievementItemInfo>(<>f__am$cache5).ToList<AchievementItemInfo>();
                if (<>f__am$cache6 == null)
                {
                    <>f__am$cache6 = info => info.getState == AchievementItemState.HadGet;
                }
                List<AchievementItemInfo> collection = this.groupInfoDic[0].Where<AchievementItemInfo>(<>f__am$cache6).ToList<AchievementItemInfo>();
                list4.AddRange(collection);
                this.groupInfoDic.Remove(0);
                this.groupInfoDic.Add(0, list4);
            }
        }

        public void UpdateOneAchieveInfo(PAchieve achieveInfo)
        {
            <UpdateOneAchieveInfo>c__AnonStoreyD0 yd = new <UpdateOneAchieveInfo>c__AnonStoreyD0();
            int typeBySubType = AchievementTemplateList.GetTypeBySubType((int) achieveInfo.type);
            if (this.groupInfoDic.ContainsKey(typeBySubType))
            {
                yd.itemInfoList = this.groupInfoDic[typeBySubType];
                <UpdateOneAchieveInfo>c__AnonStoreyD1 yd2 = new <UpdateOneAchieveInfo>c__AnonStoreyD1 {
                    <>f__ref$208 = yd,
                    i = 0
                };
                while (yd2.i < yd.itemInfoList.Count)
                {
                    uint data = 0;
                    if (yd.itemInfoList[yd2.i].subType == achieveInfo.type)
                    {
                        bool flag = this.groupInfoDic[0].Where<AchievementItemInfo>(new Func<AchievementItemInfo, bool>(yd2.<>m__3D)).ToList<AchievementItemInfo>().Count <= 0;
                        SysAchievementVo achieveTemplateBySunTypeAndLevel = AchievementTemplateList.GetAchieveTemplateBySunTypeAndLevel((int) achieveInfo.type, (int) achieveInfo.level);
                        if (AchieveSubType.SpecialArray.IndexOf(yd.itemInfoList[yd2.i].subType) != -1)
                        {
                            data = achieveInfo.data;
                        }
                        if (((achieveTemplateBySunTypeAndLevel != null) && (achieveTemplateBySunTypeAndLevel.count == 1)) && ((AchieveSubType.SpecialArray.IndexOf(yd.itemInfoList[yd2.i].subType) != -1) || (achieveTemplateBySunTypeAndLevel.subtype >= 100)))
                        {
                            yd.itemInfoList[yd2.i].currentProgress = 1;
                        }
                        else
                        {
                            yd.itemInfoList[yd2.i].currentProgress = achieveInfo.data;
                        }
                        if (AchieveSubType.reversedCanGetArray.IndexOf(yd.itemInfoList[yd2.i].subType) == -1)
                        {
                            if (yd.itemInfoList[yd2.i].currentProgress < yd.itemInfoList[yd2.i].AchievementTemplate.count)
                            {
                                yd.itemInfoList[yd2.i].getState = AchievementItemState.CannotGet;
                            }
                            else
                            {
                                yd.itemInfoList[yd2.i].getState = AchievementItemState.CanGet;
                            }
                        }
                        else if ((yd.itemInfoList[yd2.i].currentProgress > 0) && (yd.itemInfoList[yd2.i].currentProgress < yd.itemInfoList[yd2.i].AchievementTemplate.count))
                        {
                            yd.itemInfoList[yd2.i].getState = AchievementItemState.CanGet;
                        }
                        else
                        {
                            yd.itemInfoList[yd2.i].getState = AchievementItemState.CannotGet;
                        }
                        if (flag)
                        {
                            this.groupInfoDic[0].Add(yd.itemInfoList[yd2.i]);
                        }
                        yd.itemInfoList[yd2.i].realProress = data;
                    }
                    yd2.i++;
                }
                if (<>f__am$cache7 == null)
                {
                    <>f__am$cache7 = info => info.getState == AchievementItemState.CanGet;
                }
                List<AchievementItemInfo> list2 = this.groupInfoDic[0].Where<AchievementItemInfo>(<>f__am$cache7).ToList<AchievementItemInfo>();
                if (<>f__am$cache8 == null)
                {
                    <>f__am$cache8 = info => info.getState == AchievementItemState.HadGet;
                }
                List<AchievementItemInfo> collection = this.groupInfoDic[0].Where<AchievementItemInfo>(<>f__am$cache8).ToList<AchievementItemInfo>();
                list2.AddRange(collection);
                this.groupInfoDic.Remove(0);
                this.groupInfoDic.Add(0, list2);
                yd.itemInfoList.Sort(new Comparison<AchievementItemInfo>(this.SortItemInfoListByProgressPercent));
                this.Notify(2, 0, 0, null);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateOneAchieveInfo>c__AnonStoreyD0
        {
            internal List<AchievementItemInfo> itemInfoList;
        }

        [CompilerGenerated]
        private sealed class <UpdateOneAchieveInfo>c__AnonStoreyD1
        {
            internal AchievementInfo.<UpdateOneAchieveInfo>c__AnonStoreyD0 <>f__ref$208;
            internal int i;

            internal bool <>m__3D(AchievementItemInfo info)
            {
                return (info.AchievementTemplate.id == this.<>f__ref$208.itemInfoList[this.i].AchievementTemplate.id);
            }
        }
    }
}

