﻿namespace com.game.module.Achievement.data
{
    using com.game.basic.events;
    using com.game.data;
    using System;

    public class AchievementItemInfo : Notifier
    {
        public uint currentProgress;
        public AchievementItemState getState;
        public int level;
        public uint realProress;
        public int subType;
        public SysAchievementVo template;

        public SysAchievementVo AchievementTemplate
        {
            get
            {
                this.template = AchievementTemplateList.GetAchieveTemplateBySunTypeAndLevel(this.subType, this.level);
                return this.template;
            }
        }
    }
}

