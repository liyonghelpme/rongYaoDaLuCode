﻿namespace com.game.module.Achievement.data
{
    using System;

    public enum AchievementItemState
    {
        CannotGet,
        CanGet,
        HadGet
    }
}

