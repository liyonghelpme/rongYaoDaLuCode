﻿namespace com.game.module.Achievement.data
{
    using com.game.data;
    using com.game.manager;
    using System;
    using System.Collections.Generic;

    public class AchievementTemplateList
    {
        public static List<SysAchievementVo> GetAchievementTemplateListByType(int type)
        {
            List<SysAchievementVo> list = new List<SysAchievementVo>();
            return BaseDataMgr.instance.GetAchievementTemplateListByType(type);
        }

        public static SysAchievementVo GetAchieveTemplateBySunTypeAndLevel(int subType, int level)
        {
            foreach (KeyValuePair<uint, object> pair in BaseDataMgr.instance.GetAchievementTemplateList())
            {
                SysAchievementVo vo = pair.Value as SysAchievementVo;
                if ((vo.subtype == subType) && (vo.level == level))
                {
                    return vo;
                }
            }
            return null;
        }

        public static int GetTypeBySubType(int subType)
        {
            return GetAchieveTemplateBySunTypeAndLevel(subType, 1).type;
        }

        public static List<int> GetTypeToSubTypeList(int type)
        {
            List<int> list = new List<int>();
            List<SysAchievementVo> achievementTemplateListByType = GetAchievementTemplateListByType(type);
            for (int i = 0; i < achievementTemplateListByType.Count; i++)
            {
                int subtype = achievementTemplateListByType[i].subtype;
                if (list.IndexOf(subtype) == -1)
                {
                    list.Add(subtype);
                }
            }
            return list;
        }
    }
}

