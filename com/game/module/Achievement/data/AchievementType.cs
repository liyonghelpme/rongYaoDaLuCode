﻿namespace com.game.module.Achievement.data
{
    using System;

    public class AchievementType
    {
        public const int COPY = 0x2713;
        public const int DAILY = 0x2715;
        public const int HERO = 0x2714;
        public const int PVP = 0x2712;
        public const int TASK = 0x2711;
    }
}

