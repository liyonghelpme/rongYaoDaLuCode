﻿namespace com.game.module.Activity
{
    using com.game;
    using com.game.module.core;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class ActivityControl : BaseControl<ActivityControl>
    {
        private void InitActivityInfos(INetData data)
        {
            ActivityInfoMsg_9_3 g__ = new ActivityInfoMsg_9_3();
            g__.read(data.GetMemoryStream());
            Singleton<ActivityMode>.Instance.InitActivityInfos(g__.activityInfo);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("2305", new NetMsgCallback(this.UpdateActivityState));
            AppNet.main.addCMD("2306", new NetMsgCallback(this.UpdateActivityCount));
            AppNet.main.addCMD("2307", new NetMsgCallback(this.InitActivityInfos));
        }

        private void UpdateActivityCount(INetData data)
        {
            ActivityCountMsg_9_2 g__ = new ActivityCountMsg_9_2();
            g__.read(data.GetMemoryStream());
            Singleton<ActivityMode>.Instance.UpdateActivityCount(g__.activityCount);
        }

        private void UpdateActivityState(INetData data)
        {
            ActivityStateMsg_9_1 g__ = new ActivityStateMsg_9_1();
            g__.read(data.GetMemoryStream());
            Singleton<ActivityMode>.Instance.UpdateActivityState(g__.activityState);
        }
    }
}

