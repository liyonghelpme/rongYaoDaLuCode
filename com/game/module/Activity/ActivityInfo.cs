﻿namespace com.game.module.Activity
{
    using System;

    public class ActivityInfo
    {
        public uint activityId;
        public uint count;
        public uint duration;
        public byte isOpen;
        public uint level;
        public uint link;
        public string name;
        public string pictrue_id;
        public uint startTime;
        public int state = -1;

        public ActivityState CurrentState
        {
            get
            {
                return (ActivityState) this.state;
            }
        }

        public bool IsOpen
        {
            get
            {
                return (this.isOpen == 1);
            }
        }

        public enum ActivityState
        {
            InOpen = 1,
            None = -1,
            NotOpen = 0
        }
    }
}

