﻿namespace com.game.module.Activity
{
    using com.game;
    using com.game.manager;
    using com.game.utils;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ActivityItemView : UIWidgetContainer
    {
        public ActivityInfo activityInfo;
        private const uint allDayCount = 0x15180;
        private UITexture Background;
        private UILabel infoIILabel;
        private GameObject infoIIObject;
        private UILabel infoLabel;
        private GameObject infoObject;
        private UISprite selectSprite;
        private UILabel stateLabel;
        private GameObject stateObject;
        private vp_Timer.Handle timeHandle = new vp_Timer.Handle();
        private UILabel titleLabel;

        public void ClearTimeHandle()
        {
            vp_Timer.CancelTimerByHandle(this.timeHandle);
        }

        public void Init(ActivityInfo activityInfo)
        {
            this.activityInfo = activityInfo;
            this.Background = base.FindInChild<UITexture>("background");
            this.selectSprite = base.FindInChild<UISprite>("select");
            this.titleLabel = base.FindInChild<UILabel>("title/label");
            this.stateLabel = base.FindInChild<UILabel>("state/label");
            this.stateObject = base.FindChild("state");
            this.infoLabel = base.FindInChild<UILabel>("infos/itemInfo/Label");
            this.infoIILabel = base.FindInChild<UILabel>("infos/itemInfoII/Label");
            this.infoObject = base.FindChild("infos/itemInfo");
            this.infoIIObject = base.FindChild("infos/itemInfoII");
            base.onPress = new UIWidgetContainer.BoolDelegate(this.OnPressHandler);
            this.UpdateState();
        }

        private void OnPressHandler(GameObject go, bool state)
        {
            if (this.activityInfo.IsOpen)
            {
                this.selectSprite.SetActive(state);
            }
        }

        public void UpdateActivityInfo(ActivityInfo activityInfo)
        {
            this.activityInfo = activityInfo;
            this.UpdateState();
        }

        private void UpdateImgState(bool isGray = false)
        {
            foreach (UISprite sprite in base.GetComponentsInChildren<UISprite>(true))
            {
                if (isGray)
                {
                    sprite.ShowAsGray();
                }
                else
                {
                    sprite.ShowAsMyself();
                }
            }
            this.Background.shader = !isGray ? Shader.Find("Unlit/Transparent Colored") : Shader.Find("Ulint/Transparent Colored (Gray)");
            this.Background.color = !isGray ? Color.white : Color.black;
        }

        public void UpdateState()
        {
            vp_Timer.CancelTimerByHandle(this.timeHandle);
            this.selectSprite.SetActive(false);
            this.stateObject.SetActive(false);
            this.infoIIObject.SetActive(false);
            this.infoObject.SetActive(false);
            string str = !this.activityInfo.IsOpen ? "a94fe2" : "a5b4b7";
            string[] textArray1 = new string[] { "[", str, "]", this.activityInfo.name, "[-]" };
            this.titleLabel.text = string.Concat(textArray1);
            if (!this.activityInfo.IsOpen)
            {
                this.UpdateImgState(true);
            }
            else if (this.activityInfo.level <= AppMap.Instance.me.GetVo().Level)
            {
                this.stateObject.SetActive(this.activityInfo.CurrentState == ActivityInfo.ActivityState.InOpen);
                this.infoIIObject.SetActive(true);
                this.infoObject.SetActive(true);
                string param = (this.activityInfo.count >= 0x1869f) ? LanguageManager.GetWord("Activity.MaxTimer") : this.activityInfo.count.ToString();
                this.infoLabel.text = LanguageManager.GetStringToWord("[a5b4b7]{1}[-]", LanguageManager.GetWord("Activity.LastCount")) + LanguageManager.GetStringToWord("[c9ad3e]{1}[-]", param);
                if (this.activityInfo.CurrentState == ActivityInfo.ActivityState.InOpen)
                {
                    vp_Timer.CancelTimerByHandle(this.timeHandle);
                    this.stateLabel.text = LanguageManager.GetStringToWord("[dfdfc5]{1}[-]", LanguageManager.GetWord("Activity.State"));
                    if (this.activityInfo.duration >= 0x15180)
                    {
                        this.infoIILabel.text = LanguageManager.GetStringToWord("[a5b4b7]{1}[-]", LanguageManager.GetWord("Activity.Timer")) + LanguageManager.GetStringToWord("[c9ad3e]{1}[-]", LanguageManager.GetWord("Activity.Allday"));
                    }
                    else
                    {
                        vp_Timer.In(1f, delegate {
                            int num = (int) (this.activityInfo.startTime + this.activityInfo.duration);
                            TimeSpan timeOfToday = TimeUtil.GetTimeOfToday();
                            int leftTime = num - ((int) timeOfToday.TotalSeconds);
                            if (leftTime <= 0)
                            {
                                vp_Timer.CancelTimerByHandle(this.timeHandle);
                            }
                            else
                            {
                                this.infoIILabel.text = LanguageManager.GetStringToWord("[a5b4b7]{1}[-]", LanguageManager.GetWord("Activity.LastTimer")) + LanguageManager.GetStringToWord("[c9ad3e]{1}[-]", TimeUtil.GetTimeHhmmss(leftTime));
                            }
                        }, 0, 1f, this.timeHandle);
                    }
                }
                else if (this.activityInfo.duration >= 0x15180)
                {
                    this.infoIILabel.text = LanguageManager.GetStringToWord("[a5b4b7]{1}[-]", LanguageManager.GetWord("Activity.Timer")) + LanguageManager.GetStringToWord("[c9ad3e]{1}[-]", LanguageManager.GetWord("Activity.Allday"));
                }
                else
                {
                    this.infoIILabel.text = LanguageManager.GetStringToWord("[a5b4b7]{1}[-]", LanguageManager.GetWord("Activity.Timer")) + LanguageManager.GetStringToWord("[c9ad3e]{1}[-]", TimeUtil.GetTimeHm(this.activityInfo.startTime) + "-" + TimeUtil.GetTimeHm(this.activityInfo.startTime + this.activityInfo.duration));
                }
            }
            else
            {
                this.UpdateImgState(true);
                this.infoObject.SetActive(true);
                this.infoLabel.text = LanguageManager.GetStringToWord("[a5b4b7]{1}[-]", LanguageManager.GetWord("Activity.OpenLevel")) + LanguageManager.GetStringToWord("[cd3e4b]{1}[-]", this.activityInfo.level + LanguageManager.GetWord("Activity.Level"));
            }
        }
    }
}

