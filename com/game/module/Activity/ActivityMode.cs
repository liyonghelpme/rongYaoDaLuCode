﻿namespace com.game.module.Activity
{
    using com.game;
    using com.game.module.core;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ActivityMode : BaseMode<ActivityMode>
    {
        [CompilerGenerated]
        private static Func<PActivityInfo, ActivityInfo> <>f__am$cache2;
        public readonly Dictionary<uint, ActivityInfo> ActivityInfos = new Dictionary<uint, ActivityInfo>();
        private bool IsInit;
        public const int UPDATE_ACTIVITY_COUNT = 2;
        public const int UPDATE_ACTIVITY_INFO = 3;
        public const int UPDATE_ACTIVITY_STATE = 1;

        public void GetActivityCount()
        {
            MemoryStream msdata = new MemoryStream();
            Module_9.write_9_2(msdata);
            AppNet.gameNet.send(msdata, 9, 2);
        }

        private ActivityInfo getActivityInfo(uint activityId)
        {
            return (!this.ActivityInfos.ContainsKey(activityId) ? null : this.ActivityInfos[activityId]);
        }

        public void GetActivityInfos()
        {
            MemoryStream msdata = new MemoryStream();
            Module_9.write_9_3(msdata);
            AppNet.gameNet.send(msdata, 9, 3);
        }

        public void GetActivityState()
        {
            MemoryStream msdata = new MemoryStream();
            Module_9.write_9_1(msdata);
            AppNet.gameNet.send(msdata, 9, 1);
        }

        public void InitActivityInfos(List<PActivityInfo> activityInfos)
        {
            this.ActivityInfos.Clear();
            if (<>f__am$cache2 == null)
            {
                <>f__am$cache2 = info => new ActivityInfo { activityId = info.activityId, name = info.name, level = info.level, count = info.count, pictrue_id = info.pictureId, startTime = info.startTime, duration = info.duration, isOpen = info.isOpen, link = info.link };
            }
            IEnumerator<ActivityInfo> enumerator = activityInfos.Select<PActivityInfo, ActivityInfo>(<>f__am$cache2).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ActivityInfo current = enumerator.Current;
                    Debug.Log(string.Concat(new object[] { "startTime = ", current.startTime, "  duration = ", current.duration, "  isOpen = ", current.isOpen }));
                    this.ActivityInfos.Add(current.activityId, current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.IsInit = true;
            this.GetActivityState();
        }

        public void UpdateActivityCount(List<PActivityCount> activityCounts)
        {
            foreach (PActivityCount count in activityCounts)
            {
                ActivityInfo info = this.getActivityInfo(count.activityId);
                if (info != null)
                {
                    info.count = count.count;
                }
            }
            if (this.IsInit)
            {
                this.IsInit = false;
                Singleton<ActivityPanel>.Instance.OpenView();
            }
            else
            {
                base.DataUpdate(2);
            }
        }

        public void UpdateActivityState(List<PActivityState> activityStates)
        {
            foreach (PActivityState state in activityStates)
            {
                ActivityInfo info = this.getActivityInfo(state.activityId);
                if (info != null)
                {
                    info.state = state.state;
                }
            }
            if (this.IsInit)
            {
                this.GetActivityCount();
            }
            else
            {
                base.DataUpdate(1);
            }
        }
    }
}

