﻿namespace com.game.module.Activity
{
    using com.game.module.core;
    using com.game.module.WiFiPvP;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ActivityPanel : BaseView<ActivityPanel>
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map8;
        private List<ActivityItemView> activityItemViews = new List<ActivityItemView>();
        private Button Close_btn;
        private GameObject gridGameObject;
        private GameObject ItemView;
        private UIGrid uiGrid;

        public override void CancelUpdateHandler()
        {
            Singleton<ActivityMode>.Instance.dataUpdated -= new DataUpateHandler(this.DataUpdated);
        }

        private void ClearItemHandler()
        {
            foreach (ActivityItemView view in this.activityItemViews)
            {
                view.ClearTimeHandle();
            }
        }

        private void ClearItems()
        {
            this.ClearItemHandler();
            for (int i = this.gridGameObject.transform.childCount - 1; i > 0; i--)
            {
                UnityEngine.Object.Destroy(this.gridGameObject.transform.GetChild(i).gameObject);
            }
            this.activityItemViews.Clear();
        }

        private void CreateItems()
        {
            this.ClearItems();
            List<ActivityInfo> list = Singleton<ActivityMode>.Instance.ActivityInfos.Values.ToList<ActivityInfo>();
            int maxPerLine = this.uiGrid.maxPerLine;
            float cellWidth = this.uiGrid.cellWidth;
            float cellHeight = this.uiGrid.cellHeight;
            int num4 = 0;
            int num5 = 0;
            foreach (ActivityInfo info in list)
            {
                GameObject obj2 = NGUITools.AddChild(this.uiGrid.gameObject, this.ItemView);
                obj2.SetActive(true);
                ActivityItemView item = obj2.AddComponent<ActivityItemView>();
                item.onClick = new UIWidgetContainer.VoidDelegate(this.OnItemClickHandler);
                item.Init(info);
                this.activityItemViews.Add(item);
                Vector3 vector = new Vector3(cellWidth * num4, -cellHeight * num5, 0f);
                item.transform.localPosition = vector;
                if ((++num4 >= maxPerLine) && (maxPerLine > 0))
                {
                    num4 = 0;
                    num5++;
                }
            }
        }

        public override void DataUpdated(object sender, int code)
        {
            if ((code == 1) || (code == 2))
            {
                this.UpdateInfos();
            }
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdateInfos();
        }

        protected override void HandleBeforeCloseView()
        {
            this.ClearItems();
        }

        protected override void Init()
        {
            this.Close_btn = base.FindInChild<Button>("close");
            this.uiGrid = base.FindInChild<UIGrid>("List/Grid");
            this.ItemView = base.FindChild("List/Grid/Item");
            this.gridGameObject = base.FindChild("List/Grid");
            this.ItemView.SetActive(false);
            this.Close_btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
        }

        private void OnClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map8 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(1);
                    dictionary.Add("close", 0);
                    <>f__switch$map8 = dictionary;
                }
                if (<>f__switch$map8.TryGetValue(name, out num) && (num == 0))
                {
                    this.CloseView();
                }
            }
        }

        private void OnItemClickHandler(GameObject go)
        {
            ActivityItemView component = go.GetComponent<ActivityItemView>();
            if (component.activityInfo.IsOpen)
            {
                this.CloseView();
                switch (component.activityInfo.link)
                {
                    case 1:
                        Singleton<ArenaView>.Instance.OpenView();
                        break;

                    case 2:
                        Singleton<WifiPvpMode>.Instance.currentMode = WifiPvpMode.ViewMode.selecet;
                        Singleton<WifiPvpView>.Instance.OpenView();
                        break;
                }
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<ActivityMode>.Instance.dataUpdated += new DataUpateHandler(this.DataUpdated);
        }

        private void UpdateInfos()
        {
            this.CreateItems();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Activity/ActivityPanel.assetbundle";
            }
        }
    }
}

