﻿namespace com.game.module.Arena
{
    using System;

    public class ArenaAward
    {
        public uint number;
        public uint template_id;

        public ArenaAward(uint tid, uint count)
        {
            this.template_id = tid;
            this.number = count;
        }
    }
}

