﻿namespace com.game.module.Arena
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class ArenaAwardItemView
    {
        private GameObject _gameObject;
        private UISprite _icon;
        private UILabel _num;

        public ArenaAwardItemView(GameObject go)
        {
            this.gameObject = go;
        }

        private void Init()
        {
            this._icon = NGUITools.FindInChild<UISprite>(this._gameObject, "icon");
            this._num = NGUITools.FindInChild<UILabel>(this._gameObject, "awardNum");
        }

        public void Setup(string iconName, string count, bool isNormalItem)
        {
            if (isNormalItem)
            {
                this._icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ItemIconAtlas");
                this._icon.spriteName = iconName;
            }
            else
            {
                this._icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Common");
                string str = string.Empty;
                switch (int.Parse(iconName))
                {
                    case 1:
                    case 2:
                        str = "zuanshi-xiao";
                        break;

                    case 3:
                    case 4:
                        str = "jinbi-xiao";
                        break;
                }
                this._icon.spriteName = str;
            }
            this._icon.gameObject.SetActive(true);
            this._num.text = count;
        }

        public void SetupAnEmptyTip()
        {
            this._num.text = LanguageManager.GetWord("Arena.NoneDailyAward");
            this._icon.gameObject.SetActive(false);
        }

        public GameObject gameObject
        {
            get
            {
                return this._gameObject;
            }
            set
            {
                this._gameObject = value;
                this.Init();
            }
        }
    }
}

