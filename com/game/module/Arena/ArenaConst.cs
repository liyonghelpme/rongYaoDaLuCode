﻿namespace com.game.module.Arena
{
    using System;

    public static class ArenaConst
    {
        public const int ARENA_DIAM_CLEAR_CD = 20;
        public const int ARENA_POS_UNAVAILABLE = 0x1869f;
        public const int BACK_1 = 3;
        public const int BACK_2 = 4;
        public const int BACK_3 = 5;
        public const int BATTLE_MODE_ALL_AI = 0;
        public const int BATTLE_MODE_OPERATABLE = 1;
        public const int BUY_CHANCE_PRICE_ID = 0x44c;
        public const int BUY_COUNT_ONLY = 5;
        public const int CD_TIME_ONLY = 4;
        public const uint DEFAULT_ARENA = 1;
        public const int FREE_CHALLENGE_TIMES = 5;
        public const int FRONT_1 = 0;
        public const int FRONT_2 = 1;
        public const int FRONT_3 = 2;
        public const int LOG = 3;
        public const int MY_INFO = 2;
        public const int RANKLIST = 1;
        public const int REFRESH_CLICK_CD = 5;
        public const int RESULT_LOST = 0;
        public const int RESULT_REVENGE_LOST = 3;
        public const int RESULT_REVENGE_WIN = 2;
        public const int RESULT_WIN = 1;

        public enum ArenaStatus
        {
            ready,
            started,
            win,
            failed,
            confirming_win,
            confirming_failed,
            ending,
            ended
        }
    }
}

