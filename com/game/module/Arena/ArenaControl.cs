﻿namespace com.game.module.Arena
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Confirm;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class ArenaControl : BaseControl<ArenaControl>
    {
        private MemoryStream _lastReqEndMsg;
        [CompilerGenerated]
        private static ClickCallback <>f__am$cache2;
        [CompilerGenerated]
        private static vp_Timer.Callback <>f__am$cache3;
        [CompilerGenerated]
        private static vp_Timer.Callback <>f__am$cache4;
        [CompilerGenerated]
        private static Func<PArenaFightInfo, uint> <>f__am$cache5;
        [CompilerGenerated]
        private static vp_Timer.Callback <>f__am$cache6;

        public void ArenaEnd()
        {
            int num = (ArenaManager.Instance.Status != ArenaConst.ArenaStatus.confirming_win) ? 0 : 1;
            uint actualTime = (uint) ArenaManager.Instance.ActualTime;
            string key = (string.Empty + GlobalData.sign) + actualTime + num;
            string checkKey = Singleton<DungeonMode>.Instance.GetMD5(key, true);
            this.SendArenaResult(checkKey, actualTime, (byte) num);
        }

        private void ArenaResult(INetData data)
        {
            ArenaResultMsg_43_4 g__ = new ArenaResultMsg_43_4();
            g__.read(data.GetMemoryStream());
            switch (g__.code)
            {
                case 0:
                    this._lastReqEndMsg = null;
                    this.RequestMyInfo();
                    this.RequestRefreshFightList();
                    this.RequestLog();
                    if (<>f__am$cache3 == null)
                    {
                        <>f__am$cache3 = delegate {
                            Debug.Log("服务端校验成功");
                            ArenaManager.Instance.ConfirmEndStatus();
                            Singleton<ArenaEndView>.Instance.OpenView();
                        };
                    }
                    vp_Timer.In(0.5f, <>f__am$cache3, null);
                    break;

                case 0x27:
                    if (this._lastReqEndMsg != null)
                    {
                        vp_Timer.In(1f, delegate {
                            AppNet.gameNet.send(this._lastReqEndMsg, 0x2b, 4);
                            this._lastReqEndMsg = null;
                        }, null);
                    }
                    else
                    {
                        ArenaManager.Instance.DenyEndStatus();
                        Singleton<ArenaEndView>.Instance.OpenView();
                    }
                    break;

                default:
                    ErrorCodeManager.ShowError(g__.code);
                    this._lastReqEndMsg = null;
                    this.RequestMyInfo();
                    if (<>f__am$cache4 == null)
                    {
                        <>f__am$cache4 = delegate {
                            Debug.Log("服务端校验失败, 即为失败");
                            ArenaManager.Instance.DenyEndStatus();
                            Singleton<ArenaEndView>.Instance.OpenView();
                        };
                    }
                    vp_Timer.In(0.5f, <>f__am$cache4, null);
                    break;
            }
        }

        private void BuyAnotherChance(INetData receiveData)
        {
            ArenaBuyTimeMsg_43_5 g__ = new ArenaBuyTimeMsg_43_5();
            g__.read(receiveData.GetMemoryStream());
            if (g__.code == 0)
            {
                ArenaMode instance = Singleton<ArenaMode>.Instance;
                instance.TotalChallengeCount++;
                Singleton<ArenaMode>.Instance.BuyChanceCount = g__.buyTimes;
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        public void BuyChance()
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_5(msdata);
            AppNet.gameNet.send(msdata, 0x2b, 5);
        }

        private void Challenge(INetData data)
        {
            ArenaChallengeMsg_43_3 g__ = new ArenaChallengeMsg_43_3();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                ArenaManager.Instance.Enter(g__.myList, g__.anemyList);
            }
            DoChallenge(g__.code);
        }

        public void ClearCD()
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_6(msdata);
            AppNet.gameNet.send(msdata, 0x2b, 6);
        }

        private void ClearCD(INetData receiveData)
        {
            ArenaClearCdMsg_43_6 g__ = new ArenaClearCdMsg_43_6();
            g__.read(receiveData.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<ArenaMode>.Instance.ChallengeCD = 0f;
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private static void DoChallenge(ushort msgCode)
        {
            if (msgCode == 0)
            {
                if (<>f__am$cache6 == null)
                {
                    <>f__am$cache6 = delegate {
                        Singleton<ArenaView>.Instance.CloseView();
                        Singleton<ArenaRevengeConfirmView>.Instance.CloseView();
                        Singleton<ArenaReadyMaskView>.Instance.CloseView();
                        Singleton<MapMode>.Instance.changeScene_4_2();
                    };
                }
                vp_Timer.In(0.5f, <>f__am$cache6, null);
            }
            else
            {
                ErrorCodeManager.ShowError(msgCode);
            }
        }

        public static void DoRequestAddChance()
        {
            uint buyChanceCount = Singleton<ArenaMode>.Instance.BuyChanceCount;
            SysPriceVo sysPriceVo = BaseDataMgr.instance.GetSysPriceVo(0x44c, buyChanceCount + 1);
            if (sysPriceVo == null)
            {
                MessageManager.Show(LanguageManager.GetWord("Arena.ExceededMaxBuyTime"));
            }
            else
            {
                int diam = sysPriceVo.diam;
                string[] param = new string[] { diam.ToString() };
                string word = LanguageManager.GetWord("Arena.BuyChance", param);
                if (<>f__am$cache2 == null)
                {
                    <>f__am$cache2 = () => Singleton<ArenaControl>.Instance.BuyChance();
                }
                ConfirmMgr.Instance.ShowOkCancelAlert(word, "OK_CANCEL", <>f__am$cache2);
            }
        }

        private void FightLog(INetData receiveData)
        {
            ArenaFightLogMsg_43_7 g__ = new ArenaFightLogMsg_43_7();
            g__.read(receiveData.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<ArenaMode>.Instance.UpdateLogList(g__.logList);
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("11009", new NetMsgCallback(this.UpdateRoleArenaInfo));
            AppNet.main.addCMD("11010", new NetMsgCallback(this.UpdateCanFightList));
            AppNet.main.addCMD("11011", new NetMsgCallback(this.Challenge));
            AppNet.main.addCMD("11012", new NetMsgCallback(this.ArenaResult));
            AppNet.main.addCMD("11013", new NetMsgCallback(this.BuyAnotherChance));
            AppNet.main.addCMD("11014", new NetMsgCallback(this.ClearCD));
            AppNet.main.addCMD("11015", new NetMsgCallback(this.FightLog));
            AppNet.main.addCMD("11016", new NetMsgCallback(this.Revenge));
            AppNet.main.addCMD("11017", new NetMsgCallback(this.RefreshFighersInRankList));
            AppNet.main.addCMD("11018", new NetMsgCallback(this.ReplyRevengeTargetInfo));
        }

        private void RefreshFighersInRankList(INetData receiveData)
        {
            ArenaRefreshFightListMsg_43_9 g__ = new ArenaRefreshFightListMsg_43_9();
            g__.read(receiveData.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void ReplyRevengeTargetInfo(INetData receiveData)
        {
            ArenaRevengeUserInfoMsg_43_10 g__ = new ArenaRevengeUserInfoMsg_43_10();
            g__.read(receiveData.GetMemoryStream());
            this.RevengeTarget = g__.info;
            Singleton<ArenaView>.Instance.OpenArenaRevengeConfirmView();
        }

        public void RequestChallenge(ulong roleId, uint pos)
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_3(msdata, roleId, pos);
            AppNet.gameNet.send(msdata, 0x2b, 3);
        }

        public void RequestFullRankList()
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_2(msdata);
            AppNet.gameNet.send(msdata, 0x2b, 2);
        }

        public void RequestLog()
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_7(msdata);
            AppNet.gameNet.send(msdata, 0x2b, 7);
        }

        public void RequestMyInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_1(msdata);
            AppNet.gameNet.send(msdata, 0x2b, 1);
        }

        public void RequestRefreshFightList()
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_9(msdata);
            AppNet.gameNet.send(msdata, 0x2b, 9);
        }

        public void RequestRevenge(ulong targetRoleId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_8(msdata, targetRoleId);
            AppNet.gameNet.send(msdata, 0x2b, 8);
        }

        public void RequestRevengeTargetInfo(ulong id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_10(msdata, id);
            AppNet.gameNet.send(msdata, 0x2b, 10);
        }

        private void Revenge(INetData receiveData)
        {
            ArenaRevengeMsg_43_8 g__ = new ArenaRevengeMsg_43_8();
            g__.read(receiveData.GetMemoryStream());
            DoChallenge(g__.code);
        }

        public void SendArenaResult(string checkKey, uint timestamp, byte resultByte)
        {
            MemoryStream msdata = new MemoryStream();
            Module_43.write_43_4(msdata, checkKey, timestamp, resultByte);
            this._lastReqEndMsg = msdata;
            AppNet.gameNet.send(msdata, 0x2b, 4);
        }

        public void SwitchToMainCity()
        {
            Singleton<MapMode>.Instance.changeScene_4_2();
        }

        private void UpdateCanFightList(INetData data)
        {
            ArenaFightListMsg_43_2 g__ = new ArenaFightListMsg_43_2();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                if (<>f__am$cache5 == null)
                {
                    <>f__am$cache5 = x => x.pos;
                }
                List<PArenaFightInfo> pList = g__.list.OrderBy<PArenaFightInfo, uint>(<>f__am$cache5).ToList<PArenaFightInfo>();
                Singleton<ArenaMode>.Instance.UpdateRankList(pList);
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void UpdateRoleArenaInfo(INetData data)
        {
            ArenaRoleInfoMsg_43_1 g__ = new ArenaRoleInfoMsg_43_1();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<ArenaMode>.Instance.UpdateMyInfo(g__.pos, g__.challengeTimes, g__.buyTimes, g__.cd);
            }
            else
            {
                Singleton<ArenaView>.Instance.CloseView();
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        public PArenaFightInfo RevengeTarget { get; set; }
    }
}

