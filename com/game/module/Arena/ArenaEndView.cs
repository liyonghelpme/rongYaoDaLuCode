﻿namespace com.game.module.Arena
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.WiFiPvP;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ArenaEndView : BaseView<ArenaEndView>
    {
        [CompilerGenerated]
        private static Func<PNewAttr, GeneralBriefIconInfo> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<PGeneralFightAttr, PNewAttr> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<PGeneralFightAttr, PNewAttr> <>f__am$cacheA;
        private GameObject award;
        private UILabel awardLabel;
        private Button btnClose;
        private Button clickToClose;
        private string fightCounterString;
        private GameObject states;
        private GameObject teamA;
        private GameObject teamB;

        private void ClearAndClose()
        {
            ArenaManager.Instance.Clear();
            this.CloseView();
        }

        private string GetKillCountString(FightCounter fc)
        {
            object[] objArray1 = new object[] { "(", fc.Key, " / ", fc.Value, ")" };
            return string.Concat(objArray1);
        }

        protected override void HandleAfterOpenView()
        {
            if (Singleton<SystemSettingsView>.Instance.IsOpened)
            {
                Singleton<SystemSettingsView>.Instance.CloseView();
            }
            if (<>f__am$cache9 == null)
            {
                <>f__am$cache9 = x => x.attr;
            }
            List<PNewAttr> list = ArenaManager.Instance.MyAttrList.Select<PGeneralFightAttr, PNewAttr>(<>f__am$cache9).ToList<PNewAttr>();
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = x => x.attr;
            }
            List<PNewAttr> list2 = ArenaManager.Instance.EnemyAttrList.Select<PGeneralFightAttr, PNewAttr>(<>f__am$cacheA).ToList<PNewAttr>();
            if ((list.Count != 0) && (list2.Count != 0))
            {
                this.InitAward();
                this.InitFighter(this.ParseToIconList(list), true);
                this.InitFighter(this.ParseToIconList(list2), false);
                this.ShowWinOrLose(ArenaManager.Instance.Status);
            }
        }

        protected override void HandleBeforeCloseView()
        {
            Singleton<ArenaControl>.Instance.SwitchToMainCity();
        }

        protected override void Init()
        {
            this.InitView();
            this.InitHandler();
            this.InitLanguage();
        }

        private void InitAward()
        {
            string word;
            uint ranking = Singleton<ArenaMode>.Instance.LastChallenger.ranking;
            uint myRanking = Singleton<ArenaMode>.Instance.MyRanking;
            if (ranking > myRanking)
            {
                word = LanguageManager.GetWord("Arena.EndViewNotChange");
            }
            else
            {
                word = (ArenaManager.Instance.Status != ArenaConst.ArenaStatus.win) ? LanguageManager.GetWord("Arena.EndViewIFailed") : ((myRanking != 0x1869f) ? LanguageManager.GetWord("Arena.EndViewIWon", new string[2]) : LanguageManager.GetWord("Arena.EndViewFirstWon", new string[] { ranking.ToString() }));
            }
            this.awardLabel.text = word;
        }

        private void InitFighter(IEnumerable<GeneralBriefIconInfo> list, bool isMe)
        {
            GameObject parent = !isMe ? this.teamB : this.teamA;
            GameObject obj3 = NGUITools.FindChild(parent, "hero1");
            GameObject obj4 = NGUITools.FindChild(parent, "hero2");
            GameObject obj5 = NGUITools.FindChild(parent, "hero3");
            UILabel label = NGUITools.FindInChild<UILabel>(parent, "num");
            GameObject[] objArray = new GameObject[] { obj3, obj4, obj5 };
            foreach (GameObject obj6 in objArray)
            {
                obj6.SetActive(false);
            }
            int num2 = 0;
            IEnumerator<GeneralBriefIconInfo> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GeneralBriefIconInfo current = enumerator.Current;
                    FightCounter staticsByTid = ArenaManager.Instance.GetStaticsByTid(current.id, isMe);
                    GameObject obj7 = objArray[num2++];
                    UILabel label2 = NGUITools.FindInChild<UILabel>(obj7, "name");
                    UISprite sprite = NGUITools.FindInChild<UISprite>(obj7, "head/icon");
                    UILabel label3 = NGUITools.FindInChild<UILabel>(obj7, "kill/num");
                    NGUITools.FindInChild<UILabel>(obj7, "kill/kill_type").text = this.fightCounterString;
                    label2.text = current.name;
                    sprite.spriteName = current.generalTid;
                    if (staticsByTid == null)
                    {
                        label3.text = "(0/0)";
                    }
                    else
                    {
                        label3.text = this.GetKillCountString(staticsByTid);
                    }
                    obj7.SetActive(true);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            label.text = (!isMe ? ArenaManager.Instance.EnemyTotalKill : ArenaManager.Instance.TotalKill).ToString();
        }

        private void InitHandler()
        {
            this.clickToClose.onClick = go => this.ClearAndClose();
            this.btnClose.onClick = go => this.ClearAndClose();
        }

        private void InitLanguage()
        {
            this.fightCounterString = LanguageManager.GetWord("Arena.EndViewFighterCount");
        }

        private void InitView()
        {
            this.states = base.FindChild("states");
            this.teamA = base.FindChild("teamA");
            this.teamB = base.FindChild("teamB");
            this.award = base.FindChild("award");
            this.clickToClose = base.FindInChild<Button>("mask");
            this.btnClose = base.FindInChild<Button>("close");
            this.awardLabel = NGUITools.FindInChild<UILabel>(this.award, "Label");
        }

        private List<GeneralBriefIconInfo> ParseToIconList(IEnumerable<PNewAttr> list)
        {
            if (<>f__am$cache8 == null)
            {
                <>f__am$cache8 = attr => new GeneralBriefIconInfo { generalTid = attr.generalId.ToString(), id = attr.id, name = BaseDataMgr.instance.GetGeneralVo(attr.generalId, attr.quality).name };
            }
            return list.Select<PNewAttr, GeneralBriefIconInfo>(<>f__am$cache8).ToList<GeneralBriefIconInfo>();
        }

        private void ShowWinOrLose(ArenaConst.ArenaStatus state)
        {
            UISprite rect = NGUITools.FindInChild<UISprite>(this.states, "state1");
            UISprite sprite2 = NGUITools.FindInChild<UISprite>(this.states, "state2");
            if (state == ArenaConst.ArenaStatus.win)
            {
                rect.SetActive(true);
                sprite2.SetActive(false);
            }
            else if (state == ArenaConst.ArenaStatus.failed)
            {
                rect.SetActive(false);
                sprite2.SetActive(true);
            }
            else
            {
                rect.SetActive(false);
                sprite2.SetActive(true);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Arena/ArenaEndView.assetbundle";
            }
        }
    }
}

