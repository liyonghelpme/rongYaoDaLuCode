﻿namespace com.game.module.Arena
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ArenaHeadView
    {
        private GameObject _gameObject;
        private UISprite _headIcon;
        private bool _isInitalized;
        private UILabel _lvl;
        private readonly IList<UISprite> _starList = new List<UISprite>();
        private const int STAR_NUM = 5;

        public ArenaHeadView(GameObject go)
        {
            this.gameObject = go;
        }

        private UISprite GetStarSprite(int idx)
        {
            string path = "starList/star" + idx;
            return NGUITools.FindInChild<UISprite>(this._gameObject, path);
        }

        private void Init()
        {
            if (!this._isInitalized)
            {
                this._headIcon = NGUITools.FindInChild<UISprite>(this._gameObject, "headIcon");
                this._lvl = NGUITools.FindInChild<UILabel>(this._gameObject, "lv");
                if (this._lvl.fontSize < 0x16)
                {
                    this._lvl.fontSize = 0x16;
                }
                this._starList.Clear();
                for (int i = 1; i <= 5; i++)
                {
                    UISprite starSprite = this.GetStarSprite(i);
                    starSprite.gameObject.SetActive(false);
                    this._starList.Add(starSprite);
                }
                this._isInitalized = true;
            }
        }

        public void Setup(PGeneralIconInfo p)
        {
            if (!this._isInitalized)
            {
                this.Init();
            }
            this._headIcon.spriteName = p.generalId.ToString();
            this._lvl.text = p.lvl.ToString();
            for (int i = 0; i < p.star; i++)
            {
                this._starList[i].gameObject.SetActive(true);
            }
        }

        public GameObject gameObject
        {
            get
            {
                return this._gameObject;
            }
            set
            {
                this._gameObject = value;
                this.Init();
            }
        }
    }
}

