﻿namespace com.game.module.Arena
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.Public.Confirm;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class ArenaHelper
    {
        [CompilerGenerated]
        private static Func<GeneralInfo, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static ClickCallback <>f__am$cache1;

        public static void CheckChellengeBegin(ulong roleId, uint pos, Action<ulong, uint> execution)
        {
            if (execution == null)
            {
                Debug.LogError("execution方法为空");
            }
            else
            {
                CheckStep1(roleId, pos, execution);
            }
        }

        private static void CheckStep1(ulong roleId, uint pos, Action<ulong, uint> execution)
        {
            <CheckStep1>c__AnonStoreyD2 yd = new <CheckStep1>c__AnonStoreyD2 {
                roleId = roleId,
                pos = pos,
                execution = execution
            };
            if (<>f__am$cache0 == null)
            {
                <>f__am$cache0 = x => x.place > 0;
            }
            if (Singleton<GeneralMode>.Instance.generalInfoList.Values.Count<GeneralInfo>(<>f__am$cache0) < 3)
            {
                string word = LanguageManager.GetWord("Arena.PromptPlayersNotEnough");
                ConfirmMgr.Instance.ShowOkCancelAlert(word, "OK_CANCEL", new ClickCallback(yd.<>m__49));
            }
            else
            {
                CheckStep2(yd.roleId, yd.pos, yd.execution);
            }
        }

        private static void CheckStep2(ulong roleId, uint pos, Action<ulong, uint> execution)
        {
            if (!Singleton<ArenaMode>.Instance.IsAnyChanceLeft)
            {
                ArenaControl.DoRequestAddChance();
            }
            else
            {
                CheckStep3(roleId, pos, execution);
            }
        }

        private static void CheckStep3(ulong roleId, uint pos, Action<ulong, uint> execution)
        {
            if (Singleton<ArenaMode>.Instance.ChallengeCD > 0f)
            {
                string[] param = new string[] { 20.ToString() };
                string word = LanguageManager.GetWord("Arena.PromptClearCD", param);
                if (<>f__am$cache1 == null)
                {
                    <>f__am$cache1 = () => Singleton<ArenaControl>.Instance.ClearCD();
                }
                ConfirmMgr.Instance.ShowOkCancelAlert(word, "OK_CANCEL", <>f__am$cache1);
            }
            else
            {
                execution(roleId, pos);
            }
        }

        public static List<ArenaAward> GetArenaAwardFromString(string str)
        {
            List<ArenaAward> list = new List<ArenaAward>();
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(str);
            for (int i = 0; i < arrayStringToInt.Length; i += 2)
            {
                list.Add(new ArenaAward(Convert.ToUInt32(arrayStringToInt[i]), Convert.ToUInt32(arrayStringToInt[i + 1])));
            }
            return list;
        }

        public static string GetAwardContentString(string str)
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            Dictionary<int, int> dictionary2 = new Dictionary<int, int>();
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(str);
            int num = arrayStringToInt.Length / 2;
            for (int i = 0; i < num; i++)
            {
                int num4;
                int index = 2 * i;
                if (arrayStringToInt[index] < 0x186a0)
                {
                    if (dictionary.ContainsKey(arrayStringToInt[index]))
                    {
                        Dictionary<int, int> dictionary3;
                        num4 = dictionary3[num4];
                        (dictionary3 = dictionary)[num4 = arrayStringToInt[index]] = num4 + arrayStringToInt[index + 1];
                    }
                    else
                    {
                        dictionary.Add(arrayStringToInt[index], arrayStringToInt[index + 1]);
                    }
                }
                else if (dictionary2.ContainsKey(arrayStringToInt[index]))
                {
                    Dictionary<int, int> dictionary4;
                    num4 = dictionary4[num4];
                    (dictionary4 = dictionary2)[num4 = arrayStringToInt[index]] = num4 + arrayStringToInt[index + 1];
                }
                else
                {
                    dictionary2.Add(arrayStringToInt[index], arrayStringToInt[index + 1]);
                }
            }
            string str2 = string.Empty;
            foreach (KeyValuePair<int, int> pair in dictionary)
            {
                string str3;
                switch (pair.Key)
                {
                    case 1:
                        str3 = "钻石";
                        break;

                    case 2:
                        str3 = "绑定钻石";
                        break;

                    case 3:
                        str3 = "金币";
                        break;

                    case 4:
                        str3 = "绑定金币";
                        break;

                    default:
                        str3 = "未知";
                        break;
                }
                object[] objArray1 = new object[] { str2, str3, "*", pair.Value, ", " };
                str2 = string.Concat(objArray1);
            }
            foreach (KeyValuePair<int, int> pair2 in dictionary2)
            {
                SysItemsVo dataByTypeAndId = BaseDataMgr.instance.GetDataByTypeAndId<SysItemsVo>("SysItemsVo", pair2.Key);
                object[] objArray2 = new object[] { str2, dataByTypeAndId.name, "*", pair2.Value, ", " };
                str2 = string.Concat(objArray2);
            }
            char[] trimChars = new char[] { ' ' };
            char[] chArray2 = new char[] { ',' };
            return str2.TrimEnd(trimChars).TrimEnd(chArray2);
        }

        public static Chanllenger ToChanllenger(this PArenaFightInfo p)
        {
            return new Chanllenger(p.id, p.name, p.canChallenge, p.fightPoint, p.pos, p.generalList);
        }

        public static PArenaFightInfo ToPChallenger(this Chanllenger c)
        {
            return new PArenaFightInfo { id = c.roleId, name = c.name, pos = c.ranking, fightPoint = c.fightingPoints, generalList = c.generalList.ToList<PGeneralIconInfo>() };
        }

        [CompilerGenerated]
        private sealed class <CheckStep1>c__AnonStoreyD2
        {
            internal Action<ulong, uint> execution;
            internal uint pos;
            internal ulong roleId;

            internal void <>m__49()
            {
                ArenaHelper.CheckStep2(this.roleId, this.pos, this.execution);
            }
        }
    }
}

