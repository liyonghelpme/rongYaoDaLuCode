﻿namespace com.game.module.Arena
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using System;
    using UnityEngine;

    public class ArenaLogItemView
    {
        private UILabel _contentLabel;
        private GameObject _gameObject;
        private bool _isInitalized;
        private bool _isMeWin;
        private byte _logType;
        private uint _meRankAfter;
        private string _name;
        private Button _revengeBtn;
        private UILabel _revengeBtnLabel;
        private ulong _roleId;
        private int _time;
        private UILabel _timeLabel;

        public ArenaLogItemView(GameObject go)
        {
            this._gameObject = go;
        }

        private void Init()
        {
            if (!this._isInitalized)
            {
                this.InitView();
                this.InitHandler();
                this.InitLanguage();
                this._isInitalized = true;
            }
        }

        private void InitHandler()
        {
            this._revengeBtn.onClick = go => Singleton<ArenaControl>.Instance.RequestRevengeTargetInfo(this._roleId);
        }

        private void InitLanguage()
        {
            this._revengeBtnLabel.text = LanguageManager.GetWord("Arena.RevengeBtnLabel");
        }

        private void InitView()
        {
            this._revengeBtn = NGUITools.FindInChild<Button>(this._gameObject, "challege_btn");
            this._revengeBtnLabel = NGUITools.FindInChild<UILabel>(this._gameObject, "challege_btn/label");
            this._timeLabel = NGUITools.FindInChild<UILabel>(this._gameObject, "timeLabel");
            this._contentLabel = NGUITools.FindInChild<UILabel>(this._gameObject, "logLabel");
        }

        public void Setup(LogItem log)
        {
            if (!this._isInitalized)
            {
                this.Init();
            }
            this._roleId = log.roleId;
            this._meRankAfter = log.newRank;
            this._logType = log.type;
            this._isMeWin = (log.type == 1) || (log.type == 2);
            this._name = log.name;
            this._time = log.timestamp;
            this.UpdateLog();
        }

        private void UpdateLog()
        {
            this._timeLabel.text = StringUtils.GetTimeFromUnixTimestamp((long) this._time).ToString("HH:mm");
            string[] param = new string[] { this._name, this._meRankAfter.ToString() };
            string word = string.Empty;
            switch (this._logType)
            {
                case 0:
                    word = LanguageManager.GetWord("Arena.LogChallengeIFailed", param);
                    break;

                case 1:
                    word = LanguageManager.GetWord("Arena.LogChallengeIWon", param);
                    break;

                case 2:
                    word = LanguageManager.GetWord("Arena.LogRevengeIWon", param);
                    break;

                case 3:
                    word = LanguageManager.GetWord("Arena.LogRevengedEnemyWon", param);
                    break;
            }
            this._contentLabel.text = word;
            this._revengeBtn.SetActive(!this._isMeWin);
        }

        public GameObject gameObject
        {
            get
            {
                return this._gameObject;
            }
            set
            {
                this._gameObject = value;
                this.Init();
            }
        }
    }
}

