﻿namespace com.game.module.Arena
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ArenaLogView : BaseView<ArenaLogView>
    {
        private Button _bgBtn;
        private Button _closeBtn;
        private GameObject _contentItem;
        private UIGrid _grid;
        private GameObject _list;
        private readonly IList<ArenaLogItemView> _logList = new List<ArenaLogItemView>();
        private UILabel _title;

        private void DestroyAll()
        {
            IEnumerator<ArenaLogItemView> enumerator = this._logList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ArenaLogItemView current = enumerator.Current;
                    UnityEngine.Object.Destroy(current.gameObject);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this._logList.Clear();
        }

        protected override void HandleAfterOpenView()
        {
            SpringPosition.Begin(this._list, Vector3.zero, 100f);
            this.RefreshLogList();
        }

        protected override void Init()
        {
            this.InitView();
            this.InitHandler();
            this.InitLanguage();
        }

        private void InitHandler()
        {
            this._closeBtn.onClick = go => this.CloseView();
            this._bgBtn.onClick = go => this.CloseView();
        }

        private void InitLanguage()
        {
            this._title.text = LanguageManager.GetWord("Arena.LogTitle");
        }

        private void InitLogItemList()
        {
            this.DestroyAll();
            IEnumerator<LogItem> enumerator = Singleton<ArenaMode>.Instance.LogList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    LogItem current = enumerator.Current;
                    ArenaLogItemView item = new ArenaLogItemView(NGUITools.AddChild(this._grid.gameObject, this._contentItem));
                    item.Setup(current);
                    item.gameObject.SetActive(true);
                    this._logList.Add(item);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this._grid.repositionNow = true;
        }

        private void InitView()
        {
            this._bgBtn = base.FindInChild<Button>("mask");
            this._list = base.FindChild("list");
            this._title = base.FindInChild<UILabel>("list/logTitle");
            this._closeBtn = base.FindInChild<Button>("closeBtn");
            this._grid = base.FindInChild<UIGrid>("list/grid");
            this._contentItem = base.FindChild("list/grid/logItem");
        }

        public void RefreshLogList()
        {
            this.DestroyAll();
            this.InitLogItemList();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

