﻿namespace com.game.module.Arena
{
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ArenaMode : BaseMode<ArenaMode>
    {
        private uint _buyCount;
        private float _challengeCD;
        private float _lastChangeCD;
        private IList<LogItem> _logList = new List<LogItem>();
        [CompilerGenerated]
        private static Func<PArenaFightLog, LogItem> <>f__am$cacheA;
        public string ChallengeRules;
        public IList<Chanllenger> RankList = new List<Chanllenger>();

        public void ClearTickCache()
        {
            this._lastChangeCD = float.MaxValue;
        }

        public void Update(int times, float dt)
        {
            if (this.ChallengeCD <= 0f)
            {
                this.ChallengeCD = 0f;
            }
            else
            {
                this.ChallengeCD -= dt;
            }
        }

        public void UpdateLogList(IEnumerable<PArenaFightLog> list)
        {
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = log => new LogItem { name = log.name, newRank = log.pos, roleId = log.roleId, timestamp = Convert.ToInt32(log.time), type = log.type };
            }
            this._logList = list.Select<PArenaFightLog, LogItem>(<>f__am$cacheA).ToList<LogItem>();
            base.DataUpdate(3);
        }

        public void UpdateMyInfo(uint position, ushort challengeCount, uint buyTimes, uint cd)
        {
            this.MyRanking = position;
            this.BuyChanceCount = buyTimes;
            this.ChallengeCD = cd * 0.001f;
            this.ChallengedCount = challengeCount;
            this.TotalChallengeCount = 5 + buyTimes;
            base.DataUpdate(2);
        }

        public void UpdateRankList(IEnumerable<PArenaFightInfo> pList)
        {
            this.RankList.Clear();
            IEnumerator<PArenaFightInfo> enumerator = pList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PArenaFightInfo current = enumerator.Current;
                    this.RankList.Add(current.ToChanllenger());
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            base.DataUpdate(1);
        }

        public uint BuyChanceCount
        {
            get
            {
                return this._buyCount;
            }
            set
            {
                this._buyCount = value;
                base.DataUpdate(5);
            }
        }

        public float ChallengeCD
        {
            get
            {
                return this._challengeCD;
            }
            set
            {
                this._challengeCD = value;
                if (Mathf.Abs((float) (this._challengeCD - this._lastChangeCD)) > 1f)
                {
                    base.DataUpdate(4);
                    this._lastChangeCD = this._challengeCD;
                }
            }
        }

        public uint ChallengedCount { get; set; }

        public bool IsAnyChanceLeft
        {
            get
            {
                return (this.ChallengedCount < this.TotalChallengeCount);
            }
        }

        public Chanllenger LastChallenger { get; set; }

        public IList<LogItem> LogList
        {
            get
            {
                return this._logList;
            }
        }

        public uint MyRanking { get; set; }

        public uint TotalChallengeCount { get; set; }
    }
}

