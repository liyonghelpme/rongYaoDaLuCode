﻿namespace com.game.module.Arena
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ArenaRankInfoItemView
    {
        private Button _challengeBtn;
        private Chanllenger _challenger;
        private UILabel _fightPoint;
        private UILabel _fightPointLabel;
        private GameObject _gameObject;
        private readonly List<ArenaHeadView> _headViews = new List<ArenaHeadView>();
        private bool _isInitalized;
        private UILabel _name;
        private uint _pos;
        private UILabel _rank;
        private UISprite _rankIcon;
        private ulong _roleId;
        [CompilerGenerated]
        private static Action<ulong, uint> <>f__am$cacheC;
        private const int HEAD_VIEW_NUM = 3;

        public ArenaRankInfoItemView(GameObject go)
        {
            this._gameObject = go;
        }

        private void Init()
        {
            if (!this._isInitalized)
            {
                this.InitView();
                this.InitHandler();
                this.InitLanguage();
                this._isInitalized = true;
            }
        }

        private void InitHandler()
        {
            this._challengeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnChallengeClick);
        }

        private void InitHeadList()
        {
            for (int i = 0; i < 3; i++)
            {
                ArenaHeadView item = new ArenaHeadView(NGUITools.FindChild(this._gameObject, "headList/headView" + i));
                this._headViews.Add(item);
            }
        }

        private void InitLanguage()
        {
            this._fightPointLabel.text = LanguageManager.GetWord("Arena.FightPoint");
        }

        private void InitView()
        {
            this._rankIcon = NGUITools.FindInChild<UISprite>(this._gameObject, "rankIcon");
            this._rank = NGUITools.FindInChild<UILabel>(this._gameObject, "rankLabel");
            this._name = NGUITools.FindInChild<UILabel>(this._gameObject, "name");
            this._fightPointLabel = NGUITools.FindInChild<UILabel>(this._gameObject, "fightPointLabel");
            this._fightPoint = NGUITools.FindInChild<UILabel>(this._gameObject, "fightPoint");
            this._challengeBtn = NGUITools.FindInChild<Button>(this._gameObject, "challengeBtn");
            this.InitHeadList();
        }

        private void OnChallengeClick(GameObject go)
        {
            if (this._challenger == null)
            {
                Debug.LogError("欲挑战的对手信息为null");
            }
            else
            {
                Singleton<ArenaMode>.Instance.LastChallenger = this._challenger;
                if (this._challenger.roleId != MeVo.instance.Id)
                {
                    if (<>f__am$cacheC == null)
                    {
                        <>f__am$cacheC = (roleId, pos) => Singleton<ArenaControl>.Instance.RequestChallenge(roleId, pos);
                    }
                    ArenaHelper.CheckChellengeBegin(this._roleId, this._pos, <>f__am$cacheC);
                }
            }
        }

        public void Setup(Chanllenger p)
        {
            if (!this._isInitalized)
            {
                this.Init();
            }
            this._challenger = p;
            this._pos = p.ranking;
            this._rank.text = "No." + this._pos;
            if (this._pos <= 3)
            {
                this._rank.SetActive(false);
                this._rankIcon.SetActive(true);
                this._rankIcon.spriteName = "NO." + this._pos;
            }
            else
            {
                this._rank.SetActive(true);
                this._rankIcon.SetActive(false);
            }
            this._roleId = p.roleId;
            this._name.text = p.name;
            this._name.SetActive(true);
            this._fightPoint.text = p.fightingPoints.ToString();
            this._fightPoint.SetActive(true);
            if ((MeVo.instance.Id == p.roleId) || !p.canChallenge)
            {
                this._challengeBtn.gameObject.SetActive(false);
            }
            foreach (ArenaHeadView view in this._headViews)
            {
                view.gameObject.SetActive(false);
            }
            for (int i = 0; i < p.generalList.Count; i++)
            {
                this._headViews[i].gameObject.SetActive(true);
                this._headViews[i].Setup(p.generalList[i]);
            }
        }

        public GameObject gameObject
        {
            get
            {
                return this._gameObject;
            }
            set
            {
                this._gameObject = value;
                this.Init();
            }
        }
    }
}

