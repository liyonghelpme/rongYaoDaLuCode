﻿namespace com.game.module.Arena
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ArenaRankInfoView : BaseView<ArenaRankInfoView>
    {
        private GameObject _baseItem;
        private UIGrid _grid;
        private readonly List<ArenaRankInfoItemView> _rankList = new List<ArenaRankInfoItemView>();
        private UIScrollView _scrollView;

        private void ClearObsoleteFighter()
        {
            foreach (ArenaRankInfoItemView view in this._rankList)
            {
                UnityEngine.Object.Destroy(view.gameObject);
            }
            this._rankList.Clear();
        }

        protected override void HandleAfterOpenView()
        {
        }

        protected override void Init()
        {
            this._scrollView = base.FindInChild<UIScrollView>("list");
            this._grid = base.FindInChild<UIGrid>("list/grid");
            this._baseItem = base.FindChild("list/grid/item");
            this._baseItem.SetActive(false);
        }

        public void RefreshRankList()
        {
            this.ClearObsoleteFighter();
            IEnumerator<Chanllenger> enumerator = Singleton<ArenaMode>.Instance.RankList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Chanllenger current = enumerator.Current;
                    ArenaRankInfoItemView item = new ArenaRankInfoItemView(NGUITools.AddChild(this._grid.gameObject, this._baseItem));
                    item.Setup(current);
                    item.gameObject.SetActive(true);
                    this._rankList.Add(item);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this._grid.repositionNow = true;
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

