﻿namespace com.game.module.Arena
{
    using com.game.basic;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class ArenaReadyMaskView : BaseView<ArenaReadyMaskView>
    {
        private float _cd;
        private UILabel _cdLabel;
        private readonly vp_Timer.Handle _timehanHandle = new vp_Timer.Handle();

        private void DoCountingDown()
        {
            this._cd--;
            if (this._cd <= 0f)
            {
                this.HandleCountDownOver();
            }
            else
            {
                this._cdLabel.text = Mathf.CeilToInt(this._cd).ToString();
            }
        }

        protected override void HandleAfterOpenView()
        {
            this._cd = ArenaManager.Instance.CountDown;
            if (this._cd <= 0f)
            {
                this.HandleCountDownOver();
            }
            else
            {
                this._cdLabel.text = Mathf.FloorToInt(this._cd).ToString();
                vp_Timer.In(1f, new vp_Timer.Callback(this.DoCountingDown), 0, 1f, this._timehanHandle);
            }
        }

        private void HandleCountDownOver()
        {
            vp_Timer.CancelTimerByHandle(this._timehanHandle);
            GlobalAPI.facade.Notify(200, 0, 0, null);
            this.CloseView();
        }

        protected override void Init()
        {
            this._cdLabel = base.FindInChild<UILabel>("countdown");
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopUILayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Arena/ArenaReadyMask.assetbundle";
            }
        }
    }
}

