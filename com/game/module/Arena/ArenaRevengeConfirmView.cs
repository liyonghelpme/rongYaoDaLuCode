﻿namespace com.game.module.Arena
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Confirm;
    using com.game.vo;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ArenaRevengeConfirmView : BaseView<ArenaRevengeConfirmView>
    {
        private Button _bgBtn;
        private Button _btnCancel;
        private Button _btnOK;
        private UILabel _fightPointsLabel;
        private readonly List<ArenaHeadView> _headViews = new List<ArenaHeadView>();
        private UILabel _nameLabel;
        private UILabel _posLabel;
        private const int HEAD_VIEW_NUM = 3;
        private PArenaFightInfo revengeTarget;

        private void ClearRevengeTarget()
        {
            this.revengeTarget = null;
            Singleton<ArenaControl>.Instance.RevengeTarget = null;
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdateRevengeTarget();
        }

        protected override void HandleBeforeCloseView()
        {
            this.revengeTarget = null;
            foreach (ArenaHeadView view in this._headViews)
            {
                view.gameObject.SetActive(false);
            }
            this._posLabel.text = 0.ToString();
            this._fightPointsLabel.text = 0.ToString();
            this._nameLabel.text = "null";
        }

        protected override void Init()
        {
            this.InitView();
            this.InitHandler();
            this.InitLanguage();
        }

        private void InitHandler()
        {
            this._btnCancel.onClick = delegate (GameObject go) {
                this.ClearRevengeTarget();
                this.CloseView();
            };
            this._bgBtn.onClick = delegate (GameObject go) {
                this.ClearRevengeTarget();
                this.CloseView();
            };
            this._btnOK.onClick = delegate (GameObject go) {
                this.UpdateRevengeTarget();
                if (this.revengeTarget.pos > Singleton<ArenaMode>.Instance.MyRanking)
                {
                    ConfirmMgr.Instance.ShowOkCancelAlert(LanguageManager.GetWord("Arena.RevengeTip"), "OK_CANCEL", new ClickCallback(this.OnRevengeClick));
                }
                else
                {
                    this.OnRevengeClick();
                }
            };
        }

        private void InitHeadList()
        {
            for (int i = 0; i < 3; i++)
            {
                ArenaHeadView item = new ArenaHeadView(base.FindChild("headList/headView" + i));
                this._headViews.Add(item);
            }
        }

        private void InitLanguage()
        {
            this._btnOK.label.text = LanguageManager.GetWord("Arena.RevengeConfirmOK");
            this._btnCancel.label.text = LanguageManager.GetWord("Arena.RevengeConfirmCancel");
        }

        private void InitView()
        {
            this._bgBtn = base.FindInChild<Button>("mask");
            this._btnOK = base.FindInChild<Button>("btnOK");
            this._btnCancel = base.FindInChild<Button>("btnCancel");
            this._nameLabel = base.FindInChild<UILabel>("name");
            this._posLabel = base.FindInChild<UILabel>("pos");
            this._fightPointsLabel = base.FindInChild<UILabel>("fightPoint");
            this.InitHeadList();
        }

        private void OnRevengeClick()
        {
            Chanllenger chanllenger = new Chanllenger {
                fightingPoints = this.revengeTarget.fightPoint,
                generalList = this.revengeTarget.generalList,
                name = this.revengeTarget.name,
                ranking = this.revengeTarget.pos,
                roleId = this.revengeTarget.id,
                canChallenge = true
            };
            Singleton<ArenaMode>.Instance.LastChallenger = chanllenger;
            if (this.revengeTarget.id != MeVo.instance.Id)
            {
                ArenaHelper.CheckChellengeBegin(this.revengeTarget.id, 0, delegate (ulong roleId, uint pos) {
                    Singleton<ArenaControl>.Instance.RequestRevenge(this.revengeTarget.id);
                    this.ClearRevengeTarget();
                });
            }
        }

        private void UpdateRevengeTarget()
        {
            this.revengeTarget = Singleton<ArenaControl>.Instance.RevengeTarget;
            if (this.revengeTarget == null)
            {
                Debug.LogError("挑战对象为null");
            }
            else
            {
                this._nameLabel.text = this.revengeTarget.name;
                string[] param = new string[] { this.revengeTarget.fightPoint.ToString() };
                this._fightPointsLabel.text = LanguageManager.GetWord("Arena.RevengeConfirmFightPointsBegin", param);
                string[] textArray2 = new string[] { this.revengeTarget.pos.ToString() };
                this._posLabel.text = LanguageManager.GetWord("Arena.RevengeConfirmPosBegin", textArray2);
                foreach (ArenaHeadView view in this._headViews)
                {
                    view.gameObject.SetActive(false);
                }
                List<PGeneralIconInfo> generalList = this.revengeTarget.generalList;
                for (int i = 0; i < generalList.Count; i++)
                {
                    PGeneralIconInfo p = new PGeneralIconInfo {
                        generalId = generalList[i].generalId,
                        lvl = generalList[i].lvl,
                        star = generalList[i].star
                    };
                    this._headViews[i].Setup(p);
                    this._headViews[i].gameObject.SetActive(true);
                }
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }
    }
}

