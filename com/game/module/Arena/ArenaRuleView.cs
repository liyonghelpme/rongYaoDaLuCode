﻿namespace com.game.module.Arena
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ArenaRuleView : BaseView<ArenaRuleView>
    {
        private GameObject _awardLabelItem;
        private UILabel _awardTitle;
        private Button _bg_btn;
        private Button _close_btn;
        private UIGrid _grid;
        private UILabel _ruleLabel;
        private readonly IList<GameObject> _ruleList = new List<GameObject>();
        private UILabel _ruleTitle;

        private void DestroyAll()
        {
            IEnumerator<GameObject> enumerator = this._ruleList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GameObject current = enumerator.Current;
                    UnityEngine.Object.Destroy(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this._ruleList.Clear();
        }

        protected override void HandleAfterOpenView()
        {
            this.RefreshRule();
        }

        protected override void Init()
        {
            this.InitView();
            this.InitHandler();
            this.InitLanguage();
        }

        private void InitHandler()
        {
            this._close_btn.onClick = go => this.CloseView();
            this._bg_btn.onClick = go => this.CloseView();
        }

        private void InitLanguage()
        {
            this._awardTitle.text = LanguageManager.GetWord("Arena.AwardTitle");
            this._ruleTitle.text = LanguageManager.GetWord("Arena.RuleTitle");
            this._ruleLabel.text = LanguageManager.GetWord("Arena.RuleContent");
        }

        private void InitRuleItemList(IEnumerable<SysArenaRankAwardVo> ruleList)
        {
            IEnumerator<SysArenaRankAwardVo> enumerator = ruleList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    SysArenaRankAwardVo current = enumerator.Current;
                    string str = current.pos_start.ToString();
                    string str2 = current.pos_end.ToString();
                    string awardContentString = ArenaHelper.GetAwardContentString(current.award_list);
                    string str4 = !(str != str2) ? LanguageManager.GetWord("Arena.AwardTipBeginSinglePos", new string[] { str, awardContentString }) : LanguageManager.GetWord("Arena.AwardTipBegin", new string[] { str, str2, awardContentString });
                    GameObject item = NGUITools.AddChild(this._grid.gameObject, this._awardLabelItem);
                    item.GetComponent<UILabel>().text = str4;
                    item.SetActive(true);
                    this._ruleList.Add(item);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this._grid.repositionNow = true;
        }

        private void InitView()
        {
            this._close_btn = base.FindInChild<Button>("closeBtn");
            this._bg_btn = base.FindInChild<Button>("mask");
            this._grid = base.FindInChild<UIGrid>("list/grid");
            this._awardLabelItem = base.FindChild("list/grid/awardLabel");
            this._awardLabelItem.SetActive(false);
            this._awardTitle = base.FindInChild<UILabel>("list/awardTitle");
            this._ruleTitle = base.FindInChild<UILabel>("list/ruleTitle");
            this._ruleLabel = base.FindInChild<UILabel>("list/ruleLabel");
        }

        private void RefreshRule()
        {
            this.DestroyAll();
            List<SysArenaRankAwardVo> arenaRankAwardTemplateList = BaseDataMgr.instance.GetArenaRankAwardTemplateList();
            this.InitRuleItemList(arenaRankAwardTemplateList);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

