﻿namespace com.game.module.Arena
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ArenaSelfRankInfoView : BaseView<ArenaSelfRankInfoView>
    {
        private Button _addTimeBtn;
        private UILabel _awardLabel;
        private readonly List<ArenaAwardItemView> _awardList = new List<ArenaAwardItemView>();
        private uint _lastRank;
        private UILabel _leftTimes;
        private UILabel _leftTimesLabel;
        private Button _logBtn;
        private Button _refreshBtn;
        private GameObject _refreshGo;
        private UILabel _refreshTime;
        private UILabel _refreshTimeLabel;
        private Button _ruleBtn;
        private UILabel _selfRank;
        private UILabel _selfRankLabel;
        private Button _storeBtn;
        [CompilerGenerated]
        private static vp_Timer.Callback <>f__am$cacheF;
        private const int AWARD_LIST_NUM = 3;

        private void DisableRefreshBtnForAWhile(object cdTimeObj)
        {
            int num = (int) cdTimeObj;
            UILabel label = NGUITools.FindInChild<UILabel>(this._refreshGo, "Label");
            UISprite sprite = NGUITools.FindInChild<UISprite>(this._refreshGo, "bg");
            string word = LanguageManager.GetWord("Arena.RefreshPlayer");
            if ((num <= 0) || !base.IsOpened)
            {
                this._refreshBtn.isEnabled = true;
                label.text = word;
                sprite.ShowAsMyself();
            }
            else
            {
                vp_Timer.In(1f, new vp_Timer.ArgCallback(this.DisableRefreshBtnForAWhile), num - 1, null);
                this._refreshBtn.isEnabled = false;
                object[] objArray1 = new object[] { word, "(", num, ")" };
                label.text = string.Concat(objArray1);
                sprite.ShowAsGray();
            }
        }

        private void DoRefreshAwardList()
        {
            if (this._lastRank != Singleton<ArenaMode>.Instance.MyRanking)
            {
                this._lastRank = Singleton<ArenaMode>.Instance.MyRanking;
                IList<ArenaAward> dailyAward = GetDailyAward();
                int count = dailyAward.Count;
                foreach (ArenaAwardItemView view in this._awardList)
                {
                    view.gameObject.SetActive(false);
                }
                if (count == 0)
                {
                    ArenaAwardItemView view2 = this._awardList[1];
                    view2.gameObject.SetActive(true);
                    view2.SetupAnEmptyTip();
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        ArenaAwardItemView view3 = this._awardList[i];
                        if (i <= (count - 1))
                        {
                            ArenaAward award = dailyAward[i];
                            view3.gameObject.SetActive(true);
                            view3.Setup(award.template_id.ToString(), award.number.ToString(), award.template_id >= 0x186a0);
                        }
                        else
                        {
                            view3.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }

        private void DoRefreshCDTime()
        {
            int leftSecond = Mathf.FloorToInt(Singleton<ArenaMode>.Instance.ChallengeCD);
            this._refreshTime.color = Color.red;
            if (leftSecond > 0)
            {
                this._refreshTime.SetActive(true);
                this._refreshTimeLabel.SetActive(true);
                this._refreshTime.text = this.HandleTimeString(leftSecond);
            }
            else
            {
                this._refreshTime.SetActive(false);
                this._refreshTimeLabel.SetActive(false);
            }
        }

        private static IList<ArenaAward> GetDailyAward()
        {
            <GetDailyAward>c__AnonStoreyD5 yd = new <GetDailyAward>c__AnonStoreyD5 {
                nowPos = Singleton<ArenaMode>.Instance.MyRanking
            };
            SysArenaRankAwardVo vo = BaseDataMgr.instance.GetArenaRankAwardTemplateList().FirstOrDefault<SysArenaRankAwardVo>(new Func<SysArenaRankAwardVo, bool>(yd.<>m__63));
            if (vo == null)
            {
                return new List<ArenaAward>();
            }
            return ArenaHelper.GetArenaAwardFromString(vo.award_list);
        }

        protected override void HandleAfterOpenView()
        {
            Singleton<ArenaControl>.Instance.RequestMyInfo();
            if (<>f__am$cacheF == null)
            {
                <>f__am$cacheF = () => Singleton<ArenaControl>.Instance.RequestFullRankList();
            }
            vp_Timer.In(0.1f, <>f__am$cacheF, null);
        }

        protected override void HandleBeforeCloseView()
        {
        }

        private string HandleTimeString(int leftSecond)
        {
            if (leftSecond < 0)
            {
                leftSecond = 0;
            }
            int num = leftSecond / 60;
            int num2 = leftSecond % 60;
            int num3 = 0;
            if (leftSecond >= 0xe10)
            {
                num3 = num / 60;
                num = num % 60;
            }
            string str = (num >= 10) ? (num + string.Empty) : ("0" + num);
            string str2 = (num2 >= 10) ? (num2 + string.Empty) : ("0" + num2);
            string str3 = (num3 >= 10) ? (num3 + string.Empty) : ("0" + num3);
            return (str + ":" + str2);
        }

        protected override void Init()
        {
            this.InitView();
            this.InitHandler();
            this.InitLanguage();
        }

        private void InitAwardList()
        {
            for (int i = 0; i < 3; i++)
            {
                ArenaAwardItemView item = new ArenaAwardItemView(base.FindChild("awardList/item" + i));
                this._awardList.Add(item);
            }
        }

        private void InitHandler()
        {
            this._refreshBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnRefreshBtnClick);
            this._logBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnLogBtnClick);
            this._addTimeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnAddTimeBtnClick);
            this._ruleBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnRuleBtnClick);
            this._storeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnStoreBtnClick);
        }

        private void InitLanguage()
        {
            this._selfRankLabel.text = LanguageManager.GetWord("Arena.SelfRank");
            this._awardLabel.text = LanguageManager.GetWord("Arena.Award");
            this._leftTimesLabel.text = LanguageManager.GetWord("Arena.LeftTimes");
            this._refreshTimeLabel.text = LanguageManager.GetWord("Arena.RefreshTime");
            this._refreshBtn.label.text = LanguageManager.GetWord("Arena.RefreshPlayer");
            this._logBtn.label.text = LanguageManager.GetWord("Arena.ChallengeLog");
            this._storeBtn.label.text = LanguageManager.GetWord("Arena.Store");
            this._ruleBtn.label.text = LanguageManager.GetWord("Arena.Rule");
        }

        private void InitView()
        {
            this._selfRankLabel = base.FindInChild<UILabel>("label/rankLabel");
            this._awardLabel = base.FindInChild<UILabel>("label/awardLabel");
            this._refreshTimeLabel = base.FindInChild<UILabel>("label/refreshTimeLabel");
            this._leftTimesLabel = base.FindInChild<UILabel>("label/leftTimeLabel");
            this._selfRank = base.FindInChild<UILabel>("label/rank");
            this._refreshTime = base.FindInChild<UILabel>("label/refreshTime");
            this._leftTimes = base.FindInChild<UILabel>("label/leftTime");
            this._refreshBtn = base.FindInChild<Button>("refreshBtn");
            this._refreshGo = base.FindChild("refreshBtn");
            this._logBtn = base.FindInChild<Button>("logBtn");
            this._addTimeBtn = base.FindInChild<Button>("addTimeBtn");
            this._storeBtn = base.FindInChild<Button>("storeBtn");
            this._ruleBtn = base.FindInChild<Button>("ruleBtn");
            this.InitAwardList();
        }

        private void OnAddTimeBtnClick(GameObject go)
        {
            ArenaControl.DoRequestAddChance();
        }

        private void OnLogBtnClick(GameObject go)
        {
            Singleton<ArenaControl>.Instance.RequestLog();
            Singleton<ArenaLogView>.Instance.OpenView();
        }

        private void OnRefreshBtnClick(GameObject go)
        {
            Singleton<ArenaControl>.Instance.RequestRefreshFightList();
            this.DisableRefreshBtnForAWhile(5);
        }

        private void OnRuleBtnClick(GameObject go)
        {
            Singleton<ArenaRuleView>.Instance.OpenView();
        }

        private void OnStoreBtnClick(GameObject go)
        {
            Singleton<ShopView>.Instance.Show(0);
        }

        public void RefreshCDTimeOnly()
        {
            this.DoRefreshCDTime();
        }

        public void RefreshMyInfo()
        {
            if (base.IsOpened)
            {
                uint challengedCount = Singleton<ArenaMode>.Instance.ChallengedCount;
                uint totalChallengeCount = Singleton<ArenaMode>.Instance.TotalChallengeCount;
                uint myRanking = Singleton<ArenaMode>.Instance.MyRanking;
                this._selfRank.text = (myRanking >= 0x1869f) ? LanguageManager.GetWord("Arena.NotListed") : Singleton<ArenaMode>.Instance.MyRanking.ToString();
                this._leftTimes.text = (totalChallengeCount - challengedCount).ToString();
                this.DoRefreshAwardList();
                this.DoRefreshCDTime();
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        [CompilerGenerated]
        private sealed class <GetDailyAward>c__AnonStoreyD5
        {
            internal uint nowPos;

            internal bool <>m__63(SysArenaRankAwardVo x)
            {
                return (((x.pos_start <= this.nowPos) && (x.pos_end >= this.nowPos)) && (this.nowPos != 0x1869f));
            }
        }
    }
}

