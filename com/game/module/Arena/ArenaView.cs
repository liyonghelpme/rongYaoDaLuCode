﻿namespace com.game.module.Arena
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ArenaView : BaseView<ArenaView>
    {
        private Button _closeBtn;
        private UILabel _title;
        [CompilerGenerated]
        private static vp_Timer.Callback <>f__am$cache2;

        protected override void HandleAfterOpenView()
        {
            Singleton<ArenaMode>.Instance.ClearTickCache();
            Singleton<ArenaSelfRankInfoView>.Instance.OpenView();
            Singleton<ArenaRankInfoView>.Instance.OpenView();
            Singleton<ArenaLogView>.Instance.CloseView();
            Singleton<ArenaRuleView>.Instance.CloseView();
            Singleton<ArenaRevengeConfirmView>.Instance.CloseView();
        }

        protected override void HandleBeforeCloseView()
        {
            Singleton<ArenaMode>.Instance.ClearTickCache();
            Singleton<ArenaSelfRankInfoView>.Instance.CloseView();
            Singleton<ArenaRankInfoView>.Instance.CloseView();
            Singleton<ArenaLogView>.Instance.CloseView();
            Singleton<ArenaRuleView>.Instance.CloseView();
            Singleton<ArenaRevengeConfirmView>.Instance.CloseView();
        }

        protected override void Init()
        {
            this.InitView();
            this.InitHandler();
            this.InitLanguage();
        }

        private void InitHandler()
        {
            this._closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnCloseBtnClick);
        }

        private void InitLanguage()
        {
            this._title.text = LanguageManager.GetWord("Arena.Title");
        }

        private void InitView()
        {
            Singleton<ArenaSelfRankInfoView>.Instance.gameObject = base.FindChild("ArenaSelfInfoView");
            Singleton<ArenaRankInfoView>.Instance.gameObject = base.FindChild("ArenaRankInfoView");
            Singleton<ArenaLogView>.Instance.gameObject = base.FindChild("ArenaLogView");
            Singleton<ArenaRuleView>.Instance.gameObject = base.FindChild("ArenaRuleView");
            Singleton<ArenaRevengeConfirmView>.Instance.gameObject = base.FindChild("ArenaRevengeConfirmView");
            this._closeBtn = base.FindInChild<Button>("closeBtn");
            this._title = base.FindInChild<UILabel>("titleLabel");
        }

        private void OnCloseBtnClick(GameObject go)
        {
            this.CloseView();
        }

        public void OpenArenaRevengeConfirmView()
        {
            if (!base.IsOpened)
            {
                Singleton<ArenaView>.Instance.OpenView();
                if (<>f__am$cache2 == null)
                {
                    <>f__am$cache2 = () => Singleton<ArenaRevengeConfirmView>.Instance.OpenView();
                }
                vp_Timer.In(0.5f, <>f__am$cache2, null);
            }
            else
            {
                Singleton<ArenaRevengeConfirmView>.Instance.OpenView();
            }
        }

        private void RefreshCDTime()
        {
            Singleton<ArenaSelfRankInfoView>.Instance.RefreshCDTimeOnly();
        }

        private void RefreshLog()
        {
            Singleton<ArenaLogView>.Instance.RefreshLogList();
        }

        private void RefreshMyInfo()
        {
            Singleton<ArenaSelfRankInfoView>.Instance.RefreshMyInfo();
        }

        private void RefreshRankList()
        {
            Singleton<ArenaRankInfoView>.Instance.RefreshRankList();
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<ArenaMode>.Instance.dataUpdated += delegate (object sender, int code) {
                switch (code)
                {
                    case 1:
                        this.RefreshRankList();
                        break;

                    case 2:
                    case 5:
                        this.RefreshMyInfo();
                        break;

                    case 3:
                        this.RefreshLog();
                        break;

                    case 4:
                        this.RefreshCDTime();
                        break;
                }
            };
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Arena/ArenaView.assetbundle";
            }
        }
    }
}

