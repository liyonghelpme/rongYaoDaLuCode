﻿namespace com.game.module.Arena
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class Chanllenger
    {
        public bool canChallenge;
        public uint fightingPoints;
        public IList<PGeneralIconInfo> generalList;
        public string name;
        public uint ranking;
        public ulong roleId;

        public Chanllenger()
        {
            this.generalList = new List<PGeneralIconInfo>();
        }

        public Chanllenger(ulong id, string name, byte canChallenge, uint fightEffect, uint pos, IEnumerable<PGeneralIconInfo> generalId)
        {
            this.generalList = new List<PGeneralIconInfo>();
            this.roleId = id;
            this.name = name;
            this.fightingPoints = fightEffect;
            this.ranking = pos;
            this.canChallenge = Convert.ToBoolean(canChallenge);
            IEnumerator<PGeneralIconInfo> enumerator = generalId.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PGeneralIconInfo current = enumerator.Current;
                    this.generalList.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }
    }
}

