﻿namespace com.game.module.Arena
{
    using System;

    public class LogItem
    {
        public string name;
        public uint newRank;
        public ulong roleId;
        public int timestamp;
        public byte type;

        public LogItem()
        {
        }

        public LogItem(ulong roleId, string name, byte type, uint rank, int time)
        {
            this.roleId = roleId;
            this.newRank = rank;
            this.timestamp = time;
            this.name = name;
            this.type = type;
        }
    }
}

