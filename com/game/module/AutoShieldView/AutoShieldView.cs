﻿namespace com.game.module.AutoShieldView
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AutoShieldView : BaseView<com.game.module.AutoShieldView.AutoShieldView>
    {
        [CompilerGenerated]
        private static Comparison<HitGo> <>f__am$cache2;
        private Button btn_shield;
        private List<HitGo> hitGoList = new List<HitGo>();

        protected override void Init()
        {
            this.btn_shield = base.FindInChild<Button>("shield");
            this.btn_shield.onPress = new UIWidgetContainer.BoolDelegate(this.ShieldOnPress);
            this.btn_shield.onClick = new UIWidgetContainer.VoidDelegate(this.ShieldOnClick);
            this.btn_shield.onDrag = new UIWidgetContainer.VectorDelegate(this.ShieldOnDrag);
        }

        private void ShieldOnClick(GameObject go1)
        {
            this.ShieldSendHelp("OnClick", null);
        }

        private void ShieldOnDrag(GameObject go1, Vector2 delta)
        {
            this.ShieldSendHelp("OnDrag", delta);
        }

        private void ShieldOnPress(GameObject go, bool state)
        {
            if (state)
            {
                this.ShieldSendHelp("OnClick", null);
            }
        }

        private void ShieldSendHelp(string fuctionName, object param = null)
        {
            this.CloseView();
            UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;
            RaycastHit[] hitArray = Physics.RaycastAll(UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos));
            this.hitGoList.Clear();
            if (hitArray.Length > 0)
            {
                for (int i = 0; i < hitArray.Length; i++)
                {
                    GameObject gameObject = hitArray[i].collider.gameObject;
                    UIWidget component = gameObject.GetComponent<UIWidget>();
                    HitGo item = new HitGo();
                    if (component != null)
                    {
                        if (component.isVisible && ((component.hitCheck == null) || component.hitCheck(hitArray[i].point)))
                        {
                            goto Label_00EE;
                        }
                        continue;
                    }
                    UIRect rect = NGUITools.FindInParents<UIRect>(gameObject);
                    if ((rect != null) && (rect.finalAlpha < 0.001f))
                    {
                        continue;
                    }
                Label_00EE:
                    item.depth = NGUITools.CalculateRaycastDepth(gameObject);
                    item.go = gameObject;
                    if (item.depth != 0x7fffffff)
                    {
                        this.hitGoList.Add(item);
                    }
                }
                if (<>f__am$cache2 == null)
                {
                    <>f__am$cache2 = (r1, r2) => r2.depth.CompareTo(r1.depth);
                }
                this.hitGoList.Sort(<>f__am$cache2);
                (AppMap.Instance.me.Controller as ActionControler).StopWalk();
                HitGo go2 = this.hitGoList[0];
                go2.go.SendMessage(fuctionName, param, SendMessageOptions.DontRequireReceiver);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/AutoShield/AutoShield.assetbundle";
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct HitGo
        {
            public int depth;
            public GameObject go;
        }
    }
}

