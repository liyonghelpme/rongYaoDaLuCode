﻿namespace com.game.module.Baoxiang
{
    using com.game;
    using com.game.module.core;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    internal class BaoxiangControl : BaseControl<BaoxiangControl>
    {
        private void BuyBoxRs(INetData data)
        {
            TreasureBuyMsg_37_3 g__ = new TreasureBuyMsg_37_3();
            g__.read(data.GetMemoryStream());
            Singleton<BaoxiangSelectView>.Instance.OpenBuyView(g__);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("9474", new NetMsgCallback(this.SelectViewInfo));
            AppNet.main.addCMD("9475", new NetMsgCallback(this.BuyBoxRs));
        }

        private void SelectViewInfo(INetData data)
        {
            TreasureOpenPanelMsg_37_2 g__ = new TreasureOpenPanelMsg_37_2();
            g__.read(data.GetMemoryStream());
            Singleton<BaoxiangSelectView>.Instance.InitViewInfoFromServer(g__);
        }
    }
}

