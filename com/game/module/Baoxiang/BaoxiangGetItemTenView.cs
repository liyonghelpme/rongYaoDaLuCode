﻿namespace com.game.module.Baoxiang
{
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class BaoxiangGetItemTenView : BaseView<BaoxiangGetItemTenView>
    {
        private UISprite costIcon;
        private UILabel costPrice;
        private Button getNextBtn;
        private UILabel[] itemList;
        private Button okBtn;

        protected override void HandleAfterOpenView()
        {
            this.InitAllItems(Singleton<BaoxiangSelectView>.Instance.preTreasureList);
        }

        protected override void Init()
        {
            base.Init();
            this.itemList = new UILabel[] { base.FindInChild<UILabel>("item_1/item_lb"), base.FindInChild<UILabel>("item_2/item_lb"), base.FindInChild<UILabel>("item_3/item_lb"), base.FindInChild<UILabel>("item_4/item_lb"), base.FindInChild<UILabel>("item_5/item_lb"), base.FindInChild<UILabel>("item_6/item_lb"), base.FindInChild<UILabel>("item_7/item_lb"), base.FindInChild<UILabel>("item_8/item_lb"), base.FindInChild<UILabel>("item_9/item_lb"), base.FindInChild<UILabel>("item_10/item_lb") };
            this.getNextBtn = base.FindInChild<Button>("get_next_btn");
            this.okBtn = base.FindInChild<Button>("ok_btn");
            this.okBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandle);
            this.getNextBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandle);
            this.costPrice = base.FindInChild<UILabel>("price/btn_lb");
            this.costIcon = base.FindInChild<UISprite>("price/icon");
        }

        private void InitAllItems(List<PTreasure> treasureList)
        {
            Debug.Log("Init all items");
            if ((treasureList == null) || (treasureList.Count < 10))
            {
                Debug.Log("treaureList is null");
            }
            else
            {
                PTreasure[] treasureArray = treasureList.ToArray();
                for (int i = 0; i < 10; i++)
                {
                    this.itemList[i].text = treasureArray[i].id.ToString();
                }
                if (Singleton<BaoxiangSelectView>.Instance.preBuyType != 2)
                {
                    this.costPrice.text = Singleton<BaoxiangSelectView>.Instance.data.goldPrices.ToString();
                    this.costIcon.spriteName = "jinbi";
                }
                else
                {
                    this.costPrice.text = Singleton<BaoxiangSelectView>.Instance.data.diamondPrices.ToString();
                    this.costIcon.spriteName = "zuanshi";
                }
            }
        }

        private void OnClickHandle(GameObject go)
        {
            if (go == this.getNextBtn.gameObject)
            {
                Singleton<BaoxiangSelectView>.Instance.BuyAgain();
            }
            else if (go == this.okBtn.gameObject)
            {
                Singleton<BaoxiangSystem>.Instance.currentPanel = 0;
                Singleton<BaoxiangSystem>.Instance.OpenView();
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

