﻿namespace com.game.module.Baoxiang
{
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Linq;
    using UnityEngine;

    public class BaoxiangGetItemView : BaseView<BaoxiangGetItemView>
    {
        private UISprite costIcon;
        private UILabel costPrice;
        private Button getNextBuy;
        private UILabel itemLb;
        private Button okBtn;

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            PTreasure treasure = Singleton<BaoxiangSelectView>.Instance.preTreasureList.First<PTreasure>();
            if (treasure != null)
            {
                this.itemLb.text = "ID: " + treasure.id.ToString();
                if (Singleton<BaoxiangSelectView>.Instance.preBuyType != 2)
                {
                    this.costPrice.text = Singleton<BaoxiangSelectView>.Instance.data.goldPrice.ToString();
                    this.costIcon.spriteName = "jinbi";
                }
                else
                {
                    this.costPrice.text = Singleton<BaoxiangSelectView>.Instance.data.diamondPrice.ToString();
                    this.costIcon.spriteName = "zuanshi";
                }
            }
        }

        protected override void Init()
        {
            base.Init();
            this.itemLb = base.FindInChild<UILabel>("item/item_lb");
            this.getNextBuy = base.FindInChild<Button>("get_next_btn");
            this.okBtn = base.FindInChild<Button>("ok_btn");
            this.getNextBuy.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandle);
            this.okBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandle);
            this.costPrice = base.FindInChild<UILabel>("price/btn_lb");
            this.costIcon = base.FindInChild<UISprite>("price/icon");
        }

        private void OnClickHandle(GameObject go)
        {
            if (go == this.getNextBuy.gameObject)
            {
                Singleton<BaoxiangSelectView>.Instance.BuyAgain();
            }
            else if (go == this.okBtn.gameObject)
            {
                Singleton<BaoxiangSystem>.Instance.currentPanel = 0;
                Singleton<BaoxiangSystem>.Instance.OpenView();
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public Button OkBtn
        {
            get
            {
                return this.okBtn;
            }
        }
    }
}

