﻿namespace com.game.module.Baoxiang
{
    using com.game;
    using Proto;
    using System;
    using System.IO;

    internal class BaoxiangMode
    {
        public const int BAOXIANG_MODE_GET_ITEM = 1;
        public const int BAOXIANG_MODE_GET_ITEM_10 = 2;
        public const int BAOXIANG_MODE_SELECT = 0;
        public const byte BUY_NUM_1 = 1;
        public const byte BUY_NUM_10 = 10;
        public const short BUY_RES_ERR_NO_MONEY = 1;
        public const short BUY_RES_SUCC = 0;
        public const byte BUY_TYPE_GOLD = 2;
        public const byte BUY_TYPE_SELVER = 1;

        public void BuyBox(byte buyType, byte buyNums)
        {
            MemoryStream msdata = new MemoryStream();
            Module_37.write_37_3(msdata, buyType, buyNums);
            AppNet.gameNet.send(msdata, 0x25, 3);
        }

        public void GetLeftBaoxiangTimes()
        {
            MemoryStream msdata = new MemoryStream();
            Module_37.write_37_2(msdata);
            AppNet.gameNet.send(msdata, 0x25, 2);
        }
    }
}

