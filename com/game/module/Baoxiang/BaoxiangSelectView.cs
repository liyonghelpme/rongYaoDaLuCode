﻿namespace com.game.module.Baoxiang
{
    using com.game.module.core;
    using com.game.Public.Message;
    using Proto;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class BaoxiangSelectView : BaseView<BaoxiangSelectView>
    {
        private Button byBvBuy10Btn;
        private Button byBvBuy1Btn;
        private UILabel byBvCost10Lb;
        private UILabel byBvCost1Lb;
        private UILabel byBvLeftTimeLb;
        private UISprite byBvShouchouSp;
        private Button byBvUpBtn;
        public float byFreeCd;
        private Button byFvBoxBtn;
        private UILabel byFvCostLb;
        private UILabel byFvLeftTimesLb;
        private GameObject byFvMianfeiPl;
        private UILabel byFvPriceLb;
        private GameObject byFvPricePl;
        private bool byIsFree;
        public TreasureOpenPanelMsg_37_2 data;
        private UILabel gjFvPriceLb;
        private bool gjIsFree;
        private Button hjBoxBtn;
        private Button hjBvBuy10Btn;
        private Button hjBvBuy1Btn;
        private UILabel hjBvConst10Lb;
        private UILabel hjBvCost1Lb;
        private UILabel hjBvLeftTimeLb;
        private UISprite hjBvShouchouSp;
        private Button hjBvUpBtn;
        private UILabel hjCostLb;
        public float hjFreeCd;
        private UILabel hjFvLeftTimesLb;
        private GameObject hjFvMianfeiPl;
        private GameObject hjFvPricePl;
        private GameObject item1Pl;
        private bool item1Show;
        private GameObject item2Pl;
        private bool item2Show;
        private GameObject itemGm1Pl;
        private GameObject itemGm2Pl;
        private byte preBuyNums = 1;
        public byte preBuyType = 1;
        public List<PTreasure> preTreasureList;

        public void BuyAgain()
        {
            this.DoBuyBox(this.preBuyType, this.preBuyNums);
        }

        private void DoBuyBox(byte buyType, byte buyNums)
        {
            Singleton<BaoxiangMode>.Instance.BuyBox(buyType, buyNums);
            this.preBuyType = buyType;
            this.preBuyNums = buyNums;
        }

        protected override void HandleAfterOpenView()
        {
            this.item1Pl.SetActive(!this.item1Show);
            this.itemGm1Pl.SetActive(this.item1Show);
            this.item2Pl.SetActive(!this.item2Show);
            this.itemGm2Pl.SetActive(this.item2Show);
            Singleton<BaoxiangMode>.Instance.GetLeftBaoxiangTimes();
        }

        protected override void Init()
        {
            base.Init();
            this.InitAllButtons();
            this.InitAllLabels();
        }

        private void InitAllButtons()
        {
            this.item1Pl = base.FindChild("item_1");
            this.item2Pl = base.FindChild("item_2");
            this.itemGm1Pl = base.FindChild("item_gm_1");
            this.itemGm2Pl = base.FindChild("item_gm_2");
            this.byFvMianfeiPl = base.FindChild("item_1/mianfei");
            this.byFvPricePl = base.FindChild("item_1/price");
            this.hjFvMianfeiPl = base.FindChild("item_2/mianfei");
            this.hjFvPricePl = base.FindChild("item_2/price");
            this.byFvBoxBtn = base.FindInChild<Button>("item_1/chakan");
            this.hjBoxBtn = base.FindInChild<Button>("item_2/chakan");
            this.byFvBoxBtn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.hjBoxBtn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.byBvUpBtn = base.FindInChild<Button>("item_gm_1/shangla");
            this.byBvBuy1Btn = base.FindInChild<Button>("item_gm_1/cost_1_btn");
            this.byBvBuy10Btn = base.FindInChild<Button>("item_gm_1/cost_10_btn");
            this.byBvBuy1Btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.byBvBuy10Btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.byBvUpBtn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.hjBvUpBtn = base.FindInChild<Button>("item_gm_2/shangla");
            this.hjBvBuy1Btn = base.FindInChild<Button>("item_gm_2/cost_1_btn");
            this.hjBvBuy10Btn = base.FindInChild<Button>("item_gm_2/cost_10_btn");
            this.hjBvBuy1Btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.hjBvBuy10Btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.hjBvUpBtn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void InitAllLabels()
        {
            this.hjBvLeftTimeLb = base.FindInChild<UILabel>("item_gm_2/buy_msg_1/buy_msg_lb");
            this.byBvLeftTimeLb = base.FindInChild<UILabel>("item_gm_1/buy_msg_1/buy_msg_lb");
            this.byFvPriceLb = base.FindInChild<UILabel>("item_1/price/price");
            this.gjFvPriceLb = base.FindInChild<UILabel>("item_2/price/price");
            this.byFvCostLb = base.FindInChild<UILabel>("item_1/mianfei/mianfeiwenzi");
            this.byFvLeftTimesLb = base.FindInChild<UILabel>("item_1/mianfei_lb/mianfeicishu");
            this.hjCostLb = base.FindInChild<UILabel>("item_2/mianfei/mianfeiwenzi");
            this.hjFvLeftTimesLb = base.FindInChild<UILabel>("item_2/mianfei_lb/mianfeicishu");
            this.byBvCost1Lb = base.FindInChild<UILabel>("item_gm_1/cost_1/Label");
            this.byBvCost10Lb = base.FindInChild<UILabel>("item_gm_1/cost_10/Label");
            this.hjBvCost1Lb = base.FindInChild<UILabel>("item_gm_2/cost_1/Label");
            this.hjBvConst10Lb = base.FindInChild<UILabel>("item_gm_2/cost_10/Label");
            this.byBvShouchouSp = base.FindInChild<UISprite>("item_1/biaoqian/shouchou");
            this.hjBvShouchouSp = base.FindInChild<UISprite>("item_2/biaoqian/shouchou");
        }

        public void InitViewInfoFromServer(TreasureOpenPanelMsg_37_2 data)
        {
            if (data != null)
            {
                this.byBvCost1Lb.text = data.goldPrice.ToString();
                this.byBvCost10Lb.text = data.goldPrices.ToString();
                this.hjBvCost1Lb.text = data.diamondPrice.ToString();
                this.hjBvConst10Lb.text = data.diamondPrices.ToString();
                if (data.goldFirstFlag == 1)
                {
                    this.byBvShouchouSp.spriteName = "lansewupin";
                }
                else
                {
                    this.byBvShouchouSp.spriteName = "shilianchou";
                }
                if (data.diamondFirstFlag == 1)
                {
                    this.hjBvShouchouSp.spriteName = "zisewupin";
                }
                else
                {
                    this.hjBvShouchouSp.spriteName = "shilianchouzise";
                }
                this.data = data;
                this.byFreeCd = data.goldFreeCd;
                this.hjFreeCd = data.diamondFreeCd;
            }
        }

        private void onClickHandler(GameObject go)
        {
            if (go == this.byFvBoxBtn.gameObject)
            {
                this.item1Show = true;
                this.HandleAfterOpenView();
            }
            else if (go == this.hjBoxBtn.gameObject)
            {
                this.item2Show = true;
                this.HandleAfterOpenView();
            }
            else if (go == this.byBvUpBtn.gameObject)
            {
                this.item1Show = false;
                this.HandleAfterOpenView();
            }
            else if (go == this.hjBvUpBtn.gameObject)
            {
                this.item2Show = false;
                this.HandleAfterOpenView();
            }
            else if (go == this.byBvBuy1Btn.gameObject)
            {
                this.DoBuyBox(1, 1);
            }
            else if (go == this.byBvBuy10Btn.gameObject)
            {
                this.DoBuyBox(1, 10);
            }
            else if (go == this.hjBvBuy1Btn.gameObject)
            {
                this.DoBuyBox(2, 1);
            }
            else if (go == this.hjBvBuy10Btn.gameObject)
            {
                this.DoBuyBox(2, 10);
            }
        }

        public void OpenBuyView(TreasureBuyMsg_37_3 data)
        {
            if (data.code == 0)
            {
                this.preTreasureList = data.treasures;
                if (data.treasures.Count == 1)
                {
                    Singleton<BaoxiangSystem>.Instance.currentPanel = 1;
                    Singleton<BaoxiangSystem>.Instance.OpenView();
                }
                else if (data.treasures.Count == 10)
                {
                    Singleton<BaoxiangSystem>.Instance.currentPanel = 2;
                    Singleton<BaoxiangSystem>.Instance.OpenView();
                }
                Singleton<BaoxiangSystem>.Instance.SetAllBtnState(false);
            }
            else
            {
                ErrorCodeManager.ShowError(data.code);
                Singleton<BaoxiangSystem>.Instance.currentPanel = 0;
                Singleton<BaoxiangSystem>.Instance.OpenView();
            }
        }

        public void ResetInitView()
        {
            this.item1Show = false;
            this.item2Show = false;
        }

        public override void Update()
        {
            if (this.data != null)
            {
                if (this.byFreeCd <= 0f)
                {
                    if (this.data.goldFreeTimes == 0)
                    {
                        this.byBvLeftTimeLb.text = "本次免费次数已用完";
                        this.byFvLeftTimesLb.text = "本次的免费次数已用完";
                        this.byBvCost1Lb.text = this.data.goldPrice.ToString();
                        this.byFvCostLb.text = this.data.diamondPrice.ToString();
                    }
                    else
                    {
                        this.byBvLeftTimeLb.text = "免费次数: " + this.data.goldFreeTimes + "/5";
                        this.byFvLeftTimesLb.text = "免费次数: " + this.data.goldFreeTimes + "/5";
                        this.byBvCost1Lb.text = "免费";
                        this.byFvCostLb.text = "免费";
                    }
                    this.byFvMianfeiPl.SetActive(true);
                    this.byFvPricePl.SetActive(false);
                }
                else
                {
                    this.byFvMianfeiPl.SetActive(false);
                    this.byFvPricePl.SetActive(true);
                    this.byFreeCd -= Time.deltaTime;
                    this.byFvPriceLb.text = this.data.goldPrice.ToString();
                    if (this.data.goldFreeTimes == 0)
                    {
                        this.byBvLeftTimeLb.text = "本次免费次数已用完";
                        this.byFvLeftTimesLb.text = "本次的免费次数已用完";
                    }
                    else
                    {
                        this.byBvLeftTimeLb.text = this.WorkTimeToString((uint) this.byFreeCd) + " 后免费";
                        this.byFvLeftTimesLb.text = this.WorkTimeToString((uint) this.byFreeCd) + " 后免费";
                    }
                    this.byBvCost1Lb.text = this.data.goldPrice.ToString();
                }
                if (this.hjFreeCd <= 0f)
                {
                    this.hjFvMianfeiPl.SetActive(true);
                    this.hjFvPricePl.SetActive(false);
                    this.hjBvLeftTimeLb.text = string.Empty;
                    this.hjBvCost1Lb.text = "免费";
                    this.hjCostLb.text = "免费";
                    this.hjFvLeftTimesLb.text = "免费次数: " + this.data.diamondFreeTimes + "/1";
                }
                else
                {
                    this.gjFvPriceLb.text = this.data.diamondPrice.ToString();
                    this.hjFvMianfeiPl.SetActive(false);
                    this.hjFvPricePl.SetActive(true);
                    this.hjFreeCd -= Time.deltaTime;
                    this.hjBvLeftTimeLb.text = this.WorkTimeToString((uint) this.hjFreeCd) + " 后免费";
                    this.hjFvLeftTimesLb.text = this.WorkTimeToString((uint) this.hjFreeCd) + " 后免费";
                    this.hjBvCost1Lb.text = this.data.diamondPrice.ToString();
                }
            }
        }

        private string WorkNumToString(uint n)
        {
            if (n < 10)
            {
                return ("0" + n.ToString());
            }
            return n.ToString();
        }

        private string WorkTimeToString(uint sec)
        {
            uint n = (sec / 60) / 60;
            uint num2 = (sec % 0xe10) / 60;
            uint num3 = sec % 60;
            string str = string.Empty;
            if (n != 0)
            {
                str = this.WorkNumToString(n) + ":";
            }
            return (str + this.WorkNumToString(num2) + ":" + this.WorkNumToString(num3));
        }

        public Button HjBoxBtn
        {
            get
            {
                return this.hjBoxBtn;
            }
        }

        public Button HjBvBuy1Btn
        {
            get
            {
                return this.hjBvBuy1Btn;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

