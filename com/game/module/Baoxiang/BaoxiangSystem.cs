﻿namespace com.game.module.Baoxiang
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class BaoxiangSystem : BaseView<BaoxiangSystem>
    {
        private MainToolView _mainToolBarView;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map9;
        private Button closeBtn;
        public CloseViewGuideDelegate closeViewGuideDelegate;
        public int currentPanel;

        protected override void HandleAfterOpenView()
        {
            this.HandleBeforeCloseView();
            this._mainToolBarView.isActive = true;
            base.HandleAfterOpenView();
            switch (this.currentPanel)
            {
                case 0:
                    Singleton<BaoxiangSelectView>.Instance.OpenView();
                    break;

                case 1:
                    Singleton<BaoxiangGetItemView>.Instance.OpenView();
                    break;

                case 2:
                    Singleton<BaoxiangGetItemTenView>.Instance.OpenView();
                    break;

                default:
                    Singleton<BaoxiangSelectView>.Instance.OpenView();
                    break;
            }
            this.SetAllBtnState(true);
        }

        private void HandleBeforeCloseView()
        {
            this._mainToolBarView.isActive = false;
            Singleton<BaoxiangSelectView>.Instance.CloseView();
            Singleton<BaoxiangGetItemView>.Instance.CloseView();
            Singleton<BaoxiangGetItemTenView>.Instance.CloseView();
        }

        protected override void Init()
        {
            base.Init();
            this.InitAllViews();
            this.InitMainComponent();
            this.InitOnClickHandle();
        }

        private void InitAllViews()
        {
            this._mainToolBarView = new MainToolView(base.FindChild("toolbar"));
            Singleton<BaoxiangSelectView>.Instance.gameObject = base.FindChild("select_view");
            Singleton<BaoxiangGetItemView>.Instance.gameObject = base.FindChild("getbaoxiang");
            Singleton<BaoxiangGetItemTenView>.Instance.gameObject = base.FindChild("getbaoxiang_10_times");
        }

        private void InitMainComponent()
        {
            this.closeBtn = base.FindInChild<Button>("close_btn");
        }

        private void InitOnClickHandle()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandle);
        }

        private void OnClickHandle(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map9 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(1);
                    dictionary.Add("close_btn", 0);
                    <>f__switch$map9 = dictionary;
                }
                if (<>f__switch$map9.TryGetValue(name, out num) && (num == 0))
                {
                    Singleton<BaoxiangSelectView>.Instance.ResetInitView();
                    this.CloseView();
                    if (this.closeViewGuideDelegate != null)
                    {
                        this.closeViewGuideDelegate();
                        this.closeViewGuideDelegate = null;
                    }
                    return;
                }
            }
            Debug.Log("default");
        }

        private void onClickHandlerMoneys(GameObject go)
        {
            this.CloseView();
            Singleton<BuyVigourView>.Instance.Show();
        }

        public void SetAllBtnState(bool flag)
        {
            this.closeBtn.SetActive(flag);
        }

        public Button CloseBtn
        {
            get
            {
                return this.closeBtn;
            }
        }

        public override bool IsFullUI
        {
            get
            {
                return true;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Baoxiang/BaoXiang.assetbundle";
            }
        }
    }
}

