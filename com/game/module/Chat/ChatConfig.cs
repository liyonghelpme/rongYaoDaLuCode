﻿namespace com.game.module.Chat
{
    using System;
    using UnityEngine;

    public class ChatConfig
    {
        public const float DOWN_BELOW = 22f;
        public const int FONT_SIZE = 0x16;
        public Color GREEN = new Color(0.2901961f, 0.8470588f, 0.2313726f, 1f);
        public const float LEFT_BELOW = 44f;
        public const int MSG_NUM_MAX = 30;
        public Color PURPLE = new Color(0.2313726f, 0.5607843f, 0.9294118f, 1f);
        public const float RAW_DELTA = 9f;
        public const float RIGHT_BELOW = 44f;
        public const float UP_BELOW = 22f;
        public Color WHITE = new Color(1f, 1f, 1f, 1f);
        public Color YELLOW = new Color(1f, 0.8941177f, 0.003921569f, 1f);
    }
}

