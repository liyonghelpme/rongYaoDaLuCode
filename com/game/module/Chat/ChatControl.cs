﻿namespace com.game.module.Chat
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.LocalVar;
    using com.game.Public.Message;
    using com.game.vo;
    using com.net;
    using com.net.interfaces;
    using com.u3d.bases.debug;
    using PCustomDataType;
    using Proto;
    using System;

    public class ChatControl : BaseControl<ChatControl>
    {
        private void Fun_10_1(INetData data)
        {
            ChatReqMsg_10_1 g__ = new ChatReqMsg_10_1();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void Fun_10_2(INetData data)
        {
            ChatPrivateChatMsg_10_2 g__ = new ChatPrivateChatMsg_10_2();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                ChatVo addMsg = new ChatVo {
                    chatType = 7,
                    senderId = MeVo.instance.Id,
                    serverId = (ushort) MeVo.instance.serverId,
                    senderName = MeVo.instance.Name,
                    senderSex = MeVo.instance.sex,
                    senderJob = MeVo.instance.job,
                    senderLvl = (byte) MeVo.instance.Level,
                    senderVip = (byte) Singleton<VIPMode>.Instance.vip,
                    content = Singleton<ChatView>.Instance.sendMessage.content,
                    nationId = MeVo.instance.nation
                };
                Singleton<ChatMode>.Instance.AddChatMsg(addMsg);
                Singleton<ChatMode>.Instance.UpdateMainChatContent(addMsg.senderName, addMsg.chatType, addMsg.content);
                Singleton<ChatView>.Instance.msgInput.value = string.Empty;
            }
        }

        private void Fun_10_3(INetData data)
        {
            ChatContentPushMsg_10_3 g__ = new ChatContentPushMsg_10_3();
            g__.read(data.GetMemoryStream());
            if (this.IsValidMsg(g__.chatType) && !this.IsBlackListMan(g__.senderName))
            {
                Log.info(this, "判断是否接收该频道消息");
                byte chatType = g__.chatType;
                switch (chatType)
                {
                    case 1:
                        if (LocalVarManager.GetInt("Chat_RecieveZongHeChannel", 0) != 2)
                        {
                            break;
                        }
                        return;

                    case 3:
                        if (LocalVarManager.GetInt("Chat_RecieveZhenYingChannel", 0) != 2)
                        {
                            break;
                        }
                        return;

                    default:
                        if ((chatType == 7) && (LocalVarManager.GetInt("Chat_RecieveSiLiaoChannel", 0) == 2))
                        {
                            return;
                        }
                        break;
                }
                ChatVo recChat = new ChatVo {
                    chatType = g__.chatType,
                    senderId = g__.senderId,
                    serverId = g__.serverId,
                    senderName = g__.senderName,
                    senderSex = g__.senderSex,
                    senderJob = g__.senderJob,
                    senderLvl = g__.senderLvl,
                    senderVip = g__.senderVip,
                    content = g__.content,
                    goods = (g__.goodsList.Count <= 0) ? null : g__.goodsList[0],
                    nationId = g__.senderNation
                };
                this.SendChatMsg(recChat);
            }
        }

        private void Fun_10_6(INetData data)
        {
            ChatSysAnounceMsg_10_6 g__ = new ChatSysAnounceMsg_10_6();
            g__.read(data.GetMemoryStream());
            string content = g__.content;
            Singleton<ChatMode>.Instance.Notice = content;
            this.SendNoticeMsg(content);
        }

        private void Fun_10_7(INetData data)
        {
            ChatRumorMsg_10_7 rumorMsg = new ChatRumorMsg_10_7();
            rumorMsg.read(data.GetMemoryStream());
            string rumorText = this.GetRumorText(rumorMsg);
            Singleton<ChatMode>.Instance.Rumor = rumorText;
            this.SendNoticeMsg(rumorText);
        }

        private string GetRumorText(ChatRumorMsg_10_7 rumorMsg)
        {
            string str = string.Empty;
            string str2 = "$role$";
            string str3 = "$goods$";
            string str4 = "$mon$";
            string str5 = "$int$";
            string str6 = "$string$";
            SysRumor sysRumor = BaseDataMgr.instance.GetSysRumor(rumorMsg.typeId);
            if (sysRumor != null)
            {
                PRumor rumor2 = new PRumor();
                if (rumorMsg.content.Count > 0)
                {
                    rumor2 = rumorMsg.content[0];
                }
                string format = sysRumor.format;
                int startIndex = 0;
                int num2 = 0;
                int num3 = 0;
                int num4 = 0;
                int num5 = 0;
                int num6 = 0;
                while (startIndex < format.Length)
                {
                    if (format[startIndex] != '$')
                    {
                        str = str + format[startIndex];
                        startIndex++;
                    }
                    else
                    {
                        if (str2 == format.Substring(startIndex, str2.Length))
                        {
                            str = str + "[00ff00]" + rumor2.role[num2].name + "[-]";
                            num2++;
                            startIndex += str2.Length;
                            continue;
                        }
                        if (str3 == format.Substring(startIndex, str3.Length))
                        {
                            uint templateId = rumor2.goods[num3].templateId;
                            SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(templateId);
                            str = str + "[0000ff]" + dataById.name + "[-]";
                            num3++;
                            startIndex += str3.Length;
                            continue;
                        }
                        if (str4 == format.Substring(startIndex, str4.Length))
                        {
                            ulong monId = rumor2.mon[num4].monId;
                            SysMonsterVo vo2 = BaseDataMgr.instance.GetDataById<SysMonsterVo>((uint) monId);
                            str = str + "[ff0000]" + vo2.name + "[-]";
                            num4++;
                            startIndex += str4.Length;
                            continue;
                        }
                        if (str5 == format.Substring(startIndex, str5.Length))
                        {
                            string str8 = str;
                            object[] objArray1 = new object[] { str8, "[ffff00]", rumor2.iData[num5], "[-]" };
                            str = string.Concat(objArray1);
                            num5++;
                            startIndex += str5.Length;
                            continue;
                        }
                        if (str6 == format.Substring(startIndex, str6.Length))
                        {
                            str = str + "[00ffff]" + rumor2.sData[num6] + "[-]";
                            num6++;
                            startIndex += str6.Length;
                            continue;
                        }
                        str = str + format[startIndex];
                        startIndex++;
                    }
                }
            }
            return str;
        }

        private bool IsBlackListMan(string name)
        {
            if (Singleton<FriendMode>.Instance.blacksList != null)
            {
                foreach (PRelationInfo info in Singleton<FriendMode>.Instance.blacksList)
                {
                    if (name.Equals(info.name))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsValidMsg(byte msgType)
        {
            bool flag = msgType == 1;
            bool flag2 = msgType == 3;
            bool flag3 = msgType == 7;
            return ((flag || flag2) || flag3);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("2561", new NetMsgCallback(this.Fun_10_1));
            AppNet.main.addCMD("2562", new NetMsgCallback(this.Fun_10_2));
            AppNet.main.addCMD("2563", new NetMsgCallback(this.Fun_10_3));
            AppNet.main.addCMD("2566", new NetMsgCallback(this.Fun_10_6));
            AppNet.main.addCMD("2567", new NetMsgCallback(this.Fun_10_7));
        }

        public void OpenChatUI()
        {
            Singleton<ChatView>.Instance.OpenView();
        }

        private void SendChatMsg(ChatVo recChat)
        {
            if (recChat.senderName != MeVo.instance.Name)
            {
            }
            if (Singleton<ChatMode>.Instance.nameToIdDic.ContainsKey(recChat.senderName))
            {
                Singleton<ChatMode>.Instance.nameToIdDic[recChat.senderName] = recChat.senderId;
            }
            else
            {
                Singleton<ChatMode>.Instance.nameToIdDic.Add(recChat.senderName, recChat.senderId);
            }
            Singleton<ChatMode>.Instance.AddChatMsg(recChat);
            Singleton<ChatMode>.Instance.UpdateMainChatContent(recChat.senderName, recChat.chatType, recChat.content);
        }

        private void SendNoticeMsg(string content)
        {
            ChatVo recChat = new ChatVo {
                chatType = 1,
                senderName = LanguageManager.GetWord("ChatView.SysNotice"),
                senderId = 1L,
                content = content
            };
            this.SendChatMsg(recChat);
        }
    }
}

