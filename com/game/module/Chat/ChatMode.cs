﻿namespace com.game.module.Chat
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.u3d.bases.debug;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine;

    public class ChatMode : BaseMode<ChatMode>
    {
        private string[] _mainChatContent = new string[2];
        private List<ChatVo> _recMsgList = new List<ChatVo>();
        public Dictionary<string, bool> emoDic = new Dictionary<string, bool>();
        public string[] FilterString;
        public List<PChatGoods> goods = new List<PChatGoods>();
        public Color GREEN = new Color(0.2901961f, 0.8470588f, 0.2313726f, 1f);
        public Dictionary<string, ulong> nameToIdDic = new Dictionary<string, ulong>();
        private string notice;
        public Color PURPLE = new Color(0.2313726f, 0.5607843f, 0.9294118f, 1f);
        private string rumor;
        public readonly int UPDATE_CHAT_MSG = 2;
        public readonly int UPDATE_DOWNARROW = 6;
        public readonly int UPDATE_MAIN_CHAT_CONTENT = 1;
        public readonly int UPDATE_NOTICE = 3;
        public readonly int UPDATE_RUMOR = 4;
        public readonly int UPDATE_UPARROW = 5;
        public Color WHITE = new Color(1f, 1f, 1f, 1f);
        public Color YELLOW = new Color(1f, 0.8941177f, 0.003921569f, 1f);

        public void AddChatMsg(ChatVo addMsg)
        {
            Log.info(this, "将收到的消息添加到消息列表中，最多保留三十条");
            if (this._recMsgList.Count < 30)
            {
                this._recMsgList.Add(addMsg);
            }
            else
            {
                this._recMsgList.Remove(this._recMsgList[0]);
                this._recMsgList.Add(addMsg);
            }
            base.DataUpdate(this.UPDATE_CHAT_MSG);
        }

        public void SendPrivateMsg(SendMesVo sendMes)
        {
            Log.info(this, "发送10-2私聊请求给服务器");
            MemoryStream msdata = new MemoryStream();
            if (sendMes.privateChatName != null)
            {
                Module_10.write_10_2(msdata, sendMes.privateChatRoleId, sendMes.content, this.goods);
                AppNet.gameNet.send(msdata, 10, 2);
            }
            else
            {
                MessageManager.Show("未指定私聊对象");
            }
        }

        public void SendPublicMsg(SendMesVo sendMes)
        {
            Log.info(this, "发送10-1（综合，阵营聊天请求）给服务器");
            MemoryStream msdata = new MemoryStream();
            Module_10.write_10_1(msdata, sendMes.sendChatType, sendMes.content, this.goods);
            AppNet.gameNet.send(msdata, 10, 1);
        }

        public void UpdateMainChatContent(string name, byte chatType, string content)
        {
            this._mainChatContent[0] = this._mainChatContent[0];
            string str = string.Empty;
            byte num = chatType;
            switch (num)
            {
                case 1:
                    str = str + "[ffffff]";
                    break;

                case 3:
                    str = str + "[4ad836]";
                    break;

                default:
                    if (num == 7)
                    {
                        str = str + "[3b8fed]";
                    }
                    else
                    {
                        str = str + "[ffffff]";
                    }
                    break;
            }
            str = (str + name + "[-]：") + content;
            this._mainChatContent[0] = str;
            base.DataUpdate(this.UPDATE_MAIN_CHAT_CONTENT);
        }

        public string[] mainChatContent
        {
            get
            {
                return this._mainChatContent;
            }
        }

        public string Notice
        {
            get
            {
                return this.notice;
            }
            set
            {
                this.notice = value;
                if (string.Empty != value)
                {
                    base.DataUpdate(this.UPDATE_NOTICE);
                }
            }
        }

        public List<ChatVo> recChatVoList
        {
            get
            {
                return this._recMsgList;
            }
        }

        public string Rumor
        {
            get
            {
                return this.rumor;
            }
            set
            {
                this.rumor = value;
                if (string.Empty != value)
                {
                    base.DataUpdate(this.UPDATE_RUMOR);
                }
            }
        }
    }
}

