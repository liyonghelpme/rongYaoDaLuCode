﻿namespace com.game.module.Chat
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class ChatTestView : BaseView<ChatTestView>
    {
        public UILabel chatContent;
        public UIInput msgInput;
        private SendMesVo sendMessage = new SendMesVo();

        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
        }

        protected override void HandleBeforeCloseView()
        {
        }

        protected override void Init()
        {
            this.msgInput = base.FindInChild<UIInput>("Input");
            this.chatContent = base.FindInChild<UILabel>("Input/Label");
            EventDelegate.Add(this.msgInput.onSubmit, new EventDelegate.Callback(this.SendMsg));
        }

        public override void RegisterUpdateHandler()
        {
        }

        private void SendMsg()
        {
            Debug.Log("chatContent:" + this.chatContent.text);
            this.sendMessage.sendChatType = 1;
            this.sendMessage.content = this.chatContent.text;
            Singleton<ChatMode>.Instance.SendPublicMsg(this.sendMessage);
            Singleton<ChatTestView>.Instance.CloseView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/ChatTest/ChatTestView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

