﻿namespace com.game.module.Chat
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.LocalVar;
    using com.game.vo;
    using com.u3d.bases.debug;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ChatView : BaseView<ChatView>
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$mapA;
        private Transform biaoqing_packge;
        private Button btn_biaoqing;
        private Button btn_close_chat;
        private Button btn_fs;
        private Button btn_fsChannel_0;
        private Button btn_fsChannel_1;
        private Button btn_fsChannel_2;
        private Button btn_gn_jhmd;
        private Button btn_gn_jwhy;
        private Button btn_gn_sl;
        private Button btn_gn_xxzl;
        private Button btn_jsChannel_set;
        private UIScrollView chat_scroll_view;
        public UILabel chatContent;
        private Button ckb_jsChannel_sl;
        private Button ckb_jsChannel_zh;
        private Button ckb_jsChannel_zy;
        private UIToggle ckb_rec_channel_siliao_set;
        private UIToggle ckb_rec_channel_zhenying_set;
        private UIToggle ckb_rec_channel_zonghe_set;
        private UIToggle ckb_siliao;
        private UIToggle ckb_zhenying;
        private UIToggle ckb_zonghe;
        private string clickName;
        private Font contentFont;
        private int curHistoryIndex;
        private GameObject emotionPkgView;
        private GameObject gnView;
        private GameObject jsChanelView;
        private bool lockSendChannel;
        public GameObject msg;
        public UISprite msgBackground;
        public UIInput msgInput;
        public List<GameObject> msgList = new List<GameObject>();
        public UIPanel MsgPannel;
        public Transform msgs;
        private float nextPosX;
        private float nextPosY;
        private byte openChatType = 1;
        private Dictionary<byte, bool> recChatTypeDic = new Dictionary<byte, bool>();
        public List<string> sendHistoryList = new List<string>();
        public SendMesVo sendMessage = new SendMesVo();
        private float startPosY = -30f;

        private void AutoSetChannelPanel()
        {
            if (this.openChatType == 7)
            {
                this.ckb_siliao.value = true;
            }
            else
            {
                this.ckb_zonghe.value = true;
            }
        }

        public override void CancelUpdateHandler()
        {
            Singleton<ChatMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateMsg);
            Singleton<ChatMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateUpArrowHandle);
            Singleton<ChatMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateDownArrowHandle);
        }

        private void CloseChatOnClick(GameObject go)
        {
            this.CloseView();
        }

        private void CloseRoleInfoHandle(GameObject go)
        {
        }

        public bool ContainsFilter(string str)
        {
            for (int i = 0; i < Singleton<ChatMode>.Instance.FilterString.Length; i++)
            {
                if (str.Contains(Singleton<ChatMode>.Instance.FilterString[i]))
                {
                    return true;
                }
            }
            return false;
        }

        private string ContainWord(string str)
        {
            for (int i = 0; i < Singleton<ChatMode>.Instance.FilterString.Length; i++)
            {
                if (str.Contains(Singleton<ChatMode>.Instance.FilterString[i]))
                {
                    str = str.Replace(Singleton<ChatMode>.Instance.FilterString[i], "**");
                    return str;
                }
            }
            return "*";
        }

        private void EmotionFuncOnClick(GameObject go)
        {
            this.gnView.gameObject.SetActive(false);
            this.emotionPkgView.SetActive(!this.emotionPkgView.activeSelf);
        }

        private void EmotionOnClick(GameObject go)
        {
            this.emotionPkgView.SetActive(false);
            char[] separator = new char[] { '_' };
            string str = "/" + go.GetComponent<UISprite>().spriteName.Split(separator)[1];
            this.msgInput.value = this.msgInput.value + str;
        }

        private string FilterContent(string str)
        {
            for (int i = 0; i < Singleton<ChatMode>.Instance.FilterString.Length; i++)
            {
                if (str.Contains(Singleton<ChatMode>.Instance.FilterString[i]))
                {
                    str = str.Replace(Singleton<ChatMode>.Instance.FilterString[i], string.Empty);
                }
            }
            return str;
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.InitSendChannel();
            this.InitReceiveChannelSet();
            vp_Timer.In(0.1f, new vp_Timer.Callback(this.AutoSetChannelPanel), 1, 0f, null);
            if (this.openChatType == 7)
            {
                this.SetSendChannel(this.openChatType);
            }
            this.ShowMsgs();
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
        }

        protected override void Init()
        {
            this.InitLabelLanguage();
            this.msgBackground = base.FindInChild<UISprite>("center/Msgbackground");
            this.MsgPannel = base.FindInChild<UIPanel>("center/MsgPannel");
            this.msgs = base.FindInChild<Transform>("center/MsgPannel/Msgs");
            this.chat_scroll_view = this.MsgPannel.GetComponent<UIScrollView>();
            this.MsgPannel.clipRange = new Vector4(0f, 0f, ((this.msgBackground.width - 44f) - 44f) + 1f, ((this.msgBackground.height - 22f) - 22f) + 1f);
            this.msgs.transform.localPosition = new Vector3(-this.MsgPannel.clipRange.z / 2f, (this.MsgPannel.clipRange.w / 2f) - 22f, 0f);
            this.ckb_zonghe = base.FindInChild<UIToggle>("top/ckb_zonghe");
            this.ckb_zhenying = base.FindInChild<UIToggle>("top/ckb_zhenying");
            this.ckb_siliao = base.FindInChild<UIToggle>("top/ckb_siliao");
            this.ckb_zonghe.onStateChange = new UIToggle.OnStateChange(this.ZongHeChannelOnStateChange);
            this.ckb_zhenying.onStateChange = new UIToggle.OnStateChange(this.ZhenYingChannelZongHeOnStateChange);
            this.ckb_siliao.onStateChange = new UIToggle.OnStateChange(this.SiLiaoChannelZongHeOnStateChange);
            this.ckb_rec_channel_zhenying_set = base.FindInChild<UIToggle>("UpPannel/jsChannel/zy");
            this.ckb_rec_channel_zonghe_set = base.FindInChild<UIToggle>("UpPannel/jsChannel/zh");
            this.ckb_rec_channel_siliao_set = base.FindInChild<UIToggle>("UpPannel/jsChannel/sl");
            this.ckb_rec_channel_zhenying_set.onStateChange = new UIToggle.OnStateChange(this.ZY_RecChannelSetOnStateChange);
            this.ckb_rec_channel_zonghe_set.onStateChange = new UIToggle.OnStateChange(this.ZH_RecChannelSetOnStateChange);
            this.ckb_rec_channel_siliao_set.onStateChange = new UIToggle.OnStateChange(this.SL_RecChannelSetOnStateChange);
            this.btn_jsChannel_set = base.FindInChild<Button>("center/btn_shezhi");
            this.btn_jsChannel_set.onClick = new UIWidgetContainer.VoidDelegate(this.SetRecChannelOnClick);
            this.btn_close_chat = base.FindInChild<Button>("topright/btn_close_chat");
            this.chatContent = base.FindInChild<UILabel>("center/Input/content");
            this.btn_fsChannel_0 = base.FindInChild<Button>("UpPannel/fsChannel/btn_0");
            this.btn_fsChannel_0.onClick = new UIWidgetContainer.VoidDelegate(this.SetSendChannelOnClick);
            this.btn_fsChannel_1 = base.FindInChild<Button>("UpPannel/fsChannel/btn_1");
            this.btn_fsChannel_1.onClick = new UIWidgetContainer.VoidDelegate(this.SetSendChannelOnClick);
            this.btn_fsChannel_2 = base.FindInChild<Button>("UpPannel/fsChannel/btn_2");
            this.btn_fsChannel_2.onClick = new UIWidgetContainer.VoidDelegate(this.SetSendChannelOnClick);
            this.btn_biaoqing = base.FindInChild<Button>("center/btn_biaoqing");
            this.btn_biaoqing.onClick = new UIWidgetContainer.VoidDelegate(this.EmotionFuncOnClick);
            this.msgInput = base.FindInChild<UIInput>("center/Input");
            this.msgInput.characterLimit = 0x30;
            this.ckb_jsChannel_zh = base.FindInChild<Button>("top/ckb_zonghe");
            this.ckb_jsChannel_zy = base.FindInChild<Button>("top/ckb_zhenying");
            this.ckb_jsChannel_sl = base.FindInChild<Button>("top/ckb_siliao");
            this.btn_fs = base.FindInChild<Button>("center/btn_fs");
            this.gnView = base.FindInChild<Transform>("UpPannel/gn").gameObject;
            this.msgBackground.GetComponent<UIWidgetContainer>().onClick = new UIWidgetContainer.VoidDelegate(this.MsgBackgroundOnClick);
            this.btn_gn_sl = base.FindInChild<Button>("UpPannel/gn/background/btn_sl");
            this.btn_gn_sl.onClick = new UIWidgetContainer.VoidDelegate(this.SWSLOnClick);
            this.btn_gn_jwhy = base.FindInChild<Button>("UpPannel/gn/background/btn_jwhy");
            this.btn_gn_jwhy.onClick = new UIWidgetContainer.VoidDelegate(this.JWHYOnClick);
            this.btn_gn_jhmd = base.FindInChild<Button>("UpPannel/gn/background/btn_jhmd");
            this.btn_gn_jhmd.onClick = new UIWidgetContainer.VoidDelegate(this.JHMDOnClick);
            this.btn_gn_xxzl = base.FindInChild<Button>("UpPannel/gn/background/btn_xxzl");
            this.btn_gn_xxzl.onClick = new UIWidgetContainer.VoidDelegate(this.XXZLOnClick);
            this.jsChanelView = base.FindInChild<Transform>("UpPannel/jsChannel").gameObject;
            this.emotionPkgView = base.FindInChild<Transform>("UpPannel/bq").gameObject;
            this.msg = base.FindInChild<Transform>("center/MsgPannel/Msgs/msg").gameObject;
            this.msg.GetComponent<UIWidgetContainer>().onClick = new UIWidgetContainer.VoidDelegate(this.MsgOnClick);
            this.msg.transform.FindChild("zy/tc1/name").GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.NameOnClick);
            this.msgList.Add(this.msg);
            for (int i = 0; i < 0x1d; i++)
            {
                GameObject item = UnityEngine.Object.Instantiate(Singleton<ChatView>.Instance.msg, Singleton<ChatView>.Instance.msg.transform.position, Singleton<ChatView>.Instance.msg.transform.rotation) as GameObject;
                item.transform.parent = this.msg.transform.parent;
                item.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.MsgOnClick);
                item.transform.FindChild("zy/tc1/name").GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.NameOnClick);
                this.msgList.Add(item);
            }
            this.btn_close_chat.onClick = new UIWidgetContainer.VoidDelegate(this.CloseChatOnClick);
            this.btn_fs.onClick = new UIWidgetContainer.VoidDelegate(this.SendMsgOnClick);
            this.biaoqing_packge = base.FindInChild<Transform>("UpPannel/bq/bq_packge");
            IEnumerator enumerator = this.biaoqing_packge.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    current.GetComponent<UISprite>().atlas = Singleton<AtlasManager>.Instance.GetAtlas("ChatAtlas");
                    current.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.EmotionOnClick);
                    char[] separator = new char[] { '_' };
                    Singleton<ChatMode>.Instance.emoDic.Add(current.GetComponent<UISprite>().spriteName.Split(separator)[1], true);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            this.recChatTypeDic.Add(1, true);
            this.recChatTypeDic.Add(3, true);
            this.recChatTypeDic.Add(7, true);
            EventDelegate.Add(this.msgInput.onSubmit, new EventDelegate.Callback(this.msgEnterOnClick));
        }

        private void InitLabelLanguage()
        {
            base.FindInChild<UILabel>("top/ckb_zonghe/label").text = LanguageManager.GetWord("ChatView.zonghe");
            base.FindInChild<UILabel>("top/ckb_zhenying/label").text = LanguageManager.GetWord("ChatView.zhenying");
            base.FindInChild<UILabel>("top/ckb_siliao/label").text = LanguageManager.GetWord("ChatView.siliao");
            base.FindInChild<UILabel>("center/btn_fs/label").text = LanguageManager.GetWord("ChatView.send");
            base.FindInChild<UILabel>("UpPannel/gn/background/btn_xxzl").text = LanguageManager.GetWord("ChatView.roleData");
            base.FindInChild<UILabel>("UpPannel/gn/background/btn_jwhy").text = LanguageManager.GetWord("ChatView.addFriend");
            base.FindInChild<UILabel>("UpPannel/gn/background/btn_jhmd").text = LanguageManager.GetWord("ChatView.addBlack");
            base.FindInChild<UILabel>("UpPannel/gn/background/btn_sl").text = LanguageManager.GetWord("ChatView.siliao2");
            base.FindInChild<UILabel>("UpPannel/gn/background/btn_ckwp").text = LanguageManager.GetWord("ChatView.checkGoods");
            base.FindInChild<UILabel>("UpPannel/jsChannel/wenzi").text = LanguageManager.GetWord("ChatView.setReceiveChannel");
            base.FindInChild<UILabel>("UpPannel/jsChannel/zh/wenzi").text = LanguageManager.GetWord("ChatView.zongheChannel");
            base.FindInChild<UILabel>("UpPannel/jsChannel/zy/wenzi").text = LanguageManager.GetWord("ChatView.zhenyingChannel");
            base.FindInChild<UILabel>("UpPannel/jsChannel/sl/wenzi").text = LanguageManager.GetWord("ChatView.siliaoChannel");
        }

        private void InitReceiveChannelSet()
        {
            this.ckb_rec_channel_zhenying_set.startsActive = LocalVarManager.GetInt("Chat_RecieveZongHeChannel", 0) != 2;
            this.ckb_rec_channel_zonghe_set.startsActive = LocalVarManager.GetInt("Chat_RecieveZhenYingChannel", 0) != 2;
            this.ckb_rec_channel_siliao_set.startsActive = LocalVarManager.GetInt("Chat_RecieveSiLiaoChannel", 0) != 2;
        }

        private void InitSendChannel()
        {
            this.lockSendChannel = false;
        }

        private void JHMDOnClick(GameObject go)
        {
            Log.info(this, "加黑名单:" + this.clickName);
            Singleton<FriendControl>.Instance.MoveToBlackList(Singleton<ChatMode>.Instance.nameToIdDic[this.clickName]);
        }

        private void JWHYOnClick(GameObject go)
        {
            Log.info(this, "加为好友:" + this.clickName);
            Singleton<FriendControl>.Instance.SendFriendAskByName(this.clickName);
        }

        private void MsgBackgroundOnClick(GameObject go)
        {
            this.gnView.gameObject.SetActive(false);
        }

        private void msgEnterOnClick()
        {
            Log.info(this, "Enter");
        }

        private void MsgOnClick(GameObject go)
        {
            this.gnView.gameObject.SetActive(false);
        }

        private void NameOnClick(GameObject go)
        {
            if ((go.GetComponent<UILabel>().text != MeVo.instance.Name) && (go.GetComponent<UILabel>().text != LanguageManager.GetWord("ChatView.SysNotice")))
            {
                this.gnView.transform.position = go.transform.position;
                string str = go.GetComponent<UILabel>().text.Replace("[u]", string.Empty).Replace("[/u]", string.Empty);
                this.clickName = str;
                this.gnView.gameObject.SetActive(true);
            }
        }

        public void OpenPrivateChatView(string name, uint roleId)
        {
            this.openChatType = 7;
            this.sendMessage.privateChatName = name;
            this.sendMessage.privateChatRoleId = roleId;
            this.OpenView();
        }

        private void OpenRoleInfoHandle(GameObject go)
        {
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<ChatMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateMsg);
            Singleton<ChatMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateUpArrowHandle);
            Singleton<ChatMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateDownArrowHandle);
        }

        private void SendMsgOnClick(GameObject go)
        {
            if (this.chatContent.text != this.msgInput.defaultText)
            {
                string str = this.msgInput.value;
                str = this.FilterContent(str).Trim();
                if (string.Empty == str)
                {
                    this.msgInput.value = string.Empty;
                }
                else
                {
                    this.sendMessage.content = str;
                    byte sendChatType = this.sendMessage.sendChatType;
                    switch (sendChatType)
                    {
                        case 1:
                        case 3:
                            Singleton<ChatMode>.Instance.SendPublicMsg(this.sendMessage);
                            break;

                        default:
                            if (sendChatType == 7)
                            {
                                Singleton<ChatMode>.Instance.SendPrivateMsg(this.sendMessage);
                            }
                            else
                            {
                                Log.error(this, "聊天类型异常:" + this.sendMessage.sendChatType);
                            }
                            break;
                    }
                    this.emotionPkgView.SetActive(false);
                    if ((this.sendHistoryList.Count == 0) || (this.sendHistoryList[this.sendHistoryList.Count - 1] != this.sendMessage.content))
                    {
                        this.sendHistoryList.Add(this.sendMessage.content);
                        this.curHistoryIndex = this.sendHistoryList.Count;
                    }
                    else
                    {
                        this.curHistoryIndex = this.sendHistoryList.Count;
                    }
                }
            }
        }

        private void SetInputText(bool upArrow)
        {
            if (this.msgInput.isSelected && (this.sendHistoryList.Count >= 1))
            {
                if (upArrow)
                {
                    this.curHistoryIndex--;
                }
                else
                {
                    this.curHistoryIndex++;
                }
                if (this.curHistoryIndex < 0)
                {
                    this.curHistoryIndex = 0;
                }
                else if (this.curHistoryIndex > (this.sendHistoryList.Count - 1))
                {
                    this.curHistoryIndex = this.sendHistoryList.Count - 1;
                }
                this.msgInput.value = this.sendHistoryList[this.curHistoryIndex];
            }
        }

        public void SetPrivateChatObject(string name, ulong Id)
        {
            this.sendMessage.privateChatName = name;
            this.sendMessage.privateChatRoleId = Id;
            this.SetSendChannel(7);
        }

        private void SetRecChannelOnClick(GameObject go)
        {
            this.jsChanelView.SetActive(!this.jsChanelView.activeSelf);
            this.gnView.gameObject.SetActive(false);
        }

        private void SetSendChannel(byte sendChannel)
        {
            Log.info(this, "发送频道被修改为：" + sendChannel);
            this.sendMessage.sendChatType = sendChannel;
            byte num = sendChannel;
            switch (num)
            {
                case 1:
                    this.btn_fsChannel_0.FindInChild<UILabel>("name").text = LanguageManager.GetWord("ChatView.zonghe");
                    this.msgInput.defaultText = LanguageManager.GetWord("ChatView.defaultText");
                    break;

                case 3:
                    this.btn_fsChannel_0.FindInChild<UILabel>("name").text = LanguageManager.GetWord("ChatView.zhenying");
                    this.msgInput.defaultText = LanguageManager.GetWord("ChatView.defaultText");
                    break;

                default:
                    if (num == 7)
                    {
                        this.btn_fsChannel_0.FindInChild<UILabel>("name").text = LanguageManager.GetWord("ChatView.siliao");
                        if (this.sendMessage.privateChatName != null)
                        {
                            this.msgInput.defaultText = LanguageManager.GetWord("ChatView.youSay") + this.sendMessage.privateChatName + LanguageManager.GetWord("ChatView.speak");
                        }
                        else
                        {
                            this.msgInput.defaultText = LanguageManager.GetWord("ChatView.selectTalkRole");
                        }
                    }
                    else
                    {
                        Log.error(this, "发送频道异常:" + sendChannel);
                    }
                    break;
            }
            if (this.msgInput.value == string.Empty)
            {
                this.chatContent.text = this.msgInput.defaultText;
            }
        }

        private void SetSendChannelOnClick(GameObject go)
        {
            this.gnView.gameObject.SetActive(false);
            this.lockSendChannel = true;
            this.btn_fsChannel_0.FindInChild<UILabel>("name").text = LanguageManager.GetWord("ChatView.zonghe");
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$mapA == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                    dictionary.Add("btn_0", 0);
                    dictionary.Add("btn_1", 1);
                    dictionary.Add("btn_2", 2);
                    <>f__switch$mapA = dictionary;
                }
                if (<>f__switch$mapA.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            this.SetSendChannel(1);
                            goto Label_0125;

                        case 1:
                            this.btn_fsChannel_0.FindInChild<UILabel>("name").text = LanguageManager.GetWord("ChatView.siliao");
                            this.SetSendChannel(7);
                            goto Label_0125;

                        case 2:
                            this.btn_fsChannel_0.FindInChild<UILabel>("name").text = LanguageManager.GetWord("ChatView.zhenying");
                            this.SetSendChannel(3);
                            goto Label_0125;
                    }
                }
            }
            Log.error(this, "发送频道按钮名字异常:" + go.name);
        Label_0125:
            this.btn_fsChannel_1.gameObject.SetActive(!this.btn_fsChannel_1.gameObject.activeSelf);
            this.btn_fsChannel_2.gameObject.SetActive(!this.btn_fsChannel_2.gameObject.activeSelf);
        }

        private void ShowEmotion(UILabel content, string cont)
        {
            List<EmotionVo> list = new List<EmotionVo>();
            if (this.contentFont == null)
            {
                this.contentFont = content.trueTypeFont;
            }
            float num = 0f;
            string str = string.Empty;
            string key = string.Empty;
            string text = string.Empty;
            char[] chArray = cont.ToCharArray();
            for (int i = 0; i < chArray.Length; i++)
            {
                if (chArray[i] == '\n')
                {
                    text = "\n";
                    str = str + chArray[i];
                }
                else if (chArray[i] == '/')
                {
                    Log.info(this, "calX:" + NGUIText.CalculatePrintedSize(text).x);
                    Log.info(this, "calY:" + NGUIText.CalculatePrintedSize(text).y);
                    key = string.Empty;
                    int num4 = (((chArray.Length - 1) - i) <= 2) ? ((chArray.Length - 1) - i) : 3;
                    int num5 = 1;
                    num5 = 1;
                    while (num5 <= num4)
                    {
                        if (chArray[i + num5] == '\n')
                        {
                            num4++;
                            str = str + '\n';
                            text = "\n";
                        }
                        else
                        {
                            key = key + chArray[i + num5];
                        }
                        num5++;
                    }
                    i = (i + num5) - 1;
                    if (Singleton<ChatMode>.Instance.emoDic.ContainsKey(key))
                    {
                        str = str + "：";
                        text = text + "：";
                        num = NGUIText.CalculatePrintedSize(text).x % ((float) content.width);
                        float y = content.height - NGUIText.CalculatePrintedSize(text).y;
                        list.Add(new EmotionVo(new Vector3(num - 22f, y, 0f), "bq_" + key));
                    }
                    else
                    {
                        str = str + "/" + key;
                        text = text + "/" + key;
                    }
                }
                else
                {
                    str = str + chArray[i];
                    text = text + chArray[i];
                }
            }
            content.text = str;
            int childCount = content.transform.childCount;
            Transform component = content.transform.FindChild("emo").GetComponent<Transform>();
            component.GetComponent<UISprite>().atlas = Singleton<AtlasManager>.Instance.GetAtlas("ChatAtlas");
            while (content.transform.childCount < list.Count)
            {
                (UnityEngine.Object.Instantiate(component.gameObject, component.position, component.rotation) as GameObject).transform.parent = component.parent;
            }
            int num7 = 0;
            IEnumerator enumerator = content.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    if (num7 < list.Count)
                    {
                        current.localPosition = list[num7].loacalPos;
                        current.localRotation = new Quaternion(0f, 0f, 0f, 0f);
                        current.localScale = new Vector3(1f, 1f, 1f);
                        current.gameObject.SetActive(true);
                        current.GetComponent<UISprite>().spriteName = list[num7].emoName;
                        num7++;
                    }
                    else
                    {
                        current.gameObject.SetActive(false);
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }

        private void ShowMsgs()
        {
            GameObject obj3;
            Log.info(this, "更新聊天框信息");
            foreach (GameObject obj2 in Singleton<ChatView>.Instance.msgList)
            {
                if (!obj2.activeSelf)
                {
                    break;
                }
            }
            this.nextPosY = this.startPosY;
            int num = 0;
            float num2 = 0f;
            int num3 = 0;
            float num4 = 0f;
            foreach (ChatVo vo in Singleton<ChatMode>.Instance.recChatVoList)
            {
                num3++;
                if (this.recChatTypeDic[vo.chatType])
                {
                    obj3 = Singleton<ChatView>.Instance.msgList[num++];
                    obj3.gameObject.SetActive(true);
                    obj3.transform.localPosition = new Vector3(this.nextPosX, this.nextPosY, 0f);
                    obj3.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
                    obj3.transform.localScale = new Vector3(1f, 1f, 1f);
                    UILabel component = obj3.transform.FindChild("zy").GetComponent<UILabel>();
                    component.color = Singleton<ChatMode>.Instance.GREEN;
                    component.fontSize = 0x16;
                    component.text = (vo.nationId == 0) ? string.Empty : string.Concat(new object[] { "[", LanguageManager.GetWord("ChatView.zhenying"), vo.nationId, "]" });
                    UILabel label2 = component.transform.FindChild("tc1").GetComponent<UILabel>();
                    label2.transform.localPosition = new Vector3((float) component.width, 0f, 0f);
                    label2.color = Singleton<ChatMode>.Instance.PURPLE;
                    label2.fontSize = 0x16;
                    label2.text = (vo.chatType != 7) ? string.Empty : ((vo.senderId != MeVo.instance.Id) ? string.Empty : LanguageManager.GetWord("ChatView.youSay"));
                    UILabel label3 = label2.transform.FindChild("name").GetComponent<UILabel>();
                    label3.transform.localPosition = new Vector3((float) label2.width, 0f, 0f);
                    label3.color = (vo.chatType != 7) ? ((vo.chatType != 1) ? Singleton<ChatMode>.Instance.GREEN : Singleton<ChatMode>.Instance.YELLOW) : Singleton<ChatMode>.Instance.PURPLE;
                    if (vo.senderId == 1L)
                    {
                        label3.color = Color.red;
                    }
                    label3.fontSize = 0x16;
                    label3.text = ((vo.chatType != 7) || (vo.senderId != MeVo.instance.Id)) ? vo.senderName : this.sendMessage.privateChatName;
                    if (((vo.chatType == 1) && (MeVo.instance.Id != vo.senderId)) && (vo.senderId != 1L))
                    {
                        label3.text = "[u]" + label3.text + "[/u]";
                    }
                    BoxCollider collider = label3.gameObject.GetComponent<BoxCollider>();
                    if (null == collider)
                    {
                        label3.gameObject.AddComponent<BoxCollider>();
                    }
                    collider.size = new Vector3((float) label3.width, (float) label3.height, 1f);
                    collider.center = new Vector3(((float) label3.width) / 2f, ((float) label3.height) / 2f, 0f);
                    UILabel label4 = label3.transform.FindChild("tc2").GetComponent<UILabel>();
                    label4.transform.localPosition = new Vector3((float) label3.width, 0f, 0f);
                    label4.color = (vo.chatType != 7) ? ((vo.chatType != 1) ? Singleton<ChatMode>.Instance.GREEN : Singleton<ChatMode>.Instance.WHITE) : Singleton<ChatMode>.Instance.PURPLE;
                    label4.fontSize = 0x16;
                    label4.text = (vo.chatType != 7) ? ": " : ((vo.senderId != MeVo.instance.Id) ? LanguageManager.GetWord("ChatView.sayToYou") : LanguageManager.GetWord("ChatView.speak"));
                    UILabel content = label4.transform.FindChild("content").GetComponent<UILabel>();
                    content.fontSize = 0x16;
                    content.width = (((((int) Singleton<ChatView>.Instance.MsgPannel.clipRange.z) - component.width) - label2.width) - label3.width) - label4.width;
                    content.color = Singleton<ChatMode>.Instance.WHITE;
                    content.text = vo.content;
                    this.ShowEmotion(content, content.processedText);
                    content.transform.localPosition = new Vector3((float) label4.width, (float) (-content.height + content.fontSize), 0f);
                    float x = (((component.width + label2.width) + label3.width) + label4.width) + content.width;
                    obj3.GetComponent<BoxCollider>().size = new Vector3(x, content.height + 9f, 1f);
                    obj3.GetComponent<BoxCollider>().center = new Vector3(x / 2f, (((float) -content.height) / 2f) + 22f, 0f);
                    num4 += obj3.GetComponent<BoxCollider>().size.y;
                    this.nextPosY = (this.nextPosY - content.height) - 9f;
                    num2 = obj3.transform.localPosition.y - obj3.GetComponent<BoxCollider>().size.y;
                    if (num2 < -Singleton<ChatView>.Instance.MsgPannel.clipRange.w)
                    {
                        this.startPosY += -Singleton<ChatView>.Instance.MsgPannel.clipRange.w - num2;
                        this.ShowMsgs();
                        break;
                    }
                }
            }
            if (num4 > (Singleton<ChatView>.Instance.MsgPannel.clipRange.w - 32f))
            {
                this.chat_scroll_view.enabled = true;
            }
            else
            {
                this.chat_scroll_view.enabled = false;
            }
            for (int i = num; i < Singleton<ChatView>.Instance.msgList.Count; i++)
            {
                obj3 = Singleton<ChatView>.Instance.msgList[i];
                obj3.gameObject.SetActive(false);
            }
        }

        private void SiLiaoChannelZongHeOnStateChange(bool state)
        {
            if (state)
            {
                Log.info(this, "选择私聊频道");
                this.recChatTypeDic[1] = false;
                this.recChatTypeDic[3] = false;
                this.recChatTypeDic[7] = true;
                this.msgInput.value = string.Empty;
                this.ShowMsgs();
                if (!this.lockSendChannel)
                {
                    this.SetSendChannel(7);
                }
            }
        }

        private void SL_RecChannelSetOnStateChange(bool state)
        {
            Log.info(this, "勾选私聊频道接收？" + state);
            LocalVarManager.SetInt("Chat_RecieveSiLiaoChannel", !state ? 2 : 1);
        }

        private void SWSLOnClick(GameObject go)
        {
            this.gnView.gameObject.SetActive(false);
            this.ckb_siliao.value = true;
            this.SetPrivateChatObject(this.clickName, Singleton<ChatMode>.Instance.nameToIdDic[this.clickName]);
        }

        private void UpdateDownArrowHandle(object sender, int type)
        {
            if (Singleton<ChatMode>.Instance.UPDATE_DOWNARROW == type)
            {
                this.SetInputText(false);
            }
        }

        private void UpdateMsg(object sender, int code)
        {
            if (code == Singleton<ChatMode>.Instance.UPDATE_CHAT_MSG)
            {
                this.ShowMsgs();
            }
        }

        private void UpdateUpArrowHandle(object sender, int type)
        {
            if (Singleton<ChatMode>.Instance.UPDATE_UPARROW == type)
            {
                this.SetInputText(true);
            }
        }

        private void XXZLOnClick(GameObject go)
        {
            Log.info(this, "详细资料按钮被点击");
        }

        private void ZH_RecChannelSetOnStateChange(bool state)
        {
            Log.info(this, "勾选综合频道接收？" + state);
            LocalVarManager.SetInt("Chat_RecieveZongHeChannel", !state ? 2 : 1);
        }

        private void ZhenYingChannelZongHeOnStateChange(bool state)
        {
            if (state)
            {
                Log.info(this, "选择阵营频道");
                this.recChatTypeDic[1] = false;
                this.recChatTypeDic[3] = true;
                this.recChatTypeDic[7] = false;
                this.ShowMsgs();
                if (!this.lockSendChannel)
                {
                    this.SetSendChannel(3);
                }
            }
        }

        private void ZongHeChannelOnStateChange(bool state)
        {
            if (state)
            {
                Log.info(this, "选择综合频道");
                this.recChatTypeDic[1] = true;
                this.recChatTypeDic[3] = true;
                this.recChatTypeDic[7] = true;
                this.msgInput.value = string.Empty;
                this.ShowMsgs();
                if (!this.lockSendChannel)
                {
                    this.SetSendChannel(1);
                }
            }
        }

        private void ZY_RecChannelSetOnStateChange(bool state)
        {
            Log.info(this, "勾选阵营频道接收？" + state);
            LocalVarManager.SetInt("Chat_RecieveZhenYingChannel", !state ? 2 : 1);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Chat/ChatView.assetbundle";
            }
        }
    }
}

