﻿namespace com.game.module.Chat
{
    using PCustomDataType;
    using System;

    public class ChatVo
    {
        public byte chatType;
        public string content = string.Empty;
        public PChatPushGoods goods = new PChatPushGoods();
        public byte nationId;
        public ulong senderId;
        public byte senderJob;
        public byte senderLvl;
        public string senderName = string.Empty;
        public byte senderSex;
        public byte senderVip;
        public ushort serverId;
    }
}

