﻿namespace com.game.module.Chat
{
    using System;
    using UnityEngine;

    public class EmotionVo
    {
        public string emoName;
        public Vector3 loacalPos;

        public EmotionVo(Vector3 Pos, string Name)
        {
            this.loacalPos = Pos;
            this.emoName = Name;
        }
    }
}

