﻿namespace com.game.module.Chat
{
    using System;

    public class SendMesVo
    {
        public string content;
        public string privateChatName;
        public ulong privateChatRoleId;
        public byte sendChatType;
    }
}

