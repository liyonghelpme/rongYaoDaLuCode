﻿namespace com.game.module.ContinueCut
{
    using com.game.consts;
    using com.game.module.core;
    using Holoville.HOTween;
    using System;
    using UnityEngine;

    public class ContinueCutView : BaseView<ContinueCutView>
    {
        private Button _btnGroupSkill;
        private int _cutNum;
        private UILabel _labCutNum;
        private float _leftTime;
        private UISprite _sprLeftTime;

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.UpdateInfo();
        }

        protected override void Init()
        {
            base.firstOpen = false;
            this._labCutNum = base.FindInChild<UILabel>("right/cutnum");
            this._sprLeftTime = base.FindInChild<UISprite>("right/timeleft");
            this._btnGroupSkill = base.FindInChild<Button>("btnGroupSkill");
            this._btnGroupSkill.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickSkill);
            this.StartScaleNum();
        }

        private void OnClickSkill(GameObject go)
        {
            Debug.Log("战队大招");
        }

        private void ShowEffect()
        {
            this.StartScaleNum();
        }

        public void ShowLeftTime(float timeLeft)
        {
            this._leftTime = timeLeft;
            this.UpdateView();
        }

        public void ShowNum(int num)
        {
            this._cutNum = num;
            this.ShowEffect();
            this.UpdateView();
        }

        private void StartScaleNum()
        {
            Sequence sequence = new Sequence(new SequenceParms().Loops(1, Holoville.HOTween.LoopType.Yoyo));
            sequence.Append(Holoville.HOTween.HOTween.To(this._labCutNum.gameObject.transform, 0.06f, new TweenParms().Prop("localScale", new Vector3(2f, 2f, 1f), false)));
            sequence.Append(Holoville.HOTween.HOTween.To(this._labCutNum.gameObject.transform, 0.12f, new TweenParms().Prop("localScale", new Vector3(1f, 1f, 1f), false).Delay(0.02f)));
            sequence.Insert(0f, Holoville.HOTween.HOTween.To(this._labCutNum, 0.06f, new TweenParms().Prop("color", ColorConst.Blood)));
            sequence.Insert(0.08f, Holoville.HOTween.HOTween.To(this._labCutNum, 0.12f, new TweenParms().Prop("color", Color.white)));
            sequence.Play();
        }

        private void UpdateInfo()
        {
            this._labCutNum.text = this._cutNum.ToString();
            this._sprLeftTime.fillAmount = this._leftTime;
        }

        private void UpdateView()
        {
            if ((null == this.gameObject) || !this.gameObject.activeSelf)
            {
                this.OpenView();
            }
            else
            {
                this.UpdateInfo();
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.LowLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/ContinueCut/ContinueCutView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

