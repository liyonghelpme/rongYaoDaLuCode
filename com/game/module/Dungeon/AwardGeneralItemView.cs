﻿namespace com.game.module.Dungeon
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.General;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class AwardGeneralItemView
    {
        private int _curFc;
        private GameObject _effect;
        private float _endProgress;
        private UILabel _exp;
        private UISlider _expProgress;
        private SysGeneralExpVo _expVo;
        private float _gap;
        private GameObject _go;
        private UISprite _icon;
        private GeneralTemplateInfo _info;
        private UILabel _lvl;
        private UILabel _name;
        private static readonly float FRAME = 32f;

        public AwardGeneralItemView(GameObject go)
        {
            this._go = go;
            this._effect = NGUITools.FindChild(this._go, "effect");
            this._effect.SetActive(false);
            this._icon = NGUITools.FindInChild<UISprite>(this._go, "img");
            this._icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this._name = NGUITools.FindInChild<UILabel>(this._go, "name");
            this._lvl = NGUITools.FindInChild<UILabel>(this._go, "lv");
            this._exp = NGUITools.FindInChild<UILabel>(this._go, "exp");
            this._expProgress = NGUITools.FindInChild<UISlider>(this._go, "expbar");
        }

        private void InitProgress(int expTotal)
        {
            uint lv = 0;
            SysGeneralExpVo vo = null;
            SysGeneralExpVo generalExpVo = null;
            do
            {
                vo = generalExpVo;
                lv++;
                generalExpVo = BaseDataMgr.instance.GetGeneralExpVo(lv);
            }
            while (generalExpVo.total_exp <= expTotal);
            if (vo == null)
            {
                this._expProgress.value = ((float) expTotal) / ((float) generalExpVo.exp_level);
            }
            else
            {
                this._expProgress.value = ((float) (expTotal - vo.total_exp)) / ((float) generalExpVo.exp_level);
            }
        }

        private void PauseProgress()
        {
            if ((Time.frameCount - this._curFc) >= 8)
            {
                this._expProgress.value = 0f;
                this._gap = this._endProgress / FRAME;
                DungeonAwardView instance = Singleton<DungeonAwardView>.Instance;
                instance.onUpdate = (System.Action) Delegate.Remove(instance.onUpdate, new System.Action(this.PauseProgress));
                DungeonAwardView local2 = Singleton<DungeonAwardView>.Instance;
                local2.onUpdate = (System.Action) Delegate.Combine(local2.onUpdate, new System.Action(this.UpdateProgress2));
            }
        }

        public void Update(GeneralFightInfo general, PDuplicateAward award)
        {
            SysGeneralVo genralTemplateInfo = general.genralTemplateInfo;
            this._icon.spriteName = general.genralTemplateInfo.icon_id.ToString();
            this._name.text = genralTemplateInfo.name;
            this._info = Singleton<GeneralMode>.Instance.allGeneralInfoList[(uint) general.genralTemplateInfo.general_id];
            this._lvl.text = this._info.generalInfo.lvl.ToString();
            this._exp.text = "+" + award.generalExp.ToString();
            this._expVo = BaseDataMgr.instance.GetGeneralExpVo(this._info.generalInfo.lvl);
            SysGeneralExpVo generalExpVo = BaseDataMgr.instance.GetGeneralExpVo((uint) (this._info.generalInfo.lvl - 1));
            ulong num = 0L;
            if (generalExpVo != null)
            {
                num = (ulong) generalExpVo.total_exp;
            }
            this._endProgress = ((float) (this._info.generalInfo.exp - num)) / ((float) this._expVo.exp_level);
            this.InitProgress(((int) this._info.generalInfo.exp) - ((int) award.generalExp));
            this._effect.SetActive(false);
            if ((this._info.generalInfo.exp - award.generalExp) < num)
            {
                this._effect.SetActive(true);
                this._gap = (1f - this._expProgress.value) / FRAME;
                DungeonAwardView instance = Singleton<DungeonAwardView>.Instance;
                instance.onUpdate = (System.Action) Delegate.Combine(instance.onUpdate, new System.Action(this.UpdateProgress1));
            }
            else
            {
                this._gap = this._endProgress / FRAME;
                DungeonAwardView local2 = Singleton<DungeonAwardView>.Instance;
                local2.onUpdate = (System.Action) Delegate.Combine(local2.onUpdate, new System.Action(this.UpdateProgress2));
            }
        }

        private void UpdateProgress1()
        {
            this._expProgress.value += this._gap;
            if (this._expProgress.value >= 1f)
            {
                this._effect.SetActive(false);
                this._expProgress.value = 1f;
                this._curFc = Time.frameCount;
                DungeonAwardView instance = Singleton<DungeonAwardView>.Instance;
                instance.onUpdate = (System.Action) Delegate.Remove(instance.onUpdate, new System.Action(this.UpdateProgress1));
                DungeonAwardView local2 = Singleton<DungeonAwardView>.Instance;
                local2.onUpdate = (System.Action) Delegate.Combine(local2.onUpdate, new System.Action(this.PauseProgress));
            }
        }

        private void UpdateProgress2()
        {
            this._expProgress.value += this._gap;
            if (this._expProgress.value >= this._endProgress)
            {
                this._expProgress.value = this._endProgress;
                DungeonAwardView instance = Singleton<DungeonAwardView>.Instance;
                instance.onUpdate = (System.Action) Delegate.Remove(instance.onUpdate, new System.Action(this.UpdateProgress2));
            }
        }

        public GameObject go
        {
            get
            {
                return this._go;
            }
        }
    }
}

