﻿namespace com.game.module.Dungeon
{
    using com.game.data;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class ChapterBoxItemView
    {
        private SysDungeonStarAwardVo _awardVo;
        private Button _btn;
        private bool _canGet;
        private GameObject _go;
        private bool _hasGot;
        private int _index;
        private int _numStarsGot;
        private int _numStarTotal;
        private UISprite _sp;
        private Transform _trans;

        public ChapterBoxItemView(int index, GameObject go)
        {
            this._index = index;
            this._go = go;
            this._btn = this._go.GetComponent<Button>();
            this._btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
            this._sp = NGUITools.FindInChild<UISprite>(this._go, "background");
            this._trans = this._go.transform;
        }

        private void OnClick(GameObject go)
        {
            Singleton<DungeonStarAwardView>.Instance.Show(this._awardVo);
        }

        public void SetActive(bool b)
        {
            if (null != this._go)
            {
                this._go.SetActive(b);
            }
        }

        public void SetX(float x)
        {
            Vector3 localPosition = this._trans.localPosition;
            localPosition.x = x;
            this._trans.localPosition = localPosition;
        }

        public void UpdateView(SysDungeonStarAwardVo awardVo, int numGot, int numTotal)
        {
            this._awardVo = awardVo;
            this._numStarsGot = numGot;
            this._numStarTotal = numTotal;
            this._hasGot = Singleton<DungeonMode>.Instance.HasGetStarAward(this._awardVo.unikey);
            this._canGet = this._numStarsGot >= this._awardVo.star;
            switch (this._index)
            {
                case 0:
                    this._sp.spriteName = !this._hasGot ? "baoxiang-bai" : "baoxiang-bai-dakai";
                    break;

                case 1:
                    this._sp.spriteName = !this._hasGot ? "baoxiang-jin" : "baoxiang-jin-dakai";
                    break;

                case 2:
                    this._sp.spriteName = !this._hasGot ? "baoxiang-baoshi" : "baoxiang-baoshi-dakai";
                    break;
            }
        }
    }
}

