﻿namespace com.game.module.Dungeon
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.module.Dungeon.DungeonTask;
    using com.game.module.General;
    using com.game.Public.Message;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class DungeonAwardView : BaseView<DungeonAwardView>
    {
        private PDuplicateAward _award;
        private GameObject _awardItemGo;
        private List<SysItemVoCell> _awardItems = new List<SysItemVoCell>();
        private int _closeType;
        private UILabel _expLabel;
        private List<AwardGeneralItemView> _generalList;
        private UILabel _goldLabel;
        private int _lastDungeonId;
        private UIScrollView _scrollView;
        private List<UILabel> _taskLableList;
        private List<UISprite> _taskStarList;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$mapB;
        private Button again_btn;
        private UIGrid award_list;
        private Button next_mission;
        public System.Action onUpdate;
        private Button quit_btn;
        public const string STAR = "xingxing";
        private List<GameObject> start_list;

        private void AfterBackToMainCity(int type, int v1, int v2, object data)
        {
            GlobalAPI.facade.Remove(type, new NoticeListener(this.AfterBackToMainCity));
            switch (this._closeType)
            {
                case 0:
                    Singleton<DungeonChapterView>.Instance.Show(this._lastDungeonId);
                    Singleton<DungeonInfoView>.Instance.Show(this._lastDungeonId);
                    break;

                case 1:
                    Singleton<DungeonChapterView>.Instance.Show(this._lastDungeonId);
                    break;
            }
        }

        private SysItemVoCell GetCell(int idx)
        {
            if (idx >= this._awardItems.Count)
            {
                GameObject obj2;
                if (null == this._awardItemGo)
                {
                    obj2 = this._awardItemGo = base.FindChild("award/award_list/item");
                }
                else
                {
                    obj2 = NGUITools.AddChild(this.award_list.gameObject, this._awardItemGo);
                }
                SysItemVoCell item = obj2.AddComponent<SysItemVoCell>();
                this._awardItems.Add(item);
                return item;
            }
            return this._awardItems[idx];
        }

        protected override void HandleAfterOpenView()
        {
            this._scrollView.ResetPosition();
            this.gameObject.SetActive(true);
            this.UpdateTaskList();
            this.UpdateAward();
        }

        protected override void HandleBeforeCloseView()
        {
            this.onUpdate = null;
        }

        protected override void Init()
        {
            StarEffect.Instance.Init(base.FindChild("star_list/img"));
            this.start_list = new List<GameObject>();
            this.again_btn = base.FindInChild<Button>("buttons/angin");
            this.quit_btn = base.FindInChild<Button>("buttons/quit");
            this.next_mission = base.FindInChild<Button>("buttons/next");
            this.award_list = base.FindInChild<UIGrid>("award/award_list");
            this.start_list.Add(base.FindChild("star_list/star1"));
            this.start_list.Add(base.FindChild("star_list/star2"));
            this.start_list.Add(base.FindChild("star_list/star3"));
            this.gameObject.SetActive(false);
            this.next_mission.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.again_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.quit_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this._scrollView = base.FindInChild<UIScrollView>("award");
            this._taskLableList = new List<UILabel>();
            this._taskStarList = new List<UISprite>();
            for (int i = 1; i <= 3; i++)
            {
                string str = "infos/star" + i + "/";
                UILabel item = base.FindInChild<UILabel>(str + "name");
                UISprite sprite = base.FindInChild<UISprite>(str + "icon");
                this._taskLableList.Add(item);
                this._taskStarList.Add(sprite);
            }
            this._expLabel = base.FindInChild<UILabel>("title/exp");
            this._goldLabel = base.FindInChild<UILabel>("title/gold");
            this._generalList = new List<AwardGeneralItemView>();
            for (int j = 1; j <= 3; j++)
            {
                GameObject go = base.FindChild("team_info/item" + j);
                this._generalList.Add(new AwardGeneralItemView(go));
            }
        }

        private void onClickHandler(GameObject go)
        {
            this.CloseView();
            Singleton<DungeonMode>.Instance.quitDungeon();
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$mapB == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                    dictionary.Add("angin", 0);
                    dictionary.Add("next", 1);
                    dictionary.Add("quit", 2);
                    <>f__switch$mapB = dictionary;
                }
                if (<>f__switch$mapB.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            this._lastDungeonId = DungeonMgr.Instance.curDungeonId;
                            this._closeType = 0;
                            GlobalAPI.facade.Add(0x10, new NoticeListener(this.AfterBackToMainCity));
                            Singleton<BattleTopView>.Instance.ResetBossBlood();
                            GlobalAPI.facade.Notify(0x191, 0, 0, null);
                            break;

                        case 1:
                        {
                            SysDungeonVo curDungeonSysVo = DungeonMgr.Instance.curDungeonSysVo;
                            if ((curDungeonSysVo == null) || (curDungeonSysVo.next_id <= 0))
                            {
                                MessageManager.Show("没有下一关");
                                return;
                            }
                            this._lastDungeonId = curDungeonSysVo.next_id;
                            this._closeType = 0;
                            GlobalAPI.facade.Add(0x10, new NoticeListener(this.AfterBackToMainCity));
                            break;
                        }
                        case 2:
                            this._closeType = 1;
                            this._lastDungeonId = DungeonMgr.Instance.curDungeonId;
                            GlobalAPI.facade.Add(0x10, new NoticeListener(this.AfterBackToMainCity));
                            break;
                    }
                }
            }
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        public void Show(List<PDuplicateAward> list)
        {
            this._award = ((list == null) || (list.Count <= 0)) ? null : list[0];
            this.OpenView();
        }

        public override void Update()
        {
            if (this.onUpdate != null)
            {
                this.onUpdate();
            }
        }

        private void UpdateAward()
        {
            foreach (AwardGeneralItemView view in this._generalList)
            {
                view.go.SetActive(false);
            }
            if (this._award == null)
            {
                this._expLabel.text = string.Empty;
                this._goldLabel.text = string.Empty;
                this.award_list.SetActive(false);
            }
            else
            {
                this._expLabel.text = this._award.exp.ToString();
                this._goldLabel.text = this._award.gold.ToString();
                int idx = 0;
                foreach (GeneralFightInfo info in Singleton<GeneralMode>.Instance.generalsFightAttrList.Values)
                {
                    if (idx >= this._generalList.Count)
                    {
                        break;
                    }
                    this._generalList[idx].go.SetActive(true);
                    this._generalList[idx].Update(info, this._award);
                    idx++;
                }
                idx = 0;
                if ((this._award.dropList != null) && (this._award.dropList.Count > 0))
                {
                    this.award_list.SetActive(true);
                    foreach (PDuplicateDrop drop in this._award.dropList)
                    {
                        SysItemVoCell cell = this.GetCell(idx);
                        cell.gameObject.SetActive(true);
                        cell.sysVoId = (int) drop.id;
                        cell.count = drop.num;
                        idx++;
                    }
                    while (idx < this._awardItems.Count)
                    {
                        this._awardItems[idx].gameObject.SetActive(false);
                        idx++;
                    }
                    this.award_list.Reposition();
                }
            }
        }

        private void UpdateTaskList()
        {
            UILabel label;
            UISprite componentInChildren;
            int num = 0;
            int num2 = 0;
            foreach (MTask task in Singleton<MTaskMgr>.Instance.taskList.Values)
            {
                label = this._taskLableList[num2];
                componentInChildren = this._taskStarList[num2];
                label.transform.parent.gameObject.SetActive(true);
                if (task.isFinished)
                {
                    label.color = Color.green;
                    componentInChildren.spriteName = "xingxing";
                    componentInChildren.ShowAsMyself();
                    num++;
                }
                else
                {
                    label.color = Color.white;
                    componentInChildren.spriteName = "xingxing";
                    componentInChildren.ShowAsGray();
                }
                label.text = task.taskSysVo.content;
                num2++;
            }
            while (num2 < this._taskLableList.Count)
            {
                label = this._taskLableList[num2];
                label.transform.parent.gameObject.SetActive(false);
                num2++;
            }
            StarEffect.Instance.Clear();
            for (num2 = 0; num2 < this.start_list.Count; num2++)
            {
                this.start_list[num2].SetActive(true);
                componentInChildren = this.start_list[num2].GetComponentInChildren<UISprite>();
                componentInChildren.spriteName = "xingxing";
                componentInChildren.ShowAsGray();
                if (num2 < num)
                {
                    StarEffect.Instance.Add(componentInChildren);
                }
            }
            StarEffect.Instance.Play(null);
            int num3 = DungeonMgr.Instance.curDungeonSysVo.next_id;
            this.next_mission.SetActive(num3 > 0);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public Button QuitBtn
        {
            get
            {
                return this.quit_btn;
            }
        }
    }
}

