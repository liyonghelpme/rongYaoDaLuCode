﻿namespace com.game.module.Dungeon
{
    using com.game.consts;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class DungeonChapterView : BaseView<DungeonChapterView>
    {
        private GameObject _awardGo;
        private List<ChapterBoxItemView> _boxes = new List<ChapterBoxItemView>();
        private UILabel _chapterName;
        private SysDungeonChapterVo _curChapter;
        private int _curDungeonId;
        private int _curDungeonType = 1;
        private List<DungeonChapterItemView> _items = new List<DungeonChapterItemView>();
        private GameObject _itemView;
        private UILabel _progLabel;
        private UISprite _selectFlag;
        private UISlider _slider;
        private UISprite _sliderFg;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$mapC;
        public EventDelegate.Callback AfterOpenViewGuideDelegate;
        private UISprite background;
        private Button close_btn;
        private Button elite_btn;
        private UILabel eliteLabel;
        private UISprite eliteSprite;
        private GameObject item_list;
        private Button next_btn;
        private UILabel nomalLabel;
        private Button normal_btn;
        private UISprite normalSprite;
        private Button prev_btn;
        private UISprite title_img;

        public override void CancelUpdateHandler()
        {
            Singleton<DungeonMode>.Instance.dataUpdated -= new DataUpateHandler(this.DataUpdated);
        }

        private DungeonChapterItemView CreateItem()
        {
            GameObject obj2;
            if (null == this._itemView)
            {
                obj2 = this._itemView = base.FindChild("list/item");
            }
            else
            {
                obj2 = NGUITools.AddChild(this.item_list, this._itemView);
            }
            return new DungeonChapterItemView(obj2);
        }

        public override void DataUpdated(object sender, int code)
        {
            switch (code)
            {
                case 1:
                case 2:
                    this.UpdateView();
                    break;
            }
        }

        private SysDungeonChapterVo GetNewestChapter(int dungeonType)
        {
            DungeonInfo lastDungeonInfo = Singleton<DungeonMode>.Instance.GetLastDungeonInfo(dungeonType);
            SysDungeonVo vo = null;
            if (lastDungeonInfo == null)
            {
                switch (this._curDungeonType)
                {
                    case 1:
                        return BaseDataMgr.instance.getDungeonChapter(0x2711);

                    case 2:
                        return BaseDataMgr.instance.getDungeonChapter(DungeonMode.FirstEliteChapterId);
                }
            }
            else
            {
                vo = BaseDataMgr.instance.getDungeon((uint) lastDungeonInfo.dungeon.id);
                if (lastDungeonInfo.dungeon.next_id > 0)
                {
                    SysDungeonVo vo2 = BaseDataMgr.instance.getDungeon((uint) lastDungeonInfo.dungeon.next_id);
                    if (Singleton<DungeonMode>.Instance.IsChapterOpened(vo2.chapter_id))
                    {
                        vo = vo2;
                    }
                }
            }
            if (vo != null)
            {
                return BaseDataMgr.instance.getDungeonChapter((uint) vo.chapter_id);
            }
            return null;
        }

        protected override void HandleAfterOpenView()
        {
            if (this._curDungeonId > 0)
            {
                SysDungeonVo vo = BaseDataMgr.instance.getDungeon((uint) this._curDungeonId);
                if (vo != null)
                {
                    if (vo.chapter_id > 0)
                    {
                        this._curDungeonType = vo.subtype;
                        this._curChapter = BaseDataMgr.instance.getDungeonChapter((uint) vo.chapter_id);
                    }
                    else
                    {
                        this._curDungeonType = 1;
                    }
                }
            }
            if (this._curChapter == null)
            {
                this._curChapter = this.GetNewestChapter(this._curDungeonType);
            }
            this.UpdateView();
            if (this.AfterOpenViewGuideDelegate != null)
            {
                this.AfterOpenViewGuideDelegate();
            }
        }

        protected override void HandleBeforeCloseView()
        {
            this._curDungeonType = 1;
            this._curChapter = null;
            this._curDungeonId = 0;
        }

        protected override void Init()
        {
            this.title_img = base.FindInChild<UISprite>("title/titleImg");
            this._chapterName = base.FindInChild<UILabel>("title/name");
            this.background = base.FindInChild<UISprite>("backgound");
            this.close_btn = base.FindInChild<Button>("close");
            this.next_btn = base.FindInChild<Button>("next");
            this.prev_btn = base.FindInChild<Button>("prev");
            this.normal_btn = base.FindInChild<Button>("type/normal");
            this.elite_btn = base.FindInChild<Button>("type/elite");
            this.normalSprite = base.FindInChild<UISprite>("type/normal/Background");
            this.eliteSprite = base.FindInChild<UISprite>("type/elite/Background");
            this.nomalLabel = base.FindInChild<UILabel>("type/normal/Label");
            this.eliteLabel = base.FindInChild<UILabel>("type/elite/Label");
            this.item_list = base.FindChild("list");
            this.normal_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.elite_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.close_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.prev_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.next_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.gameObject.SetActive(false);
            this._awardGo = base.FindChild("info");
            this._slider = base.FindInChild<UISlider>("info/starbar");
            this._sliderFg = base.FindInChild<UISprite>("info/starbar/foreground");
            this._progLabel = base.FindInChild<UILabel>("info/progLabel");
            for (int i = 1; i <= 3; i++)
            {
                ChapterBoxItemView item = new ChapterBoxItemView(i - 1, base.FindChild("info/item" + i));
                this._boxes.Add(item);
            }
            this._selectFlag = base.FindInChild<UISprite>("list/selectFlag");
        }

        private void onClickHandler(GameObject go)
        {
            SysDungeonChapterVo vo = this._curChapter;
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$mapC == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(5);
                    dictionary.Add("close", 0);
                    dictionary.Add("prev", 1);
                    dictionary.Add("next", 2);
                    dictionary.Add("normal", 3);
                    dictionary.Add("elite", 4);
                    <>f__switch$mapC = dictionary;
                }
                if (<>f__switch$mapC.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            this.CloseView();
                            break;

                        case 1:
                            if (vo.prev_chapter != 0)
                            {
                                this._curChapter = BaseDataMgr.instance.getDungeonChapter((uint) vo.prev_chapter);
                                this.UpdateView();
                            }
                            break;

                        case 2:
                            if (vo.next_chapter != 0)
                            {
                                if (!Singleton<DungeonMode>.Instance.IsChapterOpened(vo.next_chapter))
                                {
                                    MessageManager.Show("该章节还未开启！");
                                    return;
                                }
                                this._curChapter = BaseDataMgr.instance.getDungeonChapter((uint) vo.next_chapter);
                                this.UpdateView();
                            }
                            break;

                        case 3:
                            this.SwitchType(1);
                            break;

                        case 4:
                            this.SwitchType(2);
                            break;
                    }
                }
            }
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        public override void RegisterUpdateHandler()
        {
            base.RegisterUpdateHandler();
            Singleton<DungeonMode>.Instance.dataUpdated += new DataUpateHandler(this.DataUpdated);
        }

        public void Show(int dungeonId)
        {
            this._curDungeonId = dungeonId;
            this.OpenView();
        }

        private void SwitchType(int type)
        {
            if (this._curDungeonType != type)
            {
                if ((type == 2) && !Singleton<DungeonMode>.Instance.HasPassedChapter(((int) DungeonMode.FirstEliteChapterId) - 1))
                {
                    SysDungeonChapterVo vo = BaseDataMgr.instance.getDungeonChapter(DungeonMode.FirstEliteChapterId - 1);
                    MessageManager.ShowLang("Dungeon.EliteNotOpened", vo.name);
                }
                else
                {
                    this._curDungeonType = type;
                    SysDungeonChapterVo newestChapter = this.GetNewestChapter(this._curDungeonType);
                    int num = (this._curDungeonType != 1) ? Math.Max(this._curChapter.id + 1, (int) DungeonMode.FirstEliteChapterId) : Math.Max(this._curChapter.id - 1, 0x2711);
                    if (num < newestChapter.id)
                    {
                        this._curChapter = BaseDataMgr.instance.getDungeonChapter((uint) num);
                    }
                    else
                    {
                        this._curChapter = newestChapter;
                    }
                    this._curDungeonId = 0;
                    this.UpdateView();
                }
            }
        }

        private void updateDungeonList()
        {
            DungeonChapterItemView view;
            SysDungeonChapterVo vo = BaseDataMgr.instance.getDungeonChapter((uint) this._curChapter.id);
            this._chapterName.text = vo.name;
            this.prev_btn.SetActive(vo.prev_chapter > 0);
            this.next_btn.SetActive(vo.next_chapter > 0);
            int[] stringToInt = StringUtils.GetStringToInt(vo.dungeon_list);
            while (this._items.Count < stringToInt.Length)
            {
                view = this.CreateItem();
                this._items.Add(view);
            }
            string[] valueCost = StringUtils.GetValueCost(vo.dungeon_list_point);
            view = null;
            for (int i = 0; i < this._items.Count; i++)
            {
                if (i < stringToInt.Length)
                {
                    this._items[i].go.SetActive(true);
                    this._items[i].go.transform.localPosition = MathUtils.GetVector3Cost(valueCost[i]);
                    this._items[i].dungeonId = stringToInt[i];
                    if ((this._curDungeonId > 0) && (stringToInt[i] == this._curDungeonId))
                    {
                        view = this._items[i];
                    }
                }
                else
                {
                    this._items[i].go.SetActive(false);
                }
            }
            if (view != null)
            {
                this._selectFlag.SetActive(true);
                this._selectFlag.transform.localPosition = view.go.transform.localPosition;
            }
            else
            {
                this._selectFlag.SetActive(false);
            }
        }

        private void updateSpriteName(UISprite sprite, UILabel uiLabel, bool state)
        {
            string str = sprite.spriteName.Replace("-down", string.Empty);
            string str2 = !state ? str : ((sprite.atlas.GetSprite(str + "-down") == null) ? str : (str + "-down"));
            sprite.spriteName = str2;
            uiLabel.color = !state ? ColorConst.FontColor_687687 : ColorConst.Black;
        }

        private void UpdateStarAwardList()
        {
            SysDungeonChapterVo vo = this._curChapter;
            bool flag = false;
            this._awardGo.SetActive(flag);
            if (flag)
            {
                int starAchieved = Singleton<DungeonMode>.Instance.GetStarAchieved(vo.id);
                int starTotal = Singleton<DungeonMode>.Instance.GetStarTotal(vo.id);
                this._progLabel.text = starAchieved + "/" + starTotal;
                int[] stringToInt = StringUtils.GetStringToInt(vo.star_award_list);
                float x = this._sliderFg.transform.localPosition.x;
                for (int i = 0; i < this._boxes.Count; i++)
                {
                    if (i < stringToInt.Length)
                    {
                        SysDungeonStarAwardVo dungeonStarAwardVo = BaseDataMgr.instance.GetDungeonStarAwardVo((uint) stringToInt[i]);
                        this._boxes[i].SetActive(true);
                        float num5 = ((float) dungeonStarAwardVo.star) / ((float) starTotal);
                        if (num5 > 1f)
                        {
                            num5 = 1f;
                        }
                        float num6 = (this._sliderFg.width * num5) - (this._sliderFg.width / 2);
                        this._boxes[i].SetX(x + num6);
                        this._boxes[i].UpdateView(dungeonStarAwardVo, starAchieved, starTotal);
                    }
                    else
                    {
                        this._boxes[i].SetActive(false);
                    }
                }
                this._slider.value = ((float) starAchieved) / ((float) starTotal);
            }
        }

        private void UpdateView()
        {
            this.updateDungeonList();
            this.UpdateStarAwardList();
            this.updateSpriteName(this.eliteSprite, this.eliteLabel, false);
            this.updateSpriteName(this.normalSprite, this.nomalLabel, false);
            switch (this._curDungeonType)
            {
                case 1:
                    this.updateSpriteName(this.normalSprite, this.nomalLabel, true);
                    break;

                case 2:
                    this.updateSpriteName(this.eliteSprite, this.eliteLabel, true);
                    break;
            }
        }

        public Button CloseBtn
        {
            get
            {
                return this.close_btn;
            }
        }

        public List<DungeonChapterItemView> Items
        {
            get
            {
                return this._items;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }
    }
}

