﻿namespace com.game.module.Dungeon
{
    using com.game;
    using com.game.module.core;
    using com.game.module.map.monster;
    using System;
    using System.Collections.Generic;

    public class DungeonChecker
    {
        private Dictionary<int, Func<bool>> _endCheckDic = new Dictionary<int, Func<bool>>();
        private static DungeonChecker _inst;

        public DungeonChecker()
        {
            this._endCheckDic.Add(1, new Func<bool>(this.CheckNormal));
            this._endCheckDic.Add(2, new Func<bool>(this.CheckMob));
            this._endCheckDic.Add(3, new Func<bool>(this.CheckManualMob));
        }

        private bool CheckManualMob()
        {
            return Singleton<MTaskMgr>.Instance.mainTask.isFinished;
        }

        private bool CheckMob()
        {
            if (Singleton<MTaskMgr>.Instance.mainTask.isFinished)
            {
                return true;
            }
            int num = AppMap.Instance.GetNumOfMonster(0, false, false, false);
            int num2 = Singleton<MonsterMgr>.Instance.GetNumOfMonster(0, false, false, false);
            if ((num <= 0) && (num2 <= 0))
            {
                MonsterMobCache mobCache = Singleton<MonsterActivator>.Instance.mobCache;
                if (mobCache.numMonster > 0)
                {
                    mobCache.NextStage();
                }
                else
                {
                    mobCache.curStage = mobCache.maxStage;
                }
            }
            return false;
        }

        private bool CheckNormal()
        {
            return Singleton<MTaskMgr>.Instance.mainTask.isFinished;
        }

        public bool CheckOver()
        {
            int curFightType = DungeonMgr.Instance.curFightType;
            if (!this._endCheckDic.ContainsKey(curFightType))
            {
                return false;
            }
            return this._endCheckDic[curFightType]();
        }

        public static DungeonChecker Instance
        {
            get
            {
                if (_inst == null)
                {
                    _inst = new DungeonChecker();
                }
                return _inst;
            }
        }
    }
}

