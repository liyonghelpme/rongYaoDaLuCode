﻿namespace com.game.module.Dungeon
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using com.u3d.bases.display.character;
    using Proto;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class DungeonControl : BaseControl<DungeonControl>
    {
        private void dungeonAward(INetData data)
        {
            DuplicateDupAwardMsg_23_5 g__ = new DuplicateDupAwardMsg_23_5();
            g__.read(data.GetMemoryStream());
            DungeonMgr.Instance.SetSuccess(1 == g__.type);
            if ((DungeonMgr.Instance.curDungeonSysVo != null) && (DungeonMgr.Instance.curDungeonSysVo.chapter_id <= 0))
            {
                Singleton<DungeonMode>.Instance.quitDungeon();
            }
            else
            {
                switch (g__.type)
                {
                    case 1:
                        Singleton<DungeonAwardView>.Instance.Show(g__.awardList);
                        break;

                    case 2:
                        Singleton<DungeonFailureView>.Instance.OpenView();
                        break;

                    default:
                        this.PrintDungeonResult();
                        Singleton<DungeonMode>.Instance.quitDungeon();
                        break;
                }
            }
        }

        private void dungeonList(INetData data)
        {
            DuplicateDupInfoMsg_23_1 g__ = new DuplicateDupInfoMsg_23_1();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<DungeonMode>.Instance.setDungeonList(g__.dupList);
            }
        }

        private void dungeonSweep(INetData data)
        {
            DuplicateDupMopUpMsg_23_9 g__ = new DuplicateDupMopUpMsg_23_9();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<DungeonSweepView>.Instance.Show(g__.dropList);
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void enterDungeon(INetData data)
        {
            DuplicateEnterMsg_23_2 g__ = new DuplicateEnterMsg_23_2();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                DungeonMgr.Instance.EnterDungeon(g__.dupId, g__.missionId);
                Singleton<MapMode>.Instance.changeScene_4_2();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
                if (DungeonMgr.Instance.isInDungeon)
                {
                    Singleton<DungeonMode>.Instance.quitDungeon();
                }
            }
        }

        private void enterMission(INetData data)
        {
            DuplicateEnterMissionMsg_23_3 g__ = new DuplicateEnterMissionMsg_23_3();
            g__.read(data.GetMemoryStream());
            switch (g__.code)
            {
                case 0:
                {
                    if (g__.missionId != 0)
                    {
                        DungeonMgr.Instance.EnterMission(g__.missionId);
                    }
                    Dictionary<ulong, GeneralFightInfo> generalsFightAttrList = Singleton<GeneralMode>.Instance.generalsFightAttrList;
                    foreach (PlayerDisplay display in AppMap.Instance.SelfplayerList)
                    {
                        generalsFightAttrList[display.GetVo().Id].attr.hp = display.GetVo().CurHp;
                    }
                    Singleton<MapMode>.Instance.changeScene_4_2();
                    break;
                }
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("5889", new NetMsgCallback(this.dungeonList));
            AppNet.main.addCMD("5890", new NetMsgCallback(this.enterDungeon));
            AppNet.main.addCMD("5891", new NetMsgCallback(this.enterMission));
            AppNet.main.addCMD("5892", new NetMsgCallback(this.quitDungeon));
            AppNet.main.addCMD("5893", new NetMsgCallback(this.dungeonAward));
            AppNet.main.addCMD("5894", new NetMsgCallback(this.pauseDungeon));
            AppNet.main.addCMD("5895", new NetMsgCallback(this.playDungeon));
            AppNet.main.addCMD("5897", new NetMsgCallback(this.dungeonSweep));
            AppNet.main.addCMD("5899", new NetMsgCallback(this.OnInitChapterAward));
            AppNet.main.addCMD("5900", new NetMsgCallback(this.OnGetChapterStarAward));
            AppNet.main.addCMD("5901", new NetMsgCallback(this.OnBuyDungeonCount));
        }

        private void OnBuyDungeonCount(INetData data)
        {
            DuplicateBuyDupCountMsg_23_13 g__ = new DuplicateBuyDupCountMsg_23_13();
            g__.read(data.GetMemoryStream());
            if (g__.code > 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.ShowLang("Dungeon.SucToBuyEnterCount");
            }
        }

        private void OnGetChapterStarAward(INetData data)
        {
            DuplicateGetDupStarAwardMsg_23_12 g__ = new DuplicateGetDupStarAwardMsg_23_12();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                MessageManager.Show("成功领取章节星级奖励！");
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void OnInitChapterAward(INetData data)
        {
            DuplicateDupStarAwardInfoMsg_23_11 awardMsg = new DuplicateDupStarAwardInfoMsg_23_11();
            awardMsg.read(data.GetMemoryStream());
            if (awardMsg.code == 0)
            {
                Singleton<DungeonMode>.Instance.SetChapterStarAwards(awardMsg);
            }
            else
            {
                ErrorCodeManager.ShowError(awardMsg.code);
            }
        }

        private void pauseDungeon(INetData data)
        {
            DuplicateDupPauseMsg_23_6 g__ = new DuplicateDupPauseMsg_23_6();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                DungeonMgr.Instance.Pause();
            }
        }

        private void playDungeon(INetData data)
        {
            DuplicateDupResumeMsg_23_7 g__ = new DuplicateDupResumeMsg_23_7();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                DungeonMgr.Instance.Resume();
            }
        }

        public void PrintDungeonResult()
        {
        }

        private void quitDungeon(INetData data)
        {
            DuplicateQuitDupMsg_23_4 g__ = new DuplicateQuitDupMsg_23_4();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Time.timeScale = 1f;
                Singleton<DungeonChapterView>.Instance.CloseView();
                AppMap.Instance.me.Controller.ComboHitMgr.ClearWhenQuitDup();
                DungeonMgr.Instance.ExitDungeon(false);
                Singleton<MapMode>.Instance.changeScene_4_2();
            }
        }
    }
}

