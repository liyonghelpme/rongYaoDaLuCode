﻿namespace com.game.module.Dungeon
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class DungeonFailureView : BaseView<DungeonFailureView>
    {
        private int _lastCloseType;
        private int _lastDungeonId;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$mapD;
        private Button angin;
        private Button change;
        private Button quit;
        private Button streng;
        private Button updateLv;

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.gameObject.SetActive(true);
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
            this.gameObject.SetActive(false);
        }

        protected override void Init()
        {
            this.quit = base.FindInChild<Button>("quit");
            this.change = base.FindInChild<Button>("list/change");
            this.updateLv = base.FindInChild<Button>("list/updateLv");
            this.streng = base.FindInChild<Button>("list/streng");
            this.angin = base.FindInChild<Button>("angin");
            this.gameObject.SetActive(false);
            this.quit.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.change.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.updateLv.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.streng.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.angin.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void OnBackToMainCity(int type, int v1, int v2, object data)
        {
            GlobalAPI.facade.Remove(type, new NoticeListener(this.OnBackToMainCity));
            switch (this._lastCloseType)
            {
                case 0:
                    Singleton<DungeonInfoView>.Instance.Show(this._lastDungeonId);
                    break;

                case 1:
                    Singleton<DungeonChapterView>.Instance.Show(this._lastDungeonId);
                    break;

                case 2:
                    Singleton<GeneralPanel>.Instance.OpenView();
                    break;

                case 3:
                    Singleton<ExpDrugPanel>.Instance.OpenView();
                    break;

                case 4:
                    Singleton<EquipmentMainPanel>.Instance.OpenEquipment(null, 0);
                    break;
            }
        }

        private void onClickHandler(GameObject go)
        {
            this.CloseView();
            Singleton<DungeonMode>.Instance.quitDungeon();
            GlobalAPI.facade.Add(0x10, new NoticeListener(this.OnBackToMainCity));
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$mapD == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(5);
                    dictionary.Add("quit", 0);
                    dictionary.Add("change", 1);
                    dictionary.Add("updateLv", 2);
                    dictionary.Add("streng", 3);
                    dictionary.Add("angin", 4);
                    <>f__switch$mapD = dictionary;
                }
                if (<>f__switch$mapD.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            this._lastCloseType = 1;
                            this._lastDungeonId = DungeonMgr.Instance.curDungeonId;
                            break;

                        case 1:
                            this._lastCloseType = 2;
                            break;

                        case 2:
                            this._lastCloseType = 3;
                            break;

                        case 3:
                            this._lastCloseType = 4;
                            break;

                        case 4:
                            this._lastCloseType = 0;
                            this._lastDungeonId = DungeonMgr.Instance.curDungeonId;
                            break;
                    }
                }
            }
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public Button QuipBtn
        {
            get
            {
                return this.quit;
            }
        }
    }
}

