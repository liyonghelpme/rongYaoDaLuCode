﻿namespace com.game.module.Dungeon
{
    using System;

    public class DungeonFightType
    {
        public const int MANUAL_MOB = 3;
        public const int MOB = 2;
        public const int NORMAL = 1;
    }
}

