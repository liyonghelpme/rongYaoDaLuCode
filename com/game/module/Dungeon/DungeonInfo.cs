﻿namespace com.game.module.Dungeon
{
    using com.game.data;
    using PCustomDataType;
    using System;

    public class DungeonInfo : IComparable
    {
        public SysDungeonVo dungeon;
        public PDuplicateInfo duplicateInfo;

        public int CompareTo(object obj)
        {
            if (!(obj is DungeonInfo))
            {
                throw new NotImplementedException("objisnotaStudent!");
            }
            DungeonInfo info = obj as DungeonInfo;
            return this.dungeon.id.CompareTo(info.dungeon.id);
        }

        public int leftEnterCount
        {
            get
            {
                if (2 != this.dungeon.subtype)
                {
                    return 0x7fffffff;
                }
                return (this.dungeon.day_times - ((int) this.duplicateInfo.count));
            }
        }
    }
}

