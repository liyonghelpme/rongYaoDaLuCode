﻿namespace com.game.module.Dungeon
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.module.Equipment.components;
    using com.game.Public.Message;
    using com.game.utils;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class DungeonInfoView : BaseView<DungeonInfoView>
    {
        private List<SysItemVoCell> _awardList = new List<SysItemVoCell>();
        private UIWidgetContainer _bgHot;
        private Button _btnBuy;
        private UILabel _countLabel;
        private int _dungeonId;
        private DungeonInfo _dungeonInfo;
        private SysDungeonVo _dungeonSysVo;
        private int _enterVigour;
        private UIGrid _grid;
        private GameObject _itemGo;
        private GameObject _itemGridGo;
        private MainToolView _mainToolBarView;
        private int _numDungeonNeed;
        private UIScrollView _scrollView;
        private int _sweepVigour;
        private SysDungeonMissionVo _sysMissionVo;
        private List<UILabel> _taskList;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$mapE;
        private Button about_btn;
        private Button back_btn;
        private Dictionary<byte, UISprite> headIconImageList = new Dictionary<byte, UISprite>();
        private UILabel name;
        private Button start_btn;
        private Button sweep_10_btn;
        private Button sweep_btn;

        private void DoOpen()
        {
            Singleton<DungeonChapterView>.Instance.OpenView();
            base.OpenView();
        }

        protected override void HandleAfterOpenView()
        {
            this._mainToolBarView.isActive = true;
            this._bgHot.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickEmpty);
            this._scrollView.ResetPosition();
            this.UpdateView();
            this.start_btn.label.text = "闯关" + this._enterVigour;
            this.sweep_btn.label.text = "扫荡" + this._sweepVigour;
            Singleton<DungeonMode>.Instance.dataUpdated += new DataUpateHandler(this.OnDungeonEvent);
        }

        protected override void HandleBeforeCloseView()
        {
            this._mainToolBarView.isActive = false;
            this._bgHot.onClick = null;
            Singleton<DungeonMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnDungeonEvent);
        }

        protected override void Init()
        {
            this._bgHot = base.FindInChild<UIWidgetContainer>("mask");
            this._mainToolBarView = new MainToolView(base.FindChild("toolbar"));
            this._itemGridGo = base.FindChild("infos/info2/award/award_list");
            this._itemGo = base.FindChild("infos/info2/award/award_list/item");
            this._grid = this._itemGridGo.GetComponent<UIGrid>();
            this.back_btn = base.FindInChild<Button>("back");
            this.start_btn = base.FindInChild<Button>("operation/start");
            this.sweep_btn = base.FindInChild<Button>("operation/sweep");
            this.sweep_10_btn = base.FindInChild<Button>("operation/sweep_10");
            this._btnBuy = base.FindInChild<Button>("operation/btnBuy");
            this._countLabel = base.FindInChild<UILabel>("operation/label");
            this.about_btn = base.FindInChild<Button>("about");
            this.name = base.FindInChild<UILabel>("name");
            this._scrollView = base.FindInChild<UIScrollView>("infos/info2/award");
            this.back_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.start_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.sweep_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.sweep_10_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.about_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this._btnBuy.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this._taskList = new List<UILabel>();
            for (int i = 1; i <= 3; i++)
            {
                string path = "infos/info1/item" + i + "/tips";
                UILabel item = base.FindInChild<UILabel>(path);
                this._taskList.Add(item);
            }
        }

        private void InitTaskList()
        {
            bool active = null != this._sysMissionVo;
            this.start_btn.SetActive(active);
            this.sweep_btn.SetActive(active);
            this.sweep_10_btn.SetActive(active && (1 == this._dungeonSysVo.subtype));
            if (!active)
            {
                foreach (UILabel label in this._taskList)
                {
                    label.transform.parent.gameObject.SetActive(false);
                }
                this.name.text = string.Empty;
            }
            else
            {
                this.name.text = this._sysMissionVo.name;
                int[] stringToInt = StringUtils.GetStringToInt(this._sysMissionVo.dungeon_task);
                for (int i = this._taskList.Count - 1; i >= 0; i--)
                {
                    UILabel label2 = this._taskList[i];
                    if (i >= stringToInt.Length)
                    {
                        label2.transform.parent.gameObject.SetActive(false);
                    }
                    else
                    {
                        label2.transform.parent.gameObject.SetActive(true);
                        SysDungeonTaskVo dungeonTaskVo = BaseDataMgr.instance.GetDungeonTaskVo((uint) stringToInt[i]);
                        if (dungeonTaskVo == null)
                        {
                            label2.text = "副本任务读不到：" + stringToInt[i];
                        }
                        else
                        {
                            label2.text = dungeonTaskVo.content;
                        }
                    }
                }
            }
        }

        private void OnClickEmpty(GameObject go)
        {
            this.CloseView();
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$mapE == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(6);
                    dictionary.Add("about", 0);
                    dictionary.Add("back", 1);
                    dictionary.Add("start", 2);
                    dictionary.Add("sweep", 3);
                    dictionary.Add("sweep_10", 4);
                    dictionary.Add("btnBuy", 5);
                    <>f__switch$mapE = dictionary;
                }
                if (<>f__switch$mapE.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            Singleton<DungeonRuleView>.Instance.OpenView();
                            break;

                        case 1:
                            this.CloseView();
                            break;

                        case 2:
                            if ((this._dungeonInfo == null) || (this._dungeonInfo.leftEnterCount > 0))
                            {
                                if (MeVo.instance.vigour < this._enterVigour)
                                {
                                    Singleton<BuyVigourView>.Instance.Show();
                                    return;
                                }
                                if (Singleton<GeneralMode>.Instance.GetDefaultFightGeneralList.Count < this._numDungeonNeed)
                                {
                                    MessageManager.Show(LanguageManager.GetWord("Dungeon.NeedMoreHeros", this._numDungeonNeed.ToString()));
                                    return;
                                }
                                this.CloseView();
                                Singleton<DungeonMode>.Instance.enterDungeon(this._dungeonId);
                                break;
                            }
                            MessageManager.Show(LanguageManager.GetWord("Dungeon.DungeonCountOut"));
                            return;

                        case 3:
                            if ((this._dungeonInfo == null) || (!VarMgr.VipSweep && (this._dungeonInfo.duplicateInfo.grade < 3)))
                            {
                                MessageManager.Show(LanguageManager.GetWord("Dungeon.SweepLimit"));
                                return;
                            }
                            if (MeVo.instance.vigour < this._sweepVigour)
                            {
                                Singleton<BuyVigourView>.Instance.Show();
                                return;
                            }
                            Singleton<DungeonMode>.Instance.dungeonSweep(this._dungeonId, 1);
                            break;

                        case 4:
                            if ((this._dungeonInfo == null) || (!VarMgr.VipSweep10 && (this._dungeonInfo.duplicateInfo.grade < 3)))
                            {
                                MessageManager.Show(LanguageManager.GetWord("Dungeon.SweepLimit"));
                                return;
                            }
                            if (MeVo.instance.vigour < (this._sweepVigour * 10))
                            {
                                Singleton<BuyVigourView>.Instance.Show();
                                return;
                            }
                            Singleton<DungeonMode>.Instance.dungeonSweep(this._dungeonId, 10);
                            break;

                        case 5:
                        {
                            <onClickHandler>c__AnonStoreyD7 yd = new <onClickHandler>c__AnonStoreyD7 {
                                <>f__this = this
                            };
                            if (this._dungeonInfo != null)
                            {
                                yd.priceVo = BaseDataMgr.instance.GetSysPriceVo(0x44d, (uint) (this._dungeonInfo.duplicateInfo.buyCount + 1));
                                string[] param = new string[] { yd.priceVo.diam.ToString(), this._dungeonInfo.duplicateInfo.buyCount.ToString(), VarMgr.numBuyEnterEliteDugeon.ToString() };
                                Singleton<AlertPanel>.Instance.Show(LanguageManager.GetWord("Dungeon.SureToBuyEnterCount", param), new SureCallback(yd.<>m__67), null);
                                break;
                            }
                            MessageManager.Show(LanguageManager.GetWord("Dungeon.PassDungeonFirst"));
                            return;
                        }
                    }
                }
            }
        }

        private void OnDungeonEvent(object obj, int code)
        {
            this.UpdateView();
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.DoOpen));
            if (Singleton<DungeonChapterView>.Instance.IsOpened && (null != Singleton<DungeonChapterView>.Instance.gameObject))
            {
                this.DoOpen();
            }
        }

        public void Show(int dungeonId)
        {
            this._dungeonId = dungeonId;
            this._dungeonSysVo = BaseDataMgr.instance.getDungeon((uint) this._dungeonId);
            int[] stringToInt = StringUtils.GetStringToInt(this._dungeonSysVo.mission_list);
            this._sysMissionVo = BaseDataMgr.instance.getDungeonMission((uint) stringToInt[0]);
            this._numDungeonNeed = (StringUtils.GetArrayStringToFloat(this._sysMissionVo.frozen_points).Length / 4) + 1;
            this._enterVigour = this._dungeonSysVo.need_vigour + this._dungeonSysVo.pre_vigour;
            this._sweepVigour = this._enterVigour;
            this.OpenView();
        }

        private void UpdateAwardList()
        {
            int[] stringToInt = StringUtils.GetStringToInt(this._dungeonSysVo.award_list);
            int index = 0;
            while (index < stringToInt.Length)
            {
                SysItemVoCell cell;
                int num2 = stringToInt[index];
                if (index >= this._awardList.Count)
                {
                    GameObject obj2;
                    if (this._awardList.Count <= 0)
                    {
                        obj2 = this._itemGo;
                    }
                    else
                    {
                        obj2 = NGUITools.AddChild(this._itemGridGo, this._itemGo);
                    }
                    cell = obj2.AddMissingComponent<SysItemVoCell>();
                    this._awardList.Add(cell);
                }
                else
                {
                    cell = this._awardList[index];
                }
                cell.SetActive(true);
                cell.sysVoId = num2;
                cell.count = 1;
                index++;
            }
            for (int i = this._awardList.Count - 1; i >= index; i--)
            {
                this._awardList[i].SetActive(false);
                if (i > 0)
                {
                    UnityEngine.Object.Destroy(this._awardList[i].gameObject);
                    this._awardList.RemoveAt(i);
                }
            }
            this._grid.Reposition();
        }

        private void UpdateView()
        {
            this._dungeonInfo = Singleton<DungeonMode>.Instance.getDungeonInfo((uint) this._dungeonId);
            this.InitTaskList();
            this.UpdateAwardList();
            if (2 == this._dungeonSysVo.subtype)
            {
                this._countLabel.SetActive(true);
                this._btnBuy.SetActive((this._dungeonInfo != null) && (this._dungeonInfo.leftEnterCount <= 0));
                if (this._dungeonInfo != null)
                {
                    this._countLabel.text = LanguageManager.GetWord("Dungeon.CanEnterCount", this._dungeonInfo.leftEnterCount.ToString());
                }
                else
                {
                    this._countLabel.text = LanguageManager.GetWord("Dungeon.CanEnterCount", this._dungeonSysVo.day_times.ToString());
                }
            }
            else
            {
                this._btnBuy.SetActive(false);
                this._countLabel.SetActive(false);
            }
        }

        public Button BackButton
        {
            get
            {
                return this.back_btn;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public Button StartButton
        {
            get
            {
                return this.start_btn;
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }

        [CompilerGenerated]
        private sealed class <onClickHandler>c__AnonStoreyD7
        {
            private static SureCallback <>f__am$cache2;
            internal DungeonInfoView <>f__this;
            internal SysPriceVo priceVo;

            internal void <>m__67()
            {
                if (this.<>f__this._dungeonInfo.leftEnterCount > 0)
                {
                    MessageManager.Show("用完挑战次数才可以购买！");
                }
                else if (this.<>f__this._dungeonInfo.duplicateInfo.buyCount >= VarMgr.numBuyEnterEliteDugeon)
                {
                    MessageManager.Show(LanguageManager.GetWord("Dungeon.BuyCountOut"));
                }
                else if (MeVo.instance.diamond < this.priceVo.diam)
                {
                    if (<>f__am$cache2 == null)
                    {
                        <>f__am$cache2 = () => MessageManager.Show("充值未开放！");
                    }
                    Singleton<AlertPanel>.Instance.Show(LanguageManager.GetWord("Game.SureToFill"), <>f__am$cache2, null);
                }
                else
                {
                    Singleton<DungeonMode>.Instance.BuyEnterCount(this.<>f__this._dungeonInfo.duplicateInfo.dupId);
                }
            }

            private static void <>m__68()
            {
                MessageManager.Show("充值未开放！");
            }
        }
    }
}

