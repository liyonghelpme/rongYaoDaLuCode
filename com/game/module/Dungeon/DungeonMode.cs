﻿namespace com.game.module.Dungeon
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.utils;
    using com.game.vo;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Security.Cryptography;
    using System.Text;
    using UnityEngine;

    public class DungeonMode : BaseMode<DungeonMode>
    {
        private Dictionary<int, DungeonInfo> _allDungeonDic = new Dictionary<int, DungeonInfo>();
        private Dictionary<uint, Dictionary<int, SysDungeonStarAwardVo>> _chapter_num_vo = new Dictionary<uint, Dictionary<int, SysDungeonStarAwardVo>>();
        private static uint _firstEliteChapterId;
        private Dictionary<uint, PDupStarAward> _starAwards = new Dictionary<uint, PDupStarAward>();
        private Dictionary<int, List<DungeonInfo>> _type2ListDic = new Dictionary<int, List<DungeonInfo>>();
        public const uint FirstEliteDungeonId = 0x2719;
        public const uint FirstNormalChapterId = 0x2711;
        public const uint FirstNormalDungeonId = 0x2711;
        public bool isUpdate;
        public const int UPDATE_STAR_AWARD = 2;
        public const int UPDATELIST = 1;

        public void BuyEnterCount(uint id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_13(msdata, id);
            AppNet.gameNet.send(msdata, 0x17, 13);
        }

        public bool CanEnterDungeon(uint dungeonId)
        {
            SysDungeonVo vo = BaseDataMgr.instance.getDungeon(dungeonId);
            if (vo == null)
            {
                return false;
            }
            if ((vo.subtype == 2) && !this.HasPassedChapter(vo.chapter_id - 1))
            {
                return false;
            }
            if (vo.pre_id <= 0)
            {
                return true;
            }
            DungeonInfo info = this.getDungeonInfo((uint) vo.pre_id);
            return ((info != null) && (info.duplicateInfo.grade > 0));
        }

        public void dungeonSweep(int dungeonId, int count)
        {
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_9(msdata, (ushort) dungeonId, (byte) count);
            AppNet.gameNet.send(msdata, 0x17, 9);
        }

        public void EndDungeon()
        {
            DungeonMgr instance = DungeonMgr.Instance;
            string dungeonCheckKey = this.GetDungeonCheckKey();
            string checkKey = this.GetMD5(dungeonCheckKey, true);
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_10(msdata, checkKey, (uint) DungeonMgr.Instance.curDungeonId, !instance.success ? ((byte) 0) : ((byte) 1), (uint) instance.time, instance.numStar, instance.numMonsterKilled, instance.chestIds);
            AppNet.gameNet.send(msdata, 0x17, 10);
        }

        public void enterDungeon(int dungeonId)
        {
            if (!this.CanEnterDungeon((uint) dungeonId))
            {
                MessageManager.Show("无法挑战该副本，请先通关前面的副本!");
            }
            else if (DungeonMgr.Instance.isInDungeon && !DungeonMgr.Instance.isOver)
            {
                MessageManager.Show("当前副本还未结束!");
            }
            else
            {
                MemoryStream msdata = new MemoryStream();
                int x = (int) (AppMap.Instance.me.Controller.transform.position.x * 1000f);
                int y = (int) (AppMap.Instance.me.Controller.transform.position.y * 1000f);
                int z = (int) (AppMap.Instance.me.Controller.transform.position.z * 1000f);
                Module_23.write_23_2(msdata, (uint) dungeonId, x, y, z);
                AppNet.gameNet.send(msdata, 0x17, 2);
            }
        }

        public void enterDungeonCreateRole(int dungeonId = 0x986f71)
        {
            MemoryStream msdata = new MemoryStream();
            int x = MeVo.instance.X.ToProtocolInt();
            int y = MeVo.instance.Y.ToProtocolInt();
            int z = MeVo.instance.Z.ToProtocolInt();
            Module_23.write_23_2(msdata, (uint) dungeonId, x, y, z);
            AppNet.gameNet.send(msdata, 0x17, 2);
        }

        public void enterMission()
        {
            this.isUpdate = true;
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_3(msdata);
            AppNet.gameNet.send(msdata, 0x17, 3);
        }

        public string GetDebugDungeonCheckKey(int star)
        {
            if (star > 3)
            {
                star = 3;
            }
            else if (star < 0)
            {
                star = 0;
            }
            bool flag = star > 0;
            DungeonMgr instance = DungeonMgr.Instance;
            List<ushort> chestIds = instance.chestIds;
            string str = ((string.Empty + GlobalData.sign + DungeonMgr.Instance.curDungeonId) + (!flag ? 0 : 1) + ((uint) instance.time)) + star + instance.numMonsterKilled;
            chestIds.Sort();
            foreach (ushort num in chestIds)
            {
                Debug.Log("宝箱ID:" + num);
                str = str + num;
            }
            return str;
        }

        public string GetDungeonCheckKey()
        {
            string str = string.Empty;
            DungeonMgr instance = DungeonMgr.Instance;
            List<ushort> chestIds = instance.chestIds;
            str = ((str + GlobalData.sign + DungeonMgr.Instance.curDungeonId) + (!instance.success ? 0 : 1) + ((uint) instance.time)) + instance.numStar + instance.numMonsterKilled;
            chestIds.Sort();
            foreach (ushort num in chestIds)
            {
                Debug.Log("宝箱ID:" + num);
                str = str + num;
            }
            return str;
        }

        public DungeonInfo getDungeonInfo(uint id)
        {
            int key = (int) id;
            if ((this._allDungeonDic != null) && this._allDungeonDic.ContainsKey(key))
            {
                return this._allDungeonDic[key];
            }
            return null;
        }

        public void getDungeonList()
        {
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_1(msdata);
            AppNet.gameNet.send(msdata, 0x17, 1);
        }

        public DungeonInfo GetLastDungeonInfo(int dungeonType)
        {
            if (this._type2ListDic.ContainsKey(dungeonType))
            {
                List<DungeonInfo> list = this._type2ListDic[dungeonType];
                if (list.Count <= 0)
                {
                    return null;
                }
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i].dungeon.chapter_id > 0)
                    {
                        return list[i];
                    }
                }
            }
            return null;
        }

        public string GetMD5(string key, bool toUpper)
        {
            byte[] buffer = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(key));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < buffer.Length; i++)
            {
                builder.Append(buffer[i].ToString("x2"));
            }
            string str = builder.ToString();
            if (toUpper)
            {
                str = str.ToUpper();
            }
            return str;
        }

        public int GetStarAchieved(int chapterId)
        {
            int num = 0;
            foreach (DungeonInfo info in this._allDungeonDic.Values)
            {
                if ((chapterId == 0) || (info.dungeon.chapter_id == chapterId))
                {
                    num += info.duplicateInfo.grade;
                }
            }
            return num;
        }

        public void GetStarAward(uint starAwardSysVoId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_12(msdata, starAwardSysVoId);
            AppNet.gameNet.send(msdata, 0x17, 12);
        }

        public int GetStarTotal(int chapterId)
        {
            SysDungeonChapterVo vo = BaseDataMgr.instance.getDungeonChapter((uint) chapterId);
            if (vo == null)
            {
                return 0;
            }
            int[] stringToInt = StringUtils.GetStringToInt(vo.dungeon_list);
            int num = 0;
            foreach (int num2 in stringToInt)
            {
                int[] numArray3 = StringUtils.GetStringToInt(BaseDataMgr.instance.getDungeon((uint) num2).mission_list);
                int[] numArray4 = StringUtils.GetStringToInt(BaseDataMgr.instance.getDungeonMission((uint) numArray3[0]).dungeon_task);
                num += numArray4.Length;
            }
            return num;
        }

        public bool HasGetStarAward(uint starAwardId)
        {
            return this._starAwards.ContainsKey(starAwardId);
        }

        public bool HasPassedChapter(int chapterId)
        {
            SysDungeonChapterVo vo = BaseDataMgr.instance.getDungeonChapter((uint) chapterId);
            if (vo == null)
            {
                return false;
            }
            int[] stringToInt = StringUtils.GetStringToInt(vo.dungeon_list);
            return ((stringToInt.Length == 0) || ((this._allDungeonDic != null) && this._allDungeonDic.ContainsKey(stringToInt[stringToInt.Length - 1])));
        }

        private int IndexOf(DungeonInfo dungeonInfo)
        {
            if (this._type2ListDic.ContainsKey(dungeonInfo.dungeon.subtype))
            {
                List<DungeonInfo> list = this._type2ListDic[dungeonInfo.dungeon.subtype];
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i].duplicateInfo.dupId == dungeonInfo.duplicateInfo.dupId)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public bool IsChapterOpened(int chapterId)
        {
            if (chapterId != 0x2711L)
            {
                SysDungeonChapterVo vo = BaseDataMgr.instance.getDungeonChapter((uint) chapterId);
                if ((vo.type == 2) && !this.HasPassedChapter(vo.id - 1))
                {
                    return false;
                }
                if (!this.HasPassedChapter(vo.id - 2))
                {
                    return false;
                }
            }
            return true;
        }

        public void pauseDungeon()
        {
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_6(msdata);
            AppNet.gameNet.send(msdata, 0x17, 6);
        }

        public void playDungeon()
        {
        }

        public void QuickEndDungeon(int star)
        {
        }

        public void quitDungeon()
        {
            Time.timeScale = 1f;
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_4(msdata);
            AppNet.gameNet.send(msdata, 0x17, 4);
        }

        public void ReqChapterStarAwardInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_23.write_23_11(msdata);
            AppNet.gameNet.send(msdata, 0x17, 11);
        }

        public void SetChapterStarAwards(DuplicateDupStarAwardInfoMsg_23_11 awardMsg)
        {
            foreach (PDupStarAward award in awardMsg.stateList)
            {
                if (award.state != 0)
                {
                    if (!this._starAwards.ContainsKey(award.id))
                    {
                        this._starAwards.Add(award.id, award);
                    }
                    else
                    {
                        this._starAwards[award.id] = award;
                    }
                }
            }
            base.DataUpdate(2);
        }

        public void setDungeonList(List<PDuplicateInfo> duplicateInfoList)
        {
            if (duplicateInfoList.Count > 0)
            {
                foreach (PDuplicateInfo info in duplicateInfoList)
                {
                    List<DungeonInfo> list;
                    DungeonInfo info2 = new DungeonInfo {
                        dungeon = BaseDataMgr.instance.getDungeon(info.dupId),
                        duplicateInfo = info
                    };
                    if (this._allDungeonDic.ContainsKey(info2.dungeon.id))
                    {
                        this._allDungeonDic[info2.dungeon.id] = info2;
                    }
                    else
                    {
                        this._allDungeonDic.Add(info2.dungeon.id, info2);
                    }
                    if (this._type2ListDic.ContainsKey(info2.dungeon.subtype))
                    {
                        list = this._type2ListDic[info2.dungeon.subtype];
                        int index = this.IndexOf(info2);
                        if (index >= 0)
                        {
                            list.RemoveAt(index);
                            list.Add(info2);
                        }
                        else
                        {
                            list.Add(info2);
                        }
                    }
                    else
                    {
                        list = new List<DungeonInfo>();
                        this._type2ListDic.Add(info2.dungeon.subtype, list);
                        list.Add(info2);
                    }
                }
                foreach (List<DungeonInfo> list2 in this._type2ListDic.Values)
                {
                    list2.Sort();
                }
            }
            base.DataUpdate(1);
        }

        public static uint FirstEliteChapterId
        {
            get
            {
                if (_firstEliteChapterId == 0)
                {
                    _firstEliteChapterId = 0x2712;
                    while (BaseDataMgr.instance.getDungeonChapter(_firstEliteChapterId) == null)
                    {
                        _firstEliteChapterId += 2;
                    }
                }
                return _firstEliteChapterId;
            }
        }
    }
}

