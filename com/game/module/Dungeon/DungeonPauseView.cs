﻿namespace com.game.module.Dungeon
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class DungeonPauseView : BaseView<DungeonPauseView>
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$mapF;
        private Button angin_btn;
        private Button back_btn;
        private Button quit_btn;

        public override void CancelUpdateHandler()
        {
            base.CancelUpdateHandler();
        }

        public override void DataUpdated(object sender, int code)
        {
            base.DataUpdated(sender, code);
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.gameObject.SetActive(true);
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
            this.gameObject.SetActive(false);
        }

        protected override void Init()
        {
            this.angin_btn = base.FindInChild<Button>("angin");
            this.quit_btn = base.FindInChild<Button>("quit");
            this.back_btn = base.FindInChild<Button>("back");
            this.angin_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHnadler);
            this.quit_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHnadler);
            this.back_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHnadler);
            this.gameObject.SetActive(false);
        }

        private void onClickHnadler(GameObject go)
        {
            Singleton<DungeonChapterView>.Instance.CloseView();
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$mapF == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                    dictionary.Add("angin", 0);
                    dictionary.Add("quit", 1);
                    dictionary.Add("back", 2);
                    <>f__switch$mapF = dictionary;
                }
                if (<>f__switch$mapF.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            Singleton<DungeonMode>.Instance.playDungeon();
                            break;

                        case 1:
                            Singleton<DungeonMode>.Instance.quitDungeon();
                            break;

                        case 2:
                            Singleton<DungeonMode>.Instance.quitDungeon();
                            break;
                    }
                }
            }
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        public override void RegisterUpdateHandler()
        {
            base.RegisterUpdateHandler();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }
    }
}

