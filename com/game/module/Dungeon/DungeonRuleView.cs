﻿namespace com.game.module.Dungeon
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class DungeonRuleView : BaseView<DungeonRuleView>
    {
        private UIWidgetContainer _bgHot;
        private BoxCollider _box;
        private Button _btnClose;
        private UILabel _label;
        private UIScrollView _scrolView;

        protected override void HandleAfterOpenView()
        {
            this._scrolView.ResetPosition();
            this._label.text = DescriptManager.GetText(1);
            Vector3 size = this._box.size;
            size.y = this._label.height;
            this._box.size = size;
            size = (Vector3) (size / -2f);
            this._box.center = size;
        }

        protected override void Init()
        {
            this._btnClose = base.FindInChild<Button>("btnClose");
            this._label = base.FindInChild<UILabel>("grid/item/label");
            this._box = base.FindInChild<BoxCollider>("grid/item");
            this._scrolView = base.FindInChild<UIScrollView>("grid");
            this._bgHot = base.FindInChild<UIWidgetContainer>("mask");
            this._bgHot.onClick = this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        private void OnClick(GameObject go)
        {
            this.CloseView();
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }
    }
}

