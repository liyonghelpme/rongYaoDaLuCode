﻿namespace com.game.module.Dungeon
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class DungeonStarAwardView : BaseView<DungeonStarAwardView>
    {
        private UIWidgetContainer _bgHot;
        private Button _btnAward;
        private Button _btnClose;
        private bool _canGet;
        private List<SysItemVoCell> _cells = new List<SysItemVoCell>();
        private UIGrid _grid;
        private GameObject _gridGo;
        private bool _hasGot;
        private GameObject _itemGo;
        private UILabel _label;
        private SysDungeonStarAwardVo _vo;

        public override void CancelUpdateHandler()
        {
            Singleton<DungeonMode>.Instance.dataUpdated -= new DataUpateHandler(this.DataUpdated);
        }

        private SysItemVoCell CloneCell(int idx)
        {
            GameObject obj2;
            if (this._cells.Count <= 0)
            {
                obj2 = this._itemGo;
            }
            else
            {
                obj2 = NGUITools.AddChild(this._gridGo, this._itemGo);
            }
            obj2.name = "item" + idx;
            return obj2.AddMissingComponent<SysItemVoCell>();
        }

        public override void DataUpdated(object sender, int code)
        {
            switch (code)
            {
                case 1:
                case 2:
                    this.HandleAfterOpenView();
                    break;
            }
        }

        protected override void HandleAfterOpenView()
        {
            SysItemVoCell cell;
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(this._vo.award);
            int num = arrayStringToInt.Length / 2;
            int idx = 0;
            while (this._cells.Count < num)
            {
                cell = this.CloneCell(idx);
                this._cells.Add(cell);
            }
            idx = 0;
            while (idx < num)
            {
                cell = this._cells[idx];
                cell.SetActive(true);
                cell.sysVoId = arrayStringToInt[2 * idx];
                cell.count = arrayStringToInt[(2 * idx) + 1];
                idx++;
            }
            while (idx < this._cells.Count)
            {
                this._cells[idx].SetActive(false);
                idx++;
            }
            this._grid.Reposition();
            this._hasGot = Singleton<DungeonMode>.Instance.HasGetStarAward(this._vo.unikey);
            int starAchieved = Singleton<DungeonMode>.Instance.GetStarAchieved(this._vo.chapter_id);
            this._canGet = starAchieved >= this._vo.star;
            this._btnAward.SetActive(!this._hasGot);
            this._label.text = LanguageManager.GetWord("Dungeon.ChapterStarAwards", this._vo.star.ToString());
        }

        protected override void HandleBeforeCloseView()
        {
            for (int i = this._cells.Count - 1; i > 0; i--)
            {
                UnityEngine.Object.Destroy(this._cells[i].gameObject);
                this._cells.RemoveAt(i);
            }
        }

        protected override void Init()
        {
            this._bgHot = base.FindInChild<UIWidgetContainer>("mask");
            this._gridGo = base.FindChild("list");
            this._itemGo = base.FindChild("list/item");
            this._btnClose = base.FindInChild<Button>("btnClose");
            this._btnAward = base.FindInChild<Button>("btnAward");
            this._grid = this._gridGo.GetComponent<UIGrid>();
            this._label = base.FindInChild<UILabel>("label");
            this._btnAward.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickAward);
            this._bgHot.onClick = this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickClose);
        }

        private void OnClickAward(GameObject go)
        {
            if (this._hasGot)
            {
                MessageManager.Show("已经领取了奖励");
            }
            else if (this._canGet)
            {
                this.CloseView();
                Singleton<DungeonMode>.Instance.GetStarAward(this._vo.unikey);
            }
            else
            {
                MessageManager.Show("你的星数不够！");
            }
        }

        private void OnClickClose(GameObject go)
        {
            this.CloseView();
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        public override void RegisterUpdateHandler()
        {
            base.RegisterUpdateHandler();
            Singleton<DungeonMode>.Instance.dataUpdated += new DataUpateHandler(this.DataUpdated);
        }

        public void Show(SysDungeonStarAwardVo vo)
        {
            this._vo = vo;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }
    }
}

