﻿namespace com.game.module.Dungeon
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class DungeonSweepView : BaseView<DungeonSweepView>
    {
        private UIWidgetContainer _bgHot;
        private Button _btnClose;
        private MulSweepView _mulSweepView;
        private SingleSweepView _singleSweepView;
        private List<PDuplicateMopUp> _sweepInfoList;

        protected override void HandleAfterOpenView()
        {
            this._singleSweepView.Close();
            this._mulSweepView.Close();
            if ((this._sweepInfoList != null) && (this._sweepInfoList.Count >= 1))
            {
                if (this._sweepInfoList.Count == 1)
                {
                    this._singleSweepView.Update(this._sweepInfoList);
                }
                else
                {
                    this._mulSweepView.Update(this._sweepInfoList);
                }
            }
        }

        protected override void HandleBeforeCloseView()
        {
            this._singleSweepView.Close();
            this._mulSweepView.Close();
        }

        protected override void Init()
        {
            this._bgHot = base.FindInChild<UIWidgetContainer>("mask");
            this._singleSweepView = new SingleSweepView(base.FindChild("sweep"));
            this._mulSweepView = new MulSweepView(base.FindChild("sweep_list"));
            this._bgHot.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
        }

        private void OnClickBtn(GameObject go)
        {
            this.CloseView();
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        public void Show(List<PDuplicateMopUp> sweepResultList)
        {
            this._sweepInfoList = sweepResultList;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }
    }
}

