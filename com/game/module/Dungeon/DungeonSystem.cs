﻿namespace com.game.module.Dungeon
{
    using com.game.module.core;
    using System;

    internal class DungeonSystem : BaseView<DungeonSystem>
    {
        public override void CloseView()
        {
        }

        protected override void Init()
        {
            Singleton<DungeonChapterView>.Instance.gameObject = base.FindChild("DungeonChapterPanel");
            Singleton<DungeonFailureView>.Instance.gameObject = base.FindChild("DungeonFailurePanel");
            Singleton<DungeonInfoView>.Instance.gameObject = base.FindChild("DungeonInfoPanel");
            Singleton<DungeonPauseView>.Instance.gameObject = base.FindChild("DungeonPausePanel");
            Singleton<DungeonRuleView>.Instance.gameObject = base.FindChild("DungeonRulePanel");
            Singleton<DungeonStarAwardView>.Instance.gameObject = base.FindChild("DungeonStarAwardPanel");
            Singleton<DungeonSweepView>.Instance.gameObject = base.FindChild("DungeonSweepPanel");
            Singleton<DungeonTaskView>.Instance.gameObject = base.FindChild("DungeonTaskView");
            Singleton<DungeonAwardView>.Instance.gameObject = base.FindChild("DungeonWinPanel");
        }

        public void RegisterOpenAction(OpenViewCallBack act)
        {
            if (null != this.gameObject)
            {
                this.gameObject.SetActive(true);
                if (act != null)
                {
                    act();
                }
            }
            else
            {
                base.OpenViewCallback = (OpenViewCallBack) Delegate.Combine(base.OpenViewCallback, act);
                if (!base.IsOpened)
                {
                    this.OpenView();
                }
            }
        }

        public virtual ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Dungeon/DungeonSystem.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return true;
            }
        }
    }
}

