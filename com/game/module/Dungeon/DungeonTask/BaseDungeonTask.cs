﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using System;

    public class BaseDungeonTask : IDungeonTask
    {
        private DungeonTaskCondition _dungeonTaskCon;

        public virtual bool CheckDungeonTask(int taskData, int count)
        {
            this.DungeonTaskCon.AddDungeonTaskCount(taskData, count);
            return this.DungeonTaskCon.CheckTaskListAll();
        }

        public virtual int GetType()
        {
            return 0;
        }

        public virtual void Init(SysDungeonTaskVo vo)
        {
            this._dungeonTaskCon = new DungeonTaskCondition(vo);
        }

        public DungeonTaskCondition DungeonTaskCon
        {
            get
            {
                return this._dungeonTaskCon;
            }
            set
            {
                this._dungeonTaskCon = value;
            }
        }
    }
}

