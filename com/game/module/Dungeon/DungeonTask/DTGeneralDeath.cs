﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using System;

    public class DTGeneralDeath : BaseDungeonTask
    {
        public override bool CheckDungeonTask(int taskData, int count)
        {
            this.DungeonTaskCon.AddDungeonTaskCount(taskData, count);
            return this.DungeonTaskCon.CheckTaskListAllNot();
        }

        public override int GetType()
        {
            return 2;
        }

        public override void Init(SysDungeonTaskVo vo)
        {
            base.Init(vo);
        }
    }
}

