﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game;
    using com.game.data;
    using com.u3d.bases.display.character;
    using System;
    using UnityEngine;

    public class DTGeneralHp : BaseDungeonTask
    {
        public override bool CheckDungeonTask(int taskData, int count)
        {
            if (this.DungeonTaskCon.isCheckAll)
            {
                int num = 100;
                foreach (MeDisplay display in AppMap.Instance.SelfplayerList)
                {
                    int num2 = Mathf.RoundToInt((display.GetVo().CurHp / display.GetVo().Hp) * 100f);
                    num = Math.Min(num, num2);
                }
                this.DungeonTaskCon.SetDungeonTaskCount(taskData, num);
            }
            else
            {
                foreach (MeDisplay display2 in AppMap.Instance.SelfplayerList)
                {
                    int num3 = Mathf.RoundToInt((display2.GetVo().CurHp / display2.GetVo().Hp) * 100f);
                    this.DungeonTaskCon.SetDungeonTaskCount((int) display2.GetVo().Id, num3);
                }
            }
            return this.DungeonTaskCon.CheckTaskListAll();
        }

        public override int GetType()
        {
            return 3;
        }

        public override void Init(SysDungeonTaskVo vo)
        {
            base.Init(vo);
        }
    }
}

