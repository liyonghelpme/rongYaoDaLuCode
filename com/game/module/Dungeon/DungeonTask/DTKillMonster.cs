﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using System;

    public class DTKillMonster : BaseDungeonTask
    {
        public override bool CheckDungeonTask(int taskData, int count)
        {
            this.DungeonTaskCon.AddDungeonTaskCount(taskData, count);
            return this.DungeonTaskCon.CheckTaskListAll();
        }

        public override int GetType()
        {
            return 1;
        }

        public override void Init(SysDungeonTaskVo vo)
        {
            base.Init(vo);
        }
    }
}

