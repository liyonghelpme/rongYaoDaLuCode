﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class DungeonTaskCondition
    {
        public bool isCheckAll;
        public bool isFinish;
        public Dictionary<int, int> taskCondition = new Dictionary<int, int>();
        public Dictionary<int, int> taskStatus = new Dictionary<int, int>();
        public int type;

        public DungeonTaskCondition(SysDungeonTaskVo vo)
        {
            this.taskCondition = StringUtils.GetDoubleDimensionalDicStringToInt(vo.condition);
            foreach (int num in this.taskCondition.Keys)
            {
                this.taskStatus.Add(num, 0);
            }
            if (this.taskCondition.ContainsKey(0))
            {
                this.isCheckAll = true;
            }
            this.type = vo.type;
            this.isFinish = false;
        }

        public void AddDungeonTaskCount(int taskData, int count = 1)
        {
            int num;
            this.isFinish = true;
            if (this.taskCondition.ContainsKey(taskData))
            {
                Dictionary<int, int> dictionary;
                num = dictionary[num];
                (dictionary = this.taskStatus)[num = taskData] = num + count;
            }
            else if (this.isCheckAll)
            {
                Dictionary<int, int> dictionary2;
                num = dictionary2[num];
                (dictionary2 = this.taskStatus)[num = 0] = num + count;
            }
        }

        public bool CheckTaskListAll()
        {
            bool flag = true;
            foreach (int num in this.taskStatus.Keys)
            {
                if (this.taskStatus[num] < this.taskCondition[num])
                {
                    flag = false;
                }
            }
            return flag;
        }

        public bool CheckTaskListAllNot()
        {
            bool flag = true;
            foreach (int num in this.taskStatus.Keys)
            {
                if (this.taskStatus[num] > this.taskCondition[num])
                {
                    flag = false;
                }
            }
            return flag;
        }

        public void SetDungeonTaskCount(int taskData, int count = 1)
        {
            if (this.isCheckAll)
            {
                this.taskStatus[0] = count;
            }
            else if (this.taskCondition.ContainsKey(taskData))
            {
                this.taskStatus[taskData] = count;
            }
        }
    }
}

