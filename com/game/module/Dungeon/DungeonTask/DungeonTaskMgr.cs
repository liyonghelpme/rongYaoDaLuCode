﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class DungeonTaskMgr
    {
        private Dictionary<int, DungeonTaskStatus> dungeonTaskList = new Dictionary<int, DungeonTaskStatus>();
        private Dictionary<int, Type> taskTypeList = new Dictionary<int, Type>();

        public DungeonTaskMgr(string str)
        {
            this.InitAllTask();
            int[] stringToInt = StringUtils.GetStringToInt(str);
            int length = stringToInt.Length;
            for (int i = 0; i < length; i++)
            {
                SysDungeonTaskVo dungeonTaskVo = BaseDataMgr.instance.GetDungeonTaskVo((uint) stringToInt[i]);
                this.BaseAddDungeonTask(i + 1, dungeonTaskVo);
            }
        }

        private IDungeonTask AddDungeonTask(int type)
        {
            if (this.taskTypeList.ContainsKey(type))
            {
                return (IDungeonTask) Activator.CreateInstance(this.taskTypeList[type]);
            }
            return null;
        }

        private void BaseAddDungeonTask(int idx, SysDungeonTaskVo vo)
        {
            IDungeonTask task = this.AddDungeonTask(vo.type);
            task.Init(vo);
            DungeonTaskStatus status = new DungeonTaskStatus(idx, (int) vo.unikey, vo.type) {
                dungeonTask = task
            };
            this.dungeonTaskList.Add((int) vo.unikey, status);
        }

        public Dictionary<int, DungeonTaskStatus> GetTaskStatusList()
        {
            return this.dungeonTaskList;
        }

        private void InitAllTask()
        {
            this.taskTypeList.Add(1, typeof(DTKillMonster));
            this.taskTypeList.Add(2, typeof(DTGeneralDeath));
            this.taskTypeList.Add(3, typeof(DTGeneralHp));
        }

        public void TriggerDungeonTask(int taskType, int taskData = 0, int count = 1)
        {
            foreach (DungeonTaskStatus status in this.dungeonTaskList.Values)
            {
                if (!status.isFinish && (status.taskType == taskType))
                {
                    status.isFinish = status.dungeonTask.CheckDungeonTask(taskData, count);
                }
            }
        }
    }
}

