﻿namespace com.game.module.Dungeon.DungeonTask
{
    using System;
    using System.Collections.Generic;

    public class DungeonTaskStatus
    {
        public IDungeonTask dungeonTask;
        public int index = 1;
        public bool isFinish;
        public int taskId;
        public int taskType;

        public DungeonTaskStatus(int idx, int id, int type)
        {
            this.index = idx;
            this.taskId = id;
            this.taskType = type;
        }

        public Dictionary<int, int> TaskCount
        {
            get
            {
                return this.dungeonTask.DungeonTaskCon.taskStatus;
            }
        }
    }
}

