﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using System;

    public interface IDungeonTask
    {
        bool CheckDungeonTask(int taskData, int count);
        int GetType();
        void Init(SysDungeonTaskVo vo);

        DungeonTaskCondition DungeonTaskCon { get; set; }
    }
}

