﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using System;
    using System.Runtime.InteropServices;

    public class MTask
    {
        private int _curValue;
        private int _targetId;
        private int _targetValue;
        private uint _taskId;
        private SysDungeonTaskVo _taskSysVo;

        public void HandleTaskEvent(int evtType, int id, int count, object data = null)
        {
            if (this.IsCountTask)
            {
                if (((evtType == this.taskType) && MTaskType.IsInterestEvent(this.taskType, evtType)) && ((this._targetId == 0) || (id == this._targetId)))
                {
                    this._curValue += count;
                }
            }
            else if ((evtType == 0) || (evtType == this.taskType))
            {
                this.UpdateTaskInfo(evtType, id, count, data);
            }
        }

        public void Parse(uint taskId)
        {
            this._taskId = taskId;
            this._taskSysVo = BaseDataMgr.instance.GetDungeonTaskVo(taskId);
            string[] valueListFromString = StringUtils.GetValueListFromString(StringUtils.GetValueString(this._taskSysVo.condition), ',');
            this._targetId = int.Parse(valueListFromString[0]);
            this._targetValue = int.Parse(valueListFromString[1]);
        }

        protected void UpdateTaskInfo(int evtType, int id, int count, object data)
        {
        }

        public int curValue
        {
            get
            {
                return this._curValue;
            }
        }

        public bool IsCountTask
        {
            get
            {
                return MTaskType.IsCountTask(this._taskSysVo.type);
            }
        }

        public bool isFinished
        {
            get
            {
                if (!this.IsCountTask)
                {
                    return MTaskChecker.Instance.CheckTaskFinished(this);
                }
                if (MTaskType.CompareTypeOf(this.taskType) == TaskCompareType.BIGGER)
                {
                    return (this.curValue >= this._targetValue);
                }
                return (this.curValue <= this._targetValue);
            }
        }

        public int targetId
        {
            get
            {
                return this._targetId;
            }
        }

        public int targetValue
        {
            get
            {
                return this._targetValue;
            }
        }

        public uint taskId
        {
            get
            {
                return this._taskId;
            }
        }

        public SysDungeonTaskVo taskSysVo
        {
            get
            {
                return this._taskSysVo;
            }
        }

        public int taskType
        {
            get
            {
                return this._taskSysVo.type;
            }
        }
    }
}

