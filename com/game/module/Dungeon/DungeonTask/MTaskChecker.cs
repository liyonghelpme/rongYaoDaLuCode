﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.map;
    using com.game.vo;
    using System;
    using System.Collections.Generic;

    public class MTaskChecker
    {
        private Dictionary<int, Func<MTask, bool>> _checkerDic = new Dictionary<int, Func<MTask, bool>>();
        private static MTaskChecker _inst;

        private MTaskChecker()
        {
        }

        private bool CheckDieCount(MTask t)
        {
            uint num = 0;
            foreach (MeVo vo in SelfPlayerManager.instance.MePlayerVoList)
            {
                num += vo.numDead;
            }
            return (num <= t.targetValue);
        }

        private bool CheckFindAllChest(MTask t)
        {
            return (Singleton<ChestMgr>.Instance.numNotPicked <= 0);
        }

        private bool CheckFindPartner(MTask t)
        {
            List<MeVo> mePlayerVoList = SelfPlayerManager.instance.MePlayerVoList;
            int num = (t.targetValue <= 0) ? mePlayerVoList.Count : t.targetValue;
            int num2 = 0;
            foreach (MeVo vo in mePlayerVoList)
            {
                if (!vo.isImprisoned)
                {
                    num2++;
                }
            }
            return (num2 >= num);
        }

        private bool CheckKillAllMonster(MTask t)
        {
            uint targetId = (uint) t.targetId;
            int num2 = AppMap.Instance.GetNumOfMonster(targetId, true, false, false);
            int num3 = Singleton<MonsterMgr>.Instance.GetNumOfMonster(targetId, true, false, false);
            int numOfMonster = Singleton<MonsterActivator>.Instance.GetNumOfMonster(targetId, false);
            return (((num2 <= 0) && (numOfMonster <= 0)) && (num3 <= 0));
        }

        private bool CheckPassInTime(MTask t)
        {
            return (DungeonMgr.Instance.success && (DungeonMgr.Instance.endTime <= t.targetValue));
        }

        private bool CheckRescuePartner(MTask t)
        {
            return this.CheckFindPartner(t);
        }

        public bool CheckTaskFinished(MTask t)
        {
            if (!this._checkerDic.ContainsKey(t.taskType))
            {
                return false;
            }
            Func<MTask, bool> func = this._checkerDic[t.taskType];
            return func(t);
        }

        private bool CheckUseHero(MTask t)
        {
            foreach (MeVo vo in SelfPlayerManager.instance.MePlayerVoList)
            {
                if (vo.generalTemplateInfo.id == t.targetId)
                {
                    return true;
                }
            }
            return false;
        }

        private void Setup()
        {
            this._checkerDic.Add(2, new Func<MTask, bool>(this.CheckDieCount));
            this._checkerDic.Add(4, new Func<MTask, bool>(this.CheckFindPartner));
            this._checkerDic.Add(5, new Func<MTask, bool>(this.CheckRescuePartner));
            this._checkerDic.Add(6, new Func<MTask, bool>(this.CheckPassInTime));
            this._checkerDic.Add(7, new Func<MTask, bool>(this.CheckUseHero));
            this._checkerDic.Add(8, new Func<MTask, bool>(this.CheckKillAllMonster));
            this._checkerDic.Add(9, new Func<MTask, bool>(this.CheckFindAllChest));
        }

        public static MTaskChecker Instance
        {
            get
            {
                if (_inst == null)
                {
                    _inst = new MTaskChecker();
                    _inst.Setup();
                }
                return _inst;
            }
        }
    }
}

