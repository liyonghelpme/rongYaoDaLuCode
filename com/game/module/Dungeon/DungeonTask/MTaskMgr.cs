﻿namespace com.game.module.Dungeon.DungeonTask
{
    using com.game.data;
    using com.game.module.core;
    using com.game.utils;
    using System;
    using System.Collections.Generic;

    public class MTaskMgr : Singleton<MTaskMgr>
    {
        private MTask _mainTask;
        private Dictionary<uint, MTask> _taskDic = new Dictionary<uint, MTask>();

        public void Clear()
        {
            if (this._taskDic.Count > 0)
            {
                this._taskDic.Clear();
            }
        }

        public MTask GetTask(uint taskId)
        {
            if ((this._taskDic != null) && this._taskDic.ContainsKey(taskId))
            {
                return this._taskDic[taskId];
            }
            return null;
        }

        public void Reset(SysDungeonMissionVo sysMissionVo)
        {
            if (this._taskDic.Count > 0)
            {
                this._taskDic.Clear();
            }
            if (sysMissionVo != null)
            {
                foreach (int num in StringUtils.GetStringToInt(sysMissionVo.dungeon_task))
                {
                    MTask task = new MTask();
                    task.Parse((uint) num);
                    this._taskDic.Add(task.taskId, task);
                }
                uint num3 = (uint) sysMissionVo.succ_task;
                this._mainTask = this._taskDic[num3];
            }
        }

        public void Update(int type, int id, int count, object data)
        {
            if ((this._taskDic != null) && (this._taskDic.Count > 0))
            {
                foreach (MTask task in this._taskDic.Values)
                {
                    task.HandleTaskEvent(type, id, count, data);
                }
            }
        }

        public MTask mainTask
        {
            get
            {
                return this._mainTask;
            }
        }

        public int numFinished
        {
            get
            {
                int num = 0;
                foreach (MTask task in this._taskDic.Values)
                {
                    if (task.isFinished)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        public int numTaskTotal
        {
            get
            {
                return this._taskDic.Count;
            }
        }

        public Dictionary<uint, MTask> taskList
        {
            get
            {
                return this._taskDic;
            }
        }
    }
}

