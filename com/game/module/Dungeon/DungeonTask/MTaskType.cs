﻿namespace com.game.module.Dungeon.DungeonTask
{
    using System;
    using System.Collections.Generic;

    public class MTaskType
    {
        private static Dictionary<int, bool> _countTaskDic;
        private static Dictionary<int, TaskCompareType> _taskCompDic;
        private static Dictionary<int, int[]> _taskInterestEvents;
        public const int FIND_ALL_CHEST = 9;
        public const int FIND_CHEST = 3;
        public const int FIND_PARTNER = 4;
        public const int GENERAL_DIE_LIMIT = 2;
        public const int KILL_ALL_MONSTER = 8;
        public const int KILL_MONSTER = 1;
        public const int LESS_TIME = 6;
        public const int RESCUE_PARTENER = 5;
        public const int USE_GENERAL = 7;

        public static TaskCompareType CompareTypeOf(int type)
        {
            if (_taskCompDic == null)
            {
                _taskCompDic = new Dictionary<int, TaskCompareType>();
                _taskCompDic.Add(1, TaskCompareType.BIGGER);
                _taskCompDic.Add(3, TaskCompareType.BIGGER);
            }
            return _taskCompDic[type];
        }

        public static bool IsCountTask(int type)
        {
            if (_countTaskDic == null)
            {
                _countTaskDic = new Dictionary<int, bool>();
                _countTaskDic.Add(1, true);
                _countTaskDic.Add(3, true);
            }
            return _countTaskDic.ContainsKey(type);
        }

        public static bool IsInterestEvent(int taskType, int eventType)
        {
            if (_taskInterestEvents == null)
            {
                _taskInterestEvents = new Dictionary<int, int[]>();
                int[] numArray1 = new int[] { 1 };
                _taskInterestEvents.Add(1, numArray1);
                int[] numArray3 = new int[] { 3 };
                _taskInterestEvents.Add(3, numArray3);
                int[] numArray4 = new int[] { 6 };
                _taskInterestEvents.Add(5, numArray4);
            }
            if (_taskInterestEvents.ContainsKey(taskType))
            {
                int[] numArray = _taskInterestEvents[taskType];
                foreach (int num in numArray)
                {
                    if (num == eventType)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

