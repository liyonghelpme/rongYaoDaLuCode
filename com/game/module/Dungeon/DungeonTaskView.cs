﻿namespace com.game.module.Dungeon
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.module.core;
    using com.game.module.Dungeon.DungeonTask;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class DungeonTaskView : BaseView<DungeonTaskView>
    {
        private List<UILabel> _taskList;

        protected override void HandleAfterOpenView()
        {
            this.UpdateView();
            GlobalAPI.facade.Add(12, new NoticeListener(this.OnDungeonStop));
            GlobalAPI.facade.Add(0x11, new NoticeListener(this.OnStartAnim));
            GlobalAPI.facade.Add(0x12, new NoticeListener(this.OEndAnim));
            GlobalAPI.facade.Add(11, new NoticeListener(this.OnDungeonUpdate));
            GlobalAPI.facade.Add(14, new NoticeListener(this.OnExitDungeon));
        }

        protected override void HandleBeforeCloseView()
        {
            GlobalAPI.facade.Remove(12, new NoticeListener(this.OnDungeonStop));
            GlobalAPI.facade.Remove(0x11, new NoticeListener(this.OnStartAnim));
            GlobalAPI.facade.Remove(0x12, new NoticeListener(this.OEndAnim));
            GlobalAPI.facade.Remove(11, new NoticeListener(this.OnDungeonUpdate));
            GlobalAPI.facade.Remove(14, new NoticeListener(this.OnExitDungeon));
        }

        protected override void Init()
        {
            this._taskList = new List<UILabel>();
            for (int i = 1; i <= 3; i++)
            {
                UILabel item = base.FindInChild<UILabel>("con/item" + i + "/label");
                this._taskList.Add(item);
            }
        }

        private void OEndAnim(int type, int v1, int v2, object data)
        {
            this.gameObject.SetActive(true);
        }

        private void OnDungeonStop(int type, int v1, int v2, object data)
        {
            this.UpdateView();
        }

        private void OnDungeonUpdate(int type, int v1, int v2, object data)
        {
            this.UpdateView();
        }

        private void OnExitDungeon(int type, int v1, int v2, object data)
        {
            this.CloseView();
        }

        private void OnStartAnim(int type, int v1, int v2, object data)
        {
            this.gameObject.SetActive(false);
        }

        public override void OpenView()
        {
            Singleton<DungeonSystem>.Instance.RegisterOpenAction(new OpenViewCallBack(this.OpenView));
        }

        private void UpdateView()
        {
            Dictionary<uint, MTask> taskList = Singleton<MTaskMgr>.Instance.taskList;
            int num = 0;
            foreach (MTask task in taskList.Values)
            {
                if (num >= this._taskList.Count)
                {
                    break;
                }
                this._taskList[num].transform.parent.gameObject.SetActive(true);
                if (task.taskSysVo.show_state)
                {
                    int num2;
                    if (task.IsCountTask)
                    {
                        num2 = (task.curValue <= task.targetValue) ? task.curValue : task.targetValue;
                        object[] objArray1 = new object[] { task.taskSysVo.content, "(", num2, "/", task.targetValue, ")" };
                        this._taskList[num].text = string.Concat(objArray1);
                    }
                    else
                    {
                        num2 = !task.isFinished ? 0 : 1;
                        object[] objArray2 = new object[] { task.taskSysVo.content, "(", num2, "/1)" };
                        this._taskList[num].text = string.Concat(objArray2);
                    }
                }
                else
                {
                    this._taskList[num].text = task.taskSysVo.content;
                }
                this._taskList[num].color = !task.isFinished ? Color.white : Color.green;
                num++;
            }
            while (num < this._taskList.Count)
            {
                this._taskList[num].transform.parent.gameObject.SetActive(false);
                num++;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

