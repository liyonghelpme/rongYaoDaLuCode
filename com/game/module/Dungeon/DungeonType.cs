﻿namespace com.game.module.Dungeon
{
    using System;

    public class DungeonType
    {
        public const int ELITE = 2;
        public const int NORMAL = 1;
        public const int TEAM = 3;
    }
}

