﻿namespace com.game.module.Dungeon
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class MulSweepView
    {
        private Button _btnOK;
        private GameObject _go;
        private GameObject _gridGo;
        private GameObject _itemGo;
        private UIScrollView _scrollView;
        private UIGrid _sweepGrid;
        private List<SweepItemView> _sweepList = new List<SweepItemView>();

        public MulSweepView(GameObject go)
        {
            this._go = go;
            this._gridGo = NGUITools.FindChild(this._go, "grid");
            this._scrollView = NGUITools.FindInChild<UIScrollView>(this._go, "grid");
            this._sweepGrid = NGUITools.FindInChild<UIGrid>(this._go, "grid");
            this._btnOK = NGUITools.FindInChild<Button>(this._go, "btnClose");
            this._btnOK.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        public void Close()
        {
            this._go.SetActive(false);
            for (int i = this._sweepList.Count - 1; i >= 0; i--)
            {
                if (i > 0)
                {
                    this._sweepList[i].Clear(true);
                    this._sweepList.RemoveAt(i);
                }
                else
                {
                    this._sweepList[i].Clear(false);
                }
            }
        }

        private void OnClick(GameObject go)
        {
            Singleton<DungeonSweepView>.Instance.CloseView();
        }

        public void Update(List<PDuplicateMopUp> list)
        {
            this._go.SetActive(true);
            if ((list != null) && (list.Count > 0))
            {
                SweepItemView view;
                this._scrollView.ResetPosition();
                int num = 0;
                while (this._sweepList.Count < list.Count)
                {
                    GameObject obj2;
                    if (null == this._itemGo)
                    {
                        obj2 = this._itemGo = NGUITools.FindChild(this._go, "grid/item");
                    }
                    else
                    {
                        obj2 = NGUITools.AddChild(this._gridGo, this._itemGo);
                    }
                    view = new SweepItemView(obj2);
                    this._sweepList.Add(view);
                }
                num = 0;
                while (num < list.Count)
                {
                    view = this._sweepList[num];
                    view.SetActive(true);
                    view.UpdateView(list[num], "第" + (num + 1) + "战");
                    num++;
                }
                while (num < this._sweepList.Count)
                {
                    this._sweepList[num].SetActive(false);
                    num++;
                }
                this._sweepGrid.Reposition();
            }
        }
    }
}

