﻿namespace com.game.module.Dungeon
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class SingleSweepView
    {
        private Button _btnOk;
        private GameObject _go;
        private SweepItemView _itemView;

        public SingleSweepView(GameObject go)
        {
            this._go = go;
            this._btnOk = NGUITools.FindInChild<Button>(this._go, "btnClose");
            this._btnOk.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        public void Close()
        {
            if (this._itemView != null)
            {
                this._itemView.Clear(false);
            }
            this._go.SetActive(false);
        }

        private void OnClick(GameObject go)
        {
            Singleton<DungeonSweepView>.Instance.CloseView();
        }

        public void Update(List<PDuplicateMopUp> list)
        {
            this._go.SetActive(true);
            if ((list != null) && (list.Count > 0))
            {
                if (this._itemView == null)
                {
                    this._itemView = new SweepItemView(NGUITools.FindChild(this._go, "item"));
                }
                this._itemView.UpdateView(list[0], string.Empty);
            }
        }
    }
}

