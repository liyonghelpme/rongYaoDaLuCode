﻿namespace com.game.module.Dungeon
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class StarEffect
    {
        private System.Action _call;
        private int _curFC;
        private Vector3 _destPos;
        private GameObject _go;
        private List<UISprite> _iconList = new List<UISprite>();
        private static StarEffect _inst;
        private Vector3 _originPos;
        private UISprite _originSprite;
        private Vector3 _speed;
        private UISprite _targetSprite;
        private Transform _trans;
        private static readonly float Frame_Count = 2f;

        public void Add(UISprite s)
        {
            if ((null != s) && !this._iconList.Contains(s))
            {
                this._iconList.Add(s);
            }
        }

        private void CheckNext()
        {
            if (this._iconList.Count <= 0)
            {
                this._go.SetActive(false);
                this._targetSprite = null;
                if (this._call != null)
                {
                    this._call();
                    this._call = null;
                }
            }
            else
            {
                this._go.SetActive(true);
                this._targetSprite = this._iconList[0];
                this._destPos = this._iconList[0].transform.position;
                this._iconList.RemoveAt(0);
                this._speed = (Vector3) ((this._destPos - this._originPos) / Frame_Count);
                this._trans.position = this._originPos;
                this._originSprite.alpha = 1f;
                this._curFC = Time.frameCount;
                DungeonAwardView instance = Singleton<DungeonAwardView>.Instance;
                instance.onUpdate = (System.Action) Delegate.Combine(instance.onUpdate, new System.Action(this.MoveTo));
            }
        }

        public void Clear()
        {
            this._iconList.Clear();
        }

        public void Init(GameObject go)
        {
            this._go = go;
            this._trans = this._go.transform;
            this._originPos = this._trans.position;
            this._originSprite = this._go.GetComponent<UISprite>();
            this._originSprite.spriteName = "xingxing";
            this._originSprite.ShowAsMyself();
        }

        private void MoveTo()
        {
            Vector3 vector = this._trans.position + this._speed;
            this._trans.position = vector;
            this._originSprite.alpha -= 1f / Frame_Count;
            if ((Time.frameCount - this._curFC) > Frame_Count)
            {
                this._targetSprite.ShowAsMyself();
                this._trans.position = this._destPos;
                this._go.SetActive(false);
                DungeonAwardView instance = Singleton<DungeonAwardView>.Instance;
                instance.onUpdate = (System.Action) Delegate.Remove(instance.onUpdate, new System.Action(this.MoveTo));
                if (this._iconList.Count > 0)
                {
                    this._curFC = Time.frameCount;
                    DungeonAwardView local2 = Singleton<DungeonAwardView>.Instance;
                    local2.onUpdate = (System.Action) Delegate.Combine(local2.onUpdate, new System.Action(this.Wait));
                }
                else
                {
                    this.CheckNext();
                }
            }
        }

        public void Play(System.Action call = null)
        {
            this._call = call;
            this.CheckNext();
        }

        private void Wait()
        {
            if ((Time.frameCount - this._curFC) >= 2)
            {
                DungeonAwardView instance = Singleton<DungeonAwardView>.Instance;
                instance.onUpdate = (System.Action) Delegate.Remove(instance.onUpdate, new System.Action(this.Wait));
                this.CheckNext();
            }
        }

        public static StarEffect Instance
        {
            get
            {
                if (_inst == null)
                {
                    _inst = new StarEffect();
                }
                return _inst;
            }
        }
    }
}

