﻿namespace com.game.module.Dungeon
{
    using com.game.module.bag;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class SweepItemView
    {
        private UILabel _exp;
        private GameObject _go;
        private UILabel _gold;
        private GameObject _itemCellGo;
        private UIGrid _itemGrid;
        private GameObject _itemGridGo;
        private GameObject _noDropGo;
        private UIScrollView _scrollView;
        private UILabel _title;

        public SweepItemView(GameObject go)
        {
            this._go = go;
            this._itemGridGo = NGUITools.FindChild(this._go, "award_list");
            this._noDropGo = NGUITools.FindChild(this._go, "no_props");
            this._title = NGUITools.FindInChild<UILabel>(this._go, "title");
            this._exp = NGUITools.FindInChild<UILabel>(this._go, "expValue");
            this._gold = NGUITools.FindInChild<UILabel>(this._go, "goldValue");
            this._itemGrid = this._itemGridGo.GetComponent<UIGrid>();
            this._itemCellGo = NGUITools.FindChild(this._itemGridGo, "item");
            this._scrollView = NGUITools.FindInChild<UIScrollView>(this._go, "award_list");
        }

        public void Clear(bool destroy)
        {
            Transform transform = this._itemGridGo.transform;
            for (int i = transform.childCount - 1; i > 0; i--)
            {
                UnityEngine.Object.Destroy(transform.GetChild(i).gameObject);
            }
            if (destroy && (null != this._go))
            {
                UnityEngine.Object.Destroy(this._go);
                this._go = null;
            }
        }

        private SysItemVoCell GetCell(Transform trans, int idx)
        {
            GameObject gameObject;
            if (idx < trans.childCount)
            {
                gameObject = trans.GetChild(idx).gameObject;
            }
            else
            {
                gameObject = NGUITools.AddChild(this._itemGridGo, this._itemCellGo);
            }
            return gameObject.AddMissingComponent<SysItemVoCell>();
        }

        public void SetActive(bool value)
        {
            if (null != this._go)
            {
                this._go.SetActive(value);
            }
        }

        public void UpdateView(PDuplicateMopUp sweepInfo, string title)
        {
            if (null != this._scrollView)
            {
                this._scrollView.ResetPosition();
            }
            if (null != this._title)
            {
                this._title.text = title;
            }
            this._exp.text = "+" + sweepInfo.exp.ToString();
            this._gold.text = "+" + sweepInfo.gold.ToString();
            bool flag = sweepInfo.dropList.Count > 0;
            this._itemGridGo.SetActive(flag);
            this._noDropGo.SetActive(!flag);
            Transform trans = this._itemGridGo.transform;
            int idx = 0;
            foreach (PDuplicateDrop drop in sweepInfo.dropList)
            {
                SysItemVoCell cell = this.GetCell(trans, idx);
                cell.SetActive(true);
                cell.sysVoId = (int) drop.id;
                cell.count = drop.num;
                idx++;
                if (idx >= 6)
                {
                    break;
                }
            }
            int childCount = trans.childCount;
            while (idx < childCount)
            {
                trans.GetChild(idx).gameObject.SetActive(false);
                idx++;
            }
            this._itemGrid.Reposition();
        }
    }
}

