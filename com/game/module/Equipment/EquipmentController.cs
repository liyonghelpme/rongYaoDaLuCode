﻿namespace com.game.module.Equipment
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class EquipmentController : BaseControl<EquipmentController>
    {
        private void EquipDismountStoneSucceed(INetData data)
        {
            ItemEquipTreasureUnmountMsg_36_13 g__ = new ItemEquipTreasureUnmountMsg_36_13();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.stoneInsertInfo.DismountStoneSucceed();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void EquipInsertStoneScucced(INetData data)
        {
            ItemEquipTreasureMountMsg_36_8 g__ = new ItemEquipTreasureMountMsg_36_8();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.stoneInsertInfo.InsertStoneSucceed();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void EquipStrengthSucceed(INetData data)
        {
            ItemEquipStrengthMsg_36_10 g__ = new ItemEquipStrengthMsg_36_10();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.strengthInfo.StrengthEquipSucceed(g__.id);
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void EquipUpgradeSucceed(INetData data)
        {
            ItemEquipLvUpMsg_36_7 g__ = new ItemEquipLvUpMsg_36_7();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.upgradeInfo.EquipUpgradeSucceed(g__.id);
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void MaterialMake(INetData data)
        {
            ItemMaterialMakeMsg_36_12 g__ = new ItemMaterialMakeMsg_36_12();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.upgradeInfo.MaterialMakeSucceed();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("9227", new NetMsgCallback(this.WearEquipSucceed));
            AppNet.main.addCMD("9222", new NetMsgCallback(this.PutInMaterial));
            AppNet.main.addCMD("9223", new NetMsgCallback(this.EquipUpgradeSucceed));
            AppNet.main.addCMD("9224", new NetMsgCallback(this.EquipInsertStoneScucced));
            AppNet.main.addCMD("9226", new NetMsgCallback(this.EquipStrengthSucceed));
            AppNet.main.addCMD("9229", new NetMsgCallback(this.EquipDismountStoneSucceed));
            AppNet.main.addCMD("9225", new NetMsgCallback(this.StoneMixSucceed));
            AppNet.main.addCMD("9221", new NetMsgCallback(this.StoneBatchMixSucceed));
            AppNet.main.addCMD("9228", new NetMsgCallback(this.MaterialMake));
        }

        private void PutInMaterial(INetData data)
        {
            ItemLvPutMaterialMsg_36_6 g__ = new ItemLvPutMaterialMsg_36_6();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.upgradeInfo.PutInMaterialSucceed(g__.materialIds);
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void StoneBatchMixSucceed(INetData data)
        {
            ItemBatchUseMsg_36_5 g__ = new ItemBatchUseMsg_36_5();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.mixInfo.StoneBatchMixSucceed();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void StoneMixSucceed(INetData data)
        {
            ItemTreasureMakeMsg_36_9 g__ = new ItemTreasureMakeMsg_36_9();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.mixInfo.StoneMixSucceed();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void WearEquipSucceed(INetData data)
        {
            ItemEquipWearMsg_36_11 g__ = new ItemEquipWearMsg_36_11();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<EquipmentMode>.Instance.wearEquipInfo.WearEquipSucceed();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }
    }
}

