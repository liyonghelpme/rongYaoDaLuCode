﻿namespace com.game.module.Equipment
{
    using com.game;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class EquipmentMode : BaseMode<EquipmentMode>
    {
        public MixInfo mixInfo = new MixInfo();
        public InsertInfo stoneInsertInfo = new InsertInfo();
        public StrengthInfo strengthInfo = new StrengthInfo();
        public UpgradeInfo upgradeInfo = new UpgradeInfo();
        public WearEquipInfo wearEquipInfo = new WearEquipInfo();

        public void SendDismountStone(ulong id, uint stoneTid)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_13(msdata, id, stoneTid);
            AppNet.gameNet.send(msdata, 0x24, 13);
        }

        public void SendEquipStrength(ulong id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_10(msdata, id);
            AppNet.gameNet.send(msdata, 0x24, 10);
        }

        public void SendEquipUpgrade(ulong id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_7(msdata, id);
            AppNet.gameNet.send(msdata, 0x24, 7);
        }

        public void SendMaterialMake(uint itemId, ushort num)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_12(msdata, itemId, num);
            AppNet.gameNet.send(msdata, 0x24, 12);
        }

        public void SendPutInMaterial(ulong equipId, List<PMaterial> materialIds)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_6(msdata, equipId, materialIds);
            AppNet.gameNet.send(msdata, 0x24, 6);
        }

        public void SendStoneInsert(ulong id, List<ulong> materialIds)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_8(msdata, id, materialIds);
            AppNet.gameNet.send(msdata, 0x24, 8);
        }

        public void SendStoneMix(uint id, uint num)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_9(msdata, id, num);
            AppNet.gameNet.send(msdata, 0x24, 9);
        }

        public void SendWearEquip(ulong itemId, ulong generalId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_11(msdata, itemId, generalId);
            AppNet.gameNet.send(msdata, 0x24, 11);
        }
    }
}

