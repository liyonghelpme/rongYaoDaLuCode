﻿namespace com.game.module.Equipment.components
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using com.game.module.Equipment.utils;
    using com.game.Public.Message;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AddEquipMentPanel : BaseView<AddEquipMentPanel>
    {
        private UILabel attriLab;
        private Button bgBtn;
        private Button closeBtn;
        private CurrentOperate curOperate;
        private UILabel descriptLabel;
        private Button equipBtn;
        private UISprite equipIcon;
        private UILabel equipNameLabel;
        private SysItemsVo equipTemplate;
        private int equipType;
        private PItems itemInfo;
        private BoxCollider labBoxClollider;
        private UIScrollView scrollView;

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        private void EquipBtnClickHandler(GameObject go)
        {
            switch (this.curOperate)
            {
                case CurrentOperate.EQUIP:
                {
                    PItems pItemByTemplateId = Singleton<BagMode>.Instance.GetPItemByTemplateId((uint) this.equipTemplate.id);
                    if (pItemByTemplateId != null)
                    {
                        Singleton<EquipmentMode>.Instance.SendWearEquip(pItemByTemplateId.id, Singleton<GeneralMode>.Instance.selectedGeneralInfo.generalInfo.id);
                    }
                    else
                    {
                        MessageManager.Show("背包里没有该装备");
                    }
                    break;
                }
                case CurrentOperate.FORGE:
                {
                    ToEquipmentData data = new ToEquipmentData {
                        generalInfo = Singleton<GeneralMode>.Instance.selectedGeneralInfo.generalInfo,
                        equipInfo = Singleton<GeneralMode>.Instance.selectedGeneralInfo.generalInfo.wornEquipItemDic[this.equipType]
                    };
                    Singleton<EquipmentMainPanel>.Instance.OpenEquipment(data, 0);
                    Singleton<GeneralInfoPanel>.Instance.CloseView();
                    this.CloseView();
                    break;
                }
            }
        }

        protected override void HandleAfterOpenView()
        {
            this.scrollView.ResetPosition();
            this.equipBtn.SetActive(base.LastOpenedPanelType != PanelType.CORPS_PANEL);
            if (base.LastOpenedPanelType != PanelType.CORPS_PANEL)
            {
                switch (this.curOperate)
                {
                    case CurrentOperate.EQUIP:
                        this.equipBtn.label.text = "装备";
                        break;

                    case CurrentOperate.FORGE:
                        this.equipBtn.label.text = "锻造";
                        break;
                }
            }
            this.equipIcon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ItemIconAtlas");
            this.equipIcon.spriteName = this.equipTemplate.icon.ToString();
            this.equipIcon.MakePixelPerfect();
            this.equipNameLabel.text = this.equipTemplate.name;
            this.attriLab.text = string.Empty;
            Dictionary<int, int> attriDic = ParseStringUtils.ParseAddAttri(this.equipTemplate.property, 1f);
            this.attriLab.text = AttTips.GetNoAddAttriContent(attriDic);
            this.labBoxClollider.size = new Vector3((float) this.attriLab.width, (float) this.attriLab.height, 0f);
        }

        protected override void Init()
        {
            this.equipIcon = base.FindInChild<UISprite>("panel/equipCell/icon");
            this.equipNameLabel = base.FindInChild<UILabel>("panel/equipCell/Label");
            this.attriLab = base.FindInChild<UILabel>("panel/descript/Lab/attriLabel");
            this.descriptLabel = base.FindInChild<UILabel>("panel/descript/descriptLabel");
            this.equipBtn = base.FindInChild<Button>("panel/equipBtn");
            this.closeBtn = base.FindInChild<Button>("panel/closeBtn");
            this.bgBtn = base.FindInChild<Button>("panel/bgBtn");
            this.labBoxClollider = base.FindChild("panel/descript/Lab").GetComponent<BoxCollider>();
            this.scrollView = base.FindInChild<UIScrollView>("panel/descript");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this.equipBtn.onClick = new UIWidgetContainer.VoidDelegate(this.EquipBtnClickHandler);
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            Singleton<EquipmentMode>.Instance.wearEquipInfo.Add(0, new NoticeListener(this.WearEquipSucceed));
        }

        public void SetInfo(SysItemsVo equipTemplate, int equipType, CurrentOperate operate, PanelType panelType = 0)
        {
            this.equipTemplate = equipTemplate;
            this.equipType = equipType;
            this.curOperate = operate;
            this.OpenView(panelType);
        }

        private void WearEquipSucceed(int type, int v1, int v2, object data)
        {
            MessageManager.Show("装备成功");
            this.CloseView();
        }

        public Button EquipBtn
        {
            get
            {
                return this.equipBtn;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Equipment/AddEquipment.assetbundle";
            }
        }
    }
}

