﻿namespace com.game.module.Equipment.components
{
    using com.game.module.core;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AlertPanel : BaseView<AlertPanel>
    {
        private SureCallback[] _calls = new SureCallback[2];
        private Button bgBtn;
        private Button btn_close;
        private Button cancelBtn;
        private UILabel commonLab;
        private string content;
        private UISprite jinbiIcon;
        private Button sureBtn;

        public override void CancelUpdateHandler()
        {
            SureCallback callback;
            this._calls[1] = (SureCallback) (callback = null);
            this._calls[0] = callback;
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            int num = this.content.IndexOf("@") - 2;
            if (num > -1)
            {
                this.content = this.content.Replace("@", string.Empty);
                float x = this.commonLab.transform.localPosition.x + (this.commonLab.fontSize * num);
                this.jinbiIcon.SetActive(true);
                this.jinbiIcon.transform.localPosition = new Vector3(x, this.commonLab.transform.localPosition.y + 8f, 0f);
            }
            this.commonLab.text = this.content;
        }

        protected override void Init()
        {
            this.sureBtn = base.FindInChild<Button>("alertPanel/btns/sureBtn");
            this.cancelBtn = base.FindInChild<Button>("alertPanel/btns/cancelBtn");
            this.commonLab = base.FindInChild<UILabel>("alertPanel/Label");
            this.bgBtn = base.FindInChild<Button>("alertPanel/bgBtn");
            this.jinbiIcon = base.FindInChild<UISprite>("alertPanel/jinbiICon");
            this.jinbiIcon.SetActive(false);
            this.btn_close = base.FindInChild<Button>("alertPanel/btn_close");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this.sureBtn.onClick = this.cancelBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            this.bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.btn_close.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
        }

        private void OnClickBtn(GameObject go)
        {
            int index = -1;
            if (go.name == this.sureBtn.gameObject.name)
            {
                index = 0;
            }
            else if (go.name == this.cancelBtn.gameObject.name)
            {
                index = 1;
            }
            else
            {
                return;
            }
            SureCallback callback = this._calls[index];
            this._calls[index] = null;
            if (callback != null)
            {
                callback();
            }
            if (this._calls[index] == null)
            {
                this.CloseView();
            }
        }

        public void Show(string content, SureCallback callback, SureCallback cancelCall = null)
        {
            this.content = content;
            this._calls[0] = callback;
            this._calls[1] = cancelCall;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Equipment/AlertPanel.assetbundle";
            }
        }
    }
}

