﻿namespace com.game.module.Equipment.components
{
    using com.game.basic;
    using System;
    using System.Collections.Generic;

    public class AttTips
    {
        public static List<int> attackAttList = new List<int> { 0x19, 0x1a, 0x1b, 0x1c, 0x12, 0x1d, 0x18, 30, 0x1f, 0x20 };
        public static List<int> defendAttList = new List<int> { 0x21, 0x22, 0x23, 0x24, 0x13, 0x16 };

        public static string GetAdditionAttriContent(Dictionary<int, int> oriAttriDic, Dictionary<int, int> addAttriDic)
        {
            string str3;
            string str = string.Empty;
            foreach (KeyValuePair<int, int> pair in oriAttriDic)
            {
                float num = 0f;
                float num2 = 0f;
                if ((attackAttList.IndexOf(pair.Key) > -1) || (defendAttList.IndexOf(pair.Key) > -1))
                {
                    num = ((float) pair.Value) / 100f;
                    if (addAttriDic.ContainsKey(pair.Key))
                    {
                        num2 = ((float) addAttriDic[pair.Key]) / 100f;
                        str3 = str;
                        string[] textArray1 = new string[] { str3, CategoryType.GetAttName(pair.Key), ":", num.ToString(), "[ff0000](+", num2.ToString(), "%)[-]\n" };
                        str = string.Concat(textArray1);
                    }
                    else
                    {
                        str3 = str;
                        string[] textArray2 = new string[] { str3, CategoryType.GetAttName(pair.Key), ":", num.ToString(), "%\n" };
                        str = string.Concat(textArray2);
                    }
                }
                else
                {
                    num = (float) pair.Value;
                    if (addAttriDic.ContainsKey(pair.Key))
                    {
                        num2 = (float) addAttriDic[pair.Key];
                        str3 = str;
                        string[] textArray3 = new string[] { str3, CategoryType.GetAttName(pair.Key), ":", num.ToString(), "[ff0000](+", num2.ToString(), ")[-]\n" };
                        str = string.Concat(textArray3);
                    }
                    else
                    {
                        str3 = str;
                        string[] textArray4 = new string[] { str3, CategoryType.GetAttName(pair.Key), ":", num.ToString(), "\n" };
                        str = string.Concat(textArray4);
                    }
                }
            }
            if (addAttriDic.Count > oriAttriDic.Count)
            {
                foreach (KeyValuePair<int, int> pair2 in addAttriDic)
                {
                    if (!oriAttriDic.ContainsKey(pair2.Key))
                    {
                        string str2 = string.Empty;
                        if ((attackAttList.IndexOf(pair2.Key) > -1) || (defendAttList.IndexOf(pair2.Key) > -1))
                        {
                            str2 = ((((float) pair2.Value) / 100f)).ToString() + "%";
                        }
                        else
                        {
                            str2 = pair2.Value.ToString();
                        }
                        str3 = str;
                        string[] textArray5 = new string[] { str3, "[ff0000]", CategoryType.GetAttName(pair2.Key), ":", str2, "[-]\n" };
                        str = string.Concat(textArray5);
                    }
                }
            }
            return str;
        }

        public static string GetNoAddAttriContent(Dictionary<int, int> attriDic)
        {
            string str = string.Empty;
            foreach (KeyValuePair<int, int> pair in attriDic)
            {
                string str2 = string.Empty;
                if ((attackAttList.IndexOf(pair.Key) > -1) || (defendAttList.IndexOf(pair.Key) > -1))
                {
                    str2 = ((((float) pair.Value) / 100f)).ToString() + "%";
                }
                else
                {
                    str2 = pair.Value.ToString();
                }
                string str3 = str;
                string[] textArray1 = new string[] { str3, CategoryType.GetAttName(pair.Key), ":", str2, "\n" };
                str = string.Concat(textArray1);
            }
            return str;
        }
    }
}

