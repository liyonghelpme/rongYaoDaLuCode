﻿namespace com.game.module.Equipment.components
{
    using System;

    public enum CellStates
    {
        NoMaterial,
        CanMix,
        CanPutIn,
        HadPutIn,
        Normal
    }
}

