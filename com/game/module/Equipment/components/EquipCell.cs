﻿namespace com.game.module.Equipment.components
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using System;

    public class EquipCell : ItemCell
    {
        private CellStates cellState = CellStates.Normal;
        private string content = string.Empty;
        private SysItemsVo itemTemplate;
        private UISprite selectedBg;
        private uint templateId;

        protected override void InitView()
        {
            base.InitView();
            if (base.background == null)
            {
                base.background = base.FindInChild<UISprite>("background");
            }
            if (base.highLight == null)
            {
                base.highLight = base.FindInChild<UISprite>("highlight");
            }
            if (base.label == null)
            {
                base.label = base.FindInChild<UILabel>("label");
            }
            this.selectedBg = NGUITools.FindInChild<UISprite>(base.gameObject, "selectedBg");
        }

        public void Selected(bool value)
        {
            if (this.selectedBg != null)
            {
                this.selectedBg.SetActive(value);
            }
        }

        protected override void UpdateLabel()
        {
            if (null != base.label)
            {
                base.label.text = this.CellLabContent;
            }
        }

        public string CellLabContent
        {
            get
            {
                return this.content;
            }
            set
            {
                this.content = value;
                base.UpdateCell();
            }
        }

        public CellStates CellState
        {
            get
            {
                return this.cellState;
            }
            set
            {
                this.cellState = value;
            }
        }

        protected override string iconAtlasPath
        {
            get
            {
                return "ItemIconAtlas";
            }
        }

        protected override string iconSpriteName
        {
            get
            {
                if (this.itemTemplate == null)
                {
                    return "100000";
                }
                return (string.Empty + this.itemTemplate.icon);
            }
        }

        public override SysItemsVo itemVo
        {
            get
            {
                return this.itemTemplate;
            }
        }

        public uint TemplateId
        {
            get
            {
                return this.templateId;
            }
            set
            {
                this.templateId = value;
                this.itemTemplate = BaseDataMgr.instance.getGoodsVo(this.templateId);
                base.UpdateCell();
            }
        }
    }
}

