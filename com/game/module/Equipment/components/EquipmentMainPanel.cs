﻿namespace com.game.module.Equipment.components
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using com.game.module.General;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class EquipmentMainPanel : BaseView<EquipmentMainPanel>, IEquipMainPanel
    {
        private MainToolView _mainToolbarView;
        private List<System.Type> classList = new List<System.Type>();
        private Button closeBtn;
        private ToEquipmentData configureData;
        private HeaderItemView curHeaderItemView;
        private int curIndex;
        private int curPageIndex = -1;
        private UIToggle curToggle;
        private GameObject headerItemGo;
        private List<HeaderItemView> headerItemViewList = new List<HeaderItemView>();
        private UUGUIGrid headGrid;
        private List<HeaderItemView> inActiveList = new List<HeaderItemView>();
        private GameObject leftGo;
        private UISprite mainBg;
        private Dictionary<int, IEquipmentPanel> pageList = new Dictionary<int, IEquipmentPanel>();
        private List<GameObject> panelGoList = new List<GameObject>();
        private List<UIToggle> tabButtonList = new List<UIToggle>();

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        public override void CloseView()
        {
            base.CloseView();
            this.Dispose();
        }

        private void ConfigureUI()
        {
            UIToggle.current = this.tabButtonList[this.curIndex];
            for (int i = 0; i < this.tabButtonList.Count; i++)
            {
                this.tabButtonList[i].value = false;
            }
            this.tabButtonList[this.curIndex].value = true;
            if (this.pageList.ContainsKey(this.curPageIndex))
            {
                this.pageList[this.curPageIndex].GetGameObject.SetActive(false);
            }
            this.SetIndex(this.curIndex);
            this.InitLeftHeaderView();
            this.configureData = null;
        }

        private void CreateHeaderItemView(GeneralTemplateInfo info, int index)
        {
            HeaderItemView view;
            if (this.inActiveList.Count > 0)
            {
                view = this.inActiveList[this.inActiveList.Count - 1];
                this.inActiveList.RemoveAt(this.inActiveList.Count - 1);
                this.headerItemViewList.Add(view);
            }
            else if (index >= this.headerItemViewList.Count)
            {
                view = new HeaderItemView(NGUITools.AddChild(this.headGrid.gameObject, this.headerItemGo), new Action<HeaderItemView>(this.OnClickHeaderItemView));
                this.headerItemViewList.Add(view);
            }
            else
            {
                view = this.headerItemViewList[index];
            }
            view.Info = info;
        }

        private void DiamondBtnClickHandler(GameObject go)
        {
            Singleton<BuyVigourView>.Instance.Show();
        }

        public void Dispose()
        {
            foreach (IEquipmentPanel panel in this.pageList.Values)
            {
                panel.Dispose();
            }
        }

        private void GoldBtnClickHandler(GameObject go)
        {
            Singleton<BuyVigourView>.Instance.Show();
        }

        protected override void HandleAfterOpenView()
        {
            GlobalAPI.facade.Add(0x10, new NoticeListener(this.OnChangeScene));
            this._mainToolbarView.isActive = true;
            this.ConfigureUI();
        }

        protected override void HandleBeforeCloseView()
        {
            GlobalAPI.facade.Remove(0x10, new NoticeListener(this.OnChangeScene));
            this._mainToolbarView.isActive = false;
        }

        private void HeaderSetDefault()
        {
            int num = 0;
            PItems equipItemInfo = null;
            if (this.configureData != null)
            {
                for (int i = 0; i < this.headerItemViewList.Count; i++)
                {
                    if (this.headerItemViewList[i].Info.Id == this.configureData.generalInfo.id)
                    {
                        num = i;
                        equipItemInfo = this.configureData.equipInfo;
                        break;
                    }
                }
            }
            if (this.curHeaderItemView != null)
            {
                this.curHeaderItemView.Selected = false;
            }
            this.curHeaderItemView = this.headerItemViewList[num];
            this.headerItemViewList[num].Selected = true;
            this.headerItemViewList[num].SetEquipIcons(equipItemInfo);
            this.headGrid.updateItemView();
        }

        protected override void Init()
        {
            this.classList.Add(typeof(UpgradePanel));
            this.classList.Add(typeof(StrengthPanel));
            this.classList.Add(typeof(InsertPanel));
            this.classList.Add(typeof(MixPanel));
            this.mainBg = base.FindInChild<UISprite>("EquipmentPanel/bgs/mainBg");
            this._mainToolbarView = new MainToolView(base.FindChild("EquipmentPanel/toolbar"));
            string[] strArray = new string[] { "EquipmentPanel/tab_buttons/upgradeBtn", "EquipmentPanel/tab_buttons/strengthBtn", "EquipmentPanel/tab_buttons/InsertBtn", "EquipmentPanel/tab_buttons/mixBtn" };
            for (int i = 0; i < strArray.Length; i++)
            {
                this.tabButtonList.Add(base.FindInChild<UIToggle>(strArray[i]));
                EventDelegate.Add(this.tabButtonList[i].onChange, new EventDelegate.Callback(this.TabBtnClickHandler));
            }
            this.curToggle = this.tabButtonList[0];
            this.curToggle.SetActive(true);
            this.closeBtn = base.FindInChild<Button>("EquipmentPanel/closeBtn");
            this.headGrid = base.FindInChild<UUGUIGrid>("EquipmentPanel/left/grid");
            this.headerItemGo = NGUITools.FindChild(this.gameObject, "EquipmentPanel/left/grid/bigHeader");
            HeaderItemView item = new HeaderItemView(this.headerItemGo, new Action<HeaderItemView>(this.OnClickHeaderItemView));
            this.inActiveList.Add(item);
            this.leftGo = NGUITools.FindChild(this.gameObject, "EquipmentPanel/left");
            GameObject obj2 = NGUITools.FindChild(this.gameObject, "EquipmentPanel/right/upgradePanel");
            this.panelGoList.Add(obj2);
            GameObject obj3 = NGUITools.FindChild(this.gameObject, "EquipmentPanel/right/strengthPanel");
            this.panelGoList.Add(obj3);
            GameObject obj4 = NGUITools.FindChild(this.gameObject, "EquipmentPanel/right/insertPanel");
            this.panelGoList.Add(obj4);
            GameObject obj5 = NGUITools.FindChild(this.gameObject, "EquipmentPanel/right/mixPanel");
            this.panelGoList.Add(obj5);
            this.curPageIndex = 0;
            this.SetIndex(0);
            this.InitLeftHeaderView();
            this.Register();
        }

        private void InitLeftHeaderView()
        {
            List<GeneralTemplateInfo> list = Singleton<GeneralMode>.Instance.HaveGeneralInfoList();
            for (int i = 0; i < list.Count; i++)
            {
                this.CreateHeaderItemView(list[i], i);
            }
            for (int j = list.Count; j < this.headerItemViewList.Count; j++)
            {
                this.headerItemViewList[j].HeaderGo.SetActive(false);
            }
            this.HeaderSetDefault();
        }

        private void OnChangeScene(int type, int v1, int v2, object d)
        {
            if (GlobalData.isInCopy)
            {
                this.CloseView();
            }
        }

        private void OnClickHeaderItemView(HeaderItemView headerItemView)
        {
            if (this.curHeaderItemView != null)
            {
                this.curHeaderItemView.selectedObj.SetActive(false);
                this.curHeaderItemView.Selected = false;
            }
            this.curHeaderItemView = headerItemView;
            this.curHeaderItemView.Selected = true;
            headerItemView.SetEquipIcons(null);
            this.headGrid.updateItemView();
        }

        public void OpenEquipment(ToEquipmentData data = null, int index = 0)
        {
            this.configureData = data;
            this.curIndex = index;
            this.OpenView();
        }

        private void Register()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
        }

        private void SetIndex(int index)
        {
            if (index == 3)
            {
                this.leftGo.SetActive(false);
            }
            else
            {
                this.leftGo.SetActive(true);
            }
            if (!this.pageList.ContainsKey(index))
            {
                this.panelGoList[index].SetActive(true);
                this.pageList.Add(index, Activator.CreateInstance(this.classList[index]) as IEquipmentPanel);
                this.pageList[index].InitView(this.panelGoList[index], this);
            }
            else
            {
                this.pageList[index].GetGameObject.SetActive(true);
            }
            if ((Singleton<EquipmentMode>.Instance.wearEquipInfo.curSelectedEquipItemInfo != null) || (Singleton<EquipmentMode>.Instance.wearEquipInfo.curSelectedEquipTemplateInfo != null))
            {
                this.pageList[index].UpdateView(Singleton<EquipmentMode>.Instance.wearEquipInfo.curSelectedEquipItemInfo, Singleton<EquipmentMode>.Instance.wearEquipInfo.curSelectedEquipTemplateInfo);
            }
        }

        private void TabBtnClickHandler()
        {
            UIToggle current = UIToggle.current;
            if (current.value && (current != this.curToggle))
            {
                this.curToggle = current;
                if (this.pageList.ContainsKey(this.curPageIndex))
                {
                    this.pageList[this.curPageIndex].GetGameObject.SetActive(false);
                }
                this.curPageIndex = this.tabButtonList.IndexOf(current);
                this.Dispose();
                this.SetIndex(this.curPageIndex);
            }
        }

        private void TiliBtnClickHandler(GameObject go)
        {
            Singleton<BuyVigourView>.Instance.Show();
        }

        public void UpdatePageView(PItems itemInfo, SysItemsVo template)
        {
            this.pageList[this.curPageIndex].UpdateView(itemInfo, template);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Equipment/Equipment.assetbundle";
            }
        }
    }
}

