﻿namespace com.game.module.Equipment.components
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Equipment.utils;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GetItemTipsPanel : BaseView<GetItemTipsPanel>
    {
        private Button bgBtn;
        private Button closeBtn;
        private UILabel getCountLabel;
        private UIGrid grid;
        private EquipCell itemCell;
        private GameObject itemGo;
        private UILabel itemNameLab;
        private SysItemsVo itemTemplate;
        private List<LinkLab> labList = new List<LinkLab>();

        private void ClickLabHandler()
        {
            IView panelByPannelType = PanelTypeUtils.GetPanelByPannelType(base.LastOpenedPanelType);
            if (panelByPannelType != null)
            {
                panelByPannelType.CloseView();
            }
            this.CloseView();
        }

        private void CloneItem(int id)
        {
            if (null == this.itemGo)
            {
                this.itemGo = base.FindChild("panel/getWay/grid/item");
            }
            LinkLab item = NGUITools.AddChild(this.grid.gameObject, this.itemGo).AddMissingComponent<LinkLab>();
            this.labList.Add(item);
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            GlobalAPI.facade.Add(0x10, new NoticeListener(this.OnChangeScene));
            int num = 0;
            this.itemCell.TemplateId = (uint) this.itemTemplate.id;
            this.itemNameLab.text = this.itemTemplate.name;
            foreach (KeyValuePair<int, int> pair in ParseStringUtils.ParseAddAttri(this.itemTemplate.configure, 1f))
            {
                if (num >= this.labList.Count)
                {
                    this.CloneItem(pair.Value);
                }
                this.labList[num].SetActive(true);
                this.labList[num].SetInfo(pair.Value);
                this.labList[num].clickHandler = new System.Action(this.ClickLabHandler);
                num++;
            }
            this.grid.Reposition();
        }

        protected override void HandleBeforeCloseView()
        {
            GlobalAPI.facade.Remove(0x10, new NoticeListener(this.OnChangeScene));
        }

        protected override void Init()
        {
            this.closeBtn = base.FindInChild<Button>("panel/closeBtn");
            this.itemCell = base.FindChild("panel/cell").AddMissingComponent<EquipCell>();
            this.itemNameLab = base.FindInChild<UILabel>("panel/cell/label");
            this.getCountLabel = base.FindInChild<UILabel>("panel/getCountLabel");
            this.bgBtn = base.FindInChild<Button>("panel/bgBtn");
            this.grid = base.FindInChild<UIGrid>("panel/getWay/grid");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
        }

        private void OnChangeScene(int type, int v1, int v2, object d)
        {
            if (GlobalData.isInCopy)
            {
                this.CloseView();
            }
        }

        public void SetInfo(SysItemsVo template, PanelType panelType = 0)
        {
            this.itemTemplate = template;
            this.OpenView(panelType);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Equipment/GetTipsPanel.assetbundle";
            }
        }
    }
}

