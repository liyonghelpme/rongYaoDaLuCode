﻿namespace com.game.module.Equipment.components
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.module.Role;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class HeaderItemView
    {
        private Button btn;
        private Button cell;
        private GameObject cellGo;
        private UIGrid cellGrid;
        private List<EquipCell> cellList = new List<EquipCell>();
        private UISprite cellSelected;
        private Action<HeaderItemView> clickCallback;
        private EquipCell curSelectedCell;
        private SortedDictionary<ulong, PItems> equipmentsDic;
        private UISprite fangjuIcon;
        private GameObject go;
        private UISprite headerIcon;
        private GeneralTemplateInfo info;
        private UILabel nameLabel;
        private List<PItems> personList = new List<PItems>();
        private bool selected;
        public GameObject selectedObj;
        private UISprite shipingIcon;
        private UISprite shoeIcon;
        private UISprite weaponIcon;

        public HeaderItemView(GameObject gameObject, Action<HeaderItemView> clickAction)
        {
            this.go = gameObject;
            this.clickCallback = clickAction;
            this.headerIcon = NGUITools.FindInChild<UISprite>(this.go, "default/headerSprite");
            this.nameLabel = NGUITools.FindInChild<UILabel>(this.go, "default/nameLabel");
            this.selectedObj = NGUITools.FindChild(this.go, "selected");
            this.btn = this.go.GetComponent<Button>();
            this.cellGrid = NGUITools.FindInChild<UIGrid>(this.go, "selected/grid");
            this.cellSelected = NGUITools.FindInChild<UISprite>(this.go, "selected/grid/item/selected");
            this.weaponIcon = NGUITools.FindInChild<UISprite>(this.go, "selected/weapon/weaponItem");
            this.fangjuIcon = NGUITools.FindInChild<UISprite>(this.go, "selected/fangju/fangjuItem");
            this.shipingIcon = NGUITools.FindInChild<UISprite>(this.go, "selected/shiping/shipingItem");
            this.shoeIcon = NGUITools.FindInChild<UISprite>(this.go, "selected/shoe/shoeItem");
            for (int i = 0; i < 4; i++)
            {
                this.CloneCell();
            }
            this.cellGrid.Reposition();
            this.InitEvent();
        }

        private EquipCell CloneCell()
        {
            if (null == this.cellGo)
            {
                this.cellGo = NGUITools.FindChild(this.go, "selected/grid/item");
            }
            EquipCell item = NGUITools.AddChild(this.cellGrid.gameObject, this.cellGo).AddMissingComponent<EquipCell>();
            item.Selected(false);
            item.onClick = new UIWidgetContainer.VoidDelegate(this.OnCellClickHandler);
            this.cellList.Add(item);
            return item;
        }

        private void EquipDismountStoneSucceed(int type, int v1, int v2, object data)
        {
            if (Singleton<EquipmentMode>.Instance.stoneInsertInfo.equipItemInfo.generalId == this.info.generalInfo.id)
            {
                this.SetEquipIcons(Singleton<EquipmentMode>.Instance.stoneInsertInfo.equipItemInfo);
            }
        }

        private void EquipInsertStoneSucceed(int type, int v1, int v2, object data)
        {
            if (Singleton<EquipmentMode>.Instance.stoneInsertInfo.equipItemInfo.generalId == this.info.generalInfo.id)
            {
                this.SetEquipIcons(Singleton<EquipmentMode>.Instance.stoneInsertInfo.equipItemInfo);
            }
        }

        private void EquipStrengthSucceed(int type, int v1, int v2, object data)
        {
            if (Singleton<EquipmentMode>.Instance.strengthInfo.equipItemInfo.generalId == this.info.generalInfo.id)
            {
                this.SetEquipIcons(Singleton<EquipmentMode>.Instance.strengthInfo.equipItemInfo);
            }
        }

        private void EquipUpgradeSucceed(int type, int v1, int v2, object data)
        {
            if (Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo.generalId == this.info.generalInfo.id)
            {
                this.SetEquipIcons(Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo);
            }
            Singleton<EquipmentMainPanel>.Instance.UpdatePageView(Singleton<EquipmentMode>.Instance.upgradeInfo.upgradeSucEquipItemInfo, null);
        }

        private void InitEvent()
        {
            this.btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
            Singleton<BagMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateEquipCell);
            Singleton<EquipmentMode>.Instance.upgradeInfo.Add(3, new NoticeListener(this.EquipUpgradeSucceed));
            Singleton<EquipmentMode>.Instance.strengthInfo.Add(4, new NoticeListener(this.EquipStrengthSucceed));
            Singleton<EquipmentMode>.Instance.stoneInsertInfo.Add(5, new NoticeListener(this.EquipInsertStoneSucceed));
            Singleton<EquipmentMode>.Instance.stoneInsertInfo.Add(6, new NoticeListener(this.EquipDismountStoneSucceed));
            Singleton<EquipmentMode>.Instance.upgradeInfo.Add(1, new NoticeListener(this.PutInMaterialSucceed));
        }

        private void OnCellClickHandler(GameObject go)
        {
            if (this.curSelectedCell != null)
            {
                this.curSelectedCell.Selected(false);
            }
            EquipCell component = go.GetComponent<EquipCell>();
            this.curSelectedCell = component;
            this.curSelectedCell.Selected(true);
            int index = this.cellList.IndexOf(component);
            Singleton<EquipmentMode>.Instance.wearEquipInfo.curSelectedEquipItemInfo = component.itemInfo;
            int num3 = index + 1;
            uint id = uint.Parse(this.info.generalInfo.defaultEquipIdDic[num3.ToString()]);
            SysItemsVo template = BaseDataMgr.instance.getGoodsVo(id);
            Singleton<EquipmentMode>.Instance.wearEquipInfo.curSelectedEquipTemplateInfo = template;
            Singleton<EquipmentMainPanel>.Instance.UpdatePageView(component.itemInfo, template);
        }

        private void OnClick(GameObject go)
        {
            if (this.clickCallback != null)
            {
                this.clickCallback(this);
            }
        }

        private void PutInMaterialSucceed(int type, int v1, int v2, object data)
        {
            if (Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo.generalId == this.info.generalInfo.id)
            {
                this.SetEquipIcons(Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo);
            }
        }

        private void SetDefaultEquipIcon()
        {
            uint[] numArray = new uint[] { 0x30d37, 0x30d38, 0x30d39, 0x30d3a };
            for (int i = 0; i < this.cellList.Count; i++)
            {
                this.cellList[i].TemplateId = numArray[i];
                this.cellList[i].CellLabContent = "无装备";
            }
        }

        public void SetEquipIcons(PItems equipItemInfo = null)
        {
            for (int i = 0; i < this.cellList.Count; i++)
            {
                this.cellList[i].SetActive(true);
                this.cellList[i].Selected(false);
                this.cellList[i].itemInfo = null;
            }
            foreach (KeyValuePair<int, PItems> pair in this.info.generalInfo.wornEquipItemDic)
            {
                int num2 = pair.Key - 1;
                this.cellList[num2].SetActive(true);
                this.cellList[num2].Selected(false);
                this.cellList[num2].itemInfo = pair.Value;
                this.cellList[num2].TemplateId = pair.Value.templateId;
                this.cellList[num2].CellLabContent = string.Empty;
            }
            this.cellGrid.Reposition();
            int num3 = 0;
            for (int j = 0; j < this.cellList.Count; j++)
            {
                if (equipItemInfo != null)
                {
                    if ((this.cellList[j].itemInfo == null) || (equipItemInfo.id != this.cellList[j].itemInfo.id))
                    {
                        continue;
                    }
                    num3 = j;
                    break;
                }
                if (this.cellList[j].itemInfo != null)
                {
                    num3 = j;
                    break;
                }
            }
            this.curSelectedCell = this.cellList[num3];
            this.cellList[num3].Selected(true);
            Singleton<EquipmentMode>.Instance.wearEquipInfo.curSelectedEquipItemInfo = this.cellList[num3].itemInfo;
            int num6 = num3 + 1;
            uint id = uint.Parse(this.info.generalInfo.defaultEquipIdDic[num6.ToString()]);
            SysItemsVo template = BaseDataMgr.instance.getGoodsVo(id);
            Singleton<EquipmentMainPanel>.Instance.UpdatePageView(this.cellList[num3].itemInfo, template);
        }

        private void UpdateEquipCell(object sender, int code)
        {
            if (code == BagMode.UPDATE_EQUIP)
            {
            }
        }

        public GameObject HeaderGo
        {
            get
            {
                return this.go;
            }
        }

        public GeneralTemplateInfo Info
        {
            get
            {
                return this.info;
            }
            set
            {
                this.info = value;
                this.headerIcon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                this.headerIcon.spriteName = this.info.sysGeneralVo.icon_id.ToString();
                this.headerIcon.MakePixelPerfect();
                this.nameLabel.text = this.info.sysGeneralVo.name;
                this.SetDefaultEquipIcon();
            }
        }

        public bool Selected
        {
            get
            {
                return this.selected;
            }
            set
            {
                this.selected = value;
                if (this.selected)
                {
                    this.selectedObj.SetActive(true);
                }
                else
                {
                    this.selectedObj.SetActive(false);
                }
            }
        }
    }
}

