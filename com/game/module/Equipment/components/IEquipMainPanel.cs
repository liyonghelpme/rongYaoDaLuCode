﻿namespace com.game.module.Equipment.components
{
    using com.game.data;
    using PCustomDataType;
    using System;

    public interface IEquipMainPanel
    {
        void UpdatePageView(PItems itemInfo, SysItemsVo template);
    }
}

