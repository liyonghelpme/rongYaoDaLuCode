﻿namespace com.game.module.Equipment.components
{
    using com.game.data;
    using PCustomDataType;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface IEquipmentPanel
    {
        void Dispose();
        void InitView(GameObject gameObject, IEquipMainPanel mainPanel);
        void UpdateView(PItems itemInfo = null, SysItemsVo equipItemTemplate = null);

        GameObject GetGameObject { get; }
    }
}

