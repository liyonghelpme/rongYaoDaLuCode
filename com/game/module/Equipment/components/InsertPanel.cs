﻿namespace com.game.module.Equipment.components
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using com.game.Public.Message;
    using com.game.vo;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class InsertPanel : IEquipmentPanel
    {
        private GameObject cellGo;
        private List<EquipCell> cellList = new List<EquipCell>();
        private EquipCell curRightcell;
        private StoneInsertItemView curSelectedItemView;
        private Button dismountBtn;
        private UILabel equipNameLabel;
        private Button explainBtn;
        private GameObject go;
        private Button insertBtn;
        private bool isNoEquipState;
        private GameObject itemViewGo;
        private UIGrid leftGrid;
        private UIGrid rightGrid;
        private InsertInfo stoneInsertInfo;
        private GameObject stoneItemGo;
        private List<StoneInsertItemView> stoneItemViewList = new List<StoneInsertItemView>();

        private EquipCell CloneCell()
        {
            if (null == this.cellGo)
            {
                this.cellGo = NGUITools.FindChild(this.go, "rightView/grid/item");
            }
            EquipCell item = NGUITools.AddChild(this.rightGrid.gameObject, this.cellGo).AddMissingComponent<EquipCell>();
            this.cellList.Add(item);
            return item;
        }

        private void CloneItemView()
        {
            if (this.itemViewGo == null)
            {
                this.itemViewGo = NGUITools.FindChild(this.go, "leftView/grid/item");
            }
            StoneInsertItemView item = NGUITools.AddChild(this.leftGrid.gameObject, this.itemViewGo).AddMissingComponent<StoneInsertItemView>();
            item.InitView();
            item.onClick = new UIWidgetContainer.VoidDelegate(this.ItemViewClickHandler);
            this.stoneItemViewList.Add(item);
            this.leftGrid.Reposition();
        }

        private void DismountBtnClickHandler(GameObject go)
        {
            if (this.curSelectedItemView == null)
            {
                MessageManager.Show("未选中宝石");
            }
            else
            {
                string content = "取下该宝石需消耗" + this.curSelectedItemView.Template.value2.ToString() + "  @，是否取下？";
                Singleton<AlertPanel>.Instance.Show(content, new SureCallback(this.DismountStoneBtnClickCallback), null);
            }
        }

        private void DismountStoneBtnClickCallback()
        {
            this.stoneInsertInfo.lastDismountStoneTemplate = this.curSelectedItemView.Template;
            Singleton<EquipmentMode>.Instance.SendDismountStone(this.stoneInsertInfo.equipItemInfo.id, (uint) this.curSelectedItemView.Template.id);
        }

        private void DismountStoneSucced(int type, int v1, int v2, object data)
        {
            this.UpdateView(this.stoneInsertInfo.equipItemInfo, null);
        }

        public void Dispose()
        {
        }

        private void ExplainBtnClickHandler(GameObject go)
        {
            Singleton<TipsPanel>.Instance.Show(LanguageManager.GetWord("Equipment.insertExplain"));
        }

        private void InitEvent()
        {
            this.insertBtn.onClick = new UIWidgetContainer.VoidDelegate(this.InsertBtnClickHandler);
            this.dismountBtn.onClick = new UIWidgetContainer.VoidDelegate(this.DismountBtnClickHandler);
            this.explainBtn.onClick = new UIWidgetContainer.VoidDelegate(this.ExplainBtnClickHandler);
            this.stoneInsertInfo.Add(5, new NoticeListener(this.InsertStoneSucceed));
            this.stoneInsertInfo.Add(6, new NoticeListener(this.DismountStoneSucced));
        }

        private void InitStoneView()
        {
            int num = 0;
            List<PItems> attackStoneList = new List<PItems>();
            if ((this.stoneInsertInfo.equipTemplate.subtype == 1) || (this.stoneInsertInfo.equipTemplate.subtype == 3))
            {
                attackStoneList = this.stoneInsertInfo.attackStoneList;
            }
            if ((this.stoneInsertInfo.equipTemplate.subtype == 2) || (this.stoneInsertInfo.equipTemplate.subtype == 4))
            {
                attackStoneList = this.stoneInsertInfo.defendStoneList;
            }
            for (int i = 0; i < this.cellList.Count; i++)
            {
                this.cellList[i].SetActive(false);
            }
            foreach (PItems items in attackStoneList)
            {
                if (num >= this.cellList.Count)
                {
                    EquipCell cell = this.CloneCell();
                }
                this.cellList[num].SetActive(true);
                this.cellList[num].Selected(false);
                this.cellList[num].itemInfo = items;
                this.cellList[num].TemplateId = items.templateId;
                this.cellList[num].CellLabContent = Singleton<BagMode>.Instance.GetItemCountByTemplateId(items.templateId).ToString();
                this.cellList[num].onClick = new UIWidgetContainer.VoidDelegate(this.StoneCellClickHandler);
                this.cellList[num].UpdateCell();
                num++;
            }
            this.rightGrid.Reposition();
        }

        public void InitView(GameObject gameObject, IEquipMainPanel mainPanel)
        {
            this.stoneInsertInfo = Singleton<EquipmentMode>.Instance.stoneInsertInfo;
            this.go = gameObject;
            this.equipNameLabel = NGUITools.FindInChild<UILabel>(this.go, "equipNameLabel");
            this.insertBtn = NGUITools.FindInChild<Button>(this.go, "insertBtn");
            this.dismountBtn = NGUITools.FindInChild<Button>(this.go, "dismountBtn");
            this.explainBtn = NGUITools.FindInChild<Button>(this.go, "explainBtn");
            this.rightGrid = NGUITools.FindInChild<UIGrid>(this.go, "rightView/grid");
            this.stoneItemGo = NGUITools.FindChild(this.go, "leftView/grid/item");
            this.leftGrid = NGUITools.FindInChild<UIGrid>(this.go, "leftView/grid");
            for (int i = 0; i < 4; i++)
            {
                this.CloneItemView();
            }
            this.leftGrid.Reposition();
            this.InitEvent();
        }

        private void InsertBtnClickHandler(GameObject go)
        {
            Singleton<EquipmentMode>.Instance.SendStoneInsert(this.stoneInsertInfo.equipItemInfo.id, this.stoneInsertInfo.insertStoneIdList);
        }

        private void InsertStoneSucceed(int type, int v1, int v2, object data)
        {
            this.UpdateView(this.stoneInsertInfo.equipItemInfo, null);
        }

        private void ItemViewClickHandler(GameObject go)
        {
            StoneInsertItemView component = go.GetComponent<StoneInsertItemView>();
            if (this.curSelectedItemView != null)
            {
                this.curSelectedItemView.SetSelected(false);
            }
            this.curSelectedItemView = component;
            component.SetSelected(true);
            if (component.itemViewState == StoneInsertItemViewState.NotInsert)
            {
                int index = this.stoneItemViewList.IndexOf(component);
                component.SetActive(false);
                ulong itemIdByTemplateId = Singleton<BagMode>.Instance.GetItemIdByTemplateId((uint) component.Template.id);
                this.stoneInsertInfo.insertStoneIdList.Remove(itemIdByTemplateId);
                this.stoneInsertInfo.insertStoneTemplateList.Remove(component.Template);
                this.stoneInsertInfo.insertStoneTemplateIdList.Remove((uint) component.Template.id);
                this.UpdateLeftView(null);
            }
        }

        private void ReplaceHadInsert()
        {
            SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(this.stoneInsertInfo.replaceInsertedStoneTid);
            if (MeVo.instance.diam < vo.value2)
            {
                MessageManager.Show("金币不足");
            }
            else
            {
                Singleton<EquipmentMode>.Instance.SendDismountStone(this.stoneInsertInfo.equipItemInfo.id, this.stoneInsertInfo.replaceInsertedStoneTid);
                List<ulong> materialIds = new List<ulong> {
                    this.stoneInsertInfo.curClickStoneItemInfo.id
                };
                Singleton<EquipmentMode>.Instance.SendStoneInsert(this.stoneInsertInfo.equipItemInfo.id, materialIds);
            }
        }

        private void ReplaceHadSelected(uint sameTid, SysItemsVo itemTemplate)
        {
            int index = this.stoneInsertInfo.insertStoneTemplateIdList.IndexOf(sameTid);
            this.stoneItemViewList[index].SetInfo(itemTemplate);
            this.stoneInsertInfo.insertStoneTemplateList[index] = itemTemplate;
            this.stoneInsertInfo.insertStoneTemplateIdList[index] = this.stoneInsertInfo.curClickStoneItemInfo.templateId;
            this.stoneInsertInfo.insertStoneIdList[index - this.stoneInsertInfo.equipItemInfo.hole.Count] = this.stoneInsertInfo.curClickStoneItemInfo.id;
        }

        private void StoneCellClickHandler(GameObject go)
        {
            if (this.curRightcell != null)
            {
                this.curRightcell.Selected(false);
            }
            EquipCell component = go.GetComponent<EquipCell>();
            this.curRightcell = component;
            component.Selected(true);
            PItems itemInfo = component.itemInfo;
            this.stoneInsertInfo.curClickStoneItemInfo = itemInfo;
            SysItemsVo template = BaseDataMgr.instance.getGoodsVo(itemInfo.templateId);
            if (!this.stoneInsertInfo.CanInsertStone(template))
            {
                string content = "已存在同属性的宝石，进行替换需要消耗金币，是否替换？";
                Singleton<AlertPanel>.Instance.Show(content, new SureCallback(this.ReplaceHadInsert), null);
            }
            else
            {
                uint sameCategoryTid = this.stoneInsertInfo.GetSameCategoryTid(template);
                if (sameCategoryTid != 0)
                {
                    this.ReplaceHadSelected(sameCategoryTid, template);
                }
                else if (this.stoneInsertInfo.insertStoneTemplateIdList.Count <= 3)
                {
                    this.stoneInsertInfo.insertStoneTemplateList.Add(template);
                    this.stoneInsertInfo.insertStoneTemplateIdList.Add(this.stoneInsertInfo.curClickStoneItemInfo.templateId);
                    this.stoneInsertInfo.insertStoneIdList.Add(this.stoneInsertInfo.curClickStoneItemInfo.id);
                    this.UpdateLeftView(template);
                }
            }
        }

        public void UpdateLeftView(SysItemsVo itemTemplate = null)
        {
            if (this.curSelectedItemView != null)
            {
                this.curSelectedItemView.SetSelected(false);
            }
            int count = this.stoneInsertInfo.insertStoneTemplateIdList.Count;
            for (int i = this.stoneInsertInfo.equipItemInfo.hole.Count; i < this.stoneInsertInfo.insertStoneTemplateList.Count; i++)
            {
                this.stoneItemViewList[i].itemViewState = StoneInsertItemViewState.NotInsert;
                this.stoneItemViewList[i].SetInfo(this.stoneInsertInfo.insertStoneTemplateList[i]);
                this.stoneItemViewList[i].SetSelected(true);
                this.stoneItemViewList[i].SetActive(true);
            }
            for (int j = this.stoneInsertInfo.insertStoneTemplateList.Count; j < 4; j++)
            {
                this.stoneItemViewList[j].SetActive(false);
            }
            this.curSelectedItemView = null;
            this.leftGrid.Reposition();
        }

        public void UpdateView(PItems itemInfo = null, SysItemsVo equipItemTemplate = null)
        {
            this.curSelectedItemView = null;
            if (itemInfo == null)
            {
                itemInfo = new PItems();
                itemInfo.templateId = (uint) equipItemTemplate.id;
                this.isNoEquipState = true;
            }
            else
            {
                this.isNoEquipState = false;
            }
            this.stoneInsertInfo = Singleton<EquipmentMode>.Instance.stoneInsertInfo;
            this.stoneInsertInfo.equipItemInfo = itemInfo;
            this.stoneInsertInfo.equipTemplate = BaseDataMgr.instance.getGoodsVo(itemInfo.templateId);
            this.stoneInsertInfo.insertStoneIdList.Clear();
            this.stoneInsertInfo.insertStoneTemplateIdList.Clear();
            this.stoneInsertInfo.insertStoneTemplateList.Clear();
            this.equipNameLabel.text = this.stoneInsertInfo.equipTemplate.name;
            if (!this.isNoEquipState)
            {
                this.InitStoneView();
            }
            for (int i = 0; i < this.stoneItemViewList.Count; i++)
            {
                if (this.stoneItemViewList[i] != null)
                {
                    this.stoneItemViewList[i].SetActive(false);
                    this.stoneItemViewList[i].HadInserted(false);
                }
            }
            for (int j = 0; j < itemInfo.hole.Count; j++)
            {
                SysItemsVo itemTemplate = BaseDataMgr.instance.getGoodsVo(itemInfo.hole[j].tid);
                this.stoneItemViewList[j].SetInfo(itemTemplate);
                this.stoneItemViewList[j].SetActive(true);
                this.stoneItemViewList[j].HadInserted(true);
                this.stoneItemViewList[j].SetSelected(false);
                this.stoneItemViewList[j].itemViewState = StoneInsertItemViewState.HadInsert;
                this.stoneInsertInfo.insertStoneTemplateList.Add(itemTemplate);
                this.stoneInsertInfo.insertStoneTemplateIdList.Add(itemInfo.hole[j].tid);
            }
            this.leftGrid.Reposition();
        }

        public GameObject GetGameObject
        {
            get
            {
                return this.go;
            }
        }
    }
}

