﻿namespace com.game.module.Equipment.components
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class LinkLab : Button
    {
        private BoxCollider boxCollider;
        public System.Action clickHandler;
        private int copyId;
        private UILabel lab;

        private void ClickHandler(GameObject go)
        {
            Singleton<DungeonInfoView>.Instance.Show(this.copyId);
            if (this.clickHandler != null)
            {
                this.clickHandler();
            }
        }

        private void Init()
        {
            base.InitView();
            this.boxCollider = base.gameObject.AddMissingComponent<BoxCollider>();
            this.lab = NGUITools.FindInChild<UILabel>(base.gameObject, "Label");
            this.InitEvent();
        }

        private void InitEvent()
        {
            base.onClick = new UIWidgetContainer.VoidDelegate(this.ClickHandler);
        }

        public void SetInfo(int id)
        {
            this.Init();
            SysDungeonVo vo = BaseDataMgr.instance.getDungeon((uint) id);
            this.lab.text = "[u]" + vo.name + "[/u]";
            this.boxCollider.size = new Vector3((float) this.lab.width, (float) this.lab.height, 0f);
            this.copyId = id;
        }
    }
}

