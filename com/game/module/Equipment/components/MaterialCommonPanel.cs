﻿namespace com.game.module.Equipment.components
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.utils;
    using com.game.Public.Message;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MaterialCommonPanel : BaseView<MaterialCommonPanel>
    {
        private UILabel addAttriLabel;
        private EquipCell afterCell1;
        private EquipCell afterCell2;
        private EquipCell beforeCell;
        private Button bgBtn;
        private UISprite buyIcon;
        private UILabel chargeLab;
        private Button closeBtn;
        private Button commonBtn;
        private UILabel composeChargeLab;
        private UISprite composeJinbiIcon;
        private UILabel descriptLabel;
        private UILabel fragCountLab;
        private Button getOrLookBtn;
        private bool isHideLeftGo;
        private GameObject leftGo;
        private UILabel materialCountLab;
        private SysItemsVo materialItemTemplate;
        private UILabel origMaterialCountLab;
        private MaterialPanel panelType;

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        private void CommonBtnClickHandler(GameObject go)
        {
            switch (this.panelType)
            {
                case MaterialPanel.MixPanel:
                    Singleton<EquipmentMode>.Instance.SendMaterialMake((uint) this.materialItemTemplate.id, 1);
                    break;

                case MaterialPanel.PutInPanel:
                {
                    for (int i = 0; i < Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo.colorRes.Count; i++)
                    {
                        if (Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo.colorRes[i].tid == this.materialItemTemplate.id)
                        {
                            MessageManager.Show("该材料已放入");
                            return;
                        }
                    }
                    PMaterial material = new PMaterial {
                        num = 1,
                        tid = Singleton<BagMode>.Instance.GetItemIdByTemplateId((uint) this.materialItemTemplate.id)
                    };
                    List<PMaterial> materialIds = new List<PMaterial> {
                        material
                    };
                    Singleton<EquipmentMode>.Instance.SendPutInMaterial(Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo.id, materialIds);
                    break;
                }
            }
        }

        private void GetOrLookBtnClickHandler(GameObject go)
        {
            MaterialPanel panelType = this.panelType;
            if ((panelType == MaterialPanel.MixPanel) || (panelType == MaterialPanel.PutInPanel))
            {
            }
            Singleton<GetItemTipsPanel>.Instance.SetInfo(this.materialItemTemplate, PanelType.NONE);
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            switch (this.panelType)
            {
                case MaterialPanel.MixPanel:
                    this.SetMixPanelInfo();
                    break;

                case MaterialPanel.PutInPanel:
                    this.SetPutInPanelInfo();
                    break;
            }
            this.addAttriLabel.text = string.Empty;
            Dictionary<int, int> attriDic = ParseStringUtils.ParseAddAttri(this.materialItemTemplate.property, 1f);
            this.addAttriLabel.text = AttTips.GetNoAddAttriContent(attriDic);
            this.chargeLab.text = this.materialItemTemplate.sell_price.ToString();
            this.descriptLabel.text = this.materialItemTemplate.desc;
        }

        protected override void Init()
        {
            this.beforeCell = base.FindChild("panel/left/cell1").AddMissingComponent<EquipCell>();
            this.afterCell1 = base.FindChild("panel/left/cell2").AddMissingComponent<EquipCell>();
            this.afterCell2 = base.FindChild("panel/right/cell").AddMissingComponent<EquipCell>();
            this.commonBtn = base.FindInChild<Button>("panel/left/commonBtn");
            this.getOrLookBtn = base.FindInChild<Button>("panel/right/getOrLookBtn");
            this.closeBtn = base.FindInChild<Button>("panel/closeBtn");
            this.bgBtn = base.FindInChild<Button>("panel/bgBtn");
            this.descriptLabel = base.FindInChild<UILabel>("panel/right/descriptLabel");
            this.addAttriLabel = base.FindInChild<UILabel>("panel/right/addAttriLabel");
            this.chargeLab = base.FindInChild<UILabel>("panel/right/chargeLab");
            this.fragCountLab = base.FindInChild<UILabel>("panel/left/cell1/label");
            this.materialCountLab = base.FindInChild<UILabel>("panel/left/cell2/label");
            this.composeChargeLab = base.FindInChild<UILabel>("panel/left/composeChargeLab");
            this.buyIcon = base.FindInChild<UISprite>("panel/buyIcon");
            this.leftGo = base.FindChild("panel/left");
            this.composeJinbiIcon = base.FindInChild<UISprite>("panel/left/Sprite");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.commonBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CommonBtnClickHandler);
            this.getOrLookBtn.onClick = new UIWidgetContainer.VoidDelegate(this.GetOrLookBtnClickHandler);
            Singleton<EquipmentMode>.Instance.upgradeInfo.Add(1, new NoticeListener(this.PutInMaterialSucceed));
            Singleton<EquipmentMode>.Instance.upgradeInfo.Add(9, new NoticeListener(this.MaterialMakeSucceed));
        }

        public void MaterialMakeSucceed(int type, int v1, int v2, object data)
        {
            this.materialCountLab.text = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) this.materialItemTemplate.id).ToString();
            SysMaterialComposeVo materialCompiseVo = BaseDataMgr.instance.GetMaterialCompiseVo((uint) this.materialItemTemplate.id);
            SysItemsVo vo2 = BaseDataMgr.instance.getGoodsVo(materialCompiseVo.frag_tid);
            this.fragCountLab.text = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) vo2.id).ToString() + "/" + materialCompiseVo.frag_num.ToString();
        }

        private void PutInMaterialSucceed(int type, int v1, int v2, object data)
        {
            this.materialCountLab.text = "1/1";
            this.fragCountLab.text = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) this.materialItemTemplate.id).ToString();
            this.CloseView();
        }

        public void SetInfo(SysItemsVo itemTemplate, MaterialPanel panelType, bool isHideLeftGo = false)
        {
            this.materialItemTemplate = itemTemplate;
            this.panelType = panelType;
            this.isHideLeftGo = isHideLeftGo;
            this.OpenView();
        }

        private void SetMixPanelInfo()
        {
            if (this.isHideLeftGo)
            {
                this.leftGo.SetActive(false);
            }
            else
            {
                this.leftGo.SetActive(true);
            }
            this.commonBtn.label.text = "合成";
            this.getOrLookBtn.label.text = "获取碎片";
            SysMaterialComposeVo materialCompiseVo = BaseDataMgr.instance.GetMaterialCompiseVo((uint) this.materialItemTemplate.id);
            Singleton<EquipmentMode>.Instance.upgradeInfo.materialComposeTemplate = materialCompiseVo;
            this.composeChargeLab.SetActive(true);
            this.composeJinbiIcon.SetActive(true);
            if (materialCompiseVo != null)
            {
                this.composeChargeLab.text = "费用：" + materialCompiseVo.charge.ToString();
            }
            if (materialCompiseVo != null)
            {
                SysItemsVo vo2 = BaseDataMgr.instance.getGoodsVo(materialCompiseVo.frag_tid);
                Singleton<EquipmentMode>.Instance.upgradeInfo.fragTemplate = vo2;
                this.beforeCell.TemplateId = (uint) vo2.id;
                this.afterCell1.TemplateId = (uint) this.materialItemTemplate.id;
                this.afterCell2.TemplateId = (uint) this.materialItemTemplate.id;
                this.afterCell2.label.text = this.materialItemTemplate.name;
                this.fragCountLab.text = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) vo2.id).ToString() + "/" + materialCompiseVo.frag_num.ToString();
                this.materialCountLab.text = "0/1";
            }
        }

        private void SetPutInPanelInfo()
        {
            this.leftGo.SetActive(true);
            this.commonBtn.label.text = "放入";
            this.getOrLookBtn.label.text = "查看获取途径";
            this.beforeCell.TemplateId = (uint) this.materialItemTemplate.id;
            this.afterCell1.TemplateId = (uint) this.materialItemTemplate.id;
            this.afterCell2.TemplateId = (uint) this.materialItemTemplate.id;
            this.afterCell2.label.text = this.materialItemTemplate.name;
            int itemCountByTemplateId = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) this.materialItemTemplate.id);
            this.materialCountLab.text = "0/1";
            this.fragCountLab.text = itemCountByTemplateId.ToString();
            this.composeChargeLab.SetActive(false);
            this.composeJinbiIcon.SetActive(false);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Equipment/MaterialCommonPanel.assetbundle";
            }
        }
    }
}

