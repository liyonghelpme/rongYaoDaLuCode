﻿namespace com.game.module.Equipment.components
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using com.game.Public.Message;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MixPanel : IEquipmentPanel
    {
        private Button batchMixBtn;
        private Button buyBtn;
        private GameObject cellGo;
        private UILabel chargeLabel;
        private EquipCell curSelectedLeftCell;
        private EquipCell curSelectedRightCell;
        private int curTabIndex = -1;
        private UIToggle curToggle;
        private GameObject go;
        private List<EquipCell> leftCellList = new List<EquipCell>();
        private UIGrid leftGrid;
        private EquipCell materialCell1;
        private EquipCell materialCell2;
        private EquipCell materialCell3;
        private List<EquipCell> materialCellList = new List<EquipCell>();
        private Button mixBtn;
        private MixInfo mixInfo;
        private List<EquipCell> rightCellList = new List<EquipCell>();
        private UIGrid rightGrid;
        private UILabel stoneTitleLab;
        private List<UIToggle> tabButtonList = new List<UIToggle>();
        private EquipCell targetCell;

        private void BatchMixBtnClickHandler(GameObject go)
        {
            if ((this.mixInfo.curSelectedTemplate != null) && (this.mixInfo.curSelectedTemplate.value1 == 9))
            {
                MessageManager.Show("宝石为最高等级，不可合成");
            }
            else if (this.mixInfo.curSelectedStoneCountInBag < 3)
            {
                MessageManager.Show("材料不够");
            }
            else
            {
                this.mixInfo.curSelectedStoneCountInBag = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) this.mixInfo.curSelectedTemplate.id);
                int num = Mathf.FloorToInt((float) (this.mixInfo.curSelectedStoneCountInBag / 3));
                SysGemComposeVo gemComposeVo = BaseDataMgr.instance.GetGemComposeVo((uint) this.mixInfo.curSelectedTemplate.id);
                SysItemsVo vo2 = BaseDataMgr.instance.getGoodsVo(gemComposeVo.des_tid);
                string[] textArray1 = new string[] { "总共可以合成", vo2.name, num.ToString(), "颗需要消耗", (gemComposeVo.charge * num).ToString(), "@,是否进行批量合成？" };
                string content = string.Concat(textArray1);
                Singleton<AlertPanel>.Instance.Show(content, new SureCallback(this.SureBtnClickCallback), null);
            }
        }

        private void BuyBtnClickHandler(GameObject go)
        {
            Singleton<ShopView>.Instance.OpenView();
        }

        private void ClearCell()
        {
            uint num = 0;
            this.materialCell3.TemplateId = num;
            this.materialCell2.TemplateId = num;
            this.materialCell1.TemplateId = num;
            this.targetCell.TemplateId = 0;
        }

        private void CloneCell(int type)
        {
            if (null == this.cellGo)
            {
                this.cellGo = NGUITools.FindChild(this.go, "item");
            }
            if (type == 0)
            {
                EquipCell item = NGUITools.AddChild(this.leftGrid.gameObject, this.cellGo).AddMissingComponent<EquipCell>();
                item.onClick = new UIWidgetContainer.VoidDelegate(this.LeftCellClickHandler);
                this.leftCellList.Add(item);
            }
            else
            {
                GameObject go = NGUITools.AddChild(this.rightGrid.gameObject, this.cellGo);
                go.RemoveComponent<UIDragScrollView>();
                EquipCell cell2 = go.AddMissingComponent<EquipCell>();
                cell2.onClick = new UIWidgetContainer.VoidDelegate(this.RightCellClickHandler);
                this.rightCellList.Add(cell2);
            }
        }

        public void Dispose()
        {
        }

        private void InitEvent()
        {
            this.mixBtn.onClick = new UIWidgetContainer.VoidDelegate(this.MixBtnClickHandler);
            this.batchMixBtn.onClick = new UIWidgetContainer.VoidDelegate(this.BatchMixBtnClickHandler);
            this.buyBtn.onClick = new UIWidgetContainer.VoidDelegate(this.BuyBtnClickHandler);
            this.mixInfo.Add(7, new NoticeListener(this.MixedUpdate));
            this.mixInfo.Add(8, new NoticeListener(this.MixedUpdate));
        }

        private void InitLeftView()
        {
            for (int i = 0; i < CategoryType.AttackStone.Count; i++)
            {
                this.CloneCell(0);
            }
        }

        private void InitRightView()
        {
            for (int i = 0; i < 9; i++)
            {
                this.CloneCell(1);
            }
        }

        public void InitView(GameObject gameObject, IEquipMainPanel mainPanel)
        {
            this.mixInfo = Singleton<EquipmentMode>.Instance.mixInfo;
            this.go = gameObject;
            string[] strArray = new string[] { "left/tab_btn/attackBtn", "left/tab_btn/defendBtn" };
            for (int i = 0; i < strArray.Length; i++)
            {
                this.tabButtonList.Add(NGUITools.FindInChild<UIToggle>(this.go, strArray[i]));
                EventDelegate.Add(this.tabButtonList[i].onChange, new EventDelegate.Callback(this.TabBtnClickHandler));
            }
            this.curToggle = this.tabButtonList[0];
            this.curToggle.SetActive(true);
            this.stoneTitleLab = NGUITools.FindInChild<UILabel>(this.go, "bg/rightBg/title/Label");
            this.chargeLabel = NGUITools.FindInChild<UILabel>(this.go, "center/chargeLabel");
            this.chargeLabel.text = "费用：";
            this.mixBtn = NGUITools.FindInChild<Button>(this.go, "btns/mixBtn");
            this.batchMixBtn = NGUITools.FindInChild<Button>(this.go, "btns/batchMixBtn");
            this.buyBtn = NGUITools.FindInChild<Button>(this.go, "btns/buyBtn");
            this.leftGrid = NGUITools.FindInChild<UIGrid>(this.go, "left/leftPanel/grid");
            this.rightGrid = NGUITools.FindInChild<UIGrid>(this.go, "right/grid");
            this.materialCell1 = NGUITools.FindChild(this.go, "center/materialCell1").AddMissingComponent<EquipCell>();
            this.materialCell2 = NGUITools.FindChild(this.go, "center/materialCell2").AddMissingComponent<EquipCell>();
            this.materialCell3 = NGUITools.FindChild(this.go, "center/materialCell3").AddMissingComponent<EquipCell>();
            this.materialCellList.Add(this.materialCell1);
            this.materialCellList.Add(this.materialCell2);
            this.materialCellList.Add(this.materialCell3);
            this.targetCell = NGUITools.FindChild(this.go, "center/targetCell").AddMissingComponent<EquipCell>();
            this.InitRightView();
            this.InitLeftView();
            this.SetIndex(0);
            this.InitEvent();
        }

        private void LeftCellClickHandler(GameObject go)
        {
            if (this.curSelectedLeftCell != null)
            {
                this.curSelectedLeftCell.Selected(false);
            }
            EquipCell component = go.GetComponent<EquipCell>();
            this.curSelectedLeftCell = component;
            this.curSelectedLeftCell.Selected(true);
            SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(component.TemplateId);
            this.UpdateRightView(vo.value3);
        }

        private void MixBtnClickHandler(GameObject go)
        {
            if ((this.mixInfo.curSelectedTemplate != null) && (this.mixInfo.curSelectedTemplate.value1 == 9))
            {
                MessageManager.Show("宝石为最高等级，不可合成");
            }
            else if (this.mixInfo.curSelectedStoneCountInBag >= 3)
            {
                Singleton<EquipmentMode>.Instance.SendStoneMix((uint) this.mixInfo.curSelectedTemplate.id, 1);
            }
            else
            {
                MessageManager.Show("材料不够");
            }
        }

        private void MixedUpdate(int type, int v1, int v2, object data)
        {
            this.UpdateRightView(this.mixInfo.curSelectedStoneType);
            this.ClearCell();
        }

        private void RightCellClickHandler(GameObject go)
        {
            if (this.curSelectedRightCell != null)
            {
                this.curSelectedRightCell.Selected(false);
            }
            EquipCell component = go.GetComponent<EquipCell>();
            this.curSelectedRightCell = component;
            this.curSelectedRightCell.Selected(true);
            SysItemsVo itemVo = component.itemVo;
            this.mixInfo.curSelectedStoneType = itemVo.value3;
            this.mixInfo.curSelectedTemplate = itemVo;
            this.mixInfo.curSelectedStoneCountInBag = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) itemVo.id);
            this.ClearCell();
            if (itemVo.value1 == 9)
            {
                uint num = 0;
                this.materialCell3.TemplateId = num;
                this.materialCell2.TemplateId = num;
                this.materialCell1.TemplateId = num;
                this.targetCell.TemplateId = (uint) itemVo.id;
            }
            else
            {
                this.SetMaterialCellInfo(this.mixInfo.curSelectedStoneCountInBag, itemVo);
                this.targetCell.TemplateId = this.mixInfo.GetNextLvStoneTemplateId((uint) itemVo.id);
                SysGemComposeVo gemComposeVo = BaseDataMgr.instance.GetGemComposeVo((uint) itemVo.id);
                if (gemComposeVo != null)
                {
                    this.chargeLabel.text = "费用：" + gemComposeVo.charge.ToString();
                }
            }
        }

        private void SetIndex(int index)
        {
            List<uint> attackHighLvStoneTemplateIdList = new List<uint>();
            if (index == 0)
            {
                attackHighLvStoneTemplateIdList = MixInfo.attackHighLvStoneTemplateIdList;
            }
            else
            {
                attackHighLvStoneTemplateIdList = MixInfo.defendHighLvStoneTemplateIdList;
            }
            for (int i = 0; i < attackHighLvStoneTemplateIdList.Count; i++)
            {
                this.leftCellList[i].SetActive(true);
                this.leftCellList[i].TemplateId = attackHighLvStoneTemplateIdList[i];
                SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(attackHighLvStoneTemplateIdList[i]);
            }
            for (int j = attackHighLvStoneTemplateIdList.Count; j < this.leftCellList.Count; j++)
            {
                this.leftCellList[j].SetActive(false);
            }
            if (this.curSelectedLeftCell != null)
            {
                this.curSelectedLeftCell.Selected(false);
            }
            this.curSelectedLeftCell = this.leftCellList[0];
            this.stoneTitleLab.text = this.curSelectedLeftCell.itemVo.name.Substring(2);
            this.leftCellList[0].Selected(true);
            this.UpdateRightView(this.curSelectedLeftCell.itemVo.value3);
            this.leftGrid.Reposition();
        }

        private void SetMaterialCellInfo(int count, SysItemsVo stoneTemplate)
        {
            this.ClearCell();
            if (count > 3)
            {
                count = 3;
            }
            for (int i = 0; i < count; i++)
            {
                this.materialCellList[i].TemplateId = (uint) stoneTemplate.id;
            }
        }

        private void SureBtnClickCallback()
        {
            int num = Mathf.FloorToInt((float) (this.mixInfo.curSelectedStoneCountInBag / 3));
            Singleton<EquipmentMode>.Instance.SendStoneMix((uint) this.mixInfo.curSelectedTemplate.id, (uint) num);
        }

        private void TabBtnClickHandler()
        {
            UIToggle current = UIToggle.current;
            if (current.value && (current != this.curToggle))
            {
                this.curToggle = current;
                this.curTabIndex = this.tabButtonList.IndexOf(this.curToggle);
                this.SetIndex(this.curTabIndex);
            }
        }

        private void UpdateRightView(int stoneType)
        {
            this.stoneTitleLab.text = this.curSelectedLeftCell.itemVo.name.Substring(2);
            List<StoneInfo> stoneListByType = this.mixInfo.GetStoneListByType(stoneType);
            for (int i = 0; i < 9; i++)
            {
                this.rightCellList[i].SetActive(true);
                this.rightCellList[i].TemplateId = (uint) stoneListByType[i].stoneTemplate.id;
                this.rightCellList[i].CellLabContent = stoneListByType[i].count.ToString();
            }
            this.rightGrid.Reposition();
        }

        public void UpdateView(PItems itemInfo = null, SysItemsVo equipItemTemplate = null)
        {
            this.SetIndex(0);
            this.tabButtonList[0].value = true;
            if (this.curSelectedRightCell != null)
            {
                this.curSelectedRightCell.Selected(false);
            }
            this.ClearCell();
        }

        public GameObject GetGameObject
        {
            get
            {
                return this.go;
            }
        }
    }
}

