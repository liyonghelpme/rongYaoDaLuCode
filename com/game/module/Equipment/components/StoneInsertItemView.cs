﻿namespace com.game.module.Equipment.components
{
    using com.game.basic;
    using com.game.data;
    using com.game.module.Equipment.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class StoneInsertItemView : Button
    {
        private UILabel additionLabel;
        private EquipCell equipCell;
        private UISprite hadInserted;
        private PItems itemInfo;
        public StoneInsertItemViewState itemViewState = StoneInsertItemViewState.NotInsert;
        private UILabel nameLabel;
        private bool selected;
        private UISprite selectedBg;
        private SysItemsVo template;
        private Button upgradeBtn;

        public void HadInserted(bool value)
        {
            this.hadInserted.SetActive(value);
        }

        private void InitEvent()
        {
            this.upgradeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.UpgradeBtnClickHandler);
        }

        public void InitView()
        {
            this.nameLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "nameLabel");
            this.additionLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "additionLabel");
            this.upgradeBtn = NGUITools.FindInChild<Button>(base.gameObject, "upgradeBtn");
            this.equipCell = NGUITools.FindChild(base.gameObject, "equipCell").AddMissingComponent<EquipCell>();
            this.selectedBg = NGUITools.FindInChild<UISprite>(base.gameObject, "selected");
            this.hadInserted = NGUITools.FindInChild<UISprite>(base.gameObject, "hadInserted");
            this.InitEvent();
        }

        public void SetInfo(SysItemsVo itemTemplate)
        {
            this.template = itemTemplate;
            this.equipCell.TemplateId = (uint) itemTemplate.id;
            this.equipCell.UpdateCell();
            this.nameLabel.text = itemTemplate.name;
            this.additionLabel.text = string.Empty;
            foreach (KeyValuePair<int, int> pair in ParseStringUtils.ParseAddAttri(itemTemplate.property, 1f))
            {
                string text = this.additionLabel.text;
                string[] textArray1 = new string[] { text, "+", CategoryType.GetAttName(pair.Key), (((float) pair.Value) / 100f).ToString(), "%\n" };
                this.additionLabel.text = string.Concat(textArray1);
            }
        }

        public void SetItemInfo(PItems info)
        {
        }

        public void SetSelected(bool value)
        {
            this.selected = value;
            if (this.selected)
            {
                this.selectedBg.SetActive(true);
            }
            else
            {
                this.selectedBg.SetActive(false);
            }
        }

        private void UpgradeBtnClickHandler(GameObject go)
        {
        }

        public PItems ItemInfo
        {
            get
            {
                return this.itemInfo;
            }
        }

        public SysItemsVo Template
        {
            get
            {
                return this.template;
            }
        }
    }
}

