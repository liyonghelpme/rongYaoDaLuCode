﻿namespace com.game.module.Equipment.components
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using com.game.module.Equipment.utils;
    using com.game.Public.Message;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StrengthPanel : IEquipmentPanel
    {
        private UILabel afterAttriDescript;
        private BoxCollider afterBoxColider;
        private EquipCell afterEquipCell;
        private GameObject afterStrengthGo;
        private List<UISprite> afterStrengthStarList = new List<UISprite>();
        private UILabel beforeAttriDescript;
        private BoxCollider beforeBoxColider;
        private EquipCell beforeEquipcell;
        private List<UISprite> beforeStrengthStarList = new List<UISprite>();
        private GameObject cellGo;
        private List<EquipCell> cellList = new List<EquipCell>();
        private UILabel chargeLabel;
        private PItems equipItemInfo;
        private GameObject go;
        private bool isNoEquipState;
        private UISprite jianTou;
        private UISprite jinbiIcon;
        private UIGrid materialGrid;
        private UILabel materialLabel;
        private Button strengthBtn;
        private StrengthInfo strengthInfo;

        private EquipCell CloneCell()
        {
            if (null == this.cellGo)
            {
                this.cellGo = NGUITools.FindChild(this.go, "materialList/grid/item");
            }
            EquipCell item = NGUITools.AddChild(this.materialGrid.gameObject, this.cellGo).AddMissingComponent<EquipCell>();
            this.cellList.Add(item);
            return item;
        }

        public void Dispose()
        {
        }

        private void EquipStrengthHandler(int type, int v1, int v2, object data)
        {
            MessageManager.Show(LanguageManager.GetWord("Equipment.strengthSuc"));
            this.UpdateView(this.strengthInfo.strengthedEquipItemInfo, null);
        }

        private void Handler5StarEquip(SysItemsVo itemTemplate)
        {
            SysStrengthVo strenthVo = BaseDataMgr.instance.GetStrenthVo(this.equipItemInfo.stren);
            this.beforeEquipcell.TemplateId = (uint) itemTemplate.id;
            this.beforeEquipcell.CellLabContent = itemTemplate.name;
            int stren = this.equipItemInfo.stren;
            for (int i = 1; i <= 5; i++)
            {
                this.beforeStrengthStarList[i - 1].ShowAsMyself();
            }
            this.beforeAttriDescript.text = string.Empty;
            float rate = 1f + (((float) strenthVo.rate) / 10000f);
            foreach (KeyValuePair<int, int> pair in ParseStringUtils.ParseAddAttri(itemTemplate.property, rate))
            {
                string text = this.beforeAttriDescript.text;
                string[] textArray1 = new string[] { text, CategoryType.GetAttName(pair.Key), "：", pair.Value.ToString(), "\n" };
                this.beforeAttriDescript.text = string.Concat(textArray1);
            }
            this.SetActives(false);
        }

        private void InitEvent()
        {
            this.strengthBtn.onClick = new UIWidgetContainer.VoidDelegate(this.StrengthClickHandler);
            this.strengthInfo.Add(4, new NoticeListener(this.EquipStrengthHandler));
        }

        public void InitView(GameObject gameObject, IEquipMainPanel mainPanel)
        {
            this.strengthInfo = Singleton<EquipmentMode>.Instance.strengthInfo;
            this.go = gameObject;
            this.beforeEquipcell = NGUITools.FindChild(this.go, "beforeStrength/equipCell").AddMissingComponent<EquipCell>();
            this.afterEquipCell = NGUITools.FindChild(this.go, "afterStrength/equipCell").AddMissingComponent<EquipCell>();
            this.beforeAttriDescript = NGUITools.FindInChild<UILabel>(this.go, "beforeStrength/descriptCell/Lab/attriLab");
            this.afterAttriDescript = NGUITools.FindInChild<UILabel>(this.go, "afterStrength/descriptCell/Lab/attriLab");
            this.chargeLabel = NGUITools.FindInChild<UILabel>(this.go, "chargeLabel");
            this.strengthBtn = NGUITools.FindInChild<Button>(this.go, "strengthBtn");
            this.materialGrid = NGUITools.FindInChild<UIGrid>(this.go, "materialList/grid");
            this.afterStrengthGo = NGUITools.FindChild(this.go, "afterStrength");
            this.materialLabel = NGUITools.FindInChild<UILabel>(this.go, "materialLabel");
            this.jianTou = NGUITools.FindInChild<UISprite>(this.go, "jianTou");
            this.jinbiIcon = NGUITools.FindInChild<UISprite>(this.go, "jinbiIcon");
            for (int i = 1; i <= 5; i++)
            {
                UISprite item = NGUITools.FindInChild<UISprite>(this.go, "beforeStrength/equipCell/star/bgGroup/star" + i);
                item.ShowAsGray();
                this.beforeStrengthStarList.Add(item);
                UISprite sprite2 = NGUITools.FindInChild<UISprite>(this.go, "afterStrength/equipCell/star/bgGroup/star" + i);
                sprite2.ShowAsGray();
                this.afterStrengthStarList.Add(sprite2);
            }
            this.beforeBoxColider = NGUITools.FindChild(this.go, "beforeStrength/descriptCell/Lab").GetComponent<BoxCollider>();
            this.afterBoxColider = NGUITools.FindChild(this.go, "afterStrength/descriptCell/Lab").GetComponent<BoxCollider>();
            this.InitEvent();
        }

        private void MaterialCellClickHandler(GameObject go)
        {
            if (!this.isNoEquipState)
            {
                EquipCell component = go.GetComponent<EquipCell>();
                Singleton<GetItemTipsPanel>.Instance.SetInfo(component.itemVo, PanelType.NONE);
            }
        }

        private void SetActives(bool value)
        {
            this.materialGrid.SetActive(value);
            this.afterStrengthGo.SetActive(value);
            this.strengthBtn.SetActive(value);
            this.chargeLabel.SetActive(value);
            this.jianTou.SetActive(value);
            this.materialLabel.SetActive(value);
            this.jinbiIcon.SetActive(value);
        }

        private void StrengthClickHandler(GameObject go)
        {
            if (this.isNoEquipState)
            {
                MessageManager.Show(LanguageManager.GetWord("Equipment.noEquip"));
            }
            else if (this.equipItemInfo.stren >= 5)
            {
                MessageManager.Show(LanguageManager.GetWord("Equipment.strengthToLimit"));
            }
            else if (this.strengthInfo.CanStrength())
            {
                Singleton<EquipmentMode>.Instance.SendEquipStrength(this.equipItemInfo.id);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("Equipment.materialNotEnougth"));
            }
        }

        public void UpdateView(PItems itemInfo = null, SysItemsVo equipItemTemplate = null)
        {
            if (itemInfo == null)
            {
                this.isNoEquipState = true;
                itemInfo = new PItems();
                itemInfo.stren = 0;
                itemInfo.templateId = (uint) equipItemTemplate.id;
            }
            else
            {
                this.isNoEquipState = false;
            }
            this.equipItemInfo = itemInfo;
            this.strengthInfo.equipItemInfo = this.equipItemInfo;
            SysItemsVo itemTemplate = BaseDataMgr.instance.getGoodsVo(this.equipItemInfo.templateId);
            if (this.equipItemInfo.stren == 5)
            {
                this.Handler5StarEquip(itemTemplate);
            }
            else
            {
                this.SetActives(true);
                SysStrengthVo strenthVo = BaseDataMgr.instance.GetStrenthVo(this.equipItemInfo.stren);
                SysStrengthVo vo3 = BaseDataMgr.instance.GetStrenthVo((uint) (this.equipItemInfo.stren + 1));
                uint id = (uint) itemTemplate.id;
                this.beforeEquipcell.TemplateId = id;
                this.afterEquipCell.TemplateId = id;
                string name = itemTemplate.name;
                this.beforeEquipcell.CellLabContent = name;
                this.afterEquipCell.CellLabContent = name;
                if (this.isNoEquipState)
                {
                    this.afterEquipCell.icon.ShowAsGray();
                    this.beforeEquipcell.icon.ShowAsGray();
                }
                else
                {
                    this.afterEquipCell.icon.ShowAsMyself();
                    this.beforeEquipcell.icon.ShowAsMyself();
                }
                this.chargeLabel.text = string.Empty;
                this.chargeLabel.text = this.chargeLabel.text + "费用：" + vo3.charge.ToString();
                int stren = this.equipItemInfo.stren;
                for (int i = 1; i <= 5; i++)
                {
                    if (i > stren)
                    {
                        this.beforeStrengthStarList[i - 1].ShowAsGray();
                    }
                    else
                    {
                        this.beforeStrengthStarList[i - 1].ShowAsMyself();
                    }
                    if (i > (stren + 1))
                    {
                        this.afterStrengthStarList[i - 1].ShowAsGray();
                    }
                    else
                    {
                        this.afterStrengthStarList[i - 1].ShowAsMyself();
                    }
                }
                this.beforeAttriDescript.text = string.Empty;
                float rate = 1f;
                if (strenthVo != null)
                {
                    rate = 1f + (((float) strenthVo.rate) / 10000f);
                }
                Dictionary<int, int> attriDic = ParseStringUtils.ParseAddAttri(itemTemplate.property, rate);
                this.beforeAttriDescript.text = AttTips.GetNoAddAttriContent(attriDic);
                Dictionary<int, int> dictionary2 = ParseStringUtils.ParseAddAttri(itemTemplate.property, 1f);
                Dictionary<int, int> dictionary3 = new Dictionary<int, int>();
                foreach (KeyValuePair<int, int> pair in dictionary2)
                {
                    int num4 = (int) Mathf.Ceil((1f + (((float) vo3.rate) / 10000f)) * ((float) pair.Value));
                    dictionary3.Add(pair.Key, num4);
                }
                this.afterAttriDescript.text = string.Empty;
                this.afterAttriDescript.text = AttTips.GetNoAddAttriContent(dictionary3);
                this.beforeBoxColider.size = new Vector3((float) this.beforeAttriDescript.width, (float) this.beforeAttriDescript.height, 0f);
                this.afterBoxColider.size = new Vector3((float) this.afterAttriDescript.width, (float) this.afterAttriDescript.height, 0f);
                this.strengthInfo.needMaterialList.Clear();
                if (!this.isNoEquipState)
                {
                    this.strengthInfo.needMaterialList = ParseStringUtils.ParseMaterial(vo3.material_list, 1);
                }
                for (int j = 0; j < this.strengthInfo.needMaterialList.Count; j++)
                {
                    if (j >= this.cellList.Count)
                    {
                        EquipCell cell = this.CloneCell();
                    }
                    this.cellList[j].SetActive(true);
                    this.cellList[j].onClick = new UIWidgetContainer.VoidDelegate(this.MaterialCellClickHandler);
                    this.cellList[j].TemplateId = this.strengthInfo.needMaterialList[j].TemplateId;
                    this.cellList[j].CellLabContent = Singleton<BagMode>.Instance.GetItemCountByTemplateId(this.strengthInfo.needMaterialList[j].TemplateId).ToString() + "/" + this.strengthInfo.needMaterialList[j].Count.ToString();
                }
                for (int k = this.strengthInfo.needMaterialList.Count; k < this.cellList.Count; k++)
                {
                    this.cellList[k].SetActive(false);
                }
                this.materialGrid.Reposition();
            }
        }

        public GameObject GetGameObject
        {
            get
            {
                return this.go;
            }
        }
    }
}

