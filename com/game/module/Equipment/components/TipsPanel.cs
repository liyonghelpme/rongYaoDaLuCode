﻿namespace com.game.module.Equipment.components
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class TipsPanel : BaseView<TipsPanel>
    {
        private Button bgBtn;
        private Button closeBtn;
        private string content;
        private UILabel contentLab;

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.contentLab.text = this.content;
        }

        protected override void Init()
        {
            this.contentLab = base.FindInChild<UILabel>("panel/content");
            this.closeBtn = base.FindInChild<Button>("panel/closeBtn");
            this.bgBtn = base.FindInChild<Button>("panel/bgBtn");
            this.IniteEvent();
        }

        private void IniteEvent()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
        }

        public void Show(string content)
        {
            this.content = content;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Equipment/TipsPanel.assetbundle";
            }
        }
    }
}

