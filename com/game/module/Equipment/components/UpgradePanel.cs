﻿namespace com.game.module.Equipment.components
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using com.game.module.Equipment.utils;
    using com.game.Public.Message;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class UpgradePanel : IEquipmentPanel
    {
        private UILabel afterAttriDescript;
        private BoxCollider afterBoxCollider;
        private EquipCell afterEquipCell;
        private GameObject afterUpgradeGo;
        private UILabel beforeAttriDescript;
        private BoxCollider beforeBoxCollider;
        private EquipCell beforeEquipCell;
        private GameObject cellGo;
        private List<EquipCell> cellList = new List<EquipCell>();
        private UILabel chargeLab;
        private EquipCell curMaterialCell;
        private PItems equipItemInfo;
        private GameObject go;
        private bool isNoEquipState;
        private bool isUpgradeHightLv;
        private UISprite jiantou;
        private UISprite jinbiIcon;
        private IEquipMainPanel mainPanel;
        private UIGrid materialGrid;
        private UILabel materialLabel;
        private List<MaterialInfo> materialList = new List<MaterialInfo>();
        private Button upgradeBtn;
        private UpgradeInfo upgradeInfo;
        private UILabel upgradeNeedLvLabel;

        private EquipCell CloneCell()
        {
            if (null == this.cellGo)
            {
                this.cellGo = NGUITools.FindChild(this.go, "materialList/grid/item");
            }
            GameObject go = NGUITools.AddChild(this.materialGrid.gameObject, this.cellGo);
            go.transform.position = new Vector3(0f, 0f, 0f);
            EquipCell item = go.AddMissingComponent<EquipCell>();
            this.cellList.Add(item);
            return item;
        }

        public void Dispose()
        {
        }

        private void HandlerHightLv()
        {
            this.SetActives(false);
            this.beforeEquipCell.CellLabContent = this.upgradeInfo.curEquipTemplate.name;
            this.beforeEquipCell.TemplateId = (uint) this.upgradeInfo.curEquipTemplate.id;
            this.upgradeInfo.additionAttriDic.Clear();
            this.beforeAttriDescript.text = string.Empty;
            for (int i = 0; i < this.upgradeInfo.hadPutInMaterialList.Count; i++)
            {
                Dictionary<int, int> materialAttDic = ParseStringUtils.ParseAddAttri(this.upgradeInfo.hadPutInMaterialList[i].Template.property, 1f);
                this.upgradeInfo.GetAdditionAttriDic(materialAttDic);
            }
            SysStrengthVo strenthVo = BaseDataMgr.instance.GetStrenthVo(this.equipItemInfo.stren);
            float rate = 1f;
            if (strenthVo != null)
            {
                rate = 1f + (((float) strenthVo.rate) / 10000f);
            }
            this.beforeAttriDescript.text = AttTips.GetAdditionAttriContent(ParseStringUtils.ParseAddAttri(this.upgradeInfo.curEquipTemplate.property, rate), this.upgradeInfo.additionAttriDic);
            this.afterAttriDescript.text = string.Empty;
        }

        private void InitEvent()
        {
            this.upgradeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.UpgradeClickHandler);
            this.upgradeInfo.Add(1, new NoticeListener(this.MaterialPutInHandler));
            this.upgradeInfo.Add(9, new NoticeListener(this.MaterialMakeSucceedHandler));
        }

        public void InitView(GameObject gameObject, IEquipMainPanel mainPanel)
        {
            this.upgradeInfo = Singleton<EquipmentMode>.Instance.upgradeInfo;
            this.go = gameObject;
            this.mainPanel = mainPanel;
            this.beforeEquipCell = NGUITools.FindChild(this.go, "beforeUpgrade/equipCell").AddMissingComponent<EquipCell>();
            this.afterEquipCell = NGUITools.FindChild(this.go, "afterUpgrade/equipCell").AddMissingComponent<EquipCell>();
            this.beforeAttriDescript = NGUITools.FindInChild<UILabel>(this.go, "beforeUpgrade/descriptCell/Lab/attriLab");
            this.afterAttriDescript = NGUITools.FindInChild<UILabel>(this.go, "afterUpgrade/descriptCell/Lab/attriLab");
            this.chargeLab = NGUITools.FindInChild<UILabel>(this.go, "chargeLabel");
            this.upgradeNeedLvLabel = NGUITools.FindInChild<UILabel>(this.go, "strengthedNeedLvLabel");
            this.upgradeBtn = NGUITools.FindInChild<Button>(this.go, "upgradeBtn");
            this.materialGrid = NGUITools.FindInChild<UIGrid>(this.go, "materialList/grid");
            this.jinbiIcon = NGUITools.FindInChild<UISprite>(this.go, "jinbiIcon");
            this.afterUpgradeGo = NGUITools.FindChild(this.go, "afterUpgrade");
            this.jiantou = NGUITools.FindInChild<UISprite>(this.go, "jianTou");
            this.materialLabel = NGUITools.FindInChild<UILabel>(this.go, "materialLabel");
            this.beforeBoxCollider = NGUITools.FindChild(this.go, "beforeUpgrade/descriptCell/Lab").GetComponent<BoxCollider>();
            this.afterBoxCollider = NGUITools.FindChild(this.go, "afterUpgrade/descriptCell/Lab").GetComponent<BoxCollider>();
            this.InitEvent();
        }

        private void MaterialCellClickHandler(GameObject go)
        {
            if (!this.isNoEquipState)
            {
                EquipCell component = go.GetComponent<EquipCell>();
                int index = this.cellList.IndexOf(component);
                this.curMaterialCell = component;
                this.upgradeInfo.curCellMaterialTemplate = component.itemVo;
                switch (this.curMaterialCell.CellState)
                {
                    case CellStates.NoMaterial:
                        Singleton<GetItemTipsPanel>.Instance.SetInfo(component.itemVo, PanelType.NONE);
                        break;

                    case CellStates.CanMix:
                        Singleton<MaterialCommonPanel>.Instance.SetInfo(component.itemVo, MaterialPanel.MixPanel, false);
                        break;

                    case CellStates.CanPutIn:
                        Singleton<MaterialCommonPanel>.Instance.SetInfo(component.itemVo, MaterialPanel.PutInPanel, false);
                        break;

                    case CellStates.HadPutIn:
                        Singleton<MaterialCommonPanel>.Instance.SetInfo(component.itemVo, MaterialPanel.MixPanel, true);
                        break;
                }
            }
        }

        private void MaterialMakeSucceedHandler(int type, int v1, int v2, object data)
        {
            this.UpdateView(this.equipItemInfo, null);
        }

        private void MaterialPutInHandler(int type, int v1, int v2, object data)
        {
            this.curMaterialCell.icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ItemIconAtlas");
            this.curMaterialCell.TemplateId = this.upgradeInfo.hadPutInMaterialList[this.upgradeInfo.hadPutInMaterialList.Count - 1].TemplateId;
            this.curMaterialCell.CellState = CellStates.HadPutIn;
            this.curMaterialCell.UpdateCell();
            this.beforeAttriDescript.text = string.Empty;
            this.upgradeInfo.additionAttriDic.Clear();
            Dictionary<int, int> dictionary = ParseStringUtils.ParseAddAttri(this.upgradeInfo.curEquipTemplate.property, 1f);
            Dictionary<int, int> dictionary2 = new Dictionary<int, int>();
            for (int i = 0; i < this.upgradeInfo.hadPutInMaterialList.Count; i++)
            {
                Dictionary<int, int> materialAttDic = ParseStringUtils.ParseAddAttri(this.upgradeInfo.hadPutInMaterialList[i].Template.property, 1f);
                this.upgradeInfo.GetAdditionAttriDic(materialAttDic);
            }
            SysStrengthVo strenthVo = BaseDataMgr.instance.GetStrenthVo(this.equipItemInfo.stren);
            float rate = 1f;
            if (strenthVo != null)
            {
                rate = 1f + (((float) strenthVo.rate) / 10000f);
            }
            this.beforeAttriDescript.text = AttTips.GetAdditionAttriContent(ParseStringUtils.ParseAddAttri(this.upgradeInfo.curEquipTemplate.property, rate), this.upgradeInfo.additionAttriDic);
        }

        private void SetActives(bool value)
        {
            this.jiantou.SetActive(value);
            this.upgradeNeedLvLabel.SetActive(value);
            this.afterUpgradeGo.SetActive(value);
            this.upgradeBtn.SetActive(value);
            this.chargeLab.SetActive(value);
            this.jinbiIcon.SetActive(value);
            this.materialLabel.SetActive(value);
            for (int i = 0; i < this.cellList.Count; i++)
            {
                this.cellList[i].SetActive(value);
            }
        }

        public void UpdateView(PItems itemInfo = null, SysItemsVo equipItemTemplate = null)
        {
            SysItemsVo vo;
            SysItemsVo vo3;
            if (itemInfo == null)
            {
                this.isNoEquipState = true;
                itemInfo = new PItems();
                vo = equipItemTemplate;
                itemInfo.templateId = (uint) equipItemTemplate.id;
                itemInfo.generalId = Singleton<GeneralMode>.Instance.templateInfoList[0].generalInfo.id;
            }
            else
            {
                this.isNoEquipState = false;
                this.equipItemInfo = itemInfo;
                this.upgradeInfo.equipItemInfo = this.equipItemInfo;
                vo = BaseDataMgr.instance.getGoodsVo(itemInfo.templateId);
            }
            SysEquipUpVo equipUpgradeVo = BaseDataMgr.instance.GetEquipUpgradeVo((uint) vo.id);
            if (equipUpgradeVo != null)
            {
                vo3 = BaseDataMgr.instance.getGoodsVo(equipUpgradeVo.new_tid);
            }
            else
            {
                vo3 = null;
            }
            this.equipItemInfo = itemInfo;
            this.upgradeInfo.equipItemInfo = this.equipItemInfo;
            this.upgradeInfo.curEquipTemplate = vo;
            this.upgradeInfo.afterUpEquipTemplate = vo3;
            this.beforeEquipCell.CellLabContent = vo.name;
            this.beforeEquipCell.TemplateId = (uint) vo.id;
            if (equipUpgradeVo == null)
            {
                this.isUpgradeHightLv = true;
                this.HandlerHightLv();
            }
            else
            {
                this.SetActives(true);
                this.isUpgradeHightLv = false;
                this.afterEquipCell.CellLabContent = vo3.name;
                this.afterEquipCell.TemplateId = (uint) vo3.id;
                if (this.isNoEquipState)
                {
                    this.beforeEquipCell.icon.ShowAsGray();
                    this.afterEquipCell.icon.ShowAsGray();
                }
                else
                {
                    this.beforeEquipCell.icon.ShowAsMyself();
                    this.afterEquipCell.icon.ShowAsMyself();
                }
                this.chargeLab.text = string.Empty;
                this.chargeLab.text = this.chargeLab.text + "费用：" + equipUpgradeVo.charge.ToString();
                if (Singleton<GeneralMode>.Instance.GetGeneralTemplateInfoById(this.equipItemInfo.generalId).generalInfo.lvl < vo3.lvl)
                {
                    this.upgradeNeedLvLabel.color = Color.red;
                }
                else
                {
                    this.upgradeNeedLvLabel.color = Color.white;
                }
                this.upgradeNeedLvLabel.text = LanguageManager.GetWord("Equipment.upgradeNeedLv") + vo3.lvl.ToString();
                this.materialList.Clear();
                Singleton<EquipmentMode>.Instance.upgradeInfo.hadPutInMaterialList.Clear();
                this.materialList = ParseStringUtils.ParseMaterial(equipUpgradeVo.material_list, 0);
                for (int i = 0; i < this.materialList.Count; i++)
                {
                    if (i >= this.cellList.Count)
                    {
                        EquipCell cell = this.CloneCell();
                    }
                    this.cellList[i].SetActive(true);
                    this.cellList[i].onClick = new UIWidgetContainer.VoidDelegate(this.MaterialCellClickHandler);
                    this.cellList[i].TemplateId = this.materialList[i].TemplateId;
                    this.cellList[i].icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ItemIconAtlas");
                    switch (this.materialList[i].materialState)
                    {
                        case CellStates.NoMaterial:
                            this.cellList[i].CellState = CellStates.NoMaterial;
                            this.cellList[i].icon.spriteName = "199995";
                            this.cellList[i].CellLabContent = LanguageManager.GetWord("Equipment.noMaterial");
                            break;

                        case CellStates.CanMix:
                            this.cellList[i].CellState = CellStates.CanMix;
                            this.cellList[i].icon.spriteName = "199995";
                            this.cellList[i].CellLabContent = LanguageManager.GetWord("Equipment.canMix");
                            break;

                        case CellStates.CanPutIn:
                            this.cellList[i].CellState = CellStates.CanPutIn;
                            this.cellList[i].icon.spriteName = "199995";
                            this.cellList[i].CellLabContent = LanguageManager.GetWord("Equipment.canPutIn");
                            break;

                        case CellStates.HadPutIn:
                            this.cellList[i].icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ItemIconAtlas");
                            this.cellList[i].CellState = CellStates.HadPutIn;
                            this.cellList[i].TemplateId = this.materialList[i].TemplateId;
                            this.cellList[i].CellLabContent = string.Empty;
                            this.cellList[i].UpdateCell();
                            break;
                    }
                    if (this.isNoEquipState)
                    {
                        this.cellList[i].CellLabContent = string.Empty;
                    }
                }
                for (int j = this.materialList.Count; j < this.cellList.Count; j++)
                {
                    this.cellList[j].SetActive(false);
                }
                this.materialGrid.Reposition();
                this.upgradeInfo.additionAttriDic.Clear();
                this.beforeAttriDescript.text = string.Empty;
                for (int k = 0; k < this.upgradeInfo.hadPutInMaterialList.Count; k++)
                {
                    Dictionary<int, int> materialAttDic = ParseStringUtils.ParseAddAttri(this.upgradeInfo.hadPutInMaterialList[k].Template.property, 1f);
                    this.upgradeInfo.GetAdditionAttriDic(materialAttDic);
                }
                SysStrengthVo strenthVo = BaseDataMgr.instance.GetStrenthVo(this.equipItemInfo.stren);
                float rate = 1f;
                if (strenthVo != null)
                {
                    rate = 1f + (((float) strenthVo.rate) / 10000f);
                }
                this.beforeAttriDescript.text = AttTips.GetAdditionAttriContent(ParseStringUtils.ParseAddAttri(this.upgradeInfo.curEquipTemplate.property, rate), this.upgradeInfo.additionAttriDic);
                this.afterAttriDescript.text = string.Empty;
                Dictionary<int, int> attriDic = ParseStringUtils.ParseAddAttri(vo3.property, rate);
                this.afterAttriDescript.text = AttTips.GetNoAddAttriContent(attriDic);
                this.beforeBoxCollider.size = new Vector3((float) this.beforeAttriDescript.width, (float) this.beforeAttriDescript.height, 0f);
                this.afterBoxCollider.size = new Vector3((float) this.afterAttriDescript.width, (float) this.afterAttriDescript.height, 0f);
            }
        }

        private void UpgradeClickHandler(GameObject go)
        {
            if (this.isNoEquipState)
            {
                MessageManager.Show(LanguageManager.GetWord("Equipment.noEquip"));
            }
            else if (Singleton<GeneralMode>.Instance.GetGeneralTemplateInfoById(this.equipItemInfo.generalId).generalInfo.lvl < this.upgradeInfo.afterUpEquipTemplate.lvl)
            {
                MessageManager.Show("当前武将等级不够");
            }
            else if (this.materialList.Count > this.upgradeInfo.hadPutInMaterialList.Count)
            {
                MessageManager.Show("材料不够");
            }
            else
            {
                Singleton<EquipmentMode>.Instance.SendEquipUpgrade(this.equipItemInfo.id);
            }
        }

        public GameObject GetGameObject
        {
            get
            {
                return this.go;
            }
        }
    }
}

