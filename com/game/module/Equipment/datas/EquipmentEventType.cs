﻿namespace com.game.module.Equipment.datas
{
    using System;

    public class EquipmentEventType
    {
        public const int EQUIP_DISMOUNT = 6;
        public const int EQUIP_INSERT = 5;
        public const int EQUIP_STRENGTH = 4;
        public const int EQUIP_UPGRADE = 3;
        public const int MATERIAL_COMPOSE = 2;
        public const int MATERIAL_MAKE = 9;
        public const int MATERIAL_PUTIN = 1;
        public const int STONE_BATCH_MIX = 8;
        public const int STONE_MIX = 7;
        public const int WEAR_EQUIP = 0;
    }
}

