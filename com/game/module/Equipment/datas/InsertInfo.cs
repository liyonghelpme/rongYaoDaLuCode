﻿namespace com.game.module.Equipment.datas
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Role;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class InsertInfo : Notifier
    {
        public List<PItems> attackStoneList = new List<PItems>();
        public PItems curClickStoneItemInfo;
        public List<PItems> defendStoneList = new List<PItems>();
        public PItems equipItemInfo;
        public SysItemsVo equipTemplate;
        public List<ulong> insertStoneIdList = new List<ulong>();
        public List<uint> insertStoneTemplateIdList = new List<uint>();
        public List<SysItemsVo> insertStoneTemplateList = new List<SysItemsVo>();
        public SysItemsVo lastDismountStoneTemplate;
        public uint replaceInsertedStoneTid;
        public Dictionary<ulong, PItems> stoneDict = new Dictionary<ulong, PItems>();

        public InsertInfo()
        {
            this.DistinguishStone();
            Singleton<BagMode>.Instance.dataUpdated += new DataUpateHandler(this.StoneUpdate);
        }

        public bool CanInsertStone(SysItemsVo template)
        {
            for (int i = 0; i < this.equipItemInfo.hole.Count; i++)
            {
                SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(this.equipItemInfo.hole[i].tid);
                if (vo.value3 == template.value3)
                {
                    this.replaceInsertedStoneTid = (uint) vo.id;
                    return false;
                }
            }
            return true;
        }

        public void DismountStoneSucceed()
        {
            this.equipItemInfo = Singleton<BagMode>.Instance.EquipDict[this.equipItemInfo.id];
            this.equipTemplate = BaseDataMgr.instance.getGoodsVo(this.equipItemInfo.templateId);
            this.insertStoneTemplateList.Remove(this.lastDismountStoneTemplate);
            this.Notify(6, 0, 0, null);
        }

        public void DistinguishStone()
        {
            this.attackStoneList.Clear();
            this.defendStoneList.Clear();
            this.stoneDict = Singleton<BagMode>.Instance.GetStoneDict();
            foreach (PItems items in this.stoneDict.Values)
            {
                SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(items.templateId);
                if (vo.value4 == 1)
                {
                    this.attackStoneList.Add(items);
                }
                else if (vo.value4 == 2)
                {
                    this.defendStoneList.Add(items);
                }
            }
        }

        public uint GetSameCategoryTid(SysItemsVo template)
        {
            for (int i = 0; i < this.insertStoneTemplateList.Count; i++)
            {
                if (this.insertStoneTemplateList[i].value3 == template.value3)
                {
                    return (uint) this.insertStoneTemplateList[i].id;
                }
            }
            return 0;
        }

        public void InsertStoneSucceed()
        {
            this.equipItemInfo = Singleton<BagMode>.Instance.EquipDict[this.equipItemInfo.id];
            this.equipTemplate = BaseDataMgr.instance.getGoodsVo(this.equipItemInfo.templateId);
            this.Notify(5, 0, 0, null);
        }

        private void StoneUpdate(object sender, int code)
        {
            if (code == BagMode.UPDATE_ITEM)
            {
                this.DistinguishStone();
            }
        }
    }
}

