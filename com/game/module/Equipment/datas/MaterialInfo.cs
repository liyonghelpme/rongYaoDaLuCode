﻿namespace com.game.module.Equipment.datas
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.components;
    using PCustomDataType;
    using System;

    public class MaterialInfo
    {
        private int count;
        public CellStates materialState;
        private uint templateId;

        private void SetStrengthTemplateId(uint templateId)
        {
            this.templateId = templateId;
        }

        public void SetTemplateId(uint templateId, int type)
        {
            if (type == 0)
            {
                this.SetUpgradeTemplateId(templateId);
            }
            else
            {
                this.SetStrengthTemplateId(templateId);
            }
        }

        private void SetUpgradeTemplateId(uint templateId)
        {
            this.templateId = templateId;
            int itemCountByTemplateId = Singleton<BagMode>.Instance.GetItemCountByTemplateId(templateId);
            PItems equipItemInfo = Singleton<EquipmentMode>.Instance.upgradeInfo.equipItemInfo;
            bool flag = false;
            for (int i = 0; i < equipItemInfo.colorRes.Count; i++)
            {
                if (equipItemInfo.colorRes[i].tid == templateId)
                {
                    Singleton<EquipmentMode>.Instance.upgradeInfo.hadPutInMaterialList.Add(this);
                    flag = true;
                }
            }
            if (flag)
            {
                this.materialState = CellStates.HadPutIn;
            }
            else if (itemCountByTemplateId > 0)
            {
                this.materialState = CellStates.CanPutIn;
            }
            else
            {
                SysMaterialComposeVo materialCompiseVo = BaseDataMgr.instance.GetMaterialCompiseVo(templateId);
                if (materialCompiseVo != null)
                {
                    if (Singleton<BagMode>.Instance.GetItemCountByTemplateId(materialCompiseVo.frag_tid) >= materialCompiseVo.frag_num)
                    {
                        this.materialState = CellStates.CanMix;
                    }
                    else
                    {
                        this.materialState = CellStates.NoMaterial;
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                return this.count;
            }
            set
            {
                this.count = value;
            }
        }

        public SysItemsVo Template
        {
            get
            {
                return BaseDataMgr.instance.getGoodsVo(this.TemplateId);
            }
        }

        public uint TemplateId
        {
            get
            {
                return this.templateId;
            }
        }
    }
}

