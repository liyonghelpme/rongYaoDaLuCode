﻿namespace com.game.module.Equipment.datas
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class MixInfo : Notifier
    {
        public static List<uint> attackHighLvStoneTemplateIdList = new List<uint> { 0x61b51, 0x61bb5, 0x61ce1, 0x61d45, 0x61da9, 0x61e0d, 0x61e71, 0x61ed5, 0x61f39, 0x61f9d, 0x62001 };
        public int curSelectedStoneCountInBag;
        public int curSelectedStoneType;
        public SysItemsVo curSelectedTemplate;
        public static List<uint> defendHighLvStoneTemplateIdList = new List<uint> { 0x61aed, 0x61c19, 0x61c7d, 0x62065, 0x620c9, 0x6212d, 0x62191, 0x621f5, 0x62259 };

        public uint GetNextLvStoneTemplateId(uint templateId)
        {
            return BaseDataMgr.instance.GetGemComposeVo(templateId).des_tid;
        }

        public List<StoneInfo> GetStoneListByType(int type)
        {
            List<StoneInfo> list = new List<StoneInfo>();
            List<SysItemsVo> stoneTemplateList = BaseDataMgr.instance.GetStoneTemplateList();
            for (int i = 0; i < stoneTemplateList.Count; i++)
            {
                if (stoneTemplateList[i].value3 == type)
                {
                    StoneInfo item = new StoneInfo {
                        stoneTemplate = stoneTemplateList[i],
                        count = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) stoneTemplateList[i].id)
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public List<PItems> GetStoneListByTypeAndLv(int lv, int type)
        {
            List<PItems> list = new List<PItems>();
            foreach (PItems items in Singleton<BagMode>.Instance.GetStoneDict().Values)
            {
                SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(items.templateId);
                if ((vo.value1 == lv) && (vo.value3 == type))
                {
                    list.Add(items);
                }
            }
            return list;
        }

        public void StoneBatchMixSucceed()
        {
            this.Notify(8, 0, 0, null);
        }

        public void StoneMixSucceed()
        {
            this.Notify(7, 0, 0, null);
        }
    }
}

