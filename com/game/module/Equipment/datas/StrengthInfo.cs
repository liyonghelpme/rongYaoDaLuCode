﻿namespace com.game.module.Equipment.datas
{
    using com.game.basic.events;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class StrengthInfo : Notifier
    {
        public PItems equipItemInfo;
        public List<MaterialInfo> needMaterialList = new List<MaterialInfo>();
        public PItems strengthedEquipItemInfo;

        public bool CanStrength()
        {
            for (int i = 0; i < this.needMaterialList.Count; i++)
            {
                if (Singleton<BagMode>.Instance.GetItemCountByTemplateId(this.needMaterialList[i].TemplateId) < this.needMaterialList[i].Count)
                {
                    return false;
                }
            }
            return true;
        }

        public void StrengthEquipSucceed(ulong newId)
        {
            this.strengthedEquipItemInfo = Singleton<BagMode>.Instance.EquipDict[newId];
            this.Notify(4, 0, 0, null);
        }
    }
}

