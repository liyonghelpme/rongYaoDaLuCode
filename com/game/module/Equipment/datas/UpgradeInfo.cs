﻿namespace com.game.module.Equipment.datas
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Equipment.components;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class UpgradeInfo : Notifier
    {
        public Dictionary<int, int> additionAttriDic = new Dictionary<int, int>();
        public SysItemsVo afterUpEquipTemplate;
        public SysItemsVo curCellMaterialTemplate;
        public SysItemsVo curEquipTemplate;
        public int curPutInMaterialCount;
        public PItems equipItemInfo;
        public SysItemsVo fragTemplate;
        public List<MaterialInfo> hadPutInMaterialList = new List<MaterialInfo>();
        public SysMaterialComposeVo materialComposeTemplate;
        public List<MaterialInfo> needMaterialList = new List<MaterialInfo>();
        public PItems upgradeSucEquipItemInfo;

        public void EquipUpgradeSucceed(ulong newEquipId)
        {
            this.upgradeSucEquipItemInfo = Singleton<BagMode>.Instance.EquipDict[newEquipId];
            this.equipItemInfo = Singleton<BagMode>.Instance.EquipDict[newEquipId];
            this.Notify(3, 0, 0, null);
        }

        public void GetAdditionAttriDic(Dictionary<int, int> materialAttDic)
        {
            foreach (KeyValuePair<int, int> pair in materialAttDic)
            {
                if (this.additionAttriDic.ContainsKey(pair.Key))
                {
                    Dictionary<int, int> dictionary;
                    int num;
                    num = dictionary[num];
                    (dictionary = this.additionAttriDic)[num = pair.Key] = num + pair.Value;
                }
                else
                {
                    this.additionAttriDic.Add(pair.Key, pair.Value);
                }
            }
        }

        public void MaterialMakeSucceed()
        {
            this.Notify(9, 0, 0, null);
        }

        public void PutInMaterial(uint materialTemplateId)
        {
            MaterialInfo item = new MaterialInfo();
            item.SetTemplateId(materialTemplateId, 0);
            item.Count = 1;
            item.materialState = CellStates.Normal;
            this.hadPutInMaterialList.Add(item);
        }

        public void PutInMaterialSucceed(List<PMaterial> putInMaterialList)
        {
            for (int i = 0; i < putInMaterialList.Count; i++)
            {
                this.PutInMaterial((uint) this.curCellMaterialTemplate.id);
            }
            this.curPutInMaterialCount = putInMaterialList.Count;
            this.equipItemInfo = Singleton<BagMode>.Instance.EquipDict[this.equipItemInfo.id];
            this.Notify(1, 0, 0, null);
        }
    }
}

