﻿namespace com.game.module.Equipment.datas
{
    using com.game.basic.events;
    using com.game.data;
    using PCustomDataType;
    using System;

    public class WearEquipInfo : Notifier
    {
        public PItems curSelectedEquipItemInfo;
        public SysItemsVo curSelectedEquipTemplateInfo;

        public void WearEquipSucceed()
        {
            this.Notify(0, 0, 0, null);
        }
    }
}

