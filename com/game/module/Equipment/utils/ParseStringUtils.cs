﻿namespace com.game.module.Equipment.utils
{
    using com.game.module.Equipment.datas;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ParseStringUtils
    {
        public static Dictionary<int, int> ParseAddAttri(string str, float rate = 1f)
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            str = str.Replace("[[", string.Empty);
            str = str.Replace("]]", string.Empty);
            str = str.Replace("],[", "|");
            char[] separator = new char[] { '|' };
            string[] strArray = str.Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] chArray2 = new char[] { ',' };
                string[] strArray2 = strArray[i].Split(chArray2);
                int num2 = int.Parse(strArray2[0]);
                int num4 = Mathf.CeilToInt(int.Parse(strArray2[1]) * rate);
                dictionary[num2] = num4;
            }
            return dictionary;
        }

        public static List<MaterialInfo> ParseMaterial(string str, int type)
        {
            List<MaterialInfo> list = new List<MaterialInfo>();
            str = str.Replace("[[", string.Empty);
            str = str.Replace("]]", string.Empty);
            str = str.Replace("],[", "|");
            char[] separator = new char[] { '|' };
            string[] strArray = str.Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] chArray2 = new char[] { ',' };
                string[] strArray2 = strArray[i].Split(chArray2);
                MaterialInfo item = new MaterialInfo();
                item.SetTemplateId(uint.Parse(strArray2[0]), type);
                item.Count = int.Parse(strArray2[1]);
                list.Add(item);
            }
            return list;
        }
    }
}

