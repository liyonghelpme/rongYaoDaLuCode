﻿namespace com.game.module.Exp_Drup
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class ExpDrugController : BaseControl<ExpDrugController>
    {
        private void AddExpUpdate(INetData data)
        {
            GeneralAddExpMsg_2_8 g__ = new GeneralAddExpMsg_2_8();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<ExpDrugMode>.Instance.AddExpUpdate();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("520", new NetMsgCallback(this.AddExpUpdate));
        }
    }
}

