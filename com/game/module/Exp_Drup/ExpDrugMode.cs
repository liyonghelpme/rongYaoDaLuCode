﻿namespace com.game.module.Exp_Drup
{
    using com.game;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.General;
    using Proto;
    using System;
    using System.IO;

    public class ExpDrugMode : BaseMode<ExpDrugMode>
    {
        public SysRoleInfoVo curExpTemplate;
        public int curGeneralLimitLvl;
        public GeneralTemplateInfo curSelectedGeneralInfo;

        public void AddExpUpdate()
        {
        }

        public void SendAddExp(ulong generalId, uint itemId, ushort count)
        {
            MemoryStream msdata = new MemoryStream();
            Module_2.write_2_8(msdata, generalId, itemId, count);
            AppNet.gameNet.send(msdata, 2, 8);
        }
    }
}

