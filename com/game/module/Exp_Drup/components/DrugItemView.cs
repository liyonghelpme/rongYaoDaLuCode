﻿namespace com.game.module.Exp_Drup.components
{
    using com.game.basic;
    using com.game.data;
    using com.game.Interface.Tick;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.module.Equipment.components;
    using com.game.module.Role;
    using com.game.Public.Message;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class DrugItemView : ITick
    {
        private SysItemVoCell cell;
        private int count;
        private int curExpDrugCount;
        private int curUseExpDrugCount;
        private UILabel desLabel;
        private GameObject go;
        private SysItemsVo itemTemplate;
        private UILabel nameLab;
        private Button oneKeyBtn;
        private bool send;
        private Button useBtn;

        public DrugItemView(GameObject go)
        {
            this.go = go;
            this.InitView();
        }

        private void InitEvent()
        {
            this.useBtn.onClick = new UIWidgetContainer.VoidDelegate(this.UseBtnClickHandler);
            this.useBtn.onPress = new UIWidgetContainer.BoolDelegate(this.OnpressHandler);
            this.oneKeyBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OneKeyBtnClickHandler);
            Singleton<BagMode>.Instance.dataUpdated += new DataUpateHandler(this.ItemCountUpdateHandler);
        }

        private void InitView()
        {
            this.useBtn = NGUITools.FindInChild<Button>(this.go, "useBtn");
            this.oneKeyBtn = NGUITools.FindInChild<Button>(this.go, "oneKeyBtn");
            this.nameLab = NGUITools.FindInChild<UILabel>(this.go, "nameLabel");
            this.desLabel = NGUITools.FindInChild<UILabel>(this.go, "desLabel");
            this.cell = NGUITools.FindChild(this.go, "cell").AddMissingComponent<SysItemVoCell>();
            GlobalAPI.tickManager.addTick(this);
            this.InitEvent();
        }

        private void ItemCountUpdateHandler(object sender, int code)
        {
            if (code == BagMode.UPDATE_ITEM)
            {
                this.curExpDrugCount = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) this.itemTemplate.id);
                this.cell.count = this.curExpDrugCount;
            }
        }

        private void OneKeyBtnClickHandler(GameObject go)
        {
            if (this.curExpDrugCount <= 0)
            {
                MessageManager.Show(LanguageManager.GetWord("ExpDrug.expDrugBagNotEnougth"));
            }
            else
            {
                ulong num = BaseDataMgr.instance.GetGeneralExpVo((uint) Singleton<ExpDrugMode>.Instance.curGeneralLimitLvl).total_exp - Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo.generalInfo.exp;
                if (num == 0)
                {
                    this.curUseExpDrugCount = 0;
                }
                else
                {
                    this.curUseExpDrugCount = Mathf.FloorToInt((float) (num / ((long) this.itemTemplate.value1))) + 1;
                }
                if (this.curUseExpDrugCount <= 0)
                {
                    MessageManager.Show(LanguageManager.GetWord("ExpDrug.fullExp"));
                }
                else
                {
                    this.curExpDrugCount = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) this.itemTemplate.id);
                    if (this.curUseExpDrugCount >= this.curExpDrugCount)
                    {
                        this.curUseExpDrugCount = this.curExpDrugCount;
                    }
                    ulong num2 = (ulong) (this.curUseExpDrugCount * this.itemTemplate.value1);
                    if (num2 > num)
                    {
                        num2 = num;
                    }
                    object[] objArray1 = new object[] { "确定使用", this.curUseExpDrugCount, "个", this.itemTemplate.name, "为", Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo.sysGeneralVo.name, "增加", num2.ToString(), "经验?" };
                    string content = string.Concat(objArray1);
                    Singleton<AlertPanel>.Instance.Show(content, new com.game.module.Equipment.components.SureCallback(this.SureCallback), null);
                }
            }
        }

        private void OnpressHandler(GameObject go, bool value)
        {
            if (value)
            {
                this.send = true;
            }
            else
            {
                this.send = false;
            }
        }

        private void SendMsg()
        {
            ulong id = Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo.generalInfo.id;
            uint itemId = (uint) this.itemTemplate.id;
            Singleton<ExpDrugMode>.Instance.SendAddExp(id, itemId, 1);
        }

        public void SetInfo(SysItemsVo template)
        {
            this.itemTemplate = template;
            this.nameLab.text = this.itemTemplate.name;
            this.desLabel.text = this.itemTemplate.desc;
            this.cell.sysVoId = this.itemTemplate.id;
            this.curExpDrugCount = Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) this.itemTemplate.id);
            this.cell.count = this.curExpDrugCount;
        }

        private void SureCallback()
        {
            ulong id = Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo.generalInfo.id;
            uint itemId = (uint) this.itemTemplate.id;
            Singleton<ExpDrugMode>.Instance.SendAddExp(id, itemId, (ushort) this.curUseExpDrugCount);
        }

        public void Update(int times, float dt = 0.04f)
        {
            if (this.send)
            {
                this.count++;
                if (this.count == 12)
                {
                    this.SendMsg();
                    this.count = 0;
                }
            }
            else
            {
                this.count = 0;
            }
        }

        private void UseBtnClickHandler(GameObject go)
        {
            if (this.curExpDrugCount <= 0)
            {
                MessageManager.Show(LanguageManager.GetWord("ExpDrug.expDrugBagNotEnougth"));
            }
            else
            {
                SysGeneralExpVo generalExpVo = BaseDataMgr.instance.GetGeneralExpVo((uint) Singleton<ExpDrugMode>.Instance.curGeneralLimitLvl);
                if (Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo.generalInfo.exp >= generalExpVo.total_exp)
                {
                    MessageManager.Show(LanguageManager.GetWord("ExpDrug.fullExp"));
                }
                else
                {
                    this.SendMsg();
                }
            }
        }
    }
}

