﻿namespace com.game.module.Exp_Drup.components
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ExpDrugPanel : BaseView<ExpDrugPanel>
    {
        private Button closeBtn;
        private GeneralInfo congfigureInfo;
        private GeneralItemView curSelectedGeneralItem;
        private List<DrugItemView> expDrugItemViewList = new List<DrugItemView>();
        private List<GeneralItemView> generalItemViewList = new List<GeneralItemView>();
        private GameObject leftExpDrugGo;
        private UIGrid leftGrid;
        private GameObject rightGeneralItemGo;
        private UIGrid rightGrid;
        private UIPanel scrollPanel;
        private UIScrollView scrollView;
        private SpringPanel springPanel;
        private UILabel titleLab;

        private void CloneLeftItem()
        {
            if (null == this.leftExpDrugGo)
            {
                this.leftExpDrugGo = base.FindChild("Panel/left/grid/item");
            }
            GameObject go = NGUITools.AddChild(this.leftGrid.gameObject, this.leftExpDrugGo);
            go.SetActive(true);
            DrugItemView item = new DrugItemView(go);
            this.expDrugItemViewList.Add(item);
        }

        private void CloneRightItem()
        {
            if (null == this.rightGeneralItemGo)
            {
                this.rightGeneralItemGo = base.FindChild("Panel/rightPanel/right/grid/item");
            }
            GameObject go = NGUITools.AddChild(this.rightGrid.gameObject, this.rightGeneralItemGo);
            go.SetActive(true);
            GeneralItemView item = go.AddMissingComponent<GeneralItemView>();
            item.InitView();
            item.onClick = new UIWidgetContainer.VoidDelegate(this.GeneralItemViewClickHandler);
            this.generalItemViewList.Add(item);
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        public override void CloseView()
        {
            base.CloseView();
            if (this.curSelectedGeneralItem != null)
            {
                this.curSelectedGeneralItem.Select(false);
            }
        }

        private void ExpUpdate(object sender, int code, object param)
        {
            if (code == 11)
            {
                GeneralTemplateInfo generalTemplateInfoById = this.GetGeneralTemplateInfoById(Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo.generalInfo.id);
                this.generalItemViewList[this.GetGeneralItemViewIndex(Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo.generalInfo.id)].SetInfo(generalTemplateInfoById);
            }
        }

        private void GeneralItemViewClickHandler(GameObject go)
        {
            if (this.curSelectedGeneralItem != null)
            {
                this.curSelectedGeneralItem.Select(false);
            }
            this.curSelectedGeneralItem = go.GetComponent<GeneralItemView>();
            this.curSelectedGeneralItem.Select(true);
            Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo = this.curSelectedGeneralItem.Info;
        }

        private int GetGeneralItemViewIndex(ulong generalId)
        {
            for (int i = 0; i < this.generalItemViewList.Count; i++)
            {
                if (this.generalItemViewList[i].Info.generalInfo.id == generalId)
                {
                    return i;
                }
            }
            return 0;
        }

        private GeneralTemplateInfo GetGeneralTemplateInfoById(ulong id)
        {
            List<GeneralTemplateInfo> list = Singleton<GeneralMode>.Instance.HaveGeneralInfoList();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].generalInfo.id == id)
                {
                    return list[i];
                }
            }
            return null;
        }

        protected override void HandleAfterOpenView()
        {
            List<int> configDatas = Singleton<ConfigConst>.Instance.GetConfigDatas("EXP_ITEMS_LIST");
            for (int i = 0; i < configDatas.Count; i++)
            {
                if (i >= this.expDrugItemViewList.Count)
                {
                    this.CloneLeftItem();
                }
                SysItemsVo template = BaseDataMgr.instance.getGoodsVo(configDatas[i]);
                this.expDrugItemViewList[i].SetInfo(template);
            }
            List<GeneralTemplateInfo> list2 = Singleton<GeneralMode>.Instance.HaveGeneralInfoList();
            if (((Mathf.Floor((float) (list2.Count / 2)) + 1f) * this.rightGrid.cellHeight) <= this.scrollView.panel.width)
            {
                this.scrollView.enabled = false;
            }
            else
            {
                this.scrollView.enabled = true;
            }
            for (int j = 0; j < list2.Count; j++)
            {
                if (j >= this.generalItemViewList.Count)
                {
                    this.CloneRightItem();
                }
                this.generalItemViewList[j].SetInfo(list2[j]);
            }
            this.rightGrid.Reposition();
            this.scrollView.ResetPosition();
            int generalItemViewIndex = 0;
            if (this.congfigureInfo != null)
            {
                generalItemViewIndex = this.GetGeneralItemViewIndex(this.congfigureInfo.id);
            }
            else
            {
                generalItemViewIndex = 0;
            }
            int num4 = Mathf.FloorToInt((float) (generalItemViewIndex / 2)) + 1;
            if ((num4 * this.rightGrid.cellHeight) >= this.scrollView.panel.width)
            {
                this.scrollView.MoveRelative(new Vector3(0f, (num4 % 3) * this.rightGrid.cellHeight, 0f));
            }
            this.curSelectedGeneralItem = this.generalItemViewList[generalItemViewIndex];
            this.curSelectedGeneralItem.Select(true);
            Singleton<ExpDrugMode>.Instance.curSelectedGeneralInfo = this.curSelectedGeneralItem.Info;
            int level = MeVo.instance.Level;
            Singleton<ExpDrugMode>.Instance.curExpTemplate = BaseDataMgr.instance.GetRoleInfo(level);
            Singleton<ExpDrugMode>.Instance.curGeneralLimitLvl = Singleton<ExpDrugMode>.Instance.curExpTemplate.general_lvl;
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
        }

        protected override void Init()
        {
            this.closeBtn = base.FindInChild<Button>("Panel/btnClose");
            this.leftGrid = base.FindInChild<UIGrid>("Panel/left/grid");
            this.rightGrid = base.FindInChild<UIGrid>("Panel/rightPanel/right/grid");
            this.titleLab = base.FindInChild<UILabel>("Panel/bg/titleLabel");
            this.titleLab.text = LanguageManager.GetWord("ExpDrug.title");
            this.scrollPanel = base.FindInChild<UIPanel>("Panel/rightPanel");
            this.scrollView = base.FindInChild<UIScrollView>("Panel/rightPanel");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            Singleton<GeneralMode>.Instance.dataUpdatedWithParam += new com.game.module.core.DataUpateHandlerWithParam(this.ExpUpdate);
        }

        public void OpenExpDrugPanel(GeneralInfo generalInfo)
        {
            this.congfigureInfo = generalInfo;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Exp_Drug/ExpDrugPanel.assetbundle";
            }
        }
    }
}

