﻿namespace com.game.module.Exp_Drup.components
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class GeneralItemView : Button
    {
        private UILabel expLab;
        private UISlider expSlider;
        private UISprite generalIcon;
        private GeneralTemplateInfo info;
        private UILabel lvlLab;
        private UILabel nameLabel;
        private UISprite selected;
        private List<GameObject> starGameObjects = new List<GameObject>();

        private void InitEvent()
        {
        }

        public void InitView()
        {
            this.generalIcon = NGUITools.FindInChild<UISprite>(base.gameObject, "header/headerIcon");
            this.generalIcon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this.nameLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "nameLabel");
            this.selected = NGUITools.FindInChild<UISprite>(base.gameObject, "selected");
            this.expSlider = NGUITools.FindInChild<UISlider>(base.gameObject, "expbar");
            this.expLab = NGUITools.FindInChild<UILabel>(base.gameObject, "expbar/num");
            this.lvlLab = NGUITools.FindInChild<UILabel>(base.gameObject, "lvlLab");
            for (int i = 1; i <= 5; i++)
            {
                GameObject item = base.FindChild("star_list/star" + i.ToString());
                this.starGameObjects.Add(item);
            }
            this.InitEvent();
        }

        public void Select(bool value)
        {
            if (value)
            {
                this.selected.SetActive(true);
            }
            else
            {
                this.selected.SetActive(false);
            }
        }

        public void SetInfo(GeneralTemplateInfo info)
        {
            this.info = info;
            this.generalIcon.spriteName = info.sysGeneralVo.icon_id.ToString();
            this.nameLabel.text = info.sysGeneralVo.name;
            SysGeneralExpVo generalExpVo = BaseDataMgr.instance.GetGeneralExpVo((uint) (info.generalInfo.lvl - 1));
            ulong num = 0L;
            if (generalExpVo != null)
            {
                num = (ulong) generalExpVo.total_exp;
            }
            SysGeneralExpVo vo2 = BaseDataMgr.instance.GetGeneralExpVo(info.generalInfo.lvl);
            this.expSlider.value = ((float) (info.generalInfo.exp - num)) / ((float) vo2.exp_level);
            if ((this.expSlider.value == 1f) && (info.generalInfo.lvl == MeVo.instance.Level))
            {
                this.expLab.text = LanguageManager.GetWord("ExpDrug.fullExp");
            }
            else
            {
                this.expLab.text = ((info.generalInfo.exp - num)).ToString() + "/" + vo2.exp_level.ToString();
            }
            int star = info.generalInfo.star;
            for (int i = 0; i < this.starGameObjects.Count; i++)
            {
                this.starGameObjects[i].SetActive(i < star);
            }
            this.lvlLab.text = info.generalInfo.lvl.ToString();
        }

        public GeneralTemplateInfo Info
        {
            get
            {
                return this.info;
            }
        }
    }
}

