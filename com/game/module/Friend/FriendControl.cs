﻿namespace com.game.module.Friend
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using com.u3d.bases.debug;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class FriendControl : BaseControl<FriendControl>
    {
        private Dictionary<ulong, bool> accepts = new Dictionary<ulong, bool>();

        public void MoveToBlackList(ulong roleId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_7.write_7_7(msdata, roleId);
            AppNet.gameNet.send(msdata, 7, 7);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("1793", new NetMsgCallback(this.ReceiveFriendsInfo_7_1));
            AppNet.main.addCMD("1794", new NetMsgCallback(this.ReceiveAskInfo_7_2));
            AppNet.main.addCMD("1795", new NetMsgCallback(this.ReceiveFriendAskInfo_7_3));
            AppNet.main.addCMD("1796", new NetMsgCallback(this.ReceiveReplyInfo_7_4));
            AppNet.main.addCMD("1797", new NetMsgCallback(this.ReceiveAskReplyInfo_7_5));
            AppNet.main.addCMD("1798", new NetMsgCallback(this.ReceiveDeleteFriendInfo_7_6));
            AppNet.main.addCMD("1799", new NetMsgCallback(this.ReceiveMoveToBlackList_7_7));
            AppNet.main.addCMD("1800", new NetMsgCallback(this.ReceiveFriendDeleted_7_8));
            AppNet.main.addCMD("1801", new NetMsgCallback(this.ReceiveFriendOnline_7_9));
            AppNet.main.addCMD("1802", new NetMsgCallback(this.ReceiveFriendAskByName_7_10));
            AppNet.main.addCMD("1803", new NetMsgCallback(this.ReceiveDeleteBlackList_7_11));
            AppNet.main.addCMD("1804", new NetMsgCallback(this.ReceiveFriendLevelUp_7_12));
            AppNet.main.addCMD("1806", new NetMsgCallback(this.ReceiveFriendIntimate_7_14));
            AppNet.main.addCMD("1809", new NetMsgCallback(this.ReceiveFriendNumInfo_7_17));
            AppNet.main.addCMD("1810", new NetMsgCallback(this.ReceiveNearByInfo_7_18));
            this.SendRequestForFriendInfo();
        }

        private void ReceiveAskInfo_7_2(INetData data)
        {
            RelationAcceptMsg_7_2 g__ = new RelationAcceptMsg_7_2();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("FriendControl.Suc"));
            }
        }

        private void ReceiveAskReplyInfo_7_5(INetData data)
        {
            string word;
            RelationPushAnswerMsg_7_5 g__ = new RelationPushAnswerMsg_7_5();
            g__.read(data.GetMemoryStream());
            if (g__.answer == 0)
            {
                word = LanguageManager.GetWord("FriendControl.Reject");
            }
            else
            {
                word = LanguageManager.GetWord("FriendControl.Accept");
                PRelationInfo info = new PRelationInfo {
                    roleId = g__.roleId,
                    sex = g__.sex,
                    name = g__.name,
                    job = g__.job,
                    lvl = g__.lvl,
                    vip = g__.vip,
                    isOnline = 1,
                    intimate = 0
                };
                Singleton<FriendMode>.Instance.AddFriend(info);
            }
            MessageManager.Show(g__.name + word + LanguageManager.GetWord("FriendControl.FriendAsk"));
        }

        private void ReceiveDeleteBlackList_7_11(INetData data)
        {
            RelationDeleteBlackMsg_7_11 g__ = new RelationDeleteBlackMsg_7_11();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                Singleton<FriendMode>.Instance.DeleteBlackList(g__.roleId);
            }
        }

        private void ReceiveDeleteFriendInfo_7_6(INetData data)
        {
            RelationDeleteMsg_7_6 g__ = new RelationDeleteMsg_7_6();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                Singleton<FriendMode>.Instance.DeleteFriend(g__.roleId);
            }
        }

        private void ReceiveFriendAskByName_7_10(INetData data)
        {
            RelationAcceptNameMsg_7_10 g__ = new RelationAcceptNameMsg_7_10();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("FriendControl.Suc"));
            }
        }

        private void ReceiveFriendAskInfo_7_3(INetData data)
        {
            RelationPushAcceptMsg_7_3 ask = new RelationPushAcceptMsg_7_3();
            ask.read(data.GetMemoryStream());
            if (Singleton<FriendMode>.Instance.AddFriendAskMsg(ask))
            {
                MessageManager.Show(LanguageManager.GetWord("FriendView.FriendAsk"));
            }
        }

        private void ReceiveFriendDeleted_7_8(INetData data)
        {
            RelationPushDelMsg_7_8 g__ = new RelationPushDelMsg_7_8();
            g__.read(data.GetMemoryStream());
            Singleton<FriendMode>.Instance.DeleteFriend(g__.roleId);
        }

        private void ReceiveFriendIntimate_7_14(INetData data)
        {
            RelationUpdateIntimateMsg_7_14 g__ = new RelationUpdateIntimateMsg_7_14();
            g__.read(data.GetMemoryStream());
            Singleton<FriendMode>.Instance.SetFriendIntimate(g__.roleId, g__.intimate);
        }

        private void ReceiveFriendLevelUp_7_12(INetData data)
        {
            RelationFriendsLvlMsg_7_12 g__ = new RelationFriendsLvlMsg_7_12();
            g__.read(data.GetMemoryStream());
            Singleton<FriendMode>.Instance.SetFriendIntimate(g__.roleId, g__.lvl);
        }

        private void ReceiveFriendNumInfo_7_17(INetData data)
        {
            RelationFriendsCountMsg_7_17 g__ = new RelationFriendsCountMsg_7_17();
            g__.read(data.GetMemoryStream());
            Singleton<FriendMode>.Instance.SetFriendNumInfo(g__.nowCount, g__.maxCount);
        }

        private void ReceiveFriendOnline_7_9(INetData data)
        {
            RelationPushOnlineMsg_7_9 g__ = new RelationPushOnlineMsg_7_9();
            g__.read(data.GetMemoryStream());
            Singleton<FriendMode>.Instance.SetFriendOlineInfo(g__.roleId, g__.isOnline);
        }

        private void ReceiveFriendsInfo_7_1(INetData data)
        {
            RelationInfoMsg_7_1 g__ = new RelationInfoMsg_7_1();
            g__.read(data.GetMemoryStream());
            Log.info(this, "friend list " + g__.friends.Count);
            Singleton<FriendMode>.Instance.SetFriendInfoList(g__.friends, g__.blacks);
        }

        private void ReceiveMoveToBlackList_7_7(INetData data)
        {
            RelationMoveMsg_7_7 g__ = new RelationMoveMsg_7_7();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("FriendControl.Suc"));
                Singleton<FriendMode>.Instance.MoveToBlackList(g__.roleId);
            }
        }

        private void ReceiveNearByInfo_7_18(INetData data)
        {
            RelationNearMsg_7_18 g__ = new RelationNearMsg_7_18();
            g__.read(data.GetMemoryStream());
            Log.info(this, "ReceiveNearByInfo_7_18 " + g__.near.Count);
            Singleton<FriendMode>.Instance.SetNearbyPlayerInfo(g__.near);
        }

        private void ReceiveReplyInfo_7_4(INetData data)
        {
            RelationAnswerAcceptMsg_7_4 g__ = new RelationAnswerAcceptMsg_7_4();
            g__.read(data.GetMemoryStream());
            bool accept = false;
            if (this.accepts.ContainsKey(g__.roleId))
            {
                accept = this.accepts[g__.roleId];
                this.accepts.Remove(g__.roleId);
            }
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
                Singleton<FriendMode>.Instance.AcceptFriend(g__.roleId, false);
            }
            else
            {
                Singleton<FriendMode>.Instance.AcceptFriend(g__.roleId, accept);
            }
        }

        public void ReplyFriendAskInfo(uint roleId, bool accetp)
        {
            MemoryStream msdata = new MemoryStream();
            byte answer = !accetp ? ((byte) 0) : ((byte) 1);
            Module_7.write_7_4(msdata, (ulong) roleId, answer);
            this.accepts[(ulong) roleId] = accetp;
            AppNet.gameNet.send(msdata, 7, 4);
        }

        public void SendDeleteBlackList(uint roleId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_7.write_7_11(msdata, (ulong) roleId);
            AppNet.gameNet.send(msdata, 7, 11);
        }

        public void SendDeleteFriendInfo(uint roleId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_7.write_7_6(msdata, (ulong) roleId);
            AppNet.gameNet.send(msdata, 7, 6);
        }

        public void SendFriendAskByName(string name)
        {
            MemoryStream msdata = new MemoryStream();
            Module_7.write_7_10(msdata, name);
            AppNet.gameNet.send(msdata, 7, 10);
        }

        public void SendFriendAskInfo(uint roleId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_7.write_7_2(msdata, (ulong) roleId);
            AppNet.gameNet.send(msdata, 7, 2);
        }

        public void SendRequestForFriendInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_7.write_7_1(msdata);
            Log.info(this, "SendRequestForFriendInfo ");
        }

        public void SendRequestForNearByInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_7.write_7_18(msdata);
            AppNet.gameNet.send(msdata, 7, 0x12);
        }
    }
}

