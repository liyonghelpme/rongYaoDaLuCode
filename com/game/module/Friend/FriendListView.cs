﻿namespace com.game.module.Friend
{
    using com.game.consts;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.u3d.bases.debug;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class FriendListView : BaseView<FriendListView>
    {
        private GameObject container;
        private UISprite down;
        private bool forTalk;
        private readonly int gapY = 10;
        private readonly int infoY = 0x38;
        private UIGrid onCenterGrid;
        private GameObject role;
        private int roleInfoNum;
        private List<GameObject> roleList = new List<GameObject>();
        private readonly int showNum = 6;
        private readonly int topY = -2;
        private UISprite up;

        private GameObject AddRoleObjet()
        {
            if (this.roleList.Count == 0)
            {
                this.role.name = "role" + ((0x3e8 + this.roleList.Count) + 1) + '_';
                this.roleList.Add(this.role);
                this.role.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.RoleInfoClick);
                return this.role;
            }
            GameObject item = (GameObject) UnityEngine.Object.Instantiate(this.roleList[0]);
            item.name = "role" + ((0x3e8 + this.roleList.Count) + 1) + "_";
            item.transform.parent = this.onCenterGrid.transform;
            item.transform.localScale = new Vector3(1f, 1f, 1f);
            item.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.RoleInfoClick);
            this.roleList.Add(item);
            return item;
        }

        public override void CancelUpdateHandler()
        {
            Singleton<FriendMode>.Instance.dataUpdated -= new DataUpateHandler(this.RoleDataUpdated);
        }

        private void CloseClick(GameObject obj)
        {
            base.CloseView();
        }

        private void FuncTalk(GameObject obj)
        {
            if (NGUITools.FindInChild<UILabel>(obj, "name").color == ColorConst.FONT_GRAY)
            {
                MessageManager.Show("FriendView.OffLine");
            }
            else
            {
                string text = NGUITools.FindInChild<UILabel>(obj, "name").text;
                Singleton<ChatView>.Instance.OpenPrivateChatView(text, this.getRoleId(obj));
            }
        }

        private GameObject GetNextRoleObject()
        {
            GameObject obj2;
            if (this.roleList.Count > this.roleInfoNum)
            {
                obj2 = this.roleList[this.roleInfoNum];
                obj2.transform.parent = this.onCenterGrid.transform;
            }
            else
            {
                obj2 = this.AddRoleObjet();
            }
            this.roleInfoNum++;
            return obj2;
        }

        private uint getRoleId(GameObject obj)
        {
            char[] separator = new char[] { '_' };
            string s = obj.name.Split(separator)[1];
            Log.info(this, "roleid" + s);
            return uint.Parse(s);
        }

        protected override void HandleAfterOpenView()
        {
            Singleton<FriendControl>.Instance.SendRequestForFriendInfo();
        }

        protected override void Init()
        {
            base.FindInChild<Button>("button_close").onClick = new UIWidgetContainer.VoidDelegate(this.CloseClick);
            this.container = base.FindChild("body/container");
            UIScrollView component = this.container.GetComponent<UIScrollView>();
            component.onDragFinished = (UIScrollView.OnDragFinished) Delegate.Combine(component.onDragFinished, new UIScrollView.OnDragFinished(this.OnDragFinish));
            this.onCenterGrid = base.FindInChild<UIGrid>("body/container/oncenter");
            this.role = base.FindChild("body/container/oncenter/role");
            this.role.SetActive(false);
            this.up = base.FindInChild<UISprite>("body/up/icn");
            this.down = base.FindInChild<UISprite>("body/down/icn");
            this.initLabel();
        }

        private void initLabel()
        {
            base.FindInChild<UILabel>("head/friend/label").text = LanguageManager.GetWord("FriendView.Friend");
            base.FindInChild<UILabel>("num").text = LanguageManager.GetWord("FriendView.FriendNum");
        }

        private bool IsBottom()
        {
            int num = (-this.topY + ((this.roleInfoNum - this.showNum) * this.infoY)) - this.gapY;
            return (this.container.transform.localPosition.y >= num);
        }

        private bool IsTop()
        {
            return ((this.container.transform.localPosition.y <= this.topY) || (this.roleInfoNum < this.showNum));
        }

        private void OnDragFinish()
        {
            Log.info(this, "container.transform.localPosition.y:" + this.container.transform.localPosition.y);
            this.SetUpDownIcn();
        }

        public void openViewForTalk()
        {
            this.forTalk = true;
            base.OpenView();
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<FriendMode>.Instance.dataUpdated += new DataUpateHandler(this.RoleDataUpdated);
        }

        public void RoleDataUpdated(object sender, int code)
        {
            if (code == 1)
            {
                List<PRelationInfo> friendsList = Singleton<FriendMode>.Instance.friendsList;
                this.ShowAllPRelationInfo(friendsList);
            }
            else if (code == 5)
            {
                this.SetFriendNumInfo();
            }
        }

        private void RoleInfoClick(GameObject obj)
        {
            this.SetRoleClickEffect(obj, "exp", 1);
            if (this.forTalk)
            {
                this.FuncTalk(obj);
            }
            this.SetRoleClickEffect(obj, "fgx5656", -1);
            this.CloseClick(null);
        }

        private void SetFriendNumInfo()
        {
            int friendNum = Singleton<FriendMode>.Instance.friendNum;
            int maxNum = Singleton<FriendMode>.Instance.maxNum;
            base.FindInChild<UILabel>("numvalue").text = friendNum + "/" + maxNum;
        }

        private void SetRelationRoleInfo(GameObject obj, PRelationInfo relation)
        {
            obj.name = obj.name + relation.roleId.ToString();
            NGUITools.FindInChild<UILabel>(obj, "name").text = relation.name;
            if (relation.isOnline == 1)
            {
                NGUITools.FindInChild<UILabel>(obj, "name").color = ColorConst.FONT_LIGHT;
            }
            else
            {
                NGUITools.FindInChild<UILabel>(obj, "name").color = ColorConst.FONT_GRAY;
            }
            obj.SetActive(true);
        }

        private void SetRoleClickEffect(GameObject role, string icnName, int depth)
        {
            NGUITools.FindInChild<UISprite>(role, "icn1").spriteName = icnName;
            UISprite local1 = NGUITools.FindInChild<UISprite>(role, "icn1");
            local1.depth += depth;
            NGUITools.FindInChild<UISprite>(role, "icn2").spriteName = icnName;
            UISprite local2 = NGUITools.FindInChild<UISprite>(role, "icn2");
            local2.depth += depth;
        }

        private void SetUpDownIcn()
        {
            if (this.roleInfoNum <= this.showNum)
            {
                this.down.gameObject.SetActive(false);
                this.up.gameObject.SetActive(false);
            }
            else if (this.IsTop())
            {
                this.down.spriteName = "banyuan1";
                this.up.gameObject.SetActive(false);
                this.down.gameObject.SetActive(true);
            }
            else if (this.IsBottom())
            {
                this.up.spriteName = "banyuan1";
                this.down.gameObject.SetActive(false);
                this.up.gameObject.SetActive(true);
            }
            else
            {
                this.down.gameObject.SetActive(false);
                this.up.gameObject.SetActive(false);
            }
        }

        private void ShowAllPRelationInfo(List<PRelationInfo> relationList)
        {
            this.roleInfoNum = 0;
            foreach (PRelationInfo info in relationList)
            {
                if (info.isOnline == 1)
                {
                    this.role = this.GetNextRoleObject();
                    Log.info(this, "relation.roleId:" + info.roleId);
                    this.SetRelationRoleInfo(this.role, info);
                }
            }
            foreach (PRelationInfo info2 in relationList)
            {
                if (info2.isOnline != 1)
                {
                    this.role = this.GetNextRoleObject();
                    this.SetRelationRoleInfo(this.role, info2);
                }
            }
            this.onCenterGrid.repositionNow = true;
            this.SetUpDownIcn();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Friend/FriendListView.assetbundle";
            }
        }
    }
}

