﻿namespace com.game.module.Friend
{
    using com.game.consts;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.u3d.bases.debug;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class FriendView : BaseView<FriendView>
    {
        private GameObject addView;
        private UIToggle ask;
        private UIToggle blackList;
        private GameObject container;
        private GameObject currentRole;
        private UIToggle currentUIToggle;
        private UIToggle friend;
        private GameObject funcView;
        private readonly int[] funPosx4 = new int[] { 0xd0, -210, -70, 70 };
        private readonly int[] funPosx5 = new int[] { 0xd0, -210, -70, 70, 70 };
        private readonly int gapX = 0x12;
        private UIInput input;
        private UILabel inputName;
        private UIToggle nearby;
        private UIGrid onCenterGrid;
        private GameObject role;
        private int roleInfoNum;
        private List<GameObject> roleList;
        private readonly Vector3 scale = new Vector3(1f, 1f, 1f);
        private GameObject title;

        private void AcceptClick(GameObject obj)
        {
            uint roleId = this.getRoleId(obj.transform.parent.transform.parent.gameObject);
            Singleton<FriendControl>.Instance.ReplyFriendAskInfo(roleId, true);
        }

        private void AddClick(GameObject obj)
        {
            this.addView.SetActive(true);
        }

        private void AddCloseClick(GameObject go)
        {
            this.addView.SetActive(false);
        }

        private void AddFriendByNameClick(GameObject obj)
        {
            if (((this.inputName.text != null) && !this.inputName.text.Equals(string.Empty)) && (this.inputName.text.Length > 1))
            {
                if (this.inputName.text.Length > 6)
                {
                    MessageManager.Show(LanguageManager.GetWord("FriendView.MaxLength"));
                }
                else
                {
                    Singleton<FriendControl>.Instance.SendFriendAskByName(this.inputName.text);
                    this.addView.SetActive(false);
                    this.inputName.text = LanguageManager.GetWord("FriendView.InputName");
                    this.input.value = string.Empty;
                }
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("FriendView.Empty"));
            }
        }

        private void AddMaxClick(GameObject obj)
        {
            MessageManager.Show(LanguageManager.GetWord("FriendView.MaxNum"));
        }

        private GameObject AddRoleObjet()
        {
            if (this.roleList.Count == 0)
            {
                this.role.name = "role" + ((0x3e8 + this.roleList.Count) + 1) + '_';
                this.roleList.Add(this.role);
                this.role.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.RoleInfoClick);
                NGUITools.FindInChild<Button>(this.role, "button").onClick = new UIWidgetContainer.VoidDelegate(this.OnHandleClick);
                return this.role;
            }
            GameObject parent = (GameObject) UnityEngine.Object.Instantiate(this.roleList[0]);
            parent.name = "role" + ((0x3e8 + this.roleList.Count) + 1) + "_";
            parent.transform.parent = this.onCenterGrid.transform;
            parent.transform.localScale = this.scale;
            parent.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.RoleInfoClick);
            NGUITools.FindInChild<Button>(parent, "button").onClick = new UIWidgetContainer.VoidDelegate(this.OnHandleClick);
            this.roleList.Add(parent);
            return parent;
        }

        public override void CancelUpdateHandler()
        {
            Singleton<FriendMode>.Instance.dataUpdated -= new DataUpateHandler(this.RoleDataUpdated);
        }

        private void CloseClick(GameObject go)
        {
            this.CloseView();
        }

        private void FuncAddBlackList(GameObject obj)
        {
            if (this.currentRole != null)
            {
                Singleton<FriendControl>.Instance.MoveToBlackList((ulong) this.getRoleId(this.currentRole));
            }
            this.FuncCloseClick(null);
        }

        private void FuncAddFriend(GameObject obj)
        {
            if (NGUITools.FindInChild<UILabel>(this.currentRole, "name").color == ColorConst.FONT_GRAY)
            {
                MessageManager.Show(LanguageManager.GetWord("FriendView.OffLine"));
            }
            else
            {
                if (this.currentRole != null)
                {
                    Singleton<FriendControl>.Instance.SendFriendAskInfo(this.getRoleId(this.currentRole));
                }
                this.FuncCloseClick(null);
            }
        }

        private void FuncCloseClick(GameObject go)
        {
            this.funcView.SetActive(false);
            if ((this.currentRole != null) && this.currentRole.activeSelf)
            {
                this.SetRoleClickEffect(this.currentRole, "fgx5656", -1);
            }
            this.currentRole = null;
        }

        private void FuncDeleteBlackList(GameObject obj)
        {
            if (this.currentRole != null)
            {
                Singleton<FriendControl>.Instance.SendDeleteBlackList(this.getRoleId(this.currentRole));
            }
            this.FuncCloseClick(null);
        }

        private void FuncDeleteFriend(GameObject obj)
        {
            if (this.currentRole != null)
            {
                Singleton<FriendControl>.Instance.SendDeleteFriendInfo(this.getRoleId(this.currentRole));
            }
            this.FuncCloseClick(null);
        }

        private void FuncLook(GameObject obj)
        {
        }

        private void FuncTalk(GameObject obj)
        {
            if (NGUITools.FindInChild<UILabel>(this.currentRole, "name").color == ColorConst.FONT_GRAY)
            {
                MessageManager.Show(LanguageManager.GetWord("FriendView.OffLine"));
            }
            else
            {
                string text = NGUITools.FindInChild<UILabel>(this.currentRole, "name").text;
                Singleton<ChatView>.Instance.OpenPrivateChatView(text, this.getRoleId(this.currentRole));
                this.FuncCloseClick(null);
            }
        }

        private string GetJobInfo(byte job)
        {
            if (job == 1)
            {
                return "zyjs";
            }
            if (job == 2)
            {
                return "zyfs";
            }
            if (job == 3)
            {
                return "zyck";
            }
            return string.Empty;
        }

        private GameObject GetNextRoleObject()
        {
            GameObject obj2;
            if (this.roleList.Count > this.roleInfoNum)
            {
                obj2 = this.roleList[this.roleInfoNum];
                obj2.transform.parent = this.onCenterGrid.transform;
            }
            else
            {
                obj2 = this.AddRoleObjet();
            }
            this.roleInfoNum++;
            return obj2;
        }

        private string getOnlineInfo(PRelationInfo info)
        {
            if (info.isOnline == 1)
            {
                return LanguageManager.GetWord("FriendView.Online");
            }
            long s = (long) (Time.time - info.lastLogoutTime);
            return this.getTimeDesByS(s);
        }

        private uint getRoleId(GameObject obj)
        {
            char[] separator = new char[] { '_' };
            string s = obj.name.Split(separator)[1];
            Log.info(this, "roleid" + s);
            return uint.Parse(s);
        }

        private string getTimeDesByS(long s)
        {
            int num = (int) (s / 0xe10L);
            if (num <= 0)
            {
                return LanguageManager.GetWord("FriendView.InHour");
            }
            if (num > 0x18)
            {
                return LanguageManager.GetWord("FriendView.DayBefore");
            }
            return LanguageManager.GetWord(num + "FriendView.HourBefore");
        }

        protected override void HandleAfterOpenView()
        {
            this.nearby.onStateChange = new UIToggle.OnStateChange(this.OnToggleChange);
            this.ask.onStateChange = new UIToggle.OnStateChange(this.OnToggleChange);
            this.blackList.onStateChange = new UIToggle.OnStateChange(this.OnToggleChange);
            this.friend.onStateChange = new UIToggle.OnStateChange(this.OnToggleChange);
            if (Singleton<FriendMode>.Instance.askList.Count > 0)
            {
                if (this.currentUIToggle != this.ask)
                {
                    this.ask.value = true;
                }
                else
                {
                    this.SetAskListInfo();
                }
            }
            else
            {
                if (this.currentUIToggle == this.friend)
                {
                    this.SetFriendInfo(false);
                }
                if (this.currentUIToggle == this.nearby)
                {
                    this.SetNearbyInfo(false);
                }
                if (this.currentUIToggle == this.ask)
                {
                    this.SetAskListInfo();
                }
                if (this.currentUIToggle == this.blackList)
                {
                    this.SetBlackListInfo();
                }
            }
        }

        protected override void Init()
        {
            this.roleList = new List<GameObject>();
            this.title = base.FindChild("body/title");
            this.container = base.FindChild("body/container");
            this.onCenterGrid = base.FindInChild<UIGrid>("body/container/oncenter");
            base.FindInChild<Button>("button_close").onClick = new UIWidgetContainer.VoidDelegate(this.CloseClick);
            this.role = base.FindChild("body/container/oncenter/role");
            this.role.SetActive(false);
            this.funcView = base.FindChild("function");
            base.FindInChild<Button>("function/btn_close").onClick = new UIWidgetContainer.VoidDelegate(this.FuncCloseClick);
            this.addView = base.FindChild("add");
            base.FindInChild<Button>("add/btn_close").onClick = new UIWidgetContainer.VoidDelegate(this.AddCloseClick);
            base.FindInChild<Button>("add/button").onClick = new UIWidgetContainer.VoidDelegate(this.AddFriendByNameClick);
            this.inputName = base.FindInChild<UILabel>("add/input/name");
            this.input = base.FindInChild<UIInput>("add/input");
            base.FindInChild<Button>("body/container/oncenter/role/handle/reject").onClick = new UIWidgetContainer.VoidDelegate(this.RejectClick);
            base.FindInChild<Button>("body/container/oncenter/role/handle/accept").onClick = new UIWidgetContainer.VoidDelegate(this.AcceptClick);
            base.FindInChild<Button>("button").onClick = new UIWidgetContainer.VoidDelegate(this.AddClick);
            this.ask = base.FindInChild<UIToggle>("head/ask");
            this.blackList = base.FindInChild<UIToggle>("head/blacklist");
            this.friend = base.FindInChild<UIToggle>("head/friend");
            this.currentUIToggle = this.friend;
            this.nearby = base.FindInChild<UIToggle>("head/nearby");
            this.initLabel();
        }

        private void initLabel()
        {
            base.FindInChild<UILabel>("head/ask/label").text = LanguageManager.GetWord("FriendView.Ask");
            base.FindInChild<UILabel>("head/friend/label").text = LanguageManager.GetWord("FriendView.Friend");
            base.FindInChild<UILabel>("head/blacklist/label").text = LanguageManager.GetWord("FriendView.BlackList");
            base.FindInChild<UILabel>("head/nearby/label").text = LanguageManager.GetWord("FriendView.Nearby");
            base.FindInChild<UILabel>("body/title/name").text = LanguageManager.GetWord("FriendView.Name");
            base.FindInChild<UILabel>("body/title/time").text = LanguageManager.GetWord("FriendView.Time");
            base.FindInChild<UILabel>("body/title/job").text = LanguageManager.GetWord("FriendView.Job");
            base.FindInChild<UILabel>("body/title/lvl").text = LanguageManager.GetWord("FriendView.LVL");
            base.FindInChild<UILabel>("body/title/fight").text = LanguageManager.GetWord("FriendView.Fight");
            base.FindInChild<UILabel>("body/title/handle").text = LanguageManager.GetWord("FriendView.Handle");
            base.FindInChild<UILabel>("num").text = LanguageManager.GetWord("FriendView.FriendNum");
            NGUITools.FindInChild<UILabel>(this.role, "handle/accept/label").text = LanguageManager.GetWord("FriendView.Accept");
            NGUITools.FindInChild<UILabel>(this.role, "handle/reject/label").text = LanguageManager.GetWord("FriendView.Reject");
            this.inputName.text = LanguageManager.GetWord("FriendView.InputName");
        }

        private void OnHandleClick(GameObject obj)
        {
            if (this.currentUIToggle == this.blackList)
            {
                Singleton<FriendControl>.Instance.SendDeleteBlackList(this.getRoleId(obj.transform.parent.gameObject));
            }
            else if (this.currentUIToggle == this.nearby)
            {
                Singleton<FriendControl>.Instance.SendFriendAskInfo(this.getRoleId(obj.transform.parent.gameObject));
            }
            else if (this.currentUIToggle == this.friend)
            {
                string text = NGUITools.FindInChild<UILabel>(obj.transform.parent.gameObject, "name").text;
                Singleton<ChatView>.Instance.OpenPrivateChatView(text, this.getRoleId(obj.transform.parent.gameObject));
            }
        }

        private void OnToggleChange(bool state)
        {
            if (UIToggle.current == this.nearby)
            {
                if (state)
                {
                    this.currentUIToggle = this.nearby;
                    this.SetNearbyInfo(false);
                }
            }
            else if (UIToggle.current == this.ask)
            {
                if (state)
                {
                    this.currentUIToggle = this.ask;
                    this.SetAskListInfo();
                }
            }
            else if (UIToggle.current == this.blackList)
            {
                if (state)
                {
                    this.currentUIToggle = this.blackList;
                    this.SetBlackListInfo();
                }
            }
            else if ((UIToggle.current == this.friend) && state)
            {
                this.currentUIToggle = this.friend;
                this.SetFriendInfo(false);
            }
            if (UIToggle.current != this.ask)
            {
                NGUITools.FindChild(this.ask.gameObject, "tips").SetActive(Singleton<FriendMode>.Instance.ShowTips);
            }
            else
            {
                NGUITools.FindChild(this.ask.gameObject, "tips").SetActive(false);
            }
        }

        private void RefreshClick(GameObject obj)
        {
            Singleton<FriendControl>.Instance.SendRequestForNearByInfo();
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<FriendMode>.Instance.dataUpdated += new DataUpateHandler(this.RoleDataUpdated);
        }

        private void RejectClick(GameObject obj)
        {
            uint roleId = this.getRoleId(obj.transform.parent.transform.parent.gameObject);
            Singleton<FriendControl>.Instance.ReplyFriendAskInfo(roleId, false);
        }

        private void ReSetRoleListActive()
        {
            this.roleInfoNum = 0;
            foreach (GameObject obj2 in this.roleList)
            {
                obj2.SetActive(false);
                obj2.transform.parent = this.gameObject.transform;
                char[] separator = new char[] { '_' };
                obj2.name = obj2.name.Split(separator)[0] + '_';
            }
        }

        public void RoleDataUpdated(object sender, int code)
        {
            if ((code == 3) && (this.currentUIToggle == this.nearby))
            {
                this.SetNearbyInfo(true);
            }
            if (code == 4)
            {
                if (this.currentUIToggle == this.ask)
                {
                    this.SetAskListInfo();
                    NGUITools.FindChild(this.ask.gameObject, "tips").SetActive(false);
                }
                else
                {
                    NGUITools.FindChild(this.ask.gameObject, "tips").SetActive(Singleton<FriendMode>.Instance.ShowTips);
                }
            }
            if ((code == 2) && (this.currentUIToggle == this.blackList))
            {
                this.SetBlackListInfo();
            }
            if ((code == 1) && (this.currentUIToggle == this.friend))
            {
                this.SetFriendInfo(true);
            }
            if (code == 5)
            {
                this.SetFriendNumInfo();
            }
        }

        private void RoleInfoClick(GameObject obj)
        {
            this.SetRoleClickEffect(obj, "exp", 1);
            this.currentRole = obj;
            this.funcView.SetActive(true);
        }

        private void RoleViewCloseClick(GameObject go)
        {
            this.gameObject.SetActive(true);
        }

        private void RoleViewOpenCallBack(GameObject obj)
        {
            this.gameObject.SetActive(false);
            this.FuncCloseClick(null);
        }

        private void SetAskFunctionButtons()
        {
            NGUITools.FindInChild<Button>(this.funcView, "button1").onClick = new UIWidgetContainer.VoidDelegate(this.FuncLook);
            NGUITools.FindInChild<UILabel>(this.funcView, "button1/label").text = LanguageManager.GetWord("FriendView.Look");
            this.funcView.transform.FindChild("button1").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button2").onClick = new UIWidgetContainer.VoidDelegate(this.FuncAddBlackList);
            NGUITools.FindInChild<UILabel>(this.funcView, "button2/label").text = LanguageManager.GetWord("FriendView.AddBlackList");
            this.funcView.transform.FindChild("button2").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button3").onClick = new UIWidgetContainer.VoidDelegate(this.FuncTalk);
            NGUITools.FindInChild<UILabel>(this.funcView, "button3/label").text = LanguageManager.GetWord("FriendView.Talk");
            this.funcView.transform.FindChild("button3").gameObject.SetActive(true);
            this.funcView.transform.FindChild("button5").gameObject.SetActive(false);
            this.setFunButtonPos4();
        }

        private void SetAskListInfo()
        {
            this.ReSetRoleListActive();
            this.SetASkTitle(this.title);
            this.SetAskFunctionButtons();
            List<RelationPushAcceptMsg_7_3> askList = Singleton<FriendMode>.Instance.askList;
            this.ShowAllAskInfo(askList);
            this.onCenterGrid.repositionNow = true;
        }

        private void SetAskRoleInfo(GameObject obj, RelationPushAcceptMsg_7_3 ask)
        {
            obj.name = obj.name + ask.roleId.ToString();
            NGUITools.FindInChild<UILabel>(obj, "name").text = ask.name;
            NGUITools.FindInChild<UILabel>(obj, "lvl").text = ask.lvl.ToString();
            NGUITools.FindInChild<UISprite>(obj, "job").spriteName = this.GetJobInfo(ask.job);
            NGUITools.FindInChild<UILabel>(obj, "fight").text = ask.fightpoint.ToString();
            NGUITools.FindInChild<UILabel>(obj, "name").color = ColorConst.FONT_LIGHT;
            NGUITools.FindInChild<UILabel>(obj, "lvl").color = ColorConst.FONT_BLUE;
            NGUITools.FindInChild<UILabel>(obj, "fight").color = ColorConst.FONT_BLUE;
            if (ask.vip > 0)
            {
                NGUITools.FindChild(obj, "vip").SetActive(true);
                NGUITools.FindInChild<UILabel>(obj, "vip/vipvalue").text = ask.vip.ToString();
            }
            else
            {
                NGUITools.FindChild(obj, "vip").SetActive(false);
            }
            obj.transform.FindChild("button").gameObject.SetActive(false);
            obj.transform.FindChild("time").gameObject.SetActive(false);
            obj.transform.FindChild("handle").gameObject.SetActive(true);
            obj.SetActive(true);
        }

        private void SetASkTitle(GameObject obj)
        {
            obj.transform.FindChild("handle").gameObject.SetActive(true);
            obj.transform.FindChild("time").gameObject.SetActive(false);
        }

        private void SetBlackListFunctionButtons()
        {
            NGUITools.FindInChild<Button>(this.funcView, "button1").onClick = new UIWidgetContainer.VoidDelegate(this.FuncLook);
            NGUITools.FindInChild<UILabel>(this.funcView, "button1/label").text = LanguageManager.GetWord("FriendView.Look");
            this.funcView.transform.FindChild("button1").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button2").onClick = new UIWidgetContainer.VoidDelegate(this.FuncDeleteBlackList);
            NGUITools.FindInChild<UILabel>(this.funcView, "button2/label").text = LanguageManager.GetWord("FriendView.DeleteBlackList");
            this.funcView.transform.FindChild("button2").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button3").onClick = new UIWidgetContainer.VoidDelegate(this.FuncAddFriend);
            NGUITools.FindInChild<UILabel>(this.funcView, "button3/label").text = LanguageManager.GetWord("FriendView.AddFriend");
            this.funcView.transform.FindChild("button3").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button5").onClick = new UIWidgetContainer.VoidDelegate(this.FuncTalk);
            NGUITools.FindInChild<UILabel>(this.funcView, "button5/label").text = LanguageManager.GetWord("FriendView.Talk");
            this.funcView.transform.FindChild("button5").gameObject.SetActive(false);
            this.setFunButtonPos4();
        }

        private void SetBlackListInfo()
        {
            this.ReSetRoleListActive();
            this.SetBlackListTitle(this.title);
            this.SetBlackListFunctionButtons();
            if (Singleton<FriendMode>.Instance.blacksList == null)
            {
                Singleton<FriendControl>.Instance.SendRequestForFriendInfo();
            }
            else
            {
                List<PRelationInfo> blacksList = Singleton<FriendMode>.Instance.blacksList;
                this.ShowAllPRelationInfo(blacksList);
            }
            this.onCenterGrid.repositionNow = true;
        }

        private void SetBlackListTitle(GameObject obj)
        {
            obj.transform.FindChild("handle").gameObject.SetActive(true);
            obj.transform.FindChild("time").gameObject.SetActive(false);
        }

        private void SetFriendFunctionButtons()
        {
            NGUITools.FindInChild<Button>(this.funcView, "button1").onClick = new UIWidgetContainer.VoidDelegate(this.FuncLook);
            NGUITools.FindInChild<UILabel>(this.funcView, "button1/label").text = LanguageManager.GetWord("FriendView.Look");
            this.funcView.transform.FindChild("button1").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button2").onClick = new UIWidgetContainer.VoidDelegate(this.FuncDeleteFriend);
            NGUITools.FindInChild<UILabel>(this.funcView, "button2/label").text = LanguageManager.GetWord("FriendView.DeleteFriend");
            this.funcView.transform.FindChild("button2").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button3").onClick = new UIWidgetContainer.VoidDelegate(this.FuncAddBlackList);
            NGUITools.FindInChild<UILabel>(this.funcView, "button3/label").text = LanguageManager.GetWord("FriendView.AddBlackList");
            this.funcView.transform.FindChild("button3").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button5").onClick = new UIWidgetContainer.VoidDelegate(this.FuncTalk);
            NGUITools.FindInChild<UILabel>(this.funcView, "button5/label").text = LanguageManager.GetWord("FriendView.Talk");
            this.funcView.transform.FindChild("button5").gameObject.SetActive(true);
            this.setFunButtonPos5();
        }

        private void SetFriendInfo(bool dataUpdate)
        {
            this.ReSetRoleListActive();
            this.SetFriendTitle(this.title);
            this.SetFriendFunctionButtons();
            if (!dataUpdate)
            {
                Singleton<FriendControl>.Instance.SendRequestForFriendInfo();
            }
            else
            {
                List<PRelationInfo> friendsList = Singleton<FriendMode>.Instance.friendsList;
                this.ShowAllPRelationInfo(friendsList);
            }
            this.onCenterGrid.repositionNow = true;
        }

        private void SetFriendNumInfo()
        {
            int friendNum = Singleton<FriendMode>.Instance.friendNum;
            int maxNum = Singleton<FriendMode>.Instance.maxNum;
            base.FindInChild<UILabel>("numvalue").text = friendNum.ToString();
            base.FindInChild<UILabel>("numvalue").color = ColorConst.FONT_YELLOW;
            base.FindInChild<UILabel>("numvalue2").text = "/" + maxNum;
        }

        private void SetFriendTitle(GameObject obj)
        {
            obj.transform.FindChild("handle").gameObject.SetActive(true);
            obj.transform.FindChild("time").gameObject.SetActive(false);
        }

        private void setFunButtonPos4()
        {
            this.funcView.transform.FindChild("button1").gameObject.transform.localPosition = new Vector3((float) this.funPosx4[0], -8f, 0f);
            this.funcView.transform.FindChild("button2").gameObject.transform.localPosition = new Vector3((float) this.funPosx4[1], -8f, 0f);
            this.funcView.transform.FindChild("button3").gameObject.transform.localPosition = new Vector3((float) this.funPosx4[2], -8f, 0f);
            this.funcView.transform.FindChild("button4").gameObject.transform.localPosition = new Vector3((float) this.funPosx4[3], -8f, 0f);
        }

        private void setFunButtonPos5()
        {
            this.funcView.transform.FindChild("button1").gameObject.transform.localPosition = new Vector3((float) this.funPosx5[0], -8f, 0f);
            this.funcView.transform.FindChild("button2").gameObject.transform.localPosition = new Vector3((float) this.funPosx5[1], -8f, 0f);
            this.funcView.transform.FindChild("button3").gameObject.transform.localPosition = new Vector3((float) this.funPosx5[2], -8f, 0f);
            this.funcView.transform.FindChild("button4").gameObject.transform.localPosition = new Vector3((float) this.funPosx5[3], -8f, 0f);
            this.funcView.transform.FindChild("button5").gameObject.transform.localPosition = new Vector3((float) this.funPosx5[4], -8f, 0f);
        }

        private void SetNearbyFunctionButtons()
        {
            NGUITools.FindInChild<Button>(this.funcView, "button1").onClick = new UIWidgetContainer.VoidDelegate(this.FuncLook);
            NGUITools.FindInChild<UILabel>(this.funcView, "button1/label").text = LanguageManager.GetWord("FriendView.Look");
            this.funcView.transform.FindChild("button1").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button2").onClick = new UIWidgetContainer.VoidDelegate(this.FuncAddFriend);
            NGUITools.FindInChild<UILabel>(this.funcView, "button2/label").text = LanguageManager.GetWord("FriendView.AddFriend");
            this.funcView.transform.FindChild("button2").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button3").onClick = new UIWidgetContainer.VoidDelegate(this.FuncAddBlackList);
            NGUITools.FindInChild<UILabel>(this.funcView, "button3/label").text = LanguageManager.GetWord("FriendView.AddBlackList");
            this.funcView.transform.FindChild("button3").gameObject.SetActive(true);
            NGUITools.FindInChild<Button>(this.funcView, "button5").onClick = new UIWidgetContainer.VoidDelegate(this.FuncTalk);
            NGUITools.FindInChild<UILabel>(this.funcView, "button5/label").text = LanguageManager.GetWord("FriendView.Talk");
            this.funcView.transform.FindChild("button5").gameObject.SetActive(true);
            this.setFunButtonPos5();
        }

        private void SetNearbyInfo(bool dataUpdate)
        {
            this.ReSetRoleListActive();
            this.SetNearByTitle(this.title);
            this.SetNearbyFunctionButtons();
            if (!dataUpdate)
            {
                Singleton<FriendControl>.Instance.SendRequestForNearByInfo();
            }
            else
            {
                List<PRelationNear> nearByList = Singleton<FriendMode>.Instance.nearByList;
                this.ShowAllPRelationNearInfo(nearByList);
            }
            this.onCenterGrid.repositionNow = true;
        }

        private void SetNearbyRoleInfo(GameObject obj, PRelationNear near)
        {
            obj.name = obj.name + near.roleId.ToString();
            NGUITools.FindInChild<UILabel>(obj, "name").text = near.name;
            NGUITools.FindInChild<UILabel>(obj, "lvl").text = near.lvl.ToString();
            NGUITools.FindInChild<UISprite>(obj, "job").spriteName = this.GetJobInfo(near.job);
            NGUITools.FindInChild<UILabel>(obj, "fight").text = near.fightpoint.ToString();
            NGUITools.FindInChild<UILabel>(obj, "name").color = ColorConst.FONT_LIGHT;
            NGUITools.FindInChild<UILabel>(obj, "lvl").color = ColorConst.FONT_BLUE;
            NGUITools.FindInChild<UILabel>(obj, "fight").color = ColorConst.FONT_BLUE;
            if (near.vip > 0)
            {
                NGUITools.FindChild(obj, "vip").SetActive(true);
                NGUITools.FindInChild<UILabel>(obj, "vip/vipvalue").text = near.vip.ToString();
            }
            else
            {
                NGUITools.FindChild(obj, "vip").SetActive(false);
            }
            obj.transform.FindChild("time").gameObject.SetActive(false);
            obj.transform.FindChild("button").gameObject.SetActive(true);
            NGUITools.FindInChild<UILabel>(obj, "button/label").text = "加为好友";
            obj.transform.FindChild("handle").gameObject.SetActive(false);
            obj.SetActive(true);
        }

        private void SetNearByTitle(GameObject obj)
        {
            obj.transform.FindChild("handle").gameObject.SetActive(true);
            obj.transform.FindChild("time").gameObject.SetActive(false);
        }

        private void SetRelationRoleInfo(GameObject obj, PRelationInfo relation)
        {
            obj.name = obj.name + relation.roleId.ToString();
            NGUITools.FindInChild<UILabel>(obj, "name").text = relation.name;
            NGUITools.FindInChild<UILabel>(obj, "lvl").text = relation.lvl.ToString();
            NGUITools.FindInChild<UISprite>(obj, "job").spriteName = this.GetJobInfo(relation.job);
            NGUITools.FindInChild<UILabel>(obj, "fight").text = relation.fightpoint.ToString();
            if (relation.vip > 0)
            {
                NGUITools.FindChild(obj, "vip").SetActive(true);
                NGUITools.FindInChild<UILabel>(obj, "vip/vipvalue").text = relation.vip.ToString();
            }
            else
            {
                NGUITools.FindChild(obj, "vip").SetActive(false);
            }
            if (relation.isOnline == 1)
            {
                NGUITools.FindInChild<UILabel>(obj, "name").color = ColorConst.FONT_LIGHT;
                NGUITools.FindInChild<UILabel>(obj, "lvl").color = ColorConst.FONT_BLUE;
                NGUITools.FindInChild<UILabel>(obj, "time").color = ColorConst.FONT_GREEN;
                NGUITools.FindInChild<UILabel>(obj, "fight").color = ColorConst.FONT_BLUE;
            }
            else
            {
                NGUITools.FindInChild<UILabel>(obj, "name").color = ColorConst.FONT_GRAY;
                NGUITools.FindInChild<UILabel>(obj, "lvl").color = ColorConst.FONT_GRAY;
                NGUITools.FindInChild<UILabel>(obj, "time").color = ColorConst.FONT_GRAY;
                NGUITools.FindInChild<UILabel>(obj, "fight").color = ColorConst.FONT_GRAY;
            }
            if (this.currentUIToggle == this.friend)
            {
                if (relation.isOnline == 1)
                {
                    obj.transform.FindChild("time").gameObject.SetActive(false);
                    obj.transform.FindChild("button").gameObject.SetActive(true);
                    NGUITools.FindInChild<UILabel>(obj, "button/label").text = "私聊";
                }
                else
                {
                    obj.transform.FindChild("time").gameObject.SetActive(true);
                    obj.transform.FindChild("button").gameObject.SetActive(false);
                }
            }
            if (this.currentUIToggle == this.blackList)
            {
                obj.transform.FindChild("time").gameObject.SetActive(false);
                obj.transform.FindChild("button").gameObject.SetActive(true);
                NGUITools.FindInChild<UILabel>(obj, "button/label").text = "移除";
            }
            obj.transform.FindChild("handle").gameObject.SetActive(false);
            obj.SetActive(true);
        }

        private void SetRoleClickEffect(GameObject role, string icnName, int depth)
        {
            NGUITools.FindInChild<UISprite>(role, "icn1").spriteName = icnName;
            UISprite local1 = NGUITools.FindInChild<UISprite>(role, "icn1");
            local1.depth += depth;
            NGUITools.FindInChild<UISprite>(role, "icn2").spriteName = icnName;
            UISprite local2 = NGUITools.FindInChild<UISprite>(role, "icn2");
            local2.depth += depth;
        }

        private void ShowAllAskInfo(List<RelationPushAcceptMsg_7_3> askList)
        {
            foreach (RelationPushAcceptMsg_7_3 g__ in askList)
            {
                this.role = this.GetNextRoleObject();
                this.SetAskRoleInfo(this.role, g__);
            }
        }

        private void ShowAllPRelationInfo(List<PRelationInfo> relationList)
        {
            foreach (PRelationInfo info in relationList)
            {
                if (info.isOnline == 1)
                {
                    this.role = this.GetNextRoleObject();
                    Log.info(this, "relation.roleId:" + info.roleId);
                    this.SetRelationRoleInfo(this.role, info);
                }
            }
            foreach (PRelationInfo info2 in relationList)
            {
                if (info2.isOnline != 1)
                {
                    this.role = this.GetNextRoleObject();
                    this.SetRelationRoleInfo(this.role, info2);
                }
            }
        }

        private void ShowAllPRelationNearInfo(List<PRelationNear> nearList)
        {
            foreach (PRelationNear near in nearList)
            {
                this.role = this.GetNextRoleObject();
                this.SetNearbyRoleInfo(this.role, near);
            }
        }

        public override bool isDestroy
        {
            get
            {
                return true;
            }
        }

        public override bool IsFullUI
        {
            get
            {
                return true;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Friend/FriendView.assetbundle";
            }
        }
    }
}

