﻿namespace com.game.module.GameSetting
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class GameSettingView : BaseView<GameSettingView>
    {
        private Button _btnJx;
        private Button _btnSz;
        private Button _btnTc;
        private UILabel _jxLabel;
        private UILabel _szLabel;
        private UILabel _tcLabel;

        private void GoOn(GameObject go)
        {
            this.CloseView();
            Time.timeScale = 1f;
        }

        protected override void HandleAfterOpenView()
        {
            Time.timeScale = 0f;
        }

        protected override void Init()
        {
            this._btnJx = base.FindInChild<Button>("btn_jx");
            this._btnSz = base.FindInChild<Button>("btn_sz");
            this._btnTc = base.FindInChild<Button>("btn_tc");
            this._jxLabel = base.FindInChild<UILabel>("btn_jx/label");
            this._szLabel = base.FindInChild<UILabel>("btn_sz/label");
            this._tcLabel = base.FindInChild<UILabel>("btn_tc/label");
            this._btnJx.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._btnJx.onClick, new UIWidgetContainer.VoidDelegate(this.GoOn));
            this._btnTc.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._btnTc.onClick, new UIWidgetContainer.VoidDelegate(this.QuiteCopy));
            this.InitLabel();
        }

        private void InitLabel()
        {
            this._jxLabel.text = LanguageManager.GetWord("GameSettingView.GoOn");
            this._szLabel.text = LanguageManager.GetWord("GameSettingView.Setting");
            this._tcLabel.text = LanguageManager.GetWord("GameSettingView.EndCopy");
        }

        private void QuiteCopy(GameObject go)
        {
            this.CloseView();
            Time.timeScale = 1f;
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/GameSetting/GameSettingView.assetbundle";
            }
        }
    }
}

