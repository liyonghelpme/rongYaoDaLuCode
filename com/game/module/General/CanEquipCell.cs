﻿namespace com.game.module.General
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using System;

    public class CanEquipCell : ItemCell
    {
        private CellStates cellState;
        private SysItemsVo itemTemplate;
        private UISprite jiahao;
        private uint templateId;

        protected override void InitView()
        {
            base.InitView();
            this.jiahao = NGUITools.FindInChild<UISprite>(base.gameObject, "jiahao");
        }

        public CellStates CellState
        {
            get
            {
                return this.cellState;
            }
            set
            {
                this.cellState = value;
            }
        }

        protected override string iconAtlasPath
        {
            get
            {
                return "ItemIconAtlas";
            }
        }

        protected override string iconSpriteName
        {
            get
            {
                if (this.itemTemplate == null)
                {
                    return "100000";
                }
                return (string.Empty + this.itemTemplate.icon);
            }
        }

        public override SysItemsVo itemVo
        {
            get
            {
                return this.itemTemplate;
            }
        }

        public uint TemplateId
        {
            get
            {
                return this.templateId;
            }
            set
            {
                this.templateId = value;
                this.itemTemplate = BaseDataMgr.instance.getGoodsVo(this.templateId);
                if (this.cellState == CellStates.CanEquip)
                {
                    this.jiahao.SetActive(true);
                }
                else
                {
                    this.jiahao.SetActive(false);
                }
                base.UpdateCell();
            }
        }
    }
}

