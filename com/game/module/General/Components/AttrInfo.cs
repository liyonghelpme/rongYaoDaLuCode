﻿namespace com.game.module.General.Components
{
    using com.game.consts;
    using System;
    using System.Runtime.CompilerServices;

    public class AttrInfo
    {
        public GameConst.AttrType Type { get; set; }

        public uint Value { get; set; }
    }
}

