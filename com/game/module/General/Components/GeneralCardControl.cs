﻿namespace com.game.module.General.Components
{
    using com.game.consts;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.module.Manager;
    using com.game.Public.Confirm;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GeneralCardControl : UIWidgetContainer
    {
        private GeneralTemplateInfo _generalTemplateInfo;
        private UIAtlas _skillAtlas;
        [CompilerGenerated]
        private static Func<Transform, bool> <>f__am$cache1B;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map10;
        private UILabel attack_info;
        private UILabel base_info;
        private readonly Vector3 bigCollider = new Vector3(576f, 1024f, 1f);
        private GameObject buyInfoGameObject;
        private Button buyPoint;
        private GameObject card_info;
        private UITexture cardImg;
        private float currentRotation;
        private UILabel defense_info;
        private UILabel descLabel;
        private BoxCollider imgBoxCollider;
        private ImgMode imgMode = ImgMode.Small;
        private bool isLoadAsset;
        private GameObject pointInfoGameObject;
        private bool rotationing;
        private UILabel skill_count;
        private readonly List<SkillItemView> skillItems = new List<SkillItemView>();
        private readonly List<UISprite> skillListIcon = new List<UISprite>();
        private readonly Vector3 smallCollider = new Vector3(300f, 533f, 1f);
        private ViewState state = ViewState.Atlas;
        private GameObject state_img;
        private GameObject state_info;
        private GameObject state_skill;
        private vp_Timer.Handle timehandle = new vp_Timer.Handle();

        private void baseInfo(List<AttrInfo> infoList, string type)
        {
            IEnumerator<string> enumerator = (from info in infoList select this.getAttrInfo(info)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    string current = enumerator.Current;
                    string key = type;
                    if (key != null)
                    {
                        int num;
                        if (<>f__switch$map10 == null)
                        {
                            Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                            dictionary.Add("base", 0);
                            dictionary.Add("attck", 1);
                            dictionary.Add("defense", 2);
                            <>f__switch$map10 = dictionary;
                        }
                        if (<>f__switch$map10.TryGetValue(key, out num))
                        {
                            switch (num)
                            {
                                case 0:
                                    this.base_info.text = this.base_info.text + (!(this.base_info.text == string.Empty) ? ("\n" + current) : (current + string.Empty));
                                    break;

                                case 1:
                                    this.attack_info.text = this.attack_info.text + (!(this.attack_info.text == string.Empty) ? ("\n" + current) : current);
                                    break;

                                case 2:
                                    this.defense_info.text = this.defense_info.text + (!(this.defense_info.text == string.Empty) ? ("\n" + current) : current);
                                    break;
                            }
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void cardClickHandler(GameObject go)
        {
            if (((this.state == ViewState.Atlas) && !this.rotationing) && (this.cardImg.mainTexture != null))
            {
                float num;
                float num2;
                this.imgMode = (this.imgMode != ImgMode.Big) ? ImgMode.Big : ImgMode.Small;
                if (this.imgMode == ImgMode.Big)
                {
                    this.updateChildrenLayer(base.gameObject, LayerMask.NameToLayer("3DViewport"));
                    this.card_info.SetActive(false);
                }
                Vector3 scale = new Vector3(0f, 0f, 0f);
                if (this.imgMode == ImgMode.Big)
                {
                    num = 90f;
                    num2 = 0f;
                    scale.x = scale.y = scale.z = 2.18f;
                }
                else
                {
                    num = 0f;
                    num2 = -300f;
                    scale.x = scale.y = scale.z = 1f;
                }
                this.imgBoxCollider.size = (this.imgMode != ImgMode.Big) ? this.smallCollider : this.bigCollider;
                this.imgBoxCollider.enabled = false;
                TweenPosition.Begin(base.gameObject, 0.1f, new Vector3(num2, 0f, 0f));
                TweenScale.Begin(this.state_img, 0.1f, scale);
                object[] args = new object[] { "rotation", new Vector3(0f, 0f, num), "time", 0.3f, "oncomplete", "cardImgByBig_Small", "oncompletetarget", base.gameObject };
                iTween.RotateTo(base.gameObject, iTween.Hash(args));
            }
        }

        private void cardImgByBig_Small()
        {
            this.imgBoxCollider.enabled = true;
            if (this.imgMode == ImgMode.Big)
            {
                base.transform.localPosition = new Vector3(0f, 0f, 0f);
                base.transform.eulerAngles = new Vector3(0f, 0f, 90f);
            }
            else
            {
                base.transform.localPosition = new Vector3(-300f, 0f, 0f);
                base.transform.eulerAngles = new Vector3(0f, 0f, 0f);
                this.updateChildrenLayer(base.gameObject, LayerMask.NameToLayer("UI"));
                this.card_info.SetActive(true);
            }
        }

        public void clearDatas()
        {
            vp_Timer.CancelTimerByHandle(this.timehandle);
        }

        public void CleartHeroInfo(int type = 0)
        {
            this.generalTemplateInfo = null;
            this.base_info.text = string.Empty;
            this.attack_info.text = string.Empty;
            this.defense_info.text = string.Empty;
            this.cardImg.mainTexture = null;
            if (type < 0)
            {
                this.currentRotation = 0f;
                this.UpdateViewState(ViewState.None);
                this.updateSkillItems(false);
            }
            foreach (UISprite sprite in this.skillListIcon)
            {
                sprite.spriteName = string.Empty;
            }
        }

        private void ConfirmBuyTips()
        {
            Singleton<GeneralMode>.Instance.SendGeneralSkillTime(2);
        }

        private string getAttrInfo(AttrInfo info)
        {
            switch (info.Type)
            {
                case GameConst.AttrType.Percentage:
                    return ((info.Value / 100) + "%");

                case GameConst.AttrType.Integer:
                    return (info.Value + string.Empty);

                case GameConst.AttrType.Float:
                    return (Math.Round((double) (((double) info.Value) / 100.0), 2) + string.Empty);
            }
            return string.Empty;
        }

        public void Init()
        {
            this.skillItems.Add(new SkillItemView(base.FindChild("state_skill/skill_info/skill1")));
            this.skillItems.Add(new SkillItemView(base.FindChild("state_skill/skill_info/skill2")));
            this.skillItems.Add(new SkillItemView(base.FindChild("state_skill/skill_info/skill3")));
            this.skill_count = base.FindInChild<UILabel>("state_skill/infos/info_num");
            this.skillListIcon.Add(base.FindInChild<UISprite>("state_img/info/skill_list/skill1/icon"));
            this.skillListIcon.Add(base.FindInChild<UISprite>("state_img/info/skill_list/skill2/icon"));
            this.skillListIcon.Add(base.FindInChild<UISprite>("state_img/info/skill_list/skill3/icon"));
            this.pointInfoGameObject = base.FindChild("state_skill/infos");
            this.buyInfoGameObject = base.FindChild("state_skill/buy");
            this.buyPoint = base.FindInChild<Button>("state_skill/buy/add");
            this.descLabel = base.FindInChild<UILabel>("state_skill/buy/info_num");
            this.base_info = base.FindInChild<UILabel>("state_info/infos/message/base/num");
            this.attack_info = base.FindInChild<UILabel>("state_info/infos/message/attack/num");
            this.defense_info = base.FindInChild<UILabel>("state_info/infos/message/defense/num");
            this.cardImg = base.FindInChild<UITexture>("state_img/background/icon");
            this.card_info = base.FindChild("state_img/info");
            this.state_img = base.FindChild("state_img");
            this.state_info = base.FindChild("state_info");
            this.state_skill = base.FindChild("state_skill");
            this.base_info.text = string.Empty;
            this.attack_info.text = string.Empty;
            this.defense_info.text = string.Empty;
            base.onClick = new UIWidgetContainer.VoidDelegate(this.cardClickHandler);
            this.imgBoxCollider = base.gameObject.GetComponent<BoxCollider>();
            base.transform.localPosition = new Vector3(-300f, 0f, 0f);
            base.transform.eulerAngles = new Vector3(0f, 0f, 0f);
        }

        private void kapaiComplete(Texture2D textrue)
        {
            string cradTextrue = Singleton<GeneralInfoPanel>.Instance.generalTemplateInfo.Id.ToString();
            if (textrue.name != cradTextrue)
            {
                this.updateCardImg(cradTextrue);
            }
            else
            {
                this.isLoadAsset = false;
                this.cardImg.mainTexture = null;
                this.cardImg.mainTexture = textrue;
                this.cardImg.width = 0x240;
                this.cardImg.height = 0x400;
            }
        }

        private void LoadAtlas(UIAtlas atlas)
        {
            if (this._skillAtlas == null)
            {
                this._skillAtlas = atlas;
            }
            this.SetSkillIcon();
        }

        private void onBuyClickHandler(GameObject go)
        {
            string configDataString = Singleton<ConfigConst>.Instance.GetConfigDataString("BUY_SKILL_POINT_COST_DIAM");
            string[] param = new string[] { configDataString, "10" };
            ConfirmMgr.Instance.ShowOkCancelAlert(LanguageManager.GetWord("Game.General.BuyTips", param), "OK_CANCEL", new ClickCallback(this.ConfirmBuyTips));
        }

        public void SetSkillIcon()
        {
            List<uint> list = (this.generalTemplateInfo.generalInfo == null) ? this.generalTemplateInfo.skillList : this.generalTemplateInfo.generalInfo.SkillList;
            List<uint> list2 = new List<uint>();
            for (int i = 4; i < list.Count; i++)
            {
                list2.Add(list[i]);
            }
            for (int j = 0; j < this.skillListIcon.Count; j++)
            {
                this.skillListIcon[j].atlas = this._skillAtlas;
                if (list2[j] != 0)
                {
                    SysSkillBaseVo dataById = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>(list2[j]);
                    if (dataById != null)
                    {
                        this.skillListIcon[j].spriteName = dataById.icon.ToString(CultureInfo.InvariantCulture);
                    }
                }
                int num3 = 0x3e;
                this.skillListIcon[j].height = num3;
                this.skillListIcon[j].width = num3;
            }
        }

        private void startRotation(ViewState viewState)
        {
            this.state = viewState;
            this.imgBoxCollider.enabled = false;
            this.rotationing = true;
            object[] args = new object[] { "y", 90f, "time", 0.2f, "oncomplete", "TweenCallback", "oncompletetarget", base.gameObject, "oncompleteparams", false };
            iTween.RotateTo(base.gameObject, iTween.Hash(args));
        }

        private void TweenCallback(bool isOver)
        {
            if (!isOver)
            {
                this.UpdateViewState(this.state);
                object[] args = new object[] { "y", 0f, "time", 0.2f, "oncomplete", "TweenCallback", "oncompletetarget", base.gameObject, "oncompleteparams", true };
                iTween.RotateTo(base.gameObject, iTween.Hash(args));
            }
            else
            {
                this.imgBoxCollider.enabled = this.state == ViewState.Atlas;
                this.rotationing = false;
            }
        }

        public void updateCardImg(string cradTextrue)
        {
            this.isLoadAsset = true;
            AssetManager.Instance.LoadAsset<Texture2D>("Sources/Kapai/bigIcon/" + cradTextrue + ".assetbundle", new LoadAssetFinish<Texture2D>(this.kapaiComplete), null, false, true);
        }

        public void UpdateCardRotation(ViewState viewState)
        {
            if (!this.rotationing)
            {
                switch (viewState)
                {
                    case ViewState.Info:
                        if (this.state != ViewState.Info)
                        {
                            this.startRotation(viewState);
                            break;
                        }
                        return;

                    case ViewState.Atlas:
                        if (this.state != ViewState.Atlas)
                        {
                            this.startRotation(viewState);
                            break;
                        }
                        return;

                    case ViewState.Skill:
                        if (this.state != ViewState.Skill)
                        {
                            this.startRotation(viewState);
                            break;
                        }
                        return;
                }
            }
        }

        private void updateChildrenLayer(GameObject parent, int layer)
        {
            if (<>f__am$cache1B == null)
            {
                <>f__am$cache1B = children => children.gameObject != null;
            }
            IEnumerator<Transform> enumerator = parent.GetComponentsInChildren<Transform>(true).Where<Transform>(<>f__am$cache1B).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = enumerator.Current;
                    current.gameObject.layer = layer;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void UpdateDataInfo(PNewAttr attr)
        {
            this.base_info.text = string.Empty;
            this.attack_info.text = string.Empty;
            this.defense_info.text = string.Empty;
            List<AttrInfo> list4 = new List<AttrInfo>();
            AttrInfo item = new AttrInfo {
                Type = GameConst.AttrType.Integer,
                Value = attr.hpFull
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Integer,
                Value = attr.attP
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Integer,
                Value = attr.attM
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Integer,
                Value = attr.defP
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Integer,
                Value = attr.defM
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Integer,
                Value = attr.moveSpeed
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.moveSpeedAdd
            };
            list4.Add(item);
            List<AttrInfo> infoList = list4;
            list4 = new List<AttrInfo>();
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.attPPen
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.attMPen
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.attPVam
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.attMVam
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.crit
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.critAdd
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.atkCdDec
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.atkNormalAdd
            };
            list4.Add(item);
            List<AttrInfo> list2 = list4;
            list4 = new List<AttrInfo>();
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.defPPen
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.defMPen
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.dmgBackP
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.dmgBackM
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.flex
            };
            list4.Add(item);
            item = new AttrInfo {
                Type = GameConst.AttrType.Percentage,
                Value = attr.hpRe
            };
            list4.Add(item);
            List<AttrInfo> list3 = list4;
            this.baseInfo(infoList, "base");
            this.baseInfo(list2, "attck");
            this.baseInfo(list3, "defense");
        }

        public void updateSkillIcon()
        {
            if (this._skillAtlas == null)
            {
                Singleton<AtlasManager>.Instance.LoadAtlasHold("UI/Icon/SkillIcon/SkillIconHold.assetbundle", "SkillIcon", new LoadCallBack(this.LoadAtlas), true);
            }
            else
            {
                this.SetSkillIcon();
            }
        }

        public void updateSkillItems(bool isUpdate = true)
        {
            for (int i = 0; i < this.skillItems.Count; i++)
            {
                if (isUpdate)
                {
                    this.skillItems[i].UpdateInfos(this.generalTemplateInfo, i);
                }
                else
                {
                    this.skillItems[i].ClearSprites(false);
                }
            }
        }

        public void updateTime(byte point, uint time)
        {
            <updateTime>c__AnonStoreyD8 yd = new <updateTime>c__AnonStoreyD8 {
                time = time,
                point = point,
                <>f__this = this
            };
            this.buyInfoGameObject.SetActive(false);
            this.pointInfoGameObject.SetActive(false);
            if (yd.point <= 0)
            {
                this.buyInfoGameObject.SetActive(true);
                this.descLabel.text = "(" + TimeUtil.GetTimeHhmmss((int) yd.time) + ")" + LanguageManager.GetWord("Game.General.UpdateSkinPoint", "1");
                this.buyPoint.onClick = new UIWidgetContainer.VoidDelegate(this.onBuyClickHandler);
            }
            else
            {
                this.pointInfoGameObject.SetActive(true);
                object[] objArray1 = new object[] { yd.point, "(", TimeUtil.GetTimeHhmmss((int) yd.time), ")" };
                this.skill_count.text = string.Concat(objArray1);
            }
            if (yd.time > 0)
            {
                vp_Timer.CancelTimerByHandle(this.timehandle);
                vp_Timer.In(1f, new vp_Timer.Callback(yd.<>m__69), 0, 1f, this.timehandle);
            }
            else
            {
                this.skill_count.text = yd.point + string.Empty;
                vp_Timer.CancelTimerByHandle(this.timehandle);
            }
        }

        public void UpdateViewState(ViewState viewState)
        {
            this.state_info.SetActive(viewState == ViewState.Info);
            this.state_img.SetActive(viewState == ViewState.Atlas);
            this.state_skill.SetActive(viewState == ViewState.Skill);
        }

        public GeneralTemplateInfo generalTemplateInfo
        {
            get
            {
                return this._generalTemplateInfo;
            }
            set
            {
                this._generalTemplateInfo = value;
            }
        }

        public bool LoadAssetComplete
        {
            get
            {
                return this.isLoadAsset;
            }
        }

        public List<SkillItemView> SkillItemViewList
        {
            get
            {
                return this.skillItems;
            }
        }

        [CompilerGenerated]
        private sealed class <updateTime>c__AnonStoreyD8
        {
            internal GeneralCardControl <>f__this;
            internal byte point;
            internal uint time;

            internal void <>m__69()
            {
                if (this.time <= 0)
                {
                    vp_Timer.CancelTimerByHandle(this.<>f__this.timehandle);
                    Singleton<GeneralMode>.Instance.SendGeneralSkillTime(1);
                }
                else
                {
                    this.time--;
                    this.<>f__this.descLabel.text = "(" + TimeUtil.GetTimeHhmmss((int) this.time) + ")" + LanguageManager.GetWord("Game.General.UpdateSkinPoint", "1");
                    object[] objArray1 = new object[] { this.point, "(", TimeUtil.GetTimeHhmmss((int) this.time), ")" };
                    this.<>f__this.skill_count.text = string.Concat(objArray1);
                }
            }
        }

        public enum ImgMode
        {
            Big = 1,
            Small = 2
        }

        public enum ViewState
        {
            None,
            Info,
            Atlas,
            Skill
        }
    }
}

