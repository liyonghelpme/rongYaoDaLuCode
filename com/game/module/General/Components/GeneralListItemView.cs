﻿namespace com.game.module.General.Components
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.Public.Message;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GeneralListItemView : UIDragDropItem
    {
        private GeneralTemplateInfo _generalTemlateInfo;
        [CompilerGenerated]
        private static Func<UITexture, bool> <>f__am$cache1D;
        public BoxCollider boxCollider;
        public GeneralPanel.ViewMode currentMode;
        private Vector3 currentPosition;
        private Vector3 currentRotation;
        private Vector3 currentScale;
        private ItemState currentState;
        public bool IsDisplay = true;
        private UITexture kapai_icon;
        private UILabel kapai_lv;
        private UILabel kapai_name;
        private readonly List<UISprite> kapai_starListIcon = new List<UISprite>();
        private UISprite kapai_type;
        private GameObject KapaiItem;
        private GameObject kapaiSet;
        private UISprite list_icon;
        private UILabel list_lv;
        private UILabel list_name;
        private readonly List<UISprite> list_starListIcon = new List<UISprite>();
        public readonly IDictionary<int, int> listIndex = new Dictionary<int, int>();
        private GameObject ListItem;
        private GameObject ListSet;
        public readonly List<float> positionX = new List<float> { 430f, 405f, 365f, 320f, 240f, 130f, 0f, -130f, -240f, -320f, -365f, -405f, -430f };
        public readonly List<float> rotationY = new List<float> { 130f, 100f, 70f, 60f, 20f, 0f, 0f, 0f, -20f, -60f, -70f, -100f, -130f };
        public readonly List<Vector3> scaleList = new List<Vector3> { new Vector3(0.3f, 0.5f, 1f), new Vector3(0.3f, 0.5f, 1f), new Vector3(0.3f, 0.5f, 1f), new Vector3(0.3f, 0.6f, 1f), new Vector3(0.6f, 0.7f, 1f), new Vector3(0.8f, 0.8f, 1f), new Vector3(1f, 1f, 1f), new Vector3(0.8f, 0.8f, 1f), new Vector3(0.6f, 0.7f, 1f), new Vector3(0.3f, 0.6f, 1f), new Vector3(0.3f, 0.5f, 1f), new Vector3(0.3f, 0.5f, 1f), new Vector3(0.3f, 0.5f, 1f) };
        private readonly List<UISprite> skillListIcon = new List<UISprite>();
        private const float speed = 0.25f;
        private readonly IDictionary<string, List<UITexture>> spriteList = new Dictionary<string, List<UITexture>>();
        private readonly List<string> typeList = new List<string> { "jian", "dunpai", "fazhang", "shizijia" };
        private UIPanel uiPanel;

        public void changeRelease(GameObject surface)
        {
            this.OnPress(true);
            this.OnDragDropStart();
            this.OnDragDropRelease(surface);
            this.OnPress(false);
        }

        public GameObject FindChild(string name)
        {
            return NGUITools.FindChild(base.gameObject, name);
        }

        public T FindInChild<T>(string path) where T: Component
        {
            return NGUITools.FindInChild<T>(base.gameObject, path);
        }

        public int getIndexByDictionary(int key)
        {
            if (this.listIndex.ContainsKey(key))
            {
                return this.listIndex[key];
            }
            return -1;
        }

        private List<UITexture> getTexture(string id)
        {
            return (!this.spriteList.ContainsKey(id) ? null : this.spriteList[id]);
        }

        public void Init(GeneralPanel.ViewMode mode, GeneralTemplateInfo generalTemlateInfo)
        {
            this.Start();
            this._generalTemlateInfo = generalTemlateInfo;
            base.restriction = UIDragDropItem.Restriction.Vertical;
            this.currentMode = mode;
            this.KapaiItem = this.FindChild("Kapai");
            this.ListItem = this.FindChild("List");
            this.ListSet = this.FindChild("List/selected");
            this.kapaiSet = this.FindChild("Kapai/select");
            this.kapai_type = this.FindInChild<UISprite>("Kapai/type/sprite");
            this.kapai_icon = this.FindInChild<UITexture>("Kapai/icon");
            this.skillListIcon.Add(this.FindInChild<UISprite>("Kapai/skills/skill1"));
            this.skillListIcon.Add(this.FindInChild<UISprite>("Kapai/skills/skill2"));
            this.skillListIcon.Add(this.FindInChild<UISprite>("Kapai/skills/skill3"));
            this.kapai_name = this.FindInChild<UILabel>("Kapai/name");
            this.kapai_lv = this.FindInChild<UILabel>("Kapai/lv/num");
            this.list_name = this.FindInChild<UILabel>("List/nameLabel");
            this.list_name.SetActive(false);
            this.list_lv = this.FindInChild<UILabel>("List/levelLabel");
            this.list_icon = this.FindInChild<UISprite>("List/headIcon");
            this.kapai_starListIcon.Add(this.FindInChild<UISprite>("Kapai/starContainer/star1"));
            this.kapai_starListIcon.Add(this.FindInChild<UISprite>("Kapai/starContainer/star2"));
            this.kapai_starListIcon.Add(this.FindInChild<UISprite>("Kapai/starContainer/star3"));
            this.kapai_starListIcon.Add(this.FindInChild<UISprite>("Kapai/starContainer/star4"));
            this.kapai_starListIcon.Add(this.FindInChild<UISprite>("Kapai/starContainer/star5"));
            this.list_starListIcon.Add(this.FindInChild<UISprite>("List/starContainer/star1"));
            this.list_starListIcon.Add(this.FindInChild<UISprite>("List/starContainer/star2"));
            this.list_starListIcon.Add(this.FindInChild<UISprite>("List/starContainer/star3"));
            this.list_starListIcon.Add(this.FindInChild<UISprite>("List/starContainer/star4"));
            this.list_starListIcon.Add(this.FindInChild<UISprite>("List/starContainer/star5"));
            this.boxCollider = base.gameObject.GetComponent<BoxCollider>();
            this.KapaiItem.SetActive(this.currentMode == GeneralPanel.ViewMode.KapaiMode);
            this.ListItem.SetActive(this.currentMode == GeneralPanel.ViewMode.ListMode);
            this.InitKapai();
            this.InitList();
            this.UpdateItemEnabled();
        }

        private void InitKapai()
        {
            this.kapai_name.text = this.generalTemlateInfo.sysGeneralVo.name;
            this.SetSkillIcon();
            this.kapai_type.spriteName = this.typeList[this.generalTemlateInfo.sysGeneralVo.career - 1];
            if (this.spriteList.ContainsKey(this.generalTemlateInfo.sysGeneralVo.general_id.ToString()))
            {
                this.spriteList[this.generalTemlateInfo.sysGeneralVo.general_id.ToString()].Add(this.kapai_icon);
            }
            else
            {
                List<UITexture> list = new List<UITexture> {
                    this.kapai_icon
                };
                this.spriteList.Add(this.generalTemlateInfo.sysGeneralVo.general_id.ToString(), list);
            }
            if (this.generalInfo != null)
            {
                this.kapai_lv.text = this.generalInfo.lvl.ToString();
                this.showStar(this.generalInfo.star, this.kapai_starListIcon);
            }
            else
            {
                this.kapai_lv.text = 0 + string.Empty;
                this.showStar(0, this.kapai_starListIcon);
            }
            AssetManager.Instance.LoadAsset<Texture2D>("Sources/Kapai/smallIcon/" + this.generalTemlateInfo.sysGeneralVo.general_id + ".assetbundle", new LoadAssetFinish<Texture2D>(this.kapaiComplete), null, false, true);
        }

        private void InitList()
        {
            this.list_name.text = this.generalTemlateInfo.sysGeneralVo.name;
            this.list_icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this.list_icon.spriteName = this.generalTemlateInfo.sysGeneralVo.icon_id.ToString();
            if (this.generalInfo != null)
            {
                this.list_lv.text = this.generalInfo.lvl.ToString();
                this.showStar(this.generalInfo.star, this.list_starListIcon);
            }
            else
            {
                this.list_lv.text = 0 + string.Empty;
                this.showStar(0, this.list_starListIcon);
            }
        }

        private void kapaiComplete(Texture2D textrue)
        {
            Singleton<GeneralPanel>.Instance.InitComplete(this);
            if (textrue != null)
            {
                if (<>f__am$cache1D == null)
                {
                    <>f__am$cache1D = uiTextrue => uiTextrue != null;
                }
                IEnumerator<UITexture> enumerator = this.getTexture(textrue.name).Where<UITexture>(<>f__am$cache1D).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        UITexture current = enumerator.Current;
                        current.mainTexture = textrue;
                        current.width = 110;
                        current.height = 180;
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        private void MutualPosition(UIDragDropContainer dropContainer, Transform currentTransform)
        {
            if (dropContainer.transform.childCount >= 2)
            {
                GeneralListItemView component = dropContainer.transform.GetChild(0).GetComponent<GeneralListItemView>();
                if (component == null)
                {
                    Singleton<GeneralPanel>.Instance.UpdateBoxCollider(false);
                }
                else if (((currentTransform.name != "item1") && (currentTransform.name != "item2")) && (currentTransform.name != "item3"))
                {
                    component.changeRelease(Singleton<GeneralPanel>.Instance.getImageGroup());
                    Singleton<GeneralPanel>.Instance.UpdateBoxCollider(false);
                }
                else
                {
                    component.changeRelease(currentTransform.gameObject);
                }
            }
        }

        private void OnClick()
        {
            if (this.generalInfo != null)
            {
                Singleton<GeneralPanel>.Instance.UpdateSelected(this);
                if (Singleton<GeneralPanel>.Instance.guideSelect != null)
                {
                    Singleton<GeneralPanel>.Instance.guideSelect();
                    Singleton<GeneralPanel>.Instance.guideSelect = null;
                }
            }
            else
            {
                MessageManager.Show("您还未获得此英雄!");
            }
        }

        protected override void OnDragDropRelease(GameObject surface)
        {
            base.mTouchID = -2147483648;
            if (surface != null)
            {
                UIDragDropContainer component = surface.GetComponent<UIDragDropContainer>();
                if (component != null)
                {
                    if (component.reparentTarget != base.mTrans.parent)
                    {
                        if (((component.reparentTarget.name == "item1") || (component.reparentTarget.name == "item2")) || (component.reparentTarget.name == "item3"))
                        {
                            Transform parent = base.mTrans.parent;
                            int index = int.Parse(component.reparentTarget.name.Substring(component.reparentTarget.name.Length - 1));
                            if (((base.mTrans.parent.name != "item1") && (base.mTrans.parent.name != "item2")) && (base.mTrans.parent.name != "item3"))
                            {
                                this.currentState = ItemState.show;
                                base.gameObject.SetActive(true);
                                Singleton<GeneralPanel>.Instance.UpdateItemList(this, false, index, false);
                            }
                            else
                            {
                                this.currentState = ItemState.show;
                                base.gameObject.SetActive(true);
                                Singleton<GeneralPanel>.Instance.UpdateItemPlace(this, true, 0);
                                Singleton<GeneralPanel>.Instance.UpdateItemPlace(this, false, index);
                            }
                            base.mTrans.parent = component.reparentTarget;
                            base.mTrans.localScale = component.reparentTarget.localScale;
                            base.mTrans.localPosition = new Vector3(0f, 0f, 0f);
                            this.MutualPosition(component, parent);
                        }
                        else
                        {
                            if (Singleton<GeneralPanel>.Instance.getFightCount() <= 1)
                            {
                                MessageManager.Show("至少要一名出战英雄");
                                this.reductionItemState();
                                Singleton<GeneralPanel>.Instance.UpdateBoxCollider(false);
                                return;
                            }
                            Singleton<GeneralPanel>.Instance.UpdateItemList(this, true, 0, false);
                            if (component.reparentTarget.name == "ListMode")
                            {
                                this.currentState = ItemState.none;
                            }
                            base.mTrans.parent = component.reparentTarget;
                            base.mTrans.localScale = component.reparentTarget.localScale;
                        }
                    }
                    else
                    {
                        this.reductionItemState();
                    }
                }
                else
                {
                    this.reductionItemState();
                }
            }
            else
            {
                this.reductionItemState();
            }
            Singleton<GeneralPanel>.Instance.UpdateBoxCollider(false);
            if (base.mCollider != null)
            {
                base.mCollider.enabled = true;
            }
        }

        protected override void OnDragDropStart()
        {
            if (this.generalInfo != null)
            {
                Singleton<GeneralPanel>.Instance.UpdateBoxCollider(true);
                bool flag = this.currentState == ItemState.show;
                if (this.currentMode == GeneralPanel.ViewMode.KapaiMode)
                {
                    if (((this.currentState == ItemState.state5) || (this.currentState == ItemState.state6)) || (((this.currentState == ItemState.state7) || (this.currentState == ItemState.state8)) || (this.currentState == ItemState.state9)))
                    {
                        flag = true;
                    }
                }
                else
                {
                    Singleton<GeneralPanel>.Instance.changeModeByList();
                    flag = true;
                }
                if (flag)
                {
                    Singleton<GeneralPanel>.Instance.UpdateFingerGestures(false);
                    this.setShowMode(false);
                    base.mTrans.position = UICamera.lastHit.point;
                    base.OnDragDropStart();
                }
            }
        }

        private void OnPress(bool isPressed)
        {
            if (!isPressed || (this.generalInfo != null))
            {
                if (isPressed)
                {
                    base.mPressTime = RealTime.time;
                }
                if (isPressed)
                {
                    this.currentPosition = base.mTrans.position;
                    this.currentScale = base.mTrans.localScale;
                    this.currentRotation = base.mTrans.localEulerAngles;
                }
                else if ((base.mTrans.parent.name == "ListMode") || (base.mTrans.parent.name == "KapaiMode"))
                {
                    Singleton<GeneralPanel>.Instance.UpdateFingerGestures(true);
                }
                if (!isPressed)
                {
                    this.UpdateRestriction();
                    if (this.currentState == ItemState.show)
                    {
                        base.mTrans.localScale = this.scaleList[6];
                        base.mTrans.localEulerAngles = new Vector3(0f, this.rotationY[6], 0f);
                    }
                    if (base.gameObject.GetComponent<UIPanel>() != null)
                    {
                        base.gameObject.GetComponent<UIPanel>().depth = 100;
                    }
                    if ((this.currentMode == GeneralPanel.ViewMode.ListMode) && (this.currentState != ItemState.show))
                    {
                        this.updateByPanel(false);
                    }
                    Singleton<GeneralPanel>.Instance.UpdateBoxCollider(false);
                }
                else
                {
                    this.updateByPanel(true);
                }
            }
        }

        private void reductionItemState()
        {
            if (this.currentState == ItemState.show)
            {
                base.mTrans.localPosition = new Vector3(0f, 0f, 0f);
            }
            else
            {
                switch (this.currentMode)
                {
                    case GeneralPanel.ViewMode.KapaiMode:
                        this.UpdateItemState();
                        break;

                    case GeneralPanel.ViewMode.ListMode:
                        this.KapaiItem.SetActive(false);
                        this.ListItem.SetActive(true);
                        base.mTrans.position = this.currentPosition;
                        base.mTrans.localScale = this.currentScale;
                        base.mTrans.localEulerAngles = this.currentRotation;
                        break;
                }
            }
        }

        public void setIndexByDictionary(int key, int index)
        {
            this.listIndex.Add(key, index);
        }

        public void setShowMode(bool isInit = false)
        {
            this.updateByPanel(true);
            this.uiPanel.depth = 0x7d0;
            if (this.currentMode == GeneralPanel.ViewMode.ListMode)
            {
                this.ListItem.SetActive(false);
                this.KapaiItem.SetActive(true);
            }
            base.mTrans.localScale = this.scaleList[6];
            base.mTrans.localEulerAngles = new Vector3(0f, this.rotationY[6], 0f);
            if (isInit)
            {
                base.mTrans.localPosition = new Vector3(0f, 0f, 0f);
            }
        }

        public void SetSkillIcon()
        {
            List<uint> skillList = this.generalTemlateInfo.skillList;
            List<uint> list2 = new List<uint>();
            for (int i = 4; i < skillList.Count; i++)
            {
                list2.Add(skillList[i]);
            }
            for (int j = 0; j < this.skillListIcon.Count; j++)
            {
                this.skillListIcon[j].atlas = Singleton<AtlasManager>.Instance.GetAtlas("SkillIcon");
                if (list2[j] != 0)
                {
                    SysSkillBaseVo dataById = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>(list2[j]);
                    if (dataById != null)
                    {
                        this.skillListIcon[j].spriteName = dataById.icon.ToString(CultureInfo.InvariantCulture);
                    }
                }
                int num3 = 0x1c;
                this.skillListIcon[j].height = num3;
                this.skillListIcon[j].width = num3;
            }
        }

        public void setUIspriteState(bool state)
        {
            foreach (UISprite sprite in base.gameObject.transform.GetComponentsInChildren<UISprite>(true))
            {
                if (state && !sprite.isShowAsGray)
                {
                    sprite.ShowAsGray();
                }
                if (!state)
                {
                    sprite.ShowAsMyself();
                }
            }
        }

        private void showStar(int quality, List<UISprite> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].SetActive(false);
                list[i].SetActive(i < quality);
            }
        }

        protected override void Start()
        {
            base.mTrans = base.transform;
            base.mCollider = base.collider;
        }

        public void updateByPanel(bool isAdd)
        {
            if (isAdd)
            {
                if (this.uiPanel == null)
                {
                    this.uiPanel = base.gameObject.AddComponent<UIPanel>();
                }
            }
            else if (this.uiPanel != null)
            {
                base.gameObject.RemoveComponent<UIPanel>();
                this.uiPanel = null;
            }
        }

        private void updateDepth()
        {
            UIPanel component = base.gameObject.GetComponent<UIPanel>();
            if (component != null)
            {
                switch (this.CurrentState)
                {
                    case ItemState.state13:
                    case ItemState.state1:
                        component.depth = 8;
                        break;

                    case ItemState.state12:
                    case ItemState.state2:
                        component.depth = 10;
                        break;

                    case ItemState.state11:
                    case ItemState.state3:
                        component.depth = 12;
                        break;

                    case ItemState.state10:
                    case ItemState.state4:
                        component.depth = 14;
                        break;

                    case ItemState.state9:
                    case ItemState.state5:
                        component.depth = 0x10;
                        break;

                    case ItemState.state8:
                    case ItemState.state6:
                        component.depth = 0x12;
                        break;

                    case ItemState.state7:
                        component.depth = 20;
                        break;
                }
            }
        }

        public void UpdateItemEnabled()
        {
            if (this.generalInfo != null)
            {
                this.setUIspriteState(false);
                this.kapai_icon.shader = Shader.Find("Unlit/Transparent Colored");
                this.kapai_icon.color = Color.white;
            }
            else
            {
                this.setUIspriteState(true);
                this.kapai_icon.shader = Shader.Find("Ulint/Transparent Colored (Gray)");
                this.kapai_icon.color = Color.black;
            }
        }

        public void UpdateItemState()
        {
            TweenPosition.Begin(base.gameObject, 0.25f, new Vector3(this.positionX[(int) this.CurrentState], 0f, 0f));
            object[] args = new object[] { "rotation", new Vector3(0f, this.rotationY[(int) this.CurrentState], 0f), "time", 0.25f, "oncomplete", "updateDepth", "oncompletetarget", base.gameObject };
            iTween.RotateTo(base.gameObject, iTween.Hash(args));
            TweenScale.Begin(base.gameObject, 0.25f, this.scaleList[(int) this.CurrentState]);
        }

        public void UpdateMode(GeneralPanel.ViewMode mode, ItemState state)
        {
            this.currentMode = mode;
            this.KapaiItem.SetActive(false);
            this.ListItem.SetActive(false);
            switch (this.currentMode)
            {
                case GeneralPanel.ViewMode.KapaiMode:
                    this.updateByPanel(true);
                    this.KapaiItem.SetActive(true);
                    this.boxCollider.size = new Vector3(130f, 210f, 0f);
                    base.mTrans.localScale = this.scaleList[(int) state];
                    base.mTrans.localPosition = new Vector3(this.positionX[(int) state], 0f, 0f);
                    base.mTrans.localEulerAngles = new Vector3(0f, this.rotationY[(int) state], 0f);
                    break;

                case GeneralPanel.ViewMode.ListMode:
                    this.updateByPanel(false);
                    this.ListItem.SetActive(true);
                    this.boxCollider.size = new Vector3(110f, 155f, 0f);
                    base.mTrans.localScale = this.scaleList[6];
                    base.mTrans.localPosition = new Vector3(this.positionX[6], 0f, 0f);
                    base.mTrans.localEulerAngles = new Vector3(0f, this.rotationY[6], 0f);
                    break;
            }
        }

        private void UpdateRestriction()
        {
            if (((base.mTrans.parent.name == "item1") || (base.mTrans.parent.name == "item2")) || (base.mTrans.parent.name == "item3"))
            {
                base.restriction = UIDragDropItem.Restriction.None;
            }
            else
            {
                base.restriction = UIDragDropItem.Restriction.Vertical;
            }
        }

        public void updateSelected(bool isSet)
        {
            this.kapaiSet.SetActive(isSet);
            this.ListSet.SetActive(isSet);
        }

        public ItemState CurrentState
        {
            get
            {
                return this.currentState;
            }
            set
            {
                this.currentState = value;
            }
        }

        public GeneralInfo generalInfo
        {
            get
            {
                return this.generalTemlateInfo.generalInfo;
            }
        }

        public GeneralTemplateInfo generalTemlateInfo
        {
            get
            {
                return this._generalTemlateInfo;
            }
            set
            {
                this._generalTemlateInfo = value;
                this.InitKapai();
                this.InitList();
                this.UpdateItemEnabled();
            }
        }

        public enum ItemState
        {
            none = -1,
            show = -2,
            state1 = 12,
            state10 = 3,
            state11 = 2,
            state12 = 1,
            state13 = 0,
            state2 = 11,
            state3 = 10,
            state4 = 9,
            state5 = 8,
            state6 = 7,
            state7 = 6,
            state8 = 5,
            state9 = 4
        }
    }
}

