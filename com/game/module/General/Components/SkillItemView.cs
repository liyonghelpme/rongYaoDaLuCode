﻿namespace com.game.module.General.Components
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.General;
    using com.game.utils;
    using com.game.vo;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SkillItemView
    {
        [CompilerGenerated]
        private static Func<Transform, bool> <>f__am$cache15;
        private SysSkillBaseVo currentSkillBaseVo;
        private UISprite descBgSprite;
        private UILabel descLabel;
        private readonly GameObject gameObject;
        private uint groupId;
        private readonly float labelHeight = 24f;
        private UILabel lvLabel;
        private GameObject moneyGroup;
        private UILabel moneyLabel;
        private UILabel nameLabel;
        private SysSkillBaseVo nextSkillBaseVo;
        private UIEventListener skillIconContainer;
        private UISprite skillIconSprite;
        private uint skillId;
        private int skillIndex;
        private GameObject skillInfoDesc;
        private UILabel skillInfoLabel;
        private readonly float spriteHeight = 60f;
        private GeneralTemplateInfo templateInfo;
        private Button upgrade;
        private UISprite upgradeSprite;

        public SkillItemView(GameObject _gameObject)
        {
            this.gameObject = _gameObject;
            this.Init();
        }

        public void ClearSprites(bool state)
        {
            this.skillIconSprite.ShowAsMyself();
        }

        public GameObject FindChild(string name)
        {
            return NGUITools.FindChild(this.gameObject, name);
        }

        public T FindInChild<T>(string path) where T: Component
        {
            return NGUITools.FindInChild<T>(this.gameObject, path);
        }

        public void Init()
        {
            this.nameLabel = this.FindInChild<UILabel>("name");
            this.lvLabel = this.FindInChild<UILabel>("lv");
            this.moneyLabel = this.FindInChild<UILabel>("money/money_num");
            this.descLabel = this.FindInChild<UILabel>("desc");
            this.skillIconSprite = this.FindInChild<UISprite>("skill/icon");
            this.upgrade = this.FindInChild<Button>("upgrade");
            this.moneyGroup = this.FindChild("money");
            this.upgradeSprite = this.FindInChild<UISprite>("upgrade/icon");
            this.skillIconContainer = this.FindChild("skill").AddComponent<UIEventListener>();
            this.skillInfoDesc = this.FindChild("skill_info");
            this.descBgSprite = this.FindInChild<UISprite>("skill_info/bg");
            this.skillInfoLabel = this.FindInChild<UILabel>("skill_info/Label");
        }

        private void InitInfos()
        {
            string[] valueListFromString = StringUtils.GetValueListFromString(this.templateInfo.sysGeneralVo.skill_list, ',');
            List<uint> list = new List<uint>();
            for (int i = 4; i < 7; i++)
            {
                list.Add(uint.Parse(valueListFromString[i]));
            }
            this.groupId = list[this.skillIndex];
            this.skillIconSprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("SkillIcon");
            this.skillIconSprite.spriteName = this.groupId + string.Empty;
            if (this.templateInfo.generalInfo != null)
            {
                PGeneralSkillInfo generalSkillInfo = this.templateInfo.generalInfo.GetGeneralSkillInfo(this.groupId);
                if (generalSkillInfo != null)
                {
                    ushort skillLvl = generalSkillInfo.skillLvl;
                    if (skillLvl < 1)
                    {
                        this.skillIconSprite.ShowAsGray();
                    }
                    else
                    {
                        this.skillIconSprite.ShowAsMyself();
                    }
                    this.skillId = (this.groupId * 0x3e8) + skillLvl;
                    uint id = ((this.groupId * 0x3e8) + skillLvl) + 1;
                    this.nextSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(id);
                    this.currentSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(this.skillId);
                    if (this.currentSkillBaseVo != null)
                    {
                        this.nameLabel.text = this.currentSkillBaseVo.name;
                        if (this.currentSkillBaseVo.lvl <= this.templateInfo.generalInfo.lvl)
                        {
                            this.lvLabel.SetActive(skillLvl >= 1);
                            if (this.lvLabel.gameObject.activeSelf)
                            {
                                this.lvLabel.text = "Lv. " + skillLvl + string.Empty;
                            }
                            if (this.nextSkillBaseVo != null)
                            {
                                if (this.nextSkillBaseVo.lvl <= this.templateInfo.generalInfo.lvl)
                                {
                                    this.moneyGroup.SetActive(true);
                                    this.upgrade.SetActive(true);
                                    this.descLabel.SetActive(false);
                                    this.moneyLabel.text = this.nextSkillBaseVo.beiley + string.Empty;
                                    this.moneyLabel.color = (MeVo.instance.diam >= this.nextSkillBaseVo.beiley) ? Color.white : Color.red;
                                }
                                else
                                {
                                    this.upgrade.SetActive(false);
                                    this.moneyGroup.SetActive(false);
                                    this.descLabel.SetActive(true);
                                    this.descLabel.text = LanguageManager.GetWord("Game.General.SkillNextLevel", this.nextSkillBaseVo.lvl + string.Empty);
                                    this.descLabel.color = Color.red;
                                }
                            }
                            else
                            {
                                this.moneyGroup.SetActive(false);
                                this.upgrade.SetActive(false);
                                this.descLabel.SetActive(true);
                                this.descLabel.text = LanguageManager.GetWord("Game.General.SkillMaxLevel");
                                this.descLabel.color = Color.red;
                            }
                        }
                        else
                        {
                            this.moneyGroup.SetActive(false);
                            this.lvLabel.SetActive(false);
                            this.upgrade.SetActive(false);
                            this.descLabel.SetActive(true);
                            this.descLabel.text = LanguageManager.GetWord("Game.General.SkillOpenLevel", this.currentSkillBaseVo.lvl + string.Empty);
                            this.descLabel.color = Color.red;
                        }
                    }
                }
            }
        }

        private void onClickHandler(GameObject go)
        {
            if (Singleton<GeneralInfoPanel>.Instance.guideSkillUpgradeOver != null)
            {
                Singleton<GeneralInfoPanel>.Instance.guideSkillUpgradeOver();
                Singleton<GeneralInfoPanel>.Instance.guideSkillUpgradeOver = null;
            }
            if ((this.templateInfo != null) && (this.templateInfo.generalInfo != null))
            {
                Singleton<GeneralMode>.Instance.SendGeneralSkillUpgrade(this.templateInfo.generalInfo.id, this.groupId);
            }
        }

        private void SkillIconPressHandler(GameObject go, bool state)
        {
            if (state)
            {
                PNewAttr generalAttr = Singleton<GeneralMode>.Instance.selectedGeneralInfo.GeneralAttr;
                if (generalAttr == null)
                {
                    return;
                }
                this.UpdateSkillDesc(this.currentSkillBaseVo, generalAttr);
            }
            this.updateChildrenLayer(this.skillInfoDesc, LayerMask.NameToLayer(!state ? "UI" : "3DViewport"));
            this.skillInfoDesc.SetActive(state);
        }

        private void SkillInfoLabelChange()
        {
            float num = (((float) this.skillInfoLabel.height) / this.labelHeight) - 1f;
            this.descBgSprite.height = (int) (this.spriteHeight + (num * this.labelHeight));
        }

        private void updateChildrenLayer(GameObject parent, int layer)
        {
            if (<>f__am$cache15 == null)
            {
                <>f__am$cache15 = children => children.gameObject != null;
            }
            IEnumerator<Transform> enumerator = parent.GetComponentsInChildren<Transform>(true).Where<Transform>(<>f__am$cache15).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = enumerator.Current;
                    current.gameObject.layer = layer;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void UpdateInfos(GeneralTemplateInfo generalTemplateInfo, int index)
        {
            this.skillInfoLabel.onChange = new UIWidget.OnDimensionsChanged(this.SkillInfoLabelChange);
            this.upgrade.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            if (this.skillIconContainer != null)
            {
                this.skillIconContainer.onPress = new UIEventListener.BoolDelegate(this.SkillIconPressHandler);
            }
            this.templateInfo = generalTemplateInfo;
            this.skillIndex = index;
            this.InitInfos();
        }

        private void UpdateSkillDesc(SysSkillBaseVo baseVo, PNewAttr attr)
        {
            if (attr != null)
            {
                SysSkillLevelVo sysSkillLevelVo = BaseDataMgr.instance.GetSysSkillLevelVo((uint) baseVo.id);
                if (sysSkillLevelVo != null)
                {
                    uint num;
                    GameFormula.CalcDamage(baseVo, (baseVo.att_type != 1) ? ((float) attr.attM) : ((float) attr.attP), out num);
                    object[] objArray1 = new object[] { baseVo.name, "\n冷却时间: ", baseVo.cd / 0x3e8, " s\n", baseVo.desc };
                    string word = string.Concat(objArray1);
                    string[] valueListFromString = StringUtils.GetValueListFromString(("[" + num + ",") + sysSkillLevelVo.desc.Replace("[", string.Empty), ',');
                    this.skillInfoLabel.text = LanguageManager.GetStringToWord(word, valueListFromString);
                }
            }
        }

        public Button UpgradeBtn
        {
            get
            {
                return this.upgrade;
            }
        }
    }
}

