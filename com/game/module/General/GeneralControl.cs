﻿namespace com.game.module.General
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;
    using UnityEngine;
    using UnityLog;

    public class GeneralControl : BaseControl<GeneralControl>
    {
        private void Fun_2_1(INetData data)
        {
            GeneralGetListMsg_2_1 g__ = new GeneralGetListMsg_2_1();
            g__.read(data.GetMemoryStream());
            Singleton<GeneralMode>.Instance.UpdateGeneralInfo(g__.listInfo);
        }

        private void Fun_2_2(INetData data)
        {
            GeneralSetFightListMsg_2_2 g__ = new GeneralSetFightListMsg_2_2();
            g__.read(data.GetMemoryStream());
            Singleton<GeneralMode>.Instance.UpdateFightGeneralListInfo(g__.setList, g__.type);
        }

        private void Fun_2_3(INetData data)
        {
            Log.AI(null, " Anshu 2_3 切换主角 ");
            GeneralSetFightMsg_2_3 g__ = new GeneralSetFightMsg_2_3();
            g__.read(data.GetMemoryStream());
            if ((g__.code != 0) && (g__.code != 1))
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                Singleton<GeneralMode>.Instance.UpdateFightGeneralsAttrlistinfo(g__.generalAttr, g__.code, g__.dupGeneralId, g__.fightGeneralHp);
            }
        }

        private void Fun_2_4(INetData data)
        {
            GeneralGetGeneralAttrMsg_2_4 g__ = new GeneralGetGeneralAttrMsg_2_4();
            g__.read(data.GetMemoryStream());
            Singleton<GeneralMode>.Instance.GeneralInfoByAttack(g__.attr);
        }

        private void Fun_2_5(INetData data)
        {
            GeneralStarMsg_2_5 g__ = new GeneralStarMsg_2_5();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<GeneralMode>.Instance.UpdateGeneralStar(g__.generalId, g__.star);
            }
        }

        private void Fun_2_6(INetData data)
        {
            GeneralGeneralLvlMsg_2_6 generalLvl = new GeneralGeneralLvlMsg_2_6();
            generalLvl.read(data.GetMemoryStream());
            if (generalLvl.code == 0)
            {
                Singleton<GeneralMode>.Instance.UpdateGeneralLv(generalLvl);
            }
        }

        private void Fun_2_7(INetData data)
        {
            GeneralOtherRoleGeneralInfoMsg_2_7 g__ = new GeneralOtherRoleGeneralInfoMsg_2_7();
            g__.read(data.GetMemoryStream());
            switch (g__.code)
            {
                case 0:
                    Singleton<GeneralMode>.Instance.UpdateOtherInfo(g__.generalInfoList);
                    break;

                case 0xcd:
                    ErrorCodeManager.ShowError(g__.code);
                    break;
            }
        }

        private void Fun_2_9(INetData data)
        {
            Log.AI(null, "ReceiveMsg 2_9 WifiInitData");
            GeneralFightAttrWifiMsg_2_9 msg = new GeneralFightAttrWifiMsg_2_9();
            msg.read(data.GetMemoryStream());
            if ((msg.code != 0) && (msg.code != 1))
            {
                Debug.LogError("Wifi_2_9 Msg Error");
                ErrorCodeManager.ShowError(msg.code);
            }
            else if (AppMap.Instance.IsInWifiPVP)
            {
                Singleton<WifiPvpMode>.Instance.SetHeroInfo(msg);
                Singleton<GeneralMode>.Instance.UpdateWifiPVPMyHeroInformation(msg);
            }
            else
            {
                Debug.LogError("Not In Wifi GameMode Why Receive Such Msg_2_9");
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("513", new NetMsgCallback(this.Fun_2_1));
            AppNet.main.addCMD("514", new NetMsgCallback(this.Fun_2_2));
            AppNet.main.addCMD("515", new NetMsgCallback(this.Fun_2_3));
            AppNet.main.addCMD("516", new NetMsgCallback(this.Fun_2_4));
            AppNet.main.addCMD("517", new NetMsgCallback(this.Fun_2_5));
            AppNet.main.addCMD("518", new NetMsgCallback(this.Fun_2_6));
            AppNet.main.addCMD("519", new NetMsgCallback(this.Fun_2_7));
            AppNet.main.addCMD("521", new NetMsgCallback(this.Fun_2_9));
        }
    }
}

