﻿namespace com.game.module.General
{
    using com.game.data;
    using com.game.manager;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class GeneralFightInfo : IComparable
    {
        [CompilerGenerated]
        private static Func<PGeneralSkillInfo, SysSkillBaseVo> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<SysSkillBaseVo, uint> <>f__am$cache8;
        public PNewAttr attr;
        public uint battlePlace;
        public PNewAttr EquipAttr;
        public SysGeneralVo genralTemplateInfo;
        private List<PGeneralSkillInfo> mSkillInfoList;
        public byte place;
        public List<uint> SkillList;

        public int CompareTo(object obj)
        {
            if (!(obj is GeneralFightInfo))
            {
                throw new NotImplementedException("obj is not GeneralInfo");
            }
            GeneralFightInfo info = obj as GeneralFightInfo;
            return this.place.CompareTo(info.place);
        }

        private List<uint> GetSkillId(List<PGeneralSkillInfo> skillInfoList)
        {
            if (<>f__am$cache7 == null)
            {
                <>f__am$cache7 = info => BaseDataMgr.instance.GetSysSkillBaseVo((info.groupId * 0x3e8) + info.skillLvl);
            }
            if (<>f__am$cache8 == null)
            {
                <>f__am$cache8 = baseVo => (baseVo == null) ? 0 : ((baseVo.skill_lvl <= 0) ? 0 : ((uint) baseVo.id));
            }
            return skillInfoList.Select<PGeneralSkillInfo, SysSkillBaseVo>(<>f__am$cache7).Select<SysSkillBaseVo, uint>(<>f__am$cache8).ToList<uint>();
        }

        public List<PGeneralSkillInfo> skillInfoList
        {
            get
            {
                return this.mSkillInfoList;
            }
            set
            {
                this.mSkillInfoList = value;
                this.SkillList = this.GetSkillId(value);
            }
        }
    }
}

