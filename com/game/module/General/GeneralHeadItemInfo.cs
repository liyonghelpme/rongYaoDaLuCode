﻿namespace com.game.module.General
{
    using System;
    using UnityEngine;

    internal class GeneralHeadItemInfo : MonoBehaviour
    {
        public int general_id;
        public int icon_id;
        public ulong id;
        public byte place;
    }
}

