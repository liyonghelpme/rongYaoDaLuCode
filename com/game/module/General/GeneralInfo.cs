﻿namespace com.game.module.General
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Role;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GeneralInfo : IComparable
    {
        [CompilerGenerated]
        private static Func<SysSkillBaseVo, uint> <>f__am$cacheD;
        public Dictionary<string, string> defaultEquipIdDic = new Dictionary<string, string>();
        public ulong exp;
        public uint fightPoint;
        public uint generalId;
        public SysGeneralVo genralTemplateInfo;
        public ulong id;
        public ushort lvl;
        private List<PGeneralSkillInfo> mSkillInfoList;
        public byte place;
        public byte quality;
        public List<uint> SkillList;
        public byte star;
        public Dictionary<int, PItems> wornEquipItemDic = new Dictionary<int, PItems>();

        public GeneralInfo(bool isSelf = true)
        {
            if (isSelf)
            {
                this.InitEvent();
            }
        }

        public int CompareTo(object obj)
        {
            if (!(obj is GeneralInfo))
            {
                throw new NotImplementedException("obj is not GeneralInfo");
            }
            GeneralInfo info = obj as GeneralInfo;
            return this.place.CompareTo(info.place);
        }

        private void EquipInfoUpdateHandler(object sender, int code)
        {
            if (code == BagMode.UPDATE_EQUIP)
            {
                foreach (PItems items in Singleton<BagMode>.Instance.EquipDict.Values)
                {
                    if (items.generalId == this.id)
                    {
                        SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(items.templateId);
                        if (this.wornEquipItemDic.ContainsKey(vo.subtype))
                        {
                            this.wornEquipItemDic.Remove(vo.subtype);
                        }
                        this.wornEquipItemDic.Add(vo.subtype, items);
                    }
                }
            }
        }

        public PGeneralSkillInfo GetGeneralSkillInfo(uint groupId)
        {
            <GetGeneralSkillInfo>c__AnonStoreyDA yda = new <GetGeneralSkillInfo>c__AnonStoreyDA {
                groupId = groupId
            };
            return this.skillInfoList.FirstOrDefault<PGeneralSkillInfo>(new Func<PGeneralSkillInfo, bool>(yda.<>m__73));
        }

        private List<uint> GetSkillId(List<PGeneralSkillInfo> skillInfoList)
        {
            return (from groupSkillInfo in skillInfoList select this.GetSkillIdByGroupAndLevel(groupSkillInfo.groupId, groupSkillInfo.skillLvl)).ToList<uint>();
        }

        private uint GetSkillIdByGroupAndLevel(uint groupId, int level)
        {
            <GetSkillIdByGroupAndLevel>c__AnonStoreyD9 yd = new <GetSkillIdByGroupAndLevel>c__AnonStoreyD9 {
                groupId = groupId,
                level = level
            };
            if (<>f__am$cacheD == null)
            {
                <>f__am$cacheD = skillvo => (uint) skillvo.id;
            }
            return BaseDataMgr.instance.GetDicByType<SysSkillBaseVo>().Values.Cast<SysSkillBaseVo>().Where<SysSkillBaseVo>(new Func<SysSkillBaseVo, bool>(yd.<>m__71)).Select<SysSkillBaseVo, uint>(<>f__am$cacheD).FirstOrDefault<uint>();
        }

        private void InitEvent()
        {
            Singleton<BagMode>.Instance.dataUpdated += new DataUpateHandler(this.EquipInfoUpdateHandler);
        }

        public void SetDefaultEquipIdDic(string idStr)
        {
            idStr = idStr.Replace("[[", string.Empty);
            idStr = idStr.Replace("]]", string.Empty);
            idStr = idStr.Replace("],[", "|");
            char[] separator = new char[] { '|' };
            string[] strArray = idStr.Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] chArray2 = new char[] { ',' };
                string[] strArray2 = strArray[i].Split(chArray2);
                string str = strArray2[0];
                string str2 = strArray2[1];
                this.defaultEquipIdDic[str] = str2;
            }
        }

        public List<PGeneralSkillInfo> skillInfoList
        {
            get
            {
                return this.mSkillInfoList;
            }
            set
            {
                this.mSkillInfoList = value;
                this.SkillList = this.GetSkillId(value);
            }
        }

        [CompilerGenerated]
        private sealed class <GetGeneralSkillInfo>c__AnonStoreyDA
        {
            internal uint groupId;

            internal bool <>m__73(PGeneralSkillInfo info)
            {
                return (info.groupId == this.groupId);
            }
        }

        [CompilerGenerated]
        private sealed class <GetSkillIdByGroupAndLevel>c__AnonStoreyD9
        {
            internal uint groupId;
            internal int level;

            internal bool <>m__71(SysSkillBaseVo skillvo)
            {
                return ((skillvo.skill_group == this.groupId) && (skillvo.skill_lvl == this.level));
            }
        }
    }
}

