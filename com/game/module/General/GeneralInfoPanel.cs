﻿namespace com.game.module.General
{
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.components;
    using com.game.module.General.Components;
    using com.game.module.Role;
    using com.game.Public.Message;
    using com.game.utils;
    using com.u3d.bases.consts;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class GeneralInfoPanel : BaseView<GeneralInfoPanel>
    {
        [CompilerGenerated]
        private static Func<Transform, bool> <>f__am$cache2A;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map11;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map12;
        private Button add_atlas;
        private Animator animator;
        private Button atlas;
        private UIEventListener atlasIconContainer;
        private vp_Timer.Handle attackHandler = new vp_Timer.Handle();
        private GeneralCardControl cardControl;
        private GameObject cardImgObject;
        private UIEventListener cardWidgetContainer;
        private Button close;
        private GameObject dragItem;
        private readonly List<CanEquipCell> equipList = new List<CanEquipCell>();
        private UILabel exp;
        private UIEventListener expCollider;
        private UILabel expLabel;
        private UISlider expSlider;
        private UILabel fight;
        private UISprite generalCareer;
        public GeneralTemplateInfo generalTemplateInfo;
        public GuideEquip guideEquip;
        public GuideSkillUpgrade guideSkillUpgrade;
        public GuideSkillUpgradeOver guideSkillUpgradeOver;
        public GuideUpgradeStar guideUpgradeStar;
        private GameObject hero;
        private UISprite heroAtlasSprite;
        private GameObject heroModel;
        private UIEventListener heroWidgetContainer;
        private Button info;
        private Button left_btn;
        private UILabel lv;
        private ModelState modelState;
        private UILabel name;
        private Button next;
        private Button prev;
        private Button right_btn;
        private bool rotateing;
        private Button skill_upgrade;
        private readonly List<GameObject> starGameObjects = new List<GameObject>();
        private readonly List<int> StatusList = new List<int> { 0x270f, Status.NAME_HASH_SKILL1, Status.NAME_HASH_SKILL2, Status.NAME_HASH_SKILL2, Status.NAME_HASH_WIN };
        private const int ThrreAttack = 0x270f;
        private readonly List<string> typeList = new List<string> { "jian", "dunpai", "fazhang", "shizijia" };
        private Button upgrade;

        private void CancelFingerGestures()
        {
            FingerGestures.OnFingerDown -= new FingerGestures.FingerDownEventHandler(this.OnFingerDown);
            FingerGestures.OnFingerDragMove -= new FingerGestures.FingerDragMoveEventHandler(this.OnFingerDragMove);
        }

        public override void CancelUpdateHandler()
        {
            Singleton<GeneralMode>.Instance.dataUpdatedWithParam -= new DataUpateHandlerWithParam(this.updatedWithParam);
        }

        private void ChangeHeroInfo(int type = 10)
        {
            Singleton<GeneralMode>.Instance.SendGeneralSkillTime(1);
            this.CleartHeroInfo(0);
            if (type == -1)
            {
                this.cardControl.UpdateCardRotation(GeneralCardControl.ViewState.Atlas);
            }
            this.cardControl.CleartHeroInfo(0);
            this.generalTemplateInfo = Singleton<GeneralMode>.Instance.selectedGeneralInfo;
            if (base.LastOpenedPanelType == PanelType.GENERAL_PANEL)
            {
                Singleton<GeneralMode>.Instance.SendGeneralAttak(this.generalTemplateInfo.generalInfo.generalId);
            }
            this.cardControl.generalTemplateInfo = this.generalTemplateInfo;
            this.cardControl.updateSkillIcon();
            this.cardControl.updateCardImg(this.generalTemplateInfo.Id + string.Empty);
            this.cardControl.updateSkillItems(true);
            this.updateHeroModel();
            this.InitEquipInfo();
            this.InitHeroInfo();
            this.UpdateStarSprites();
            this.RegisterFingerGestures();
            if (base.LastOpenedPanelType == PanelType.CORPS_PANEL)
            {
                PNewAttr generalAttrInfo = Singleton<GeneralMode>.Instance.GetGeneralAttrInfo(this.generalTemplateInfo.Id);
                if (generalAttrInfo != null)
                {
                    this.fight.text = generalAttrInfo.fightPoint + string.Empty;
                    this.cardControl.UpdateDataInfo(generalAttrInfo);
                }
            }
        }

        private void ChangeInfoByButton(GameObject go)
        {
            if (!this.cardControl.LoadAssetComplete)
            {
                List<GeneralTemplateInfo> list = Singleton<GeneralMode>.Instance.CurrentTemplateInfoList(base.LastOpenedPanelType == PanelType.GENERAL_PANEL);
                if (list.Count > 1)
                {
                    Singleton<GeneralMode>.Instance.ChangeSelectedGeneralInfo(!(go.name == "next") ? -1 : 1, list);
                    this.ChangeHeroInfo(10);
                }
            }
        }

        private void CleartHeroInfo(int type = 0)
        {
            this.generalTemplateInfo = null;
            this.cardControl.clearDatas();
            this.cardControl.CleartHeroInfo(type);
            if (this.heroModel != null)
            {
                UnityEngine.Object.Destroy(this.heroModel);
            }
            this.CancelFingerGestures();
        }

        private void ClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map12 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(8);
                    dictionary.Add("close", 0);
                    dictionary.Add("info", 1);
                    dictionary.Add("atlas", 2);
                    dictionary.Add("skill_upgrade", 3);
                    dictionary.Add("add_atlas", 4);
                    dictionary.Add("expCollider", 4);
                    dictionary.Add("icon", 4);
                    dictionary.Add("upgrade", 5);
                    <>f__switch$map12 = dictionary;
                }
                if (<>f__switch$map12.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            base.ReturnToToggerPanel();
                            this.CloseView();
                            break;

                        case 1:
                            this.cardControl.UpdateCardRotation(GeneralCardControl.ViewState.Info);
                            break;

                        case 2:
                            this.cardControl.UpdateCardRotation(GeneralCardControl.ViewState.Atlas);
                            break;

                        case 3:
                            if (this.guideSkillUpgrade != null)
                            {
                                this.guideSkillUpgrade();
                                this.guideSkillUpgrade = null;
                            }
                            this.cardControl.UpdateCardRotation(GeneralCardControl.ViewState.Skill);
                            break;

                        case 4:
                        {
                            SysGeneralStarVo generalStarVo = BaseDataMgr.instance.GetGeneralStarVo(this.generalTemplateInfo.generalInfo.generalId, this.generalTemplateInfo.generalInfo.star + 1);
                            if (generalStarVo != null)
                            {
                                SysItemsVo template = BaseDataMgr.instance.getGoodsVo((uint) generalStarVo.item_id);
                                Singleton<GetItemTipsPanel>.Instance.SetInfo(template, PanelType.GENERAL_INFO_PANEL);
                                break;
                            }
                            MessageManager.Show("已进化至顶级!");
                            return;
                        }
                        case 5:
                            Singleton<GeneralMode>.Instance.SendGeneralUpgradeStar((ulong) this.generalTemplateInfo.generalInfo.generalId);
                            break;
                    }
                }
            }
        }

        public Button CloseBtn()
        {
            return this.close;
        }

        private void EquipCellClickHandler(GameObject go)
        {
            CanEquipCell component = go.GetComponent<CanEquipCell>();
            int equipType = this.equipList.IndexOf(component) + 1;
            if (this.generalTemplateInfo.generalInfo.defaultEquipIdDic.ContainsKey(equipType.ToString()))
            {
                uint id = uint.Parse(this.generalTemplateInfo.generalInfo.defaultEquipIdDic[equipType.ToString()]);
                SysItemsVo template = BaseDataMgr.instance.getGoodsVo(id);
                switch (component.CellState)
                {
                    case com.game.module.General.CellStates.NoEquip:
                        if (base.LastOpenedPanelType == PanelType.GENERAL_PANEL)
                        {
                            Singleton<GetItemTipsPanel>.Instance.SetInfo(template, PanelType.GENERAL_INFO_PANEL);
                            Singleton<AddEquipMentPanel>.Instance.CloseView();
                        }
                        break;

                    case com.game.module.General.CellStates.CanEquip:
                        Singleton<AddEquipMentPanel>.Instance.SetInfo(template, equipType, CurrentOperate.EQUIP, base.LastOpenedPanelType);
                        Singleton<GetItemTipsPanel>.Instance.CloseView();
                        break;

                    case com.game.module.General.CellStates.HadEquip:
                        Singleton<AddEquipMentPanel>.Instance.SetInfo(component.itemVo, equipType, CurrentOperate.FORGE, base.LastOpenedPanelType);
                        Singleton<GetItemTipsPanel>.Instance.CloseView();
                        break;
                }
            }
        }

        public List<CanEquipCell> GetEquipCell()
        {
            return this.equipList;
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdatePanelType();
            vp_Timer.In(0f, new vp_Timer.Callback(this.UpdateRotation), 0, null);
            this.cardControl.UpdateViewState(GeneralCardControl.ViewState.Atlas);
            this.ChangeHeroInfo(-1);
        }

        protected override void HandleBeforeCloseView()
        {
            vp_Timer.CancelAll("UpdateRotation");
            this.CleartHeroInfo(-1);
        }

        private void HeroClickHandler(GameObject go)
        {
            if (this.heroModel != null)
            {
                this.animator = this.heroModel.GetComponent<Animator>();
                if (this.animator.GetCurrentAnimatorStateInfo(0).nameHash == Status.NAME_HASH_IDLE)
                {
                    int num = UnityEngine.Random.Range(0, this.StatusList.Count - 1);
                    if (this.StatusList[num] == 0x270f)
                    {
                        vp_Timer.CancelTimerByHandle(this.attackHandler);
                        this.animator.Play(Status.NAME_HASH_ATTACK1);
                        vp_Timer.In(0.001f, new vp_Timer.Callback(this.UpdateThrreAttack), 0, this.attackHandler);
                    }
                    else
                    {
                        this.animator.Play(this.StatusList[num]);
                    }
                }
            }
        }

        protected override void Init()
        {
            this.cardImgObject = base.FindChild("card");
            this.close = base.FindInChild<Button>("buttons/close");
            this.next = base.FindInChild<Button>("buttons/next");
            this.prev = base.FindInChild<Button>("buttons/prev");
            this.upgrade = base.FindInChild<Button>("infos/buttons/upgrade");
            this.info = base.FindInChild<Button>("infos/buttons/info");
            this.generalCareer = base.FindInChild<UISprite>("infos/hero/type/icon");
            this.atlas = base.FindInChild<Button>("infos/buttons/atlas");
            this.skill_upgrade = base.FindInChild<Button>("infos/buttons/skill_upgrade");
            this.add_atlas = base.FindInChild<Button>("infos/atllas_exp/add_atlas");
            this.lv = base.FindInChild<UILabel>("infos/equip_info/infos_label/lv");
            this.fight = base.FindInChild<UILabel>("infos/equip_info/infos_label/fight");
            this.exp = base.FindInChild<UILabel>("infos/equip_info/infos_label/exp");
            this.hero = base.FindChild("infos/hero");
            this.expSlider = base.FindInChild<UISlider>("infos/atllas_exp/expbar");
            this.expLabel = base.FindInChild<UILabel>("infos/atllas_exp/expbar/num");
            this.heroAtlasSprite = base.FindInChild<UISprite>("infos/atllas_exp/icon");
            this.starGameObjects.Add(base.FindChild("infos/hero/star_list/star1"));
            this.starGameObjects.Add(base.FindChild("infos/hero/star_list/star2"));
            this.starGameObjects.Add(base.FindChild("infos/hero/star_list/star3"));
            this.starGameObjects.Add(base.FindChild("infos/hero/star_list/star4"));
            this.starGameObjects.Add(base.FindChild("infos/hero/star_list/star5"));
            this.name = base.FindInChild<UILabel>("infos/hero/name");
            this.left_btn = base.FindInChild<Button>("infos/hero_option/left");
            this.right_btn = base.FindInChild<Button>("infos/hero_option/right");
            this.equipList.Add(base.FindChild("infos/equip_info/item1").AddComponent<CanEquipCell>());
            this.equipList.Add(base.FindChild("infos/equip_info/item2").AddComponent<CanEquipCell>());
            this.equipList.Add(base.FindChild("infos/equip_info/item3").AddComponent<CanEquipCell>());
            this.equipList.Add(base.FindChild("infos/equip_info/item4").AddComponent<CanEquipCell>());
            this.heroWidgetContainer = this.hero.AddMissingComponent<UIEventListener>();
            this.atlasIconContainer = base.FindChild("infos/atllas_exp/icon").AddMissingComponent<UIEventListener>();
            this.expCollider = base.FindChild("infos/atllas_exp/expCollider").AddMissingComponent<UIEventListener>();
            this.RegisterEventHandler();
        }

        private void InitEquipInfo()
        {
            uint[] numArray = new uint[] { 0x30d37, 0x30d38, 0x30d39, 0x30d3a };
            if (this.generalTemplateInfo.generalInfo == null)
            {
                for (int i = 0; i < this.equipList.Count; i++)
                {
                    this.equipList[i].CellState = com.game.module.General.CellStates.NoEquip;
                    this.equipList[i].TemplateId = numArray[i];
                    this.equipList[i].label.text = "无装备";
                }
            }
            else
            {
                Dictionary<int, PItems> wornEquipItemDic = this.generalTemplateInfo.generalInfo.wornEquipItemDic;
                Dictionary<string, string> defaultEquipIdDic = this.generalTemplateInfo.generalInfo.defaultEquipIdDic;
                for (int j = 0; j < this.equipList.Count; j++)
                {
                    int key = j + 1;
                    CanEquipCell cell = this.equipList[j];
                    uint templateId = uint.Parse(defaultEquipIdDic[key.ToString()]);
                    int num5 = (base.LastOpenedPanelType != PanelType.GENERAL_PANEL) ? 0 : Singleton<BagMode>.Instance.GetItemCountByTemplateId(templateId);
                    if (wornEquipItemDic.ContainsKey(key))
                    {
                        cell.CellState = com.game.module.General.CellStates.HadEquip;
                        cell.TemplateId = wornEquipItemDic[j + 1].templateId;
                    }
                    else if (num5 > 0)
                    {
                        cell.CellState = com.game.module.General.CellStates.CanEquip;
                        cell.TemplateId = numArray[j];
                        cell.label.text = "可装备";
                    }
                    else
                    {
                        cell.CellState = com.game.module.General.CellStates.NoEquip;
                        cell.TemplateId = numArray[j];
                        cell.label.text = "无装备";
                    }
                }
            }
        }

        private void InitHeroInfo()
        {
            SysGeneralStarVo generalStarVo = BaseDataMgr.instance.GetGeneralStarVo(this.generalTemplateInfo.generalInfo.generalId, this.generalTemplateInfo.generalInfo.star);
            SysItemsVo vo2 = BaseDataMgr.instance.getGoodsVo((uint) generalStarVo.item_id);
            this.heroAtlasSprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ItemIconAtlas");
            if (vo2 != null)
            {
                this.heroAtlasSprite.spriteName = vo2.icon + string.Empty;
            }
            this.generalCareer.spriteName = this.typeList[this.generalTemplateInfo.sysGeneralVo.career - 1];
            this.name.text = this.generalTemplateInfo.sysGeneralVo.name;
            SysGeneralExpVo generalExpVo = BaseDataMgr.instance.GetGeneralExpVo(this.generalTemplateInfo.generalInfo.lvl);
            SysGeneralExpVo vo4 = BaseDataMgr.instance.GetGeneralExpVo((uint) (this.generalTemplateInfo.generalInfo.lvl - 1));
            ulong num = 0L;
            if (vo4 != null)
            {
                num = (ulong) vo4.total_exp;
            }
            this.lv.text = this.generalTemplateInfo.generalInfo.lvl + string.Empty;
            this.exp.text = (this.generalTemplateInfo.generalInfo.exp - num) + "/" + generalExpVo.exp_level;
            if (this.generalTemplateInfo.generalInfo.star < 5)
            {
                generalStarVo = BaseDataMgr.instance.GetGeneralStarVo(this.generalTemplateInfo.generalInfo.generalId, this.generalTemplateInfo.generalInfo.star + 1);
                if (generalStarVo != null)
                {
                    int num2 = (base.LastOpenedPanelType != PanelType.GENERAL_PANEL) ? 0 : Singleton<BagMode>.Instance.GetItemCountByTemplateId((uint) generalStarVo.item_id);
                    this.expSlider.value = ((float) num2) / ((float) generalStarVo.count);
                    this.expLabel.text = num2 + "/" + generalStarVo.count;
                    if (base.LastOpenedPanelType != PanelType.GENERAL_PANEL)
                    {
                        this.upgrade.SetActive(false);
                    }
                    else
                    {
                        this.upgrade.SetActive(num2 >= generalStarVo.count);
                    }
                }
            }
            else
            {
                this.upgrade.SetActive(false);
                this.expSlider.value = 1f;
                this.expLabel.text = "已进化至顶级";
            }
        }

        private void LoaCallBack(GameObject go)
        {
            if (go == null)
            {
                MessageManager.Show("英雄模型加载失败!");
            }
            else if (go.name != this.generalTemplateInfo.sysGeneralVo.model_id.ToString())
            {
                this.updateHeroModel();
            }
            else
            {
                if (this.heroModel != null)
                {
                    UnityEngine.Object.Destroy(this.heroModel);
                }
                this.heroModel = NGUITools.AddChild(this.hero, go);
                this.heroModel.name = "heroModel";
                CharacterController component = this.heroModel.GetComponent<CharacterController>();
                component.radius = 0.76f;
                component.height = 1.6f;
                this.updateChildrenLayer(this.heroModel, this.hero.layer);
                this.heroModel.transform.localRotation = new Quaternion(0f, 180f, 0f, 0f);
                this.heroModel.transform.localPosition = new Vector3(0f, -120f, -200f);
                this.heroModel.transform.localScale = new Vector3(160f, 160f, 160f);
            }
        }

        private void OnFingerDown(int fizngerIndex, Vector2 fingerPos)
        {
            this.dragItem = this.PickObject(fingerPos);
        }

        private void OnFingerDragMove(int fingerIndex, Vector2 fingerPos, Vector2 delta)
        {
            if ((this.dragItem != null) && (this.dragItem.transform.name == "heroModel"))
            {
                this.heroModel.transform.eulerAngles = new Vector3(0f, this.heroModel.transform.eulerAngles.y - delta.x, 0f);
            }
        }

        public GameObject PickObject(Vector2 screenPos)
        {
            RaycastHit hit;
            Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) screenPos);
            LayerMask mask = 2;
            return (!Physics.Raycast(ray, out hit, (float) mask.value) ? null : hit.collider.gameObject);
        }

        private void PressHandler(GameObject go, bool state)
        {
            if (!state)
            {
                this.modelState = ModelState.Normal;
            }
            else
            {
                string name = go.name;
                if (name != null)
                {
                    int num;
                    if (<>f__switch$map11 == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(2);
                        dictionary.Add("left", 0);
                        dictionary.Add("right", 1);
                        <>f__switch$map11 = dictionary;
                    }
                    if (<>f__switch$map11.TryGetValue(name, out num))
                    {
                        if (num == 0)
                        {
                            this.modelState = ModelState.Left;
                        }
                        else if (num == 1)
                        {
                            this.modelState = ModelState.Right;
                        }
                    }
                }
            }
        }

        private void RegisterEventHandler()
        {
            this.cardControl = this.cardImgObject.AddMissingComponent<GeneralCardControl>();
            this.cardControl.Init();
            this.close.onClick = new UIWidgetContainer.VoidDelegate(this.ClickHandler);
            this.atlasIconContainer.onClick = new UIEventListener.VoidDelegate(this.ClickHandler);
            this.next.onClick = new UIWidgetContainer.VoidDelegate(this.ChangeInfoByButton);
            this.prev.onClick = new UIWidgetContainer.VoidDelegate(this.ChangeInfoByButton);
            this.upgrade.onClick = new UIWidgetContainer.VoidDelegate(this.ClickHandler);
            this.info.onClick = new UIWidgetContainer.VoidDelegate(this.ClickHandler);
            this.atlas.onClick = new UIWidgetContainer.VoidDelegate(this.ClickHandler);
            this.skill_upgrade.onClick = new UIWidgetContainer.VoidDelegate(this.ClickHandler);
            this.add_atlas.onClick = new UIWidgetContainer.VoidDelegate(this.ClickHandler);
            this.left_btn.onPress = new UIWidgetContainer.BoolDelegate(this.PressHandler);
            this.right_btn.onPress = new UIWidgetContainer.BoolDelegate(this.PressHandler);
            this.expCollider.onClick = new UIEventListener.VoidDelegate(this.ClickHandler);
            foreach (CanEquipCell cell in this.equipList)
            {
                cell.onClick = new UIWidgetContainer.VoidDelegate(this.EquipCellClickHandler);
            }
            this.heroWidgetContainer.onClick = new UIEventListener.VoidDelegate(this.HeroClickHandler);
            Singleton<EquipmentMode>.Instance.wearEquipInfo.Add(0, new NoticeListener(this.WearEquipSucceed));
        }

        private void RegisterFingerGestures()
        {
            this.CancelFingerGestures();
            FingerGestures.OnFingerDown += new FingerGestures.FingerDownEventHandler(this.OnFingerDown);
            FingerGestures.OnFingerDragMove += new FingerGestures.FingerDragMoveEventHandler(this.OnFingerDragMove);
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<GeneralMode>.Instance.dataUpdatedWithParam += new DataUpateHandlerWithParam(this.updatedWithParam);
        }

        private void updateChildrenLayer(GameObject parent, int layer)
        {
            if (<>f__am$cache2A == null)
            {
                <>f__am$cache2A = children => children.gameObject != null;
            }
            IEnumerator<Transform> enumerator = parent.GetComponentsInChildren<Transform>(true).Where<Transform>(<>f__am$cache2A).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = enumerator.Current;
                    current.gameObject.layer = layer;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void updatedWithParam(object sender, int code, object param)
        {
            if ((base.LastOpenedPanelType != PanelType.CORPS_PANEL) || (code == 9))
            {
                switch (code)
                {
                    case 9:
                    {
                        PNewAttr attr = param as PNewAttr;
                        if (attr != null)
                        {
                            this.fight.text = attr.fightPoint + string.Empty;
                            this.cardControl.UpdateDataInfo(attr);
                        }
                        break;
                    }
                    case 10:
                    case 11:
                    case 12:
                        this.ChangeHeroInfo(10);
                        break;

                    case 13:
                    {
                        SkillLearningSkillPointMsg_19_4 g__ = param as SkillLearningSkillPointMsg_19_4;
                        if (g__ != null)
                        {
                            this.cardControl.updateTime(g__.currentSkillPoint, g__.nextRefreshTime);
                        }
                        break;
                    }
                }
            }
        }

        private void updateHeroModel()
        {
            RoleDisplay.Instance.CreateModel(ModelType.Role, this.generalTemplateInfo.sysGeneralVo.model_id.ToString(), new LoadAssetFinish<GameObject>(this.LoaCallBack));
        }

        private void UpdatePanelType()
        {
            if (base.LastOpenedPanelType == PanelType.GENERAL_PANEL)
            {
                this.add_atlas.SetActive(true);
                this.skill_upgrade.SetActive(true);
            }
            else if (base.LastOpenedPanelType == PanelType.CORPS_PANEL)
            {
                this.add_atlas.SetActive(false);
                this.skill_upgrade.SetActive(false);
            }
        }

        private void UpdateRotation()
        {
            if (this.modelState != ModelState.Normal)
            {
                int num = 0;
                switch (this.modelState)
                {
                    case ModelState.Left:
                        num = 100;
                        break;

                    case ModelState.Right:
                        num = -100;
                        break;
                }
                this.heroModel.transform.Rotate(0f, num * Time.deltaTime, 0f, Space.Self);
            }
        }

        private void UpdateStarSprites()
        {
            int star = this.generalTemplateInfo.generalInfo.star;
            if ((star >= 2) && (this.guideUpgradeStar != null))
            {
                this.guideUpgradeStar();
                this.guideUpgradeStar = null;
            }
            for (int i = 0; i < this.starGameObjects.Count; i++)
            {
                this.starGameObjects[i].SetActive(i < star);
            }
        }

        private void UpdateThrreAttack()
        {
            AnimatorStateInfo currentAnimatorStateInfo = this.animator.GetCurrentAnimatorStateInfo(0);
            if ((currentAnimatorStateInfo.normalizedTime > 0.9) && (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ATTACK1))
            {
                this.animator.Play(Status.NAME_HASH_ATTACK2);
            }
            if ((currentAnimatorStateInfo.normalizedTime > 0.8) && (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ATTACK2))
            {
                this.animator.Play(Status.NAME_HASH_ATTACK3);
            }
            if ((currentAnimatorStateInfo.normalizedTime > 0.95) && (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ATTACK3))
            {
                vp_Timer.CancelTimerByHandle(this.attackHandler);
            }
        }

        private void WearEquipSucceed(int type, int v1, int v2, object data)
        {
            if (base.LastOpenedPanelType == PanelType.GENERAL_PANEL)
            {
                this.InitEquipInfo();
                if (this.guideEquip != null)
                {
                    this.guideEquip();
                    this.guideEquip = null;
                }
            }
        }

        public GeneralCardControl CardControl
        {
            get
            {
                return this.cardControl;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override PanelType panelType
        {
            get
            {
                return PanelType.GENERAL_INFO_PANEL;
            }
        }

        public Button SkillUpgrade
        {
            get
            {
                return this.skill_upgrade;
            }
        }

        public Button UpgradeStar
        {
            get
            {
                return this.upgrade;
            }
        }

        public override string url
        {
            get
            {
                return "UI/General/GeneralInfoPanel.assetbundle";
            }
        }

        public enum ModelState
        {
            Normal,
            Left,
            Right
        }
    }
}

