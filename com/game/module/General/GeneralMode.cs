﻿namespace com.game.module.General
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.map;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using com.net.wifi_pvp;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityLog;

    public class GeneralMode : BaseMode<GeneralMode>
    {
        private GeneralTemplateInfo _selectedGeneralInfo;
        [CompilerGenerated]
        private static Func<GeneralInfo, bool> <>f__am$cache10;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache11;
        [CompilerGenerated]
        private static Func<PGeneralInfo, GeneralInfo> <>f__am$cache12;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache13;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache14;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache15;
        [CompilerGenerated]
        private static Comparison<GeneralTemplateInfo> <>f__am$cache16;
        [CompilerGenerated]
        private static Comparison<GeneralTemplateInfo> <>f__am$cache17;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache18;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache19;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache1A;
        [CompilerGenerated]
        private static Comparison<GeneralTemplateInfo> <>f__am$cache1B;
        [CompilerGenerated]
        private static Comparison<GeneralTemplateInfo> <>f__am$cache1C;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache1D;
        [CompilerGenerated]
        private static Func<PGeneralSkillInfo, bool> <>f__am$cache1E;
        [CompilerGenerated]
        private static Comparison<PGeneralFightAttr> <>f__am$cache1F;
        [CompilerGenerated]
        private static Comparison<PGeneralFightAttr> <>f__am$cache20;
        [CompilerGenerated]
        private static Func<int, uint> <>f__am$cache21;
        [CompilerGenerated]
        private static Func<GeneralFightInfo, uint> <>f__am$cache22;
        [CompilerGenerated]
        private static Func<GeneralFightInfo, bool> <>f__am$cache23;
        [CompilerGenerated]
        private static Func<GeneralFightInfo, byte> <>f__am$cache24;
        [CompilerGenerated]
        private static Func<GeneralInfo, ulong> <>f__am$cache25;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache26;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache27;
        [CompilerGenerated]
        private static Func<GeneralTemplateInfo, bool> <>f__am$cache28;
        [CompilerGenerated]
        private static Func<GeneralFightInfo, bool> <>f__am$cacheE;
        [CompilerGenerated]
        private static Func<GeneralInfo, bool> <>f__am$cacheF;
        public readonly Dictionary<uint, GeneralTemplateInfo> allGeneralInfoList = new Dictionary<uint, GeneralTemplateInfo>();
        public uint curSelectedHp;
        public byte curSelectedPlace = 1;
        public readonly List<uint> fightGenralIdList = new List<uint>();
        public Dictionary<ulong, PNewAttr> GeneralAttrList = new Dictionary<ulong, PNewAttr>();
        public readonly Dictionary<ulong, GeneralInfo> generalInfoList = new Dictionary<ulong, GeneralInfo>();
        public readonly Dictionary<ulong, GeneralFightInfo> generalsFightAttrList = new Dictionary<ulong, GeneralFightInfo>();
        public List<string> modeUrlList = new List<string>();
        private readonly Dictionary<uint, PNewAttr> OtherAttrs = new Dictionary<uint, PNewAttr>();
        public uint OtherGeneralId;
        public readonly List<GeneralTemplateInfo> OtherTemplateInfoList = new List<GeneralTemplateInfo>();
        public readonly List<PSetGeneralInfo> selectedGeneralList = new List<PSetGeneralInfo>();
        public List<GeneralTemplateInfo> templateInfoList = new List<GeneralTemplateInfo>();
        public const int UPDATE_CHUZHAN = 4;
        public const int UPDATE_COPYVIEW_HEADICON = 6;
        public const int UPDATE_DELETE_MODEL = 7;
        public const int UPDATE_FIGHT_GENERAL_LIST_INFO = 2;
        public const int UPDATE_GENERAL_ATTACKS = 9;
        public const int UPDATE_GENERAL_INFO = 1;
        public const int UPDATE_GENERAL_LV = 11;
        public const int UPDATE_GENERAL_SKILL_TIME = 13;
        public const int UPDATE_GENERAL_SKILL_UPGRAD = 12;
        public const int UPDATE_GENERAL_STAR = 10;
        public const int UPDATE_GENERALS_FIGHT_ATTR_LIST = 3;
        public const int UPDATE_GENERALS_FIGHT_ATTR_LIST_BY_HP = 8;

        public void AddSelectedGeneral(GeneralInfo info, int place)
        {
            <AddSelectedGeneral>c__AnonStoreyDF ydf = new <AddSelectedGeneral>c__AnonStoreyDF {
                info = info
            };
            ydf.info.place = (byte) place;
            IEnumerator<PSetGeneralInfo> enumerator = this.selectedGeneralList.Where<PSetGeneralInfo>(new Func<PSetGeneralInfo, bool>(ydf.<>m__93)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PSetGeneralInfo current = enumerator.Current;
                    current.place = ydf.info.place;
                    return;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.selectedGeneralList.Add(this.GeneralInfoToPSetGeneralInfo(ydf.info));
        }

        public void ApplyGenralListInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_2.write_2_1(msdata);
            AppNet.gameNet.send(msdata, 2, 1);
        }

        public void ChangeFightGeneralsPlace(ulong curClickGeneralId, byte curClickPlace)
        {
            this.curSelectedPlace = curClickPlace;
            SelfPlayerManager.instance.changeOnFightVo(this.currentFightingGeneralInfo.attr.id);
            this.SetMeVo();
        }

        public void ChangeSelectedGeneralInfo(int index, List<GeneralTemplateInfo> list)
        {
            int num;
            if (list.Count <= 1)
            {
                return;
            }
        Label_000D:
            num = list.IndexOf(this.selectedGeneralInfo);
            if (num != -1)
            {
                num += index;
            }
            if (num < 0)
            {
                num = list.Count - 1;
            }
            else if (num > (list.Count - 1))
            {
                num = 0;
            }
            if (list[num].generalInfo == null)
            {
                this.selectedGeneralInfo = list[num];
                goto Label_000D;
            }
            this.selectedGeneralInfo = list[num];
        }

        public List<GeneralTemplateInfo> CurrentTemplateInfoList(bool isSelf = true)
        {
            return (!isSelf ? this.OtherTemplateInfoList : this.templateInfoList);
        }

        private Dictionary<int, PItems> equipItemses(List<PItems> items)
        {
            Dictionary<int, PItems> dictionary = new Dictionary<int, PItems>();
            foreach (PItems items2 in items)
            {
                SysItemsVo vo = BaseDataMgr.instance.getGoodsVo(items2.templateId);
                dictionary.Add(vo.subtype, items2);
            }
            return dictionary;
        }

        public void GeneralInfoByAttack(PNewAttr attr)
        {
            <GeneralInfoByAttack>c__AnonStoreyE1 ye = new <GeneralInfoByAttack>c__AnonStoreyE1 {
                attr = attr
            };
            IEnumerator<GeneralTemplateInfo> enumerator = this.templateInfoList.Where<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ye.<>m__96)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GeneralTemplateInfo current = enumerator.Current;
                    current.GeneralAttr = ye.attr;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            base.DataUpdateWithParam(9, ye.attr);
        }

        private PSetGeneralInfo GeneralInfoToPSetGeneralInfo(GeneralInfo info)
        {
            return new PSetGeneralInfo { id = info.id, place = info.place };
        }

        public List<uint> GetCurGeneralSkillList()
        {
            if (BaseDataMgr.instance.GetSceneVo(MeVo.instance.mapId).type == 1)
            {
                return this.generalInfoList[(ulong) ((uint) MeVo.instance.generalId)].SkillList;
            }
            return ((this.currentFightingGeneralInfo != null) ? this.currentFightingGeneralInfo.SkillList : null);
        }

        public uint GetCurrentFightGeneralId()
        {
            if (this.currentFightingGeneralInfo != null)
            {
                return (uint) this.currentFightingGeneralInfo.genralTemplateInfo.id;
            }
            return 0;
        }

        public List<uint> GetCurRoleSkillList(ulong Id)
        {
            if (BaseDataMgr.instance.GetSceneVo(MeVo.instance.mapId).type == 1)
            {
                return this.generalInfoList[MeVo.instance.generalId].SkillList;
            }
            return ((this.currentFightingGeneralInfo != null) ? this.generalInfoList[Id].SkillList : null);
        }

        public List<uint> GetFightGeneralIdList()
        {
            this.fightGenralIdList.Clear();
            if (<>f__am$cache22 == null)
            {
                <>f__am$cache22 = generalInfo => (uint) generalInfo.genralTemplateInfo.id;
            }
            IEnumerator<uint> enumerator = this.generalsFightAttrList.Values.Select<GeneralFightInfo, uint>(<>f__am$cache22).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    uint current = enumerator.Current;
                    this.fightGenralIdList.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return this.fightGenralIdList;
        }

        public List<uint> GetFightGeneralSkillListById(uint id)
        {
            <GetFightGeneralSkillListById>c__AnonStoreyDD ydd = new <GetFightGeneralSkillListById>c__AnonStoreyDD {
                id = id
            };
            string str = string.Empty;
            List<uint> list = new List<uint>();
            IEnumerator<GeneralInfo> enumerator = this.generalInfoList.Values.Where<GeneralInfo>(new Func<GeneralInfo, bool>(ydd.<>m__8C)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GeneralInfo current = enumerator.Current;
                    str = current.genralTemplateInfo.skill_list;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (str != string.Empty)
            {
                int[] stringToInt = StringUtils.GetStringToInt(str);
                if (<>f__am$cache21 == null)
                {
                    <>f__am$cache21 = skillId => (uint) skillId;
                }
                list.AddRange(stringToInt.Select<int, uint>(<>f__am$cache21));
            }
            return list;
        }

        public PNewAttr GetGeneralAttrInfo(uint generalId)
        {
            return (!this.OtherAttrs.ContainsKey(generalId) ? null : this.OtherAttrs[generalId]);
        }

        public GeneralFightInfo getGeneralById(int id)
        {
            <getGeneralById>c__AnonStoreyDE yde = new <getGeneralById>c__AnonStoreyDE {
                id = id
            };
            return this.generalsFightAttrList.Values.FirstOrDefault<GeneralFightInfo>(new Func<GeneralFightInfo, bool>(yde.<>m__8F));
        }

        public GeneralTemplateInfo getGeneralById(ulong generalId)
        {
            <getGeneralById>c__AnonStoreyDB ydb = new <getGeneralById>c__AnonStoreyDB {
                generalId = generalId
            };
            if (<>f__am$cache11 == null)
            {
                <>f__am$cache11 = templateInfo => templateInfo.generalInfo != null;
            }
            return this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache11).FirstOrDefault<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ydb.<>m__7A));
        }

        public Dictionary<ulong, GeneralInfo> GetGeneralLlistByCareer(int career)
        {
            <GetGeneralLlistByCareer>c__AnonStoreyE0 ye = new <GetGeneralLlistByCareer>c__AnonStoreyE0 {
                career = career
            };
            if (ye.career == 0)
            {
                return this.generalInfoList;
            }
            if (<>f__am$cache25 == null)
            {
                <>f__am$cache25 = generalInfo => generalInfo.id;
            }
            return this.generalInfoList.Values.Where<GeneralInfo>(new Func<GeneralInfo, bool>(ye.<>m__94)).ToDictionary<GeneralInfo, ulong>(<>f__am$cache25);
        }

        private GeneralTemplateInfo getGeneralTemplateInfo(List<GeneralTemplateInfo> list, uint id)
        {
            <getGeneralTemplateInfo>c__AnonStoreyE5 ye = new <getGeneralTemplateInfo>c__AnonStoreyE5 {
                id = id
            };
            return list.FirstOrDefault<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ye.<>m__9D));
        }

        public GeneralTemplateInfo GetGeneralTemplateInfoById(ulong id)
        {
            foreach (GeneralTemplateInfo info in this.allGeneralInfoList.Values)
            {
                if ((info.generalInfo != null) && (info.generalInfo.id == id))
                {
                    return info;
                }
            }
            return null;
        }

        public uint GetLiveGeneralPlace()
        {
            if (<>f__am$cache23 == null)
            {
                <>f__am$cache23 = fightGeneralInfo => fightGeneralInfo.attr.hp >= (fightGeneralInfo.attr.hpFull * 0.1f);
            }
            if (<>f__am$cache24 == null)
            {
                <>f__am$cache24 = fightGeneralInfo => fightGeneralInfo.place;
            }
            return (from fightGeneralInfo in this.generalsFightAttrList.Values
                where fightGeneralInfo.place != this.curSelectedPlace
                select fightGeneralInfo).Where<GeneralFightInfo>(<>f__am$cache23).Select<GeneralFightInfo, byte>(<>f__am$cache24).FirstOrDefault<byte>();
        }

        public List<GeneralTemplateInfo> HaveGeneralInfoList()
        {
            this.UpdateListStroe();
            if (<>f__am$cache18 == null)
            {
                <>f__am$cache18 = info => info.generalInfo != null;
            }
            return this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache18).ToList<GeneralTemplateInfo>();
        }

        public void PushChangeGeneralMsg(byte GeneralPlace)
        {
            base.DataUpdateWithParam(8, GeneralPlace);
        }

        public void RemoveSelectedGeneral(GeneralInfo info)
        {
            info.place = 0;
            for (int i = 0; i < this.selectedGeneralList.Count; i++)
            {
                if (this.selectedGeneralList[i].id == info.id)
                {
                    this.selectedGeneralList.RemoveAt(i);
                    break;
                }
            }
        }

        public void SendFightGenral(ulong genralId)
        {
            Log.AI(null, " sendFightGeral " + genralId);
            MemoryStream msdata = new MemoryStream();
            PlayerVo meVoByType = AppMap.Instance.me.GetMeVoByType<PlayerVo>();
            Module_2.write_2_3(msdata, genralId, meVoByType.CurHp);
            AppNet.gameNet.send(msdata, 2, 3);
        }

        public void SendFightGenralList(List<PSetGeneralInfo> genralInfoList, byte type)
        {
            MemoryStream msdata = new MemoryStream();
            Module_2.write_2_2(msdata, type, genralInfoList);
            AppNet.gameNet.send(msdata, 2, 2);
        }

        public void SendGeneralAttak(uint generalId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_2.write_2_4(msdata, (ulong) generalId);
            AppNet.gameNet.send(msdata, 2, 4);
        }

        public void SendGeneralSkillTime(byte type = 1)
        {
            MemoryStream msdata = new MemoryStream();
            Module_19.write_19_4(msdata, type);
            AppNet.gameNet.send(msdata, 0x13, 4);
        }

        public void SendGeneralSkillUpgrade(ulong id, uint groupId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_19.write_19_2(msdata, id, groupId);
            AppNet.gameNet.send(msdata, 0x13, 2);
        }

        public void SendGeneralUpgradeStar(ulong generalId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_2.write_2_5(msdata, generalId);
            AppNet.gameNet.send(msdata, 2, 5);
        }

        public void SendOtherGeneralInfo(uint generalId, ulong playerId)
        {
            this.OtherGeneralId = generalId;
            MemoryStream msdata = new MemoryStream();
            Module_2.write_2_7(msdata, playerId);
            AppNet.gameNet.send(msdata, 2, 7);
        }

        private void SetMeVo()
        {
            VoUtils.SetVoAllAttribute(MeVo.instance, this.currentFightingGeneralInfo.attr, this.currentFightingGeneralInfo.EquipAttr);
            MeVo.instance.SetHp((this.curSelectedHp <= 0) ? this.currentFightingGeneralInfo.attr.hp : this.curSelectedHp);
            if ((MeVo.instance.CurHp > 0) && GlobalData.isInCopy)
            {
                Singleton<BattleCenterView>.Instance.updateHp(AppMap.Instance.me.GetMeVoByType<PlayerVo>());
            }
        }

        private uint skillListToUintId(PGeneralSkillInfo info)
        {
            return ((info.groupId * 0x2710) + info.skillLvl);
        }

        private List<GeneralTemplateInfo> sortInfoList(List<GeneralTemplateInfo> list)
        {
            if (<>f__am$cache19 == null)
            {
                <>f__am$cache19 = info => info.place > 0;
            }
            List<GeneralTemplateInfo> list2 = list.Where<GeneralTemplateInfo>(<>f__am$cache19).ToList<GeneralTemplateInfo>();
            if (<>f__am$cache1A == null)
            {
                <>f__am$cache1A = info => info.place <= 0;
            }
            List<GeneralTemplateInfo> collection = list.Where<GeneralTemplateInfo>(<>f__am$cache1A).ToList<GeneralTemplateInfo>();
            if (<>f__am$cache1B == null)
            {
                <>f__am$cache1B = (x, y) => x.place.CompareTo(y.place);
            }
            list2.Sort(<>f__am$cache1B);
            if (<>f__am$cache1C == null)
            {
                <>f__am$cache1C = (x, y) => -x.listIndex.CompareTo(y.listIndex);
            }
            collection.Sort(<>f__am$cache1C);
            list2.AddRange(collection);
            return list2;
        }

        public void UpdateAllGeneralInfo()
        {
            foreach (SysGeneralVo vo in BaseDataMgr.instance.GetAllGeneralList().Values)
            {
                if (vo.is_display)
                {
                    GeneralTemplateInfo info = new GeneralTemplateInfo {
                        sysGeneralVo = vo
                    };
                    this.allGeneralInfoList.Add((uint) vo.general_id, info);
                    this.templateInfoList.Add(this.allGeneralInfoList[(uint) vo.general_id]);
                }
            }
        }

        public void UpdateChuZhanGeneral(GeneralInfo info)
        {
            base.DataUpdateWithParam(4, info);
        }

        public void UpdateDeleteModel(GeneralInfo info)
        {
            base.DataUpdateWithParam(7, info);
        }

        public void UpdateFightGeneralListInfo(List<PSetGeneralInfo> pFightGeneralInfoList, byte type)
        {
            foreach (PSetGeneralInfo info in pFightGeneralInfoList)
            {
                this.UpdateGeneralPlace(info.id, info.place);
                GeneralInfo info2 = new GeneralInfo(true) {
                    id = info.id,
                    place = info.place
                };
                info2.generalId = this.generalInfoList[info2.id].generalId;
                info2.quality = this.generalInfoList[info2.id].quality;
                info2.skillInfoList = this.generalInfoList[info2.id].skillInfoList;
                info2.genralTemplateInfo = BaseDataMgr.instance.GetGeneralVo(info2.generalId, info2.quality);
                info2.defaultEquipIdDic = this.generalInfoList[info2.id].defaultEquipIdDic;
                info2.wornEquipItemDic = this.generalInfoList[info2.id].wornEquipItemDic;
                if (this.generalInfoList.ContainsKey(info2.id))
                {
                    this.generalInfoList.Remove(info2.id);
                }
                this.generalInfoList.Add(info2.id, info2);
            }
            base.DataUpdate(2);
            WifiLAN.Instance.UpdateRemoteRecordWhenChange();
            this.UpdateListStroe();
        }

        public void UpdateFightGeneralsAttrlistinfo(List<PGeneralFightAttr> attrList, uint type, ulong dupGeneralId, uint CurHp)
        {
            if (!AppMap.Instance.IsInWifiPVP)
            {
                uint num = type;
                if ((num != 0) && (num == 1))
                {
                    this.generalsFightAttrList.Clear();
                    if (<>f__am$cache20 == null)
                    {
                        <>f__am$cache20 = (a, b) => a.place - b.place;
                    }
                    attrList.Sort(<>f__am$cache20);
                    foreach (PGeneralFightAttr attr in attrList)
                    {
                        GeneralFightInfo info = new GeneralFightInfo {
                            attr = attr.attr,
                            place = attr.place,
                            genralTemplateInfo = this.generalInfoList[attr.attr.id].genralTemplateInfo,
                            skillInfoList = attr.skill
                        };
                        if (this.generalsFightAttrList.ContainsKey(attr.attr.id))
                        {
                            this.generalsFightAttrList.Remove(attr.attr.id);
                        }
                        this.generalsFightAttrList.Add(attr.attr.id, info);
                    }
                    if (dupGeneralId > 0L)
                    {
                        this.curSelectedPlace = this.generalsFightAttrList[dupGeneralId].place;
                    }
                    else
                    {
                        this.curSelectedPlace = this.currentFightMinPlace;
                    }
                    base.DataUpdate(6);
                    this.SetMeVo();
                }
            }
            Log.AI(null, " Change Wujiang " + type);
            base.DataUpdate(3);
        }

        public void UpdateFightGeneralsAttrWhenArena(IEnumerable<PGeneralFightAttr> list)
        {
            IList<PGeneralFightAttr> list2;
            this.generalsFightAttrList.Clear();
            IList<PGeneralFightAttr> list1 = list as IList<PGeneralFightAttr>;
            if (list1 != null)
            {
                list2 = list1;
            }
            else
            {
                list2 = list.ToList<PGeneralFightAttr>();
            }
            IEnumerator<PGeneralFightAttr> enumerator = list2.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PGeneralFightAttr current = enumerator.Current;
                    PNewAttr attr = current.attr;
                    GeneralFightInfo info = new GeneralFightInfo {
                        attr = attr,
                        place = this.generalInfoList[attr.id].place,
                        genralTemplateInfo = this.generalInfoList[attr.id].genralTemplateInfo,
                        skillInfoList = current.skill
                    };
                    if (this.generalsFightAttrList.ContainsKey(attr.id))
                    {
                        this.generalsFightAttrList.Remove(attr.id);
                    }
                    this.generalsFightAttrList.Add(attr.id, info);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            base.DataUpdate(6);
            this.curSelectedPlace = this.currentFightMinPlace;
            this.SetMeVo();
            base.DataUpdate(3);
        }

        public void UpdateGeneralInfo(List<PGeneralInfo> pGeneralInfoList)
        {
            if (this.allGeneralInfoList.Count == 0)
            {
                this.UpdateAllGeneralInfo();
            }
            if (<>f__am$cache12 == null)
            {
                <>f__am$cache12 = generalInfo => new GeneralInfo(true) { id = generalInfo.id, generalId = generalInfo.generalId, quality = generalInfo.quality, place = generalInfo.place, star = generalInfo.star, lvl = generalInfo.lvl, fightPoint = generalInfo.fightPoint, exp = generalInfo.exp, skillInfoList = generalInfo.skillList };
            }
            IEnumerator<GeneralInfo> enumerator = pGeneralInfoList.Select<PGeneralInfo, GeneralInfo>(<>f__am$cache12).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GeneralInfo current = enumerator.Current;
                    current.genralTemplateInfo = BaseDataMgr.instance.GetGeneralVo(current.generalId, current.quality);
                    current.SetDefaultEquipIdDic(current.genralTemplateInfo.default_equip_id);
                    if (this.generalInfoList.ContainsKey(current.id))
                    {
                        current.wornEquipItemDic = this.generalInfoList[current.id].wornEquipItemDic;
                        this.generalInfoList.Remove(current.id);
                    }
                    this.generalInfoList.Add(current.id, current);
                    if (this.allGeneralInfoList.ContainsKey(current.generalId))
                    {
                        this.allGeneralInfoList[current.generalId].generalInfo = current;
                        this.allGeneralInfoList[current.generalId].place = current.place;
                        this.allGeneralInfoList[current.generalId].Id = current.generalId;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.UpdateListStroe();
            base.DataUpdate(1);
        }

        public void UpdateGeneralLv(GeneralGeneralLvlMsg_2_6 generalLvl)
        {
            <UpdateGeneralLv>c__AnonStoreyE4 ye = new <UpdateGeneralLv>c__AnonStoreyE4 {
                generalLvl = generalLvl
            };
            if (<>f__am$cache28 == null)
            {
                <>f__am$cache28 = templateInfo => templateInfo.generalInfo != null;
            }
            IEnumerator<GeneralTemplateInfo> enumerator = this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache28).Where<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ye.<>m__9C)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GeneralTemplateInfo current = enumerator.Current;
                    current.generalInfo.lvl = ye.generalLvl.lvl;
                    current.generalInfo.exp = ye.generalLvl.exp;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            base.DataUpdateWithParam(11, ye.generalLvl);
        }

        public void UpdateGeneralPlace(ulong id, byte place)
        {
            <UpdateGeneralPlace>c__AnonStoreyE2 ye = new <UpdateGeneralPlace>c__AnonStoreyE2 {
                id = id
            };
            if (<>f__am$cache26 == null)
            {
                <>f__am$cache26 = templateInfo => templateInfo.generalInfo != null;
            }
            IEnumerator<GeneralTemplateInfo> enumerator = this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache26).Where<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ye.<>m__98)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GeneralTemplateInfo current = enumerator.Current;
                    current.generalInfo.place = place;
                    current.place = place;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void UpdateGeneralSkillInfo(SkillLearningSkillLvlupMsg_19_2 skillLvlup)
        {
            <UpdateGeneralSkillInfo>c__AnonStoreyDC ydc = new <UpdateGeneralSkillInfo>c__AnonStoreyDC {
                skillLvlup = skillLvlup
            };
            if (<>f__am$cache1D == null)
            {
                <>f__am$cache1D = templateInfo => templateInfo.generalInfo != null;
            }
            if (<>f__am$cache1E == null)
            {
                <>f__am$cache1E = info => info != null;
            }
            IEnumerator<PGeneralSkillInfo> enumerator = this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache1D).Where<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ydc.<>m__87)).Select<GeneralTemplateInfo, PGeneralSkillInfo>(new Func<GeneralTemplateInfo, PGeneralSkillInfo>(ydc.<>m__88)).Where<PGeneralSkillInfo>(<>f__am$cache1E).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PGeneralSkillInfo current = enumerator.Current;
                    current.skillLvl = ydc.skillLvlup.skillLevel;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            base.DataUpdateWithParam(12, ydc.skillLvlup);
        }

        public void UpdateGeneralSkillTime(SkillLearningSkillPointMsg_19_4 skillPoint)
        {
            base.DataUpdateWithParam(13, skillPoint);
        }

        public void UpdateGeneralStar(ulong id, byte generalStar)
        {
            <UpdateGeneralStar>c__AnonStoreyE3 ye = new <UpdateGeneralStar>c__AnonStoreyE3 {
                id = id
            };
            if (<>f__am$cache27 == null)
            {
                <>f__am$cache27 = templateInfo => templateInfo.generalInfo != null;
            }
            IEnumerator<GeneralTemplateInfo> enumerator = this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache27).Where<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ye.<>m__9A)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GeneralTemplateInfo current = enumerator.Current;
                    current.generalInfo.star = generalStar;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            base.DataUpdateWithParam(10, generalStar);
        }

        private void UpdateListStroe()
        {
            if (<>f__am$cache13 == null)
            {
                <>f__am$cache13 = info => info.place > 0;
            }
            List<GeneralTemplateInfo> list = this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache13).ToList<GeneralTemplateInfo>();
            if (<>f__am$cache14 == null)
            {
                <>f__am$cache14 = info => (info.generalInfo != null) && (info.place <= 0);
            }
            List<GeneralTemplateInfo> collection = this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache14).ToList<GeneralTemplateInfo>();
            if (<>f__am$cache15 == null)
            {
                <>f__am$cache15 = info => info.generalInfo == null;
            }
            List<GeneralTemplateInfo> list3 = this.templateInfoList.Where<GeneralTemplateInfo>(<>f__am$cache15).ToList<GeneralTemplateInfo>();
            if (<>f__am$cache16 == null)
            {
                <>f__am$cache16 = (x, y) => x.place.CompareTo(y.place);
            }
            list.Sort(<>f__am$cache16);
            if (<>f__am$cache17 == null)
            {
                <>f__am$cache17 = (x, y) => -x.listIndex.CompareTo(y.listIndex);
            }
            collection.Sort(<>f__am$cache17);
            collection.AddRange(list3);
            list.AddRange(collection);
            this.templateInfoList = list;
        }

        public void UpdateOtherInfo(List<POtherRoleGeneralInfo> generalInfoList)
        {
            if (this.OtherTemplateInfoList.Count != 0)
            {
                this.OtherTemplateInfoList.Clear();
            }
            if (this.OtherAttrs.Count<KeyValuePair<uint, PNewAttr>>() != 0)
            {
                this.OtherAttrs.Clear();
            }
            foreach (POtherRoleGeneralInfo info in generalInfoList)
            {
                this.OtherAttrs.Add(info.generalTemplateId, info.generalAttrInfo);
                GeneralTemplateInfo item = new GeneralTemplateInfo();
                SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo(info.generalTemplateId, info.quality);
                GeneralInfo info3 = new GeneralInfo(false) {
                    id = info.id,
                    generalId = info.generalTemplateId,
                    quality = info.quality,
                    place = info.place,
                    star = info.star,
                    exp = info.exp,
                    lvl = info.lvl,
                    skillInfoList = info.skillList,
                    genralTemplateInfo = generalVo,
                    wornEquipItemDic = this.equipItemses(info.equipList)
                };
                info3.SetDefaultEquipIdDic(generalVo.default_equip_id);
                item.generalInfo = info3;
                item.sysGeneralVo = generalVo;
                item.place = info3.place;
                item.Id = info3.generalId;
                this.OtherTemplateInfoList.Add(item);
            }
            if (Singleton<CorpsPanel>.Instance != null)
            {
                Singleton<CorpsPanel>.Instance.CloseView();
            }
            this.selectedGeneralInfo = this.getGeneralTemplateInfo(this.OtherTemplateInfoList, this.OtherGeneralId);
            if (this.selectedGeneralInfo != null)
            {
                Singleton<GeneralInfoPanel>.Instance.OpenView(PanelType.CORPS_PANEL);
            }
        }

        public void UpdateWifiPVPMyHeroInformation(GeneralFightAttrWifiMsg_2_9 wifiMsg)
        {
            Log.AI(null, "Init Wifi Data Clear heroInfo " + this.generalsFightAttrList.Count);
            this.generalsFightAttrList.Clear();
            Log.AI(null, "wifiMsg Len " + wifiMsg.generalAttr.Count);
            if (<>f__am$cache1F == null)
            {
                <>f__am$cache1F = (a, b) => a.place - b.place;
            }
            wifiMsg.generalAttr.Sort(<>f__am$cache1F);
            foreach (PGeneralFightAttr attr in wifiMsg.generalAttr)
            {
                PNewAttr equipAttr = ObjectManager.GetEquipAttr(attr.attr.id, wifiMsg.generalEquipAttr);
                GeneralFightInfo info = new GeneralFightInfo {
                    attr = attr.attr,
                    EquipAttr = equipAttr,
                    place = attr.place,
                    genralTemplateInfo = this.generalInfoList[attr.attr.id].genralTemplateInfo,
                    skillInfoList = attr.skill
                };
                if (this.generalsFightAttrList.ContainsKey(attr.attr.id))
                {
                    this.generalsFightAttrList.Remove(attr.attr.id);
                }
                this.generalsFightAttrList.Add(attr.attr.id, info);
            }
            this.curSelectedPlace = this.currentFightMinPlace;
            base.DataUpdate(6);
            this.SetMeVo();
            base.DataUpdate(3);
        }

        public GeneralFightInfo currentFightingGeneralInfo
        {
            get
            {
                return this.generalsFightAttrList.Values.FirstOrDefault<GeneralFightInfo>(fightGeneralInfo => (fightGeneralInfo.place == this.curSelectedPlace));
            }
        }

        public byte currentFightMinPlace
        {
            get
            {
                if (<>f__am$cacheE == null)
                {
                    <>f__am$cacheE = fightGeneralInfo => fightGeneralInfo != null;
                }
                List<GeneralFightInfo> list = this.generalsFightAttrList.Values.Where<GeneralFightInfo>(<>f__am$cacheE).ToList<GeneralFightInfo>();
                list.Sort();
                if (list.Count != 0)
                {
                    return list[0].place;
                }
                return 1;
            }
        }

        public List<GeneralInfo> GetDefaultFightGeneralList
        {
            get
            {
                if (<>f__am$cacheF == null)
                {
                    <>f__am$cacheF = generalInfo => generalInfo.place > 0;
                }
                List<GeneralInfo> list = this.generalInfoList.Values.Where<GeneralInfo>(<>f__am$cacheF).ToList<GeneralInfo>();
                list.Sort();
                return list;
            }
        }

        public List<GeneralInfo> GetNotFightGeneralList
        {
            get
            {
                if (<>f__am$cache10 == null)
                {
                    <>f__am$cache10 = generalInfo => generalInfo.place == 0;
                }
                return this.generalInfoList.Values.Where<GeneralInfo>(<>f__am$cache10).ToList<GeneralInfo>();
            }
        }

        public GeneralTemplateInfo selectedGeneralInfo
        {
            get
            {
                return this._selectedGeneralInfo;
            }
            set
            {
                this._selectedGeneralInfo = value;
            }
        }

        [CompilerGenerated]
        private sealed class <AddSelectedGeneral>c__AnonStoreyDF
        {
            internal GeneralInfo info;

            internal bool <>m__93(PSetGeneralInfo generalInfo)
            {
                return (generalInfo.id == this.info.id);
            }
        }

        [CompilerGenerated]
        private sealed class <GeneralInfoByAttack>c__AnonStoreyE1
        {
            internal PNewAttr attr;

            internal bool <>m__96(GeneralTemplateInfo info)
            {
                return ((info.generalInfo != null) && (info.generalInfo.id == this.attr.id));
            }
        }

        [CompilerGenerated]
        private sealed class <GetFightGeneralSkillListById>c__AnonStoreyDD
        {
            internal uint id;

            internal bool <>m__8C(GeneralInfo generalInfo)
            {
                return (generalInfo.genralTemplateInfo.id == this.id);
            }
        }

        [CompilerGenerated]
        private sealed class <getGeneralById>c__AnonStoreyDB
        {
            internal ulong generalId;

            internal bool <>m__7A(GeneralTemplateInfo templateInfo)
            {
                return (templateInfo.generalInfo.id == this.generalId);
            }
        }

        [CompilerGenerated]
        private sealed class <getGeneralById>c__AnonStoreyDE
        {
            internal int id;

            internal bool <>m__8F(GeneralFightInfo generalInfo)
            {
                return (generalInfo.genralTemplateInfo.general_id == this.id);
            }
        }

        [CompilerGenerated]
        private sealed class <GetGeneralLlistByCareer>c__AnonStoreyE0
        {
            internal int career;

            internal bool <>m__94(GeneralInfo generalInfo)
            {
                return (generalInfo.genralTemplateInfo.career == this.career);
            }
        }

        [CompilerGenerated]
        private sealed class <getGeneralTemplateInfo>c__AnonStoreyE5
        {
            internal uint id;

            internal bool <>m__9D(GeneralTemplateInfo info)
            {
                return (info.Id == this.id);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateGeneralLv>c__AnonStoreyE4
        {
            internal GeneralGeneralLvlMsg_2_6 generalLvl;

            internal bool <>m__9C(GeneralTemplateInfo templateInfo)
            {
                return (templateInfo.generalInfo.id == this.generalLvl.generalId);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateGeneralPlace>c__AnonStoreyE2
        {
            internal ulong id;

            internal bool <>m__98(GeneralTemplateInfo templateInfo)
            {
                return (templateInfo.generalInfo.id == this.id);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateGeneralSkillInfo>c__AnonStoreyDC
        {
            internal SkillLearningSkillLvlupMsg_19_2 skillLvlup;

            internal bool <>m__87(GeneralTemplateInfo templateInfo)
            {
                return (templateInfo.generalInfo.id == this.skillLvlup.generalId);
            }

            internal PGeneralSkillInfo <>m__88(GeneralTemplateInfo templateInfo)
            {
                return templateInfo.generalInfo.GetGeneralSkillInfo(this.skillLvlup.groupId);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateGeneralStar>c__AnonStoreyE3
        {
            internal ulong id;

            internal bool <>m__9A(GeneralTemplateInfo templateInfo)
            {
                return (templateInfo.generalInfo.id == this.id);
            }
        }
    }
}

