﻿namespace com.game.module.General
{
    using com.game.consts;
    using com.game.module.core;
    using com.game.module.General.Components;
    using com.game.Public.Message;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GeneralPanel : BaseView<GeneralPanel>
    {
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache2D;
        [CompilerGenerated]
        private static Func<GameObject, bool> <>f__am$cache2E;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache2F;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache30;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache31;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache32;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache33;
        [CompilerGenerated]
        private static Func<GeneralListItemView, bool> <>f__am$cache34;
        [CompilerGenerated]
        private static Func<GameObject, bool> <>f__am$cache35;
        [CompilerGenerated]
        private static Func<GameObject, GeneralListItemView> <>f__am$cache36;
        [CompilerGenerated]
        private static Func<GeneralListItemView, bool> <>f__am$cache37;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache38;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache39;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache3A;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache3B;
        [CompilerGenerated]
        private static Comparison<GeneralListItemView> <>f__am$cache3C;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map13;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map14;
        private readonly List<GeneralListItemView> allItemList = new List<GeneralListItemView>();
        private int AssetCount;
        private readonly List<GeneralListItemView> attackItemList = new List<GeneralListItemView>();
        private readonly List<GeneralListItemView> auxiliaryItemList = new List<GeneralListItemView>();
        private BoxCollider backgroundBoxCollider;
        private Button close_btn;
        private dirMode currentDir;
        private int currentIndex;
        private List<GeneralListItemView> currentItemList;
        public GeneralListItemView currentItemView;
        private ViewMode currentMode = ViewMode.KapaiMode;
        private int currentToggleIndex;
        private UIToggle curToggle;
        private readonly List<GeneralListItemView> defenseItemList = new List<GeneralListItemView>();
        private UIDragDropContainer dragDropContainer;
        private GameObject dragItem;
        private GameObject dropDown_list;
        private Button feed_btn;
        private Button fight_btn;
        private readonly List<GeneralListItemView> fightItemList = new List<GeneralListItemView>();
        private UILabel fightLabel;
        private GameObject GeneralInfoItemView;
        private UIPanel GroupPanel;
        public GuideHeroDelegate guideHeroDelegate;
        public GuidHeroOverDelegate guideHeroOver;
        public GuideSelectDelete guideSelect;
        private BoxCollider ImgGroupBoxCollider;
        private GameObject KapaiGroup;
        private GameObject ListGroup;
        private Button look_btn;
        private readonly List<GeneralListItemView> magicItemList = new List<GeneralListItemView>();
        private const int maxCount = 6;
        private Button mode_btn;
        private UILabel modeLabel;
        private PanelType NextPanel;
        private bool openExpDragPanel;
        private UIPopupList popupList;
        private UIScrollView scrollView;
        private readonly List<GameObject> showItems = new List<GameObject>();
        private readonly List<UIToggle> tabButtonList = new List<UIToggle>();
        private vp_Timer.Handle timehandle = new vp_Timer.Handle();
        private int totalDelta;
        private Button user_general;

        private void ButtonClickHandler(GameObject go)
        {
            if (!this.IsDraing)
            {
                string name = go.name;
                if (name != null)
                {
                    int num;
                    if (<>f__switch$map14 == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(6);
                        dictionary.Add("close", 0);
                        dictionary.Add("mode", 1);
                        dictionary.Add("look", 2);
                        dictionary.Add("feed", 3);
                        dictionary.Add("fight", 4);
                        dictionary.Add("all_hero", 5);
                        <>f__switch$map14 = dictionary;
                    }
                    if (<>f__switch$map14.TryGetValue(name, out num))
                    {
                        switch (num)
                        {
                            case 0:
                                Singleton<GeneralMode>.Instance.SendFightGenralList(Singleton<GeneralMode>.Instance.selectedGeneralList, 2);
                                this.CloseView();
                                break;

                            case 1:
                                this.currentMode = (this.currentMode != ViewMode.KapaiMode) ? ViewMode.KapaiMode : ViewMode.ListMode;
                                this.UpdateShowItemMode();
                                this.currentToggleIndex = 0;
                                this.tabButtonList[this.currentToggleIndex].value = true;
                                this.currentItemList = this.allItemList;
                                if (<>f__am$cache2D == null)
                                {
                                    <>f__am$cache2D = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
                                }
                                this.currentItemList.Sort(<>f__am$cache2D);
                                this.PopupListChange();
                                break;

                            case 2:
                                if (this.currentItemView != null)
                                {
                                    Singleton<GeneralMode>.Instance.selectedGeneralInfo = this.currentItemView.generalTemlateInfo;
                                    this.NextPanel = PanelType.GENERAL_INFO_PANEL;
                                    this.CloseView();
                                    Singleton<GeneralInfoPanel>.Instance.OpenView(PanelType.GENERAL_PANEL);
                                    break;
                                }
                                MessageManager.Show("先选择一名英雄");
                                return;

                            case 3:
                                this.OpenExpDrugPanel();
                                break;

                            case 4:
                                if (this.guideHeroOver != null)
                                {
                                    this.guideHeroOver();
                                    this.guideHeroOver = null;
                                }
                                if (this.currentItemView == null)
                                {
                                    MessageManager.Show("先选择一名英雄");
                                    return;
                                }
                                if (this.currentItemView.generalInfo.place > 0)
                                {
                                    this.currentItemView.changeRelease(this.dragDropContainer.gameObject);
                                }
                                else if (this.fightItemList.Count >= 3)
                                {
                                    MessageManager.Show("出战英雄已满");
                                }
                                else
                                {
                                    if (<>f__am$cache2E == null)
                                    {
                                        <>f__am$cache2E = item => item.transform.childCount <= 0;
                                    }
                                    GameObject surface = this.showItems.FirstOrDefault<GameObject>(<>f__am$cache2E);
                                    if (surface != null)
                                    {
                                        this.currentItemView.changeRelease(surface);
                                    }
                                    else
                                    {
                                        MessageManager.Show("出战英雄已满");
                                    }
                                }
                                break;

                            case 5:
                                vp_Timer.CancelTimerByHandle(this.timehandle);
                                vp_Timer.In(0.01f, new vp_Timer.Callback(this.PopupListOpetion), 1, 1f, this.timehandle);
                                break;
                        }
                    }
                }
            }
        }

        private void CancelFingerGestures()
        {
            FingerGestures.OnFingerDown -= new FingerGestures.FingerDownEventHandler(this.OnFingerDown);
            FingerGestures.OnFingerDragMove -= new FingerGestures.FingerDragMoveEventHandler(this.OnFingerDragMove);
        }

        public override void CancelUpdateHandler()
        {
            Singleton<GeneralMode>.Instance.dataUpdatedWithParam -= new DataUpateHandlerWithParam(this.updatedWithParam);
        }

        private void changeModeByKapai()
        {
            if (this.currentItemList != null)
            {
                this.dragDropContainer.reparentTarget = this.KapaiGroup.transform;
                this.ListGroup.SetActive(false);
                foreach (GeneralListItemView view in this.currentItemList)
                {
                    if (!view.IsDisplay)
                    {
                        view.gameObject.SetActive(false);
                    }
                    else
                    {
                        view.gameObject.SetActive(true);
                        view.GetComponent<UIDragScrollView>().enabled = false;
                        view.transform.parent = this.KapaiGroup.transform;
                        view.UpdateItemEnabled();
                        view.UpdateMode(ViewMode.KapaiMode, GeneralListItemView.ItemState.state13);
                    }
                }
                this.currentIndex = 0;
                if (<>f__am$cache34 == null)
                {
                    <>f__am$cache34 = itemView => itemView.IsDisplay;
                }
                List<GeneralListItemView> itemList = this.currentItemList.Where<GeneralListItemView>(<>f__am$cache34).ToList<GeneralListItemView>();
                this.showKapaiList(this.currentIndex, 6, itemList, true);
                this.KapaiGroup.SetActive(true);
            }
        }

        public void changeModeByList()
        {
            if (this.currentItemList != null)
            {
                this.dragDropContainer.reparentTarget = this.ListGroup.transform;
                this.KapaiGroup.SetActive(false);
                UIGrid component = this.ListGroup.GetComponent<UIGrid>();
                int maxPerLine = component.maxPerLine;
                float cellWidth = component.cellWidth;
                float cellHeight = component.cellHeight;
                int num4 = 0;
                int num5 = 0;
                foreach (GeneralListItemView view in this.currentItemList)
                {
                    if (!view.IsDisplay)
                    {
                        view.gameObject.SetActive(false);
                    }
                    else
                    {
                        view.gameObject.SetActive(true);
                        view.GetComponent<UIDragScrollView>().enabled = true;
                        view.transform.parent = this.ListGroup.transform;
                        view.UpdateItemEnabled();
                        view.UpdateMode(ViewMode.ListMode, GeneralListItemView.ItemState.none);
                        Vector3 vector = new Vector3(cellWidth * num5, -cellHeight * num4, 0f);
                        view.transform.localPosition = vector;
                        if ((++num4 >= maxPerLine) && (maxPerLine > 0))
                        {
                            num4 = 0;
                            num5++;
                        }
                    }
                }
                this.ListGroup.SetActive(true);
            }
        }

        private void createItemView(GeneralTemplateInfo generalTemplateInfo)
        {
            GameObject obj2 = NGUITools.AddChild((this.currentMode != ViewMode.KapaiMode) ? this.ListGroup : this.KapaiGroup, this.GeneralInfoItemView);
            obj2.SetActive(true);
            obj2.name = "generalItem";
            obj2.GetComponent<UIDragScrollView>().enabled = this.currentMode != ViewMode.KapaiMode;
            GeneralListItemView item = obj2.AddComponent<GeneralListItemView>();
            item.Init(this.currentMode, generalTemplateInfo);
            item.setIndexByDictionary(0, this.allItemList.Count - 1);
            this.allItemList.Add(item);
            if (generalTemplateInfo.sysGeneralVo.career == 1)
            {
                item.setIndexByDictionary(1, this.attackItemList.Count - 1);
                this.attackItemList.Add(item);
            }
            else if (generalTemplateInfo.sysGeneralVo.career == 2)
            {
                item.setIndexByDictionary(2, this.defenseItemList.Count - 1);
                this.defenseItemList.Add(item);
            }
            else if (generalTemplateInfo.sysGeneralVo.career == 3)
            {
                item.setIndexByDictionary(3, this.magicItemList.Count - 1);
                this.magicItemList.Add(item);
            }
            else if (generalTemplateInfo.sysGeneralVo.career == 4)
            {
                item.setIndexByDictionary(4, this.auxiliaryItemList.Count - 1);
                this.auxiliaryItemList.Add(item);
            }
        }

        private void DataUpdatedOpenExpDrugPanel(object sender, int code)
        {
            if ((code == 2) && this.openExpDragPanel)
            {
                Singleton<ExpDrugPanel>.Instance.OpenExpDrugPanel(this.currentItemView.generalInfo);
                this.CloseView();
                this.openExpDragPanel = false;
            }
        }

        public int getFightCount()
        {
            return this.fightItemList.Count;
        }

        public GeneralListItemView GetFightGeneralItemView()
        {
            return this.getItemView();
        }

        public GameObject getImageGroup()
        {
            return this.dragDropContainer.gameObject;
        }

        private int getInsertByIndex(List<GeneralListItemView> list, int itemIndex)
        {
            if (itemIndex >= list.Count)
            {
                return -1;
            }
            return itemIndex;
        }

        private GeneralListItemView.ItemState getItemState(GeneralListItemView itemView)
        {
            int currentState = (int) itemView.CurrentState;
            if (this.currentDir == dirMode.right)
            {
                if (currentState <= 11)
                {
                    currentState++;
                }
            }
            else if ((this.currentDir == dirMode.left) && (currentState >= 1))
            {
                currentState--;
            }
            return (GeneralListItemView.ItemState) currentState;
        }

        private GeneralListItemView getItemView()
        {
            if (<>f__am$cache35 == null)
            {
                <>f__am$cache35 = go => go.transform.childCount >= 1;
            }
            if (<>f__am$cache36 == null)
            {
                <>f__am$cache36 = go => go.transform.GetChild(0).GetComponent<GeneralListItemView>();
            }
            return this.showItems.Where<GameObject>(<>f__am$cache35).Select<GameObject, GeneralListItemView>(<>f__am$cache36).FirstOrDefault<GeneralListItemView>();
        }

        public Transform getKapaiItem()
        {
            return ((this.dragDropContainer.reparentTarget.childCount != 0) ? this.dragDropContainer.reparentTarget.GetChild(0) : null);
        }

        public Button GetLookBtn()
        {
            return this.look_btn;
        }

        protected override void HandleAfterOpenView()
        {
            this.currentToggleIndex = 0;
            this.tabButtonList[this.currentToggleIndex].value = true;
            this.UpdateItemInfo(true);
            if (this.getItemView() != null)
            {
                this.currentItemView = this.getItemView();
            }
            this.UpdateSelected(this.currentItemView);
        }

        protected override void HandleBeforeCloseView()
        {
            if (this.currentItemView != null)
            {
                this.currentItemView.updateSelected(false);
                this.currentItemView = null;
            }
        }

        protected override void Init()
        {
            string[] strArray = new string[] { "TabBar/tab_buttons/Grid/tab_index_1", "TabBar/tab_buttons/Grid/tab_index_2", "TabBar/tab_buttons/Grid/tab_index_3", "TabBar/tab_buttons/Grid/tab_index_4", "TabBar/tab_buttons/Grid/tab_index_5" };
            for (int i = 0; i < strArray.Length; i++)
            {
                this.tabButtonList.Add(base.FindInChild<UIToggle>(strArray[i]));
                EventDelegate.Add(this.tabButtonList[i].onChange, new EventDelegate.Callback(this.TabBtnClickHandler));
            }
            this.tabButtonList[0].SetActive(true);
            this.modeLabel = base.FindInChild<UILabel>("TabBar/background/mode/name");
            this.close_btn = base.FindInChild<Button>("option_buttons/close");
            this.mode_btn = base.FindInChild<Button>("TabBar/background/mode");
            this.look_btn = base.FindInChild<Button>("option_buttons/look");
            this.feed_btn = base.FindInChild<Button>("option_buttons/feed");
            this.fight_btn = base.FindInChild<Button>("option_buttons/fight");
            this.user_general = base.FindInChild<Button>("option_buttons/all_hero");
            this.popupList = base.FindInChild<UIPopupList>("option_buttons/all_hero");
            this.fightLabel = base.FindInChild<UILabel>("option_buttons/fight/Label");
            this.KapaiGroup = base.FindChild("ImgGroup/GroupPanel/KapaiMode");
            this.GroupPanel = base.FindInChild<UIPanel>("ImgGroup/GroupPanel");
            this.ListGroup = base.FindChild("ImgGroup/GroupPanel/ListMode");
            this.showItems.Add(base.FindChild("show/item1"));
            this.showItems.Add(base.FindChild("show/item2"));
            this.showItems.Add(base.FindChild("show/item3"));
            this.GeneralInfoItemView = base.FindChild("ImgGroup/GroupPanel/item");
            this.dragDropContainer = base.FindInChild<UIDragDropContainer>("ImgGroup");
            this.scrollView = base.FindInChild<UIScrollView>("ImgGroup/GroupPanel");
            this.ImgGroupBoxCollider = base.FindInChild<BoxCollider>("ImgGroup");
            this.backgroundBoxCollider = base.FindInChild<BoxCollider>("Background");
            this.ImgGroupBoxCollider.enabled = false;
            this.AssetCount = 0;
            this.RegisterEventHandler();
            this.initItemView();
        }

        public void InitComplete(GeneralListItemView itemView)
        {
            this.AssetCount++;
            if (this.AssetCount == Singleton<GeneralMode>.Instance.templateInfoList.Count)
            {
                EventDelegate.Add(this.popupList.onChange, new EventDelegate.Callback(this.PopupListChange));
                base.LoadChildComplete();
            }
        }

        private void InitItemByType()
        {
            foreach (GeneralListItemView view in this.allItemList)
            {
                switch (view.generalInfo.genralTemplateInfo.career)
                {
                    case 1:
                        this.attackItemList.Add(view);
                        break;

                    case 2:
                        this.defenseItemList.Add(view);
                        break;

                    case 3:
                        this.magicItemList.Add(view);
                        break;

                    case 4:
                        this.auxiliaryItemList.Add(view);
                        break;
                }
            }
        }

        private void initItemView()
        {
            foreach (GeneralTemplateInfo info in Singleton<GeneralMode>.Instance.templateInfoList)
            {
                this.createItemView(info);
            }
            int num = 0;
            while (num < this.allItemList.Count)
            {
                GeneralListItemView itemView = this.allItemList[num];
                if (itemView.generalTemlateInfo.place > 0)
                {
                    this.UpdateItemList(itemView, false, itemView.generalTemlateInfo.place, true);
                }
                else
                {
                    num++;
                }
            }
            this.updateShowPosition();
            this.UpdateShowBoxCollider(false);
        }

        private void InsertByItemList(List<GeneralListItemView> list, int index, GeneralListItemView item)
        {
            if (index > -1)
            {
                list.Insert(index, item);
            }
            else
            {
                list.Add(item);
            }
        }

        private void OnFingerDown(int fizngerIndex, Vector2 fingerPos)
        {
            this.dragItem = this.PickObject(fingerPos);
        }

        private void OnFingerDragMove(int fingerIndex, Vector2 fingerPos, Vector2 delta)
        {
            if ((this.dragItem != null) && (this.dragItem.transform.parent.name == "KapaiMode"))
            {
                this.totalDelta += (int) delta.x;
                if (Math.Abs(this.totalDelta) >= 50)
                {
                    this.updateState((this.totalDelta >= 0) ? dirMode.left : dirMode.right);
                    this.totalDelta = 0;
                }
            }
        }

        private void OpenExpDrugPanel()
        {
            if (this.currentItemView == null)
            {
                MessageManager.Show("先选择一名英雄");
            }
            else
            {
                Singleton<GeneralMode>.Instance.SendFightGenralList(Singleton<GeneralMode>.Instance.selectedGeneralList, 2);
                this.openExpDragPanel = true;
            }
        }

        public GameObject PickObject(Vector2 screenPos)
        {
            RaycastHit hit;
            Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) screenPos);
            LayerMask mask = 2;
            return (!Physics.Raycast(ray, out hit, (float) mask) ? null : hit.collider.gameObject);
        }

        private void PopupListChange()
        {
            this.UpdateItemActive(false);
            foreach (GeneralListItemView view in this.currentItemList)
            {
                string key = this.popupList.value;
                if (key != null)
                {
                    int num;
                    if (<>f__switch$map13 == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                        dictionary.Add("全部英雄", 0);
                        dictionary.Add("已拥有", 1);
                        dictionary.Add("未拥有", 2);
                        <>f__switch$map13 = dictionary;
                    }
                    if (<>f__switch$map13.TryGetValue(key, out num))
                    {
                        switch (num)
                        {
                            case 0:
                                view.IsDisplay = true;
                                break;

                            case 1:
                                view.IsDisplay = view.generalTemlateInfo.generalInfo != null;
                                break;

                            case 2:
                                view.IsDisplay = view.generalTemlateInfo.generalInfo == null;
                                break;
                        }
                    }
                }
            }
            this.updateModeState();
        }

        private void PopupListOpetion()
        {
            vp_Timer.CancelTimerByHandle(this.timehandle);
            this.dropDown_list = base.FindChild("option_buttons/Drop-down List");
            this.dropDown_list.transform.localPosition = new Vector3(325f, 256f, 0f);
        }

        private void RegisterEventHandler()
        {
            this.close_btn.onClick = new UIWidgetContainer.VoidDelegate(this.ButtonClickHandler);
            this.mode_btn.onClick = new UIWidgetContainer.VoidDelegate(this.ButtonClickHandler);
            this.look_btn.onClick = new UIWidgetContainer.VoidDelegate(this.ButtonClickHandler);
            this.feed_btn.onClick = new UIWidgetContainer.VoidDelegate(this.ButtonClickHandler);
            this.fight_btn.onClick = new UIWidgetContainer.VoidDelegate(this.ButtonClickHandler);
            this.user_general.onClick = new UIWidgetContainer.VoidDelegate(this.ButtonClickHandler);
            Singleton<GeneralMode>.Instance.dataUpdated += new DataUpateHandler(this.DataUpdatedOpenExpDrugPanel);
        }

        private void RegisterFingerGestures()
        {
            this.CancelFingerGestures();
            FingerGestures.OnFingerDown += new FingerGestures.FingerDownEventHandler(this.OnFingerDown);
            FingerGestures.OnFingerDragMove += new FingerGestures.FingerDragMoveEventHandler(this.OnFingerDragMove);
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<GeneralMode>.Instance.dataUpdatedWithParam += new DataUpateHandlerWithParam(this.updatedWithParam);
        }

        private void showKapaiList(int startIndex, int endIndex, List<GeneralListItemView> itemList, bool init = false)
        {
            if (startIndex > 6)
            {
                startIndex -= 6;
            }
            else
            {
                startIndex = 0;
            }
            if (endIndex > (itemList.Count - 1))
            {
                endIndex = itemList.Count - 1;
            }
            while (startIndex <= endIndex)
            {
                if (init)
                {
                    itemList[startIndex].CurrentState = (GeneralListItemView.ItemState) (6 - startIndex);
                }
                else
                {
                    itemList[startIndex].CurrentState = this.getItemState(itemList[startIndex]);
                }
                itemList[startIndex].UpdateItemState();
                startIndex++;
            }
        }

        private void showLineState(GameObject TabBarObject, bool state)
        {
            NGUITools.FindInChild<UISprite>(TabBarObject, "xian").SetActive(state);
            NGUITools.FindInChild<UILabel>(TabBarObject, "Label").color = !state ? ColorConst.FontColor_687687 : ColorConst.FontColor_DFDFC5;
        }

        private void TabBtnClickHandler()
        {
            UIToggle current = UIToggle.current;
            if (current.value && (current != this.curToggle))
            {
                if (this.curToggle != null)
                {
                    this.showLineState(this.curToggle.gameObject, false);
                }
                this.curToggle = current;
                this.showLineState(current.gameObject, true);
                int index = this.tabButtonList.IndexOf(current);
                if (index != this.currentToggleIndex)
                {
                    this.currentToggleIndex = index;
                    switch (index)
                    {
                        case 0:
                            this.currentItemList = this.allItemList;
                            break;

                        case 1:
                            this.currentItemList = this.defenseItemList;
                            break;

                        case 2:
                            this.currentItemList = this.attackItemList;
                            break;

                        case 3:
                            this.currentItemList = this.magicItemList;
                            break;

                        case 4:
                            this.currentItemList = this.auxiliaryItemList;
                            break;
                    }
                    this.PopupListChange();
                }
            }
        }

        public void UpdateBoxCollider(bool isEnabled)
        {
            this.IsDraing = isEnabled;
            this.backgroundBoxCollider.enabled = !isEnabled;
            this.ImgGroupBoxCollider.enabled = isEnabled;
            foreach (GeneralListItemView view in this.allItemList)
            {
                view.boxCollider.enabled = !isEnabled;
            }
            foreach (GeneralListItemView view2 in this.fightItemList)
            {
                view2.boxCollider.enabled = !isEnabled;
            }
            this.UpdateShowBoxCollider(isEnabled);
            if (this.currentMode == ViewMode.ListMode)
            {
                this.scrollView.enabled = !isEnabled;
            }
        }

        private void updatedWithParam(object sender, int code, object param)
        {
            switch (code)
            {
                case 10:
                case 11:
                    this.UpdateItemInfo(false);
                    break;
            }
        }

        public void UpdateFingerGestures(bool isRegister)
        {
            if (this.currentMode == ViewMode.KapaiMode)
            {
                if (isRegister)
                {
                    this.RegisterFingerGestures();
                }
                else
                {
                    this.CancelFingerGestures();
                }
            }
        }

        private void UpdateItemActive(bool isActive)
        {
            this.UpdateSelected(this.currentItemView);
            foreach (GeneralListItemView view in this.allItemList)
            {
                if (this.currentMode == ViewMode.KapaiMode)
                {
                    view.gameObject.SetActive(false);
                    view.GetComponent<UIDragScrollView>().enabled = false;
                    view.transform.parent = this.KapaiGroup.transform;
                    view.CurrentState = GeneralListItemView.ItemState.state13;
                    view.UpdateMode(ViewMode.KapaiMode, GeneralListItemView.ItemState.state13);
                }
                else
                {
                    view.gameObject.SetActive(isActive);
                    view.GetComponent<UIDragScrollView>().enabled = true;
                    view.transform.parent = this.ListGroup.transform;
                    view.UpdateMode(ViewMode.ListMode, GeneralListItemView.ItemState.none);
                }
            }
        }

        private void UpdateItemInfo(bool isInit = false)
        {
            List<GeneralTemplateInfo> templateInfoList = Singleton<GeneralMode>.Instance.templateInfoList;
            foreach (GeneralListItemView view in this.allItemList)
            {
                <UpdateItemInfo>c__AnonStoreyE6 ye = new <UpdateItemInfo>c__AnonStoreyE6 {
                    view = view
                };
                IEnumerator<GeneralTemplateInfo> enumerator = templateInfoList.Where<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ye.<>m__A9)).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        GeneralTemplateInfo current = enumerator.Current;
                        view.generalTemlateInfo = current;
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
            foreach (GeneralListItemView view2 in this.fightItemList)
            {
                <UpdateItemInfo>c__AnonStoreyE7 ye2 = new <UpdateItemInfo>c__AnonStoreyE7 {
                    view = view2
                };
                IEnumerator<GeneralTemplateInfo> enumerator4 = templateInfoList.Where<GeneralTemplateInfo>(new Func<GeneralTemplateInfo, bool>(ye2.<>m__AA)).GetEnumerator();
                try
                {
                    while (enumerator4.MoveNext())
                    {
                        GeneralTemplateInfo info2 = enumerator4.Current;
                        view2.generalTemlateInfo = info2;
                    }
                }
                finally
                {
                    if (enumerator4 == null)
                    {
                    }
                    enumerator4.Dispose();
                }
            }
            if (<>f__am$cache38 == null)
            {
                <>f__am$cache38 = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
            }
            this.allItemList.Sort(<>f__am$cache38);
            if (<>f__am$cache39 == null)
            {
                <>f__am$cache39 = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
            }
            this.attackItemList.Sort(<>f__am$cache39);
            if (<>f__am$cache3A == null)
            {
                <>f__am$cache3A = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
            }
            this.defenseItemList.Sort(<>f__am$cache3A);
            if (<>f__am$cache3B == null)
            {
                <>f__am$cache3B = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
            }
            this.magicItemList.Sort(<>f__am$cache3B);
            if (<>f__am$cache3C == null)
            {
                <>f__am$cache3C = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
            }
            this.auxiliaryItemList.Sort(<>f__am$cache3C);
            this.UpdateItemName();
            if (isInit)
            {
                this.currentItemList = this.allItemList;
            }
            this.PopupListChange();
        }

        public void UpdateItemList(GeneralListItemView itemView, bool isAdd, int index = 0, bool isInit = false)
        {
            if (isAdd)
            {
                this.fightItemList.Remove(itemView);
                Singleton<GeneralMode>.Instance.RemoveSelectedGeneral(itemView.generalInfo);
                int itemIndex = itemView.getIndexByDictionary(0);
                this.InsertByItemList(this.allItemList, this.getInsertByIndex(this.allItemList, itemIndex), itemView);
                if (<>f__am$cache2F == null)
                {
                    <>f__am$cache2F = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
                }
                this.allItemList.Sort(<>f__am$cache2F);
                switch (itemView.generalInfo.genralTemplateInfo.career)
                {
                    case 1:
                        itemIndex = itemView.getIndexByDictionary(1);
                        this.InsertByItemList(this.attackItemList, this.getInsertByIndex(this.attackItemList, itemIndex), itemView);
                        if (<>f__am$cache30 == null)
                        {
                            <>f__am$cache30 = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
                        }
                        this.attackItemList.Sort(<>f__am$cache30);
                        break;

                    case 2:
                        itemIndex = itemView.getIndexByDictionary(2);
                        this.InsertByItemList(this.defenseItemList, this.getInsertByIndex(this.defenseItemList, itemIndex), itemView);
                        if (<>f__am$cache31 == null)
                        {
                            <>f__am$cache31 = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
                        }
                        this.defenseItemList.Sort(<>f__am$cache31);
                        break;

                    case 3:
                        itemIndex = itemView.getIndexByDictionary(3);
                        this.InsertByItemList(this.magicItemList, this.getInsertByIndex(this.magicItemList, itemIndex), itemView);
                        if (<>f__am$cache32 == null)
                        {
                            <>f__am$cache32 = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
                        }
                        this.magicItemList.Sort(<>f__am$cache32);
                        break;

                    case 4:
                        itemIndex = itemView.getIndexByDictionary(4);
                        this.InsertByItemList(this.auxiliaryItemList, this.getInsertByIndex(this.auxiliaryItemList, itemIndex), itemView);
                        if (<>f__am$cache33 == null)
                        {
                            <>f__am$cache33 = (x, y) => -x.generalTemlateInfo.listIndex.CompareTo(y.generalTemlateInfo.listIndex);
                        }
                        this.auxiliaryItemList.Sort(<>f__am$cache33);
                        break;
                }
            }
            else
            {
                if (index >= this.fightItemList.Count)
                {
                    this.fightItemList.Add(itemView);
                }
                else
                {
                    this.fightItemList.Insert(index, itemView);
                }
                Singleton<GeneralMode>.Instance.AddSelectedGeneral(itemView.generalInfo, index);
                this.allItemList.Remove(itemView);
                switch (itemView.generalInfo.genralTemplateInfo.career)
                {
                    case 1:
                        this.attackItemList.Remove(itemView);
                        break;

                    case 2:
                        this.defenseItemList.Remove(itemView);
                        break;

                    case 3:
                        this.magicItemList.Remove(itemView);
                        break;

                    case 4:
                        this.auxiliaryItemList.Remove(itemView);
                        break;
                }
            }
            if (!isInit)
            {
                if ((this.currentItemView != null) && (this.currentItemView.generalInfo.id == itemView.generalInfo.id))
                {
                    this.UpdateSelected(itemView);
                }
                this.PopupListChange();
            }
        }

        private void UpdateItemName()
        {
            for (int i = 0; i < this.allItemList.Count; i++)
            {
                this.allItemList[i].name = "generalItem_" + i;
            }
        }

        public void UpdateItemPlace(GeneralListItemView itemView, bool isAdd, int index = 0)
        {
            if (isAdd)
            {
                Singleton<GeneralMode>.Instance.RemoveSelectedGeneral(itemView.generalInfo);
            }
            else
            {
                Singleton<GeneralMode>.Instance.AddSelectedGeneral(itemView.generalInfo, index);
            }
        }

        private void updateModeState()
        {
            this.GroupPanel.gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
            this.GroupPanel.clipOffset = new Vector2(0f, 0f);
            if (this.currentMode == ViewMode.ListMode)
            {
                this.CancelFingerGestures();
                this.modeLabel.text = "卡牌模式";
                this.scrollView.enabled = true;
                this.changeModeByList();
            }
            else
            {
                this.RegisterFingerGestures();
                this.modeLabel.text = "列表模式";
                this.scrollView.enabled = false;
                this.changeModeByKapai();
            }
        }

        public void UpdateSelected(GeneralListItemView itemView)
        {
            if (itemView != null)
            {
                if (this.currentItemView != null)
                {
                    this.currentItemView.updateSelected(false);
                }
                this.currentItemView = itemView;
                this.currentItemView.updateSelected(true);
                this.fightLabel.text = (itemView.generalInfo.place <= 0) ? "出战" : "休息";
                if (this.guideHeroDelegate != null)
                {
                    this.guideHeroDelegate();
                    this.guideHeroDelegate = null;
                }
            }
        }

        public void UpdateShowBoxCollider(bool isEnabled = false)
        {
            foreach (GameObject obj2 in this.showItems)
            {
                if (obj2.transform.childCount >= 1)
                {
                    obj2.transform.GetChild(0).GetComponent<UIPanel>().depth = 100;
                }
                if (isEnabled)
                {
                    obj2.GetComponent<BoxCollider>().enabled = true;
                }
                else
                {
                    obj2.GetComponent<BoxCollider>().enabled = obj2.transform.childCount < 1;
                }
            }
        }

        private void UpdateShowItemMode()
        {
            foreach (GeneralListItemView view in this.fightItemList)
            {
                view.currentMode = this.currentMode;
            }
        }

        private void updateShowPosition()
        {
            foreach (GeneralListItemView view in this.fightItemList)
            {
                view.setShowMode(true);
                view.transform.parent = this.showItems[view.generalInfo.place - 1].transform;
                view.transform.localPosition = new Vector3(0f, 0f, 0f);
                view.transform.localScale = new Vector3(1f, 1f, 1f);
                view.CurrentState = GeneralListItemView.ItemState.show;
            }
        }

        private void updateState(dirMode dir)
        {
            int num2;
            this.currentDir = dir;
            int currentIndex = this.currentIndex;
            if (<>f__am$cache37 == null)
            {
                <>f__am$cache37 = itemView => itemView.IsDisplay;
            }
            List<GeneralListItemView> itemList = this.currentItemList.Where<GeneralListItemView>(<>f__am$cache37).ToList<GeneralListItemView>();
            dirMode mode = dir;
            if (mode != dirMode.right)
            {
                if ((mode == dirMode.left) && (this.currentIndex >= 1))
                {
                    num2 = this.currentIndex + 6;
                    this.currentIndex--;
                    this.showKapaiList(currentIndex, num2, itemList, false);
                }
            }
            else if (this.currentIndex < (itemList.Count - 1))
            {
                num2 = this.currentIndex + 6;
                this.currentIndex++;
                this.showKapaiList(currentIndex, num2, itemList, false);
            }
        }

        public Button Close_btn
        {
            get
            {
                return this.close_btn;
            }
        }

        public Button Fight_btn
        {
            get
            {
                return this.fight_btn;
            }
        }

        public bool IsDraing { get; set; }

        public override bool IsLoadChild
        {
            get
            {
                return true;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/General/GeneralPanel.assetbundle";
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateItemInfo>c__AnonStoreyE6
        {
            internal GeneralListItemView view;

            internal bool <>m__A9(GeneralTemplateInfo templateInfo)
            {
                return ((this.view.generalTemlateInfo.Id == templateInfo.Id) && (templateInfo.generalInfo != null));
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateItemInfo>c__AnonStoreyE7
        {
            internal GeneralListItemView view;

            internal bool <>m__AA(GeneralTemplateInfo info)
            {
                return (this.view.generalTemlateInfo.Id == info.Id);
            }
        }

        private enum dirMode
        {
            none,
            right,
            left
        }

        public enum ViewMode
        {
            KapaiMode = 1,
            ListMode = 2
        }
    }
}

