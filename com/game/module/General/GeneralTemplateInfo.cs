﻿namespace com.game.module.General
{
    using com.game.data;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class GeneralTemplateInfo
    {
        private PNewAttr _cacheAttr;
        private GeneralInfo _generalInfo;
        private PNewAttr _newAttr;
        private SysGeneralVo _sysGeneralVo;
        public uint Id;
        public int place;
        public readonly List<uint> skillList = new List<uint>();

        private void setSkill()
        {
            string[] valueListFromString = StringUtils.GetValueListFromString(this.sysGeneralVo.skill_list, ',');
            for (int i = 0; i < valueListFromString.Length; i++)
            {
                valueListFromString[i] = valueListFromString[i] + "001";
                this.skillList.Add(uint.Parse(valueListFromString[i]));
            }
        }

        public PNewAttr GeneralAttr
        {
            get
            {
                return this._newAttr;
            }
            set
            {
                this._newAttr = value;
            }
        }

        public PNewAttr GeneralCacheAttr
        {
            get
            {
                return this._cacheAttr;
            }
            set
            {
                this._cacheAttr = value;
            }
        }

        public GeneralInfo generalInfo
        {
            get
            {
                return this._generalInfo;
            }
            set
            {
                this._generalInfo = value;
            }
        }

        public uint listIndex
        {
            get
            {
                return ((this.generalInfo == null) ? 0 : this.generalInfo.fightPoint);
            }
        }

        public SysGeneralVo sysGeneralVo
        {
            get
            {
                return this._sysGeneralVo;
            }
            set
            {
                this._sysGeneralVo = value;
                this.setSkill();
            }
        }
    }
}

