﻿namespace com.game.module.GoldBox
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class GoldBoxControl : BaseControl<GoldBoxControl>
    {
        private void Fun_3_42(INetData data)
        {
            RoleGoldBuyInfoMsg_3_42 msg = new RoleGoldBuyInfoMsg_3_42();
            msg.read(data.GetMemoryStream());
            Singleton<GoldBoxMode>.Instance.SetGoldBuyInfo(msg);
        }

        private void Fun_3_43(INetData data)
        {
            RoleGoldBuyMsg_3_43 g__ = new RoleGoldBuyMsg_3_43();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<GoldBoxMode>.Instance.HandleBuySuc(g__.type);
                SysVipBoxVo vipBox = BaseDataMgr.instance.GetVipBox(Singleton<GoldBoxMode>.Instance.BuyTimesUsed);
                int num = vipBox.gold_ratio * g__.type;
                MessageManager.Show(LanguageManager.GetWord("GoldBox.SucToBuyGold", num.ToString()));
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("810", new NetMsgCallback(this.Fun_3_42));
            AppNet.main.addCMD("811", new NetMsgCallback(this.Fun_3_43));
        }
    }
}

