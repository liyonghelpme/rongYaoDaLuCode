﻿namespace com.game.module.GoldBox
{
    using com.game;
    using com.game.module.core;
    using com.game.utils;
    using Proto;
    using System;
    using System.IO;

    public class GoldBoxMode : BaseMode<GoldBoxMode>
    {
        private ushort buyTimesUsed;
        private ushort diamNeedsForBatch;
        private ushort diamNeedsForOnce;
        private byte goldGetType;
        private uint goldNumNextBuy;
        public const int UPDATE_GOLD_GET_TYPE = 2;
        public const int UPDATE_GOLD_INFO = 1;

        private void BuyGold(byte value)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_43(msdata, value);
            AppNet.gameNet.send(msdata, 3, 0x2b);
        }

        public void BuyGoldBatch()
        {
            this.BuyGold(1);
        }

        public void BuyGoldOnce()
        {
            this.BuyGold(0);
        }

        public void GetGoldBuyInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_42(msdata);
            AppNet.gameNet.send(msdata, 3, 0x2a);
        }

        public void HandleBuySuc(byte type)
        {
            this.buyTimesUsed = (ushort) (this.buyTimesUsed + 1);
            base.DataUpdate(1);
        }

        public void SetGoldBuyInfo(RoleGoldBuyInfoMsg_3_42 msg)
        {
            this.buyTimesUsed = msg.times;
            this.diamNeedsForOnce = msg.diam;
            this.goldNumNextBuy = msg.gold;
            this.diamNeedsForBatch = msg.batchDiam;
            base.DataUpdate(1);
        }

        public ushort BuyTimesUsed
        {
            get
            {
                return this.buyTimesUsed;
            }
        }

        public ushort DiamNeedsForBatch
        {
            get
            {
                return this.diamNeedsForBatch;
            }
        }

        public ushort DiamNeedsForOnce
        {
            get
            {
                return this.diamNeedsForOnce;
            }
        }

        public byte GoldGetType
        {
            get
            {
                return this.goldGetType;
            }
            set
            {
                this.goldGetType = value;
                base.DataUpdate(2);
            }
        }

        public uint GoldNumNextBuy
        {
            get
            {
                return this.goldNumNextBuy;
            }
        }

        public int RemainBuyTimes
        {
            get
            {
                return (VarMgr.numBuyGoldMax - this.buyTimesUsed);
            }
        }
    }
}

