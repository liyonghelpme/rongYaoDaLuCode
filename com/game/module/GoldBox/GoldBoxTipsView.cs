﻿namespace com.game.module.GoldBox
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GoldBoxTipsView : BaseView<GoldBoxTipsView>
    {
        private bool batchBuy;
        private Button btnCancel;
        public Button btnOk;
        private CloseCallBack closeCallBack;
        private UILabel labDiamond;
        private UILabel labGold;
        private UILabel labNextHide;
        private UILabel labOpenCnt;
        private UILabel labWaste;
        private bool needHide;
        private UIToggle togNextHide;

        private void CancelOnClick(GameObject go)
        {
            this.CloseView();
        }

        public override void CloseView()
        {
            this.needHide = this.togNextHide.value;
            if (this.closeCallBack != null)
            {
                this.closeCallBack(this.panelType);
            }
            base.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            ushort num2;
            ushort diamNeedsForBatch;
            base.HandleAfterOpenView();
            ushort num = 10;
            if (this.batchBuy)
            {
                ushort remainBuyTimes = (ushort) Singleton<GoldBoxMode>.Instance.RemainBuyTimes;
                num2 = (remainBuyTimes <= num) ? remainBuyTimes : num;
                diamNeedsForBatch = Singleton<GoldBoxMode>.Instance.DiamNeedsForBatch;
            }
            else
            {
                num2 = 1;
                diamNeedsForBatch = Singleton<GoldBoxMode>.Instance.DiamNeedsForOnce;
            }
            uint goldNumNextBuy = Singleton<GoldBoxMode>.Instance.GoldNumNextBuy;
            string[] param = new string[] { num2.ToString() };
            string[] strArray2 = new string[] { diamNeedsForBatch.ToString(), goldNumNextBuy.ToString() };
            this.labOpenCnt.text = LanguageManager.GetWord("GoldBoxTipsView.OpenCount", param);
            this.labWaste.text = LanguageManager.GetWord("GoldBoxTipsView.Waste", strArray2);
            this.labDiamond.text = strArray2[0];
            this.labGold.text = strArray2[1];
        }

        protected override void Init()
        {
            this.btnOk = base.FindInChild<Button>("center/btnOk");
            this.btnCancel = base.FindInChild<Button>("center/btnCancel");
            this.togNextHide = base.FindInChild<UIToggle>("center/chkNextTips");
            this.labOpenCnt = base.FindInChild<UILabel>("center/kq");
            this.labWaste = base.FindInChild<UILabel>("center/xh");
            this.labNextHide = base.FindInChild<UILabel>("center/chkNextTips/Label");
            this.labDiamond = base.FindInChild<UILabel>("center/waste/diamond/shuzi");
            this.labGold = base.FindInChild<UILabel>("center/waste/gold/shuzi");
            this.btnOk.onClick = new UIWidgetContainer.VoidDelegate(this.OkOnClick);
            this.btnCancel.onClick = new UIWidgetContainer.VoidDelegate(this.CancelOnClick);
            this.togNextHide.value = this.needHide;
            this.InitLabel();
        }

        private void InitLabel()
        {
            this.labNextHide.text = LanguageManager.GetWord("GoldBoxTipsView.NextHide");
            this.btnOk.label.text = LanguageManager.GetWord("GoldBoxTipsView.Ok");
            this.btnCancel.label.text = LanguageManager.GetWord("GoldBoxTipsView.Cancel");
        }

        private void OkOnClick(GameObject go)
        {
            if (this.batchBuy)
            {
                Singleton<GoldBoxMode>.Instance.BuyGoldBatch();
            }
            else
            {
                Singleton<GoldBoxMode>.Instance.BuyGoldOnce();
            }
            this.CloseView();
        }

        public void ShowWindow(CloseCallBack callBack, bool batchBuy = false)
        {
            this.closeCallBack = callBack;
            this.batchBuy = batchBuy;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public bool NeedHide
        {
            get
            {
                return this.needHide;
            }
        }

        public override string url
        {
            get
            {
                return "UI/GoldBox/GoldBoxTipsView.assetbundle";
            }
        }
    }
}

