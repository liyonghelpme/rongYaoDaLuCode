﻿namespace com.game.module.GoldBox
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using com.game.vo;
    using System;
    using UnityEngine;

    public class GoldBoxView : BaseView<GoldBoxView>
    {
        private UIWidgetContainer _bgHot;
        private Button _btnCancel;
        private Button _btnClose;
        private Button _btnUse;
        private UILabel _lblCount;
        private UILabel _lblGold;
        private UILabel _lblStone;
        private SysVipBoxVo _vo;

        protected override void HandleAfterOpenView()
        {
            this.UpdateView();
            this._bgHot.onClick = this._btnCancel.onClick = this._btnClose.onClick = this._btnUse.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            Singleton<GoldBoxMode>.Instance.dataUpdated += new DataUpateHandler(this.OnGoldBoxEvent);
            Singleton<VIPMode>.Instance.dataUpdated += new DataUpateHandler(this.OnVipEvent);
        }

        protected override void HandleBeforeCloseView()
        {
            this._btnCancel.onClick = this._btnClose.onClick = (UIWidgetContainer.VoidDelegate) (this._btnUse.onClick = null);
            Singleton<GoldBoxMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnGoldBoxEvent);
            Singleton<VIPMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnVipEvent);
        }

        protected override void Init()
        {
            this._bgHot = base.FindInChild<UIWidgetContainer>("mask");
            this._lblCount = base.FindInChild<UILabel>("up/countLabel");
            this._lblStone = base.FindInChild<UILabel>("center/stone/label");
            this._lblGold = base.FindInChild<UILabel>("center/gold/label");
            this._btnCancel = base.FindInChild<Button>("btns/btnCancel");
            this._btnClose = base.FindInChild<Button>("btns/btnClose");
            this._btnUse = base.FindInChild<Button>("btns/btnUse");
        }

        private void OnClickBtn(GameObject go)
        {
            if (((go.name == this._bgHot.name) || (go.name == this._btnCancel.name)) || (go.name == this._btnClose.name))
            {
                this.CloseView();
            }
            else if (go.name == this._btnUse.name)
            {
                if (Singleton<GoldBoxMode>.Instance.RemainBuyTimes <= 0)
                {
                    Singleton<AlertPanel>.Instance.Show(LanguageManager.GetWord("GoldBox.CountOut"), delegate {
                        this.CloseView();
                        Singleton<VIPView>.Instance.OpenView();
                    }, null);
                }
                else if (MeVo.instance.diamond < this._vo.diam)
                {
                    Singleton<AlertPanel>.Instance.Show(LanguageManager.GetWord("Game.SureToFill"), delegate {
                        this.CloseView();
                        Singleton<VIPView>.Instance.OpenView();
                    }, null);
                }
                else
                {
                    Singleton<GoldBoxMode>.Instance.BuyGoldOnce();
                }
            }
        }

        private void OnGoldBoxEvent(object sender, int code)
        {
            if (code == 1)
            {
                this.UpdateView();
            }
        }

        private void OnVipEvent(object send, int code)
        {
            this.UpdateView();
        }

        private void UpdateView()
        {
            string[] param = new string[] { Singleton<GoldBoxMode>.Instance.RemainBuyTimes.ToString(), VarMgr.numBuyGoldMax.ToString() };
            this._lblCount.text = LanguageManager.GetWord("GoldBox.LeftCount", param);
            this._vo = BaseDataMgr.instance.GetVipBox(Singleton<GoldBoxMode>.Instance.BuyTimesUsed + 1);
            if (this._vo == null)
            {
                this._vo = BaseDataMgr.instance.GetVipBox(Singleton<GoldBoxMode>.Instance.BuyTimesUsed);
            }
            this._lblStone.text = this._vo.diam.ToString();
            this._lblGold.text = this._vo.gold_ratio.ToString();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/GoldBox/GoldBoxView.assetbundle";
            }
        }
    }
}

