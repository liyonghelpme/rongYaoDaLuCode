﻿namespace com.game.module.GoldBox
{
    using System;

    public enum GoldGetType
    {
        LargeCrit = 5,
        Normal = 1,
        SmallCrit = 2
    }
}

