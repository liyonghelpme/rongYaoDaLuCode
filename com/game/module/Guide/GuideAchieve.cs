﻿namespace com.game.module.Guide
{
    using com.game.module.Achievement;
    using com.game.module.core;
    using System;

    public class GuideAchieve : GuideBase
    {
        private int _guideId;

        public GuideAchieve(int guideId)
        {
            this._guideId = guideId;
        }

        private void BeginGuide()
        {
            this.CloseView();
            if (!Singleton<MainBottomRightView>.Instance.isOpen)
            {
                this.GuideOpenBtn();
            }
            else
            {
                this.GuideAchieveBtn();
            }
        }

        public override void BeginGuide(GuideBase.GuideFinishCallBack cb)
        {
            base.BeginGuide(cb);
            if ((Singleton<MainView>.Instance.gameObject != null) && Singleton<MainView>.Instance.IsOpened)
            {
                this.BeginGuide();
            }
            else
            {
                Singleton<MainView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
                Singleton<MainView>.Instance.OpenView();
            }
        }

        private void CloseGuide()
        {
            base.FinishGuide();
            Singleton<MainView>.Instance.AfterOpenGuideDelegate = null;
        }

        private void CloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void GuideAchieveBtn()
        {
            base.SetCurrentGuideButton(Singleton<MainBottomRightView>.Instance.AchieveButton, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击成就按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            Singleton<AchievementMainPanel>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.WaitGuideGetAward);
        }

        private void GuideAchieveOver()
        {
            this.CloseView();
            this.CloseGuide();
        }

        private void GuideGetAward()
        {
            Singleton<AchievementMode>.Instance.guideAchieve = new com.game.module.Achievement.GuideAchieve(this.GuideAchieveOver);
            this.CloseView();
            base.SetCurrentGuideButton(Singleton<AchievementMainPanel>.Instance.ItemViewList[0].GetBtn, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“领取”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void GuideOpenBtn()
        {
            base.SetCurrentGuideButton(Singleton<MainBottomRightView>.Instance.OpenButton, new UIWidgetContainer.GuideDelegate(this.CloseView), new UIWidgetContainer.GuideDelegate(this.WaitGuideAchieveBtn));
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "展开操作菜单", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void WaitGuideAchieveBtn()
        {
            vp_Timer.In(1f, new vp_Timer.Callback(this.GuideAchieveBtn), null);
        }

        private void WaitGuideGetAward()
        {
            vp_Timer.In(0.5f, new vp_Timer.Callback(this.GuideGetAward), null);
        }
    }
}

