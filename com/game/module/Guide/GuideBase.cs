﻿namespace com.game.module.Guide
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuideBase
    {
        private vp_Timer.Callback _clickMainKongZhiBtnCallback;
        private bool _isFunctionOpen;
        protected UIWidgetContainer CurrentGuideButton;
        public GuideFinishCallBack guideFinishCallBack;

        public virtual void BeginGuide()
        {
        }

        public virtual void BeginGuide(GuideFinishCallBack cb)
        {
            this.guideFinishCallBack = cb;
        }

        public void FinishGuide()
        {
            if (this.guideFinishCallBack != null)
            {
                this.guideFinishCallBack();
            }
        }

        private void HideGuideButton()
        {
            int childCount = this.CurrentGuideButton.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                this.CurrentGuideButton.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        public void RemoveToggleDelegate(UIToggle currentGuideToggle, EventDelegate.Callback clickCallback)
        {
            EventDelegate.Remove(currentGuideToggle.onChange, clickCallback);
        }

        protected void SetCurrentGuideButton(UIWidgetContainer currentGuideButton, UIWidgetContainer.GuideDelegate beforeClickGuideButtonDelegate = null, UIWidgetContainer.GuideDelegate afterClickGuideButtonDelegate = null)
        {
            this.CurrentGuideButton = currentGuideButton;
            this.CurrentGuideButton.guideBeforeClickDelegate = beforeClickGuideButtonDelegate;
            this.CurrentGuideButton.guideAfterClickDelegate = afterClickGuideButtonDelegate;
        }

        protected void SetCurrentGuideToggle(UIToggle currentGuideToggle, EventDelegate.Callback clickCallback)
        {
            this.CurrentGuideButton = currentGuideToggle;
            EventDelegate.Add(currentGuideToggle.onChange, clickCallback);
        }

        public delegate void GuideFinishCallBack();
    }
}

