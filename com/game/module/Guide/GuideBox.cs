﻿namespace com.game.module.Guide
{
    using com.game.module.core;
    using System;

    internal class GuideBox : GuideBase
    {
        private void AfterOpenBaoXiangGetItemView()
        {
            base.SetCurrentGuideButton(Singleton<BaoxiangGetItemView>.Instance.OkBtn, new UIWidgetContainer.GuideDelegate(this.CloseView), new UIWidgetContainer.GuideDelegate(this.GuideCloseBtn));
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“确定”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void AfterOpenBaoXiangView()
        {
            base.SetCurrentGuideButton(Singleton<BaoxiangSelectView>.Instance.HjBoxBtn, new UIWidgetContainer.GuideDelegate(this.GuideBuyOne), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“查看”", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void BeginGuide()
        {
            base.SetCurrentGuideButton(Singleton<MainTopRightView>.Instance.LuckBtn, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“抽奖”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            Singleton<BaoxiangSelectView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.AfterOpenBaoXiangView);
        }

        public override void BeginGuide(GuideBase.GuideFinishCallBack cb)
        {
            base.BeginGuide(cb);
            Singleton<MainView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
        }

        private void CloseGuide()
        {
            base.FinishGuide();
        }

        private void CloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void GuideBuyOne()
        {
            this.CloseView();
            vp_Timer.In(0.5f, new vp_Timer.Callback(this.GuideBuyOneAfter), null);
        }

        private void GuideBuyOneAfter()
        {
            Singleton<BaoxiangGetItemView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.AfterOpenBaoXiangGetItemView);
            base.SetCurrentGuideButton(Singleton<BaoxiangSelectView>.Instance.HjBvBuy1Btn, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "购买一个黄金宝箱", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void GuideCloseBtn()
        {
            base.SetCurrentGuideButton(Singleton<BaoxiangSystem>.Instance.CloseBtn, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“关闭”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            this.CloseGuide();
        }
    }
}

