﻿namespace com.game.module.Guide
{
    using com.game;
    using com.game.module.core;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class GuideControl : BaseControl<GuideControl>
    {
        private void Fun_40_1(INetData data)
        {
            GuideTriggerMsg_40_1 g__ = new GuideTriggerMsg_40_1();
            g__.read(data.GetMemoryStream());
            this.StartGuide(g__.guideId);
        }

        public void GuideEvent(byte eventType)
        {
            Singleton<GuideMode>.Instance.GuideEvent(eventType);
        }

        public void GuideFinish(byte guideId)
        {
            Singleton<GuideMode>.Instance.GuideFinish(guideId);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("10241", new NetMsgCallback(this.Fun_40_1));
        }

        public void StartGuide(int guideId)
        {
            GuideBase guideLogic = Singleton<GuideMode>.Instance.GetGuideLogic(guideId);
            Singleton<GuideMode>.Instance.CurrentGuideId = guideId;
            guideLogic.BeginGuide();
        }
    }
}

