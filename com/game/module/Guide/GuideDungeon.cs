﻿namespace com.game.module.Guide
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class GuideDungeon : GuideBase
    {
        private int _dungeonId;
        private int _guideId;

        public GuideDungeon(int dungeonId, int guideId)
        {
            this._dungeonId = dungeonId;
            this._guideId = guideId;
        }

        private void AfterClickCorrectDungeonButton()
        {
            Singleton<GuideView>.Instance.CloseView();
            vp_Timer.In(0.2f, new vp_Timer.Callback(this.AfterClickCorrectDungeonButton_1), null);
        }

        private void AfterClickCorrectDungeonButton_1()
        {
            Singleton<DungeonInfoView>.Instance.BackButton.guideBeforeClickDelegate = new UIWidgetContainer.GuideDelegate(this.AfterOpenDungeonView);
            base.SetCurrentGuideButton(Singleton<DungeonInfoView>.Instance.StartButton, new UIWidgetContainer.GuideDelegate(this.AfterClickEnter), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“闯关”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void AfterClickEnter()
        {
            Singleton<DungeonChapterView>.Instance.RecoverGuideDelegate = null;
            Singleton<GuideView>.Instance.CloseView();
            Singleton<DungeonAwardView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.QuitFinshView);
            Singleton<DungeonFailureView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.FailureRepeatGuide);
        }

        private void AfterOpenDungeonView()
        {
            Singleton<DungeonChapterView>.Instance.RecoverGuideDelegate = new RecoverGuideDelegate(this.RepeatGuide);
            vp_Timer.In(0.2f, new vp_Timer.Callback(this.GuideChooseDungeon), null);
        }

        private void BeginGuide()
        {
            base.SetCurrentGuideButton(Singleton<MainTopRightView>.Instance.DungeonButton, new UIWidgetContainer.GuideDelegate(this.GuideOpenClickDungeonButton), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“副本”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            Singleton<DungeonChapterView>.Instance.AfterOpenViewGuideDelegate = new EventDelegate.Callback(this.AfterOpenDungeonView);
        }

        public override void BeginGuide(GuideBase.GuideFinishCallBack cb)
        {
            Debug.Log("GuideDungeon:44444");
            base.BeginGuide(cb);
            if ((Singleton<MainView>.Instance.gameObject != null) && Singleton<MainView>.Instance.IsOpened)
            {
                this.BeginGuide();
            }
            else
            {
                Singleton<MainView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
                Singleton<MainView>.Instance.OpenView();
            }
        }

        private void ClearDungeonGuide()
        {
            Singleton<GuideView>.Instance.CloseView();
        }

        private void EndGuide()
        {
            Singleton<DungeonInfoView>.Instance.BackButton.guideBeforeClickDelegate = null;
            Singleton<DungeonChapterView>.Instance.RecoverGuideDelegate = null;
            Singleton<DungeonAwardView>.Instance.AfterOpenGuideDelegate = null;
            base.FinishGuide();
        }

        private void FailureRepeatGuide()
        {
            base.SetCurrentGuideButton(Singleton<DungeonFailureView>.Instance.QuipBtn, new UIWidgetContainer.GuideDelegate(this.ClearDungeonGuide), new UIWidgetContainer.GuideDelegate(this.RepeatGuide));
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“离开”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private string getDungeonTip()
        {
            SysDungeonVo vo = BaseDataMgr.instance.getDungeon((uint) this._dungeonId);
            return ("选择" + vo.name);
        }

        private void GuideChooseDungeon()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
            Singleton<DungeonChapterView>.Instance.AfterOpenViewGuideDelegate = null;
            foreach (DungeonChapterItemView view in Singleton<DungeonChapterView>.Instance.Items)
            {
                Button btn = view.Btn;
                if (view.dungeonId == this._dungeonId)
                {
                    base.SetCurrentGuideButton(btn, new UIWidgetContainer.GuideDelegate(this.AfterClickCorrectDungeonButton), null);
                }
                else
                {
                    btn.guideBeforeClickDelegate = new UIWidgetContainer.GuideDelegate(this.ClearDungeonGuide);
                }
            }
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, this.getDungeonTip(), GuideShowType.Left, ViewLayer.TopUILayer, null);
        }

        private void GuideCloseBtn()
        {
            base.SetCurrentGuideButton(Singleton<DungeonChapterView>.Instance.CloseBtn, new UIWidgetContainer.GuideDelegate(this.ClearDungeonGuide), new UIWidgetContainer.GuideDelegate(this.EndGuide));
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“关闭”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void GuideOpenClickDungeonButton()
        {
            Singleton<GuideView>.Instance.CloseView();
        }

        private void QuitFinshView()
        {
            base.SetCurrentGuideButton(Singleton<DungeonAwardView>.Instance.QuitBtn, new UIWidgetContainer.GuideDelegate(this.ClearDungeonGuide), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“离开”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            Singleton<DungeonChapterView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.GuideCloseBtn);
        }

        private void RepeatGuide()
        {
            Singleton<GuideView>.Instance.CloseView();
            this.BeginGuide();
        }
    }
}

