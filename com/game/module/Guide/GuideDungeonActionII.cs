﻿namespace com.game.module.Guide
{
    using com.game;
    using com.game.basic;
    using com.game.basic.events;
    using com.game.module.core;
    using com.game.module.effect;
    using com.game.module.fight.arpg;
    using com.game.module.map;
    using com.game.vo;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.joystick;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class GuideDungeonActionII : GuideBase
    {
        private int _currentStep;
        private int _index;
        private Vector3 checkPos;
        private EffectControlerII eControler;
        private uint len = 1;
        private string moveEffectId = "10051";
        private int moveStep = 1;
        private Dictionary<int, Vector3> posDic = new Dictionary<int, Vector3>();

        public override void BeginGuide()
        {
            Singleton<BattleView>.Instance.RecoverGuideDelegate = new RecoverGuideDelegate(this.CloseGuide);
            foreach (Button button in Singleton<BattleBottomRightView>.Instance.SkillButtonList)
            {
                button.SetActive(false);
            }
            this.InitMovePoint();
            this.GuideJoyStick();
            Singleton<BattleTopLeftView>.Instance.HideAutoAttackBtn(false);
        }

        private void CheckMove(Vector3 pos)
        {
            if (this.posDic.ContainsKey(this.moveStep))
            {
                if (Vector3.Distance(pos, this.posDic[this.moveStep]) <= this.len)
                {
                    this.eControler.Clear();
                    this.moveStep++;
                    if (this.posDic.ContainsKey(this.moveStep))
                    {
                        EffectMgr.Instance.CreateStaticBuffEffect(this.moveEffectId, this.posDic[this.moveStep], new com.game.module.effect.Effect.EffectCreateCallback(this.CreateEffectCallBack), null, true);
                    }
                }
            }
            else
            {
                this.GuideAttack();
                Singleton<MonsterMgr>.Instance.moveMoveToCallBack = null;
            }
        }

        private void CloseGuide()
        {
            Singleton<BattleView>.Instance.RecoverGuideDelegate = null;
            Singleton<GuideView>.Instance.CloseView();
        }

        private void CloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void CreateEffectCallBack(EffectControlerII effControler)
        {
            this.eControler = effControler;
        }

        private void GuideAttack()
        {
            this._currentStep = 1;
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
            Singleton<MonsterActivator>.Instance.mobCache.NextStage();
            base.SetCurrentGuideButton(Singleton<BattleBottomRightView>.Instance.AttackButton, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "试试普通攻击", GuideShowType.Right, ViewLayer.LowLayer, null);
            GlobalAPI.facade.Add(1, new NoticeListener(this.OnKillMonster));
        }

        private void GuideEnd()
        {
            Singleton<BattleView>.Instance.RecoverGuideDelegate = null;
            GlobalAPI.facade.Remove(1, new NoticeListener(this.OnKillMonster));
            Singleton<GuideView>.Instance.CloseView();
            for (int i = 0; i <= 2; i++)
            {
                Singleton<BattleBottomRightView>.Instance.SkillButtonList[i].SetActive(true);
            }
        }

        private void GuideJoyStick()
        {
            Singleton<GuideView>.Instance.OpenGuide(Singleton<BattleBottomLeftView>.Instance.Joystick.transform, "操作遥感移动角色", GuideShowType.Joystick, ViewLayer.LowLayer, null);
            EffectMgr.Instance.CreateStaticBuffEffect(this.moveEffectId, this.posDic[this.moveStep], new com.game.module.effect.Effect.EffectCreateCallback(this.CreateEffectCallBack), null, true);
            Singleton<MonsterMgr>.Instance.moveMoveToCallBack = new MonsterMgr.MoveToCallBack(this.CheckMove);
            JoystickController.instance.guideDungeonCallBack = new JoystickController.GuideDungeon(this.JoyStickCloseView);
        }

        private void GuideMeetBoss()
        {
            Singleton<BattleBottomRightView>.Instance.SkillButtonList[0].SetActive(true);
            this._currentStep = 3;
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void GuideSkill_1()
        {
            Singleton<MonsterActivator>.Instance.mobCache.NextStage();
            Button currentGuideButton = Singleton<BattleBottomRightView>.Instance.SkillButtonList[0];
            currentGuideButton.SetActive(true);
            base.SetCurrentGuideButton(currentGuideButton, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenFlyButtonGuide(base.CurrentGuideButton.transform, "试试你的新技能", ViewLayer.LowLayer, true);
            Singleton<MonsterMgr>.Instance.ShowMonsterByGroupId(2);
        }

        private void GuideSkill_2()
        {
            Singleton<MonsterActivator>.Instance.mobCache.NextStage();
            Button currentGuideButton = Singleton<BattleBottomRightView>.Instance.SkillButtonList[1];
            currentGuideButton.SetActive(true);
            base.SetCurrentGuideButton(currentGuideButton, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenFlyButtonGuide(base.CurrentGuideButton.transform, "试试第二个技能", ViewLayer.LowLayer, true);
            Singleton<MonsterMgr>.Instance.ShowAllMonster();
        }

        private void GuideSkill_3()
        {
            Singleton<MonsterActivator>.Instance.mobCache.NextStage();
            DamageCheck.Instance.SelfInjuryGuideCallback = null;
            Button currentGuideButton = Singleton<BattleBottomRightView>.Instance.SkillButtonList[2];
            currentGuideButton.SetActive(true);
            base.SetCurrentGuideButton(currentGuideButton, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenFlyButtonGuide(base.CurrentGuideButton.transform, "试试终极技能", ViewLayer.LowLayer, true);
            (AppMap.Instance.me.Controller as ActionControler).StopAndClearPath();
        }

        private void GuideSkillByMonsterGroupId(int groupId)
        {
            if ((this._index < groupId) && AppMap.Instance.isAllDeadByGroup(groupId))
            {
                switch (groupId)
                {
                    case 1:
                        this.GuideSkill_1();
                        this._index++;
                        break;

                    case 2:
                        this.GuideSkill_2();
                        this._index++;
                        break;

                    case 3:
                        this.GuideSkill_3();
                        this._index++;
                        break;

                    case 4:
                        this.GuideEnd();
                        this._index++;
                        break;
                }
            }
        }

        private void InitMovePoint()
        {
            this.posDic.Add(1, new Vector3(-0.226f, 4.497f, -5.819f));
            this.posDic.Add(2, new Vector3(2.841f, 4.577f, -3.081f));
            this.posDic.Add(3, new Vector3(0.526f, 4.642f, -1.307f));
        }

        private void JoyStickCloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
            JoystickController.instance.guideDungeonCallBack = null;
        }

        private void OnKillMonster(int type, int v1, int v2, object data)
        {
            MonsterVo vo = data as MonsterVo;
            this.GuideSkillByMonsterGroupId(vo.groupIndex);
        }
    }
}

