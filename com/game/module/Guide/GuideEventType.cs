﻿namespace com.game.module.Guide
{
    using System;

    public class GuideEventType
    {
        public const int EnterCity = 1;
        public const int EnterDungeon = 3;
        public const int FinishAvatarGuide = 5;
        public const int FinishEnterDungeonGuide = 6;
        public const int FinishHeroGuide = 4;
        public const int FinishJoyStickGuide = 2;
    }
}

