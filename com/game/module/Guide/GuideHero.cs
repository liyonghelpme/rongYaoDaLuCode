﻿namespace com.game.module.Guide
{
    using com.game.module.Baoxiang;
    using com.game.module.core;
    using com.game.module.General;
    using System;

    public class GuideHero : GuideBase
    {
        private int _guideId;

        public GuideHero(int guideId)
        {
            this._guideId = guideId;
        }

        private void BeginGuide()
        {
            this.CloseView();
            if (Singleton<MainBottomRightView>.Instance.isOpen)
            {
                this.GuideHeroBtn();
            }
            else
            {
                this.GuideOpenBtn();
            }
        }

        public override void BeginGuide(GuideBase.GuideFinishCallBack cb)
        {
            base.BeginGuide(cb);
            switch (this._guideId)
            {
                case 8:
                    Singleton<MainView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
                    Singleton<BaoxiangSystem>.Instance.closeViewGuideDelegate = new CloseViewGuideDelegate(this.BeginGuide);
                    break;

                case 10:
                    if ((Singleton<MainView>.Instance.gameObject == null) || !Singleton<MainView>.Instance.IsOpened)
                    {
                        Singleton<MainView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
                        Singleton<MainView>.Instance.OpenView();
                        break;
                    }
                    this.BeginGuide();
                    break;
            }
        }

        private void CloseGuide()
        {
            base.FinishGuide();
            Singleton<MainView>.Instance.AfterOpenGuideDelegate = null;
        }

        private void CloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void GuideChooseCard()
        {
            Singleton<GuideView>.Instance.OpenGuide(Singleton<GeneralPanel>.Instance.getKapaiItem(), "选中需要出战英雄", GuideShowType.Left, ViewLayer.TopUILayer, null);
            Singleton<GeneralPanel>.Instance.guideHeroDelegate = new GuideHeroDelegate(this.GuideHeroFight);
        }

        private void GuideClose()
        {
            base.SetCurrentGuideButton(Singleton<GeneralPanel>.Instance.Close_btn, new UIWidgetContainer.GuideDelegate(this.CloseView), new UIWidgetContainer.GuideDelegate(this.CloseGuide));
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "关闭当前界面", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void GuideHeroBtn()
        {
            base.SetCurrentGuideButton(Singleton<MainBottomRightView>.Instance.TeamButton, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“英雄”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            Singleton<GeneralPanel>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.WaitGuideChooseCard);
        }

        private void GuideHeroFight()
        {
            this.CloseView();
            base.SetCurrentGuideButton(Singleton<GeneralPanel>.Instance.Fight_btn, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“英雄”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            Singleton<GeneralPanel>.Instance.guideHeroOver = new GuidHeroOverDelegate(this.GuideClose);
        }

        private void GuideOpenBtn()
        {
            base.SetCurrentGuideButton(Singleton<MainBottomRightView>.Instance.OpenButton, new UIWidgetContainer.GuideDelegate(this.CloseView), new UIWidgetContainer.GuideDelegate(this.WaitGuideHeroBtn));
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "展开操作菜单", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void WaitGuideChooseCard()
        {
            vp_Timer.In(1f, new vp_Timer.Callback(this.GuideChooseCard), null);
        }

        private void WaitGuideHeroBtn()
        {
            vp_Timer.In(1f, new vp_Timer.Callback(this.GuideHeroBtn), null);
        }
    }
}

