﻿namespace com.game.module.Guide
{
    using com.game.module.core;
    using com.game.module.effect;
    using com.game.module.map;
    using com.game.utils;
    using com.u3d.bases.joystick;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class GuideJoystick : GuideBase
    {
        private EffectControlerII eControler;
        private uint len = 1;
        private string moveEffectId = "10051";
        private int moveStep = 1;
        private Dictionary<int, Vector3> posDic = new Dictionary<int, Vector3>();

        private void BeginGuide()
        {
            Singleton<BattleView>.Instance.RecoverGuideDelegate = new RecoverGuideDelegate(this.CloseGuide);
            foreach (Button button in Singleton<BattleBottomRightView>.Instance.SkillButtonList)
            {
                button.SetActive(false);
            }
            Singleton<BattleTopLeftView>.Instance.HideAutoAttackBtn(false);
            Singleton<BattleBottomLeftView>.Instance.Joystick.SetActive(true);
            this.InitMovePoint();
            this.GuideJoyStick();
        }

        public override void BeginGuide(GuideBase.GuideFinishCallBack cb)
        {
            base.BeginGuide(cb);
            Singleton<BattleView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
        }

        private void CheckMove(Vector3 pos)
        {
            if (this.posDic.ContainsKey(this.moveStep))
            {
                if (Vector3.Distance(pos, this.posDic[this.moveStep]) <= this.len)
                {
                    this.eControler.Clear();
                    this.moveStep++;
                    if (this.posDic.ContainsKey(this.moveStep))
                    {
                        EffectMgr.Instance.CreateStaticBuffEffect(this.moveEffectId, this.posDic[this.moveStep], new com.game.module.effect.Effect.EffectCreateCallback(this.CreateEffectCallBack), null, true);
                    }
                }
            }
            else
            {
                this.CloseView();
                base.FinishGuide();
                Singleton<MonsterMgr>.Instance.moveMoveToCallBack = null;
            }
        }

        private void CloseGuide()
        {
            Singleton<BattleView>.Instance.RecoverGuideDelegate = null;
            Singleton<GuideView>.Instance.CloseView();
        }

        private void CloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void CreateEffectCallBack(EffectControlerII effControler)
        {
            this.eControler = effControler;
        }

        private void GuideJoyStick()
        {
            Singleton<GuideView>.Instance.OpenGuide(Singleton<BattleBottomLeftView>.Instance.Joystick.transform, "操作摇杆移动角色", GuideShowType.Joystick, ViewLayer.LowLayer, null);
            EffectMgr.Instance.CreateStaticBuffEffect(this.moveEffectId, this.posDic[this.moveStep], new com.game.module.effect.Effect.EffectCreateCallback(this.CreateEffectCallBack), null, true);
            Singleton<MonsterMgr>.Instance.moveMoveToCallBack = new MonsterMgr.MoveToCallBack(this.CheckMove);
            JoystickController.instance.guideDungeonCallBack = new JoystickController.GuideDungeon(this.JoyStickCloseView);
        }

        private void InitMovePoint()
        {
            this.posDic.Add(1, StringUtils.StringToVector3(Singleton<ConfigConst>.Instance.GetConfigDataString("GUID_POS_1")));
            this.posDic.Add(2, StringUtils.StringToVector3(Singleton<ConfigConst>.Instance.GetConfigDataString("GUID_POS_2")));
            this.posDic.Add(3, StringUtils.StringToVector3(Singleton<ConfigConst>.Instance.GetConfigDataString("GUID_POS_3")));
            this.posDic.Add(4, StringUtils.StringToVector3(Singleton<ConfigConst>.Instance.GetConfigDataString("GUID_POS_4")));
        }

        private void JoyStickCloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
            JoystickController.instance.guideDungeonCallBack = null;
        }
    }
}

