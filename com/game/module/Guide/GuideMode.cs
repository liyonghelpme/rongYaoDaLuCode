﻿namespace com.game.module.Guide
{
    using com.game;
    using com.game.data;
    using com.game.module.core;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class GuideMode : BaseMode<GuideMode>
    {
        private Dictionary<int, GuideBase> _guideDictionary;
        private bool _isShowGuide;
        public int CurrentGuideId;
        public SysGuideVo CurrentTriggeredGuideVo;
        public static GuideMode Instance = new GuideMode();
        public const int ShowGuideStatuUpdate = 1;

        public GuideMode()
        {
            this.RegisterGuide();
        }

        public GuideBase GetGuideLogic(int guideType)
        {
            if (this._guideDictionary.ContainsKey(guideType))
            {
                return this._guideDictionary[guideType];
            }
            return null;
        }

        public void GuideEvent(byte eventType)
        {
            MemoryStream msdata = new MemoryStream();
            Module_40.write_40_1(msdata, eventType);
            AppNet.gameNet.send(msdata, 40, 1);
        }

        public void GuideFinish(byte guideId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_40.write_40_2(msdata, guideId);
            AppNet.gameNet.send(msdata, 40, 2);
        }

        public void RegisterGuide()
        {
            this._guideDictionary = new Dictionary<int, GuideBase>();
            this._guideDictionary.Add(1, new GuideJoystick());
            this._guideDictionary.Add(2, new GuideSkill(1));
            this._guideDictionary.Add(3, new GuideSkill(2));
            this._guideDictionary.Add(4, new GuideSkill(3));
            this._guideDictionary.Add(5, new GuideSkill(4));
            this._guideDictionary.Add(6, new GuideDungeon(0x2711, 6));
            this._guideDictionary.Add(9, new GuideDungeon(0x2712, 9));
            this._guideDictionary.Add(11, new GuideDungeon(0x2713, 11));
            this._guideDictionary.Add(12, new GuideDungeon(0x2714, 12));
            this._guideDictionary.Add(13, new GuideDungeon(0x2715, 13));
            this._guideDictionary.Add(14, new GuideDungeon(0x2716, 14));
            this._guideDictionary.Add(15, new GuideDungeon(0x2717, 15));
            this._guideDictionary.Add(0x10, new GuideDungeon(0x2718, 0x10));
            this._guideDictionary.Add(7, new GuideBox());
            this._guideDictionary.Add(8, new GuideHero(8));
            this._guideDictionary.Add(10, new GuideHero(10));
            this._guideDictionary.Add(0x11, new GuideEquipment(0x11));
            this._guideDictionary.Add(0x12, new GuideAchieve(0x12));
            this._guideDictionary.Add(0x13, new GuideUpgradeStar(0x13));
            this._guideDictionary.Add(20, new GuideSkillUpgrade(20));
        }

        public void StartGuide(int guideId, GuideBase.GuideFinishCallBack cb)
        {
            if (this._guideDictionary.ContainsKey(guideId))
            {
                this._guideDictionary[guideId].BeginGuide(cb);
                this.CurrentGuideId = guideId;
            }
        }

        public void StartGuideNext(GuideBase.GuideFinishCallBack cb)
        {
            if (this._guideDictionary.ContainsKey(this.CurrentGuideId + 1))
            {
                this._guideDictionary[this.CurrentGuideId + 1].BeginGuide(cb);
                this.CurrentGuideId++;
            }
        }

        public bool IsShowGuide
        {
            get
            {
                return this._isShowGuide;
            }
            set
            {
                this._isShowGuide = value;
                base.DataUpdate(1);
            }
        }
    }
}

