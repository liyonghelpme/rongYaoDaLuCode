﻿namespace com.game.module.Guide
{
    using com.game.module.core;
    using System;

    public class GuideModel : BaseMode<GuideModel>
    {
        private bool _isShowGuide;
        public const int ShowGuideStatuUpdate = 1;

        public bool IsShowGuide
        {
            get
            {
                return this._isShowGuide;
            }
            set
            {
                this._isShowGuide = value;
                base.DataUpdate(1);
            }
        }
    }
}

