﻿namespace com.game.module.Guide
{
    using System;

    public enum GuideShowType
    {
        Up,
        Down,
        Left,
        Right,
        Joystick
    }
}

