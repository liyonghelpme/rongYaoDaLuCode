﻿namespace com.game.module.Guide
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.display.controler;
    using System;

    public class GuideSkill : GuideBase
    {
        public int skilltype;

        public GuideSkill(int skillType)
        {
            this.skilltype = skillType;
        }

        private void BeginGuide()
        {
            foreach (Button button in Singleton<BattleBottomRightView>.Instance.SkillButtonList)
            {
                button.SetActive(false);
            }
            Singleton<BattleTopLeftView>.Instance.HideAutoAttackBtn(false);
            switch (this.skilltype)
            {
                case 1:
                    this.GuideAttack();
                    break;

                case 2:
                    this.GuideSkill_1();
                    break;

                case 3:
                    this.GuideSkill_2();
                    break;

                case 4:
                    this.GuideSkill_3();
                    break;
            }
        }

        public override void BeginGuide(GuideBase.GuideFinishCallBack cb)
        {
            base.BeginGuide(cb);
            Singleton<BattleView>.Instance.RecoverGuideDelegate = new RecoverGuideDelegate(this.CloseGuide);
            Singleton<BattleView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
        }

        private void CloseGuide()
        {
            Singleton<BattleView>.Instance.RecoverGuideDelegate = null;
            Singleton<GuideView>.Instance.CloseView();
            base.FinishGuide();
        }

        private void CloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void GuideAttack()
        {
            base.SetCurrentGuideButton(Singleton<BattleBottomRightView>.Instance.AttackButton, new UIWidgetContainer.GuideDelegate(this.CloseGuide), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "试试普通攻击", GuideShowType.Right, ViewLayer.LowLayer, null);
        }

        private void GuideSkill_1()
        {
            Button currentGuideButton = Singleton<BattleBottomRightView>.Instance.SkillButtonList[0];
            currentGuideButton.SetActive(true);
            base.SetCurrentGuideButton(currentGuideButton, new UIWidgetContainer.GuideDelegate(this.CloseGuide), null);
            Singleton<GuideView>.Instance.OpenFlyButtonGuide(base.CurrentGuideButton.transform, "试试你的新技能", ViewLayer.LowLayer, true);
        }

        private void GuideSkill_2()
        {
            Singleton<BattleBottomRightView>.Instance.SkillButtonList[0].SetActive(true);
            Button currentGuideButton = Singleton<BattleBottomRightView>.Instance.SkillButtonList[1];
            currentGuideButton.SetActive(true);
            base.SetCurrentGuideButton(currentGuideButton, new UIWidgetContainer.GuideDelegate(this.CloseGuide), null);
            Singleton<GuideView>.Instance.OpenFlyButtonGuide(base.CurrentGuideButton.transform, "试试第二个技能", ViewLayer.LowLayer, true);
            Singleton<MonsterMgr>.Instance.ShowAllMonster();
        }

        private void GuideSkill_3()
        {
            Singleton<BattleBottomRightView>.Instance.SkillButtonList[0].SetActive(true);
            Singleton<BattleBottomRightView>.Instance.SkillButtonList[1].SetActive(true);
            Button currentGuideButton = Singleton<BattleBottomRightView>.Instance.SkillButtonList[2];
            currentGuideButton.SetActive(true);
            base.SetCurrentGuideButton(currentGuideButton, new UIWidgetContainer.GuideDelegate(this.CloseGuide), null);
            Singleton<GuideView>.Instance.OpenFlyButtonGuide(base.CurrentGuideButton.transform, "试试终极技能", ViewLayer.LowLayer, true);
            (AppMap.Instance.me.Controller as ActionControler).StopAndClearPath();
        }
    }
}

