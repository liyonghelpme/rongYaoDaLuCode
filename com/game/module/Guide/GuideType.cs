﻿namespace com.game.module.Guide
{
    using System;

    public class GuideType
    {
        public const int GuideAcheieve = 0x12;
        public const int GuideArenaOpen = 230;
        public const int GuideAvator = 7;
        public const int GuideBox = 7;
        public const int GuideCopy = 0;
        public const int GuideDaemonIslandOpen = 240;
        public const int GuideDungeon_1 = 6;
        public const int GuideDungeon_2 = 9;
        public const int GuideDungeon_3 = 11;
        public const int GuideDungeon_4 = 12;
        public const int GuideDungeon_5 = 13;
        public const int GuideDungeon_6 = 14;
        public const int GuideDungeon_7 = 15;
        public const int GuideDungeon_8 = 0x10;
        public const int GuideDungeonAction = 6;
        public const int GuideEquip = 0x6f;
        public const int GuideEquipInlay = 0x84;
        public const int GuideEquipment = 0x11;
        public const int GuideEquipMerge = 0x85;
        public const int GuideEquipRefine = 0x83;
        public const int GuideForgeOpen = 130;
        public const int GuideGoldBoxOpen = 330;
        public const int GuideGoldHitOpen = 220;
        public const int GuideGoldSilverIslandOpen = 210;
        public const int GuideGrow = 0x70;
        public const int GuideGuildOpen = 150;
        public const int GuideHero_1 = 8;
        public const int GuideHero_2 = 10;
        public const int GuideHeroFight_1 = 8;
        public const int GuideHeroFight_2 = 9;
        public const int GuideJoystick = 1;
        public const int GuideLuckDraw = 320;
        public const int GuideMedal = 0x71;
        public const int GuideMedalOpen = 0x71;
        public const int GuidePetEquip = 0x8d;
        public const int GuidePetOpen = 140;
        public const int GuidePetSkill = 0x8e;
        public const int GuideRoleOpen = 0x72;
        public const int GuideShopOpen = 310;
        public const int GuideSkill1 = 2;
        public const int GuideSkill2 = 3;
        public const int GuideSkill3 = 4;
        public const int GuideSkill3Learn = 0x79;
        public const int GuideSkill4 = 5;
        public const int GuideSkill4Learn = 0x7a;
        public const int GuideSkillOpen = 120;
        public const int GuideSkillRollLearn = 0x7b;
        public const int GuideSkillUpgrade = 20;
        public const int GuideSwitchHero = 10;
        public const int GuideUpgradeStar = 0x13;
        public const int GuideWorldBoss = 340;
    }
}

