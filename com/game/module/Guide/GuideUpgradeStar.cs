﻿namespace com.game.module.Guide
{
    using com.game.module.core;
    using com.game.module.General;
    using System;

    public class GuideUpgradeStar : GuideBase
    {
        private int _guideId;

        public GuideUpgradeStar(int guideId)
        {
            this._guideId = guideId;
        }

        private void BeginGuide()
        {
            this.CloseView();
            if (!Singleton<MainBottomRightView>.Instance.isOpen)
            {
                this.GuideOpenBtn();
            }
            else
            {
                this.GuideHeroBtn();
            }
        }

        public override void BeginGuide(GuideBase.GuideFinishCallBack cb)
        {
            base.BeginGuide(cb);
            if ((Singleton<MainView>.Instance.gameObject != null) && Singleton<MainView>.Instance.IsOpened)
            {
                this.BeginGuide();
            }
            else
            {
                Singleton<MainView>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.BeginGuide);
                Singleton<MainView>.Instance.OpenView();
            }
        }

        private void CloseGuide()
        {
            base.FinishGuide();
            Singleton<MainView>.Instance.AfterOpenGuideDelegate = null;
        }

        private void CloseView()
        {
            if (GuideMode.Instance.IsShowGuide)
            {
                Singleton<GuideView>.Instance.CloseView();
            }
        }

        private void GuideHeroBtn()
        {
            base.SetCurrentGuideButton(Singleton<MainBottomRightView>.Instance.TeamButton, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击“英雄”按钮", GuideShowType.Right, ViewLayer.TopUILayer, null);
            Singleton<GeneralPanel>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.WaitOpenGeneralPanel);
            Singleton<GeneralPanel>.Instance.guideSelect = new GuideSelectDelete(this.GuideLookBtn);
        }

        private void GuideLookBtn()
        {
            this.CloseView();
            base.SetCurrentGuideButton(Singleton<GeneralPanel>.Instance.GetLookBtn(), new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(Singleton<GeneralPanel>.Instance.GetLookBtn().transform, "点击查看按钮", GuideShowType.Left, ViewLayer.TopUILayer, null);
            Singleton<GeneralInfoPanel>.Instance.AfterOpenGuideDelegate = new OpenViewGuideDelegate(this.WaitOpenGeneralInfoPanel);
        }

        private void GuideOpenBtn()
        {
            base.SetCurrentGuideButton(Singleton<MainBottomRightView>.Instance.OpenButton, new UIWidgetContainer.GuideDelegate(this.CloseView), new UIWidgetContainer.GuideDelegate(this.WaitGuideHeroBtn));
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "展开操作菜单", GuideShowType.Right, ViewLayer.TopUILayer, null);
        }

        private void GuideOpenGeneralPanel()
        {
            Singleton<GuideView>.Instance.OpenGuide(Singleton<GeneralPanel>.Instance.GetFightGeneralItemView().transform, "点击选中", GuideShowType.Left, ViewLayer.TopUILayer, null);
        }

        private void GuideUpgrade()
        {
            base.SetCurrentGuideButton(Singleton<GeneralInfoPanel>.Instance.UpgradeStar, new UIWidgetContainer.GuideDelegate(this.CloseView), null);
            Singleton<GuideView>.Instance.OpenGuide(base.CurrentGuideButton.transform, "点击升阶按钮", GuideShowType.Left, ViewLayer.TopUILayer, null);
            Singleton<GeneralInfoPanel>.Instance.guideUpgradeStar = new com.game.module.General.GuideUpgradeStar(this.GuideUpgradeStarSuc);
        }

        private void GuideUpgradeStarSuc()
        {
            this.CloseView();
            this.CloseGuide();
        }

        private void WaitGuideHeroBtn()
        {
            vp_Timer.In(1f, new vp_Timer.Callback(this.GuideHeroBtn), null);
        }

        private void WaitOpenGeneralInfoPanel()
        {
            vp_Timer.In(1f, new vp_Timer.Callback(this.GuideUpgrade), null);
        }

        private void WaitOpenGeneralPanel()
        {
            vp_Timer.In(1f, new vp_Timer.Callback(this.GuideOpenGeneralPanel), null);
        }
    }
}

