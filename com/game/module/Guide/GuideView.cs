﻿namespace com.game.module.Guide
{
    using com.game.module.core;
    using com.u3d.bases.debug;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GuideView : BaseView<GuideView>
    {
        private GameObject _currentGuideArrow;
        private GameObject _downGuideArrow;
        private UILabel _functionOpenInfoLabel;
        private GameObject _guideEffect;
        private Transform _guideItem;
        private int _guideItemOriginalLayer;
        private int _guideItemOriginalPanelDepth;
        private Transform _guideItemOriginalParentTransform;
        private GuideShowType _guideShowType;
        private string _guideTip;
        private bool _isFlyButton;
        private bool _isJoystickGuide;
        private bool _isNeedMask;
        private int _layer;
        private GameObject _leftGuideArrow;
        private GameObject _mask;
        private bool _needGuideOpen;
        private UIPanel _newParentPanel;
        private Transform _parentTransform;
        private GameObject _rightGuideArrow;
        private Vector3 _targetPosition;
        private GameObject _upGuideArrow;
        private ViewLayer _viewLayer;

        private Vector3 CalcArrowPosAndDirect(Vector3 pos, BoxCollider collider)
        {
            Vector3 lossyScale = this._guideItem.transform.lossyScale;
            pos.x += collider.center.x * lossyScale.x;
            pos.y += collider.center.y * lossyScale.y;
            this._guideEffect.transform.position = pos;
            switch (this._guideShowType)
            {
                case GuideShowType.Up:
                    this._currentGuideArrow = this._upGuideArrow;
                    pos.y -= (collider.size.y / 2f) * lossyScale.y;
                    return pos;

                case GuideShowType.Down:
                    this._currentGuideArrow = this._downGuideArrow;
                    pos.y += (collider.size.y / 2f) * lossyScale.y;
                    return pos;

                case GuideShowType.Left:
                    this._currentGuideArrow = this._leftGuideArrow;
                    pos.x += (collider.size.x / 2f) * lossyScale.x;
                    return pos;

                case GuideShowType.Right:
                    this._currentGuideArrow = this._rightGuideArrow;
                    pos.x -= (collider.size.x / 2f) * lossyScale.x;
                    return pos;
            }
            return pos;
        }

        public static int CalculateNextPanelDepth(GameObject go)
        {
            int a = -1;
            UIPanel[] componentsInChildren = go.GetComponentsInChildren<UIPanel>();
            int index = 0;
            int length = componentsInChildren.Length;
            while (index < length)
            {
                a = Mathf.Max(a, componentsInChildren[index].depth);
                index++;
            }
            return (a + 1);
        }

        private Transform FindParentUIPanelTransform(GameObject go)
        {
            if (go == null)
            {
                return null;
            }
            Transform parent = go.transform.parent;
            while ((parent != null) && (parent.gameObject.GetComponent<UIPanel>() == null))
            {
                parent = parent.parent;
            }
            return parent;
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.SetGuideViewLayer(this._guideItem.gameObject);
            if (this._guideShowType == GuideShowType.Joystick)
            {
                this.ShowJoystickGuide();
            }
            else
            {
                if (this._isNeedMask)
                {
                    this._mask.SetActive(false);
                    this._mask.SetActive(true);
                    this.SetGuideItemToGuideViewLayer();
                }
                this.ShowGuideArrow();
            }
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
            if (this._isNeedMask && (this._guideShowType != GuideShowType.Joystick))
            {
                this.RecoverGuideItemToOriginalLayer();
            }
            this._guideItem = null;
            this._guideItemOriginalParentTransform = null;
            this._leftGuideArrow.SetActive(false);
            this._rightGuideArrow.SetActive(false);
            this._upGuideArrow.SetActive(false);
            this._downGuideArrow.SetActive(false);
            this._mask.SetActive(false);
            GuideMode.Instance.IsShowGuide = false;
        }

        protected override void Init()
        {
            this._parentTransform = base.FindChild("GuideParent").transform;
            this._functionOpenInfoLabel = base.FindInChild<UILabel>("Buttom/FunctionOpenInfoLabel");
            this._mask = base.FindChild("Mask");
            this._leftGuideArrow = base.FindChild("GuideArrow/LeftGuide");
            this._rightGuideArrow = base.FindChild("GuideArrow/RightGuide");
            this._upGuideArrow = base.FindChild("GuideArrow/UpGuide");
            this._downGuideArrow = base.FindChild("GuideArrow/DownGuide");
            this._guideEffect = base.FindChild("GuideEffect");
            this._leftGuideArrow.SetActive(false);
            this._rightGuideArrow.SetActive(false);
            this._upGuideArrow.SetActive(false);
            this._downGuideArrow.SetActive(false);
            this._mask.SetActive(false);
            this._functionOpenInfoLabel.text = string.Empty;
            this.SetToLayerTopUI();
        }

        public void OpenFlyButtonGuide(Transform guideItem, string guideTip, ViewLayer layerType, bool needGuideOpen = true)
        {
            this._viewLayer = layerType;
            if (!Singleton<GuideModel>.Instance.IsShowGuide)
            {
                this._isFlyButton = true;
                this._guideItem = guideItem;
                this._targetPosition = guideItem.position;
                this._guideTip = guideTip;
                this._needGuideOpen = needGuideOpen;
                this._guideShowType = GuideShowType.Right;
                this.OpenView();
            }
        }

        public void OpenGuide(Transform guideItem, string guideTip, GuideShowType guideShowType, ViewLayer viewLayer = 5, UIPanel uiPanel = null)
        {
            this._viewLayer = viewLayer;
            this._newParentPanel = uiPanel;
            this._guideShowType = guideShowType;
            if (!GuideMode.Instance.IsShowGuide)
            {
                this._guideItem = guideItem;
                this._guideTip = guideTip;
                this.OpenView();
            }
        }

        public override void OpenView()
        {
            GuideMode.Instance.IsShowGuide = true;
            base.OpenView();
        }

        private void RecoverGuideItemToOriginalLayer()
        {
            UIPanel component = this._guideItem.GetComponent<UIPanel>();
            if (component != null)
            {
                component.depth = this._guideItemOriginalPanelDepth;
                this._guideItemOriginalPanelDepth = 0;
            }
            this._guideItem.parent = this._guideItemOriginalParentTransform;
            this._guideItem.gameObject.SetActive(false);
            this._guideItem.gameObject.SetActive(true);
            NGUITools.SetLayer(this._guideItem.gameObject, this._guideItemOriginalLayer);
        }

        private void SetGuideItemToGuideViewLayer()
        {
            this._guideItemOriginalParentTransform = this._guideItem.parent.transform;
            this._guideItemOriginalLayer = this._guideItem.gameObject.layer;
            this._guideItem.parent = this._parentTransform;
            UIPanel component = this._guideItem.GetComponent<UIPanel>();
            if (component != null)
            {
                this._guideItemOriginalPanelDepth = component.depth;
                component.depth = 0x7d5;
            }
            else
            {
                this._guideItemOriginalPanelDepth = 0;
            }
            NGUITools.SetLayer(this._parentTransform.gameObject, LayerMask.NameToLayer("TopUI"));
        }

        private void SetGuideItemVisiable()
        {
            this._guideItem.gameObject.SetActive(true);
            int childCount = this._guideItem.childCount;
            for (int i = 0; i < childCount; i++)
            {
                if (this._guideItem.GetChild(i).name != "tips")
                {
                    this._guideItem.GetChild(i).gameObject.SetActive(true);
                }
            }
        }

        private void SetGuideViewDepth(GameObject go)
        {
            UIPanel panel = NGUITools.FindInParents<UIPanel>(go);
            NGUITools.FindInParents<UIPanel>(this.gameObject).depth = panel.depth + 1;
        }

        private void SetGuideViewLayer(GameObject go)
        {
        }

        private void SetToLayerTopUI()
        {
            NGUITools.SetLayer(this.gameObject, LayerMask.NameToLayer("TopUI"));
        }

        private void ShowGuideArrow()
        {
            if (this._guideItem != null)
            {
                Vector3 position = this._guideItem.transform.position;
                BoxCollider component = this._guideItem.GetComponent<BoxCollider>();
                if (component == null)
                {
                    Log.error(this, "被指引的对象需要有BoxCollider组件，来表示这个组件的宽高，便于指引箭头准确的计算指引位置");
                }
                else
                {
                    position = this.CalcArrowPosAndDirect(position, component);
                    if (this._currentGuideArrow != null)
                    {
                        if (this._newParentPanel == null)
                        {
                            Transform transform = this.FindParentUIPanelTransform(this._guideItem.gameObject);
                            UIPanel panel = transform.gameObject.GetComponent<UIPanel>();
                            this.gameObject.transform.parent = transform;
                            int b = CalculateNextPanelDepth(panel.gameObject);
                            int depth = panel.depth;
                            UIPanel panel2 = NGUITools.FindInParents<UIPanel>(this.gameObject);
                            int num3 = Mathf.Max(depth, b);
                            panel2.depth = num3 + 1;
                        }
                        else
                        {
                            this.gameObject.transform.parent = this._newParentPanel.gameObject.transform;
                            int num4 = CalculateNextPanelDepth(this._newParentPanel.gameObject);
                            int a = this._newParentPanel.depth;
                            UIPanel panel3 = NGUITools.FindInParents<UIPanel>(this.gameObject);
                            int num6 = Mathf.Max(a, num4);
                            panel3.depth = num6 + 1;
                        }
                        this._currentGuideArrow.transform.position = position;
                        this._currentGuideArrow.transform.FindChild("Arrow/Content/Label").GetComponent<UILabel>().text = this._guideTip;
                        this._currentGuideArrow.SetActive(true);
                    }
                }
            }
        }

        private void ShowJoystickGuide()
        {
            this._mask.SetActive(false);
            Vector3 position = this._guideItem.transform.position;
            this._currentGuideArrow = this._leftGuideArrow;
            if (this._currentGuideArrow != null)
            {
                Transform transform = this.FindParentUIPanelTransform(this._guideItem.gameObject);
                this.gameObject.transform.parent = transform;
                this._currentGuideArrow.transform.position = position;
                this._currentGuideArrow.transform.FindChild("Arrow/Content/Label").GetComponent<UILabel>().text = this._guideTip;
                this._currentGuideArrow.SetActive(true);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopUILayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Guide/GuideView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

