﻿namespace com.game.module.Mail
{
    using com.game.module.bag;
    using com.game.module.core;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class MailAttachmentView
    {
        private PMailBasicInfo _basicInfo;
        private Button _btnAccept;
        private UILabel _contentLabel;
        private MailGetInfoMsg_12_2 _detailInfo;
        private UILabel _diamondLabel;
        private GameObject _go;
        private UILabel _goldLabel;
        private GameObject _itemGo;
        private UIGrid _itemGrid;
        private GameObject _itemGridGo;
        private UIScrollView _scrollView;
        private UILabel _titleLabel;

        public MailAttachmentView(GameObject obj)
        {
            this._go = obj;
            this._itemGridGo = NGUITools.FindChild(this._go, "award_list");
            this._itemGo = NGUITools.FindChild(this._go, "award_list/item");
            this._btnAccept = NGUITools.FindInChild<Button>(this._go, "btn_accept");
            this._diamondLabel = NGUITools.FindInChild<UILabel>(this._go, "Money/Diamond/label");
            this._goldLabel = NGUITools.FindInChild<UILabel>(this._go, "Money/Gold/label");
            this._contentLabel = NGUITools.FindInChild<UILabel>(this._go, "contentLabel");
            this._titleLabel = NGUITools.FindInChild<UILabel>(this._go, "titleLabel");
            this._itemGrid = this._itemGridGo.GetComponent<UIGrid>();
            this._scrollView = this._itemGridGo.GetComponent<UIScrollView>();
            this._go.SetActive(true);
        }

        private void btnAcceptOnClick(GameObject go)
        {
            if (this._detailInfo != null)
            {
                Singleton<MailMode>.Instance.AcceptReward(this._detailInfo.mailId);
                this._detailInfo = null;
            }
        }

        private void ClearView()
        {
            this._btnAccept.onClick = null;
            this._titleLabel.text = string.Empty;
            this._diamondLabel.text = string.Empty;
            this._goldLabel.text = string.Empty;
            this._contentLabel.text = string.Empty;
            this._itemGridGo.SetActive(false);
            this._btnAccept.SetActive(false);
        }

        public void CloseView()
        {
            Transform transform = this._itemGridGo.transform;
            for (int i = transform.childCount - 1; i > 0; i--)
            {
                UnityEngine.Object.Destroy(transform.GetChild(i).gameObject);
            }
            this._btnAccept.onClick = null;
            Singleton<MailMode>.Instance.MailTempVo.curMailDetailVo = null;
        }

        public void DelReadMail()
        {
            MailGetInfoMsg_12_2 curMailDetailVo = Singleton<MailMode>.Instance.MailTempVo.curMailDetailVo;
            PMailBasicInfo mailInfo = null;
            if (curMailDetailVo != null)
            {
                mailInfo = Singleton<MailMode>.Instance.MailListVo.GetMailInfo(curMailDetailVo.mailId);
            }
            if ((mailInfo != null) && (mailInfo.type != 1))
            {
                Singleton<MailMode>.Instance.MailTempVo.Select(mailInfo.id);
                Singleton<MailMode>.Instance.RequestDeleteSelected();
            }
            this._basicInfo = null;
            this._detailInfo = null;
        }

        private SysItemVoCell GetCell(Transform trans, int idx, int numChild)
        {
            GameObject gameObject;
            if (idx < numChild)
            {
                gameObject = trans.GetChild(idx).gameObject;
            }
            else
            {
                gameObject = NGUITools.AddChild(this._itemGridGo, this._itemGo);
            }
            return gameObject.AddMissingComponent<SysItemVoCell>();
        }

        public void SetActive(bool v)
        {
            this._go.SetActive(v);
        }

        private void setReward(List<PMailAttach> mailAttachList)
        {
            this._itemGridGo.SetActive(true);
            if (null != this._scrollView)
            {
                this._scrollView.ResetPosition();
            }
            bool active = 1 == this._basicInfo.type;
            this._btnAccept.SetActive(active);
            Transform trans = this._itemGridGo.transform;
            int childCount = trans.childCount;
            int idx = 0;
            foreach (PMailAttach attach in mailAttachList)
            {
                SysItemVoCell cell = this.GetCell(trans, idx, childCount);
                cell.SetActive(true);
                cell.sysVoId = (int) attach.id;
                cell.count = attach.count;
                idx++;
            }
            while (idx < childCount)
            {
                trans.GetChild(idx).gameObject.SetActive(false);
                idx++;
            }
            this._itemGrid.Reposition();
        }

        public void UpdateView()
        {
            this._detailInfo = Singleton<MailMode>.Instance.MailTempVo.curMailDetailVo;
            if (this._detailInfo != null)
            {
                this._basicInfo = Singleton<MailMode>.Instance.MailListVo.GetMailInfo(this._detailInfo.mailId);
            }
            if ((this._basicInfo == null) || (this._detailInfo == null))
            {
                this.ClearView();
            }
            else
            {
                this._btnAccept.onClick = new UIWidgetContainer.VoidDelegate(this.btnAcceptOnClick);
                this._titleLabel.text = this._detailInfo.title;
                this._diamondLabel.text = this._detailInfo.diam + string.Empty;
                this._goldLabel.text = this._detailInfo.gold + string.Empty;
                this._contentLabel.text = this._detailInfo.content;
                this.setReward(this._detailInfo.mailAttachList);
            }
        }
    }
}

