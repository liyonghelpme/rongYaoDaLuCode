﻿namespace com.game.module.Mail
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class MailBoxListVo
    {
        private List<ulong> _mailIds = new List<ulong>();
        private List<PMailBasicInfo> _mailList = new List<PMailBasicInfo>();

        public int AddMail(PMailBasicInfo info)
        {
            int index = this.IndexOf(info.id);
            if (index >= 0)
            {
                this._mailList.RemoveAt(index);
            }
            this._mailList.Add(info);
            this._mailIds.Add(info.id);
            return index;
        }

        public void ClearMails()
        {
            this._mailList.Clear();
            this._mailIds.Clear();
        }

        public bool Contains(ulong mailId)
        {
            return (this.IndexOf(mailId) >= 0);
        }

        public PMailBasicInfo GetMailInfo(ulong mailId)
        {
            int index = this.IndexOf(mailId);
            if (index >= 0)
            {
                return this._mailList[index];
            }
            return null;
        }

        private int IndexOf(ulong mailId)
        {
            for (int i = this._mailList.Count - 1; i >= 0; i--)
            {
                if (this._mailList[i].id == mailId)
                {
                    return i;
                }
            }
            return -1;
        }

        public void RemoveMail(ulong mailId)
        {
            int index = this.IndexOf(mailId);
            if (index >= 0)
            {
                this._mailList.RemoveAt(index);
                this._mailIds.Remove(mailId);
            }
        }

        public List<ulong> mailIds
        {
            get
            {
                return this._mailIds;
            }
        }

        public List<PMailBasicInfo> MailList
        {
            get
            {
                return this._mailList;
            }
        }
    }
}

