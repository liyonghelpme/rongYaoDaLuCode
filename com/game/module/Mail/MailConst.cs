﻿namespace com.game.module.Mail
{
    using System;

    public class MailConst
    {
        public const int ATTACHITEM_3 = 3;

        public enum attachStatus
        {
            HasNoAttachment,
            HasAttachment,
            HasGetAttachment,
            ReleaseNote
        }

        public enum readStatus
        {
            AleadyRead,
            UnRead
        }
    }
}

