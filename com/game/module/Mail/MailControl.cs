﻿namespace com.game.module.Mail
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class MailControl : BaseControl<MailControl>
    {
        private MailMode mailMode;

        private void Fun_12_1(INetData data)
        {
            MailGetBoxListMsg_12_1 g__ = new MailGetBoxListMsg_12_1();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this.mailMode.SetUpdateMailList(g__.mailList);
            }
        }

        private void Fun_12_2(INetData data)
        {
            MailGetInfoMsg_12_2 infoMsg = new MailGetInfoMsg_12_2();
            infoMsg.read(data.GetMemoryStream());
            if (infoMsg.code != 0)
            {
                ErrorCodeManager.ShowError(infoMsg.code);
            }
            else
            {
                this.mailMode.CheckCurrentMailInfo(infoMsg);
            }
        }

        private void Fun_12_3(INetData data)
        {
            MailDelMailMsg_12_3 g__ = new MailDelMailMsg_12_3();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this.mailMode.UpdateDelMailList(g__.mailIds);
            }
        }

        private void Fun_12_4(INetData data)
        {
            MailTakeAttachMsg_12_4 g__ = new MailTakeAttachMsg_12_4();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("Mail.AcceptSuccess"));
                this.mailMode.AcceptRewardSuccess(g__.mailId);
            }
        }

        private void Fun_12_6(INetData data)
        {
            MailNewMailInformMsg_12_6 g__ = new MailNewMailInformMsg_12_6();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this.mailMode.SetNewUnreadMail(g__.mailCount);
            }
        }

        private void Fun_12_7(INetData data)
        {
            MailDelAllMsg_12_7 g__ = new MailDelAllMsg_12_7();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this.mailMode.UpdateOneyKeyDelSuccess();
            }
        }

        private void Fun_12_8(INetData data)
        {
            MailGetAllAttachMsg_12_8 g__ = new MailGetAllAttachMsg_12_8();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this.mailMode.UpdateOneKeyAcceptSuccess();
                MessageManager.Show(LanguageManager.GetWord("Mail.OneKeyAcceptSuccess"));
            }
        }

        protected override void NetListener()
        {
            this.mailMode = Singleton<MailMode>.Instance;
            AppNet.main.addCMD("3073", new NetMsgCallback(this.Fun_12_1));
            AppNet.main.addCMD("3074", new NetMsgCallback(this.Fun_12_2));
            AppNet.main.addCMD("3075", new NetMsgCallback(this.Fun_12_3));
            AppNet.main.addCMD("3076", new NetMsgCallback(this.Fun_12_4));
            AppNet.main.addCMD("3078", new NetMsgCallback(this.Fun_12_6));
            AppNet.main.addCMD("3079", new NetMsgCallback(this.Fun_12_7));
            AppNet.main.addCMD("3080", new NetMsgCallback(this.Fun_12_8));
        }
    }
}

