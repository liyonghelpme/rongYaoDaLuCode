﻿namespace com.game.module.Mail
{
    using System;
    using System.Collections.Generic;

    public class MailGetInfoVo
    {
        private string content;
        private uint diam;
        private uint diamBind;
        private uint gold;
        private List<PMailAttach> mailAttachList = new List<PMailAttach>();
        private uint mailId;
        private List<PMailOther> mailOtherDataList = new List<PMailOther>();
        private uint sendTime;
        private string title;

        public string Content
        {
            get
            {
                return this.content;
            }
            set
            {
                this.content = value;
            }
        }

        public uint Diam
        {
            get
            {
                return this.diam;
            }
            set
            {
                this.diam = value;
            }
        }

        public uint DiamBind
        {
            get
            {
                return this.diamBind;
            }
            set
            {
                this.diamBind = value;
            }
        }

        public uint Gold
        {
            get
            {
                return this.gold;
            }
            set
            {
                this.gold = value;
            }
        }

        public List<PMailAttach> MailAttachList
        {
            get
            {
                return this.mailAttachList;
            }
            set
            {
                this.mailAttachList = value;
            }
        }

        public uint MailId
        {
            get
            {
                return this.mailId;
            }
            set
            {
                this.mailId = value;
            }
        }

        public List<PMailOther> MailOtherDataList
        {
            get
            {
                return this.mailOtherDataList;
            }
            set
            {
                this.mailOtherDataList = value;
            }
        }

        public uint SendTime
        {
            get
            {
                return this.sendTime;
            }
            set
            {
                this.sendTime = value;
            }
        }

        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                this.title = value;
            }
        }
    }
}

