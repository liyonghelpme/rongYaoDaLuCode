﻿namespace com.game.module.Mail
{
    using com.game.manager;
    using com.game.utils;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class MailItem
    {
        private Button _btn;
        private UISprite _choosenBg;
        private Action<MailItem> _clickCallback;
        private GameObject _go;
        private GameObject _iconGo;
        private PMailBasicInfo _mailBaseInfo;
        private GameObject _readGo;
        private UILabel _senderLabel;
        private UILabel _timeLabel;
        private UILabel _titleLabel;
        private GameObject _unreadGo;

        public MailItem(GameObject obj, Action<MailItem> clickAction)
        {
            this._go = obj;
            this._clickCallback = clickAction;
            this._readGo = NGUITools.FindChild(this._go, "readTag");
            this._unreadGo = NGUITools.FindChild(this._go, "unreadTag");
            this._iconGo = NGUITools.FindChild(this._go, "icon");
            this._btn = this._go.GetComponent<Button>();
            this._titleLabel = NGUITools.FindInChild<UILabel>(this._go, "titleLabel");
            this._timeLabel = NGUITools.FindInChild<UILabel>(this._go, "timeLabel");
            this._senderLabel = NGUITools.FindInChild<UILabel>(this._go, "senderLabel");
            this._choosenBg = NGUITools.FindInChild<UISprite>(this._go, "chooseBg");
            this._choosenBg.SetActive(false);
            this._btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        private void OnClick(GameObject go)
        {
            if (this._clickCallback != null)
            {
                this._clickCallback(this);
            }
        }

        private void SetActive(bool b)
        {
            this._go.SetActive(b);
        }

        public void UpdateView()
        {
            bool flag = null == this._mailBaseInfo;
            this.SetActive(!flag);
            if (!flag)
            {
                this._titleLabel.text = this._mailBaseInfo.title;
                this._senderLabel.text = LanguageManager.GetWord("Mail.Sender", this._mailBaseInfo.senderNick);
                this._timeLabel.text = TimeUtil.GetTimeYyyymmddHhmmss(this._mailBaseInfo.sentTime);
                bool flag2 = this._mailBaseInfo.isRead == 0;
                this._readGo.SetActive(flag2);
                this._unreadGo.SetActive(!flag2);
                bool flag3 = this._mailBaseInfo.type == 1;
                this._iconGo.gameObject.SetActive(flag3);
            }
        }

        public GameObject go
        {
            get
            {
                return this._go;
            }
        }

        public PMailBasicInfo info
        {
            get
            {
                return this._mailBaseInfo;
            }
            set
            {
                this._mailBaseInfo = value;
                this.UpdateView();
            }
        }

        public bool selected
        {
            get
            {
                return this._choosenBg.enabled;
            }
            set
            {
                this._choosenBg.SetActive(value);
            }
        }
    }
}

