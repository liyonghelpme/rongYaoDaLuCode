﻿namespace com.game.module.Mail
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Confirm;
    using com.game.Public.Message;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine;

    public class MailMode : BaseMode<MailMode>
    {
        private MailBoxListVo _mailListVo = new MailBoxListVo();
        private com.game.module.Mail.MailTempVo _mailTempVo = new com.game.module.Mail.MailTempVo();
        public const int DELETE_MAILSUCCESS = 4;
        public const int GETAWARD_SUCCESS = 3;
        public const int NUM_UNREAD = 7;
        public const int UPDATE_CURRENTMAIL = 2;
        public const int UPDATE_MAILLIST = 1;
        public const int UPDATE_ONEKEYATTACHE = 5;
        public const int UPDATE_ONEKEYDEL = 6;

        public void AcceptReward(ulong mailID)
        {
            MemoryStream msdata = new MemoryStream();
            Module_12.write_12_4(msdata, mailID);
            AppNet.gameNet.send(msdata, 12, 4);
        }

        public void AcceptRewardSuccess(ulong mailId)
        {
            for (int i = 0; i < this._mailListVo.MailList.Count; i++)
            {
                if (this._mailListVo.MailList[i].id == mailId)
                {
                    this._mailListVo.MailList[i].type = 0;
                    break;
                }
            }
            this.MailTempVo.curMailDetailVo = null;
            this.MailTempVo.Select(mailId);
            this.RequestDeleteSelected();
            base.DataUpdateWithParam(3, mailId);
        }

        private void CancelDel()
        {
            Singleton<MailMode>.Instance.MailTempVo.SelectMails.Clear();
        }

        public void CheckCurrentMailInfo(MailGetInfoMsg_12_2 infoMsg)
        {
            this._mailTempVo.curMailDetailVo = infoMsg;
            this._mailTempVo.numUnread = 0;
            for (int i = 0; i < this._mailListVo.MailList.Count; i++)
            {
                if (this._mailListVo.MailList[i].id == infoMsg.mailId)
                {
                    this._mailListVo.MailList[i].isRead = 0;
                }
                if (this._mailListVo.MailList[i].isRead == 1)
                {
                    this._mailTempVo.numUnread++;
                }
            }
            base.DataUpdateWithParam(2, infoMsg.mailId);
        }

        private void DeleteSelected()
        {
            MemoryStream msdata = new MemoryStream();
            Module_12.write_12_3(msdata, this._mailTempVo.SelectMails);
            AppNet.gameNet.send(msdata, 12, 3);
            this._mailTempVo.SelectMails.Clear();
        }

        public void OneKeyDelAll()
        {
            if (this._mailListVo.MailList.Count == 0)
            {
                MessageManager.Show("当前没有邮件");
            }
            else
            {
                for (int i = 0; i < this._mailListVo.MailList.Count; i++)
                {
                    if (this._mailListVo.MailList[i].type == 1)
                    {
                        ConfirmMgr.Instance.ShowOkCancelAlert("你还有没领取的附件，真的要删除他们吗？", "OK_CANCEL", new ClickCallback(this.oneToDelAllMail));
                        return;
                    }
                }
                this.oneToDelAllMail();
            }
        }

        private void oneToDelAllMail()
        {
            MemoryStream msdata = new MemoryStream();
            Module_12.write_12_7(msdata);
            AppNet.gameNet.send(msdata, 12, 7);
        }

        public void OneyKeyGetAllAttach()
        {
            MemoryStream msdata = new MemoryStream();
            Module_12.write_12_8(msdata);
            AppNet.gameNet.send(msdata, 12, 8);
        }

        public void RequestDeleteSelected()
        {
            if (this._mailTempVo.SelectMails.Count == 0)
            {
                MessageManager.Show(LanguageManager.GetWord("Mail.SelectMailFirst"));
            }
            else
            {
                foreach (ulong num in this._mailTempVo.SelectMails)
                {
                    PMailBasicInfo mailInfo = this.MailListVo.GetMailInfo(num);
                    if ((mailInfo != null) && (mailInfo.type == 1))
                    {
                        ConfirmMgr.Instance.ShowCommonAlert(LanguageManager.GetWord("Mail.SureToDelMails"), "OK_CANCEL", new ClickCallback(this.DeleteSelected), string.Empty, new ClickCallback(this.CancelDel), string.Empty);
                        return;
                    }
                }
                this.DeleteSelected();
            }
        }

        public void RequestMailBasicInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_12.write_12_1(msdata);
            AppNet.gameNet.send(msdata, 12, 1);
        }

        public void RequestMailDetail(ulong mailId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_12.write_12_2(msdata, mailId);
            AppNet.gameNet.send(msdata, 12, 2);
        }

        public void SetNewUnreadMail(uint mailCount)
        {
            this._mailTempVo.numUnread = mailCount;
            base.DataUpdateWithParam(7, null);
        }

        public void SetUpdateMailList(List<PMailBasicInfo> mailBasicInfo)
        {
            List<ulong> param = new List<ulong>();
            if (mailBasicInfo.Count <= 0)
            {
                base.DataUpdateWithParam(1, param);
            }
            else
            {
                foreach (PMailBasicInfo info in mailBasicInfo)
                {
                    if (this._mailListVo.AddMail(info) < 0)
                    {
                        param.Add(info.id);
                    }
                }
                base.DataUpdateWithParam(1, param);
            }
        }

        public void UpdateDelMailList(List<ulong> delMailList)
        {
            foreach (ulong num in delMailList)
            {
                this._mailListVo.RemoveMail(num);
                if ((this._mailTempVo.curMailDetailVo != null) && (num == this._mailTempVo.curMailDetailVo.mailId))
                {
                    this.MailTempVo.curMailDetailVo = null;
                }
            }
            base.DataUpdateWithParam(4, delMailList);
        }

        public void UpdateOneKeyAcceptSuccess()
        {
            this.MailTempVo.curMailDetailVo = null;
            this._mailTempVo.numUnread = 0;
            Singleton<MailMode>.Instance.MailTempVo.SelectMails.Clear();
            for (int i = this._mailListVo.MailList.Count - 1; i >= 0; i--)
            {
                PMailBasicInfo info = this._mailListVo.MailList[i];
                if (info.type == 1)
                {
                    info.type = 2;
                    info.isRead = 0;
                    this._mailListVo.MailList.RemoveAt(i);
                    Singleton<MailMode>.Instance.MailTempVo.Select(info.id);
                }
                else
                {
                    this._mailTempVo.numUnread++;
                }
            }
            Debug.Log("选择的邮件数量：" + Singleton<MailMode>.Instance.MailTempVo.SelectMails.Count);
            Singleton<MailMode>.Instance.RequestDeleteSelected();
            base.DataUpdateWithParam(5, null);
        }

        public void UpdateOneyKeyDelSuccess()
        {
            this._mailListVo.MailList.Clear();
            this._mailTempVo.numUnread = 0;
            this.MailTempVo.curMailDetailVo = null;
            base.DataUpdateWithParam(6, null);
        }

        public MailBoxListVo MailListVo
        {
            get
            {
                return this._mailListVo;
            }
        }

        public com.game.module.Mail.MailTempVo MailTempVo
        {
            get
            {
                return this._mailTempVo;
            }
        }
    }
}

