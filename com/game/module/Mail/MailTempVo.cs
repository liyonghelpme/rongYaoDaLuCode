﻿namespace com.game.module.Mail
{
    using Proto;
    using System;
    using System.Collections.Generic;

    public class MailTempVo
    {
        private MailGetInfoMsg_12_2 _curMailDetailInfo;
        private uint _numUnread;
        private List<ulong> _selectMails = new List<ulong>();

        public void Select(ulong mailId)
        {
            if (!this._selectMails.Contains(mailId))
            {
                this._selectMails.Add(mailId);
            }
        }

        public void Unselect(ulong mailId)
        {
            this._selectMails.Remove(mailId);
        }

        public MailGetInfoMsg_12_2 curMailDetailVo
        {
            get
            {
                return this._curMailDetailInfo;
            }
            set
            {
                this._curMailDetailInfo = value;
            }
        }

        public uint numUnread
        {
            get
            {
                return this._numUnread;
            }
            set
            {
                this._numUnread = value;
            }
        }

        public List<ulong> SelectMails
        {
            get
            {
                return this._selectMails;
            }
        }
    }
}

