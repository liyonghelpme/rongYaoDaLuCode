﻿namespace com.game.module.Mail
{
    using com.game.module.core;
    using PCustomDataType;
    using System;

    public class MailUtil
    {
        public static PMailBasicInfo findMailDataByID(ulong id)
        {
            MailBoxListVo mailListVo = Singleton<MailMode>.Instance.MailListVo;
            for (int i = 0; i < mailListVo.MailList.Count; i++)
            {
                PMailBasicInfo info = mailListVo.MailList[i];
                if (info.id == id)
                {
                    return info;
                }
            }
            return null;
        }

        public static int SortMailList(PMailBasicInfo a, PMailBasicInfo b)
        {
            if (a.isRead != b.isRead)
            {
                return (b.isRead - a.isRead);
            }
            return (int) (b.sentTime - a.sentTime);
        }
    }
}

