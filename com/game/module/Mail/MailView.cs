﻿namespace com.game.module.Mail
{
    using com.game.manager;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MailView : BaseView<MailView>
    {
        private GameObject _bg2Go;
        private GameObject _bgGo;
        private Button _btnClose;
        private Button _btnOneKeyDel;
        private Button _btnOneKeyExtract;
        private MailItem _curMailView;
        private MailAttachmentView _detailView;
        private UIGrid _mailGrid;
        private UIScrollView _sv;
        private Vector3 _svStartPos;
        private UILabel _titleLabel;
        private List<MailItem> _viewList = new List<MailItem>();
        [CompilerGenerated]
        private static Predicate<PMailBasicInfo> <>f__am$cacheC;

        public override void CancelUpdateHandler()
        {
            this._btnClose.onClick = null;
            this._btnOneKeyExtract.onClick = null;
            this._btnOneKeyDel.onClick = null;
            Singleton<MailMode>.Instance.dataUpdatedWithParam -= new DataUpateHandlerWithParam(this.OnMailEvent);
        }

        private void ClearView(bool destroy)
        {
            int num = !destroy ? -1 : 0;
            for (int i = this._viewList.Count - 1; i > num; i--)
            {
                if (destroy)
                {
                    UnityEngine.Object.Destroy(this._viewList[i].go);
                    this._viewList.RemoveAt(i);
                }
                else
                {
                    this._viewList[i].info = null;
                }
            }
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdateView(true);
        }

        protected override void HandleBeforeCloseView()
        {
            if (this._detailView != null)
            {
                this._detailView.CloseView();
            }
            if (this._curMailView != null)
            {
                this._curMailView.selected = false;
                this._curMailView = null;
            }
            Singleton<MailMode>.Instance.MailTempVo.SelectMails.Clear();
        }

        protected override void Init()
        {
            MailItem item = new MailItem(NGUITools.FindChild(this.gameObject, "left/grid/item_0"), new Action<MailItem>(this.OnClickMail)) {
                info = null
            };
            this._viewList.Add(item);
            this._bgGo = NGUITools.FindChild(this.gameObject, "bgs");
            this._bg2Go = NGUITools.FindChild(this.gameObject, "bg2");
            this._sv = base.FindInChild<UIScrollView>("left");
            this._svStartPos = this._sv.transform.localPosition;
            this._mailGrid = base.FindInChild<UIGrid>("left/grid");
            this._titleLabel = base.FindInChild<UILabel>("title/label");
            this._btnClose = base.FindInChild<Button>("btn_close");
            this._btnOneKeyExtract = base.FindInChild<Button>("btnOneKeyExtract");
            this._btnOneKeyDel = base.FindInChild<Button>("btnOneKeyDel");
            this._titleLabel.text = LanguageManager.GetWord("Game.Mail");
            this._btnOneKeyExtract.FindInChild<UILabel>("label").GetComponent<UILabel>().text = LanguageManager.GetWord("Game.onekeyextract");
            this._detailView = new MailAttachmentView(NGUITools.FindChild(this.gameObject, "right"));
        }

        private void OnClickClose(GameObject go)
        {
            this.CloseView();
        }

        private void OnClickMail(MailItem mail)
        {
            if (mail != this._curMailView)
            {
                if (this._curMailView != null)
                {
                    this._curMailView.selected = false;
                }
                this._curMailView = mail;
                this._curMailView.selected = true;
                if ((this._curMailView != null) && (this._curMailView.info != null))
                {
                    Singleton<MailMode>.Instance.RequestMailDetail(this._curMailView.info.id);
                }
            }
        }

        private void OnClickOneKeyDel(GameObject go)
        {
            Singleton<MailMode>.Instance.OneKeyDelAll();
            this._btnOneKeyDel.SetActive(false);
        }

        private void OnClickOneKeyExtract(GameObject go)
        {
            Singleton<MailMode>.Instance.OneyKeyGetAllAttach();
        }

        private void OnMailEvent(object sender, int code, object data)
        {
            if ((code == 4) && (this._curMailView != null))
            {
                if (this._curMailView.info == null)
                {
                    this._curMailView.selected = false;
                    this._curMailView = null;
                }
                else
                {
                    List<ulong> list = (List<ulong>) data;
                    if ((list != null) && list.Contains(this._curMailView.info.id))
                    {
                        this._curMailView.selected = false;
                        this._curMailView = null;
                    }
                }
            }
            this.UpdateView(false);
        }

        public override void RegisterUpdateHandler()
        {
            this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickClose);
            this._btnOneKeyExtract.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickOneKeyExtract);
            this._btnOneKeyDel.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickOneKeyDel);
            Singleton<MailMode>.Instance.dataUpdatedWithParam += new DataUpateHandlerWithParam(this.OnMailEvent);
        }

        private void ReLayout()
        {
            bool flag = Singleton<MailMode>.Instance.MailListVo.MailList.Count > 0;
            this._bgGo.SetActive(flag);
            this._bg2Go.SetActive(!flag);
            if (!this._mailGrid.gameObject.activeInHierarchy && flag)
            {
                this._sv.MoveRelative(this._svStartPos - this._sv.transform.localPosition);
            }
            this._mailGrid.SetActive(Singleton<MailMode>.Instance.MailListVo.MailList.Count > 0);
            if (<>f__am$cacheC == null)
            {
                <>f__am$cacheC = p => p.type == 1;
            }
            bool active = Singleton<MailMode>.Instance.MailListVo.MailList.Exists(<>f__am$cacheC);
            this._btnOneKeyExtract.SetActive(active);
            this._detailView.SetActive(flag);
            if (flag)
            {
                this._detailView.UpdateView();
            }
        }

        private void UpdateView(bool sort)
        {
            if (Singleton<MailMode>.Instance.MailListVo.MailList.Count > 0)
            {
                if (sort)
                {
                    Singleton<MailMode>.Instance.MailListVo.MailList.Sort(new Comparison<PMailBasicInfo>(MailUtil.SortMailList));
                }
            }
            else if (this._curMailView != null)
            {
                this._curMailView.selected = false;
                this._curMailView = null;
            }
            while (this._viewList.Count < Singleton<MailMode>.Instance.MailListVo.MailList.Count)
            {
                MailItem item = new MailItem(NGUITools.AddChild(this._mailGrid.gameObject, this._viewList[0].go), new Action<MailItem>(this.OnClickMail));
                this._viewList.Add(item);
            }
            int num = 0;
            while (num < Singleton<MailMode>.Instance.MailListVo.MailList.Count)
            {
                this._viewList[num].info = Singleton<MailMode>.Instance.MailListVo.MailList[num];
                num++;
            }
            while (num < this._viewList.Count)
            {
                this._viewList[num].info = null;
                num++;
            }
            this._mailGrid.Reposition();
            this.ReLayout();
        }

        public override bool IsFullUI
        {
            get
            {
                return true;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Mail/MailView.assetbundle";
            }
        }
    }
}

