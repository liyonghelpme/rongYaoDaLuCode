﻿namespace com.game.module.Manager
{
    using com.game.manager;
    using com.game.module.core;
    using com.u3d.bases.debug;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AtlasManager : Singleton<AtlasManager>
    {
        private Dictionary<string, UIAtlas> atlasDic = new Dictionary<string, UIAtlas>();
        public const string BigHeader = "BigHeader";
        private int cacheLength = 5;
        public const string Chat = "ChatAtlas";
        public const string Common = "Common";
        public const string Header = "Header";
        private bool isCache;
        public const string Item = "ItemAtlas";
        public const string Items = "ItemIconAtlas";
        private LoadCallBack loadCallBack;
        public LoadFinished LoadFinishedCallBack;
        public const string NewCommon = "NewCommonAtlas";

        public void AddAtlas(string name, UIAtlas atlas)
        {
            this.atlasDic.Add(name, atlas);
        }

        public UIAtlas GetAtlas(string name)
        {
            UIAtlas atlas;
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            if (!this.atlasDic.TryGetValue(name, out atlas))
            {
                Debug.Log("Can't find in Atlas Cache , Please Load it first");
                Log.info(this, "Can't find in Atlas Cache , Please Load it first!");
            }
            return atlas;
        }

        public void Init()
        {
            this.LoadAtlasHold("UI/Icon/SkillIcon/SkillIconHold.assetbundle", "SkillIcon", null, true);
            this.LoadAtlas("UI/Common/NewCommonAtlas.assetbundle", "NewCommonAtlas");
            this.LoadAtlas("UI/Common/Common.assetbundle", "Common");
            this.LoadAtlas("UI/Icon/Header/Header.assetbundle", "Header");
            this.LoadAtlas("UI/Icon/Header/BigHeader.assetbundle", "BigHeader");
            this.LoadAtlas("UI/Icon/ChatIcon/ChatAtlas.assetbundle", "ChatAtlas");
            this.LoadAtlas("UI/Icon/item/ItemIconAtlas.assetbundle", "ItemIconAtlas");
        }

        [Obsolete("Use 'LoadAtlas(string url, string name, LoadCallBack loadCallBack, bool isCache = false)' instead")]
        private void LoadAtlas(string url, string name)
        {
            AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.LoadAtlasCallBack), name, false, true);
        }

        public void LoadAtlas(string url, string name, LoadCallBack loadCallBack, bool isCache = false)
        {
            this.loadCallBack = loadCallBack;
            this.isCache = isCache;
            if (this.atlasDic.ContainsKey(name))
            {
                loadCallBack(this.atlasDic[name]);
            }
            else
            {
                AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.LoadAtlasCallBack1), name, false, true);
            }
        }

        private void LoadAtlasCallBack(GameObject asset)
        {
            if (!this.atlasDic.ContainsKey(asset.name))
            {
                this.atlasDic.Add(asset.name, asset.GetComponent<UIAtlas>());
            }
            if (this.LoadFinishedCallBack != null)
            {
                this.LoadFinishedCallBack();
            }
        }

        private void LoadAtlasCallBack1(GameObject asset)
        {
            if ((asset.GetComponent<UIAtlas>() != null) && this.isCache)
            {
                this.atlasDic.Add(asset.name, asset.GetComponent<UIAtlas>());
            }
        }

        public void LoadAtlasHold(string holdUrl, string normalName, LoadCallBack loadCallBack, bool isCache = false)
        {
            this.loadCallBack = loadCallBack;
            this.isCache = isCache;
            if (this.atlasDic.ContainsKey(normalName))
            {
                loadCallBack(this.atlasDic[normalName]);
            }
            else
            {
                AssetManager.Instance.LoadAsset<GameObject>(holdUrl, new LoadAssetFinish<GameObject>(this.LoadAtlasHoldCallBack), null, false, true);
            }
        }

        private void LoadAtlasHoldCallBack(GameObject asset)
        {
            UIAtlas atlas = asset.GetComponent<UISprite>().atlas;
            if (atlas != null)
            {
                this.atlasDic.Add(atlas.name, atlas);
                if (this.loadCallBack != null)
                {
                    this.loadCallBack(atlas);
                }
            }
        }

        public void RemoveAtlas(string name)
        {
            this.atlasDic.Remove(name);
        }
    }
}

