﻿namespace com.game.module.Manager
{
    using System;

    public class AtlasUrl
    {
        public const string BigHeaderIcon = "BigHeader";
        public const string BigHeaderURL = "UI/Icon/Header/BigHeader.assetbundle";
        public const string ChatURL = "UI/Icon/ChatIcon/ChatAtlas.assetbundle";
        public const string CommonURL = "UI/Common/Common.assetbundle";
        public const string CopyIconGray = "CopyIconGrayAtlas";
        public const string CopyIconHold = "UI/Icon/CopyMapIcon/CopyIconHold.assetbundle";
        public const string CopyIconNormal = "CopyIconAtlas";
        public const string EquipIconGray = "EquipIcon_Gray";
        public const string EquipIconHold = "UI/Icon/EquipIcon/EquipIconHold.assetbundle";
        public const string EquipIconNormal = "EquipIcon";
        public const string GemIcon = "GemIcon";
        public const string GemIconURL = "UI/Icon/GemIcon/GemIcon.assetbundle";
        public const string HeaderIcon = "Header";
        public const string HeaderIconURL = "UI/Icon/Header/Header.assetbundle";
        public const string HeaderURL = "UI/Icon/Header/Header.assetbundle";
        public const string ItemIconURL = "UI/Icon/item/ItemIconAtlas.assetbundle";
        public const string NewCommonURL = "UI/Common/NewCommonAtlas.assetbundle";
        public const string PropIcon = "PropIcon";
        public const string PropIconURL = "UI/Icon/PropIcon/PropIcon.assetbundle";
        public const string SkillIconGray = "SkillIcon_Gray";
        public const string SkillIconHold = "UI/Icon/SkillIcon/SkillIconHold.assetbundle";
        public const string SkillIconNormal = "SkillIcon";
        public const string SkillTalentIcon = "UI/Icon/SkillTalent/TalentIcon.assetbundle";
        public const string SkillTalentNormal = "TalentIcon";
    }
}

