﻿namespace com.game.module.Manager
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.u3d.bases.debug;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class ItemManager : Singleton<ItemManager>
    {
        public readonly uint ADD_ICON = 2;
        public readonly uint EMPTY_ICON = 1;
        public readonly uint LOCK_ICON;

        public void InitItem(UIWidgetContainer item, uint templateId, ItemType type)
        {
            int color = 1;
            string name = string.Empty;
            UIAtlas atlas = null;
            this.InitSpriteHelp(item, "border", string.Empty, string.Empty);
            if (templateId == this.LOCK_ICON)
            {
                this.InitSpriteHelp(item, "background", "common", "pz_1");
                this.InitSpriteHelp(item, "icon", "common", "suo23");
            }
            else if (templateId == this.EMPTY_ICON)
            {
                this.InitSpriteHelp(item, "background", "common", "pz_1");
                this.InitSpriteHelp(item, "icon", "common", "pzkong");
            }
            else if (templateId == this.ADD_ICON)
            {
                this.InitSpriteHelp(item, "background", "common", "pz_1");
                this.InitSpriteHelp(item, "icon", "common", "add");
            }
            else if (templateId > 0x186a0)
            {
                SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(templateId);
                color = dataById.color;
                name = string.Empty + dataById.icon;
                if (dataById.type == 3)
                {
                    atlas = Singleton<AtlasManager>.Instance.GetAtlas("GemIcon");
                }
                else
                {
                    atlas = Singleton<AtlasManager>.Instance.GetAtlas("PropIcon");
                }
                if ((atlas == null) || (atlas.GetSprite(name) == null))
                {
                    name = "100000";
                }
                item.gameObject.SetChildActive("color", false);
                this.InitSpriteHelp(item, "icon", atlas, name);
                this.InitSpriteHelp(item, "background", "common", "pz_" + color);
            }
        }

        public void InitItem(GameObject item, uint templateId, ItemType type)
        {
            int color = 1;
            string name = string.Empty;
            UIAtlas atlas = null;
            if (templateId == this.LOCK_ICON)
            {
                this.InitSpriteHelp(item, "background", "common", "pz_1");
                this.InitSpriteHelp(item, "icon", "common", "suo23");
            }
            else if (templateId == this.EMPTY_ICON)
            {
                this.InitSpriteHelp(item, "background", "common", "pz_1");
                this.InitSpriteHelp(item, "icon", "common", "pzkong");
            }
            else if (templateId == this.ADD_ICON)
            {
                this.InitSpriteHelp(item, "background", "common", "pz_1");
                this.InitSpriteHelp(item, "icon", "common", "add");
            }
            else if (templateId > 0x186a0)
            {
                SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(templateId);
                color = dataById.color;
                name = string.Empty + dataById.icon;
                if (dataById.type == 3)
                {
                    atlas = Singleton<AtlasManager>.Instance.GetAtlas("GemIcon");
                }
                else
                {
                    atlas = Singleton<AtlasManager>.Instance.GetAtlas("PropIcon");
                }
                if ((atlas == null) || (atlas.GetSprite(name) == null))
                {
                    name = "100000";
                }
                item.SetChildActive("color", false);
                this.InitSpriteHelp(item, "icon", atlas, name);
                this.InitSpriteHelp(item, "background", "common", "pz_" + color);
            }
        }

        public void InitItemByUId(UIWidgetContainer item, uint uid, ItemType type)
        {
            int color = 1;
            string name = string.Empty;
            UIAtlas atlas = null;
            PItems pItemById = Singleton<BagMode>.Instance.GetPItemById(uid);
            if (pItemById == null)
            {
                Log.info(this, "背包没有找到改唯一id的物品");
            }
            else
            {
                uint templateId = pItemById.templateId;
                if (templateId > 0x186a0)
                {
                    SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(templateId);
                    color = dataById.color;
                    name = string.Empty + dataById.icon;
                    if (dataById.type == 3)
                    {
                        atlas = Singleton<AtlasManager>.Instance.GetAtlas("GemIcon");
                    }
                    else
                    {
                        atlas = Singleton<AtlasManager>.Instance.GetAtlas("PropIcon");
                    }
                    if ((atlas == null) || (atlas.GetSprite(name) == null))
                    {
                        name = "100000";
                    }
                    item.gameObject.SetChildActive("color", false);
                    this.InitSpriteHelp(item, "icon", atlas, name);
                    this.InitSpriteHelp(item, "background", "common", "pz_" + color);
                }
            }
        }

        public void InitItemByUId(GameObject item, uint uid, ItemType type)
        {
            int color = 1;
            string name = string.Empty;
            UIAtlas atlas = null;
            PItems pItemById = Singleton<BagMode>.Instance.GetPItemById(uid);
            if (pItemById == null)
            {
                Log.info(this, "背包没有找到改唯一id的物品");
            }
            else
            {
                uint templateId = pItemById.templateId;
                if (templateId > 0x186a0)
                {
                    SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(templateId);
                    color = dataById.color;
                    name = string.Empty + dataById.icon;
                    if (dataById.type == 3)
                    {
                        atlas = Singleton<AtlasManager>.Instance.GetAtlas("GemIcon");
                    }
                    else
                    {
                        atlas = Singleton<AtlasManager>.Instance.GetAtlas("PropIcon");
                    }
                    if ((atlas == null) || (atlas.GetSprite(name) == null))
                    {
                        name = "100000";
                    }
                    item.SetChildActive("color", false);
                    this.InitSpriteHelp(item, "icon", atlas, name);
                    this.InitSpriteHelp(item, "background", "common", "pz_" + color);
                }
            }
        }

        private void InitSpriteHelp(UIWidgetContainer item, string path, string atlas, string spriteName)
        {
            UISprite sprite = item.FindInChild<UISprite>(path);
            if (string.IsNullOrEmpty(spriteName))
            {
                atlas = null;
            }
            if (sprite != null)
            {
                sprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas(atlas);
                sprite.spriteName = spriteName;
            }
        }

        private void InitSpriteHelp(UIWidgetContainer item, string path, UIAtlas atlas, string spriteName)
        {
            UISprite sprite = item.FindInChild<UISprite>(path);
            if (string.IsNullOrEmpty(spriteName))
            {
                atlas = null;
            }
            if (sprite != null)
            {
                sprite.atlas = atlas;
                sprite.spriteName = spriteName;
            }
        }

        private void InitSpriteHelp(GameObject item, string path, string atlas, string spriteName)
        {
            UISprite sprite = NGUITools.FindInChild<UISprite>(item, path);
            if (string.IsNullOrEmpty(spriteName))
            {
                atlas = null;
            }
            if (sprite != null)
            {
                sprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas(atlas);
                sprite.spriteName = spriteName;
            }
        }

        private void InitSpriteHelp(GameObject item, string path, UIAtlas atlas, string spriteName)
        {
            UISprite sprite = NGUITools.FindInChild<UISprite>(item, path);
            if (string.IsNullOrEmpty(spriteName))
            {
                atlas = null;
            }
            if (sprite != null)
            {
                sprite.atlas = atlas;
                sprite.spriteName = spriteName;
            }
        }
    }
}

