﻿namespace com.game.module.NPCBust
{
    using com.game.manager;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class NPCBustMgr
    {
        private string blackNPCId = "90000";
        private BustCreatedCallback bustCallBack;
        private IDictionary<string, GameObject> bustDictionary = new Dictionary<string, GameObject>();
        private GameObject bustRoot = NGUITools.AddChild(ViewTree.go);
        private string bustUrl;
        public static NPCBustMgr Instance = ((Instance != null) ? Instance : new NPCBustMgr());
        public const string SpecialNPCId = "10001";
        public const float ZoomFactor = 100f;

        private NPCBustMgr()
        {
            this.bustRoot.name = "BustRoot";
            NGUITools.SetLayer(this.bustRoot, LayerMask.NameToLayer("UI"));
            UnityEngine.Object.DontDestroyOnLoad(this.bustRoot);
        }

        private void BustLoaded(GameObject obj)
        {
            if (null == obj)
            {
                AssetManager.Instance.LoadAsset<GameObject>(this.blackBustUrl, new LoadAssetFinish<GameObject>(this.BustLoaded), null, false, true);
            }
            else
            {
                GameObject obj2 = UnityEngine.Object.Instantiate(obj) as GameObject;
                GameObject go = new GameObject("bust");
                obj2.transform.parent = go.transform;
                go.SetActive(false);
                NGUITools.SetLayer(go, LayerMask.NameToLayer("UI"));
                obj2.transform.localPosition = Vector3.zero;
                obj2.transform.localRotation = Quaternion.identity;
                obj2.transform.localScale = Vector3.one;
                go.transform.parent = this.bustRoot.transform;
                go.transform.position = Vector3.zero;
                go.transform.localRotation = Quaternion.identity;
                go.transform.localScale = new Vector3(100f, 100f, 100f);
                this.bustDictionary[this.bustUrl] = go;
                this.bustCallBack(go);
            }
        }

        public void GetBust(string url, BustCreatedCallback callBack)
        {
            this.bustUrl = url;
            this.bustCallBack = callBack;
            if (this.bustDictionary.ContainsKey(this.bustUrl))
            {
                this.bustCallBack(this.bustDictionary[this.bustUrl]);
            }
            else
            {
                AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.BustLoaded), null, false, true);
            }
        }

        private string blackBustUrl
        {
            get
            {
                return UrlUtils.npcBustUrl(this.blackNPCId);
            }
        }

        public delegate void BustCreatedCallback(GameObject bustGo);
    }
}

