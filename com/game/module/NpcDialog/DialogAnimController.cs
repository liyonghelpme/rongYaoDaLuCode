﻿namespace com.game.module.NpcDialog
{
    using System;
    using UnityEngine;

    public class DialogAnimController : MonoBehaviour
    {
        private int _curIndex;
        private float _lastTime;
        private float _lastUpdateTime;
        private DialogAnimPanel _panel;
        private int _totalCount;
        private static readonly float TIME_GAP = 20f;

        public void BeginAnim(DialogAnimPanel panel, int totalCount)
        {
            base.enabled = true;
            this._panel = panel;
            this._totalCount = totalCount;
            this._curIndex = 0;
            this._lastTime = Time.time;
            this._panel.SetLineIndex(this._curIndex);
        }

        public void EndAnim()
        {
            base.enabled = false;
            this._panel = null;
            this._totalCount = 0;
            this._curIndex = 0;
        }

        private void NextLine()
        {
            this._curIndex = this._panel.currentIndex;
            this._curIndex++;
            if (this._curIndex < this._totalCount)
            {
                this._panel.SetLineIndex(this._curIndex);
                this._lastTime = Time.time;
            }
        }

        private void Update()
        {
            if ((Time.time - this._lastUpdateTime) >= 0.2f)
            {
                this._lastUpdateTime = Time.time;
                if ((this._panel != null) && ((Time.time - this._lastTime) >= TIME_GAP))
                {
                    this.NextLine();
                }
            }
        }
    }
}

