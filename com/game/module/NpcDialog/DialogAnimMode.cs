﻿namespace com.game.module.NpcDialog
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;

    public class DialogAnimMode : BaseMode<DialogAnimMode>
    {
        private bool _isPlaying;

        public List<DialogAnimItemInfo> ParseDialog(string text)
        {
            List<DialogAnimItemInfo> list = new List<DialogAnimItemInfo>();
            char[] separator = new char[] { '|' };
            string[] strArray = text.Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] chArray2 = new char[] { '#' };
                string[] strArray2 = strArray[i].Split(chArray2);
                DialogAnimItemInfo item = new DialogAnimItemInfo {
                    roleId = int.Parse(strArray2[0]),
                    content = strArray2[1]
                };
                list.Add(item);
            }
            return list;
        }

        public void SetIsPlaying(bool value)
        {
            this._isPlaying = value;
        }

        public bool isPlaying
        {
            get
            {
                return this._isPlaying;
            }
        }
    }
}

