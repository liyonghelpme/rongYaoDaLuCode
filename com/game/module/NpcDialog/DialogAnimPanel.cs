﻿namespace com.game.module.NpcDialog
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class DialogAnimPanel : BaseView<DialogAnimPanel>
    {
        private Button _btn;
        private DialogAnimController _controller;
        private UISprite _icon1;
        private UISprite _icon2;
        private UILabel _label;
        private List<DialogAnimItemInfo> _list;
        public int currentIndex;
        private FinishDialogCallback finishCallback;

        protected override void HandleAfterOpenView()
        {
            if (null == this._controller)
            {
                DialogAnimController component = this.gameObject.GetComponent<DialogAnimController>();
                if (component == null)
                {
                }
                this._controller = this.gameObject.AddComponent<DialogAnimController>();
            }
            this._controller.BeginAnim(this, this._list.Count);
        }

        protected override void HandleBeforeCloseView()
        {
            this._controller.EndAnim();
            base.HandleBeforeCloseView();
            Singleton<DialogAnimMode>.Instance.SetIsPlaying(false);
            if (this.finishCallback != null)
            {
                this.finishCallback();
                this.finishCallback = null;
            }
        }

        protected override void Init()
        {
            this._label = base.FindInChild<UILabel>("DialogPanel/label");
            this._btn = base.FindInChild<Button>("DialogPanel/btn");
            this._icon1 = base.FindInChild<UISprite>("DialogPanel/headIcon1");
            this._icon2 = base.FindInChild<UISprite>("DialogPanel/headIcon1");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this._btn.onClick = new UIWidgetContainer.VoidDelegate(this.SkipBtnClickHandler);
        }

        public void SetLineIndex(int index)
        {
            this.currentIndex = index;
            this._icon1.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this._icon1.spriteName = this._list[index].roleId.ToString();
            this._icon1.MakePixelPerfect();
            this._label.text = this._list[index].content;
        }

        public void Show(string content, FinishDialogCallback callback = null)
        {
            if (!string.IsNullOrEmpty(content))
            {
                this.currentIndex = 0;
                this._list = Singleton<DialogAnimMode>.Instance.ParseDialog(content);
                if ((this._list != null) && (this._list.Count > 0))
                {
                    this.finishCallback = callback;
                    Singleton<DialogAnimMode>.Instance.SetIsPlaying(true);
                    this.OpenView();
                }
            }
        }

        public void Show(uint despId, System.Action act = null)
        {
            this.Show(DescriptManager.GetText(despId), null);
        }

        private void SkipBtnClickHandler(GameObject go)
        {
            this.currentIndex++;
            if (this.currentIndex >= this._list.Count)
            {
                this.currentIndex = 0;
                this.CloseView();
            }
            else
            {
                this.SetLineIndex(this.currentIndex);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopUILayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/DialogAnim/Dialog.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

