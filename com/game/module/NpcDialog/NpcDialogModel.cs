﻿namespace com.game.module.NpcDialog
{
    using com.game.module.core;
    using com.game.module.Task;
    using System;

    public class NpcDialogModel : BaseMode<NpcDialogModel>
    {
        public uint CurrentNpcId;
        public TaskVo CurrentTaskVo;
    }
}

