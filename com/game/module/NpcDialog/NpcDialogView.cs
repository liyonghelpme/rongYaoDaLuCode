﻿namespace com.game.module.NpcDialog
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.NPCBust;
    using com.game.module.Task;
    using com.game.Public.Message;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class NpcDialogView : BaseView<NpcDialogView>
    {
        private uint _currentNpcId;
        private int _currentSpeak;
        private SysNpcVo _currentSysNpcVo;
        private TaskVo _currentTaskVo;
        private Button _dialogButton;
        private UILabel _dialogNpcLabel;
        private UILabel _dialogPlayerLabel;
        private bool _hasTask;
        private bool _isDialog;
        private bool _isType;
        private NpcDialogModel _npcDialogModel;
        private GameObject _npcFunctionItem;
        private GameObject _npcHead;
        private UILabel _npcNameLabel;
        private UISprite _npcSprite;
        private GameObject _playerHead;
        private UILabel _playerNameLabel;
        private UISprite _playerSprite;
        private UISprite _speakBackground;
        private int _talkIndex;
        private TaskModel _taskModel;
        private List<string> _wordsList;
        private bool canSwitch;
        private GameObject npcBust;
        private const int NpcSpeaker = 1;
        private const int PlayerSpeaker = 2;
        private GameObject roleBust;

        private void AnalysWords()
        {
            this._wordsList.Clear();
            this._talkIndex = 0;
            this._currentTaskVo = this._npcDialogModel.CurrentTaskVo;
            string str = string.Empty;
            switch (this._currentTaskVo.Statu)
            {
                case TaskStatu.StatuUnAccept:
                    str = this._currentTaskVo.SysTaskVo.talk_accept;
                    break;

                case TaskStatu.StatuAccepted:
                    if (this._currentTaskVo.CanCommit)
                    {
                        str = this._currentTaskVo.SysTaskVo.talk_com;
                    }
                    else
                    {
                        str = this._currentTaskVo.SysTaskVo.talk_uncom;
                    }
                    break;
            }
            char ch2 = '\x0001';
            char[] separator = new char[] { '\x0001' };
            foreach (string str2 in str.Replace("#n", ch2.ToString()).Split(separator))
            {
                if (str2.Length > 0)
                {
                    this._wordsList.Add(str2);
                }
            }
        }

        private void EndDialog()
        {
            switch (this._currentTaskVo.Statu)
            {
                case TaskStatu.StatuUnAccept:
                    this._taskModel.AccetpTask(this._currentTaskVo.TaskId, this._npcDialogModel.CurrentNpcId);
                    break;

                case TaskStatu.StatuAccepted:
                    if (this._currentTaskVo.CanCommit)
                    {
                        this._taskModel.CommitTask(this._currentTaskVo.TaskId, this._npcDialogModel.CurrentNpcId);
                    }
                    else
                    {
                        MessageManager.Show(LanguageManager.GetWord("NpcDialogView.NotFinished"));
                    }
                    break;
            }
            this.CloseView();
            Singleton<MainView>.Instance.OpenView();
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this._currentNpcId = this._npcDialogModel.CurrentNpcId;
            this._currentSysNpcVo = BaseDataMgr.instance.GetNpcVoById(this._currentNpcId);
            this.ShowRightInfo();
            Singleton<MainView>.Instance.CloseView();
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
            this.HideAllBust();
        }

        private void HandleFunction()
        {
            this.CloseView();
            Singleton<MainView>.Instance.OpenView();
        }

        private void HandleTask()
        {
            if (this._talkIndex >= this._wordsList.Count)
            {
                this.EndDialog();
            }
            else
            {
                if (this._currentSpeak == 2)
                {
                    this._currentSpeak = 1;
                    this._talkIndex++;
                }
                else
                {
                    this._currentSpeak = 2;
                }
                this.ShowRoleDialog();
            }
        }

        private void HideAllBust()
        {
            if (null != this.roleBust)
            {
                this.roleBust.SetActive(false);
            }
            if (null != this.npcBust)
            {
                this.npcBust.SetActive(false);
            }
        }

        protected override void Init()
        {
            this._npcDialogModel = Singleton<NpcDialogModel>.Instance;
            this._dialogButton = base.FindChild("Buttom/Button").GetComponent<Button>();
            this._dialogButton.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._dialogButton.onClick, new UIWidgetContainer.VoidDelegate(this.OnDialogButtonClick));
            this._dialogPlayerLabel = base.FindChild("Buttom/Content/leftLabel").GetComponent<UILabel>();
            this._dialogNpcLabel = base.FindChild("Buttom/Content/rightLabel").GetComponent<UILabel>();
            this._speakBackground = base.FindChild("Buttom/Content/Background").GetComponent<UISprite>();
            this._npcHead = base.FindChild("Buttom/Npc");
            this._npcNameLabel = base.FindChild("Buttom/Npc/Name").GetComponent<UILabel>();
            this._npcSprite = base.FindChild("Buttom/Npc/FaceIcon").GetComponent<UISprite>();
            this._playerHead = base.FindChild("Buttom/Player");
            this._playerNameLabel = base.FindChild("Buttom/Player/Name").GetComponent<UILabel>();
            this._playerSprite = base.FindChild("Buttom/Player/FaceIcon").GetComponent<UISprite>();
            this._taskModel = Singleton<TaskModel>.Instance;
            this._npcDialogModel = Singleton<NpcDialogModel>.Instance;
            this._wordsList = new List<string>();
        }

        private void NPCBustLoaded(GameObject bustObj)
        {
            this.npcBust = bustObj;
            this.npcBust.name = "npc_" + this._currentSysNpcVo.npcId + "_bust";
            this.npcBust.transform.position = this._npcSprite.transform.position;
            this.npcBust.SetActive(true);
            this.canSwitch = true;
        }

        private void OnDialogButtonClick(GameObject button)
        {
            if (!this._isType && this.canSwitch)
            {
                if (this._hasTask)
                {
                    this.HandleTask();
                }
                else
                {
                    this.HandleFunction();
                }
            }
        }

        private void OpenNpcFunctionView()
        {
        }

        private void OpenTaskInfo()
        {
        }

        private void ResetIsTypeStatu()
        {
            this._isType = false;
        }

        private void RoleBustLoaded(GameObject bustObj)
        {
            this.roleBust = bustObj;
            this.roleBust.name = "my_bust";
            this.roleBust.transform.position = this._playerSprite.transform.position;
            MeVo.instance.InitRoleBustPosition(this.roleBust);
            this.roleBust.SetActive(true);
            this.canSwitch = true;
        }

        private void SetDialogTypeWriter(int charsPerSecond)
        {
            TypewriterEffect component = this._dialogLabel.GetComponent<TypewriterEffect>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
        }

        private void ShowFunctionInfo()
        {
            this.canSwitch = false;
            this._hasTask = false;
            this._currentSpeak = 1;
            this._playerHead.gameObject.SetActive(false);
            this._npcHead.gameObject.SetActive(true);
            this._npcSprite.SetActive(false);
            this._dialogPlayerLabel.SetActive(false);
            this._dialogNpcLabel.SetActive(true);
            Vector3 localScale = this._speakBackground.transform.localScale;
            localScale.x = 1f;
            this._speakBackground.transform.localScale = localScale;
            this._dialogLabel.supportEncoding = true;
            this._npcSprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this._npcSprite.spriteName = this._currentSysNpcVo.npcId.ToString();
            this._npcNameLabel.text = this._currentSysNpcVo.name;
        }

        private void ShowRightInfo()
        {
            if ((this._taskModel.CurrentMainTaskVo != null) && (this._taskModel.CurrentMainTaskVo.TargetNpcId == this._currentNpcId))
            {
                this._npcDialogModel.CurrentTaskVo = this._taskModel.CurrentMainTaskVo;
                this.ShowTaskInfo();
            }
            else if ((this._taskModel.CurrentSubTaskVo != null) && (this._taskModel.CurrentSubTaskVo.TargetNpcId == this._currentNpcId))
            {
                this._npcDialogModel.CurrentTaskVo = this._taskModel.CurrentSubTaskVo;
                this.ShowTaskInfo();
            }
            else
            {
                this.ShowFunctionInfo();
            }
        }

        private void ShowRoleDialog()
        {
            if (this._talkIndex >= this._wordsList.Count)
            {
                this.EndDialog();
            }
            else
            {
                string str = this._wordsList[this._talkIndex];
                char ch2 = '\x0001';
                char[] separator = new char[] { '\x0001' };
                string[] strArray = str.Replace("[playername]", "[00ff00]" + MeVo.instance.Name + "[-]").Replace("#r", ch2.ToString()).Split(separator);
                this.HideAllBust();
                this.canSwitch = false;
                if (this._currentSpeak == 1)
                {
                    this._playerHead.gameObject.SetActive(false);
                    this._npcHead.gameObject.SetActive(true);
                    this._dialogPlayerLabel.SetActive(false);
                    this._dialogNpcLabel.SetActive(true);
                    this.SetDialogTypeWriter(20);
                    this._dialogLabel.supportEncoding = true;
                    this._dialogLabel.text = strArray[0];
                    Vector3 localScale = this._speakBackground.transform.localScale;
                    localScale.x = 1f;
                    this._speakBackground.transform.localScale = localScale;
                    this._npcNameLabel.text = this._currentSysNpcVo.name;
                    this._npcSprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                    this._npcSprite.spriteName = this._currentSysNpcVo.npcId.ToString();
                    this._npcSprite.SetActive(false);
                }
                else if (this._currentSpeak == 2)
                {
                    if (strArray.Length > 1)
                    {
                        this._npcHead.gameObject.SetActive(false);
                        this._playerHead.gameObject.SetActive(true);
                        this._dialogPlayerLabel.SetActive(true);
                        this._dialogNpcLabel.SetActive(false);
                        this.SetDialogTypeWriter(20);
                        this._dialogLabel.supportEncoding = true;
                        this._dialogLabel.text = strArray[1];
                        Vector3 vector2 = this._speakBackground.transform.localScale;
                        vector2.x = -1f;
                        this._speakBackground.transform.localScale = vector2;
                        this._playerNameLabel.text = MeVo.instance.Name;
                        this._playerSprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                        this._playerSprite.spriteName = "101";
                        this._playerSprite.SetActive(false);
                        NPCBustMgr.Instance.GetBust(MeVo.instance.BustUrl, new NPCBustMgr.BustCreatedCallback(this.RoleBustLoaded));
                    }
                    else
                    {
                        this.EndDialog();
                    }
                }
            }
        }

        private void ShowTaskInfo()
        {
            this._hasTask = true;
            this.AnalysWords();
            this._currentSpeak = 1;
            this.ShowRoleDialog();
        }

        private void ShowTaskStatu()
        {
            string str2;
            string word = LanguageManager.GetWord("NpcDialogView.TaskReward");
            if (this._currentTaskVo.SysTaskVo.exp > 0)
            {
                str2 = word;
                object[] objArray1 = new object[] { str2, LanguageManager.GetWord("NpcDialogView.Exp"), " [00ff00]", this._currentTaskVo.SysTaskVo.exp, "[-]  " };
                word = string.Concat(objArray1);
            }
            if (this._currentTaskVo.SysTaskVo.gold > 0)
            {
                str2 = word;
                object[] objArray2 = new object[] { str2, LanguageManager.GetWord("NpcDialogView.Gold"), " [00ff00]", this._currentTaskVo.SysTaskVo.gold, "[-] " };
                word = string.Concat(objArray2);
            }
            this._dialogLabel.supportEncoding = true;
            this._dialogLabel.text = word;
        }

        private UILabel _dialogLabel
        {
            get
            {
                if (this._currentSpeak == 1)
                {
                    return this._dialogNpcLabel;
                }
                return this._dialogPlayerLabel;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/NpcDialog/NpcDialogView.assetbundle";
            }
        }
    }
}

