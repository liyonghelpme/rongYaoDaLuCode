﻿namespace com.game.module.NpcDialog
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;

    public class NpcFunctionView : BaseView<NpcFunctionView>
    {
        private uint _currentNpcId;
        private NpcDialogModel _npcDialogModel;
        private UILabel _npcSpeakLabel;

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this._currentNpcId = this._npcDialogModel.CurrentNpcId;
            SysNpcVo npcVoById = BaseDataMgr.instance.GetNpcVoById(this._currentNpcId);
        }

        public void Init()
        {
            this._npcSpeakLabel = base.FindChild("NpcSpeak/Label").GetComponent<UILabel>();
            this._npcDialogModel = Singleton<NpcDialogModel>.Instance;
        }
    }
}

