﻿namespace com.game.module.NpcDialog
{
    using com.game.module.core;
    using com.game.module.Task;
    using com.game.Public.Message;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class NpcTaskView : BaseView<NpcTaskView>
    {
        private uint _currentNpcId;
        private TaskVo _currentTaskVo;
        private bool _isDialog;
        private NpcDialogModel _npcDialogModel;
        private UILabel _npcSpeakLabel;
        private int _talkIndex;
        private Button _taskButton;
        private UILabel _taskInfoLabel;
        private TaskModel _taskModel;
        private List<string> _wordsList;

        private void AnalysWords()
        {
            this._wordsList.Clear();
            this._currentTaskVo = this._npcDialogModel.CurrentTaskVo;
            string str = string.Empty;
            switch (this._currentTaskVo.Statu)
            {
                case TaskStatu.StatuUnAccept:
                    str = this._currentTaskVo.SysTaskVo.talk_accept;
                    break;

                case TaskStatu.StatuAccepted:
                    if (this._currentTaskVo.CanCommit)
                    {
                        str = this._currentTaskVo.SysTaskVo.talk_com;
                    }
                    else
                    {
                        str = this._currentTaskVo.SysTaskVo.talk_uncom;
                    }
                    break;
            }
            char[] separator = new char[] { '@' };
            foreach (string str2 in str.Replace("#n", "@").Split(separator))
            {
                if (str2.Length > 0)
                {
                    this._wordsList.Add(str2);
                }
            }
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this._currentNpcId = this._npcDialogModel.CurrentNpcId;
            this.ShowTaskInfo();
        }

        public void Init()
        {
            this._npcSpeakLabel = base.FindChild("NpcSpeak/Label").GetComponent<UILabel>();
            this._taskInfoLabel = base.FindChild("TaskInfo/Label").GetComponent<UILabel>();
            this._taskButton = base.FindChild("Button").GetComponent<Button>();
            this._npcDialogModel = Singleton<NpcDialogModel>.Instance;
            this._taskModel = Singleton<TaskModel>.Instance;
            this._wordsList = new List<string>();
            this._talkIndex = 0;
            this._taskButton.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._taskButton.onClick, new UIWidgetContainer.VoidDelegate(this.OnTaskButtonClick));
        }

        private void OnTaskButtonClick(GameObject button)
        {
            if (this._isDialog)
            {
                this._talkIndex++;
                this.ShowRoleDialog();
            }
            else
            {
                switch (this._currentTaskVo.Statu)
                {
                    case TaskStatu.StatuUnAccept:
                        this._taskModel.AccetpTask(this._currentTaskVo.TaskId, this._npcDialogModel.CurrentNpcId);
                        break;

                    case TaskStatu.StatuAccepted:
                        if (this._currentTaskVo.CanCommit)
                        {
                            this._taskModel.CommitTask(this._currentTaskVo.TaskId, this._npcDialogModel.CurrentNpcId);
                        }
                        else
                        {
                            MessageManager.Show("任务还没完成哦");
                        }
                        break;
                }
                Singleton<NpcDialogView>.Instance.CloseView();
            }
        }

        private void ShowRoleDialog()
        {
            string str = this._wordsList[this._talkIndex];
            char[] separator = new char[] { '@' };
            string[] strArray = str.Replace("#r", "@").Split(separator);
            this._npcSpeakLabel.text = strArray[0];
            if (strArray.Length > 1)
            {
                this._isDialog = true;
                this._taskInfoLabel.text = strArray[1];
                this._taskButton.label.text = "继续对话";
            }
            else
            {
                this._isDialog = false;
                this.ShowTaskStatu();
            }
        }

        private void ShowTaskInfo()
        {
            this.AnalysWords();
            this._taskButton.SetActive(true);
            this.ShowRoleDialog();
        }

        private void ShowTaskStatu()
        {
            string str = "任务奖励：";
            if (this._currentTaskVo.SysTaskVo.exp > 0)
            {
                string str2 = str;
                object[] objArray1 = new object[] { str2, "经验：", this._currentTaskVo.SysTaskVo.exp, "    " };
                str = string.Concat(objArray1);
            }
            if (this._currentTaskVo.SysTaskVo.gold > 0)
            {
                str = str + "金币：" + this._currentTaskVo.SysTaskVo.gold;
            }
            this._taskInfoLabel.text = str;
            switch (this._currentTaskVo.Statu)
            {
                case TaskStatu.StatuUnAccept:
                    this._taskButton.label.text = "接受任务";
                    break;

                case TaskStatu.StatuAccepted:
                    if (this._currentTaskVo.CanCommit)
                    {
                        this._taskButton.label.text = "完成任务";
                    }
                    else
                    {
                        this._taskButton.SetActive(false);
                    }
                    break;
            }
        }
    }
}

