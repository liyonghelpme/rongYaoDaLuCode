﻿namespace com.game.module.Rank
{
    using com.game;
    using com.game.module.core;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class RankControl : BaseControl<RankControl>
    {
        private void Fun_21_1(INetData data)
        {
            RankInfoMsg_21_1 rankInfo = new RankInfoMsg_21_1();
            rankInfo.read(data.GetMemoryStream());
            Singleton<RankMode>.Instance.SetRankInfo(rankInfo.type, rankInfo);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("5377", new NetMsgCallback(this.Fun_21_1));
        }
    }
}

