﻿namespace com.game.module.Rank
{
    using com.game;
    using com.game.module.core;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class RankMode : BaseMode<RankMode>
    {
        private ushort _currentType;
        public readonly int RANK_UPDATE_NEW = 1;
        public readonly int RANK_UPDATE_OLD = 2;
        private Dictionary<ushort, ushort> rankCounterList = new Dictionary<ushort, ushort>();
        private Dictionary<ushort, RankInfoMsg_21_1> rankInfoList = new Dictionary<ushort, RankInfoMsg_21_1>();

        public RankInfoMsg_21_1 GetRankInfo(ushort type)
        {
            if (this.rankInfoList.ContainsKey(type))
            {
                return this.rankInfoList[type];
            }
            return null;
        }

        public void GetRankList(ushort type)
        {
            MemoryStream msdata = new MemoryStream();
            ushort counter = !this.rankCounterList.ContainsKey(type) ? ((ushort) 0) : this.rankCounterList[type];
            Module_21.write_21_1(msdata, type, counter);
            AppNet.gameNet.send(msdata, 0x15, 1);
        }

        public void SetRankInfo(ushort type, RankInfoMsg_21_1 rankInfo)
        {
            if (this.rankCounterList.ContainsKey(type) && (this.rankCounterList[type] == rankInfo.counter))
            {
                base.DataUpdateWithParam(this.RANK_UPDATE_OLD, type);
            }
            else
            {
                if (this.rankCounterList.ContainsKey(type))
                {
                    this.rankCounterList[type] = rankInfo.counter;
                }
                else
                {
                    this.rankCounterList.Add(type, rankInfo.counter);
                }
                if (this.rankInfoList.ContainsKey(type))
                {
                    this.rankInfoList[type] = rankInfo;
                }
                else
                {
                    this.rankInfoList.Add(type, rankInfo);
                }
                base.DataUpdateWithParam(this.RANK_UPDATE_NEW, type);
            }
        }

        public ushort CurrentType
        {
            get
            {
                return this._currentType;
            }
            set
            {
                this._currentType = value;
            }
        }
    }
}

