﻿namespace com.game.module.Rank
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.vo;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class RankView : BaseView<RankView>
    {
        private GameObject _chooseRank;
        private UISprite _currentTypeHighLihgt;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map15;
        private Button btn_chengjiu;
        private Button btn_fightEffect;
        private Button btn_fightwar;
        private Button btn_guild;
        private CreateRank createRank;
        private UIGrid grid_chengjiu;
        private UIGrid grid_fightEffect;
        private UIGrid grid_fightwar;
        private UIGrid grid_guild;
        private UILabel lab_name;
        private UILabel lab_pos;
        private GameObject my_chengjiu;
        private GameObject my_fightEffect;
        private GameObject my_fightwar;
        private GameObject my_guid;
        private GameObject rankItem;
        private Dictionary<ushort, GameObject> rankShowList = new Dictionary<ushort, GameObject>();

        public override void CancelUpdateHandler()
        {
        }

        private void CloseClick(GameObject obj)
        {
            this.CloseView();
        }

        private void CreateFightEffectRank(ushort Type, RankInfoMsg_21_1 Msg)
        {
            foreach (PRankNew new2 in Msg.info)
            {
                this.CreateFightEffectRankItem(Type, new2);
            }
            this.SetMeFightEffect(Msg.me);
        }

        private void CreateFightEffectRankItem(ushort Type, PRankNew rankInfo)
        {
            UIGrid grid = this.grid_fightEffect;
            this.rankItem = NGUITools.FindChild(grid.gameObject, "item");
            GameObject parent = UnityEngine.Object.Instantiate(this.rankItem) as GameObject;
            parent.transform.parent = grid.gameObject.transform;
            parent.transform.localPosition = this.rankItem.transform.localPosition;
            parent.transform.localScale = Vector3.one;
            NGUITools.FindInChild<UILabel>(parent, "rank_num").text = rankInfo.pos.ToString() + ":";
            NGUITools.FindInChild<UILabel>(parent, "name").text = rankInfo.name;
            NGUITools.FindInChild<UILabel>(parent, "rank_value").text = rankInfo.value.ToString();
            UISprite sprHead = NGUITools.FindInChild<UISprite>(parent, "role_icon");
            string str = BaseDataMgr.instance.GetGeneralVo(rankInfo.iconId, 0).icon_id.ToString();
            this.SetsprHead(sprHead, str);
            parent.name = rankInfo.userid.ToString();
            parent.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.OnClickRankItem);
            parent.SetActive(true);
        }

        public void DestroyAllChild(GameObject obj)
        {
            Transform transform = obj.transform;
            for (int i = transform.childCount - 1; i > 0; i--)
            {
                UnityEngine.Object.Destroy(transform.GetChild(i).gameObject);
            }
        }

        protected override void HandleAfterOpenView()
        {
            this.SetButtonHighLight(this.btn_fightEffect.gameObject);
            Singleton<RankMode>.Instance.GetRankList(0x4e21);
        }

        protected override void HandleBeforeCloseView()
        {
        }

        protected override void Init()
        {
            NGUITools.FindInChild<Button>(this.gameObject, "closeBtn").onClick = new UIWidgetContainer.VoidDelegate(this.CloseClick);
            this.btn_fightEffect = base.FindInChild<Button>("left/btn_zhanli");
            this.btn_guild = base.FindInChild<Button>("left/btn_guild");
            this.btn_fightwar = base.FindInChild<Button>("left/btn_fightwar");
            this.btn_chengjiu = base.FindInChild<Button>("left/btn_chengjiu");
            this.grid_fightEffect = base.FindInChild<UIGrid>("rank_list/zhanli/Grid_zhanli");
            this.grid_chengjiu = base.FindInChild<UIGrid>("rank_list/chengjiu/Grid_chengjiu");
            this.grid_fightwar = base.FindInChild<UIGrid>("rank_list/fightwar/Grid_fightwar");
            this.grid_guild = base.FindInChild<UIGrid>("rank_list/guild/Grid_guild");
            this.my_fightEffect = base.FindChild("rank_list/zhanli/my");
            this.my_chengjiu = base.FindChild("rank_list/zhanli/my");
            this.my_guid = base.FindChild("rank_list/zhanli/my");
            this.my_fightwar = base.FindChild("rank_list/zhanli/my");
            this.btn_fightEffect.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.btn_guild.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.btn_fightwar.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.btn_chengjiu.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void InitShowList()
        {
        }

        private void onClickHandler(GameObject go)
        {
            this.SetButtonHighLight(go);
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map15 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                    dictionary.Add("btn_zhanli", 0);
                    dictionary.Add("btn_guild", 1);
                    dictionary.Add("btn_fightwar", 2);
                    dictionary.Add("btn_chengjiu", 3);
                    <>f__switch$map15 = dictionary;
                }
                if (<>f__switch$map15.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            Singleton<RankMode>.Instance.GetRankList(0x4e21);
                            break;
                    }
                }
            }
        }

        private void OnClickRankItem(GameObject go)
        {
            Singleton<RoleControl>.Instance.RequestOtherInfo(Convert.ToUInt64(go.name));
        }

        public void RankDataUpdated(object sender, int code, object param)
        {
            ushort type = (ushort) param;
            if (Singleton<RankMode>.Instance.RANK_UPDATE_NEW == code)
            {
                this.SetRankInfo(type);
            }
            else if (Singleton<RankMode>.Instance.RANK_UPDATE_OLD == code)
            {
                this.rankShowList[type].SetActive(true);
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<RankMode>.Instance.dataUpdatedWithParam += new com.game.module.core.DataUpateHandlerWithParam(this.RankDataUpdated);
        }

        private void SetButtonHighLight(GameObject go)
        {
            if ((null == this._currentTypeHighLihgt) || (this._currentTypeHighLihgt.name != go.name))
            {
                if (null != this._currentTypeHighLihgt)
                {
                    this._currentTypeHighLihgt.SetActive(false);
                }
                this._currentTypeHighLihgt = NGUITools.FindInChild<UISprite>(go, "bg_highLight");
                this._currentTypeHighLihgt.SetActive(true);
                if (null != this._chooseRank)
                {
                    this._chooseRank.SetActive(false);
                }
            }
        }

        private void SetMeFightEffect(PRankNew rankInfo)
        {
            NGUITools.FindInChild<UILabel>(this.my_fightEffect, "rank_num").text = rankInfo.pos.ToString();
            NGUITools.FindInChild<UILabel>(this.my_fightEffect, "name").text = MeVo.instance.Name;
            NGUITools.FindInChild<UILabel>(this.my_fightEffect, "rank_value").text = rankInfo.value.ToString();
            UISprite sprHead = NGUITools.FindInChild<UISprite>(this.my_fightEffect, "role_icon");
            string str = Singleton<GeneralMode>.Instance.generalInfoList[MeVo.instance.generalId].genralTemplateInfo.big_icon_id.ToString();
            this.SetsprHead(sprHead, str);
        }

        private void SetRankInfo(ushort type)
        {
            GameObject gameObject;
            UIGrid grid;
            RankInfoMsg_21_1 rankInfo = Singleton<RankMode>.Instance.GetRankInfo(type);
            if (type == 0x4e21)
            {
                this.createRank = new CreateRank(this.CreateFightEffectRank);
                grid = this.grid_fightEffect;
                gameObject = this.grid_fightEffect.transform.parent.gameObject;
            }
            else
            {
                gameObject = this.grid_fightEffect.transform.parent.gameObject;
                grid = this.grid_fightEffect;
            }
            this.DestroyAllChild(grid.gameObject);
            gameObject.SetActive(true);
            this.createRank(type, rankInfo);
            grid.Reposition();
            this.UpdateScrollBar(grid.gameObject);
            this.rankShowList[type] = gameObject;
            this._chooseRank = gameObject;
        }

        private void SetsprHead(UISprite sprHead, string icon_id)
        {
            sprHead.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            sprHead.spriteName = icon_id;
            sprHead.MakePixelPerfect();
            sprHead.SetDimensions(0x2a, 0x2c);
        }

        private void UpdateScrollBar(GameObject obj)
        {
            UIScrollView component = obj.GetComponent<UIScrollView>();
            this.grid_fightEffect.repositionNow = true;
            component.UpdatePosition();
            component.RestrictWithinBounds(true);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Rank/RankView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

