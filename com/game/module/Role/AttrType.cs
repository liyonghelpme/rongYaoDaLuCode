﻿namespace com.game.module.Role
{
    using System;

    public enum AttrType : byte
    {
        ATTR_ID_AGI = 2,
        ATTR_ID_ATT_DEF_M = 12,
        ATTR_ID_ATT_DEF_P = 11,
        ATTR_ID_ATT_M_MAX = 10,
        ATTR_ID_ATT_M_MIN = 9,
        ATTR_ID_ATT_MAX = 0x16,
        ATTR_ID_ATT_MIN = 0x15,
        ATTR_ID_ATT_P_MAX = 8,
        ATTR_ID_ATT_P_MIN = 7,
        ATTR_ID_CRIT = 15,
        ATTR_ID_CRIT_RATIO = 0x10,
        ATTR_ID_DODGE = 14,
        ATTR_ID_FLEX = 0x11,
        ATTR_ID_HIT = 13,
        ATTR_ID_HP = 5,
        ATTR_ID_HURT_RE = 0x12,
        ATTR_ID_LUCK = 20,
        ATTR_ID_MP = 6,
        ATTR_ID_PHY = 3,
        ATTR_ID_SPEED = 0x13,
        ATTR_ID_STR = 1,
        ATTR_ID_WIT = 4
    }
}

