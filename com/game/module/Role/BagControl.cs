﻿namespace com.game.module.Role
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;
    using System.IO;

    public class BagControl : BaseControl<BagControl>
    {
        public void BatchUseItem(uint id, ushort count)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_5(msdata, (ulong) id, count);
            AppNet.gameNet.send(msdata, 0x24, 5);
        }

        private void DeleteItem_36_4(INetData data)
        {
            ItemDeleteMsg_36_4 g__ = new ItemDeleteMsg_36_4();
            g__.read(data.GetMemoryStream());
            if (g__.repos == 1)
            {
                Singleton<BagMode>.Instance.DeleteItem(g__.id, g__.repos);
            }
            else if (g__.repos == 2)
            {
                Singleton<BagMode>.Instance.DeleteEquip(g__.id, g__.repos);
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("9217", new NetMsgCallback(this.RefreshItemInfo_36_1));
            AppNet.main.addCMD("9220", new NetMsgCallback(this.DeleteItem_36_4));
            AppNet.main.addCMD("9219", new NetMsgCallback(this.SellItem_36_3));
        }

        private void RefreshItemInfo_36_1(INetData data)
        {
            ItemListMsg_36_1 g__ = new ItemListMsg_36_1();
            g__.read(data.GetMemoryStream());
            if (g__.repos == 1)
            {
                Singleton<BagMode>.Instance.UpdateItemInfo(g__.itemInfo, g__.repos);
            }
            else if (g__.repos == 2)
            {
                Singleton<BagMode>.Instance.UpdateEquipmentsInfo(g__.itemInfo, g__.repos);
            }
        }

        public void RequestItemInfo(byte type)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_1(msdata, type);
            AppNet.gameNet.send(msdata, 0x24, 1);
        }

        public void SellItem(ulong itemId, uint count)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_3(msdata, itemId, count);
            AppNet.gameNet.send(msdata, 0x24, 3);
        }

        private void SellItem_36_3(INetData data)
        {
            ItemSellMsg_36_3 g__ = new ItemSellMsg_36_3();
            g__.read(data.GetMemoryStream());
            ErrorCodeManager.ShowError(g__.code);
        }

        public void UseItem(ulong itemId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_36.write_36_2(msdata, itemId);
            AppNet.gameNet.send(msdata, 0x24, 2);
        }
    }
}

