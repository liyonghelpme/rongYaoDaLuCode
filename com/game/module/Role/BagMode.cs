﻿namespace com.game.module.Role
{
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;

    public class BagMode : BaseMode<BagMode>
    {
        private SortedDictionary<ulong, PItems> _equipDict = new SortedDictionary<ulong, PItems>();
        private SortedDictionary<ulong, PItems> _itemDict = new SortedDictionary<ulong, PItems>();
        private Dictionary<ulong, PItems> _stoneDict = new Dictionary<ulong, PItems>();
        public BagCell CurrentCell;
        public const int ITEM_START_ID = 0x186a0;
        public BagCell LastSelectCell;
        public BagCell SellingCell;
        public uint TagType;
        public static readonly int UPDATE_CELL_INFO = 2;
        public static readonly int UPDATE_EQUIP = 3;
        public static readonly int UPDATE_ITEM = 1;

        public void DeleteEquip(List<ulong> ids, byte repos)
        {
            foreach (ulong num in ids)
            {
                if (this.EquipDict.ContainsKey(num))
                {
                    this.EquipDict.Remove(num);
                }
            }
            base.DataUpdate(UPDATE_EQUIP);
        }

        public void DeleteItem(List<ulong> ids, byte repos)
        {
            foreach (ulong num in ids)
            {
                if (this.ItemDict.ContainsKey(num))
                {
                    this.ItemDict.Remove(num);
                }
            }
            base.DataUpdate(UPDATE_ITEM);
        }

        public int GetItemCountByTemplateId(uint templateId)
        {
            int num = 0;
            foreach (PItems items in this._itemDict.Values)
            {
                if (items.templateId == templateId)
                {
                    num += (int) items.count;
                }
            }
            return num;
        }

        public ulong GetItemIdByTemplateId(uint templateId)
        {
            foreach (PItems items in this._itemDict.Values)
            {
                if (items.templateId == templateId)
                {
                    return items.id;
                }
            }
            return 0L;
        }

        public PItems GetPItemById(uint id)
        {
            return this._itemDict[(ulong) id];
        }

        public PItems GetPItemByTemplateId(uint templateId)
        {
            foreach (PItems items in this.ItemDict.Values)
            {
                if (items.templateId == templateId)
                {
                    return items;
                }
            }
            return null;
        }

        public Dictionary<ulong, PItems> GetStoneDict()
        {
            this._stoneDict.Clear();
            foreach (PItems items in this._itemDict.Values)
            {
                if (BaseDataMgr.instance.getGoodsVo(items.templateId).type == 4)
                {
                    this._stoneDict.Add(items.id, items);
                }
            }
            return this._stoneDict;
        }

        private bool IsShowTips()
        {
            return false;
        }

        public void UpdateEquipmentsInfo(List<PItems> itemInfo, byte repos)
        {
            foreach (PItems items in itemInfo)
            {
                this.EquipDict[items.id] = items;
            }
            base.DataUpdate(UPDATE_EQUIP);
        }

        public void UpdateItemInfo(List<PItems> itemInfo, byte repos)
        {
            foreach (PItems items in itemInfo)
            {
                this.ItemDict[items.id] = items;
            }
            base.DataUpdate(UPDATE_ITEM);
        }

        public SortedDictionary<ulong, PItems> EquipDict
        {
            get
            {
                return this._equipDict;
            }
        }

        public int ItemAmount
        {
            get
            {
                return this._itemDict.Count;
            }
        }

        public SortedDictionary<ulong, PItems> ItemDict
        {
            get
            {
                return this._itemDict;
            }
        }

        public override bool ShowTips
        {
            get
            {
                return this.IsShowTips();
            }
        }

        public enum ItemType
        {
            Total,
            Other
        }
    }
}

