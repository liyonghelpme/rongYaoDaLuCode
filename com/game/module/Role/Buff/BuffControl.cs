﻿namespace com.game.module.Role.Buff
{
    using com.game;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.Role;
    using com.net;
    using com.net.interfaces;
    using com.u3d.bases.display;
    using PCustomDataType;
    using Proto;
    using System;

    public class BuffControl : BaseControl<BuffControl>
    {
        private void BuffAddSync_35_4(INetData receiveData)
        {
            BuffAddSyncMsg_35_4 g__ = new BuffAddSyncMsg_35_4();
            g__.read(receiveData.GetMemoryStream());
            ActionDisplay actionDisplayByDisplayTypeAndId = SkillControl.GetActionDisplayByDisplayTypeAndId(DamageCheck.GetDisplayType(g__.type), g__.id);
            if (((actionDisplayByDisplayTypeAndId != null) && !actionDisplayByDisplayTypeAndId.GetMeVoByType<BaseRoleVo>().IsEmptyHp) && ((actionDisplayByDisplayTypeAndId.GoBase != null) && (actionDisplayByDisplayTypeAndId.Controller != null)))
            {
                BuffController buffController = actionDisplayByDisplayTypeAndId.Controller.buffController;
                if (buffController != null)
                {
                    foreach (PDamageBuff buff in g__.buffList)
                    {
                        ActionDisplay attackDisplay = SkillControl.GetActionDisplayByDisplayTypeAndId(DamageCheck.GetDisplayType(buff.attackerType), buff.attackerId);
                        buffController.AddBuffNoBroadCast(buff.buffId, buff.lvl, buff.val1, buff.val2, buff.buffOwnVal1, buff.buffOwnVal2, attackDisplay);
                    }
                }
            }
        }

        private void BuffRemoveSync_35_5(INetData receiveData)
        {
            BuffRemoveSyncMsg_35_5 g__ = new BuffRemoveSyncMsg_35_5();
            g__.read(receiveData.GetMemoryStream());
            ActionDisplay actionDisplayByDisplayTypeAndId = SkillControl.GetActionDisplayByDisplayTypeAndId(DamageCheck.GetDisplayType(g__.type), g__.id);
            if (((actionDisplayByDisplayTypeAndId != null) && (actionDisplayByDisplayTypeAndId.Controller != null)) && !actionDisplayByDisplayTypeAndId.GetMeVoByType<BaseRoleVo>().IsEmptyHp)
            {
                BuffController buffController = actionDisplayByDisplayTypeAndId.Controller.buffController;
                if (buffController != null)
                {
                    foreach (PBuffRemover remover in g__.buffList)
                    {
                        SimpleBuffVo simpleBuff = new SimpleBuffVo(remover.buffId, remover.buffLvl, null) {
                            syncUnikey = (int) remover.uinqueKey
                        };
                        buffController.DoRemoveBuffNoBroadCast(simpleBuff);
                    }
                }
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("8964", new NetMsgCallback(this.BuffAddSync_35_4));
            AppNet.main.addCMD("8965", new NetMsgCallback(this.BuffRemoveSync_35_5));
        }
    }
}

