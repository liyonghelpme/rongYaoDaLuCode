﻿namespace com.game.module.Role.Buff
{
    using com.game;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.net.wifi_pvp;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityLog;

    public class BuffMode : BaseMode<BuffMode>
    {
        [CompilerGenerated]
        private static Func<SimpleBuffVo, PBuffRemover> <>f__am$cache0;

        private bool CheckNeedSyn()
        {
            return (AppMap.Instance.IsInWifiPVP || AppMap.Instance.mapParserII.NeedSyn);
        }

        private void DoSend(MemoryStream ms, byte section, byte method)
        {
            if (AppMap.Instance.IsInWifiPVP)
            {
                WifiLAN.Instance.Send(ms, section, method);
            }
        }

        private static void GetIdAndType(ActionControler controler, out ulong id, out byte type)
        {
            id = controler.Me.GetVo().Id;
            type = (byte) DamageCheck.GetSynType(controler.Me.Type);
        }

        public void SyncBuffAdd(ActionControler controller, List<SimpleBuffVo> simpleBuffList)
        {
            List<PDamageBuff> list = new List<PDamageBuff>();
            foreach (SimpleBuffVo vo in simpleBuffList)
            {
                PDamageBuff item = new PDamageBuff {
                    buffId = vo.BuffId,
                    lvl = Convert.ToByte(vo.BuffLevel),
                    val1 = vo.syncUnikey,
                    val2 = vo._hp,
                    buffOwnVal1 = vo.val1,
                    buffOwnVal2 = vo.val2
                };
                ActionDisplay attackDisplay = vo.attackDisplay;
                if (attackDisplay != null)
                {
                    item.attackerId = attackDisplay.GetVo().Id;
                    item.attackerType = (byte) DamageCheck.GetSynType(attackDisplay.Type);
                }
                list.Add(item);
            }
            this.SyncBuffAdd(controller, list);
        }

        public void SyncBuffAdd(ActionControler controler, List<PDamageBuff> buff_list)
        {
            if (this.CheckNeedSyn())
            {
                ulong num;
                byte num2;
                GetIdAndType(controler, out num, out num2);
                MemoryStream msdata = new MemoryStream();
                Module_35.write_35_4(msdata, num, num2, buff_list);
                this.DoSend(msdata, 0x23, 4);
            }
        }

        public void SyncBuffRemove(ActionControler controler, List<SimpleBuffVo> list)
        {
            if (list.Count != 0)
            {
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = x => new PBuffRemover { buffId = x.BuffId, buffLvl = Convert.ToByte(x.BuffLevel), uinqueKey = (uint) x.syncUnikey };
                }
                List<PBuffRemover> list2 = list.Select<SimpleBuffVo, PBuffRemover>(<>f__am$cache0).ToList<PBuffRemover>();
                this.SyncBuffRemove(controler, list2);
            }
        }

        public void SyncBuffRemove(ActionControler controler, List<PBuffRemover> list)
        {
            Log.Net("RemoveBuff");
            if (this.CheckNeedSyn())
            {
                ulong num;
                byte num2;
                GetIdAndType(controler, out num, out num2);
                MemoryStream msdata = new MemoryStream();
                Module_35.write_35_5(msdata, num, num2, list);
                this.DoSend(msdata, 0x23, 5);
            }
        }
    }
}

