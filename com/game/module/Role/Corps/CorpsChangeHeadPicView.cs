﻿namespace com.game.module.Role.Corps
{
    using com.game.module.core;
    using com.game.module.General;
    using com.game.module.Role;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class CorpsChangeHeadPicView : BaseView<CorpsChangeHeadPicView>
    {
        public EventDelegate.Callback AfterOpenViewGuideDelegate;
        private Button btn_choose;
        private Button btn_close;
        private UIGrid grid;
        private GameObject headItem;
        private List<GameObject> headItemContainerList = new List<GameObject>();
        private UISprite headItemHighLight;
        private UIPanel panel;
        public RoleDisplay RoleModel;
        private UIScrollView scrollView;
        private RoleDisplay SelectModel;

        public override void CancelUpdateHandler()
        {
            Singleton<CorpsMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateRoleIcon);
        }

        private void ChangeSelectModel(uint general_id)
        {
            if (this.RoleModel != null)
            {
                UnityEngine.Object.Destroy(this.RoleModel.GoBase);
            }
            foreach (GeneralInfo info in Singleton<GeneralMode>.Instance.generalInfoList.Values)
            {
                if (info.id == general_id)
                {
                    this.RoleModel = new RoleDisplay();
                    this.RoleModel.CreateRole(info.genralTemplateInfo.model_id, new com.game.module.Role.ModelControl(this.LoaCallBack), false);
                    break;
                }
            }
        }

        private void ChooseBtnClickHandler(GameObject go)
        {
            uint generalId = uint.Parse(Singleton<CorpsMode>.Instance.chooseObj.name);
            Singleton<CorpsMode>.Instance.sendRoleStyleBin(generalId);
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.Dispose();
            this.CloseView();
        }

        private void Dispose()
        {
            if (this.RoleModel != null)
            {
                UnityEngine.Object.Destroy(this.RoleModel.GoBase);
            }
        }

        protected override void HandleAfterOpenView()
        {
            if (Singleton<CorpsMode>.Instance.chooseObj != null)
            {
                this.ChangeSelectModel(uint.Parse(Singleton<CorpsMode>.Instance.chooseObj.name));
            }
            if (this.AfterOpenViewGuideDelegate != null)
            {
                this.AfterOpenViewGuideDelegate();
            }
        }

        private void HeadClickHandler(GameObject go)
        {
            go.transform.Find("choosehightlight").GetComponent<UISprite>().SetActive(true);
            if ((Singleton<CorpsMode>.Instance.chooseObj != null) && (Singleton<CorpsMode>.Instance.chooseObj.name != go.name))
            {
                Singleton<CorpsMode>.Instance.chooseObj.transform.Find("choosehightlight").GetComponent<UISprite>().SetActive(false);
                this.ChangeSelectModel(uint.Parse(go.name));
            }
            Singleton<CorpsMode>.Instance.chooseObj = go;
        }

        protected override void Init()
        {
            this.panel = this.gameObject.GetComponent<UIPanel>();
            this.btn_close = base.FindInChild<Button>("btn_close");
            this.scrollView = base.FindInChild<UIScrollView>("list");
            this.grid = base.FindInChild<UIGrid>("list/Grid");
            this.headItem = base.FindChild("list/Grid/item");
            this.headItem.SetActive(false);
            this.headItemContainerList.Add(this.headItem);
            this.btn_choose = base.FindInChild<Button>("btn_choose");
            this.InitIcon();
            this.InitClick();
        }

        private void InitClick()
        {
            this.btn_close.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.btn_choose.onClick = new UIWidgetContainer.VoidDelegate(this.ChooseBtnClickHandler);
        }

        private void InitIcon()
        {
            foreach (GeneralInfo info in Singleton<GeneralMode>.Instance.generalInfoList.Values)
            {
                GameObject item = UnityEngine.Object.Instantiate(this.headItemContainerList[0]) as GameObject;
                item.transform.parent = this.headItemContainerList[0].transform.parent;
                item.transform.localPosition = this.headItemContainerList[0].transform.localPosition;
                item.transform.localScale = Vector3.one;
                item.name = info.id.ToString();
                item.SetActive(true);
                item.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.HeadClickHandler);
                this.headItemContainerList.Add(item);
                UISprite component = item.transform.Find("Sprite").GetComponent<UISprite>();
                component.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                component.spriteName = info.genralTemplateInfo.icon_id.ToString();
                component.MakePixelPerfect();
                component.SetDimensions(80, 80);
                item.transform.Find("choosehightlight").GetComponent<UISprite>().SetActive(false);
                if (MeVo.instance.generalId == info.id)
                {
                    item.transform.Find("choosehightlight").GetComponent<UISprite>().SetActive(true);
                    Singleton<CorpsMode>.Instance.chooseObj = item;
                }
            }
        }

        private void LoaCallBack(GameObject go)
        {
            this.RoleModel.GoBase.transform.localPosition = new Vector3(183f, -55f, 0f);
            this.RoleModel.GoBase.transform.eulerAngles = new Vector3(0f, -180f, 0f);
            this.RoleModel.GoBase.transform.localScale = new Vector3(200f, 200f, 200f);
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<CorpsMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateRoleIcon);
        }

        private void UpdateRoleIcon(object sender, int type)
        {
            if (Singleton<CorpsMode>.Instance.UPDATE_ROLEICON == type)
            {
            }
        }

        public Button ChooseButton
        {
            get
            {
                return this.btn_choose;
            }
        }

        public Button CloseButton
        {
            get
            {
                return this.btn_close;
            }
        }

        public UIPanel ContainPanel
        {
            get
            {
                return this.panel;
            }
        }

        public List<GameObject> HeadItemList
        {
            get
            {
                return this.headItemContainerList;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Corps/CorpsChangeHeadPicView.assetbundle";
            }
        }
    }
}

