﻿namespace com.game.module.Role.Corps
{
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.utils;
    using System;
    using UnityEngine;

    public class CorpsChangeNameView : BaseView<CorpsChangeNameView>
    {
        private Button cancelBtn;
        private UIInput nameInputLab;
        private Button sureBtn;

        private void CancelBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
        }

        protected override void Init()
        {
            this.cancelBtn = base.FindInChild<Button>("cancelBtn");
            this.sureBtn = base.FindInChild<Button>("sureBtn");
            this.nameInputLab = base.FindInChild<UIInput>("input/Input");
            this.InitClick();
        }

        private void InitClick()
        {
            this.cancelBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CancelBtnClickHandler);
            this.sureBtn.onClick = new UIWidgetContainer.VoidDelegate(this.SureBtnClickHandler);
        }

        private void SureBtnClickHandler(GameObject go)
        {
            string param = this.nameInputLab.value.Replace(" ", string.Empty);
            this.nameInputLab.label.text = param;
            if (StringUtils.isEmpty(param) || param.Equals("请输入角色名称"))
            {
                this.nameInputLab.label.text = "请输入角色名称";
            }
            else if (param.Length < 2)
            {
                MessageManager.Show("名字至少要包含" + 2 + "个字符");
            }
            else if (Singleton<ChatView>.Instance.ContainsFilter(param))
            {
                MessageManager.Show("名字中包含敏感词汇！");
                this.nameInputLab.label.text = "请输入角色名称";
            }
            else
            {
                Singleton<CorpsMode>.Instance.SendRoleName(param);
                this.CloseView();
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Corps/CorpsChangeNameView.assetbundle";
            }
        }
    }
}

