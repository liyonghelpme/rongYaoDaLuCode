﻿namespace com.game.module.Role.Corps
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.vo;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class CorpsControl : BaseControl<CorpsControl>
    {
        private void Fun_3_7(INetData data)
        {
            RoleChangeIconMsg_3_7 g__ = new RoleChangeIconMsg_3_7();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void Fun_3_8(INetData data)
        {
            RoleChangeNameMsg_3_8 g__ = new RoleChangeNameMsg_3_8();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MeVo.instance.Name = g__.name;
                Singleton<CorpsMode>.Instance.UpdateRoleName(g__.name);
            }
        }

        private void Fun_3_9(INetData data)
        {
            RoleChangeGeneralMsg_3_9 g__ = new RoleChangeGeneralMsg_3_9();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                Singleton<CorpsMode>.Instance.UpdatePlayerStyle(g__.roleId, g__.generalId, g__.styleBin);
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("775", new NetMsgCallback(this.Fun_3_7));
            AppNet.main.addCMD("776", new NetMsgCallback(this.Fun_3_8));
            AppNet.main.addCMD("777", new NetMsgCallback(this.Fun_3_9));
        }
    }
}

