﻿namespace com.game.module.Role.Corps
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.vo;
    using com.u3d.bases.display.character;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Globalization;
    using System.IO;
    using UnityEngine;

    public class CorpsMode : BaseMode<CorpsMode>
    {
        public GameObject chooseObj;
        public uint RoleIconId;
        public string RoleName;
        public readonly int UPDATE_PLAYERSTYLE = 3;
        public readonly int UPDATE_ROLEICON = 1;
        public readonly int UPDATE_ROLENAME = 2;

        public void SendIconId(uint iconId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_7(msdata, iconId);
            AppNet.gameNet.send(msdata, 3, 7);
        }

        public void SendRoleName(string name)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_8(msdata, name);
            AppNet.gameNet.send(msdata, 3, 8);
        }

        public void sendRoleStyleBin(uint GeneralId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_9(msdata, (ulong) GeneralId);
            AppNet.gameNet.send(msdata, 3, 9);
        }

        public void UpdateIconInfo(uint iconId)
        {
            this.RoleIconId = iconId;
            base.DataUpdate(this.UPDATE_ROLEICON);
        }

        public void UpdatePlayerStyle(ulong roleId, ulong generalId, PStylebin stylebin)
        {
            if (MeVo.instance.Id == roleId)
            {
                AppMap.Instance.me.UpdateStyle(stylebin);
                MeVo.instance.generalId = generalId;
                MeVo.instance.stylebin = stylebin;
                uint generalUniKey = GlobalData.GetGeneralUniKey(stylebin.id, stylebin.quality);
                SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo((uint) stylebin.id, stylebin.quality);
                MeVo.instance.generalTemplateInfo = generalVo;
                Singleton<CorpsMode>.Instance.UpdateIconInfo((uint) generalVo.icon_id);
            }
            else
            {
                PlayerDisplay player = AppMap.Instance.GetPlayer(roleId.ToString(CultureInfo.InvariantCulture));
                if (player != null)
                {
                    player.UpdateStyle(stylebin);
                }
            }
            base.DataUpdate(this.UPDATE_PLAYERSTYLE);
        }

        public void UpdateRoleName(string name)
        {
            this.RoleName = name;
            base.DataUpdate(this.UPDATE_ROLENAME);
            base.DataUpdateWithParam(this.UPDATE_ROLENAME, name);
        }

        public int GetExpTotal
        {
            get
            {
                return BaseDataMgr.instance.GetRoleInfo(MeVo.instance.Level).exp_level;
            }
        }

        public int GetLevUp
        {
            get
            {
                return BaseDataMgr.instance.GetRoleInfo(MeVo.instance.Level).general_lvl;
            }
        }
    }
}

