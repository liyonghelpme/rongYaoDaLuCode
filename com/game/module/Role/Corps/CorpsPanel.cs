﻿namespace com.game.module.Role.Corps
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using com.game.vo;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CorpsPanel : BaseView<CorpsPanel>
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map16;
        private Button change_title;
        private Button close_btn;
        private List<UISprite> headList = new List<UISprite>();
        private UISprite headSprite;
        private UIWidgetContainer heroGroup1;
        private UIWidgetContainer heroGroup2;
        private UIWidgetContainer heroGroup3;
        private UIEventListener maskListener;
        private UILabel playerClubName;
        private UILabel playerFight;
        private GameObject playerGroup;
        private UILabel playerLv;
        private UILabel playerName;
        private UILabel playerTitle;
        private Button quit_club;
        private UILabel selfClubName;
        private UILabel selfExp;
        private GameObject selfGroup;
        private UILabel selfLv;
        private UILabel selfMaxLv;
        private UILabel selfName;
        private UILabel selfTitle;
        private Button system_btn;

        public override void CancelUpdateHandler()
        {
            Singleton<RoleMode>.Instance.dataUpdated -= new DataUpateHandler(this.DataUpdated);
        }

        public override void DataUpdated(object sender, int code)
        {
            if (code == 4)
            {
                this.OtherRoleInfo = Singleton<RoleMode>.Instance.otherAttr;
                switch (base.LastOpenedPanelType)
                {
                    case PanelType.NONE:
                        this.UpdateOtherInfos();
                        break;

                    case PanelType.MAIN_VIEW:
                        this.UpdateInfos();
                        break;
                }
            }
        }

        private PSetGeneralInfo getGeneralInfo(int Place)
        {
            <getGeneralInfo>c__AnonStoreyE8 ye = new <getGeneralInfo>c__AnonStoreyE8 {
                Place = Place
            };
            return ((this.OtherRoleInfo == null) ? null : this.OtherRoleInfo.generalIdList.FirstOrDefault<PSetGeneralInfo>(new Func<PSetGeneralInfo, bool>(ye.<>m__B4)));
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdateShowType();
            this.OtherRoleInfo = Singleton<RoleMode>.Instance.otherAttr;
            switch (base.LastOpenedPanelType)
            {
                case PanelType.NONE:
                    this.UpdateOtherInfos();
                    break;

                case PanelType.MAIN_VIEW:
                    this.UpdateInfos();
                    break;
            }
        }

        protected override void Init()
        {
            this.close_btn = base.FindInChild<Button>("close");
            this.system_btn = base.FindInChild<Button>("self/buttons/system_btn");
            this.quit_club = base.FindInChild<Button>("self/buttons/quit_club");
            this.change_title = base.FindInChild<Button>("self/buttons/change_chenhao");
            this.selfGroup = base.FindChild("self");
            this.playerGroup = base.FindChild("player");
            this.headSprite = base.FindInChild<UISprite>("head/icon");
            this.maskListener = base.FindChild("bg/mask").AddMissingComponent<UIEventListener>();
            this.selfName = base.FindInChild<UILabel>("self/infos/name");
            this.selfLv = base.FindInChild<UILabel>("self/infos/lv");
            this.selfExp = base.FindInChild<UILabel>("self/infos/exp");
            this.selfMaxLv = base.FindInChild<UILabel>("self/infos/hero_lv");
            this.selfClubName = base.FindInChild<UILabel>("self/infos/club_name");
            this.selfTitle = base.FindInChild<UILabel>("self/infos/chenhao_name");
            this.playerName = base.FindInChild<UILabel>("player/infos/name");
            this.playerClubName = base.FindInChild<UILabel>("player/infos/club");
            this.playerLv = base.FindInChild<UILabel>("player/infos/lv");
            this.playerTitle = base.FindInChild<UILabel>("player/infos/chenhao");
            this.playerFight = base.FindInChild<UILabel>("player/infos/fight");
            this.headList.Add(base.FindInChild<UISprite>("player/heads/hero1/icon"));
            this.headList.Add(base.FindInChild<UISprite>("player/heads/hero2/icon"));
            this.headList.Add(base.FindInChild<UISprite>("player/heads/hero3/icon"));
            this.heroGroup1 = base.FindChild("player/heads/hero1").AddMissingComponent<UIWidgetContainer>();
            this.heroGroup2 = base.FindChild("player/heads/hero2").AddMissingComponent<UIWidgetContainer>();
            this.heroGroup3 = base.FindChild("player/heads/hero3").AddMissingComponent<UIWidgetContainer>();
            this.quit_club.SetActive(false);
            this.RegisterEventHandler();
        }

        private void OnClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map16 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(8);
                    dictionary.Add("close", 0);
                    dictionary.Add("mask", 0);
                    dictionary.Add("system_btn", 1);
                    dictionary.Add("quit_club", 2);
                    dictionary.Add("change_chenhao", 3);
                    dictionary.Add("hero1", 4);
                    dictionary.Add("hero2", 5);
                    dictionary.Add("hero3", 6);
                    <>f__switch$map16 = dictionary;
                }
                if (<>f__switch$map16.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            this.CloseView();
                            break;

                        case 1:
                            this.CloseView();
                            Singleton<SystemSettingsView>.Instance.OpenView();
                            break;

                        case 4:
                            if (this.getGeneralInfo(1) != null)
                            {
                                Singleton<GeneralMode>.Instance.SendOtherGeneralInfo((uint) this.getGeneralInfo(1).id, this.OtherRoleInfo.id);
                            }
                            break;

                        case 5:
                            if (this.getGeneralInfo(2) != null)
                            {
                                Singleton<GeneralMode>.Instance.SendOtherGeneralInfo((uint) this.getGeneralInfo(2).id, this.OtherRoleInfo.id);
                            }
                            break;

                        case 6:
                            if (this.getGeneralInfo(3) != null)
                            {
                                Singleton<GeneralMode>.Instance.SendOtherGeneralInfo((uint) this.getGeneralInfo(3).id, this.OtherRoleInfo.id);
                            }
                            break;
                    }
                }
            }
        }

        private void RegisterEventHandler()
        {
            this.maskListener.onClick = new UIEventListener.VoidDelegate(this.OnClickHandler);
            this.close_btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.heroGroup1.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.heroGroup2.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.heroGroup3.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.system_btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<RoleMode>.Instance.dataUpdated += new DataUpateHandler(this.DataUpdated);
        }

        private void UpdateHeadSprite(uint generalId, UISprite sprite, int size = 80)
        {
            if (generalId != 0)
            {
                sprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                sprite.spriteName = generalId + string.Empty;
            }
            else
            {
                sprite.atlas = null;
                sprite.spriteName = string.Empty;
            }
            sprite.MakePixelPerfect();
            sprite.SetDimensions(size, size);
        }

        public void UpdateInfos()
        {
            if (this.OtherRoleInfo != null)
            {
                this.GeneralId = this.OtherRoleInfo.generalId;
                this.selfName.text = this.OtherRoleInfo.name;
                this.selfLv.text = this.OtherRoleInfo.lvl + string.Empty;
                this.selfExp.text = MeVo.instance.exp + " / " + MeVo.instance.expFull;
                SysRoleInfoVo roleInfo = BaseDataMgr.instance.GetRoleInfo(this.OtherRoleInfo.lvl);
                if (roleInfo != null)
                {
                    this.selfMaxLv.text = roleInfo.general_lvl + string.Empty;
                }
                this.selfClubName.text = !(this.OtherRoleInfo.guildName.Replace(" ", string.Empty) != string.Empty) ? "无" : this.OtherRoleInfo.guildName;
                this.selfTitle.text = !(this.OtherRoleInfo.title.Replace(" ", string.Empty) != string.Empty) ? "无" : this.OtherRoleInfo.title;
                this.UpdateHeadSprite(this.GeneralId, this.headSprite, 80);
            }
        }

        private void UpdateOtherInfos()
        {
            if (this.OtherRoleInfo != null)
            {
                this.GeneralId = this.OtherRoleInfo.generalId;
                this.playerName.text = this.OtherRoleInfo.name;
                this.playerClubName.text = !(this.OtherRoleInfo.guildName.Replace(" ", string.Empty) != string.Empty) ? "无" : this.OtherRoleInfo.guildName;
                this.playerTitle.text = !(this.OtherRoleInfo.title.Replace(" ", string.Empty) != string.Empty) ? "无" : this.OtherRoleInfo.title;
                this.playerLv.text = this.OtherRoleInfo.lvl + string.Empty;
                this.playerFight.text = this.OtherRoleInfo.fightEffect + string.Empty;
                this.UpdateHeadSprite(this.GeneralId, this.headSprite, 80);
                for (int i = 0; i < this.headList.Count; i++)
                {
                    PSetGeneralInfo info = this.getGeneralInfo(i + 1);
                    this.UpdateHeadSprite((info == null) ? 0 : ((uint) info.id), this.headList[i], 0x49);
                }
            }
        }

        private void UpdateShowType()
        {
            this.selfGroup.SetActive(base.LastOpenedPanelType == PanelType.MAIN_VIEW);
            this.playerGroup.SetActive(base.LastOpenedPanelType == PanelType.NONE);
        }

        public uint GeneralId { get; set; }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public POtherRoleInfo OtherRoleInfo { get; set; }

        public override string url
        {
            get
            {
                return "UI/Corps/CorpsPanel.assetbundle";
            }
        }

        [CompilerGenerated]
        private sealed class <getGeneralInfo>c__AnonStoreyE8
        {
            internal int Place;

            internal bool <>m__B4(PSetGeneralInfo info)
            {
                return (info.place == this.Place);
            }
        }
    }
}

