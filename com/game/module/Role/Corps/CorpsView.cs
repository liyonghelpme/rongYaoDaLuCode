﻿namespace com.game.module.Role.Corps
{
    using com.game.module.core;
    using com.game.vo;
    using System;
    using UnityEngine;

    public class CorpsView : BaseView<CorpsView>
    {
        public EventDelegate.Callback AfterOpenViewGuideDelegate;
        private Button btn_changeHeadPic;
        private Button btn_changeNick;
        private Button btn_close;
        private UILabel expLab;
        private UILabel expNum;
        private UILabel guildIdLab;
        private UILabel guildNameLab;
        private UISprite headPic;
        private UILabel levelLab;
        private UILabel levelLimitLab;
        private UILabel levelLimitNum;
        private UILabel levelNum;
        private UILabel nameLab;

        public override void CancelUpdateHandler()
        {
            Singleton<CorpsMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateHeadSprite);
            Singleton<CorpsMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateRoleName);
        }

        private void ChangeHeadClickHandler(GameObject go)
        {
            Singleton<CorpsChangeHeadPicView>.Instance.OpenView();
        }

        private void ChangeNickClickHandler(GameObject go)
        {
            Singleton<CorpsChangeNameView>.Instance.OpenView();
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            if (this.AfterOpenViewGuideDelegate != null)
            {
                this.AfterOpenViewGuideDelegate();
            }
            this.UpdateInfo();
        }

        protected override void Init()
        {
            this.btn_close = base.FindInChild<Button>("btn_close");
            this.btn_changeHeadPic = base.FindInChild<Button>("btn_changeHeadPic");
            this.btn_changeNick = base.FindInChild<Button>("btn_changeNick");
            this.headPic = base.FindInChild<UISprite>("btn_head/headPic");
            this.levelLab = base.FindInChild<UILabel>("zhandui/levelLab");
            this.expLab = base.FindInChild<UILabel>("zhandui/expLab");
            this.levelLimitLab = base.FindInChild<UILabel>("zhandui/levelLimitLab");
            this.levelNum = base.FindInChild<UILabel>("zhandui/levelNum");
            this.expNum = base.FindInChild<UILabel>("zhandui/expNum");
            this.levelLimitNum = base.FindInChild<UILabel>("zhandui/levelLimitNum");
            this.nameLab = base.FindInChild<UILabel>("name/nameLab");
            this.guildNameLab = base.FindInChild<UILabel>("gonghui/gonghuiNameLabText");
            this.guildIdLab = base.FindInChild<UILabel>("gonghui/gonghuiIdLabText");
            this.InitClick();
        }

        private void InitClick()
        {
            this.btn_close.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
            this.btn_changeHeadPic.onClick = new UIWidgetContainer.VoidDelegate(this.ChangeHeadClickHandler);
            this.btn_changeNick.onClick = new UIWidgetContainer.VoidDelegate(this.ChangeNickClickHandler);
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<CorpsMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateHeadSprite);
            Singleton<CorpsMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateRoleName);
        }

        private void UpdateHeadSprite(object sender, int code)
        {
            if (Singleton<CorpsMode>.Instance.UPDATE_ROLEICON == code)
            {
                this.headPic.spriteName = Singleton<CorpsMode>.Instance.RoleIconId.ToString();
                this.headPic.MakePixelPerfect();
            }
        }

        private void UpdateInfo()
        {
            this.levelNum.text = MeVo.instance.Level.ToString();
            this.expNum.text = MeVo.instance.exp.ToString() + "/" + Singleton<CorpsMode>.Instance.GetExpTotal.ToString();
            this.levelLimitNum.text = Singleton<CorpsMode>.Instance.GetLevUp.ToString();
            this.nameLab.text = MeVo.instance.Name;
            this.headPic.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this.headPic.spriteName = Singleton<GeneralMode>.Instance.generalInfoList[MeVo.instance.generalId].genralTemplateInfo.icon_id.ToString();
            this.headPic.MakePixelPerfect();
            this.headPic.SetDimensions(80, 80);
        }

        public void UpdateRoleName(object sender, int type)
        {
            if (Singleton<CorpsMode>.Instance.UPDATE_ROLENAME == type)
            {
                this.nameLab.text = MeVo.instance.Name;
            }
        }

        public Button ChangeAvatorButton
        {
            get
            {
                return this.btn_changeHeadPic;
            }
        }

        public Button CloseButton
        {
            get
            {
                return this.btn_close;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Corps/Corps.assetbundle";
            }
        }
    }
}

