﻿namespace com.game.module.Role
{
    using com.game.module.core;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class FriendMode : BaseMode<FriendMode>
    {
        private List<RelationPushAcceptMsg_7_3> _askList = new List<RelationPushAcceptMsg_7_3>();
        public const int ASKLIST = 4;
        public const int BLACKLIST = 2;
        public const int FRIEND = 1;
        public const int FRIENDNUM = 5;
        public const int NEARBY = 3;

        public void AcceptFriend(ulong roleId, bool accept)
        {
            if (accept)
            {
                for (int i = 0; i < this.askList.Count; i++)
                {
                    if (this.askList[i].roleId == roleId)
                    {
                        RelationPushAcceptMsg_7_3 g__ = this.askList[i];
                        PRelationInfo info = new PRelationInfo {
                            roleId = roleId,
                            sex = g__.sex,
                            name = g__.name,
                            job = g__.job,
                            lvl = g__.lvl,
                            vip = g__.vip,
                            isOnline = 1,
                            intimate = 0
                        };
                        this.AddFriend(info);
                    }
                }
            }
            this.DeleteFriendAskInfo(roleId);
        }

        private void AddBlackList(PRelationInfo info)
        {
            if (this.blacksList != null)
            {
                for (int i = 0; i < this.blacksList.Count; i++)
                {
                    if (this.blacksList[i].roleId == info.roleId)
                    {
                        this.blacksList[i] = info;
                        return;
                    }
                }
            }
            else
            {
                this.blacksList = new List<PRelationInfo>();
            }
            this.blacksList.Add(info);
            base.DataUpdate(2);
        }

        public void AddFriend(PRelationInfo info)
        {
            if (this.friendsList != null)
            {
                this.friendsList = new List<PRelationInfo>();
            }
            for (int i = 0; i < this.friendsList.Count; i++)
            {
                if (this.friendsList[i].roleId == info.roleId)
                {
                    this.friendsList[i] = info;
                    return;
                }
            }
            this.friendsList.Add(info);
            base.DataUpdate(1);
        }

        public bool AddFriendAskMsg(RelationPushAcceptMsg_7_3 ask)
        {
            for (int i = 0; i < this.askList.Count; i++)
            {
                RelationPushAcceptMsg_7_3 g__ = this.askList[i];
                if (g__.roleId == ask.roleId)
                {
                    this.askList[i] = ask;
                    return false;
                }
            }
            this.askList.Add(ask);
            base.DataUpdate(4);
            return true;
        }

        public void DeleteBlackList(ulong roleId)
        {
            int index = -1;
            if (this.blacksList != null)
            {
                for (int i = 0; i < this.blacksList.Count; i++)
                {
                    if (this.blacksList[i].roleId == roleId)
                    {
                        index = i;
                        break;
                    }
                }
                if (index > -1)
                {
                    this.blacksList.RemoveAt(index);
                    base.DataUpdate(2);
                }
            }
        }

        public void DeleteFriend(ulong roleId)
        {
            int index = -1;
            if (this.friendsList != null)
            {
                for (int i = 0; i < this.friendsList.Count; i++)
                {
                    if (this.friendsList[i].roleId == roleId)
                    {
                        index = i;
                        break;
                    }
                }
                if (index > -1)
                {
                    this.friendsList.RemoveAt(index);
                    base.DataUpdate(1);
                }
            }
        }

        private void DeleteFriendAskInfo(ulong roleId)
        {
            int index = -1;
            for (int i = 0; i < this.askList.Count; i++)
            {
                if (this.askList[i].roleId == roleId)
                {
                    index = i;
                    break;
                }
            }
            if (index > -1)
            {
                this.askList.RemoveAt(index);
                base.DataUpdate(4);
            }
        }

        public void MoveToBlackList(ulong roleId)
        {
            int index = -1;
            if (this.friendsList != null)
            {
                for (int i = 0; i < this.friendsList.Count; i++)
                {
                    if (this.friendsList[i].roleId == roleId)
                    {
                        index = i;
                        this.AddBlackList(this.friendsList[i]);
                        break;
                    }
                }
                if (index > -1)
                {
                    this.friendsList.RemoveAt(index);
                    base.DataUpdate(1);
                }
            }
            index = -1;
            if (this.nearByList != null)
            {
                for (int j = 0; j < this.nearByList.Count; j++)
                {
                    if (this.nearByList[j].roleId == roleId)
                    {
                        index = j;
                        PRelationInfo info = new PRelationInfo {
                            roleId = this.nearByList[j].roleId,
                            sex = this.nearByList[j].sex,
                            name = this.nearByList[j].name,
                            job = this.nearByList[j].job,
                            lvl = this.nearByList[j].lvl,
                            isOnline = 1,
                            intimate = 0
                        };
                        this.AddBlackList(info);
                        break;
                    }
                }
                if (index > -1)
                {
                    this.nearByList.RemoveAt(index);
                    base.DataUpdate(3);
                }
            }
            index = -1;
            if (this.askList != null)
            {
                for (int k = 0; k < this.askList.Count; k++)
                {
                    if (this.askList[k].roleId == roleId)
                    {
                        index = k;
                        PRelationInfo info2 = new PRelationInfo {
                            roleId = this.askList[k].roleId,
                            sex = this.askList[k].sex,
                            name = this.askList[k].name,
                            job = this.askList[k].job,
                            lvl = this.askList[k].lvl,
                            vip = this.askList[k].vip,
                            isOnline = 1,
                            intimate = 0
                        };
                        this.AddBlackList(info2);
                        break;
                    }
                }
                if (index > -1)
                {
                    this.askList.RemoveAt(index);
                    base.DataUpdate(4);
                }
            }
        }

        public void SetFriendInfoList(List<PRelationInfo> friendsList, List<PRelationInfo> blacksList)
        {
            this.friendsList = friendsList;
            this.blacksList = blacksList;
            base.DataUpdate(1);
            base.DataUpdate(2);
        }

        public void SetFriendIntimate(ulong roleId, uint itm)
        {
            int num = -1;
            if (this.friendsList != null)
            {
                for (int i = 0; i < this.friendsList.Count; i++)
                {
                    if (this.friendsList[i].roleId == roleId)
                    {
                        num = i;
                        break;
                    }
                }
                if (num > -1)
                {
                    this.friendsList[num].intimate = itm;
                    base.DataUpdate(1);
                }
            }
        }

        public void SetFriendLvl(uint roleId, byte lvl)
        {
            int num = -1;
            if (this.friendsList != null)
            {
                for (int i = 0; i < this.friendsList.Count; i++)
                {
                    if (this.friendsList[i].roleId == roleId)
                    {
                        num = i;
                        break;
                    }
                }
                if (num > -1)
                {
                    this.friendsList[num].lvl = lvl;
                    base.DataUpdate(1);
                }
            }
        }

        public void SetFriendNumInfo(ushort friendNum, ushort maxNum)
        {
            this.friendNum = friendNum;
            this.maxNum = maxNum;
            base.DataUpdate(5);
        }

        public void SetFriendOlineInfo(ulong roleId, byte online)
        {
            int num = -1;
            if (this.friendsList != null)
            {
                for (int i = 0; i < this.friendsList.Count; i++)
                {
                    if (this.friendsList[i].roleId == roleId)
                    {
                        num = i;
                        break;
                    }
                }
                if (num > -1)
                {
                    this.friendsList[num].isOnline = online;
                    base.DataUpdate(1);
                    return;
                }
            }
            if (this.blacksList != null)
            {
                for (int j = 0; j < this.blacksList.Count; j++)
                {
                    if (this.blacksList[j].roleId == roleId)
                    {
                        num = j;
                        break;
                    }
                }
                if (num > -1)
                {
                    this.blacksList[num].isOnline = online;
                    base.DataUpdate(2);
                }
            }
        }

        public void SetNearbyPlayerInfo(List<PRelationNear> nearByList)
        {
            this.nearByList = nearByList;
            base.DataUpdate(3);
        }

        public List<RelationPushAcceptMsg_7_3> askList
        {
            get
            {
                return this._askList;
            }
        }

        public List<PRelationInfo> blacksList { get; private set; }

        public ushort friendNum { get; private set; }

        public List<PRelationInfo> friendsList { get; private set; }

        public ushort maxNum { get; private set; }

        public List<PRelationNear> nearByList { get; private set; }

        public override bool ShowTips
        {
            get
            {
                return (this.askList.Count > 0);
            }
        }
    }
}

