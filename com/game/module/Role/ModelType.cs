﻿namespace com.game.module.Role
{
    using System;

    public enum ModelType
    {
        Pet,
        Role,
        Monster,
        Npc,
        Trap,
        Teleport,
        Load
    }
}

