﻿namespace com.game.module.Role
{
    using com.game;
    using com.game.basic;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.SDK;
    using com.game.vo;
    using com.liyong;
    using com.net;
    using com.net.interfaces;
    using com.u3d.bases.debug;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class RoleControl : BaseControl<RoleControl>
    {
        public void Fun_3_1(INetData data)
        {
            Log.info(this, "-Fun_3_1（）获取人物详细属性");
            RoleInfoMsg_3_1 g__ = new RoleInfoMsg_3_1();
            g__.read(data.GetMemoryStream());
            PRoleAttr role = g__.role;
            MeVo.instance.Id = role.id;
            MeVo.instance.Name = role.name;
            MeVo.instance.sex = role.sex;
            MeVo.instance.Level = role.level;
            MeVo.instance.job = role.job;
            MeVo.instance.exp = role.exp;
            MeVo.instance.Fight = (uint) role.combat;
            MeVo.instance.expFull = role.expFull;
            Singleton<VIPMode>.Instance.vip = role.vip;
            MeVo.instance.nation = role.nation;
            MeVo.instance.diam = role.gold;
            MeVo.instance.diamond = role.diam;
            MeVo.instance.bindingDiamond = role.diamBind;
            MeVo.instance.vigour = role.vigour;
            MeVo.instance.vigourFull = role.vigourFull;
            MeVo.instance.hasCombine = role.hasCombine;
            MeVo.instance.customFace = role.customFace;
            MeVo.instance.titleList = role.titleList;
            MeVo.instance.repu = role.repu;
            MeVo.instance.generalId = role.generalId;
            MeVo.instance.stylebin = role.styleBin;
            MeVo.instance.generalTemplateInfo = BaseDataMgr.instance.GetGeneralVo((uint) MeVo.instance.stylebin.id, MeVo.instance.stylebin.quality);
            if (MeVo.instance.guildId != role.guildId)
            {
                MeVo.instance.guildId = role.guildId;
                MeVo.instance.guildName = role.guildName;
            }
            MeVo.instance.DataUpdate(0);
            Singleton<RoleMode>.Instance.UpdateMeVoInfo();
            GlobalAPI.facade.Notify(0x12d, 0, 0, null);
        }

        private void Fun_3_10(INetData data)
        {
            RoleExpIncMsg_3_10 g__ = new RoleExpIncMsg_3_10();
            g__.read(data.GetMemoryStream());
            MeVo.instance.exp += g__.inc;
        }

        private void Fun_3_11(INetData data)
        {
            RoleUpgradeMsg_3_11 g__ = new RoleUpgradeMsg_3_11();
            g__.read(data.GetMemoryStream());
            int level = MeVo.instance.Level;
            MeVo.instance.exp = g__.exp;
            MeVo.instance.expFull = g__.expFull;
            MeVo.instance.Level = g__.lvl;
            SDKManager.SDKRoleLevelLog(g__.lvl.ToString(), Singleton<LoginMode>.Instance.serverId.ToString());
            Singleton<RoleMode>.Instance.PlayerUpgrade(level, g__.lvl);
            GlobalAPI.facade.Notify(300, level, g__.lvl, null);
        }

        private void Fun_3_2(INetData data)
        {
            RoleListMsg_3_2 g__ = new RoleListMsg_3_2();
            g__.read(data.GetMemoryStream());
            Singleton<RoleMode>.Instance.UpdateBuffList(g__.list);
            Log.info(this, "-Fun_3_2()获取角色buff列表，Count : " + g__.list.Count);
        }

        private void Fun_3_20(INetData data)
        {
            RoleReviveMsg_3_20 g__ = new RoleReviveMsg_3_20();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                MeVo.instance.AddHpToPercent((uint) 100);
                if ((g__.type == 4) && (MeVo.instance.mapId == 0x9c41))
                {
                    AppMap.Instance.me.Pos(3f, 1.8f);
                }
                ((MeControler) AppMap.Instance.me.Controller).Relive();
                Singleton<RoleMode>.Instance.UpdateReLife();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void Fun_3_3(INetData data)
        {
            RoleInfoOtherMsg_3_3 g__ = new RoleInfoOtherMsg_3_3();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                Singleton<RoleMode>.Instance.UpdateOtherInfo(g__.info);
            }
        }

        private void Fun_3_4(INetData data)
        {
            Log.info(this, "-Fun_3_4()玩家基础属性更新");
            RoleAttrMsg_3_4 g__ = new RoleAttrMsg_3_4();
            g__.read(data.GetMemoryStream());
            PNewAttr attr = g__.attr;
            MeVo.instance.Hp = attr.hpFull;
            MeVo.instance.SetHp(attr.hp);
            MeVo.instance.AttrPhy = attr.attP;
            MeVo.instance.AttrMag = attr.attM;
            MeVo.instance.DefP = attr.defP;
            MeVo.instance.DefM = attr.defM;
            MeVo.instance.Hit = attr.hit;
            MeVo.instance.Dodge = attr.dodge;
            MeVo.instance.Crit = attr.crit;
            MeVo.instance.Flex = attr.flex;
            MeVo.instance.AbDef = attr.abDef;
            MeVo.instance.AbHurt = attr.abHurt;
            MeVo.instance.FightGeneral = (uint) attr.id;
            Singleton<RoleMode>.Instance.UpdateAttr();
        }

        private void Fun_3_40(INetData data)
        {
            RoleVigourBuyMsg_3_40 g__ = new RoleVigourBuyMsg_3_40();
            g__.read(data.GetMemoryStream());
            Log.info(this, string.Concat(new object[] { "体力购买错误码: ", g__.code, "购买方式：", g__.type }));
            if (g__.code > 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("Game.SuccessToBuyVigour"));
            }
        }

        private void Fun_3_41(INetData data)
        {
            RoleVigourInfoMsg_3_41 g__ = new RoleVigourInfoMsg_3_41();
            g__.read(data.GetMemoryStream());
            MeVo.instance.vigour = g__.vigour;
            MeVo.instance.numBuyVigour = g__.dailyBuyTimes;
            MeVo.instance.vigourFull = g__.vigourFull;
            MeVo.instance.DiamNeedForVigour = g__.diam;
            MeVo.instance.lastVigourRecoverTime = (int) g__.lastRecoverTime;
            Log.info(this, "体力有更新，新的体力值: " + MeVo.instance.vigour);
            Singleton<RoleMode>.Instance.UpdateVigour();
        }

        private void Fun_3_44(INetData data)
        {
            <Fun_3_44>c__AnonStoreyE9 ye = new <Fun_3_44>c__AnonStoreyE9 {
                roleSyncMsg = new RoleSyncMsg_3_44()
            };
            ye.roleSyncMsg.read(data.GetMemoryStream());
            if (ye.roleSyncMsg.id != MeVo.instance.Id)
            {
                PlayerDisplay player = AppMap.Instance.GetPlayer(ye.roleSyncMsg.id.ToString());
                if ((player != null) && (player.Controller != null))
                {
                    Vector3 vec = new Vector3((float) ye.roleSyncMsg.x, (float) ye.roleSyncMsg.y, (float) ye.roleSyncMsg.z);
                    vec = vec.FromProtocolInt();
                    player.Controller.StatuController.MeControler.WalkTo(vec, new MoveEndCallback(ye.<>m__B5), true, false);
                    player.Controller.StatuController.SetStatu(ye.roleSyncMsg.state);
                    if (ye.roleSyncMsg.state == 0)
                    {
                        CommandHandler.AddCommandStatic(player.GoBase, "idle", 1f);
                    }
                    player.SetSortingOrder(false);
                }
            }
        }

        private void Fun_3_5(INetData data)
        {
            Log.info(this, "-Fun_3_5()玩家财富值更新");
            RoleFortuneMsg_3_5 g__ = new RoleFortuneMsg_3_5();
            g__.read(data.GetMemoryStream());
            MeVo.instance.diam = g__.gold;
            MeVo.instance.diamond = g__.diam;
            MeVo.instance.bindingDiamond = g__.diamBind;
            MeVo.instance.repu = g__.repu;
            Singleton<RoleMode>.Instance.UpdateFortune();
            Singleton<WaitingView>.Instance.CloseView();
            Log.info(this, string.Concat(new object[] { "chafuxin..............................................................", MeVo.instance.diam, "................", MeVo.instance.diamond, "................", MeVo.instance.bindingDiamond }));
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("769", new NetMsgCallback(this.Fun_3_1));
            AppNet.main.addCMD("770", new NetMsgCallback(this.Fun_3_2));
            AppNet.main.addCMD("771", new NetMsgCallback(this.Fun_3_3));
            AppNet.main.addCMD("772", new NetMsgCallback(this.Fun_3_4));
            AppNet.main.addCMD("773", new NetMsgCallback(this.Fun_3_5));
            AppNet.main.addCMD("808", new NetMsgCallback(this.Fun_3_40));
            AppNet.main.addCMD("809", new NetMsgCallback(this.Fun_3_41));
            AppNet.main.addCMD("778", new NetMsgCallback(this.Fun_3_10));
            AppNet.main.addCMD("779", new NetMsgCallback(this.Fun_3_11));
            AppNet.main.addCMD("788", new NetMsgCallback(this.Fun_3_20));
            AppNet.main.addCMD("812", new NetMsgCallback(this.Fun_3_44));
        }

        public void RequestOtherInfo(ulong roleId)
        {
            Singleton<RoleMode>.Instance.RequestOtherRoleInfo(roleId);
            Log.info(this, "请求他人角色信息，角色Id : " + roleId);
        }

        public void SendHpMpChange()
        {
            Singleton<RoleMode>.Instance.SendHeroHpAndMagic(MeVo.instance.CurHp, MeVo.instance.CurMp);
        }

        [CompilerGenerated]
        private sealed class <Fun_3_44>c__AnonStoreyE9
        {
            internal RoleSyncMsg_3_44 roleSyncMsg;

            internal void <>m__B5(BaseControler bc)
            {
                bc.Me.ChangeDire((this.roleSyncMsg.dir * 0.001f) * 0.001f);
            }
        }
    }
}

