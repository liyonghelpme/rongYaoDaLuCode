﻿namespace com.game.module.Role
{
    using com.game.data;
    using com.game.manager;
    using com.game.module;
    using com.u3d.bases.display;
    using com.u3d.bases.display.vo;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class RoleDisplay : BaseDisplay
    {
        private static RoleDisplay instance = new RoleDisplay();
        public com.game.module.Role.ModelControl modelControl;
        private Transform parent;
        private int roleType = -1;
        private string url;
        private DisplayVo vo = new DisplayVo();

        protected override void AddScript(GameObject go)
        {
            NGUITools.SetLayer(base.GoBase, LayerMask.NameToLayer("Mode"));
            SpriteRenderer[] componentsInChildren = go.GetComponentsInChildren<SpriteRenderer>();
            if (componentsInChildren == null)
            {
            }
            foreach (SpriteRenderer renderer in componentsInChildren)
            {
                renderer.sortingLayerName = "Default";
                renderer.sortingOrder += 100;
            }
            if (this.parent != null)
            {
                go.transform.parent = this.parent;
                this.parent = null;
            }
            else
            {
                go.transform.parent = com.game.module.ViewTree.go.transform;
            }
            go.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
            go.transform.localScale = new Vector3(170f, 170f, 170f);
            go.transform.localPosition = new Vector3(this.vo.X, this.vo.Y, 0f);
            if (this.modelControl != null)
            {
                this.modelControl(go);
            }
        }

        public void ChangePosition(float x, float y, int roleType = 0, com.game.module.Role.ModelControl modelControl = null)
        {
            this.vo.X = x;
            this.vo.Y = y;
            this.roleType = roleType;
            this.parent = null;
            if (this.roleType == 0)
            {
                base.GoBase.transform.localPosition = new Vector3(x, y, 0f);
                if (modelControl != null)
                {
                    modelControl(base.GoBase);
                }
            }
            else
            {
                this.CreateRole(roleType, modelControl, false);
            }
        }

        public void CreateModel(ModelType type, string id, LoadAssetFinish<GameObject> loadInitCallBack)
        {
            if (id.Equals("0"))
            {
                id = "10004";
            }
            object[] objArray1 = new object[] { "Model/", type, "/", id, "/Model/", id, ".assetbundle" };
            string fileName = string.Concat(objArray1);
            AssetManager.Instance.LoadAsset<GameObject>(fileName, loadInitCallBack, null, false, true);
        }

        public void CreateRole(int roleType, com.game.module.Role.ModelControl loadInitCallBack, bool isInCopy = false)
        {
            this.roleType = roleType;
            this.vo = new DisplayVo();
            this.vo.Type = 0x65;
            SysRoleBaseInfoVo sysRoleBaseInfo = BaseDataMgr.instance.GetSysRoleBaseInfo((uint) roleType);
            object[] objArray1 = new object[] { "Model/Role/", roleType, "/Model/", roleType, ".assetbundle" };
            this.vo.ClothUrl = string.Concat(objArray1);
            this.modelControl = loadInitCallBack;
            this.SetVo(this.vo);
        }

        public void RemoveModel()
        {
            base.GoBase.transform.localPosition = new Vector3(2000f, 2000f, 0f);
        }

        public static RoleDisplay Instance
        {
            get
            {
                return instance;
            }
        }
    }
}

