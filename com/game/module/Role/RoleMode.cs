﻿namespace com.game.module.Role
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.debug;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class RoleMode : BaseMode<RoleMode>
    {
        private string _roleName;
        public ulong CurrentRoleId;
        public int CurrentTipsType;
        private int newLevel;
        private int oldLevel;
        public readonly int PLAYER_UPGRADE_INFO = 5;
        public readonly int UPDATE_BUFF = 2;
        public readonly int UPDATE_FORTUNE = 3;
        public const int UPDATE_OTHER_INFO = 4;
        public readonly int UPDATE_ROLE_ATTR = 6;
        public const int UPDATE_ROLE_INFO = 8;
        public readonly int UPDATE_ROLE_RELIFE = 7;
        public const int UPDATE_VIGOUR = 1;

        public void BuyVigour(byte type)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_40(msdata, type);
            AppNet.gameNet.send(msdata, 3, 40);
        }

        public string GetPlayerHeadSpriteName(byte job)
        {
            switch (job)
            {
                case 1:
                    return "101";

                case 2:
                    return "201";

                case 3:
                    return "301";
            }
            return string.Empty;
        }

        public int GetTeamLevel()
        {
            return this.newLevel;
        }

        public void PlayerUpgrade(int preLevel, int curLevel)
        {
            this.oldLevel = preLevel;
            this.newLevel = curLevel;
            base.DataUpdate(this.PLAYER_UPGRADE_INFO);
        }

        public void ReLife()
        {
            this.ReLife(1);
        }

        public void ReLife(byte type)
        {
            Log.info(this, "-ReLife()发送3-20复活协议，参数：" + type);
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_20(msdata, type);
            AppNet.gameNet.send(msdata, 3, 20);
        }

        public void RequestBuffList()
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_2(msdata);
            AppNet.gameNet.send(msdata, 3, 2);
        }

        public void RequestOtherRoleInfo(ulong id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_3(msdata, id);
            AppNet.gameNet.send(msdata, 3, 3);
        }

        public void SendHeroHpAndMagic(uint hp, uint mp)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_6(msdata, hp, mp);
            AppNet.gameNet.send(msdata, 3, 6);
        }

        public void SendStatuChange()
        {
            MemoryStream msdata = new MemoryStream();
            Vector3 position = AppMap.Instance.me.GoBase.transform.position;
            float curDire = AppMap.Instance.me.CurDire;
            byte state = Convert.ToByte(AppMap.Instance.me.Controller.StatuController.CurrentStatu);
            int[] numArray = position.ToProtocolInt();
            int dir = curDire.ToProtocolInt().ToProtocolInt();
            Module_3.write_3_44(msdata, state, numArray[0], numArray[1], numArray[2], dir);
            AppNet.gameNet.send(msdata, 3, 0x2c);
        }

        public void UpdateAttr()
        {
            base.DataUpdate(this.UPDATE_ROLE_ATTR);
        }

        public void UpdateBuffList(List<PBuff> buffList)
        {
            this.buffList = buffList;
            base.DataUpdate(this.UPDATE_BUFF);
        }

        public void UpdateFortune()
        {
            base.DataUpdate(this.UPDATE_FORTUNE);
        }

        public void UpdateMeVoInfo()
        {
            base.DataUpdate(8);
        }

        public void UpdateMoney()
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_5(msdata);
            AppNet.gameNet.send(msdata, 3, 5);
        }

        public void UpdateOtherInfo(POtherRoleInfo roleAttr)
        {
            this.otherAttr = roleAttr;
            base.DataUpdate(4);
        }

        public void UpdateReLife()
        {
            base.DataUpdate(this.UPDATE_ROLE_RELIFE);
        }

        public void UpdateVigour()
        {
            base.DataUpdate(1);
        }

        public List<PBuff> buffList { get; set; }

        public POtherRoleInfo otherAttr { get; set; }

        public string roleName
        {
            get
            {
                return this._roleName;
            }
            set
            {
                this._roleName = value;
            }
        }

        public bool Upgraded
        {
            get
            {
                return (this.newLevel > this.oldLevel);
            }
        }
    }
}

