﻿namespace com.game.module.Role.RoleOther
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class RoleOtherView : BaseView<RoleOtherView>
    {
        private UISprite big_icon;
        private Button close_btn;
        private UILabel fight_effect;
        private UILabel guild_name;
        private UILabel role_lvl;
        private UILabel role_name;

        public override void CancelUpdateHandler()
        {
            Singleton<RoleMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateRoleInfo);
        }

        protected override void HandleAfterOpenView()
        {
            this.big_icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this.big_icon.spriteName = BaseDataMgr.instance.GetGeneralVo(Singleton<RoleMode>.Instance.otherAttr.generalId, 0).icon_id.ToString();
            this.big_icon.MakePixelPerfect();
            this.big_icon.SetDimensions(80, 80);
            this.role_name.text = Singleton<RoleMode>.Instance.otherAttr.name.ToString();
            this.role_lvl.text = Singleton<RoleMode>.Instance.otherAttr.lvl.ToString();
        }

        protected override void Init()
        {
            this.big_icon = base.FindInChild<UISprite>("big_icon/icon");
            this.role_name = base.FindInChild<UILabel>("name");
            this.role_lvl = base.FindInChild<UILabel>("big_icon/lvl_icon/Label");
            this.close_btn = base.FindInChild<Button>("mask");
            this.close_btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        private void OnClick(GameObject go)
        {
            this.CloseView();
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<RoleMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateRoleInfo);
        }

        private void UpdateRoleInfo(object sender, int code)
        {
            if (code == 4)
            {
                this.big_icon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                this.big_icon.spriteName = BaseDataMgr.instance.GetGeneralVo(Singleton<RoleMode>.Instance.otherAttr.generalId, 0).icon_id.ToString();
                this.big_icon.MakePixelPerfect();
                this.big_icon.SetDimensions(80, 80);
                this.role_name.text = Singleton<RoleMode>.Instance.otherAttr.name.ToString();
                this.role_lvl.text = Singleton<RoleMode>.Instance.otherAttr.lvl.ToString();
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/RoleOther/RoleOtherView.assetbundle";
            }
        }
    }
}

