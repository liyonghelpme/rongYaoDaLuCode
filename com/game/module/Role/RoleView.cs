﻿namespace com.game.module.Role
{
    using com.game.consts;
    using com.game.manager;
    using com.game.module.core;
    using com.game.vo;
    using System;
    using UnityEngine;

    public class RoleView : BaseView<RoleView>
    {
        public EventDelegate.Callback AfterOpenViewGuideDelegate;
        public Button btn_close;
        public UIToggle ckb_beibao;
        public UIToggle ckb_medal;
        public UIToggle ckb_peiyue;
        public UIToggle ckb_shuxing;
        private int currentClickNavID;
        private int currentId;
        private UIToggle currentTab;
        private bool isfirst;
        private UIToggle lastTab;
        private SpinWithMouse modelSpin;
        private GameObject repuGameObject;
        private GameObject roleBackground;
        public RoleDisplay RoleModel;
        public TweenPosition tweenPosition;

        public override void CancelUpdateHandler()
        {
        }

        private void DisableModel()
        {
            this.roleBackground.SetActive(false);
            if ((this.RoleModel != null) && (this.RoleModel.GoBase != null))
            {
                NGUITools.SetActive(this.RoleModel.GoBase, false);
            }
        }

        protected override void HandleAfterOpenView()
        {
            this.TabViewHelp();
            if (this.RoleModel == null)
            {
            }
            TweenUtils.AdjustTransformToClick(this.tweenPosition);
            if (this.AfterOpenViewGuideDelegate != null)
            {
                EventDelegate.Add(this.tweenPosition.onFinished, this.AfterOpenViewGuideDelegate);
            }
        }

        protected override void HandleBeforeCloseView()
        {
            this.SetToggleFalseHelp(this.ckb_medal);
            this.SetToggleFalseHelp(this.ckb_beibao);
            this.SetToggleFalseHelp(this.ckb_peiyue);
            this.SetToggleFalseHelp(this.ckb_shuxing);
            this.DisableModel();
        }

        protected override void Init()
        {
            base.showTween = base.FindInChild<TweenPlay>(null);
            this.roleBackground = base.FindChild("rolebackground");
            this.repuGameObject = base.FindChild("Medal/repu");
            this.modelSpin = NGUITools.FindInChild<SpinWithMouse>(this.roleBackground, "rolebackgroundl");
            this.tweenPosition = base.FindInChild<TweenPosition>(null);
            this.ckb_shuxing = base.FindInChild<UIToggle>("top/ckb_shuxing");
            this.ckb_shuxing.FindInChild<UILabel>("label").text = LanguageManager.GetWord("RoleView.PropertyTab");
            this.ckb_beibao = base.FindInChild<UIToggle>("top/ckb_beibao");
            this.ckb_beibao.FindInChild<UILabel>("label").text = LanguageManager.GetWord("RoleView.BagTab");
            this.ckb_peiyue = base.FindInChild<UIToggle>("top/ckb_peiyu");
            this.ckb_peiyue.FindInChild<UILabel>("label").text = LanguageManager.GetWord("RoleView.GrowTab");
            this.ckb_medal = base.FindInChild<UIToggle>("top/ckb_medal");
            this.ckb_medal.FindInChild<UILabel>("label").text = LanguageManager.GetWord("RoleView.MedalTab");
            this.btn_close = base.FindInChild<Button>("topright/btn_close");
            this.btn_close.clickDelegate = (UIWidgetContainer.ClickDelegate) Delegate.Combine(this.btn_close.clickDelegate, new UIWidgetContainer.ClickDelegate(this.CloseView));
            EventDelegate.Add(this.ckb_medal.onChange, new EventDelegate.Callback(this.TabViewOnClick));
            EventDelegate.Add(this.ckb_shuxing.onChange, new EventDelegate.Callback(this.TabViewOnClick));
            EventDelegate.Add(this.ckb_peiyue.onChange, new EventDelegate.Callback(this.TabViewOnClick));
            EventDelegate.Add(this.ckb_beibao.onChange, new EventDelegate.Callback(this.TabViewOnClick));
            Singleton<RolePropView>.Instance.gameObject = base.FindChild("Property");
        }

        public void LoadCallBack(GameObject go)
        {
            this.modelSpin.target = this.RoleModel.GoBase.transform.GetChild(0);
            this.modelSpin.speed = 3f;
            this.SetModelPosition();
        }

        public void OpenGoodsView()
        {
            this.currentId = 1;
            this.OpenView();
        }

        public void OpenProView()
        {
            this.currentId = 2;
            this.OpenView();
        }

        public void OpenSkillView()
        {
            this.currentId = 3;
            this.OpenView();
        }

        public override void RegisterUpdateHandler()
        {
        }

        private void setClickNavState(int type)
        {
            switch (type)
            {
                case 1:
                    this.ckb_beibao.value = true;
                    break;

                case 2:
                    this.ckb_shuxing.value = true;
                    break;

                case 3:
                    this.ckb_medal.value = true;
                    break;

                case 4:
                    this.ckb_peiyue.value = true;
                    break;
            }
        }

        private void SetModelPosition()
        {
            this.roleBackground.SetActive(true);
            if ((this.RoleModel != null) && (this.RoleModel.GoBase != null))
            {
                NGUITools.SetActive(this.RoleModel.GoBase, true);
                this.RoleModel.GoBase.transform.localPosition = new Vector3(-248.8286f, -162f, 0f);
            }
        }

        private void SetToggleFalseHelp(UIToggle toggle)
        {
            toggle.optionCanBeNone = true;
            toggle.value = false;
            toggle.optionCanBeNone = false;
        }

        private void TabViewHelp()
        {
            if (this.currentId == 1)
            {
                this.currentTab = this.ckb_beibao;
                this.ckb_beibao.value = true;
            }
            if (this.currentId == 2)
            {
                this.currentTab = this.ckb_shuxing;
                this.ckb_shuxing.value = true;
            }
            if (this.currentId == 3)
            {
                this.currentTab = this.ckb_medal;
                this.ckb_medal.value = true;
            }
            if (this.currentId == 4)
            {
                this.currentTab = this.ckb_peiyue;
                this.ckb_peiyue.value = true;
            }
        }

        private void TabViewOnClick()
        {
            UIToggle current = UIToggle.current;
            if (current.Equals(this.ckb_beibao))
            {
                if (!current.value)
                {
                    Singleton<BagView>.Instance.CloseView();
                }
                else
                {
                    this.currentClickNavID = 1;
                    Singleton<BagView>.Instance.OpenView();
                    this.SetModelPosition();
                }
            }
            else if (current.Equals(this.ckb_shuxing))
            {
                if (!UIToggle.current.value)
                {
                    Singleton<RolePropView>.Instance.CloseView();
                }
                else
                {
                    this.currentClickNavID = 2;
                    Singleton<RolePropView>.Instance.OpenView();
                    this.SetModelPosition();
                }
            }
            else if (UIToggle.current.Equals(this.ckb_medal))
            {
                if (UIToggle.current.value)
                {
                    if (OpenLevelLimitManager.checkLeveByGuideLimitID(0x71, MeVo.instance.Level))
                    {
                        this.setClickNavState(this.currentClickNavID);
                        return;
                    }
                    this.currentClickNavID = 3;
                    this.repuGameObject.SetActive(true);
                    this.DisableModel();
                }
            }
            else if (current.Equals(this.ckb_peiyue) && current.value)
            {
                if (OpenLevelLimitManager.checkLeveByGuideLimitID(0x70, MeVo.instance.Level))
                {
                    this.setClickNavState(this.currentClickNavID);
                    return;
                }
                this.currentClickNavID = 4;
                this.SetModelPosition();
            }
            if (current.value)
            {
                current.FindInChild<UILabel>("label").color = Color.white;
            }
            else
            {
                current.FindInChild<UILabel>("label").color = ColorConst.FONT_GRAY;
            }
            if (current.Equals(this.ckb_medal))
            {
                this.roleBackground.SetActive(false);
                this.repuGameObject.SetActive(true);
            }
            else
            {
                this.repuGameObject.SetActive(false);
                this.roleBackground.SetActive(true);
            }
            this.UpdatePeiyuTips();
        }

        private void UpdatePeiyuTips()
        {
            this.ckb_peiyue.FindInChild<UISprite>("tips").SetActive(false);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Role/RoleView.assetbundle";
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.CityView;
            }
        }

        private enum NAVIGATION_TYPE
        {
            NAV_BEIBAO = 1,
            NAV_PEIYU = 4,
            NAV_SHUXING = 2,
            NAV_XUNZHANG = 3
        }
    }
}

