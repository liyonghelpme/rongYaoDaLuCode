﻿namespace com.game.module.Role
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.hud;
    using com.game.Public.Message;
    using com.game.utils;
    using com.game.vo;
    using com.net;
    using com.net.interfaces;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SkillControl : BaseControl<SkillControl>
    {
        private static void DoSyncDamage(SkillDamageSyncMsg_13_12 skillDamageSynInfo)
        {
            int displayType = DamageCheck.GetDisplayType(skillDamageSynInfo.actorType);
            ulong actorId = skillDamageSynInfo.actorId;
            ActionDisplay actionDisplayByDisplayTypeAndId = GetActionDisplayByDisplayTypeAndId(displayType, actorId);
            Vector3 position = actionDisplayByDisplayTypeAndId.Controller.SkillController.transform.position;
            SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(skillDamageSynInfo.skillId);
            foreach (PDamage damage in skillDamageSynInfo.damageList)
            {
                ActionVo vo13;
                bool isDodge = false;
                bool isCrit = false;
                bool isParry = false;
                bool isKnockDown = false;
                bool isKnockBack = false;
                List<PDamageBuff> buffList = damage.buffList;
                DamageCheck.SetStateTypeAndDamageType(damage.dmgType, damage.stateType, ref isDodge, ref isCrit, ref isKnockDown, ref isKnockBack);
                switch (DamageCheck.GetDisplayType(damage.type))
                {
                    case 100:
                        if (damage.id == AppMap.Instance.me.GetVo().Id)
                        {
                            if (!isDodge)
                            {
                                ActionVo attackVo = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, AppMap.Instance.me, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                if (attackVo == null)
                                {
                                    break;
                                }
                                attackVo = DamageCheck.FixHurtType(attackVo, isKnockBack, isKnockDown);
                                AppMap.Instance.me.Controller.BeAttackedController.BeAttacked(attackVo);
                            }
                            if (MeVo.instance.CurHp > damage.dmg)
                            {
                                MeVo.instance.ReduceHp(damage.dmg);
                            }
                            else
                            {
                                MeVo.instance.ReduceHpToPercent((uint) 0);
                                if (!MeVo.instance.IsEmptyHp)
                                {
                                    ActionVo vo3 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, AppMap.Instance.me, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                    if (vo3 == null)
                                    {
                                        break;
                                    }
                                    vo3 = DamageCheck.FixHurtType(vo3, isKnockBack, isKnockDown);
                                    AppMap.Instance.me.Controller.BeAttackedController.BeAttacked(vo3);
                                    return;
                                }
                                if (AppMap.Instance.me.Controller.buffController != null)
                                {
                                    AppMap.Instance.me.Controller.buffController.Dead();
                                }
                                vo13 = new ActionVo {
                                    ActionType = "death",
                                    SkillId = skillDamageSynInfo.skillId
                                };
                                ActionVo vo = vo13;
                                AppMap.Instance.me.Controller.AttackController.AddAttackList(vo, true, false);
                            }
                            if (AppMap.Instance.me.Controller.buffController != null)
                            {
                                AppMap.Instance.me.Controller.buffController.AddBuff(buffList, null);
                            }
                            DamageCheck.ShowBuffEffects(actionDisplayByDisplayTypeAndId, AppMap.Instance.me, buffList);
                            Vector3 target = AppMap.Instance.me.Controller.SkillController.MeController.transform.position;
                            HudView.Instance.CreateDamageHudText(target, isDodge, isCrit, isParry, damage.dmgForShow, false, true, false, false, null, actionDisplayByDisplayTypeAndId);
                            DamageCheck.UpdateHealthBarRelated(AppMap.Instance.me, damage.dmg);
                        }
                        else
                        {
                            PlayerDisplay player = AppMap.Instance.GetPlayer(damage.id + string.Empty);
                            if ((player == null) || (player.GoBase == null))
                            {
                                return;
                            }
                            BaseRoleVo vo5 = player.GetVo();
                            if (vo5 == null)
                            {
                                return;
                            }
                            if (vo5.CurHp > damage.dmg)
                            {
                                if (!isDodge)
                                {
                                    ActionVo vo6 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, player, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                    if (vo6 == null)
                                    {
                                        break;
                                    }
                                    vo6 = DamageCheck.FixHurtType(vo6, isKnockBack, isKnockDown);
                                    player.Controller.BeAttackedController.BeAttacked(vo6);
                                }
                                vo5.ReduceHp(damage.dmg);
                            }
                            else
                            {
                                vo5.ReduceHpToPercent((uint) 0);
                                if (!vo5.IsEmptyHp)
                                {
                                    ActionVo vo7 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, player, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                    if (vo7 == null)
                                    {
                                        break;
                                    }
                                    vo7 = DamageCheck.FixHurtType(vo7, isKnockBack, isKnockDown);
                                    player.Controller.BeAttackedController.BeAttacked(vo7);
                                    return;
                                }
                                if (player.Controller.buffController != null)
                                {
                                    player.Controller.buffController.Dead();
                                }
                                vo13 = new ActionVo {
                                    ActionType = "death",
                                    SkillId = skillDamageSynInfo.skillId
                                };
                                ActionVo vo8 = vo13;
                                player.Controller.AttackController.AddAttackList(vo8, true, false);
                            }
                            if (player.Controller.buffController != null)
                            {
                                player.Controller.buffController.AddBuff(buffList, null);
                            }
                            DamageCheck.ShowBuffEffects(actionDisplayByDisplayTypeAndId, player, buffList);
                            Vector3 vector3 = player.Controller.SkillController.MeController.transform.position;
                            HudView.Instance.CreateDamageHudText(vector3, isDodge, isCrit, isParry, damage.dmgForShow, false, false, false, false, null, actionDisplayByDisplayTypeAndId);
                            DamageCheck.UpdateHealthBarRelated(player, damage.dmg);
                        }
                        break;

                    case 400:
                    {
                        MonsterDisplay monster = AppMap.Instance.GetMonster(damage.id + string.Empty);
                        if ((monster == null) || (monster.GoBase == null))
                        {
                            return;
                        }
                        MonsterVo meVoByType = monster.GetMeVoByType<MonsterVo>();
                        if (meVoByType == null)
                        {
                            return;
                        }
                        if (MeVo.instance.mapId != 0x9c41)
                        {
                            if (meVoByType.CurHp > damage.dmg)
                            {
                                if (!isDodge)
                                {
                                    ActionVo vo10 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, monster, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                    if (vo10 == null)
                                    {
                                        break;
                                    }
                                    vo10 = DamageCheck.FixHurtType(vo10, isKnockBack, isKnockDown);
                                    monster.Controller.BeAttackedController.BeAttacked(vo10);
                                }
                                meVoByType.ReduceHp(damage.dmg);
                            }
                            else
                            {
                                meVoByType.ReduceHpToPercent((uint) 0);
                                if (!meVoByType.IsEmptyHp)
                                {
                                    ActionVo vo11 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, monster, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                    if (vo11 == null)
                                    {
                                        break;
                                    }
                                    vo11 = DamageCheck.FixHurtType(vo11, isKnockBack, isKnockDown);
                                    monster.Controller.BeAttackedController.BeAttacked(vo11);
                                    return;
                                }
                                if (monster.Controller.buffController != null)
                                {
                                    monster.Controller.buffController.Dead();
                                }
                                float num4 = (sysSkillBaseVo.hurt_fly_velocity_max * (((float) damage.dmg) / ((float) meVoByType.Hp))) * 0.001f;
                                monster.Controller.MoveController.DeathFlySpeed = num4;
                                vo13 = new ActionVo {
                                    ActionType = "deathfly"
                                };
                                ActionVo vo12 = vo13;
                                monster.Controller.AttackController.AddAttackList(vo12, true, false);
                            }
                        }
                        if (monster.Controller.buffController != null)
                        {
                            monster.Controller.buffController.AddBuff(buffList, null);
                        }
                        DamageCheck.ShowBuffEffects(actionDisplayByDisplayTypeAndId, monster, buffList);
                        Vector3 vector4 = Tools.find(monster.Controller.gameObject, "pivot").transform.position;
                        HudView.Instance.CreateDamageHudText(vector4, isDodge, isCrit, isParry, damage.dmgForShow, false, false, false, false, null, actionDisplayByDisplayTypeAndId);
                        DamageCheck.UpdateHealthBarRelated(monster, damage.dmg);
                        break;
                    }
                }
            }
        }

        public static ActionDisplay GetActionDisplayByDisplayTypeAndId(int displayType, ulong displayId)
        {
            <GetActionDisplayByDisplayTypeAndId>c__AnonStoreyEA yea = new <GetActionDisplayByDisplayTypeAndId>c__AnonStoreyEA {
                displayId = displayId
            };
            switch (displayType)
            {
                case 100:
                {
                    PlayerDisplay local1 = AppMap.Instance.SelfplayerList.FirstOrDefault<PlayerDisplay>(new Func<PlayerDisplay, bool>(yea.<>m__B6));
                    if (local1 == null)
                    {
                    }
                    return AppMap.Instance.playerList.FirstOrDefault<PlayerDisplay>(new Func<PlayerDisplay, bool>(yea.<>m__B7));
                }
                case 400:
                    return AppMap.Instance.GetMonster(yea.displayId + string.Empty);
            }
            return null;
        }

        public void GetSkillDamageSynInfo13_12(INetData data)
        {
            if (AppMap.Instance.mapParserII.NeedSyn && (AppMap.Instance.me != null))
            {
                SkillDamageSyncMsg_13_12 skillDamageSynInfo = new SkillDamageSyncMsg_13_12();
                skillDamageSynInfo.read(data.GetMemoryStream());
                DoSyncDamage(skillDamageSynInfo);
            }
        }

        private void GetSkillInfo_13_4(INetData data)
        {
            SkillInfoMsg_13_4 g__ = new SkillInfoMsg_13_4();
            g__.read(data.GetMemoryStream());
            Singleton<SkillMode>.Instance.setSkillInfo(g__.skillids, g__.restPoint);
        }

        public void GetSkillPointInfo_13_3(INetData data)
        {
            SkillPointMsg_13_3 g__ = new SkillPointMsg_13_3();
            g__.read(data.GetMemoryStream());
            Singleton<SkillMode>.Instance.setSkillPoint(g__.restPoint);
        }

        private void GetSkillResetInfo_13_7(INetData data)
        {
            SkillResetMsg_13_7 g__ = new SkillResetMsg_13_7();
            g__.read(data.GetMemoryStream());
            Log.info(this, "GetSkillResetInfo_13_7:" + g__.code);
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void GetSkillStudyInfo_13_5(INetData data)
        {
            SkillStudyMsg_13_5 g__ = new SkillStudyMsg_13_5();
            g__.read(data.GetMemoryStream());
            Log.info(this, "GetSkillStudyInfo_13_5:" + g__.code);
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void GetSkillSynInfo13_11(INetData data)
        {
            SkillUseSyncMsg_13_11 g__ = new SkillUseSyncMsg_13_11();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                if ((g__.type == 2) && (g__.id != AppMap.Instance.me.GetVo().Id))
                {
                    PlayerDisplay player = AppMap.Instance.GetPlayer(g__.id + string.Empty);
                    if ((player != null) && (player.GoBase != null))
                    {
                        ActionDisplay finalTarget = null;
                        player.ChangeDire((g__.eularY * 0.001f) * 0.001f);
                        player.Controller.SkillController.UseSkillById(g__.skillId, out finalTarget, false, true, false, false, null, null, null, -1, null);
                    }
                }
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        public void LearnFirstSkill()
        {
            SysSkillBaseVo vo = SkillViewLogic.GetDefaultSkill(MeVo.instance.job, true)[0];
            this.LearnSkill((uint) vo.id);
        }

        public void LearnSkill(uint skillId)
        {
            MemoryStream msdata = new MemoryStream();
            SysSkillBaseVo dataById = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>(skillId);
            if (dataById.next != 0)
            {
                Module_13.write_13_5(msdata, skillId, (uint) dataById.next);
            }
            else
            {
                return;
            }
            AppNet.gameNet.send(msdata, 13, 5);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("3331", new NetMsgCallback(this.GetSkillPointInfo_13_3));
            AppNet.main.addCMD("3332", new NetMsgCallback(this.GetSkillInfo_13_4));
            AppNet.main.addCMD("3333", new NetMsgCallback(this.GetSkillStudyInfo_13_5));
            AppNet.main.addCMD("3335", new NetMsgCallback(this.GetSkillResetInfo_13_7));
            AppNet.main.addCMD("3339", new NetMsgCallback(this.GetSkillSynInfo13_11));
            AppNet.main.addCMD("3340", new NetMsgCallback(this.GetSkillDamageSynInfo13_12));
            this.SendRequstForSKillsInfo();
            SkillViewLogic.InitConfig();
        }

        public void ResetSkillPoint()
        {
        }

        public void SendRequstForSKillsInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_13.write_13_4(msdata);
            AppNet.gameNet.send(msdata, 13, 4);
        }

        public void SetFirstSkillPos()
        {
            uint[] skillsPos = Singleton<SkillMode>.Instance.SkillsPos;
            uint num = 0;
            for (int i = 0; i < skillsPos.Length; i++)
            {
                num += skillsPos[i];
            }
            if ((num == 0) && (Singleton<SkillMode>.Instance.GetLearnedSkillIds().Count == 1))
            {
                skillsPos[0] = 1;
                this.SetSkillPosValue(skillsPos);
            }
        }

        public void SetSkillPosValue(uint[] skillpos)
        {
        }

        [CompilerGenerated]
        private sealed class <GetActionDisplayByDisplayTypeAndId>c__AnonStoreyEA
        {
            internal ulong displayId;

            internal bool <>m__B6(PlayerDisplay x)
            {
                return (x.GetVo().Id == this.displayId);
            }

            internal bool <>m__B7(PlayerDisplay x)
            {
                return (x.GetVo().Id == this.displayId);
            }
        }
    }
}

