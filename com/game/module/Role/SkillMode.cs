﻿namespace com.game.module.Role
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.vo;
    using com.u3d.bases.debug;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SkillMode : BaseMode<SkillMode>
    {
        private List<SysSkillBaseVo> bdSkills;
        public const int FirstSkill = 4;
        private uint leftPoint;
        public ushort skill_seq;
        public const int SkillList = 1;
        public const int SkillPoint = 2;
        public const int SkillPos = 3;
        private List<uint> skillsId = new List<uint>();
        public uint[] SkillsPos = new uint[5];
        private List<SysSkillBaseVo> zdSkills;

        public List<SysSkillBaseVo> CurrentSkills(bool active)
        {
            if (active)
            {
                if (this.zdSkills == null)
                {
                    this.zdSkills = this.GetPlayerCurrentSkills(MeVo.instance.job, true);
                }
                return this.zdSkills;
            }
            if (this.bdSkills == null)
            {
                this.bdSkills = this.GetPlayerCurrentSkills(MeVo.instance.job, false);
            }
            return this.bdSkills;
        }

        private List<SysSkillBaseVo> GetDefaultSkills(int carser)
        {
            List<SysSkillBaseVo> list = new List<SysSkillBaseVo>();
            foreach (SysSkillBaseVo vo in BaseDataMgr.instance.GetDicByType<SysSkillBaseVo>().Values)
            {
                if (((vo.position > 0) && (vo.position <= 8)) && (list[vo.position - 1] == null))
                {
                    if ((vo.active && (vo.pre == 0)) && ((vo.skill_lvl == 0) && (vo.job == carser)))
                    {
                        list[vo.position - 1] = vo;
                    }
                    else if ((!vo.active && (vo.skill_lvl == 0)) && (vo.job == carser))
                    {
                        if (vo.pre == 0)
                        {
                            list[vo.position - 1] = vo;
                        }
                        else if (BaseDataMgr.instance.GetDataById<SysSkillBaseVo>((uint) vo.pre).active)
                        {
                            list[vo.position - 1] = vo;
                        }
                    }
                }
            }
            return list;
        }

        public List<uint> GetLearnedSkillIds()
        {
            return this.skillsId;
        }

        public ushort GetNewSkillSeq()
        {
            if (this.skill_seq >= 0xffff)
            {
                this.skill_seq = 0;
            }
            else
            {
                this.skill_seq = (ushort) (this.skill_seq + 1);
            }
            return this.skill_seq;
        }

        private List<SysSkillBaseVo> GetPlayerCurrentSkills(int job, bool active)
        {
            List<SysSkillBaseVo> list;
            if (this.skillsId.Count < 0x18)
            {
                list = SkillViewLogic.GetDefaultSkill(job, active).ToList<SysSkillBaseVo>();
            }
            else
            {
                list = new List<SysSkillBaseVo>();
            }
            foreach (uint num in this.skillsId)
            {
                SysSkillBaseVo dataById = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>(num);
                if (dataById.active == active)
                {
                    list[dataById.position - 1] = dataById;
                }
            }
            return list;
        }

        public ushort GetSkillSeq()
        {
            return this.skill_seq;
        }

        public uint[] GetUsedSkill()
        {
            uint[] numArray = new uint[5];
            for (int i = 0; i < this.SkillsPos.Length; i++)
            {
                if (this.SkillsPos[i] != 0)
                {
                    numArray[i] = (uint) this.zdSkills[((int) this.SkillsPos[i]) - 1].id;
                }
            }
            return numArray;
        }

        public bool IfShowTips(bool active)
        {
            foreach (SysSkillBaseVo vo in this.CurrentSkills(active))
            {
                if (((vo != null) && (vo.next != 0)) && SkillViewLogic.IsConditionEnough(BaseDataMgr.instance.GetDataById<SysSkillBaseVo>((uint) vo.next)))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsSkillLearned(uint skillId)
        {
            if (skillId == 0)
            {
                return true;
            }
            SysSkillBaseVo dataById = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>(skillId);
            if (dataById == null)
            {
                Log.error(this, "前置技能值为空" + skillId);
            }
            if (dataById.active)
            {
                foreach (SysSkillBaseVo vo2 in this.zdSkills)
                {
                    if ((vo2 != null) && (vo2.id == skillId))
                    {
                        return true;
                    }
                }
            }
            else
            {
                foreach (SysSkillBaseVo vo3 in this.bdSkills)
                {
                    if ((vo3 != null) && (vo3.id == skillId))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public uint LeftPoint()
        {
            return this.leftPoint;
        }

        public void SendHeroSkillDamageList(uint skillId, List<PDamage> damage, ulong actorId, int actorType)
        {
            MemoryStream msdata = new MemoryStream();
            ushort newSkillSeq = this.GetNewSkillSeq();
            Module_13.write_13_12(msdata, newSkillSeq, actorId, DamageCheck.GetSynType(actorType), skillId, damage);
            AppNet.gameNet.send(msdata, 13, 12);
        }

        public void SendHeroUseSkill(ulong id, byte type, uint skillid, Vector3 eular)
        {
            MemoryStream msdata = new MemoryStream();
            int eularAngelsForProtocol = DamageCheck.GetEularAngelsForProtocol(eular.y);
            Module_13.write_13_11(msdata, id, type, skillid, eularAngelsForProtocol);
            AppNet.gameNet.send(msdata, 13, 11);
        }

        public void setSkillInfo(List<uint> skillsId, uint leftPoint)
        {
            this.leftPoint = leftPoint;
            this.skillsId = skillsId;
            this.zdSkills = this.GetPlayerCurrentSkills(MeVo.instance.job, true);
            this.bdSkills = this.GetPlayerCurrentSkills(MeVo.instance.job, false);
            this.dataInit = true;
            base.DataUpdate(1);
        }

        public void setSkillPoint(uint piont)
        {
            this.leftPoint = piont;
            base.DataUpdate(2);
        }

        public void SetSkillPos(uint[] skillPos)
        {
            this.SkillsPos = skillPos;
            base.DataUpdate(3);
        }

        public void SetSkillSeq(ushort seq)
        {
            this.skill_seq = seq;
        }

        public bool dataInit { get; private set; }

        public override bool ShowTips
        {
            get
            {
                return (this.IfShowTips(true) || this.IfShowTips(false));
            }
        }
    }
}

