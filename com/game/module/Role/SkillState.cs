﻿namespace com.game.module.Role
{
    using System;

    public enum SkillState
    {
        Learn,
        Upgrade,
        Change,
        Max
    }
}

