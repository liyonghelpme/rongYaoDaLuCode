﻿namespace com.game.module.Role
{
    using System;

    public enum SkillSynType
    {
        Monster = 1,
        Pet = 3,
        Player = 2,
        Unavailable = 4
    }
}

