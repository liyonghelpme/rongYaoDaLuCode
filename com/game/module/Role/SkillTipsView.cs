﻿namespace com.game.module.Role
{
    using com.game.consts;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.u3d.bases.debug;
    using System;
    using UnityEngine;

    public class SkillTipsView : BaseView<SkillTipsView>
    {
        private GameObject condition;
        private GameObject info;
        private GameObject skillButton;
        private UISprite skillIcn;
        private SkillState skillState = SkillState.Upgrade;
        private SysSkillBaseVo SkillVo;
        private GameObject title;

        private void closeClick(GameObject obj)
        {
            this.gameObject.SetActive(false);
            this.SkillVo = null;
            this.gameObject = null;
        }

        protected override void Init()
        {
            this.skillIcn = base.FindInChild<UISprite>("tips/icon/icn");
            this.title = base.FindChild("tips/title");
            this.condition = base.FindChild("tips/condition");
            this.info = base.FindChild("tips/info");
            this.skillButton = base.FindChild("tips/btn_learn");
            base.FindInChild<Button>("tips/btn_close").onClick = new UIWidgetContainer.VoidDelegate(this.closeClick);
            this.initLabels();
            this.setSkillSate(SkillState.Upgrade);
        }

        public void initLabels()
        {
            NGUITools.FindInChild<UILabel>(this.title, "level").text = LanguageManager.GetWord("SkillTipsView.Level");
            NGUITools.FindInChild<UILabel>(this.info, "title").text = LanguageManager.GetWord("SkillTipsView.Info");
            NGUITools.FindInChild<UILabel>(this.info, "type/title").text = LanguageManager.GetWord("SkillTipsView.SkillType");
            NGUITools.FindInChild<UILabel>(this.info, "cost/title").text = LanguageManager.GetWord("SkillTipsView.Cost");
            NGUITools.FindInChild<UILabel>(this.info, "distance/title").text = LanguageManager.GetWord("SkillTipsView.Distance");
            NGUITools.FindInChild<UILabel>(this.info, "time/title").text = LanguageManager.GetWord("SkillTipsView.Time");
            NGUITools.FindInChild<UILabel>(this.info, "describe/title").text = LanguageManager.GetWord("SkillTipsView.Describe");
            NGUITools.FindInChild<UILabel>(this.condition, "money/title").text = LanguageManager.GetWord("SkillTipsView.Money");
            NGUITools.FindInChild<UILabel>(this.condition, "level/title").text = LanguageManager.GetWord("SkillTipsView.RoleLevel");
            NGUITools.FindInChild<UILabel>(this.condition, "point/title").text = LanguageManager.GetWord("SkillTipsView.Point");
            NGUITools.FindInChild<UILabel>(this.condition, "preskill/title").text = LanguageManager.GetWord("SkillTipsView.PreSkill");
            NGUITools.FindInChild<UILabel>(this.condition, "nextlevel").text = LanguageManager.GetWord("SkillTipsView.Level");
        }

        public void OpenView(SysSkillBaseVo skillVo, GameObject obj)
        {
            if (base.gameObject == null)
            {
                base.gameObject = obj;
                this.Init();
            }
            base.gameObject.SetActive(true);
            this.SetSkillDetailInfo(skillVo);
        }

        private void setBtnEnable(bool enable)
        {
            if (enable)
            {
                NGUITools.FindInChild<UISprite>(this.skillButton, "background").color = ColorConst.Normal;
                this.skillButton.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.skillUpgradeClick);
            }
            else
            {
                NGUITools.FindInChild<UISprite>(this.skillButton, "background").color = ColorConst.GRAY;
                this.skillButton.GetComponent<Button>().onClick = null;
            }
        }

        private void setNextSkillTitle(string name, int level, string des)
        {
            NGUITools.FindInChild<UILabel>(this.condition, "nextname").text = name;
            NGUITools.FindInChild<UILabel>(this.condition, "nextlevelvalue").text = level.ToString();
            NGUITools.FindInChild<UILabel>(this.condition, "describe/value").text = des;
        }

        private void setSkillCondition(int level, int point, int money, string preSkill, bool levelEnough, bool pointEnough, bool moneyEnough, bool preSkillEnough)
        {
            NGUITools.FindInChild<UILabel>(this.condition, "level/value").text = level.ToString();
            NGUITools.FindInChild<UILabel>(this.condition, "point/value").text = point.ToString();
            NGUITools.FindInChild<UILabel>(this.condition, "money/value").text = money.ToString();
            if ((preSkill == null) || preSkill.Equals(string.Empty))
            {
                base.FindChild("tips/condition/preskill").SetActive(false);
            }
            else
            {
                base.FindChild("tips/condition/preskill").SetActive(true);
                NGUITools.FindInChild<UILabel>(this.condition, "preskill/value").text = preSkill;
                if (preSkillEnough)
                {
                    NGUITools.FindInChild<UILabel>(this.condition, "preskill/state").text = LanguageManager.GetWord("SkillTipsView.Enough");
                    NGUITools.FindInChild<UILabel>(this.condition, "preskill/state").color = ColorConst.GREEN_YES;
                }
                else
                {
                    NGUITools.FindInChild<UILabel>(this.condition, "preskill/state").text = LanguageManager.GetWord("SkillTipsView.NotEnough");
                    NGUITools.FindInChild<UILabel>(this.condition, "preskill/state").color = ColorConst.RED_NO;
                }
            }
            if (levelEnough)
            {
                NGUITools.FindInChild<UILabel>(this.condition, "level/state").text = LanguageManager.GetWord("SkillTipsView.Enough");
                NGUITools.FindInChild<UILabel>(this.condition, "level/state").color = ColorConst.GREEN_YES;
            }
            else
            {
                NGUITools.FindInChild<UILabel>(this.condition, "level/state").text = LanguageManager.GetWord("SkillTipsView.NotEnough");
                NGUITools.FindInChild<UILabel>(this.condition, "level/state").color = ColorConst.RED_NO;
            }
            if (pointEnough)
            {
                NGUITools.FindInChild<UILabel>(this.condition, "point/state").text = LanguageManager.GetWord("SkillTipsView.Enough");
                NGUITools.FindInChild<UILabel>(this.condition, "point/state").color = ColorConst.GREEN_YES;
            }
            else
            {
                NGUITools.FindInChild<UILabel>(this.condition, "point/state").text = LanguageManager.GetWord("SkillTipsView.NotEnough");
                NGUITools.FindInChild<UILabel>(this.condition, "point/state").color = ColorConst.RED_NO;
            }
            if (moneyEnough)
            {
                NGUITools.FindInChild<UILabel>(this.condition, "money/state").text = LanguageManager.GetWord("SkillTipsView.Enough");
                NGUITools.FindInChild<UILabel>(this.condition, "money/state").color = ColorConst.GREEN_YES;
            }
            else
            {
                NGUITools.FindInChild<UILabel>(this.condition, "money/state").text = LanguageManager.GetWord("SkillTipsView.NotEnough");
                NGUITools.FindInChild<UILabel>(this.condition, "money/state").color = ColorConst.RED_NO;
            }
            this.setBtnEnable(((levelEnough && pointEnough) && moneyEnough) && preSkillEnough);
        }

        public void SetSkillDetailInfo(SysSkillBaseVo svo)
        {
            if (this.gameObject != null)
            {
                string word;
                this.SkillVo = svo;
                SysSkillBaseVo skillVo = this.SkillVo;
                SkillState skillState = SkillViewLogic.GetSkillState(skillVo);
                bool enable = skillState != SkillState.Learn;
                bool flag2 = skillState != SkillState.Change;
                bool flag3 = skillState == SkillState.Change;
                bool limit = skillVo.next == 0;
                uint next = (uint) skillVo.next;
                if (next == 0)
                {
                }
                SysSkillBaseVo dataById = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>(next);
                if (!enable && (dataById != null))
                {
                    skillVo = dataById;
                }
                if (skillVo.active)
                {
                    word = LanguageManager.GetWord("SkillTipsView.TypeZD");
                    if (skillVo.target_num > 1)
                    {
                        word = word + LanguageManager.GetWord("SkillTipsView.Group");
                    }
                    else
                    {
                        word = word + LanguageManager.GetWord("SkillTipsView.Single");
                    }
                }
                else
                {
                    word = LanguageManager.GetWord("SkillTipsView.TypeBD");
                }
                string icnName = skillVo.icon.ToString();
                if (!SkillViewLogic.IsLevelEnough(skillVo))
                {
                    icnName = "suo";
                }
                this.setSkillIcn(icnName);
                this.setSkillEnable(enable);
                this.setSkillTitle(skillVo.name, skillVo.skill_lvl.ToString(), limit);
                this.setSkillInfo(word, skillVo.shape_x, 0, ((float) skillVo.cd) / 1000f, skillVo.desc);
                if (next != 0)
                {
                    bool levelEnough = SkillViewLogic.IsLevelEnough(dataById);
                    bool moneyEnough = SkillViewLogic.IsMoneyEnough(dataById);
                    bool pointEnough = SkillViewLogic.IsSkillPointEnough(dataById);
                    string preSkill = null;
                    bool preSkillEnough = true;
                    if (skillVo.pre != 0)
                    {
                        SysSkillBaseVo vo3 = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>((uint) skillVo.pre);
                        if (vo3.skill_lvl != 0)
                        {
                            preSkill = vo3.name;
                            preSkillEnough = SkillViewLogic.IsPreSkillEnough(dataById);
                        }
                    }
                    this.setSkillCondition(dataById.lvl, 0, dataById.beiley, preSkill, levelEnough, pointEnough, moneyEnough, preSkillEnough);
                    this.setNextSkillTitle(dataById.name, dataById.skill_lvl, dataById.desc);
                    this.condition.SetActive(true);
                    this.skillButton.SetActive(true);
                }
                else
                {
                    this.condition.SetActive(false);
                    this.skillButton.SetActive(false);
                }
            }
        }

        private void setSkillEnable(bool enable)
        {
            if (enable)
            {
                this.skillIcn.color = ColorConst.Normal;
            }
            else
            {
                this.setSkillSate(SkillState.Learn);
                this.skillIcn.color = ColorConst.GRAY;
            }
        }

        private void setSkillIcn(string icnName)
        {
            this.skillIcn.spriteName = icnName;
        }

        private void setSkillInfo(string skilltype, int distance, int cost, float second, string describe)
        {
            NGUITools.FindInChild<UILabel>(this.info, "type/value").text = skilltype;
            NGUITools.FindInChild<UILabel>(this.info, "distance/value").text = distance.ToString();
            NGUITools.FindInChild<UILabel>(this.info, "time/value").text = second.ToString() + LanguageManager.GetWord("SkillTipsView.Seconds");
            NGUITools.FindInChild<UILabel>(this.info, "cost/value").text = cost.ToString();
            NGUITools.FindInChild<UILabel>(this.info, "describe/value").text = describe;
        }

        private void setSkillSate(SkillState state)
        {
            string word;
            this.skillState = state;
            if (state == SkillState.Learn)
            {
                word = LanguageManager.GetWord("SkillTipsView.Learn");
            }
            else if (state == SkillState.Upgrade)
            {
                word = LanguageManager.GetWord("SkillTipsView.Upgrade");
            }
            else
            {
                word = LanguageManager.GetWord("SkillTipsView.Change");
            }
            NGUITools.FindInChild<UILabel>(this.condition, "title").text = word + LanguageManager.GetWord("SkillTipsView.NextSkill");
            NGUITools.FindInChild<UILabel>(this.skillButton, "label").text = word;
        }

        private void setSkillTitle(string skillName, string level, bool limit)
        {
            NGUITools.FindInChild<UILabel>(this.title, "name").text = skillName;
            NGUITools.FindInChild<UILabel>(this.title, "levelvalue").text = level.ToString();
            if (limit)
            {
                NGUITools.FindInChild<UILabel>(this.title, "state").color = ColorConst.RED_NO;
                NGUITools.FindInChild<UILabel>(this.title, "state").text = LanguageManager.GetWord("SkillTipsView.Limit");
                this.setSkillSate(SkillState.Change);
                Log.info(this, string.Empty + NGUITools.FindInChild<UILabel>(this.title, "state").color.r);
            }
            else
            {
                NGUITools.FindInChild<UILabel>(this.title, "state").color = ColorConst.GREEN_YES;
                NGUITools.FindInChild<UILabel>(this.title, "state").text = LanguageManager.GetWord("SkillTipsView.UnLimit");
            }
        }

        private void skillUpgradeClick(GameObject obj)
        {
            Singleton<SkillControl>.Instance.LearnSkill((uint) this.SkillVo.id);
        }

        public override string url
        {
            get
            {
                return "UI/Skill/SkillTips.assetbundle";
            }
        }
    }
}

