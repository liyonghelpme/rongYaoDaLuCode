﻿namespace com.game.module.Role
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections.Generic;

    public class SkillViewLogic
    {
        private static SysSkillBaseVo[][] DefaultSkill = new SysSkillBaseVo[6][];

        public static SysSkillBaseVo[] GetDefaultSkill(int job, bool active)
        {
            int num = 0;
            if (active)
            {
                num = 1;
            }
            return DefaultSkill[(((job - 1) * 2) + 1) - num];
        }

        public static SkillState GetSkillState(SysSkillBaseVo skillVo)
        {
            if (IsLastLevel(skillVo))
            {
                return SkillState.Max;
            }
            if (skillVo.skill_lvl == 0)
            {
                return SkillState.Learn;
            }
            return SkillState.Upgrade;
        }

        public static void InitConfig()
        {
            Dictionary<uint, object>.ValueCollection values = BaseDataMgr.instance.GetDicByType<SysSkillBaseVo>().Values;
            SysSkillBaseVo[] voArray = new SysSkillBaseVo[12];
            SysSkillBaseVo[] voArray2 = new SysSkillBaseVo[12];
            SysSkillBaseVo[] voArray3 = new SysSkillBaseVo[12];
            SysSkillBaseVo[] voArray4 = new SysSkillBaseVo[12];
            SysSkillBaseVo[] voArray5 = new SysSkillBaseVo[12];
            SysSkillBaseVo[] voArray6 = new SysSkillBaseVo[12];
            DefaultSkill[0] = voArray;
            DefaultSkill[1] = voArray2;
            DefaultSkill[2] = voArray3;
            DefaultSkill[3] = voArray4;
            DefaultSkill[4] = voArray5;
            DefaultSkill[5] = voArray6;
            foreach (SysSkillBaseVo vo in values)
            {
                if (((vo.job != 0) && (vo.skill_lvl == 0)) && (vo.next != 0))
                {
                    int num = 0;
                    if (vo.active)
                    {
                        num = 1;
                    }
                    DefaultSkill[(((vo.job - 1) * 2) + 1) - num][vo.position - 1] = vo;
                }
            }
        }

        public static bool IsConditionEnough(SysSkillBaseVo skillVo)
        {
            return (((IsLevelEnough(skillVo) && IsMoneyEnough(skillVo)) && IsPreSkillEnough(skillVo)) && IsSkillPointEnough(skillVo));
        }

        public static bool IsLastLevel(SysSkillBaseVo skillVo)
        {
            return (skillVo.next == 0);
        }

        public static bool IsLevelEnough(SysSkillBaseVo skillVo)
        {
            return (skillVo.lvl <= MeVo.instance.Level);
        }

        public static bool IsMoneyEnough(SysSkillBaseVo skillVo)
        {
            return (skillVo.beiley <= MeVo.instance.diam);
        }

        public static bool IsPreSkillEnough(SysSkillBaseVo skillVo)
        {
            return Singleton<SkillMode>.Instance.IsSkillLearned((uint) skillVo.pre);
        }

        public static bool IsSkillPointEnough(SysSkillBaseVo skillVo)
        {
            return (Singleton<SkillMode>.Instance.LeftPoint() >= 0);
        }
    }
}

