﻿namespace com.game.module.RoleII
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.shop;
    using com.game.Public.Message;
    using com.game.utils;
    using com.game.vo;
    using System;
    using UnityEngine;

    public class BuyVigourView : BaseView<BuyVigourView>
    {
        private Button _btnCancel;
        private Button _btnOk;
        private int _cost;
        private UILabel _costLabel;
        private UILabel _countLabel;
        private UISprite _currencySprite;
        private UILabel _numLabel;
        private SysPriceVo _priceVo;
        public const int NUM_VIGOUR_TO_BUY = 120;

        protected override void HandleAfterOpenView()
        {
            Singleton<RoleMode>.Instance.dataUpdated += new DataUpateHandler(this.OnVigourEvent);
            this.UpdateView();
        }

        protected override void HandleBeforeCloseView()
        {
            Singleton<RoleMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnVigourEvent);
            this._priceVo = null;
            this._cost = 0;
        }

        protected override void Init()
        {
            this._currencySprite = base.FindInChild<UISprite>("currencyIcon");
            this._costLabel = base.FindInChild<UILabel>("costLabel");
            this._numLabel = base.FindInChild<UILabel>("numLabel");
            this._countLabel = base.FindInChild<UILabel>("countLabel");
            this._btnOk = base.FindInChild<Button>("okBtn");
            this._btnCancel = base.FindInChild<Button>("cancelBtn");
            this._btnOk.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            this._btnCancel.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
        }

        private void OnClickBtn(GameObject go)
        {
            if (this._btnCancel.gameObject == go)
            {
                this.CloseView();
            }
            else
            {
                int numBuyVigourMax = VarMgr.NumBuyVigourMax;
                if (MeVo.instance.numBuyVigour >= numBuyVigourMax)
                {
                    MessageManager.Show(LanguageManager.GetWord("Game.BuyVigourCountOut"));
                }
                else if (ShopMode.Currency.GetOwn(this._priceVo.money) < this._cost)
                {
                    Singleton<AlertPanel>.Instance.Show(LanguageManager.GetWord("Game.SureToFill"), null, null);
                }
                else
                {
                    Singleton<RoleMode>.Instance.BuyVigour(0);
                    this.CloseView();
                }
            }
        }

        private void OnVigourEvent(object data, int code)
        {
            this._priceVo = BaseDataMgr.instance.GetSysPriceVo(0x3ea, (uint) (MeVo.instance.numBuyVigour + 1));
            this.UpdateView();
        }

        public void Show()
        {
            int numBuyVigourMax = VarMgr.NumBuyVigourMax;
            if (MeVo.instance.numBuyVigour >= numBuyVigourMax)
            {
                MessageManager.Show(LanguageManager.GetWord("Game.BuyVigourCountOut"));
            }
            else
            {
                this._priceVo = BaseDataMgr.instance.GetSysPriceVo(0x3ea, (uint) (MeVo.instance.numBuyVigour + 1));
                this.OpenView();
            }
        }

        private void UpdateView()
        {
            ShopMode.Currency currency = ShopMode.Currency.GetCurrency(this._priceVo.money);
            this._currencySprite.spriteName = currency.iconName;
            switch (this._priceVo.money)
            {
                case 1:
                    this._cost = this._priceVo.diam;
                    break;

                case 3:
                    this._cost = this._priceVo.gold;
                    break;
            }
            this._costLabel.text = this._cost.ToString();
            this._numLabel.text = 120.ToString();
            string[] param = new string[] { MeVo.instance.numBuyVigour + string.Empty, VarMgr.NumBuyVigourMax + string.Empty };
            this._countLabel.text = LanguageManager.GetWord("Dungeon.VigourBuyCount", param);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/RoleII/BuyVigourPanel.assetbundle";
            }
        }
    }
}

