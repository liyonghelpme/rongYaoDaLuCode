﻿namespace com.game.module.RoleII
{
    using com.game.basic;
    using com.game.module.core;
    using com.game.module.Role;
    using System;
    using UnityEngine;

    public class RoleIIView : BaseView<RoleIIView>
    {
        private UILabel absoluteDefLabel;
        private UILabel absoluteHitLabel;
        private AttributeInfo att;
        private Button attriBtn;
        private Button closeBtn;
        private UILabel crticalAttackLabel;
        private Button graphBtn;
        private UILabel hpLabal;
        private Button leftBtn;
        private UILabel magicAttackLabel;
        private UILabel magicDefLabel;
        private UILabel mingzhongLabel;
        private UILabel physicAttackLabel;
        private UILabel physicDefLabel;
        private Button promoteBtn;
        private UILabel qucikAvoidLabel;
        private Button rightBtn;
        public RoleDisplay RoleModel;
        private Button shenxingBtn;
        private Button skillBtn;
        private UILabel strongLabel;

        private void AttriBtnClickHandler(GameObject go)
        {
        }

        public override void CancelUpdateHandler()
        {
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        private void GraphBtnClickHandler(GameObject go)
        {
        }

        protected override void HandleAfterOpenView()
        {
        }

        protected override void Init()
        {
            this.hpLabal = base.FindInChild<UILabel>("attLabList/hpLabel");
            this.physicAttackLabel = base.FindInChild<UILabel>("attLabList/physicAttackLabel");
            this.magicAttackLabel = base.FindInChild<UILabel>("attLabList/magicAttackLabel");
            this.physicDefLabel = base.FindInChild<UILabel>("attLabList/physicDefLabel");
            this.magicDefLabel = base.FindInChild<UILabel>("attLabList/magicDefLabel");
            this.mingzhongLabel = base.FindInChild<UILabel>("attLabList/mingzhongLabel");
            this.qucikAvoidLabel = base.FindInChild<UILabel>("attLabList/qucikAvoidLabel");
            this.crticalAttackLabel = base.FindInChild<UILabel>("attLabList/crticalAttackLabel");
            this.strongLabel = base.FindInChild<UILabel>("attLabList/strongLabel");
            this.absoluteHitLabel = base.FindInChild<UILabel>("attLabList/absoluteHitLabel");
            this.absoluteDefLabel = base.FindInChild<UILabel>("attLabList/absoluteDefLabel");
            this.promoteBtn = base.FindInChild<Button>("promoteBtn");
            this.attriBtn = base.FindInChild<Button>("attriBtn");
            this.shenxingBtn = base.FindInChild<Button>("shenxingBtn");
            this.graphBtn = base.FindInChild<Button>("graphBtn");
            this.skillBtn = base.FindInChild<Button>("skillBtn");
            this.leftBtn = base.FindInChild<Button>("leftBtn");
            this.rightBtn = base.FindInChild<Button>("rightBtn");
            this.closeBtn = base.FindInChild<Button>("closeBtn");
            this.hpLabal.text = this.hpLabal.text + this.att.hpFull;
            this.physicAttackLabel.text = this.physicAttackLabel.text + this.att.attP;
            this.magicAttackLabel.text = this.magicAttackLabel.text + this.att.attM;
            this.physicDefLabel.text = this.physicDefLabel.text + this.att.defP;
            this.magicDefLabel.text = this.magicDefLabel.text + this.att.defM;
            this.mingzhongLabel.text = this.mingzhongLabel.text + this.att.hit;
            this.qucikAvoidLabel.text = this.qucikAvoidLabel.text + this.att.dodge;
            this.crticalAttackLabel.text = this.crticalAttackLabel.text + this.att.crit;
            this.strongLabel.text = this.strongLabel.text + this.att.flex;
            this.absoluteHitLabel.text = this.absoluteHitLabel.text + this.att.abDef;
            this.absoluteDefLabel.text = this.absoluteDefLabel.text + this.att.abHurt;
            this.InitClick();
        }

        private void InitClick()
        {
            this.promoteBtn.onClick = new UIWidgetContainer.VoidDelegate(this.PromoteBtnClickHandler);
            this.attriBtn.onClick = new UIWidgetContainer.VoidDelegate(this.AttriBtnClickHandler);
            this.shenxingBtn.onClick = new UIWidgetContainer.VoidDelegate(this.ShenXingBtnClickHandler);
            this.graphBtn.onClick = new UIWidgetContainer.VoidDelegate(this.GraphBtnClickHandler);
            this.skillBtn.onClick = new UIWidgetContainer.VoidDelegate(this.SkillBtnClickHandler);
            this.leftBtn.onClick = new UIWidgetContainer.VoidDelegate(this.LeftBtnClickHandler);
            this.rightBtn.onClick = new UIWidgetContainer.VoidDelegate(this.RightBtnClickHandler);
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
        }

        private void LeftBtnClickHandler(GameObject go)
        {
        }

        private void LoadRoleModelCallBack(GameObject go)
        {
            this.SetModePosition();
        }

        private void PromoteBtnClickHandler(GameObject go)
        {
        }

        public override void RegisterUpdateHandler()
        {
        }

        private void RightBtnClickHandler(GameObject go)
        {
        }

        public void SetItemInfo(AttributeInfo att)
        {
            this.att = att;
        }

        private void SetModePosition()
        {
            if ((this.RoleModel != null) && (this.RoleModel.GoBase != null))
            {
                NGUITools.SetActive(this.RoleModel.GoBase, true);
                this.RoleModel.GoBase.transform.localPosition = new Vector3(-248.8286f, -162f, 0f);
            }
        }

        private void ShenXingBtnClickHandler(GameObject go)
        {
        }

        private void SkillBtnClickHandler(GameObject go)
        {
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/RoleII/RollII.assetbundle";
            }
        }
    }
}

