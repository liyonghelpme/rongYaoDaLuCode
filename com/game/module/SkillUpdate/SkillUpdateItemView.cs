﻿namespace com.game.module.SkillUpdate
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class SkillUpdateItemView : Singleton<SkillUpdateItemView>
    {
        private UILabel _skillCopperLabel;
        private UISprite _skillIcon;
        private SysSkillBaseVo _skillInfo;
        private UILabel _skillLevelLabel;
        private UILabel _skillNameLabel;
        private GameObject _skillUpdateGO;
        private Button _updateLevelBtn;

        public void Dispose()
        {
            this._skillNameLabel.text = string.Empty;
            this._skillLevelLabel.text = string.Empty;
            this._skillCopperLabel.text = string.Empty;
            this._skillIcon.SetActive(false);
            this._updateLevelBtn.SetActive(false);
        }

        public void Init(GameObject obj)
        {
            this._skillUpdateGO = obj;
            this.initView();
            this.initClick();
            this.SetSkillID(0x2f4d61);
        }

        private void initClick()
        {
            this._updateLevelBtn.onClick = new UIWidgetContainer.VoidDelegate(this.updateOnClick);
        }

        private void initView()
        {
            this._skillIcon = NGUITools.FindInChild<UISprite>(this._skillUpdateGO, "skillIcon");
            this._skillLevelLabel = NGUITools.FindInChild<UILabel>(this._skillUpdateGO, "skillLevel");
            this._skillNameLabel = NGUITools.FindInChild<UILabel>(this._skillUpdateGO, "skillName");
            this._skillCopperLabel = NGUITools.FindInChild<UILabel>(this._skillUpdateGO, "goldLabel");
            this._updateLevelBtn = this._skillUpdateGO.transform.FindChild("addBtn").GetComponent<Button>();
        }

        public void SetSkillID(uint id)
        {
            this._skillInfo = BaseDataMgr.instance.GetSysSkillBaseVo(id);
            this._skillNameLabel.text = this._skillInfo.name;
            this._skillLevelLabel.text = this._skillInfo.skill_lvl.ToString();
            this._skillCopperLabel.text = this._skillInfo.beiley.ToString();
        }

        private void updateOnClick(GameObject go)
        {
            Debug.Log("click");
            this._skillIcon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("common");
            this._skillIcon.spriteName = "epz_3";
            this._skillIcon.MakePixelPerfect();
        }
    }
}

