﻿namespace com.game.module.SkillUpdate
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class SkillUpdateView : BaseView<SkillUpdateView>
    {
        private Button _closeBtn;
        private UILabel _leftSkillPointLabel;
        private UILabel _refreshSkillPointLabel;
        private UIScrollView _scrollView;
        private List<SysSkillBaseVo> _skillInfoList = new List<SysSkillBaseVo>();
        private UIGrid _skillItemGrid;
        private List<SkillUpdateItemView> _skillUpdataItemList = new List<SkillUpdateItemView>();
        private GameObject _skillUpdateItem;
        private const int SKILL_LIST_LENGTH = 4;

        public override void CancelUpdateHandler()
        {
            base.CancelUpdateHandler();
        }

        private void closeOnClick(GameObject go)
        {
            base.CloseView();
        }

        public override void Destroy()
        {
            base.Destroy();
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
        }

        protected override void Init()
        {
            this.initView();
            this.initSkillViews();
            this.initClick();
        }

        private void initClick()
        {
            this._closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.closeOnClick);
        }

        private void initSkillViews()
        {
            this._skillUpdataItemList = new List<SkillUpdateItemView>();
            SkillUpdateItemView item = new SkillUpdateItemView();
            item.Init(this._skillUpdateItem);
            this._skillUpdataItemList.Add(item);
            for (int i = 1; i < 4; i++)
            {
                GameObject obj2 = UnityEngine.Object.Instantiate(this._skillUpdateItem) as GameObject;
                obj2.transform.parent = this._skillUpdateItem.transform.parent;
                obj2.transform.localScale = this._skillUpdateItem.transform.localScale;
                obj2.SetActive(true);
                item = new SkillUpdateItemView();
                item.Init(obj2);
                this._skillUpdataItemList.Add(item);
                this._skillItemGrid.Reposition();
            }
        }

        private void initView()
        {
            this._leftSkillPointLabel = base.FindInChild<UILabel>("Panel/leftSkillPoint");
            this._refreshSkillPointLabel = base.FindInChild<UILabel>("Panel/buySkill/leftSkillNum");
            this._scrollView = base.FindInChild<UIScrollView>("Panel/skillItemList");
            this._skillItemGrid = base.FindInChild<UIGrid>("Panel/skillItemList/grid");
            this._closeBtn = base.FindInChild<Button>("Panel/closeBtn");
            this._skillItemGrid.onReposition = (UIGrid.OnReposition) Delegate.Combine(this._skillItemGrid.onReposition, new UIGrid.OnReposition(this._scrollView.ResetPosition));
            this._skillUpdateItem = NGUITools.FindChild(this.gameObject, "Panel/skillItemList/grid/skillItem");
            this._skillUpdateItem.SetActive(true);
        }

        public override void RegisterUpdateHandler()
        {
            base.RegisterUpdateHandler();
        }

        public override bool IsFullUI
        {
            get
            {
                return false;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/SkillUpdate/SkillUpdateView.assetbundle";
            }
        }
    }
}

