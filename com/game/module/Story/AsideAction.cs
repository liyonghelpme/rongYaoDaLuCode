﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class AsideAction : BaseAction
    {
        private GameObject asideWindow;
        private float duration;
        private UILabel labAsideText;
        private string text;

        public override void AfterPlayed()
        {
            this.asideWindow.SetActive(false);
        }

        public override void ParseNode(XMLNode node)
        {
            this.text = node.GetValue("@text");
            this.duration = Convert.ToSingle(node.GetValue("@duration"));
            if (this.duration <= 0f)
            {
                this.duration = 2f;
            }
        }

        public override void Run()
        {
            this.asideWindow = Singleton<StoryView>.Instance.AsideWindow;
            this.labAsideText = Singleton<StoryView>.Instance.AsideText;
            this.labAsideText.text = this.text;
            this.asideWindow.SetActive(true);
            vp_Timer.In(this.duration, new vp_Timer.Callback(this.Finish), 1, 0f, null);
        }

        public override bool CanInterrupt
        {
            get
            {
                return base.finished;
            }
        }
    }
}

