﻿namespace com.game.module.Story
{
    using System;

    public class BaseAction : IParseNode
    {
        protected bool block;
        protected bool finished;

        public virtual void AfterPlayed()
        {
        }

        public virtual void Clear()
        {
        }

        protected virtual void Finish()
        {
            this.finished = true;
        }

        public virtual void ParseNode(XMLNode node)
        {
        }

        public virtual void Run()
        {
        }

        public virtual void Stop()
        {
        }

        public virtual bool CanInterrupt
        {
            get
            {
                return true;
            }
        }

        public virtual bool CanMannualSwitch
        {
            get
            {
                return false;
            }
        }

        public virtual bool MannualSwitch
        {
            get
            {
                return false;
            }
        }
    }
}

