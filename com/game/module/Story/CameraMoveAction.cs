﻿namespace com.game.module.Story
{
    using com.game;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class CameraMoveAction : BaseAction
    {
        private string followId;
        private bool reset;
        private GameObject role;
        private int speed;
        private float targetX;
        private float targetY;

        public override void Clear()
        {
            this.DeleteCameraMoveComponent();
        }

        private void DeleteCameraMoveComponent()
        {
            StoryCameraMove component = Camera.main.gameObject.GetComponent<StoryCameraMove>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
        }

        public override void ParseNode(XMLNode node)
        {
            this.targetX = Convert.ToSingle(node.GetValue("@targetX"));
            this.targetY = Convert.ToSingle(node.GetValue("@targetY"));
            this.followId = node.GetValue("@followId");
            this.reset = Convert.ToBoolean(node.GetValue("@reset"));
            this.speed = Convert.ToInt32(node.GetValue("@speed"));
            base.block = Convert.ToBoolean(node.GetValue("@block"));
        }

        public override void Run()
        {
            if ("-1" == this.followId)
            {
                this.role = AppMap.Instance.me.Controller.gameObject;
            }
            else
            {
                CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.followId);
                if (createAction != null)
                {
                    if (createAction.IsMonster)
                    {
                        this.role = AppMap.Instance.GetMonster(createAction.MonsterId).Controller.gameObject;
                    }
                    else
                    {
                        this.role = createAction.Role;
                    }
                }
            }
            if (this.reset)
            {
                this.targetX = AppMap.Instance.me.Controller.transform.position.x;
            }
            this.DeleteCameraMoveComponent();
            StoryCameraMove move = Camera.main.gameObject.AddComponent<StoryCameraMove>();
            move.FollowTarget = this.role;
            move.MoveSpeed = this.speed;
            move.TargetPos.x = this.targetX;
        }

        public override bool CanInterrupt
        {
            get
            {
                if (!base.block)
                {
                    StoryCameraMove component = Camera.main.gameObject.GetComponent<StoryCameraMove>();
                    if ((null == component) || component.ExceedBorder)
                    {
                        return true;
                    }
                    if (null == this.role)
                    {
                        return (Camera.main.transform.position.x == this.targetX);
                    }
                    MoveAction moveAction = Singleton<StoryMode>.Instance.GetMoveAction(this.followId);
                    if (moveAction != null)
                    {
                        return (this.role.transform.position.x == moveAction.TargetX);
                    }
                }
                return true;
            }
        }
    }
}

