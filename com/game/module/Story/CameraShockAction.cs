﻿namespace com.game.module.Story
{
    using com.game.manager;
    using System;

    public class CameraShockAction : BaseAction
    {
        private float duration;
        private int shockType;

        public override void ParseNode(XMLNode node)
        {
            this.shockType = Convert.ToInt32(node.GetValue("@shockType"));
            this.duration = Convert.ToSingle(node.GetValue("@duration"));
        }

        public override void Run()
        {
            CameraEffectManager.TweenShakeCamera(0f, this.duration);
            vp_Timer.In(this.duration, new vp_Timer.Callback(this.Finish), 1, 0f, null);
        }

        public override bool CanInterrupt
        {
            get
            {
                return base.finished;
            }
        }
    }
}

