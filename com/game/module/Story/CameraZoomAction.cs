﻿namespace com.game.module.Story
{
    using System;

    public class CameraZoomAction : BaseAction
    {
        private float duration;
        private float value;

        public override void ParseNode(XMLNode node)
        {
            this.value = Convert.ToSingle(node.GetValue("@value"));
            this.duration = Convert.ToSingle(node.GetValue("@duration"));
            base.block = Convert.ToBoolean(node.GetValue("@block"));
            if (this.value > 0f)
            {
                this.value = 1f / this.value;
            }
        }

        public override void Run()
        {
            vp_Timer.In(this.duration, new vp_Timer.Callback(this.Finish), 1, 0f, null);
        }

        public override bool CanInterrupt
        {
            get
            {
                return (base.block || base.finished);
            }
        }
    }
}

