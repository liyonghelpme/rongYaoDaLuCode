﻿namespace com.game.module.Story
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.NPCBust;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using System;
    using UnityEngine;

    public class CreateAction : BaseAction
    {
        private GameObject bustGo;
        private bool bustLoaded;
        private string createId;
        private string dir;
        private bool modelLoaded;
        private static uint monsterCreatedNo = 1;
        private uint monsterId;
        private const string RESOURCE_MONSTER = "monster";
        private const string RESOURCE_NPC = "npc";
        private string resourceId;
        private string resourceType;
        private GameObject roleGo;
        private float targetX;
        private float targetY;

        private void AddNPCFootShadow(GameObject npcObj)
        {
            GameObject obj2 = UnityEngine.Object.Instantiate(CommonModelAssetManager.Instance.GetFootShadowGameObject()) as GameObject;
            float num = 1.5f;
            Vector3 vector = (Vector3) (Vector3.one * num);
            Vector3 localScale = npcObj.transform.localScale;
            BoxCollider2D component = npcObj.GetComponent<BoxCollider2D>();
            if (null != component)
            {
                vector.x = (component.size.x * localScale.x) * num;
                vector.y = (component.size.y * localScale.y) * num;
            }
            if (null != obj2)
            {
                obj2.transform.parent = npcObj.transform;
                obj2.transform.localScale = vector;
                obj2.transform.localPosition = Vector3.zero;
                obj2.transform.localRotation = Quaternion.identity;
            }
        }

        private void BustLoaded(GameObject bustObj)
        {
            try
            {
                if (null != bustObj)
                {
                    this.bustGo = bustObj;
                    this.bustGo.name = this.resourceType + "_" + this.resourceId + "_bust";
                    this.bustGo.SetActive(false);
                    this.bustGo.transform.position = Singleton<StoryView>.Instance.RightNPCSprite.transform.position;
                    if ("10001" == this.resourceId)
                    {
                        this.bustGo.transform.localScale = new Vector3(100f, 100f, 100f);
                    }
                    else
                    {
                        this.bustGo.transform.localScale = new Vector3(-100f, 100f, 100f);
                    }
                }
            }
            finally
            {
                this.bustLoaded = true;
            }
        }

        public override void Clear()
        {
            if (null != this.roleGo)
            {
                UnityEngine.Object.Destroy(this.roleGo);
                this.roleGo = null;
            }
            if (null != this.bustGo)
            {
                this.bustGo.SetActive(false);
            }
            if (this.IsMonster)
            {
                Singleton<MonsterMgr>.Instance.RemoveMonster(this.MonsterId);
            }
        }

        private void CreateMonster()
        {
            MonsterVo vo = new MonsterVo {
                monsterId = Convert.ToUInt32(this.resourceId)
            };
            this.monsterId = (vo.monsterId * 10) + monsterCreatedNo;
            vo.Id = this.monsterId;
            vo.Name = this.resourceType + "_" + this.resourceId;
            vo.X = this.targetX;
            vo.Y = this.targetY;
            vo.AddHpToPercent((uint) 100);
            vo.ModelLoadCallBack = new Action<BaseDisplay>(this.LoadMonsterCallback);
            AppMap.Instance.CreateMonster(vo);
            monsterCreatedNo++;
        }

        private void LoadMonsterCallback(BaseDisplay display)
        {
            MonsterAiController component = display.Controller.gameObject.GetComponent<MonsterAiController>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
            this.modelLoaded = true;
        }

        public override void ParseNode(XMLNode node)
        {
            this.createId = node.GetValue("@id");
            this.resourceId = node.GetValue("@resourceId");
            this.targetX = Convert.ToSingle(node.GetValue("@targetX"));
            this.targetY = Convert.ToSingle(node.GetValue("@targetY"));
            this.resourceType = node.GetValue("@resourceType");
            this.dir = node.GetValue("@dir");
        }

        private void RoleLoaded(GameObject obj)
        {
            try
            {
                if (null != obj)
                {
                    Vector3 one;
                    GameObject npcObj = UnityEngine.Object.Instantiate(obj) as GameObject;
                    this.roleGo = new GameObject(this.resourceType + "_" + this.resourceId);
                    npcObj.transform.parent = this.roleGo.transform;
                    npcObj.AddComponent<StoryRoleEvent>();
                    Vector3 vector = new Vector3(0f, 0f, -30f);
                    npcObj.transform.localPosition = vector;
                    npcObj.transform.localRotation = Quaternion.identity;
                    npcObj.transform.localScale = Vector3.one;
                    this.roleGo.transform.localPosition = new Vector3(this.targetX, this.targetY, this.targetY * 5f);
                    this.roleGo.transform.localRotation = Quaternion.identity;
                    if ("1" == this.dir)
                    {
                        one = new Vector3(-1f, 1f, 1f);
                    }
                    else
                    {
                        one = Vector3.one;
                    }
                    this.roleGo.transform.localScale = one;
                    this.AddNPCFootShadow(npcObj);
                }
            }
            finally
            {
                this.modelLoaded = true;
            }
        }

        public override void Run()
        {
            try
            {
                if (this.IsMonster)
                {
                    this.CreateMonster();
                }
                else
                {
                    AssetManager.Instance.LoadAsset<GameObject>(this.url, new LoadAssetFinish<GameObject>(this.RoleLoaded), null, false, true);
                }
                NPCBustMgr.Instance.GetBust(this.bustUrl, new NPCBustMgr.BustCreatedCallback(this.BustLoaded));
            }
            catch (Exception exception)
            {
                this.modelLoaded = true;
                this.bustLoaded = true;
                Log.error(this, "CreateAction error, exception is: " + exception.StackTrace);
            }
        }

        public GameObject Bust
        {
            get
            {
                return this.bustGo;
            }
        }

        private string bustUrl
        {
            get
            {
                string str = null;
                if ("npc" == this.resourceType)
                {
                    return UrlUtils.npcBustUrl(this.resourceId);
                }
                if ("monster" == this.resourceType)
                {
                    SysMonsterVo vo = BaseDataMgr.instance.getSysMonsterVo(Convert.ToUInt32(this.resourceId));
                    if (vo != null)
                    {
                        str = UrlUtils.monsterBustUrl(vo.res.ToString());
                    }
                }
                return str;
            }
        }

        public override bool CanInterrupt
        {
            get
            {
                return (this.modelLoaded && this.bustLoaded);
            }
        }

        public string CreateId
        {
            get
            {
                return this.createId;
            }
        }

        public bool IsMonster
        {
            get
            {
                return ("monster" == this.resourceType);
            }
        }

        public string MonsterId
        {
            get
            {
                return this.monsterId.ToString();
            }
        }

        public string ResourceId
        {
            get
            {
                return this.resourceId;
            }
        }

        public GameObject Role
        {
            get
            {
                return this.roleGo;
            }
        }

        private string url
        {
            get
            {
                string str = null;
                if ("npc" == this.resourceType)
                {
                    return UrlUtils.npcModeUrl(this.resourceId);
                }
                if ("monster" == this.resourceType)
                {
                    SysMonsterVo vo = BaseDataMgr.instance.getSysMonsterVo(Convert.ToUInt32(this.resourceId));
                    if (vo != null)
                    {
                        str = UrlUtils.monsterModeUrl(vo.res.ToString());
                    }
                }
                return str;
            }
        }
    }
}

