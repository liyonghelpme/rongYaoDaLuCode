﻿namespace com.game.module.Story
{
    using System;
    using UnityEngine;

    public class DelayAction : BaseAction
    {
        private float createTime;
        private float value;

        public override void ParseNode(XMLNode node)
        {
            this.value = Convert.ToSingle(node.GetValue("@value"));
            this.createTime = Time.time;
        }

        public override bool CanInterrupt
        {
            get
            {
                return ((Time.time - this.createTime) >= this.value);
            }
        }
    }
}

