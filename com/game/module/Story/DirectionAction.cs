﻿namespace com.game.module.Story
{
    using com.game;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class DirectionAction : BaseAction
    {
        private string createId;
        private string dir;

        public override void ParseNode(XMLNode node)
        {
            this.createId = node.GetValue("@id");
            this.dir = node.GetValue("@dir");
        }

        public override void Run()
        {
            Vector3 one;
            if ("1" == this.dir)
            {
                one = new Vector3(-1f, 1f, 1f);
            }
            else
            {
                one = Vector3.one;
            }
            if ("-1" == this.createId)
            {
                AppMap.Instance.me.Controller.transform.localScale = one;
            }
            else
            {
                CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.createId);
                if (createAction.IsMonster)
                {
                    AppMap.Instance.GetMonster(createAction.MonsterId).Controller.transform.localScale = one;
                }
                else
                {
                    createAction.Role.transform.localScale = one;
                }
            }
        }
    }
}

