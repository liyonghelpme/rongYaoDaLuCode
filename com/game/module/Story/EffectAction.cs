﻿namespace com.game.module.Story
{
    using com.game.module.effect;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class EffectAction : BaseAction
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map17;
        private string effectId;
        private string key;
        private float startX;
        private float startY;
        private float targetX;
        private float targetY;
        private float time;

        private void CreatedCallback(EffectControlerII controller)
        {
            GameObject gameObject = controller.gameObject;
            float z = gameObject.transform.localPosition.z;
            TweenPosition component = gameObject.GetComponent<TweenPosition>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
            component = gameObject.AddComponent<TweenPosition>();
            component.from = new Vector3(this.startX, this.startY, z);
            component.to = new Vector3(this.targetX, this.targetY, z);
            component.style = UITweener.Style.Once;
            component.method = UITweener.Method.QuintEaseInOut;
            component.duration = this.time;
        }

        protected override void Finish()
        {
            base.Finish();
            EffectMgr.Instance.RemoveEffect(UrlUtils.GetStorySceneEffectUrl(this.effectId));
        }

        private string GetEffectId()
        {
            string key = this.key;
            if (key != null)
            {
                int num;
                if (<>f__switch$map17 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                    dictionary.Add("key1", 0);
                    dictionary.Add("key2", 1);
                    dictionary.Add("key3", 2);
                    <>f__switch$map17 = dictionary;
                }
                if (<>f__switch$map17.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return "100001";

                        case 1:
                            return "100002";

                        case 2:
                            return "100003";
                    }
                }
            }
            return string.Empty;
        }

        public override void ParseNode(XMLNode node)
        {
            this.key = node.GetValue("@key");
            this.startX = Convert.ToSingle(node.GetValue("@startX"));
            this.startY = Convert.ToSingle(node.GetValue("@startY"));
            this.time = Convert.ToSingle(node.GetValue("@time")) / 30f;
            this.targetX = Convert.ToSingle(node.GetValue("@targetX"));
            this.targetY = Convert.ToSingle(node.GetValue("@targetY"));
            this.effectId = this.key;
        }

        public override void Run()
        {
            Vector3 zero = Vector3.zero;
            zero.x = this.startX;
            zero.y = this.startY;
            EffectMgr.Instance.CreateStorySceneEffect(this.effectId, zero, null, new com.game.module.effect.Effect.EffectCreateCallback(this.CreatedCallback));
            vp_Timer.In(this.time, new vp_Timer.Callback(this.Finish), 1, 0f, null);
        }

        public override bool CanInterrupt
        {
            get
            {
                return base.finished;
            }
        }
    }
}

