﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using System;

    public class EmotionAction : TalkAction
    {
        private string key;
        private UISprite sprEmotion;

        public override void AfterPlayed()
        {
            base.AfterPlayed();
            this.sprEmotion.gameObject.SetActive(false);
        }

        public override void Clear()
        {
            this.Finish();
        }

        protected override void Finish()
        {
            base.Finish();
            this.sprEmotion.gameObject.SetActive(false);
        }

        public override void ParseNode(XMLNode node)
        {
            base.ParseNode(node);
            this.key = node.GetValue("@key");
        }

        public override void Run()
        {
            base.Run();
            if ("-1" == base.npcId)
            {
                this.sprEmotion = Singleton<StoryView>.Instance.LeftEmotionSprite;
            }
            else
            {
                this.sprEmotion = Singleton<StoryView>.Instance.RightEmotionSprite;
            }
            this.sprEmotion.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ChatAtlas");
            this.sprEmotion.spriteName = "bq_" + this.key;
            this.sprEmotion.gameObject.SetActive(true);
        }
    }
}

