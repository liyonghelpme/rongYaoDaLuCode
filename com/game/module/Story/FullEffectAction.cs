﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class FullEffectAction : BaseAction
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map18;
        private float duration;
        private float fadeInAlpha;
        private float fadeInTime;
        private float fadeOutAlpha;
        private float fadeOutTime;
        private string key;
        private UISprite sprFullEffect;

        public override void AfterPlayed()
        {
            this.sprFullEffect.SetActive(false);
        }

        private void ChangeFullEffectAlpha(float time, float fromAlpha, float toAlpha)
        {
            TweenAlpha component = this.sprFullEffect.GetComponent<TweenAlpha>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
            component = this.sprFullEffect.gameObject.AddComponent<TweenAlpha>();
            component.from = fromAlpha;
            component.to = toAlpha;
            component.style = UITweener.Style.Once;
            component.method = UITweener.Method.QuintEaseInOut;
            component.duration = time;
        }

        private void FadeInAlpha()
        {
            this.ChangeFullEffectAlpha(this.fadeInTime, 0f, this.fadeInAlpha);
        }

        private void FadeOutAlpha()
        {
            this.ChangeFullEffectAlpha(this.fadeOutTime, this.fadeInAlpha, this.fadeOutAlpha);
            vp_Timer.In(this.fadeOutTime, new vp_Timer.Callback(this.Finish), 1, 0f, null);
        }

        public override void ParseNode(XMLNode node)
        {
            this.key = node.GetValue("@key");
            this.duration = Convert.ToSingle(node.GetValue("@duration"));
            this.fadeInTime = Convert.ToSingle(node.GetValue("@fadeIn"));
            this.fadeOutTime = Convert.ToSingle(node.GetValue("@fadeOut"));
            this.fadeInAlpha = Convert.ToSingle(node.GetValue("@fadeInTo"));
            this.fadeOutAlpha = Convert.ToSingle(node.GetValue("@fadeOutTo"));
        }

        public override void Run()
        {
            this.sprFullEffect = Singleton<StoryView>.Instance.FullEffectSprite;
            this.sprFullEffect.spriteName = this.effectSpriteName;
            this.sprFullEffect.gameObject.SetActive(true);
            this.FadeInAlpha();
            vp_Timer.In(this.fadeInTime + this.duration, new vp_Timer.Callback(this.FadeOutAlpha), 1, 0f, null);
        }

        public override bool CanInterrupt
        {
            get
            {
                return base.finished;
            }
        }

        private string effectSpriteName
        {
            get
            {
                string key = this.key;
                if (key != null)
                {
                    int num;
                    if (<>f__switch$map18 == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(2);
                        dictionary.Add("black", 0);
                        dictionary.Add("white", 1);
                        <>f__switch$map18 = dictionary;
                    }
                    if (<>f__switch$map18.TryGetValue(key, out num))
                    {
                        if (num == 0)
                        {
                            return "black";
                        }
                        if (num == 1)
                        {
                            return "baise";
                        }
                    }
                }
                return string.Empty;
            }
        }
    }
}

