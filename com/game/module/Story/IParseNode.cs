﻿namespace com.game.module.Story
{
    using System;

    public interface IParseNode
    {
        void ParseNode(XMLNode node);
    }
}

