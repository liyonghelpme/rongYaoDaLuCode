﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using System;

    public class MapChangeAction : BaseAction
    {
        private uint mapId;

        public override void ParseNode(XMLNode node)
        {
            this.mapId = Convert.ToUInt32(node.GetValue("@target_id"));
        }

        public override void Run()
        {
            Singleton<StartLoadingView>.Instance.OpenView();
            Singleton<StoryMode>.Instance.DataUpdate(Singleton<StoryMode>.Instance.FORCE_STOP_STORY);
        }

        public override bool CanInterrupt
        {
            get
            {
                return true;
            }
        }
    }
}

