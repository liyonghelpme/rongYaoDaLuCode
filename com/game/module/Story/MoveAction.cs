﻿namespace com.game.module.Story
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class MoveAction : BaseAction
    {
        private string createId;
        private GameObject role;
        private int speed;
        private Vector3 targetPos = Vector3.zero;
        private float targetX;
        private float targetY;

        public override void ParseNode(XMLNode node)
        {
            this.createId = node.GetValue("@id");
            this.targetX = Convert.ToSingle(node.GetValue("@targetX"));
            this.targetY = Convert.ToSingle(node.GetValue("@targetY"));
            this.speed = Convert.ToInt32(node.GetValue("@speed"));
            base.block = Convert.ToBoolean(node.GetValue("@block"));
            if (this.speed <= 0f)
            {
                this.speed = 3;
            }
        }

        public override void Run()
        {
            this.targetPos = new Vector3(this.targetX, this.targetY, 0f);
            if ("-1" == this.createId)
            {
                (AppMap.Instance.me.Controller as ActionControler).MoveSpeed = this.speed;
                AppMap.Instance.me.Controller.MoveTo(this.targetX, this.targetY, null);
            }
            else
            {
                CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.createId);
                if (createAction != null)
                {
                    if (createAction.IsMonster)
                    {
                        (AppMap.Instance.GetMonster(createAction.MonsterId).Controller as ActionControler).MoveSpeed = this.speed;
                        AppMap.Instance.GetMonster(createAction.MonsterId).Controller.MoveTo(this.targetPos.x, this.targetPos.y, null);
                    }
                    else
                    {
                        this.role = createAction.Role;
                        StoryRoleMove component = this.role.GetComponent<StoryRoleMove>();
                        if (null != component)
                        {
                            UnityEngine.Object.Destroy(component);
                        }
                        component = this.role.AddComponent<StoryRoleMove>();
                        this.targetPos.z = this.role.transform.position.z;
                        component.TargetPos = this.targetPos;
                        component.MoveSpeed = this.speed;
                    }
                }
            }
        }

        public override void Stop()
        {
            if ("-1" == this.createId)
            {
                (AppMap.Instance.me.Controller as ActionControler).StopWalk();
            }
            else
            {
                CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.createId);
                if (createAction != null)
                {
                    if (createAction.IsMonster)
                    {
                        (AppMap.Instance.GetMonster(createAction.MonsterId).Controller as ActionControler).StopWalk();
                    }
                    else
                    {
                        this.role = createAction.Role;
                        StoryRoleMove component = this.role.GetComponent<StoryRoleMove>();
                        if (null != component)
                        {
                            component.Stop();
                        }
                    }
                }
            }
        }

        public override bool CanInterrupt
        {
            get
            {
                if (!base.block)
                {
                    if ("-1" == this.createId)
                    {
                        return (0 == AppMap.Instance.me.Controller.StatuController.CurrentStatu);
                    }
                    CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.createId);
                    if (createAction == null)
                    {
                        return true;
                    }
                    if (createAction.IsMonster)
                    {
                        return (0 == AppMap.Instance.GetMonster(createAction.MonsterId).Controller.StatuController.CurrentStatu);
                    }
                    if (null != this.role)
                    {
                        return (this.role.transform.position == this.targetPos);
                    }
                }
                return true;
            }
        }

        public string CreateId
        {
            get
            {
                return this.createId;
            }
        }

        public float TargetX
        {
            get
            {
                return this.targetX;
            }
        }
    }
}

