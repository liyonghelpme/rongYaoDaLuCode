﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using com.game.module.effect;
    using System;
    using UnityEngine;

    public class MovieAction : BaseAction
    {
        private string key;

        public override void ParseNode(XMLNode node)
        {
            this.key = node.GetValue("@key");
        }

        public override void Run()
        {
            Vector3 cameraPosition = Singleton<EffectCameraMgr>.Instance.CameraPosition;
            EffectMgr.Instance.CreateStoryMovieEffect(this.key, cameraPosition, null);
        }

        public override bool CanInterrupt
        {
            get
            {
                return EffectMgr.Instance.IsStoryMovieEffectStopped(this.key);
            }
        }
    }
}

