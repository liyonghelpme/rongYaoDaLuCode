﻿namespace com.game.module.Story
{
    using System;

    public class PoseAction : SpellAction
    {
        public override void ParseNode(XMLNode node)
        {
            base.createId = node.GetValue("@id");
            base.skill = Convert.ToInt32(node.GetValue("@pose"));
            base.repeat = false;
        }
    }
}

