﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using System;

    public class RemoveAction : BaseAction
    {
        private string createId;

        public override void ParseNode(XMLNode node)
        {
            this.createId = node.GetValue("@id");
        }

        public override void Run()
        {
            CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.createId);
            if (createAction != null)
            {
                createAction.Clear();
            }
        }
    }
}

