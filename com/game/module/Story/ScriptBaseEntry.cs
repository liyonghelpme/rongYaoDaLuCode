﻿namespace com.game.module.Story
{
    using com.game.vo;
    using System;
    using System.Collections;

    public class ScriptBaseEntry : IParseNode
    {
        private string comparator;
        public string ScriptName;
        private string value;

        public override bool Equals(object other)
        {
            return ((other is ScriptBaseEntry) && this.LevelMatch());
        }

        protected bool LevelMatch()
        {
            bool flag = (this.comparator == null) || (string.Empty == this.comparator);
            bool flag2 = (this.value == null) || (string.Empty == this.value);
            if (flag || flag2)
            {
                return true;
            }
            int level = MeVo.instance.Level;
            int num2 = Convert.ToInt32(this.value);
            if (">" == this.comparator)
            {
                return (level > num2);
            }
            if ("<" == this.comparator)
            {
                return (level < num2);
            }
            if (">=" == this.comparator)
            {
                return (level >= num2);
            }
            if ("<=" == this.comparator)
            {
                return (level <= num2);
            }
            return (("=" == this.comparator) && (level == num2));
        }

        public virtual void ParseNode(XMLNode node)
        {
            IEnumerator enumerator = node.GetNodeList("condition").GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    XMLNode current = (XMLNode) enumerator.Current;
                    string str = current.GetValue("@comparator");
                    string str2 = current.GetValue("@value");
                    if (str != null)
                    {
                        this.comparator = str;
                    }
                    if (str2 != null)
                    {
                        this.value = str2;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }
    }
}

