﻿namespace com.game.module.Story
{
    using System;
    using System.Collections;

    public class ScriptBaseSceneEntry : ScriptBaseEntry
    {
        public string SceneId;

        public override bool Equals(object other)
        {
            if (base.Equals(other))
            {
                ScriptBaseSceneEntry entry = other as ScriptBaseSceneEntry;
                if (entry != null)
                {
                    return (this.SceneId == entry.SceneId);
                }
            }
            return false;
        }

        public override void ParseNode(XMLNode node)
        {
            base.ParseNode(node);
            IEnumerator enumerator = node.GetNodeList("condition").GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    string str = ((XMLNode) enumerator.Current).GetValue("@sceneId");
                    if (str != null)
                    {
                        this.SceneId = str;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }
    }
}

