﻿namespace com.game.module.Story
{
    using System;
    using System.Collections;

    public class ScriptBaseStageEntry : ScriptBaseSceneEntry
    {
        public string StageId;

        public override bool Equals(object other)
        {
            if (!base.Equals(other))
            {
                return false;
            }
            ScriptBaseStageEntry entry = other as ScriptBaseStageEntry;
            return ((entry != null) && (this.StageId == entry.StageId));
        }

        public override void ParseNode(XMLNode node)
        {
            base.ParseNode(node);
            IEnumerator enumerator = node.GetNodeList("condition").GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    string str = ((XMLNode) enumerator.Current).GetValue("@stageId");
                    if (str != null)
                    {
                        this.StageId = str;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }
    }
}

