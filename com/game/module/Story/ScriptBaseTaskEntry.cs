﻿namespace com.game.module.Story
{
    using System;
    using System.Collections;

    public class ScriptBaseTaskEntry : ScriptBaseEntry
    {
        public string TaskId;

        public override bool Equals(object other)
        {
            if (!base.Equals(other))
            {
                return false;
            }
            ScriptBaseTaskEntry entry = other as ScriptBaseTaskEntry;
            return ((entry != null) && (this.TaskId == entry.TaskId));
        }

        public override void ParseNode(XMLNode node)
        {
            IEnumerator enumerator = node.GetNodeList("condition").GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    this.TaskId = ((XMLNode) enumerator.Current).GetValue("@value");
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }
    }
}

