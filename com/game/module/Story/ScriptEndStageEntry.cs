﻿namespace com.game.module.Story
{
    using System;

    public class ScriptEndStageEntry : ScriptBaseStageEntry
    {
        public override bool Equals(object other)
        {
            return (base.Equals(other) && (other is ScriptEndStageEntry));
        }
    }
}

