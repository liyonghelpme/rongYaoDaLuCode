﻿namespace com.game.module.Story
{
    using System;

    public class ScriptEnterSceneEntry : ScriptBaseSceneEntry
    {
        public override bool Equals(object other)
        {
            return (base.Equals(other) && (other is ScriptEnterSceneEntry));
        }
    }
}

