﻿namespace com.game.module.Story
{
    using System;

    public class ScriptExitSceneEntry : ScriptBaseSceneEntry
    {
        public override bool Equals(object other)
        {
            return (base.Equals(other) && (other is ScriptExitSceneEntry));
        }
    }
}

