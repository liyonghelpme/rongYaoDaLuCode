﻿namespace com.game.module.Story
{
    using System;
    using System.Collections;

    public class ScriptFightStageEntry : ScriptBaseStageEntry
    {
        public string MonsterId;

        public override bool Equals(object other)
        {
            if (!base.Equals(other))
            {
                return false;
            }
            ScriptFightStageEntry entry = other as ScriptFightStageEntry;
            return ((entry != null) && (this.MonsterId == entry.MonsterId));
        }

        public override void ParseNode(XMLNode node)
        {
            base.ParseNode(node);
            IEnumerator enumerator = node.GetNodeList("condition").GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    string str = ((XMLNode) enumerator.Current).GetValue("@monsterId");
                    if (str != null)
                    {
                        this.MonsterId = str;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }
    }
}

