﻿namespace com.game.module.Story
{
    using System;

    public class ScriptFinishTaskEntry : ScriptBaseTaskEntry
    {
        public override bool Equals(object other)
        {
            return (base.Equals(other) && (other is ScriptFinishTaskEntry));
        }
    }
}

