﻿namespace com.game.module.Story
{
    using com.game.utils;
    using System;
    using System.Collections;
    using UnityEngine;

    public class ScriptHeroArriveAreaEntry : ScriptBaseSceneEntry
    {
        private float bottomLeftX;
        private float bottomLeftY;
        public Vector3 Position;
        private float topRightX;
        private float topRightY;
        private string value;

        public override bool Equals(object other)
        {
            if (!base.Equals(other))
            {
                return false;
            }
            ScriptHeroArriveAreaEntry entry = other as ScriptHeroArriveAreaEntry;
            return ((entry != null) && this.InArea(entry.Position));
        }

        private bool InArea(Vector3 pos)
        {
            if ((pos.x < this.bottomLeftX) || (pos.x > this.topRightX))
            {
                return false;
            }
            return ((pos.y >= this.bottomLeftY) && (pos.y <= this.topRightY));
        }

        private void ParseCoordinate()
        {
            string[] valueListFromString = StringUtils.GetValueListFromString(this.value, '|');
            if (valueListFromString.Length >= 2)
            {
                string[] strArray2 = StringUtils.GetValueListFromString(valueListFromString[0], ',');
                string[] strArray3 = StringUtils.GetValueListFromString(valueListFromString[1], ',');
                this.bottomLeftX = Convert.ToSingle(strArray2[0]);
                this.bottomLeftY = Convert.ToSingle(strArray2[1]);
                this.topRightX = Convert.ToSingle(strArray3[0]);
                this.topRightY = Convert.ToSingle(strArray3[1]);
            }
        }

        public override void ParseNode(XMLNode node)
        {
            base.ParseNode(node);
            IEnumerator enumerator = node.GetNodeList("condition").GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    this.value = ((XMLNode) enumerator.Current).GetValue("@value");
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            this.ParseCoordinate();
        }
    }
}

