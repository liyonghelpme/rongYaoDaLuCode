﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections;

    public class ScriptHpChangeEntry : ScriptBaseEntry
    {
        private string hpComparator;
        private string hpValue;
        public string Id = string.Empty;
        public string RoleType = string.Empty;

        public override bool Equals(object other)
        {
            if (base.Equals(other))
            {
                ScriptHpChangeEntry entry = other as ScriptHpChangeEntry;
                if (entry != null)
                {
                    bool flag = this.RoleType.Equals(entry.RoleType);
                    bool flag2 = this.Id.Equals(entry.Id);
                    return ((flag && flag2) && this.HpChangeMatch());
                }
            }
            return false;
        }

        private bool HpChangeMatch()
        {
            bool flag = (this.hpComparator == null) || (string.Empty == this.hpComparator);
            bool flag2 = (this.hpValue == null) || (string.Empty == this.hpValue);
            if (flag || flag2)
            {
                return true;
            }
            uint curHp = MeVo.instance.CurHp;
            uint num2 = Convert.ToUInt32(this.hpValue);
            if (this.RoleType.Equals("monster"))
            {
                MonsterVo monsterWithConfigId = Singleton<MonsterMgr>.Instance.GetMonsterWithConfigId(this.Id);
                if (monsterWithConfigId == null)
                {
                    return false;
                }
                curHp = monsterWithConfigId.CurHp;
            }
            if (">" == this.hpComparator)
            {
                return (curHp > num2);
            }
            if ("<" == this.hpComparator)
            {
                return (curHp < num2);
            }
            if (">=" == this.hpComparator)
            {
                return (curHp >= num2);
            }
            if ("<=" == this.hpComparator)
            {
                return (curHp <= num2);
            }
            return (("=" == this.hpComparator) && (curHp == num2));
        }

        public override void ParseNode(XMLNode node)
        {
            base.ParseNode(node);
            IEnumerator enumerator = node.GetNodeList("condition").GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    XMLNode current = (XMLNode) enumerator.Current;
                    string str = current.GetValue("@comparator");
                    string str2 = current.GetValue("@value");
                    string str3 = current.GetValue("@roleType");
                    string str4 = current.GetValue("@id");
                    if (str != null)
                    {
                        this.hpComparator = str;
                    }
                    if (str2 != null)
                    {
                        this.hpValue = str2;
                    }
                    if (str3 != null)
                    {
                        this.RoleType = str3;
                    }
                    if (str4 != null)
                    {
                        this.Id = str4;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }
    }
}

