﻿namespace com.game.module.Story
{
    using System;

    public class ScriptLevelUpEntry : ScriptBaseEntry
    {
        public override bool Equals(object other)
        {
            return (base.Equals(other) && (other is ScriptLevelUpEntry));
        }
    }
}

