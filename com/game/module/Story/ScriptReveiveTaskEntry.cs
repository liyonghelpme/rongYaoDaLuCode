﻿namespace com.game.module.Story
{
    using System;

    public class ScriptReveiveTaskEntry : ScriptBaseTaskEntry
    {
        public override bool Equals(object other)
        {
            return (base.Equals(other) && (other is ScriptReveiveTaskEntry));
        }
    }
}

