﻿namespace com.game.module.Story
{
    using System;

    public class ScriptStartStageEntry : ScriptBaseStageEntry
    {
        public override bool Equals(object other)
        {
            return (base.Equals(other) && (other is ScriptStartStageEntry));
        }
    }
}

