﻿namespace com.game.module.Story
{
    using com.game;
    using com.game.module.core;
    using System;

    public class SpellAction : BaseAction
    {
        protected string createId;
        protected bool repeat;
        protected int skill;

        private int GetSkillId(int skill)
        {
            int[] numArray = new int[] { 
                0, 1, 8, 0, 1, 2, 3, 4, 5, 6, 10, 11, 12, 13, 14, 15, 
                0x10, 7, 0x12
             };
            return numArray[skill];
        }

        public override void ParseNode(XMLNode node)
        {
            this.createId = node.GetValue("@id");
            this.skill = Convert.ToInt32(node.GetValue("@skill"));
            this.repeat = Convert.ToBoolean(node.GetValue("@repeat"));
        }

        public override void Run()
        {
            if ("-1" == this.createId)
            {
                if (((this.skill >= 2) && (this.skill <= 9)) || (this.skill == 0x11))
                {
                    AppMap.Instance.me.Controller.SkillController.RequestUseSkill(this.GetSkillId(this.skill), false, false, null, null, null);
                }
                else
                {
                    AppMap.Instance.me.Controller.StatuController.SetStatu(this.skill);
                }
            }
            else
            {
                CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.createId);
                if (createAction.IsMonster)
                {
                    if ((this.skill >= 3) && (this.skill <= 4))
                    {
                        AppMap.Instance.GetMonster(createAction.MonsterId).Controller.SkillController.RequestUseSkill(this.GetSkillId(this.skill), false, false, null, null, null);
                    }
                    else
                    {
                        AppMap.Instance.GetMonster(createAction.MonsterId).Controller.StatuController.SetStatu(this.skill);
                    }
                }
                else
                {
                    StoryRoleSpell spell = createAction.Role.AddComponent<StoryRoleSpell>();
                    spell.Repeat = this.repeat;
                    spell.Skill = this.skill;
                }
            }
            vp_Timer.In(0.5f, new vp_Timer.Callback(this.Finish), 1, 0f, null);
        }

        public override bool CanInterrupt
        {
            get
            {
                return base.finished;
            }
        }
    }
}

