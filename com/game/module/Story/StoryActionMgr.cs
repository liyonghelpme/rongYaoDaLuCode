﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using com.u3d.bases.debug;
    using System;

    public class StoryActionMgr
    {
        public Callback EndStoryCallBack;
        public static StoryActionMgr Instance = ((Instance != null) ? Instance : new StoryActionMgr());
        private int talkIndex = -1;

        public void AutoStep()
        {
            if ((this.curAction != null) && this.curAction.CanInterrupt)
            {
                this.Step();
            }
        }

        public void FullOnClick()
        {
            if (((this.curAction != null) && this.curAction.MannualSwitch) && this.curAction.CanMannualSwitch)
            {
                this.Step();
            }
        }

        public void Init()
        {
            this.talkIndex = -1;
        }

        private void RunNextAction()
        {
            try
            {
                if (this.talkIndex < (Singleton<StoryMode>.Instance.ActionList.Count - 1))
                {
                    this.talkIndex++;
                    BaseAction action = Singleton<StoryMode>.Instance.ActionList[this.talkIndex];
                    if (action != null)
                    {
                        action.Run();
                    }
                }
            }
            catch (Exception exception)
            {
                Log.warin(this, exception.StackTrace);
            }
        }

        public void Skip()
        {
            if (this.curAction != null)
            {
                this.curAction.Stop();
            }
        }

        public void Start()
        {
            this.Step();
        }

        private void Step()
        {
            if (this.curAction != null)
            {
                this.curAction.AfterPlayed();
            }
            if (this.talkIndex >= (Singleton<StoryMode>.Instance.ActionList.Count - 1))
            {
                if (this.EndStoryCallBack != null)
                {
                    this.EndStoryCallBack();
                }
            }
            else
            {
                this.RunNextAction();
            }
        }

        public void Stop()
        {
            if (this.curAction != null)
            {
                this.curAction.AfterPlayed();
            }
            Singleton<StoryMode>.Instance.ClearActions();
        }

        private BaseAction curAction
        {
            get
            {
                if ((this.talkIndex >= 0) && (this.talkIndex < Singleton<StoryMode>.Instance.ActionList.Count))
                {
                    return Singleton<StoryMode>.Instance.ActionList[this.talkIndex];
                }
                return null;
            }
        }

        public BaseAction CurAction
        {
            get
            {
                return this.curAction;
            }
        }
    }
}

