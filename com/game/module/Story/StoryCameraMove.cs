﻿namespace com.game.module.Story
{
    using System;
    using UnityEngine;

    public class StoryCameraMove : MonoBehaviour
    {
        private float borderMax = 100f;
        private float borderMin;
        private float cameraGapLen;
        private Vector3 cameraPos;
        private bool exceedBorder;
        public GameObject FollowTarget;
        private Transform mapFarBg1;
        private Vector3 mapFarBg1Pos;
        public float MoveSpeed = 5f;
        private Transform selfTransform;
        private bool stopped;
        public Vector3 TargetPos;

        private void Start()
        {
            this.selfTransform = base.gameObject.transform;
            this.cameraPos = this.selfTransform.position;
            this.TargetPos.y = this.cameraPos.y;
            this.TargetPos.z = this.cameraPos.z;
            this.cameraGapLen = (((float) Screen.width) / ((float) Screen.height)) * Camera.main.orthographicSize;
            if (this.MoveSpeed <= 0f)
            {
                this.MoveSpeed = 10f;
            }
        }

        public void Stop()
        {
            this.stopped = true;
        }

        private void Update()
        {
            if ((!this.stopped && !this.exceedBorder) && (this.selfTransform.position != this.TargetPos))
            {
                if (null == this.FollowTarget)
                {
                    this.cameraPos = Vector3.MoveTowards(this.selfTransform.position, this.TargetPos, Time.deltaTime * this.MoveSpeed);
                }
                else
                {
                    this.cameraPos.x = this.FollowTarget.transform.position.x;
                }
                if ((this.cameraPos.x < this.borderMin) || (this.cameraPos.x > this.borderMax))
                {
                    this.exceedBorder = true;
                    this.cameraPos.x = Mathf.Clamp(this.cameraPos.x, this.borderMin, this.borderMax);
                }
                this.selfTransform.position = this.cameraPos;
                if (null != this.mapFarBg1)
                {
                    this.mapFarBg1Pos.x = this.selfTransform.position.x - this.cameraGapLen;
                    this.mapFarBg1.position = this.mapFarBg1Pos;
                }
            }
        }

        public bool ExceedBorder
        {
            get
            {
                return this.exceedBorder;
            }
        }
    }
}

