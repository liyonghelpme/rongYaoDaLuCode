﻿namespace com.game.module.Story
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class StoryControl : BaseControl<StoryControl>
    {
        private ClosedCallback closedCallback;

        private void LoadDataCallback()
        {
            if (Singleton<StoryMode>.Instance.LoadedOk)
            {
                Singleton<StoryView>.Instance.ViewClosedCallback = this.closedCallback;
            }
            else if (this.closedCallback != null)
            {
                this.closedCallback();
            }
        }

        public bool PlayEndStageStory(uint mapId, uint stageId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptEndStageEntry baseEntry = StoryFactory.GetEntry("end_stage") as ScriptEndStageEntry;
            baseEntry.SceneId = mapId.ToString();
            baseEntry.StageId = stageId.ToString();
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayEnterSceneStory(uint mapId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptEnterSceneEntry baseEntry = StoryFactory.GetEntry("enter_scene") as ScriptEnterSceneEntry;
            baseEntry.SceneId = mapId.ToString();
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayExitSceneStory(uint mapId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptExitSceneEntry baseEntry = StoryFactory.GetEntry("exit_scene") as ScriptExitSceneEntry;
            baseEntry.SceneId = mapId.ToString();
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayFightStageStory(uint mapId, uint stageId, uint monsterId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptFightStageEntry baseEntry = StoryFactory.GetEntry("fight_stage") as ScriptFightStageEntry;
            baseEntry.SceneId = mapId.ToString();
            baseEntry.StageId = stageId.ToString();
            baseEntry.MonsterId = monsterId.ToString();
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayFinishTaskStory(ulong taskId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptFinishTaskEntry baseEntry = StoryFactory.GetEntry("quest_finished") as ScriptFinishTaskEntry;
            baseEntry.TaskId = taskId.ToString();
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayHeroArriveStory(uint mapId, Vector3 pos, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptHeroArriveAreaEntry baseEntry = StoryFactory.GetEntry("hero_arrive_area") as ScriptHeroArriveAreaEntry;
            baseEntry.SceneId = mapId.ToString();
            baseEntry.Position = pos;
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayHeroHpChangeStory(ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptHpChangeEntry baseEntry = StoryFactory.GetEntry("hp_change") as ScriptHpChangeEntry;
            baseEntry.RoleType = string.Empty;
            baseEntry.Id = "-1";
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayLevelUpStory(ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptLevelUpEntry baseEntry = StoryFactory.GetEntry("level_up") as ScriptLevelUpEntry;
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayMonsterHpChangeStory(string monsterId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptHpChangeEntry baseEntry = StoryFactory.GetEntry("hp_change") as ScriptHpChangeEntry;
            baseEntry.RoleType = "monster";
            baseEntry.Id = monsterId;
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayReceiveTaskStory(ulong taskId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptReveiveTaskEntry baseEntry = StoryFactory.GetEntry("quest_add") as ScriptReveiveTaskEntry;
            baseEntry.TaskId = taskId.ToString();
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }

        public bool PlayStartStageStory(uint mapId, uint stageId, ClosedCallback callBack)
        {
            bool flag = false;
            this.closedCallback = callBack;
            ScriptStartStageEntry baseEntry = StoryFactory.GetEntry("start_stage") as ScriptStartStageEntry;
            baseEntry.SceneId = mapId.ToString();
            baseEntry.StageId = stageId.ToString();
            flag = Singleton<StoryMode>.Instance.StoryExits(baseEntry);
            if (flag)
            {
                Singleton<StoryMode>.Instance.LoadActionData(baseEntry, new LoadActionData(this.LoadDataCallback));
            }
            return flag;
        }
    }
}

