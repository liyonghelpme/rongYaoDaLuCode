﻿namespace com.game.module.Story
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class StoryFactory
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map19;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map1A;

        public static BaseAction GetAction(string type)
        {
            string key = type;
            if (key != null)
            {
                int num;
                if (<>f__switch$map1A == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(0x13);
                    dictionary.Add("talk", 0);
                    dictionary.Add("effect", 1);
                    dictionary.Add("full_effect", 2);
                    dictionary.Add("create", 3);
                    dictionary.Add("move", 4);
                    dictionary.Add("visible", 5);
                    dictionary.Add("remove", 6);
                    dictionary.Add("direction", 7);
                    dictionary.Add("pose", 8);
                    dictionary.Add("spell", 9);
                    dictionary.Add("delay", 10);
                    dictionary.Add("horse", 11);
                    dictionary.Add("movie", 12);
                    dictionary.Add("aside", 13);
                    dictionary.Add("cam_move", 14);
                    dictionary.Add("cam_shock", 15);
                    dictionary.Add("cam_zoom", 0x10);
                    dictionary.Add("emotion", 0x11);
                    dictionary.Add("mapChange", 0x12);
                    <>f__switch$map1A = dictionary;
                }
                if (<>f__switch$map1A.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return new TalkAction();

                        case 1:
                            return new EffectAction();

                        case 2:
                            return new FullEffectAction();

                        case 3:
                            return new CreateAction();

                        case 4:
                            return new MoveAction();

                        case 5:
                            return new VisibleAction();

                        case 6:
                            return new RemoveAction();

                        case 7:
                            return new DirectionAction();

                        case 8:
                            return new PoseAction();

                        case 9:
                            return new SpellAction();

                        case 10:
                            return new DelayAction();

                        case 11:
                            return new HorseAction();

                        case 12:
                            return new MovieAction();

                        case 13:
                            return new AsideAction();

                        case 14:
                            return new CameraMoveAction();

                        case 15:
                            return new CameraShockAction();

                        case 0x10:
                            return new CameraZoomAction();

                        case 0x11:
                            return new EmotionAction();

                        case 0x12:
                            return new MapChangeAction();
                    }
                }
            }
            return null;
        }

        public static ScriptBaseEntry GetEntry(string type)
        {
            string key = type;
            if (key != null)
            {
                int num;
                if (<>f__switch$map19 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(10);
                    dictionary.Add("enter_scene", 0);
                    dictionary.Add("exit_scene", 1);
                    dictionary.Add("start_stage", 2);
                    dictionary.Add("fight_stage", 3);
                    dictionary.Add("end_stage", 4);
                    dictionary.Add("quest_add", 5);
                    dictionary.Add("quest_finished", 6);
                    dictionary.Add("level_up", 7);
                    dictionary.Add("hp_change", 8);
                    dictionary.Add("hero_arrive_area", 9);
                    <>f__switch$map19 = dictionary;
                }
                if (<>f__switch$map19.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return new ScriptEnterSceneEntry();

                        case 1:
                            return new ScriptExitSceneEntry();

                        case 2:
                            return new ScriptStartStageEntry();

                        case 3:
                            return new ScriptFightStageEntry();

                        case 4:
                            return new ScriptEndStageEntry();

                        case 5:
                            return new ScriptReveiveTaskEntry();

                        case 6:
                            return new ScriptFinishTaskEntry();

                        case 7:
                            return new ScriptLevelUpEntry();

                        case 8:
                            return new ScriptHpChangeEntry();

                        case 9:
                            return new ScriptHeroArriveAreaEntry();
                    }
                }
            }
            return null;
        }
    }
}

