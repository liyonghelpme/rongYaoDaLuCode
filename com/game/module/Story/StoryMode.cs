﻿namespace com.game.module.Story
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class StoryMode : BaseMode<StoryMode>
    {
        private IList<BaseAction> actionList = new List<BaseAction>();
        private ScriptBaseEntry curScriptEntry;
        public readonly int FORCE_STOP_STORY = 1;
        private com.game.module.Story.LoadActionData loadActionDataCallback;
        private bool loadingData;
        private bool roleGoActive = true;
        private Dictionary<string, TextAsset> scriptDict = new Dictionary<string, TextAsset>();
        private IList<ScriptBaseEntry> scriptEntryList = new List<ScriptBaseEntry>();

        public void ClearActions()
        {
            IEnumerator<BaseAction> enumerator = this.actionList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.Clear();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.actionList.Clear();
        }

        private string GetBgAtlasUrl(string atlasName)
        {
            return ("Atlas/StoryGround/" + atlasName + ".assetbundle");
        }

        public CreateAction GetCreateAction(string createId)
        {
            IEnumerator<BaseAction> enumerator = this.actionList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    BaseAction current = enumerator.Current;
                    if (current is CreateAction)
                    {
                        CreateAction action2 = current as CreateAction;
                        if (action2.CreateId == createId)
                        {
                            return action2;
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return null;
        }

        public MoveAction GetMoveAction(string createId)
        {
            IEnumerator<BaseAction> enumerator = this.actionList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    BaseAction current = enumerator.Current;
                    if (current is MoveAction)
                    {
                        MoveAction action2 = current as MoveAction;
                        if (action2.CreateId == createId)
                        {
                            return action2;
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return null;
        }

        private string GetNPCAtlasUrl(string atlasName)
        {
            return ("Atlas/Npc/" + atlasName + ".assetbundle");
        }

        public void Init()
        {
            string storyScriptUrl = UrlUtils.GetStoryScriptUrl("script_t");
            AssetManager.Instance.LoadAsset<TextAsset>(storyScriptUrl, new LoadAssetFinish<TextAsset>(this.ScriptTemplateLoaded), null, false, true);
        }

        public void LoadActionData(ScriptBaseEntry baseEntry, com.game.module.Story.LoadActionData loadDataCallback)
        {
            if (this.loadingData)
            {
                if (loadDataCallback != null)
                {
                    loadDataCallback();
                }
                return;
            }
            this.loadingData = true;
            this.curScriptEntry = null;
            IEnumerator<ScriptBaseEntry> enumerator = this.scriptEntryList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ScriptBaseEntry current = enumerator.Current;
                    if (current.Equals(baseEntry))
                    {
                        this.curScriptEntry = current;
                        this.actionList.Clear();
                        this.loadActionDataCallback = loadDataCallback;
                        string storyScriptUrl = UrlUtils.GetStoryScriptUrl(current.ScriptName);
                        AssetManager.Instance.LoadAsset<TextAsset>(storyScriptUrl, new LoadAssetFinish<TextAsset>(this.ScriptLoaded), null, false, true);
                        goto Label_00AA;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        Label_00AA:
            if (this.curScriptEntry == null)
            {
                this.loadingData = false;
                if (loadDataCallback != null)
                {
                    loadDataCallback();
                }
            }
        }

        private void ScriptLoaded(TextAsset textObj)
        {
            try
            {
                if (!this.scriptDict.ContainsKey(this.curScriptEntry.ScriptName) && (null != textObj))
                {
                    this.scriptDict.Add(this.curScriptEntry.ScriptName, textObj);
                }
                if (this.scriptDict.ContainsKey(this.curScriptEntry.ScriptName))
                {
                    string path = "script>0>action";
                    XMLNode node = XMLParser.Parse(this.scriptDict[this.curScriptEntry.ScriptName].ToString());
                    if (node != null)
                    {
                        IEnumerator enumerator = node.GetNodeList(path).GetEnumerator();
                        try
                        {
                            while (enumerator.MoveNext())
                            {
                                XMLNode current = (XMLNode) enumerator.Current;
                                BaseAction item = StoryFactory.GetAction(current.GetValue("@type"));
                                if (item != null)
                                {
                                    item.ParseNode(current);
                                    this.actionList.Add(item);
                                }
                            }
                        }
                        finally
                        {
                            IDisposable disposable = enumerator as IDisposable;
                            if (disposable == null)
                            {
                            }
                            disposable.Dispose();
                        }
                    }
                }
            }
            finally
            {
                this.loadingData = false;
                if (this.loadActionDataCallback != null)
                {
                    this.loadActionDataCallback();
                }
            }
        }

        private void ScriptTemplateLoaded(TextAsset textObj)
        {
            if (null != textObj)
            {
                string path = "triggerTable>0>trigger";
                IEnumerator enumerator = XMLParser.Parse(textObj.ToString()).GetNodeList(path).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        XMLNode current = (XMLNode) enumerator.Current;
                        string str2 = current.GetValue("@scriptName");
                        ScriptBaseEntry item = StoryFactory.GetEntry(current.GetValue("@type"));
                        if (item != null)
                        {
                            item.ScriptName = str2;
                            item.ParseNode(current);
                            this.scriptEntryList.Add(item);
                        }
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable == null)
                    {
                    }
                    disposable.Dispose();
                }
            }
        }

        public bool StoryExits(ScriptBaseEntry baseEntry)
        {
            IEnumerator<ScriptBaseEntry> enumerator = this.scriptEntryList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ScriptBaseEntry current = enumerator.Current;
                    if (current.Equals(baseEntry))
                    {
                        return true;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return false;
        }

        public IList<BaseAction> ActionList
        {
            get
            {
                return this.actionList;
            }
        }

        public bool LoadedOk
        {
            get
            {
                return (null != this.curScriptEntry);
            }
        }

        public bool RoleGoActive
        {
            get
            {
                return this.roleGoActive;
            }
        }
    }
}

