﻿namespace com.game.module.Story
{
    using System;
    using UnityEngine;

    public class StoryRoleMove : MonoBehaviour
    {
        private Animator animator;
        public float MoveSpeed = 3f;
        private Transform selfTransform;
        private bool stopped;
        public Vector3 TargetPos;

        private void Start()
        {
            this.selfTransform = base.gameObject.transform;
            this.animator = base.gameObject.GetComponentInChildren<Animator>();
            if (null != this.animator)
            {
                this.animator.SetInteger("State", 1);
            }
            if (this.selfTransform.position.x < this.TargetPos.x)
            {
                this.selfTransform.localScale = Vector3.one;
            }
            else
            {
                this.selfTransform.localScale = new Vector3(-1f, 1f, 1f);
            }
            if (this.MoveSpeed <= 0f)
            {
                this.MoveSpeed = 3f;
            }
        }

        public void Stop()
        {
            this.stopped = true;
        }

        private void Update()
        {
            if (!this.stopped)
            {
                if (this.selfTransform.position == this.TargetPos)
                {
                    if (null != this.animator)
                    {
                        this.animator.SetInteger("State", 0);
                    }
                }
                else
                {
                    if (null != this.animator)
                    {
                        this.animator.SetInteger("State", 1);
                    }
                    this.selfTransform.position = Vector3.MoveTowards(this.selfTransform.position, this.TargetPos, Time.deltaTime * this.MoveSpeed);
                }
            }
        }
    }
}

