﻿namespace com.game.module.Story
{
    using System;
    using UnityEngine;

    public class StoryRoleSpell : MonoBehaviour
    {
        private Animator animator;
        public bool Repeat;
        public int Skill;
        protected AnimatorStateInfo stateInfo;
        private int[] stateVector = new int[] { 
            Status.NAME_HASH_IDLE, Status.NAME_HASH_RUN, Status.NAME_HASH_ROLL, Status.NAME_HASH_ATTACK1, Status.NAME_HASH_ATTACK2, Status.NAME_HASH_ATTACK3, Status.NAME_HASH_ATTACK4, Status.NAME_HASH_SKILL1, Status.NAME_HASH_SKILL2, Status.NAME_HASH_SKILL3, Status.NAME_HASH_DEATH, Status.NAME_HASH_HURT1, Status.NAME_HASH_HURT2, Status.NAME_HASH_HURT3, Status.NAME_HASH_HURT4, Status.NAME_HASH_HURTDOWN, 
            Status.NAME_HASH_STANDUP, Status.NAME_HASH_DEATH_END, Status.NAME_HASH_WIN
         };

        private void ProcessState()
        {
            if (this.stateInfo.normalizedTime > 0.8)
            {
                this.animator.SetInteger("State", 0);
            }
        }

        private void Start()
        {
            this.animator = base.gameObject.GetComponentInChildren<Animator>();
            if (null != this.animator)
            {
                this.animator.SetInteger("State", this.Skill);
            }
        }

        private void Update()
        {
            if (!this.Repeat)
            {
                this.stateInfo = this.animator.GetCurrentAnimatorStateInfo(0);
                if (this.stateInfo.nameHash == this.stateVector[this.Skill])
                {
                    this.ProcessState();
                }
            }
        }
    }
}

