﻿namespace com.game.module.Story
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using com.u3d.bases.display.controler;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StoryView : BaseView<StoryView>
    {
        private GameObject asideWindow;
        private Vector3 bottomMaskDstPos;
        private Vector3 bottomMaskOrgPos;
        private Button btnFull;
        private Button btnSkip;
        private bool isOpen;
        private UILabel labAsideText;
        private UILabel labLeftName;
        private UILabel labLeftWords;
        private UILabel labRightName;
        private UILabel labRightWords;
        private bool lastRoleAi;
        private GameObject leftNPC;
        private readonly float maskEffectTime = 1f;
        private GameObject rightNPC;
        private UISprite sprBottomMask;
        private UISprite sprFullEffect;
        private UISprite sprLeftEmotion;
        private UISprite sprLeftNPC;
        private UISprite sprRightEmotion;
        private UISprite sprRightNPC;
        private UISprite sprTopMask;
        private GameObject talkWindow;
        private Vector3 topMaskDstPos;
        private Vector3 topMaskOrgPos;

        public override void CancelUpdateHandler()
        {
            Singleton<StoryMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateForceStopStoryHandler);
        }

        public override void CloseView()
        {
            try
            {
                base.CloseView();
                this.isOpen = false;
                StoryActionMgr.Instance.Stop();
                AppMap.Instance.me.Controller.AiController.SetAi(this.lastRoleAi);
                (AppMap.Instance.me.Controller as ActionControler).MoveSpeed = 3f;
            }
            finally
            {
                if (this.ViewClosedCallback != null)
                {
                    this.ViewClosedCallback();
                }
            }
        }

        private void EndStory()
        {
            this.isOpen = false;
            this.HideAllSubWindows();
            this.HideMask();
            vp_Timer.In(this.maskEffectTime, new vp_Timer.Callback(this.CloseView), 1, 0f, null);
        }

        private void FullOnClick(GameObject go)
        {
            if (this.isOpen)
            {
                StoryActionMgr.Instance.FullOnClick();
            }
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.HideAllSubWindows();
            this.ShowMask();
            vp_Timer.In(this.maskEffectTime, new vp_Timer.Callback(this.StartStory), 1, 0f, null);
        }

        private void HideAllSubWindows()
        {
            this.talkWindow.gameObject.SetActive(false);
            this.asideWindow.gameObject.SetActive(false);
            this.sprFullEffect.gameObject.SetActive(false);
        }

        private void HideMask()
        {
            this.ShowMaskMoveEffect(this.sprTopMask, this.topMaskOrgPos, this.topMaskDstPos);
            this.ShowMaskMoveEffect(this.sprBottomMask, this.bottomMaskOrgPos, this.bottomMaskDstPos);
        }

        protected override void Init()
        {
            this.asideWindow = this.gameObject.transform.Find("aside").gameObject;
            this.labAsideText = base.FindInChild<UILabel>("aside/words");
            this.talkWindow = this.gameObject.transform.Find("play/bottom/talkwindow").gameObject;
            this.leftNPC = this.gameObject.transform.Find("play/bottom/talkwindow/talk/left").gameObject;
            this.rightNPC = this.gameObject.transform.Find("play/bottom/talkwindow/talk/right").gameObject;
            this.labLeftName = base.FindInChild<UILabel>("play/bottom/talkwindow/talk/left/halfbody/name");
            this.sprLeftNPC = base.FindInChild<UISprite>("play/bottom/talkwindow/talk/left/halfbody/bust");
            this.labLeftWords = base.FindInChild<UILabel>("play/bottom/talkwindow/talk/left/words");
            this.sprLeftEmotion = base.FindInChild<UISprite>("play/bottom/talkwindow/talk/left/emotion");
            this.labRightName = base.FindInChild<UILabel>("play/bottom/talkwindow/talk/right/halfbody/name");
            this.sprRightNPC = base.FindInChild<UISprite>("play/bottom/talkwindow/talk/right/halfbody/bust");
            this.labRightWords = base.FindInChild<UILabel>("play/bottom/talkwindow/talk/right/words");
            this.sprRightEmotion = base.FindInChild<UISprite>("play/bottom/talkwindow/talk/right/emotion");
            this.btnSkip = base.FindInChild<Button>("play/bottom/talkwindow/talk/skip");
            this.btnFull = base.FindInChild<Button>("play/bottom/fullButton");
            this.sprFullEffect = base.FindInChild<UISprite>("fulleffect/image");
            this.sprTopMask = base.FindInChild<UISprite>("mask/topmask/background1");
            this.sprBottomMask = base.FindInChild<UISprite>("mask/bottommask/background1");
            this.topMaskOrgPos = this.sprTopMask.transform.localPosition;
            this.topMaskDstPos = this.topMaskOrgPos + new Vector3(0f, (float) this.sprTopMask.height, 0f);
            this.bottomMaskOrgPos = this.sprBottomMask.transform.localPosition;
            this.bottomMaskDstPos = this.bottomMaskOrgPos - new Vector3(0f, (float) this.sprBottomMask.height, 0f);
            this.btnSkip.onClick = new UIWidgetContainer.VoidDelegate(this.SkipOnClick);
            this.btnFull.onClick = new UIWidgetContainer.VoidDelegate(this.FullOnClick);
            StoryActionMgr.Instance.EndStoryCallBack = new com.game.module.Story.Callback(this.EndStory);
            this.InitLabel();
        }

        private void InitLabel()
        {
            this.btnSkip.label.text = LanguageManager.GetWord("StoryView.Skip");
        }

        public override void OpenView(PanelType type = 0)
        {
            this.lastRoleAi = AppMap.Instance.me.Controller.AiController.IsAiEnable;
            this.isOpen = false;
            StoryActionMgr.Instance.Init();
            base.OpenView(type);
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<StoryMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateForceStopStoryHandler);
        }

        private void ShowMask()
        {
            this.ShowMaskMoveEffect(this.sprTopMask, this.topMaskDstPos, this.topMaskOrgPos);
            this.ShowMaskMoveEffect(this.sprBottomMask, this.bottomMaskDstPos, this.bottomMaskOrgPos);
        }

        private void ShowMaskMoveEffect(UISprite sprMask, Vector3 from, Vector3 to)
        {
            TweenPosition component = sprMask.GetComponent<TweenPosition>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
            component = sprMask.gameObject.AddComponent<TweenPosition>();
            component.from = from;
            component.to = to;
            component.style = UITweener.Style.Once;
            component.method = UITweener.Method.QuintEaseInOut;
            component.duration = this.maskEffectTime;
        }

        private void SkipOnClick(GameObject go)
        {
            StoryActionMgr.Instance.Skip();
            this.CloseView();
        }

        private void StartStory()
        {
            StoryActionMgr.Instance.Start();
            this.isOpen = true;
        }

        public override void Update()
        {
            if (this.isOpen)
            {
                StoryActionMgr.Instance.AutoStep();
            }
        }

        private void UpdateForceStopStoryHandler(object sender, int code)
        {
            if (Singleton<StoryMode>.Instance.FORCE_STOP_STORY == code)
            {
                this.CloseView();
            }
        }

        public UILabel AsideText
        {
            get
            {
                return this.labAsideText;
            }
        }

        public GameObject AsideWindow
        {
            get
            {
                return this.asideWindow;
            }
        }

        public UISprite FullEffectSprite
        {
            get
            {
                return this.sprFullEffect;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public UISprite LeftEmotionSprite
        {
            get
            {
                return this.sprLeftEmotion;
            }
        }

        public GameObject LeftNPC
        {
            get
            {
                return this.leftNPC;
            }
        }

        public UILabel LeftNPCName
        {
            get
            {
                return this.labLeftName;
            }
        }

        public UISprite LeftNPCSprite
        {
            get
            {
                return this.sprLeftNPC;
            }
        }

        public UILabel LeftNPCWords
        {
            get
            {
                return this.labLeftWords;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public UISprite RightEmotionSprite
        {
            get
            {
                return this.sprRightEmotion;
            }
        }

        public GameObject RightNPC
        {
            get
            {
                return this.rightNPC;
            }
        }

        public UILabel RightNPCName
        {
            get
            {
                return this.labRightName;
            }
        }

        public UISprite RightNPCSprite
        {
            get
            {
                return this.sprRightNPC;
            }
        }

        public UILabel RightNPCWords
        {
            get
            {
                return this.labRightWords;
            }
        }

        public GameObject TalkWindow
        {
            get
            {
                return this.talkWindow;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Story/StoryView.assetbundle";
            }
        }

        public ClosedCallback ViewClosedCallback { get; set; }
    }
}

