﻿namespace com.game.module.Story
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.NPCBust;
    using com.game.vo;
    using System;
    using UnityEngine;

    public class TalkAction : BaseAction
    {
        private GameObject bust;
        private bool bustVisible;
        protected float delay;
        protected string npcId;
        private GameObject roleBust;
        private UISprite sprNPC;
        protected GameObject talkWindow;
        protected string words;

        public override void AfterPlayed()
        {
            this.talkWindow.SetActive(false);
            if (null != this.bust)
            {
                this.bust.SetActive(false);
            }
            if (null != this.roleBust)
            {
                this.roleBust.SetActive(false);
            }
        }

        public override void Clear()
        {
            this.Finish();
        }

        public override void ParseNode(XMLNode node)
        {
            this.npcId = node.GetValue("@id");
            this.words = node.GetValue("@text");
            this.delay = Convert.ToSingle(node.GetValue("@delay"));
        }

        private void RoleBustLoaded(GameObject bustObj)
        {
            this.roleBust = bustObj;
            this.roleBust.name = "my_bust";
            this.roleBust.transform.position = this.sprNPC.transform.position;
            MeVo.instance.InitRoleBustPosition(this.roleBust);
            this.roleBust.SetActive(true);
            this.bustVisible = true;
        }

        public override void Run()
        {
            UILabel leftNPCName;
            UILabel leftNPCWords;
            if ("-1" == this.npcId)
            {
                Singleton<StoryView>.Instance.LeftNPC.SetActive(true);
                Singleton<StoryView>.Instance.RightNPC.SetActive(false);
                this.sprNPC = Singleton<StoryView>.Instance.LeftNPCSprite;
                leftNPCName = Singleton<StoryView>.Instance.LeftNPCName;
                leftNPCWords = Singleton<StoryView>.Instance.LeftNPCWords;
                NPCBustMgr.Instance.GetBust(MeVo.instance.BustUrl, new NPCBustMgr.BustCreatedCallback(this.RoleBustLoaded));
            }
            else
            {
                Singleton<StoryView>.Instance.LeftNPC.SetActive(false);
                Singleton<StoryView>.Instance.RightNPC.SetActive(true);
                this.sprNPC = Singleton<StoryView>.Instance.RightNPCSprite;
                leftNPCName = Singleton<StoryView>.Instance.RightNPCName;
                leftNPCWords = Singleton<StoryView>.Instance.RightNPCWords;
            }
            this.talkWindow = Singleton<StoryView>.Instance.TalkWindow;
            if (!this.talkWindow.activeSelf)
            {
                this.talkWindow.SetActive(true);
            }
            TypewriterEffect component = leftNPCWords.GetComponent<TypewriterEffect>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
            string playerHeadSpriteName = string.Empty;
            string name = string.Empty;
            if ("-1" == this.npcId)
            {
                playerHeadSpriteName = Singleton<RoleMode>.Instance.GetPlayerHeadSpriteName(MeVo.instance.job);
                name = MeVo.instance.Name;
            }
            else
            {
                CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.npcId);
                if (createAction != null)
                {
                    string resourceId = createAction.ResourceId;
                    if (createAction.IsMonster)
                    {
                        SysMonsterVo vo = BaseDataMgr.instance.getSysMonsterVo(Convert.ToUInt32(resourceId));
                        if (vo != null)
                        {
                            playerHeadSpriteName = vo.icon.ToString();
                            name = vo.name;
                        }
                    }
                    else
                    {
                        SysNpcVo npcVoById = BaseDataMgr.instance.GetNpcVoById(Convert.ToUInt32(resourceId));
                        if (npcVoById != null)
                        {
                            name = npcVoById.name;
                        }
                    }
                    this.bust = createAction.Bust;
                }
            }
            this.sprNPC.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            this.sprNPC.spriteName = playerHeadSpriteName;
            this.sprNPC.SetActive(false);
            leftNPCName.text = name;
            leftNPCWords.text = this.words;
            if (null != this.bust)
            {
                this.bust.SetActive(true);
                this.bustVisible = true;
            }
            if (this.delay > 0f)
            {
                vp_Timer.In(this.delay, new vp_Timer.Callback(this.Finish), 1, 0f, null);
            }
        }

        public override bool CanInterrupt
        {
            get
            {
                return (base.finished && this.bustVisible);
            }
        }

        public override bool CanMannualSwitch
        {
            get
            {
                return this.bustVisible;
            }
        }

        public override bool MannualSwitch
        {
            get
            {
                return true;
            }
        }
    }
}

