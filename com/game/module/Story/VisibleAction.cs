﻿namespace com.game.module.Story
{
    using com.game;
    using com.game.module.core;
    using com.game.module.effect;
    using System;
    using UnityEngine;

    public class VisibleAction : BaseAction
    {
        private string createId;
        private bool visible;

        public override void ParseNode(XMLNode node)
        {
            this.createId = node.GetValue("@id");
            this.visible = Convert.ToBoolean(node.GetValue("@visible"));
        }

        public override void Run()
        {
            if ("-1" == this.createId)
            {
                this.ShowMe(this.visible);
            }
            else
            {
                CreateAction createAction = Singleton<StoryMode>.Instance.GetCreateAction(this.createId);
                if (createAction.IsMonster)
                {
                    AppMap.Instance.GetMonster(createAction.MonsterId).Controller.gameObject.SetActive(this.visible);
                }
                else
                {
                    createAction.Role.SetActive(this.visible);
                }
            }
        }

        private void ShowMe(bool visible)
        {
            AppMap.Instance.me.Controller.gameObject.SetActive(visible);
            if (Singleton<StoryMode>.Instance.RoleGoActive && (null != AppMap.Instance.me.Controller.GoName))
            {
                AppMap.Instance.me.Controller.GoName.SetActive(visible);
            }
            GameObject mainEffectGameObject = EffectMgr.Instance.GetMainEffectGameObject("20006", string.Empty);
            if (null != mainEffectGameObject)
            {
                mainEffectGameObject.SetActive(false);
            }
        }
    }
}

