﻿namespace com.game.module.SystemData
{
    using System;

    public class ServerTime
    {
        private static ServerTime _instance;
        private int _timestamp;

        private void UpdateServerTime()
        {
            this._timestamp++;
        }

        public static ServerTime Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServerTime();
                }
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        public int Timestamp
        {
            get
            {
                return this._timestamp;
            }
            set
            {
                this._timestamp = value;
                vp_Timer.CancelAll("UpdateServerTime");
                vp_Timer.In(1f, new vp_Timer.Callback(this.UpdateServerTime), 0x989680, 1f, null);
            }
        }
    }
}

