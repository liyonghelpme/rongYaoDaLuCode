﻿namespace com.game.module.SystemSettings
{
    using com.game;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;

    public static class SendBuffer
    {
        public static List<EventDelegate> Add(this List<EventDelegate> list, EventDelegate.Callback callback)
        {
            list.Add(new EventDelegate(callback));
            return list;
        }

        public static void Add(this List<PSystemSettings> sendBuffer, byte Key, byte Value)
        {
            PSystemSettings item = new PSystemSettings {
                key = Key,
                value = Value
            };
            sendBuffer.Add(item);
        }

        public static List<PSystemSettings> Empty(this List<PSystemSettings> list)
        {
            list.Clear();
            return list;
        }

        public static void Flush(this List<PSystemSettings> sendBuffer)
        {
            if (sendBuffer.Count != 0)
            {
                MemoryStream msdata = new MemoryStream();
                Module_8.write_8_2(msdata, sendBuffer);
                AppNet.gameNet.send(msdata, 8, 2);
                sendBuffer.Empty();
            }
        }
    }
}

