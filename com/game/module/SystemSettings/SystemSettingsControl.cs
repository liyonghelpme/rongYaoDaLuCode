﻿namespace com.game.module.SystemSettings
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;
    using UnityEngine;

    public class SystemSettingsControl : BaseControl<SystemSettingsControl>
    {
        private void Fun_8_1(INetData data)
        {
            SettingsGetAllMsg_8_1 g__ = new SettingsGetAllMsg_8_1();
            g__.read(data.GetMemoryStream());
            for (int i = 0; i < g__.key.Count; i++)
            {
                switch (g__.key[i])
                {
                    case 250:
                        Singleton<SystemSettingsMode>.Instance.BackgroundMusic = Convert.ToBoolean(g__.val[i]);
                        break;

                    case 0xfb:
                        Singleton<SystemSettingsMode>.Instance.EffectSound = Convert.ToBoolean(g__.val[i]);
                        break;

                    case 0xfc:
                        Singleton<SystemSettingsMode>.Instance.ScreenQuality = g__.val[i];
                        break;

                    case 0xfd:
                        Singleton<SystemSettingsMode>.Instance.ParicleQuality = g__.val[i];
                        break;
                }
            }
        }

        private void Fun_8_2(INetData data)
        {
            SettingsSaveMsg_8_2 g__ = new SettingsSaveMsg_8_2();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                Debug.LogWarning("保存系统设置错误! Code = " + g__.code);
            }
        }

        private void Fun_8_3(INetData data)
        {
            SettingsProtoForbidMsg_8_3 g__ = new SettingsProtoForbidMsg_8_3();
            g__.read(data.GetMemoryStream());
            string[] param = new string[] { g__.section.ToString(), g__.method.ToString() };
            MessageManager.Show(LanguageManager.GetWord("SystemSettingControl.ForbidMessage", param));
            Debug.LogWarning(string.Concat(new object[] { "protocal forbidden! section:", g__.section, " method:", g__.method }));
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("2049", new NetMsgCallback(this.Fun_8_1));
            AppNet.main.addCMD("2050", new NetMsgCallback(this.Fun_8_2));
            AppNet.main.addCMD("2051", new NetMsgCallback(this.Fun_8_3));
        }
    }
}

