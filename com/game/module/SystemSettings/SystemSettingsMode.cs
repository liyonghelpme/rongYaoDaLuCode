﻿namespace com.game.module.SystemSettings
{
    using com.game;
    using com.game.module.core;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine;

    public class SystemSettingsMode : BaseMode<SystemSettingsMode>
    {
        private bool _backgroundMusic = true;
        private bool _effectSound = true;
        private uint _particleQuality = 100;
        private uint _screenQuality;
        private List<PSystemSettings> sendBuffer = new List<PSystemSettings>();
        public const int updateCode = 1;

        public void GetAllSystemSettings()
        {
            MemoryStream msdata = new MemoryStream();
            Module_8.write_8_1(msdata);
            AppNet.gameNet.send(msdata, 8, 1);
        }

        public void PrintQualityLevel()
        {
        }

        public void SaveSettings(SystemSettingsVo lastVo)
        {
            this.sendBuffer.Empty();
            if (lastVo._backgroundMusic != this._backgroundMusic)
            {
                this.sendBuffer.Add(250, Convert.ToByte(this._backgroundMusic));
            }
            if (lastVo._effectSound != this._effectSound)
            {
                this.sendBuffer.Add(0xfb, Convert.ToByte(this._effectSound));
            }
            if (lastVo._screenQuality != this._screenQuality)
            {
                this.sendBuffer.Add(0xfc, Convert.ToByte(this._screenQuality));
            }
            if (lastVo._particleQuality != this._particleQuality)
            {
                this.sendBuffer.Add(0xfd, Convert.ToByte(this._particleQuality));
            }
            this.sendBuffer.Flush();
        }

        public bool BackgroundMusic
        {
            get
            {
                return this._backgroundMusic;
            }
            set
            {
                this._backgroundMusic = value;
            }
        }

        public bool EffectSound
        {
            get
            {
                return this._effectSound;
            }
            set
            {
                this._effectSound = value;
            }
        }

        public uint ParicleQuality
        {
            get
            {
                return this._particleQuality;
            }
            set
            {
                this._particleQuality = value;
            }
        }

        public uint ScreenQuality
        {
            get
            {
                return this._screenQuality;
            }
            set
            {
                this._screenQuality = value;
                QualitySettings.SetQualityLevel((int) (value / 0x10));
                QualitySettings.antiAliasing = 0;
            }
        }
    }
}

