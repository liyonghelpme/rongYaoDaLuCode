﻿namespace com.game.module.SystemSettings
{
    using com.game;
    using com.game.consts;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SystemSettingsView : BaseView<SystemSettingsView>
    {
        private Button _btnBackToDungeon;
        private Button _btnClose;
        private Button _btnPromtCancel;
        private Button _btnPromtOK;
        private Button _btnQuitDungeon;
        private Button _btnSurrender;
        private UILabel _lblBackToDungeon;
        private UILabel _lblPromptCancel;
        private UILabel _lblPromptLabel;
        private UILabel _lblPromptOK;
        private UILabel _lblPromptTitle;
        private UILabel _lblQuitDungeon;
        private UILabel _lblSurrender;
        private Button _promt;
        private UISlider _sldParticleQuality;
        private UISlider _sldScreenQuality;
        private UIToggle _toBackgroundMusic;
        private UIToggle _toSoundEffect;
        [CompilerGenerated]
        private static UIWidgetContainer.VoidDelegate <>f__am$cache16;
        private bool lastBackgroundMusic;
        private float lastParticleQuality;
        private float lastScreenQuality;
        private bool lastSoundEffect;

        private void AdjustTogShow(UIToggle tog)
        {
            tog.FindChild("Background").SetActive(!tog.value);
            tog.FindChild("Checkmark").SetActive(tog.value);
        }

        private void BackgroundMusicOnChange()
        {
            Singleton<SystemSettingsMode>.Instance.BackgroundMusic = this._toBackgroundMusic.value;
            this.AdjustTogShow(this._toBackgroundMusic);
        }

        private void ClearViewButtonMethods()
        {
            this._btnQuitDungeon.onClick = (UIWidgetContainer.VoidDelegate) (this._btnBackToDungeon.onClick = null);
        }

        private void CloseAndSave(GameObject go)
        {
            SystemSettingsVo lastVo = new SystemSettingsVo(this.lastBackgroundMusic, this.lastSoundEffect, (byte) this.lastScreenQuality, (byte) this.lastParticleQuality);
            Singleton<SystemSettingsMode>.Instance.SaveSettings(lastVo);
            this._promt.SetActive(false);
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            this.lastBackgroundMusic = Singleton<SystemSettingsMode>.Instance.BackgroundMusic;
            this.lastSoundEffect = Singleton<SystemSettingsMode>.Instance.EffectSound;
            this.lastScreenQuality = Singleton<SystemSettingsMode>.Instance.ScreenQuality;
            this.lastParticleQuality = Singleton<SystemSettingsMode>.Instance.ParicleQuality;
            this._toBackgroundMusic.value = this.lastBackgroundMusic;
            this._toSoundEffect.value = this.lastSoundEffect;
            this._sldParticleQuality.value = this.lastParticleQuality * 0.01f;
            this._sldScreenQuality.value = this.lastScreenQuality * 0.01f;
            this.AdjustTogShow(this._toBackgroundMusic);
            this.AdjustTogShow(this._toSoundEffect);
            if (AppMap.Instance.IsInWifiPVP)
            {
                this.ClearViewButtonMethods();
                this._btnQuitDungeon.SetActive(false);
                this._btnBackToDungeon.SetActive(false);
                this._btnSurrender.SetActive(true);
                this._btnSurrender.onClick = go => this._promt.SetActive(true);
                this.InitPrompt(true);
            }
            else if (AppMap.Instance.IsInArena)
            {
                this.ClearViewButtonMethods();
                this._btnQuitDungeon.SetActive(false);
                this._btnBackToDungeon.SetActive(false);
                this._btnSurrender.SetActive(true);
                this._lblSurrender.text = "返回";
                this._btnSurrender.onClick = go => this.CloseView();
            }
            else if (GlobalData.isInCopy)
            {
                this.ClearViewButtonMethods();
                this._btnQuitDungeon.SetActive(true);
                this._btnBackToDungeon.SetActive(true);
                this._btnSurrender.SetActive(false);
                this._lblBackToDungeon.text = "回到副本";
                this._lblQuitDungeon.text = "退出副本";
                this._btnBackToDungeon.onClick = new UIWidgetContainer.VoidDelegate(this.CloseAndSave);
                if (<>f__am$cache16 == null)
                {
                    <>f__am$cache16 = go => DungeonMgr.Instance.Resume();
                }
                this._btnBackToDungeon.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._btnBackToDungeon.onClick, <>f__am$cache16);
                this._btnQuitDungeon.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._btnQuitDungeon.onClick, new UIWidgetContainer.VoidDelegate(this.OpenPrompt));
                DungeonMgr.Instance.Pause();
                this.InitPrompt(false);
            }
            else
            {
                this.ClearViewButtonMethods();
                this._btnQuitDungeon.SetActive(true);
                this._btnBackToDungeon.SetActive(true);
                this._btnSurrender.SetActive(false);
                this._lblBackToDungeon.text = "确定";
                this._lblQuitDungeon.text = "取消";
                this._btnBackToDungeon.onClick = new UIWidgetContainer.VoidDelegate(this.CloseAndSave);
                this._btnQuitDungeon.onClick = delegate (GameObject go) {
                    this.RevertChange();
                    this.CloseView();
                };
                this.InitPrompt(false);
            }
        }

        protected override void Init()
        {
            this._promt = base.FindInChild<Button>("prompt");
            this._btnPromtOK = base.FindInChild<Button>("prompt/body/btn_ok");
            this._btnPromtCancel = base.FindInChild<Button>("prompt/body/btn_cancel");
            this._lblPromptOK = base.FindInChild<UILabel>("prompt/body/btn_ok/label");
            this._lblPromptCancel = base.FindInChild<UILabel>("prompt/body/btn_cancel/label");
            this._lblPromptTitle = base.FindInChild<UILabel>("prompt/title/label");
            this._lblPromptLabel = base.FindInChild<UILabel>("prompt/body/label");
            this._btnSurrender = base.FindInChild<Button>("body/btn_surrender");
            this._lblSurrender = base.FindInChild<UILabel>("body/btn_surrender/label");
            this._btnClose = base.FindInChild<Button>("mask");
            this._btnQuitDungeon = base.FindInChild<Button>("body/btn_quitDungeon");
            this._lblQuitDungeon = base.FindInChild<UILabel>("body/btn_quitDungeon/label");
            this._btnBackToDungeon = base.FindInChild<Button>("body/btn_backToDungeon");
            this._lblBackToDungeon = base.FindInChild<UILabel>("body/btn_backToDungeon/label");
            this._sldParticleQuality = base.FindInChild<UISlider>("body/slider_particleQuality/Slider");
            this._sldScreenQuality = base.FindInChild<UISlider>("body/slider_screenQuality/Slider");
            this._toBackgroundMusic = base.FindInChild<UIToggle>("body/toggle_backgroundMusic/Toggle");
            this._toSoundEffect = base.FindInChild<UIToggle>("body/toggle_soundEffect/Toggle");
            this._sldParticleQuality.onChange.Add(new EventDelegate.Callback(this.ParticleQualityOnChange));
            this._sldScreenQuality.onChange.Add(new EventDelegate.Callback(this.ScreenQualityOnChange));
            this._toBackgroundMusic.onChange.Add(new EventDelegate.Callback(this.BackgroundMusicOnChange));
            this._toSoundEffect.onChange.Add(new EventDelegate.Callback(this.SoundEffectOnChange));
            this._promt.SetActive(false);
        }

        private void InitPrompt(bool isPVP)
        {
            if (isPVP)
            {
                this._lblPromptLabel.text = "对战中投降只能获得30%奖励, 是否投降?";
                this._lblPromptTitle.text = "投降";
                this._lblPromptCancel.text = "取消";
                this._btnPromtCancel.onClick = go => this._promt.SetActive(false);
                this._lblPromptOK.text = "投降";
                this._btnPromtOK.onClick = delegate (GameObject go) {
                    Singleton<WifiPvpMode>.Instance.PVPEndAndTellOther(WifiPvpConst.PVPEndState.fail, false);
                    this.CloseView();
                };
                this._btnClose.onClick = go => this.CloseView();
            }
            else
            {
                this._lblPromptLabel.text = "确定是否退出副本?";
                this._lblPromptTitle.text = "退出副本";
                this._lblPromptCancel.text = "取消";
                this._btnPromtCancel.onClick = go => this._promt.SetActive(false);
                this._lblPromptOK.text = "退出副本";
                this._btnPromtOK.onClick = delegate (GameObject go) {
                    this.QuitDungeon();
                    this.CloseAndSave(go);
                };
                this._btnClose.onClick = delegate (GameObject go) {
                    this.CloseAndSave(go);
                    DungeonMgr.Instance.Resume();
                };
            }
        }

        private void OpenPrompt(GameObject go)
        {
            this._promt.SetActive(true);
        }

        private void ParticleQualityOnChange()
        {
            Singleton<SystemSettingsMode>.Instance.ParicleQuality = (uint) (this._sldParticleQuality.value * 100f);
        }

        private void QuitDungeon()
        {
            if (GlobalData.isInCopy)
            {
                Singleton<DungeonMode>.Instance.quitDungeon();
                SystemSettingsVo lastVo = new SystemSettingsVo(this.lastBackgroundMusic, this.lastSoundEffect, (byte) this.lastScreenQuality, (byte) this.lastParticleQuality);
                Singleton<SystemSettingsMode>.Instance.SaveSettings(lastVo);
            }
        }

        private void RevertChange()
        {
            Singleton<SystemSettingsMode>.Instance.ParicleQuality = (uint) this.lastParticleQuality;
            Singleton<SystemSettingsMode>.Instance.ScreenQuality = (uint) this.lastScreenQuality;
            Singleton<SystemSettingsMode>.Instance.EffectSound = this.lastSoundEffect;
            Singleton<SystemSettingsMode>.Instance.BackgroundMusic = this.lastBackgroundMusic;
        }

        private void ScreenQualityOnChange()
        {
            Singleton<SystemSettingsMode>.Instance.ScreenQuality = (uint) (this._sldScreenQuality.value * 100f);
        }

        private void SoundEffectOnChange()
        {
            Singleton<SystemSettingsMode>.Instance.EffectSound = this._toSoundEffect.value;
            this.AdjustTogShow(this._toSoundEffect);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/SystemSettings/SystemSettingsView.assetbundle";
            }
        }
    }
}

