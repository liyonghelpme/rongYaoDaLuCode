﻿namespace com.game.module.SystemSettings
{
    using System;

    public sealed class SystemSettingsVo
    {
        public bool _backgroundMusic = true;
        public bool _effectSound = true;
        public byte _particleQuality = 100;
        public byte _screenQuality;

        public SystemSettingsVo(bool bgMusic, bool effSound, byte screenQlty, byte patclQlty)
        {
            this._backgroundMusic = bgMusic;
            this._effectSound = effSound;
            this._screenQuality = screenQlty;
            this._particleQuality = patclQlty;
        }
    }
}

