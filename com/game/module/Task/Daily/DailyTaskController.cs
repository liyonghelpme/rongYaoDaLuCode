﻿namespace com.game.module.Task.Daily
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class DailyTaskController : BaseControl<DailyTaskController>
    {
        protected override void NetListener()
        {
            AppNet.main.addCMD("10498", new NetMsgCallback(this.OnTaskUpdate));
            AppNet.main.addCMD("10499", new NetMsgCallback(this.OnSubmitTask));
        }

        private void OnSubmitTask(INetData data)
        {
            DailyTaskGetRewardMsg_41_3 g__ = new DailyTaskGetRewardMsg_41_3();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void OnTaskUpdate(INetData data)
        {
            DailyTaskUpdateDailyMsg_41_2 g__ = new DailyTaskUpdateDailyMsg_41_2();
            g__.read(data.GetMemoryStream());
            DailyTaskMode.Instance.Update(g__.dailyList);
        }
    }
}

