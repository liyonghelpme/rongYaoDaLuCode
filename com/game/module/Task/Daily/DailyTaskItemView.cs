﻿namespace com.game.module.Task.Daily
{
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.shop;
    using com.game.module.Utility;
    using System;
    using System.Linq;
    using UnityEngine;

    public class DailyTaskItemView
    {
        private UILabel _awardLabel;
        private UISprite _bg;
        private Button _btn;
        private Button _btnBg;
        private UILabel _btnLabel;
        private SysItemVoCell _cell;
        private GameObject _cellParentGo;
        private Action<DailyTaskItemView> _click;
        private UILabel _content;
        private UILabel _count;
        private GameObject _go;
        private UIGrid _grid;
        private DailyTaskItemVo _info;
        private UILabel _label;
        private UILabel _name;
        private UISprite _submitTag;

        public DailyTaskItemView(GameObject go, Action<DailyTaskItemView> click)
        {
            this._click = click;
            this._go = go;
            this._btnBg = this._go.GetComponent<Button>();
            this._bg = NGUITools.FindInChild<UISprite>(this._go, "bg");
            this._submitTag = NGUITools.FindInChild<UISprite>(this._go, "submitTag");
            this._name = NGUITools.FindInChild<UILabel>(this._go, "title/name");
            this._content = NGUITools.FindInChild<UILabel>(this._go, "content");
            this._count = NGUITools.FindInChild<UILabel>(this._go, "count");
            this._btn = NGUITools.FindInChild<Button>(this._go, "btn");
            this._btnLabel = NGUITools.FindInChild<UILabel>(this._btn.gameObject, "label");
            this._cellParentGo = NGUITools.FindChild(this._go, "award_list");
            this._grid = this._cellParentGo.GetComponent<UIGrid>();
            this._cell = NGUITools.FindChild(this._go, "award_list/item").AddMissingComponent<SysItemVoCell>();
            this._cell.SetActive(false);
            this._label = NGUITools.FindInChild<UILabel>(this._go, "labels/label");
            this._label.SetActive(false);
            this._awardLabel = NGUITools.FindInChild<UILabel>(this._go, "awardLabel");
            this.isSelected = false;
            this._btn.onClick = this._btnBg.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        public void Dispose()
        {
            if (null != this._go)
            {
                this._btn.onClick = (UIWidgetContainer.VoidDelegate) (this._btnBg.onClick = null);
                UnityEngine.Object.Destroy(this._go);
                this._go = null;
            }
        }

        private void OnClick(GameObject go)
        {
            if (this._btn.gameObject == go)
            {
                if (this._info.isCompleted)
                {
                    DailyTaskMode.Instance.SubmitTask(this._info.basicInfo.id);
                }
                else
                {
                    DailyTaskUtil.Instance.GetTaskCmd(this._info.basicInfo.id).Exe();
                }
            }
            else if ((this._btnBg.gameObject == go) && (this._click != null))
            {
                this._click(this);
            }
        }

        public void Update(DailyTaskItemVo info)
        {
            this._info = info;
            if (this._info == null)
            {
                this._go.SetActive(false);
            }
            else
            {
                int num2;
                this._go.SetActive(true);
                this._name.text = this._info.taskSysVo.name;
                this._content.text = this._info.taskSysVo.desc;
                string[] param = new string[] { this._info.basicInfo.count.ToString(), info.taskSysVo.count.ToString() };
                this._count.text = LanguageManager.GetWord("Common.Progress", param);
                switch (this._info.basicInfo.status)
                {
                    case 0:
                        this._btnLabel.text = LanguageManager.GetWord("Common.Goto");
                        break;

                    case 1:
                        this._btnLabel.text = LanguageManager.GetWord("Common.Get");
                        break;
                }
                this._bg.spriteName = (!this._info.isCompleted && !this._info.isSubmit) ? "weidianjizhuangtai-di" : "yixuanzhong-di";
                this._submitTag.SetActive(this._info.isSubmit);
                this._btn.SetActive(!this._info.isSubmit);
                int childCount = this._cellParentGo.transform.childCount;
                AwardInfo awardInfo = DailyTaskUtil.Instance.GetAwardInfo(this._info.basicInfo.id);
                while (childCount < awardInfo.ids.Count)
                {
                    SysItemVoCell cell = NGUITools.AddChild(this._cellParentGo, this._cell.gameObject).AddMissingComponent<SysItemVoCell>();
                    childCount = this._cellParentGo.transform.childCount;
                }
                for (num2 = 0; num2 < childCount; num2++)
                {
                    SysItemVoCell component = this._cellParentGo.transform.GetChild(num2).GetComponent<SysItemVoCell>();
                    if (num2 < awardInfo.ids.Count)
                    {
                        component.SetActive(true);
                        component.sysVoId = awardInfo.ids[num2];
                        component.count = awardInfo.counts[num2];
                    }
                    else
                    {
                        component.SetActive(false);
                    }
                }
                this._awardLabel.SetActive(awardInfo.currencyDic.Count > 0);
                int[] source = awardInfo.currencyDic.Keys.ToArray<int>();
                GameObject gameObject = this._label.transform.parent.gameObject;
                childCount = gameObject.transform.childCount;
                while (childCount < source.Count<int>())
                {
                    NGUITools.AddChild(gameObject, this._label.gameObject);
                    childCount = gameObject.transform.childCount;
                }
                float num3 = 0f;
                for (num2 = 0; num2 < childCount; num2++)
                {
                    UILabel rect = gameObject.transform.GetChild(num2).GetComponent<UILabel>();
                    if (num2 < source.Count<int>())
                    {
                        rect.SetActive(true);
                        rect.text = ShopMode.Currency.GetCurrency(source[num2]).name + ":" + awardInfo.GetCurrency(source[num2]);
                        Vector3 localPosition = this._label.transform.localPosition;
                        localPosition.x += num3;
                        rect.transform.localPosition = localPosition;
                        num3 += rect.width + 5;
                    }
                    else
                    {
                        rect.SetActive(false);
                    }
                }
                this._grid.Reposition();
            }
        }

        public GameObject go
        {
            get
            {
                return this._go;
            }
        }

        public bool isSelected
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
    }
}

