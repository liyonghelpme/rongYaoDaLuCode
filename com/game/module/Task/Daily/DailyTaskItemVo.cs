﻿namespace com.game.module.Task.Daily
{
    using com.game.data;
    using com.game.manager;
    using PCustomDataType;
    using System;

    public class DailyTaskItemVo
    {
        private PEachDaily _basicInfo;
        private SysDailyTaskVo _taskSysVo;

        public PEachDaily basicInfo
        {
            get
            {
                return this._basicInfo;
            }
            set
            {
                this._basicInfo = value;
                this._taskSysVo = BaseDataMgr.instance.GetDailyTask(this._basicInfo.id);
            }
        }

        public bool isCompleted
        {
            get
            {
                return (1 == this._basicInfo.status);
            }
        }

        public bool isSubmit
        {
            get
            {
                return (2 == this._basicInfo.status);
            }
        }

        public SysDailyTaskVo taskSysVo
        {
            get
            {
                return this._taskSysVo;
            }
        }
    }
}

