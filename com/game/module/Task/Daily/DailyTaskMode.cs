﻿namespace com.game.module.Task.Daily
{
    using com.game;
    using com.game.basic.events;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class DailyTaskMode : Notifier
    {
        private static DailyTaskMode _inst;
        private List<DailyTaskItemVo> _taskList = new List<DailyTaskItemVo>();

        private DailyTaskMode()
        {
        }

        public DailyTaskItemVo GetTask(ulong id)
        {
            int taskIndex = this.GetTaskIndex(id);
            return ((taskIndex >= 0) ? this._taskList[taskIndex] : null);
        }

        public int GetTaskIndex(ulong id)
        {
            for (int i = this._taskList.Count - 1; i >= 0; i--)
            {
                if (this._taskList[i].basicInfo.id == id)
                {
                    return i;
                }
            }
            return -1;
        }

        public void ReqTaskInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_41.write_41_1(msdata);
            AppNet.gameNet.send(msdata, 0x29, 1);
        }

        private int SortCompare(DailyTaskItemVo a, DailyTaskItemVo b)
        {
            if (a.isCompleted != b.isCompleted)
            {
                if (a.isCompleted)
                {
                    return -1;
                }
                return 1;
            }
            if (a.isSubmit != b.isSubmit)
            {
                if (a.isSubmit)
                {
                    return 1;
                }
                return -1;
            }
            if (!a.isSubmit)
            {
                double num = ((double) a.basicInfo.count) / ((double) a.taskSysVo.count);
                double num2 = ((double) b.basicInfo.count) / ((double) b.taskSysVo.count);
                if (num > num2)
                {
                    return -1;
                }
                if (num < num2)
                {
                    return 1;
                }
            }
            return (int) (a.basicInfo.id - b.basicInfo.id);
        }

        public void SubmitTask(uint id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_41.write_41_3(msdata, id);
            AppNet.gameNet.send(msdata, 0x29, 3);
        }

        public void Update(List<PEachDaily> arr)
        {
            foreach (PEachDaily daily in arr)
            {
                int taskIndex = this.GetTaskIndex((ulong) daily.id);
                if (taskIndex >= 0)
                {
                    this._taskList[taskIndex].basicInfo = daily;
                }
                else
                {
                    DailyTaskItemVo item = new DailyTaskItemVo {
                        basicInfo = daily
                    };
                    this._taskList.Add(item);
                }
            }
            this._taskList.Sort(new Comparison<DailyTaskItemVo>(this.SortCompare));
            this.Notify(1, 0, 0, null);
        }

        public static DailyTaskMode Instance
        {
            get
            {
                if (_inst == null)
                {
                    _inst = new DailyTaskMode();
                }
                return _inst;
            }
        }

        public List<DailyTaskItemVo> taskList
        {
            get
            {
                return this._taskList;
            }
        }

        public class Event
        {
            public const int SUBMIT = 2;
            public const int UPDATE = 1;
        }

        public class Status
        {
            public const int ACCEPTED = 0;
            public const int COMPLETE = 1;
            public const int NOTACCEPTED = -1;
            public const int SUBMIT = 2;
        }
    }
}

