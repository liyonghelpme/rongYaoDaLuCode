﻿namespace com.game.module.Task.Daily
{
    using com.game.cmd;
    using com.game.data;
    using com.game.manager;
    using com.game.module.Utility;
    using com.game.utils;
    using System;
    using System.Collections.Generic;

    public class DailyTaskUtil
    {
        private Dictionary<uint, AwardInfo> _awardDic = new Dictionary<uint, AwardInfo>();
        public static DailyTaskUtil _inst;
        private Dictionary<uint, Command> _taskCmdDic = new Dictionary<uint, Command>();

        private DailyTaskUtil()
        {
        }

        public AwardInfo GetAwardInfo(uint taskId)
        {
            if (!this._awardDic.ContainsKey(taskId))
            {
                SysDailyTaskVo dailyTask = BaseDataMgr.instance.GetDailyTask(taskId);
                if (dailyTask != null)
                {
                    AwardInfo info = AwardInfo.Parse(dailyTask.reward);
                    this._awardDic.Add(taskId, info);
                }
            }
            return (!this._awardDic.ContainsKey(taskId) ? null : this._awardDic[taskId]);
        }

        public Command GetTaskCmd(uint taskId)
        {
            if (!this._taskCmdDic.ContainsKey(taskId))
            {
                SysDailyTaskVo dailyTask = BaseDataMgr.instance.GetDailyTask(taskId);
                if (dailyTask != null)
                {
                    Command command = Command.Parse(StringUtils.GetValueString(dailyTask.pos));
                    this._taskCmdDic.Add(taskId, command);
                }
            }
            return (!this._taskCmdDic.ContainsKey(taskId) ? null : this._taskCmdDic[taskId]);
        }

        public static DailyTaskUtil Instance
        {
            get
            {
                if (_inst == null)
                {
                    _inst = new DailyTaskUtil();
                }
                return _inst;
            }
        }
    }
}

