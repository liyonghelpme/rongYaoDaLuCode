﻿namespace com.game.module.Task.Daily
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class DailyTaskView : BaseView<DailyTaskView>
    {
        private Button _btnClose;
        private DailyTaskItemView _curItem;
        private UIGrid _grid;
        private List<DailyTaskItemView> _items = new List<DailyTaskItemView>();
        private UIScrollView _scrl;
        private UILabel _title;

        protected override void HandleAfterOpenView()
        {
            this.UpdateView();
            this._scrl.ResetPosition();
            DailyTaskMode.Instance.Add(1, new NoticeListener(this.OnTaskEvent));
            DailyTaskMode.Instance.Add(2, new NoticeListener(this.OnTaskEvent));
            GlobalAPI.facade.Add(0x10, new NoticeListener(this.OnChangeScene));
        }

        protected override void HandleBeforeCloseView()
        {
            DailyTaskMode.Instance.Remove(1, new NoticeListener(this.OnTaskEvent));
            DailyTaskMode.Instance.Remove(2, new NoticeListener(this.OnTaskEvent));
            GlobalAPI.facade.Remove(0x10, new NoticeListener(this.OnChangeScene));
            for (int i = this._items.Count - 1; i > 0; i--)
            {
                this._items[i].Dispose();
                this._items.RemoveAt(i);
            }
            if (this._curItem != null)
            {
                this._curItem.isSelected = false;
                this._curItem = null;
            }
        }

        protected override void Init()
        {
            DailyTaskMode.Instance.ReqTaskInfo();
            this._title = base.FindInChild<UILabel>("title/name");
            this._title.text = LanguageManager.GetWord("Task.DailyTask");
            this._btnClose = base.FindInChild<Button>("btnClose");
            this._scrl = base.FindInChild<UIScrollView>("list");
            this._grid = base.FindInChild<UIGrid>("list/grid");
            DailyTaskItemView item = new DailyTaskItemView(base.FindChild("list/grid/item"), new Action<DailyTaskItemView>(this.OnClickItem));
            item.Update(null);
            this._items.Add(item);
            this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        private void OnChangeScene(int type, int v1, int v2, object data)
        {
            if (GlobalData.isInCopy)
            {
                this.CloseView();
            }
        }

        private void OnClick(GameObject go)
        {
            this.CloseView();
        }

        private void OnClickItem(DailyTaskItemView item)
        {
            if (this._curItem != item)
            {
                if (this._curItem != null)
                {
                    this._curItem.isSelected = false;
                }
                this._curItem = item;
                if (this._curItem != null)
                {
                    this._curItem.isSelected = true;
                }
            }
        }

        private void OnTaskEvent(int type, int v1, int v2, object data)
        {
            if (this._curItem != null)
            {
                this._curItem.isSelected = false;
                this._curItem = null;
            }
            this.UpdateView();
        }

        private void UpdateView()
        {
            while (this._items.Count < DailyTaskMode.Instance.taskList.Count)
            {
                GameObject go = NGUITools.AddChild(this._grid.gameObject, this._items[0].go);
                this._items.Add(new DailyTaskItemView(go, new Action<DailyTaskItemView>(this.OnClickItem)));
            }
            int num = 0;
            while (num < DailyTaskMode.Instance.taskList.Count)
            {
                this._items[num].Update(DailyTaskMode.Instance.taskList[num]);
                num++;
            }
            while (num < this._items.Count)
            {
                this._items[num].Update(null);
                num++;
            }
            this._grid.Reposition();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Task/DailyTaskPanel.assetbundle";
            }
        }
    }
}

