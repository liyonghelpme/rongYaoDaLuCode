﻿namespace com.game.module.Task
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.effect;
    using com.game.module.Story;
    using com.game.Public.Message;
    using com.game.sound;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;
    using UnityEngine;

    public class TaskControl : BaseControl<TaskControl>
    {
        private TaskAcceptMsg_6_2 _curTaskAccetpInfo;
        private TaskCommitMsg_6_4 _curTaskCommitInfo;
        private bool _finishTaskStoryPlaying;
        private bool _receiveTaskStoryPlaying;
        private TaskModel _taskMode;

        private void FinishTaskStoryCallback()
        {
            if (this._finishTaskStoryPlaying)
            {
                this._finishTaskStoryPlaying = false;
                this.ShowMainView();
            }
        }

        private void Fun_6_1(INetData data)
        {
            TaskInfoMsg_6_1 g__ = new TaskInfoMsg_6_1();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this._taskMode.NextMainTaskId = g__.nextId;
                this._taskMode.TaskDoing = g__.taskDoing;
            }
        }

        private void Fun_6_14(INetData data)
        {
            TaskRecruitMsg_6_14 g__ = new TaskRecruitMsg_6_14();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this._taskMode.LeftWantedTaskCount = g__.count;
                this._taskMode.WantedTaskList = g__.tasklist;
                this._taskMode.LeftRefreshTime = (int) g__.time;
            }
        }

        private void Fun_6_15(INetData data)
        {
            TaskRecruitRefreshMsg_6_15 g__ = new TaskRecruitRefreshMsg_6_15();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this._taskMode.WantedTaskList = g__.tasklist;
            }
        }

        private void Fun_6_16(INetData data)
        {
            TaskRecruitPushMsg_6_16 g__ = new TaskRecruitPushMsg_6_16();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this._taskMode.LeftWantedTaskCount = g__.count;
                for (int i = 0; i < this._taskMode.WantedTaskList.Count; i++)
                {
                    if (this._taskMode.WantedTaskList[i].id == this._taskMode.SelectedWantedTaskId)
                    {
                        this._taskMode.WantedTaskList[i] = g__.tasklist[0];
                        break;
                    }
                }
            }
        }

        private void Fun_6_17(INetData data)
        {
            TaskRecruitListPushMsg_6_17 g__ = new TaskRecruitListPushMsg_6_17();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                this._taskMode.WantedTaskList = g__.tasklist;
            }
        }

        private void Fun_6_2(INetData data)
        {
            this._curTaskAccetpInfo = new TaskAcceptMsg_6_2();
            this._curTaskAccetpInfo.read(data.GetMemoryStream());
            if (this._curTaskAccetpInfo.code != 0)
            {
                ErrorCodeManager.ShowError(this._curTaskAccetpInfo.code);
            }
            else
            {
                SysTask sysTaskVo = BaseDataMgr.instance.GetSysTaskVo((uint) this._curTaskAccetpInfo.taskId);
                if (this._curTaskAccetpInfo.taskId == this._taskMode.CurrentMainTaskVo.TaskId)
                {
                    TaskVo currentMainTaskVo = this._taskMode.CurrentMainTaskVo;
                    currentMainTaskVo.Statu = TaskStatu.StatuAccepted;
                    currentMainTaskVo.CanCommit = this._curTaskAccetpInfo.state == 1;
                    this._taskMode.CurrentMainTaskVo = currentMainTaskVo;
                }
                else if (sysTaskVo.type == 2)
                {
                    if (this._taskMode.CurrentSubTaskVo == null)
                    {
                        TaskVo vo2 = new TaskVo {
                            TaskId = this._curTaskAccetpInfo.taskId,
                            SysTaskVo = sysTaskVo,
                            Statu = TaskStatu.StatuAccepted,
                            CanCommit = this._curTaskAccetpInfo.state == 1
                        };
                        this._taskMode.CurrentSubTaskVo = vo2;
                        Singleton<WantedTaskView>.Instance.CloseView();
                    }
                    else if (this._curTaskAccetpInfo.taskId == this._taskMode.CurrentSubTaskVo.TaskId)
                    {
                        TaskVo currentSubTaskVo = this._taskMode.CurrentSubTaskVo;
                        currentSubTaskVo.Statu = TaskStatu.StatuAccepted;
                        currentSubTaskVo.CanCommit = this._curTaskAccetpInfo.state == 1;
                        this._taskMode.CurrentSubTaskVo = currentSubTaskVo;
                    }
                }
                Vector3 position = AppMap.Instance.me.GoBase.transform.position;
                position.y += 2.5f;
                EffectMgr.Instance.CreateMainEffect("20010", position, true, null, 0f);
                SoundMgr.Instance.PlayUIAudio("3016", 0f);
                if (Singleton<StoryControl>.Instance.PlayReceiveTaskStory(this._curTaskAccetpInfo.taskId, new ClosedCallback(this.ReceiveTaskStoryCallback)))
                {
                    this._receiveTaskStoryPlaying = true;
                    Singleton<MainView>.Instance.CloseView();
                }
            }
        }

        private void Fun_6_4(INetData data)
        {
            this._curTaskCommitInfo = new TaskCommitMsg_6_4();
            this._curTaskCommitInfo.read(data.GetMemoryStream());
            if (this._curTaskCommitInfo.code != 0)
            {
                ErrorCodeManager.ShowError(this._curTaskCommitInfo.code);
            }
            else
            {
                if (this._curTaskCommitInfo.taskId == this._taskMode.CurrentMainTaskVo.TaskId)
                {
                    SysTask sysTaskVo = BaseDataMgr.instance.GetSysTaskVo((uint) this._curTaskCommitInfo.nextId);
                    TaskVo vo = new TaskVo {
                        TaskId = this._curTaskCommitInfo.nextId,
                        SysTaskVo = sysTaskVo,
                        Statu = TaskStatu.StatuUnAccept
                    };
                    this._taskMode.CurrentMainTaskVo = vo;
                }
                else if ((this._taskMode.CurrentSubTaskVo != null) && (this._curTaskCommitInfo.taskId == this._taskMode.CurrentSubTaskVo.TaskId))
                {
                    this._taskMode.CurrentSubTaskVo = null;
                }
                Vector3 position = AppMap.Instance.me.GoBase.transform.position;
                position.y += 2.5f;
                EffectMgr.Instance.CreateMainEffect("20005", position, true, null, 0f);
                SoundMgr.Instance.PlayUIAudio("3017", 0f);
                if (Singleton<StoryControl>.Instance.PlayFinishTaskStory(this._curTaskCommitInfo.taskId, new ClosedCallback(this.FinishTaskStoryCallback)))
                {
                    this._finishTaskStoryPlaying = true;
                    Singleton<MainView>.Instance.CloseView();
                }
            }
        }

        private void Fun_6_5(INetData data)
        {
            TaskTrackInfoMsg_6_5 g__ = new TaskTrackInfoMsg_6_5();
            g__.read(data.GetMemoryStream());
            if (g__.taskId == this._taskMode.CurrentMainTaskVo.TaskId)
            {
                TaskVo currentMainTaskVo = this._taskMode.CurrentMainTaskVo;
                currentMainTaskVo.CanCommit = true;
                this._taskMode.CurrentMainTaskVo = currentMainTaskVo;
            }
            else if (g__.taskId == this._taskMode.CurrentSubTaskVo.TaskId)
            {
                TaskVo currentSubTaskVo = this._taskMode.CurrentSubTaskVo;
                currentSubTaskVo.CanCommit = true;
                this._taskMode.CurrentSubTaskVo = currentSubTaskVo;
            }
        }

        protected override void NetListener()
        {
            this._taskMode = Singleton<TaskModel>.Instance;
            AppNet.main.addCMD("1537", new NetMsgCallback(this.Fun_6_1));
            AppNet.main.addCMD("1538", new NetMsgCallback(this.Fun_6_2));
            AppNet.main.addCMD("1540", new NetMsgCallback(this.Fun_6_4));
            AppNet.main.addCMD("1541", new NetMsgCallback(this.Fun_6_5));
            AppNet.main.addCMD("1550", new NetMsgCallback(this.Fun_6_14));
            AppNet.main.addCMD("1551", new NetMsgCallback(this.Fun_6_15));
            AppNet.main.addCMD("1552", new NetMsgCallback(this.Fun_6_16));
            AppNet.main.addCMD("1553", new NetMsgCallback(this.Fun_6_17));
            this._taskMode.GetTaskInfo();
        }

        private void ReceiveTaskStoryCallback()
        {
            if (this._receiveTaskStoryPlaying)
            {
                this._receiveTaskStoryPlaying = false;
                this.ShowMainView();
            }
        }

        private void ShowMainView()
        {
            Singleton<MainView>.Instance.OpenView();
        }
    }
}

