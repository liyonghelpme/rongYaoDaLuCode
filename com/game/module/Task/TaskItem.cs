﻿namespace com.game.module.Task
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.u3d.bases.display;
    using System;
    using UnityEngine;

    public class TaskItem : MonoBehaviour
    {
        private Button _btnGuide;
        private UILabel _taskDescribeLabel;
        private UILabel _taskExpLabel;
        private UILabel _taskGoldLabel;
        private GameObject _taskReward;
        private UILabel _taskRewardLabel;
        private UILabel _taskTitleLabel;
        private TaskVo _taskVo;

        public void Dispose()
        {
            TaskItemPool.Instance.DeSpawn(base.gameObject.transform);
        }

        public void Init()
        {
            this._btnGuide = NGUITools.FindInChild<Button>(base.gameObject, "BtnGuide");
            this._taskTitleLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Content/TaskTitle");
            this._taskDescribeLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Content/TaskDescribe");
            this._taskRewardLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Content/TaskReward");
            this._taskExpLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Reward/ExpLabel");
            this._taskGoldLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Reward/GoldLabel");
            this._taskReward = NGUITools.FindChild(base.gameObject, "Reward");
            this._btnGuide.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._btnGuide.onClick, new UIWidgetContainer.VoidDelegate(this.OnGuideTask));
        }

        private bool MoveTo(BaseDisplay display)
        {
            if (display == null)
            {
                return false;
            }
            if (display.GoBase == null)
            {
                return false;
            }
            Transform transform = display.GoBase.transform;
            AppMap.Instance.clickVo.SaveClicker(display);
            AppMap.Instance.me.Controller.MoveTo(transform.position.x, transform.position.y, null);
            return true;
        }

        private void OnGuideTask(GameObject button)
        {
            string traceInfo;
            if (this._taskVo != null)
            {
                traceInfo = TaskUtil.GetTraceInfo(this._taskVo);
            }
            else
            {
                traceInfo = "n.10001.20000";
            }
            Singleton<TaskView>.Instance.CloseView();
        }

        public void SetData(TaskVo taskVo)
        {
            this._taskVo = taskVo;
            this._taskReward.SetActive(true);
            this._taskExpLabel.text = taskVo.SysTaskVo.exp + string.Empty;
            this._taskGoldLabel.text = taskVo.SysTaskVo.gold + string.Empty;
            this._taskTitleLabel.text = taskVo.SysTaskVo.name;
            this._taskDescribeLabel.text = LanguageManager.GetWord("MainLeftView.mainTask") + TaskUtil.GetTaskDescribe(taskVo);
            this._taskRewardLabel.text = "任务奖励：";
        }

        public void SetWantedTaskData()
        {
            this._taskVo = null;
            this._taskTitleLabel.text = "[支]悬赏任务";
            this._taskDescribeLabel.text = "完成悬赏任务可获得大量的经验和金币";
            this._taskReward.SetActive(false);
            this._taskRewardLabel.text = "剩余悬赏次数：" + Singleton<TaskModel>.Instance.LeftWantedTaskCount;
        }
    }
}

