﻿namespace com.game.module.Task
{
    using com.game.Public.PoolManager;
    using System;
    using UnityEngine;

    public class TaskItemPool
    {
        private const string _poolName = "TaskItem";
        private Transform _taskItemPrefab;
        public static TaskItemPool Instance;
        private SpawnPool pool;

        static TaskItemPool()
        {
            if (Instance == null)
            {
            }
            Instance = new TaskItemPool();
        }

        public void DeSpawn(Transform taskItem)
        {
            taskItem.parent = this.pool.transform;
            this.pool.Despawn(taskItem);
        }

        public void Init(Transform taskItemPrefab)
        {
            this._taskItemPrefab = taskItemPrefab;
            this.pool = com.game.Public.PoolManager.PoolManager.Pools.Create("TaskItem");
            this.pool.group.parent = com.game.Public.PoolManager.PoolManager.PoolParent;
            this.pool.group.localPosition = new Vector3(0f, 0f, 0f);
            this.pool.group.localRotation = Quaternion.identity;
            PrefabPool prefabPool = new PrefabPool(this._taskItemPrefab) {
                preloadAmount = 5
            };
            this.pool.CreatePrefabPool(prefabPool);
        }

        public Transform SpawnTaskItem()
        {
            return this.pool.Spawn(this._taskItemPrefab, Vector3.zero, Quaternion.identity);
        }
    }
}

