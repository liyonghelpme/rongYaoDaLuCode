﻿namespace com.game.module.Task
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;

    public class TaskModel : BaseMode<TaskModel>
    {
        private TaskVo _currentMainTaskVo;
        private TaskVo _currentSubTaskVo;
        private int _leftRefreshTime;
        private int _leftWantedTaskCount;
        private ulong _nextMainTaskId;
        private List<PTaskDoing> _taskDoing;
        private List<PRecruit> _wantedTaskList;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map1B;
        public const int EventLeftRefreshTimeUpdate = 5;
        public const int EventMainTaskAutoGuide = 7;
        public const int EventMainTaskIdUpdate = 1;
        public const int EventMainTaskStatuUpdate = 6;
        public const int EventSubTaskIdUpdate = 2;
        public const int EventWantedCountUpdate = 4;
        public const int EventWantedTaskUpdate = 3;
        public int SelectedWantedTaskId;
        public uint TaskCopyMapId;
        public const int TypeMain = 1;
        public const int TypeSub = 2;

        public void AccetpTask(ulong taskId, uint npcId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_6.write_6_2(msdata, taskId, npcId);
            AppNet.gameNet.send(msdata, 6, 2);
        }

        public void AutoGuideMainTask()
        {
            base.DataUpdate(7);
        }

        public void CommitTask(ulong taskId, uint npcId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_6.write_6_4(msdata, taskId, npcId);
            AppNet.gameNet.send(msdata, 6, 4);
        }

        public void GetTaskInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_6.write_6_1(msdata);
            AppNet.gameNet.send(msdata, 6, 1);
        }

        public void GetWantedTaskInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_6.write_6_14(msdata);
            AppNet.gameNet.send(msdata, 6, 14);
        }

        public void RefreshWantedTask()
        {
            MemoryStream msdata = new MemoryStream();
            Module_6.write_6_15(msdata);
            AppNet.gameNet.send(msdata, 6, 15);
        }

        private void SetTaskCopyId()
        {
            char[] separator = new char[] { '.' };
            string[] strArray = TaskUtil.GetTraceInfo(this._currentMainTaskVo).Split(separator);
            string key = strArray[0];
            if (key != null)
            {
                int num;
                if (<>f__switch$map1B == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(2);
                    dictionary.Add("n", 0);
                    dictionary.Add("fb", 1);
                    <>f__switch$map1B = dictionary;
                }
                if (<>f__switch$map1B.TryGetValue(key, out num))
                {
                    if (num == 0)
                    {
                        this.TaskCopyMapId = 0;
                    }
                    else if (num == 1)
                    {
                        this.TaskCopyMapId = uint.Parse(strArray[2]);
                    }
                }
            }
        }

        public TaskVo CurrentMainTaskVo
        {
            get
            {
                return this._currentMainTaskVo;
            }
            set
            {
                this._currentMainTaskVo = value;
                this.SetTaskCopyId();
                base.DataUpdate(1);
                base.DataUpdate(6);
            }
        }

        public TaskVo CurrentSubTaskVo
        {
            get
            {
                return this._currentSubTaskVo;
            }
            set
            {
                this._currentSubTaskVo = value;
                base.DataUpdate(2);
            }
        }

        public int LeftRefreshTime
        {
            get
            {
                return this._leftRefreshTime;
            }
            set
            {
                this._leftRefreshTime = value;
                base.DataUpdate(5);
            }
        }

        public int LeftWantedTaskCount
        {
            get
            {
                return this._leftWantedTaskCount;
            }
            set
            {
                this._leftWantedTaskCount = value;
                base.DataUpdate(4);
            }
        }

        public ulong NextMainTaskId
        {
            get
            {
                return this._nextMainTaskId;
            }
            set
            {
                this._nextMainTaskId = value;
            }
        }

        public List<PTaskDoing> TaskDoing
        {
            get
            {
                return this._taskDoing;
            }
            set
            {
                this._taskDoing = value;
                foreach (PTaskDoing doing in this._taskDoing)
                {
                    SysTask sysTaskVo = BaseDataMgr.instance.GetSysTaskVo(doing.taskId);
                    if (sysTaskVo.type == 1)
                    {
                        TaskVo vo = new TaskVo {
                            TaskId = doing.taskId,
                            SysTaskVo = sysTaskVo,
                            Statu = TaskStatu.StatuAccepted,
                            CanCommit = doing.state != 0
                        };
                        this.CurrentMainTaskVo = vo;
                    }
                    else if (sysTaskVo.type == 2)
                    {
                        TaskVo vo2 = new TaskVo {
                            TaskId = doing.taskId,
                            SysTaskVo = sysTaskVo,
                            Statu = TaskStatu.StatuAccepted,
                            CanCommit = doing.state != 0
                        };
                        this.CurrentSubTaskVo = vo2;
                    }
                }
                if (this.CurrentMainTaskVo == null)
                {
                    SysTask task2 = BaseDataMgr.instance.GetSysTaskVo((uint) this.NextMainTaskId);
                    if ((task2 != null) && (task2.type == 1))
                    {
                        TaskVo vo3 = new TaskVo {
                            TaskId = this.NextMainTaskId,
                            SysTaskVo = task2,
                            Statu = TaskStatu.StatuUnAccept
                        };
                        this.CurrentMainTaskVo = vo3;
                    }
                }
            }
        }

        public List<PRecruit> WantedTaskList
        {
            get
            {
                return this._wantedTaskList;
            }
            set
            {
                this._wantedTaskList = value;
                base.DataUpdate(3);
            }
        }
    }
}

