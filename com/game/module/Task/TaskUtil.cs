﻿namespace com.game.module.Task
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.u3d.bases.display;
    using com.u3d.bases.display.effect;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class TaskUtil
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map1C;

        private static string GetFindCopyTaskDescribe(string[] items)
        {
            SysMapVo mapVo = BaseDataMgr.instance.GetMapVo(uint.Parse(items[2]));
            return string.Format("{0}{1}", "通关", mapVo.name);
        }

        private static string GetFindNpcTaskDescribe(string[] items)
        {
            SysNpcVo npcVoById = BaseDataMgr.instance.GetNpcVoById(uint.Parse(items[2]));
            return string.Format("{0}{1}", "找到", npcVoById.name);
        }

        private static MapPointDisplay GetMapPointDisplay(string[] items)
        {
            return AppMap.Instance.MapPointDisplay;
        }

        public static string GetTaskDescribe(TaskVo taskVo)
        {
            int num;
            string traceInfo = GetTraceInfo(taskVo);
            string str2 = string.Empty;
            char[] separator = new char[] { '.' };
            string[] items = traceInfo.Split(separator);
            string key = items[0];
            if (key == null)
            {
                return str2;
            }
            if (<>f__switch$map1C == null)
            {
                Dictionary<string, int> dictionary = new Dictionary<string, int>(2);
                dictionary.Add("n", 0);
                dictionary.Add("fb", 1);
                <>f__switch$map1C = dictionary;
            }
            if (!<>f__switch$map1C.TryGetValue(key, out num))
            {
                return str2;
            }
            if (num == 0)
            {
                return GetFindNpcTaskDescribe(items);
            }
            if (num != 1)
            {
                return str2;
            }
            return GetFindCopyTaskDescribe(items);
        }

        public static BaseDisplay GetTaskDisplay(string trace)
        {
            return null;
        }

        public static int GetTaskTriggerType(TaskVo taskVo)
        {
            int num = 0;
            if (taskVo.Statu == TaskStatu.StatuAccepted)
            {
                num = !taskVo.CanCommit ? 1 : 2;
            }
            return num;
        }

        public static string GetTraceInfo(TaskVo taskVo)
        {
            string str = string.Empty;
            switch (taskVo.Statu)
            {
                case TaskStatu.StatuUnAccept:
                    return taskVo.SysTaskVo.trace_accept;

                case TaskStatu.StatuAccepted:
                    if (!taskVo.CanCommit)
                    {
                        return taskVo.SysTaskVo.trace_uncom;
                    }
                    return taskVo.SysTaskVo.trace_com;

                case TaskStatu.StatuFinished:
                    return taskVo.SysTaskVo.trace_com;
            }
            return str;
        }
    }
}

