﻿namespace com.game.module.Task
{
    using com.game.module.core;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class TaskView : BaseView<TaskView>
    {
        private UIToggle _btnAcceptedTask;
        private UIToggle _btnCanAcceptTask;
        private Button _btnClose;
        private UIGrid _itemsParent;
        private UILabel _labelAcceptedTask;
        private UILabel _labelCanAcceptedTask;
        private GameObject _taskItemGameObject;
        private List<TaskItem> _taskItems;
        private TaskModel _taskModel;

        private void CloseOnClick(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            UIToggle.current = this._btnAcceptedTask;
            this.UpdateTaskInfo();
        }

        protected override void Init()
        {
            this._btnAcceptedTask = base.FindInChild<UIToggle>("Top/BtnAccepted");
            this._btnCanAcceptTask = base.FindInChild<UIToggle>("Top/BtnCanAccept");
            this._labelAcceptedTask = base.FindInChild<UILabel>("Top/BtnAccepted/label");
            this._labelCanAcceptedTask = base.FindInChild<UILabel>("Top/BtnCanAccept/label");
            this._btnClose = base.FindInChild<Button>("TopRight/BtnClose");
            this._btnClose.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._btnClose.onClick, new UIWidgetContainer.VoidDelegate(this.CloseOnClick));
            this._taskItemGameObject = Tools.find(this.gameObject, "Item").gameObject;
            TaskItemPool.Instance.Init(this._taskItemGameObject.transform);
            this._itemsParent = base.FindInChild<UIGrid>("TaskItemPanel/Items");
            this._taskItems = new List<TaskItem>();
            this._taskModel = Singleton<TaskModel>.Instance;
            UIToggle.current = this._btnAcceptedTask;
            this._btnAcceptedTask.onChange.Add(new EventDelegate(new EventDelegate.Callback(this.TopOnClick)));
            this._btnCanAcceptTask.onChange.Add(new EventDelegate(new EventDelegate.Callback(this.TopOnClick)));
            this.InitText();
        }

        private void InitText()
        {
        }

        private void TopOnClick()
        {
            if (UIToggle.current.value)
            {
                this.UpdateTaskInfo();
            }
        }

        private void UpdateAcceptedTaskInfo()
        {
            for (int i = this._taskItems.Count - 1; i >= 0; i--)
            {
                TaskItem item = this._taskItems[i];
                this._taskItems.Remove(item);
                item.Dispose();
            }
            int num2 = 0;
            TaskVo currentMainTaskVo = this._taskModel.CurrentMainTaskVo;
            if ((currentMainTaskVo != null) && (currentMainTaskVo.Statu == TaskStatu.StatuAccepted))
            {
                GameObject gameObject = TaskItemPool.Instance.SpawnTaskItem().gameObject;
                TaskItem component = gameObject.GetComponent<TaskItem>();
                if (component == null)
                {
                    component = gameObject.AddComponent<TaskItem>();
                    component.Init();
                }
                gameObject.SetActive(false);
                gameObject.SetActive(true);
                component.SetData(currentMainTaskVo);
                this._taskItems.Add(component);
                gameObject.transform.parent = this._itemsParent.transform;
                gameObject.transform.localPosition = new Vector3(0f, (float) num2, 0f);
                gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                num2 -= 0xc3;
            }
        }

        private void UpdateCanAcceptTaskInfo()
        {
            for (int i = this._taskItems.Count - 1; i >= 0; i--)
            {
                TaskItem item = this._taskItems[i];
                this._taskItems.Remove(item);
                item.Dispose();
            }
            int num2 = 0;
            TaskVo currentMainTaskVo = this._taskModel.CurrentMainTaskVo;
            if ((currentMainTaskVo != null) && (currentMainTaskVo.Statu == TaskStatu.StatuUnAccept))
            {
                GameObject gameObject = TaskItemPool.Instance.SpawnTaskItem().gameObject;
                TaskItem component = gameObject.GetComponent<TaskItem>();
                if (component == null)
                {
                    component = gameObject.AddComponent<TaskItem>();
                    component.Init();
                }
                gameObject.SetActive(false);
                gameObject.SetActive(true);
                component.SetData(currentMainTaskVo);
                this._taskItems.Add(component);
                gameObject.transform.parent = this._itemsParent.transform;
                gameObject.transform.localPosition = new Vector3(0f, (float) num2, 0f);
                gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                num2 -= 0xc3;
            }
        }

        private void UpdateTaskInfo()
        {
            if (UIToggle.current.Equals(this._btnAcceptedTask))
            {
                this.UpdateAcceptedTaskInfo();
            }
            else if (UIToggle.current.Equals(this._btnCanAcceptTask))
            {
                this.UpdateCanAcceptTaskInfo();
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Task/TaskView.assetbundle";
            }
        }
    }
}

