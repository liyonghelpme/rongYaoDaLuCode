﻿namespace com.game.module.Task
{
    using com.game.data;
    using System;
    using System.Collections.Generic;

    public class TaskVo
    {
        private int _targetNpcId;
        public bool CanCommit;
        public TaskStatu Statu;
        public SysTask SysTaskVo;
        public ulong TaskId;

        public List<uint> RelatedNpcList
        {
            get
            {
                List<uint> list = new List<uint>();
                if (this.SysTaskVo.trace_accept.IndexOf("n", StringComparison.Ordinal) == 0)
                {
                    char[] separator = new char[] { '.' };
                    list.Add(uint.Parse(this.SysTaskVo.trace_accept.Split(separator)[2]));
                }
                if (this.SysTaskVo.trace_uncom.IndexOf("n", StringComparison.Ordinal) == 0)
                {
                    char[] chArray2 = new char[] { '.' };
                    list.Add(uint.Parse(this.SysTaskVo.trace_uncom.Split(chArray2)[2]));
                }
                if (this.SysTaskVo.trace_com.IndexOf("n", StringComparison.Ordinal) == 0)
                {
                    char[] chArray3 = new char[] { '.' };
                    list.Add(uint.Parse(this.SysTaskVo.trace_com.Split(chArray3)[2]));
                }
                return list;
            }
        }

        public int TargetNpcId
        {
            get
            {
                switch (this.Statu)
                {
                    case TaskStatu.StatuUnAccept:
                    {
                        if (this.SysTaskVo.trace_accept.IndexOf("n", StringComparison.Ordinal) != 0)
                        {
                            break;
                        }
                        char[] separator = new char[] { '.' };
                        return int.Parse(this.SysTaskVo.trace_accept.Split(separator)[2]);
                    }
                    case TaskStatu.StatuAccepted:
                    {
                        if (!this.CanCommit)
                        {
                            if (this.SysTaskVo.trace_uncom.IndexOf("n", StringComparison.Ordinal) == 0)
                            {
                                char[] chArray3 = new char[] { '.' };
                                return int.Parse(this.SysTaskVo.trace_uncom.Split(chArray3)[2]);
                            }
                            break;
                        }
                        if (this.SysTaskVo.trace_com.IndexOf("n", StringComparison.Ordinal) != 0)
                        {
                            break;
                        }
                        char[] chArray2 = new char[] { '.' };
                        return int.Parse(this.SysTaskVo.trace_com.Split(chArray2)[2]);
                    }
                }
                return 0;
            }
        }

        public int TaskIcon
        {
            get
            {
                switch (this.Statu)
                {
                    case TaskStatu.StatuUnAccept:
                        if (this.SysTaskVo.trace_accept.IndexOf("n", StringComparison.Ordinal) != 0)
                        {
                            break;
                        }
                        return 1;

                    case TaskStatu.StatuAccepted:
                        if (!this.CanCommit)
                        {
                            if (this.SysTaskVo.trace_uncom.IndexOf("n", StringComparison.Ordinal) == 0)
                            {
                                return 1;
                            }
                            break;
                        }
                        if (this.SysTaskVo.trace_com.IndexOf("n", StringComparison.Ordinal) != 0)
                        {
                            break;
                        }
                        return 1;
                }
                return 0;
            }
        }
    }
}

