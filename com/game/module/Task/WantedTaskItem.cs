﻿namespace com.game.module.Task
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class WantedTaskItem : MonoBehaviour
    {
        private Button _acceptTaskButton;
        private int _color;
        private UILabel _taskDetail;
        private UILabel _taskExpRewardLabel;
        private UILabel _taskGoldRewardLabel;
        private TaskModel _taskModel;
        private SysTask _taskVo;
        private UILabel _titleLabel;

        public void Init()
        {
            this._titleLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Title/Label");
            this._taskDetail = NGUITools.FindInChild<UILabel>(base.gameObject, "Content/Label");
            this._taskExpRewardLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Reward/ExpLabel");
            this._taskGoldRewardLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "Reward/GoldLabel");
            this._acceptTaskButton = NGUITools.FindInChild<Button>(base.gameObject, "ButtonAccept/");
            this._acceptTaskButton.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._acceptTaskButton.onClick, new UIWidgetContainer.VoidDelegate(this.OnAcceptTask));
            this._acceptTaskButton.label.text = LanguageManager.GetWord("WantedTaskItem.AcceptTask");
            this._taskModel = Singleton<TaskModel>.Instance;
        }

        private void OnAcceptTask(GameObject button)
        {
            this._taskModel.SelectedWantedTaskId = this._taskVo.taskId;
            this._taskModel.AccetpTask((ulong) this._taskVo.taskId, 0);
        }

        public void SetData(SysTask taskVo, int color)
        {
            this._color = color;
            this._taskVo = taskVo;
            this._titleLabel.text = taskVo.name;
            this._taskExpRewardLabel.text = taskVo.exp.ToString();
            this._taskGoldRewardLabel.text = taskVo.gold.ToString();
            TaskVo vo = new TaskVo {
                Statu = TaskStatu.StatuAccepted,
                TaskId = (ulong) taskVo.taskId,
                SysTaskVo = taskVo,
                CanCommit = false
            };
            this._taskDetail.text = TaskUtil.GetTaskDescribe(vo);
            switch (this._color)
            {
                case 1:
                    this._taskDetail.color = Color.white;
                    break;

                case 2:
                    this._taskDetail.color = Color.green;
                    break;

                case 3:
                    this._taskDetail.color = Color.blue;
                    break;

                case 4:
                    this._taskDetail.color = Color.cyan;
                    break;

                case 5:
                    this._taskDetail.color = Color.magenta;
                    break;
            }
        }
    }
}

