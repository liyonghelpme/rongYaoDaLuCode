﻿namespace com.game.module.Task
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.SystemData;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class WantedTaskView : BaseView<WantedTaskView>
    {
        private Button _closeButton;
        private List<WantedTaskItem> _items;
        private UILabel _leftRefreshTimeLabel;
        private UILabel _processInfoLabel;
        private Button _refreshButton;
        private TaskModel _taskModel;

        public override void CancelUpdateHandler()
        {
            this._taskModel.dataUpdated -= new DataUpateHandler(this.WantedTaskDataUpdate);
        }

        public override void CloseView()
        {
            base.CloseView();
            vp_Timer.CancelAll("UpdateWantedLeftTime");
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.UpdateWantedTaskInfo();
        }

        protected override void Init()
        {
            this._taskModel = Singleton<TaskModel>.Instance;
            this._closeButton = base.FindChild("ButtonClose").GetComponent<Button>();
            this._closeButton.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._closeButton.onClick, new UIWidgetContainer.VoidDelegate(this.OnCloseButtonClick));
            this._processInfoLabel = base.FindChild("Statu/Process").GetComponent<UILabel>();
            this._leftRefreshTimeLabel = base.FindChild("Statu/LeftRefreshTime").GetComponent<UILabel>();
            this._refreshButton = base.FindChild("ButtonRefresh").GetComponent<Button>();
            this._refreshButton.label.text = LanguageManager.GetWord("WantedTaskView.Refresh");
            this._refreshButton.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._refreshButton.onClick, new UIWidgetContainer.VoidDelegate(this.OnRefreshWantedTask));
            this._items = new List<WantedTaskItem>();
            this._taskModel.GetWantedTaskInfo();
            for (int i = 0; i < 3; i++)
            {
                string path = "Item" + (i + 1);
                WantedTaskItem item = base.FindChild(path).AddComponent<WantedTaskItem>();
                item.Init();
                this._items.Add(item);
            }
        }

        private void OnCloseButtonClick(GameObject button)
        {
            this.CloseView();
        }

        private void OnRefreshWantedTask(GameObject button)
        {
        }

        private void RefreshWantedTask()
        {
            this._taskModel.RefreshWantedTask();
        }

        public override void RegisterUpdateHandler()
        {
            this._taskModel.dataUpdated += new DataUpateHandler(this.WantedTaskDataUpdate);
        }

        private void ShowLeftRefreshTime()
        {
            int num = (0x708 + this._taskModel.LeftRefreshTime) - ServerTime.Instance.Timestamp;
            int num2 = num / 60;
            int num3 = num % 60;
            string word = LanguageManager.GetWord("WantedTaskView.LeftRefreshTime");
            object[] objArray1 = new object[] { word, " ", num2, ":", num3 };
            this._leftRefreshTimeLabel.text = string.Concat(objArray1);
        }

        public void UpdateLeftTime()
        {
            this.ShowLeftRefreshTime();
            vp_Timer.CancelAll("UpdateWantedLeftTime");
            vp_Timer.In(1f, new vp_Timer.Callback(this.UpdateWantedLeftTime), 0x186a0, 1f, null);
        }

        private void UpdateProcess()
        {
            string word = LanguageManager.GetWord("WantedTaskView.Process");
            object[] objArray1 = new object[] { word, " ", this._taskModel.LeftWantedTaskCount, "/15" };
            this._processInfoLabel.text = string.Concat(objArray1);
        }

        private void UpdateWantedLeftTime()
        {
            this.ShowLeftRefreshTime();
        }

        private void UpdateWantedTaskInfo()
        {
            if (this._taskModel.WantedTaskList != null)
            {
                for (int i = 0; i < this._taskModel.WantedTaskList.Count; i++)
                {
                    uint id = this._taskModel.WantedTaskList[i].id;
                    SysTask sysTaskVo = BaseDataMgr.instance.GetSysTaskVo(id);
                    byte color = this._taskModel.WantedTaskList[i].color;
                    this._items[i].SetData(sysTaskVo, color);
                }
            }
            this.UpdateProcess();
            this.UpdateLeftTime();
        }

        private void WantedTaskDataUpdate(object sender, int code)
        {
            switch (code)
            {
                case 3:
                    this.UpdateWantedTaskInfo();
                    break;

                case 4:
                    this.UpdateProcess();
                    break;

                case 5:
                    this.UpdateLeftTime();
                    break;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/WantedTask/WantedTaskView.assetbundle";
            }
        }
    }
}

