﻿namespace com.game.module.TweenManager
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class TweenParams<T, V> where T: UITweener
    {
        public List<EventDelegate> OnFinished;

        public TweenParams()
        {
            this.OnFinished = new List<EventDelegate>();
        }

        public float Delay { get; set; }

        public float Duration { get; set; }

        public V FromValue { get; set; }

        public int GroupIndex { get; set; }

        public UITweener.Method Method { get; set; }

        public GameObject Target { get; set; }

        public V ToValue { get; set; }

        public T TweenType { get; set; }
    }
}

