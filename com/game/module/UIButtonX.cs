﻿namespace com.game.module
{
    using System;
    using UnityEngine;

    [AddComponentMenu("NGUI/UI/ButtonX")]
    public class UIButtonX : UIWidgetContainer
    {
        public GameObject target;

        private void Awake()
        {
            NGUITools.SetActive(this.target, false);
        }

        private void OnPress(bool pressed)
        {
            if (this.target.activeSelf != pressed)
            {
                NGUITools.SetActive(this.target, pressed);
            }
        }
    }
}

