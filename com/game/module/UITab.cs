﻿namespace com.game.module
{
    using System;
    using UnityEngine;

    [AddComponentMenu("NGUI/UI/Tab"), ExecuteInEditMode]
    public class UITab : MonoBehaviour
    {
        public UITabController controller;
        public bool first;
        private UILabel label;
        public GameObject target;

        private void Awake()
        {
            if (this.controller == null)
            {
                this.controller = NGUITools.FindInParents<UITabController>(base.gameObject);
            }
            this.label = base.GetComponentInChildren<UILabel>();
            this.setActive(false);
            if (this.controller != null)
            {
                this.controller.addTab(this);
                if (this.first)
                {
                    this.controller.switchTab(this);
                }
            }
        }

        private void OnPress(bool pressed)
        {
            this.controller.switchTab(this);
        }

        public void setActive(bool flag)
        {
            NGUITools.SetActive(this.target, flag);
            if (this.label != null)
            {
                this.label.color = !flag ? NGUITools.ParseColor("EAAE8F", 0) : Color.white;
            }
        }
    }
}

