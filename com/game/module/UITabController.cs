﻿namespace com.game.module
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [AddComponentMenu("NGUI/UI/Tab Controller")]
    public class UITabController : MonoBehaviour
    {
        private List<UITab> tabs = new List<UITab>();
        public UISprite titleSprite;

        public void addTab(UITab tab)
        {
            if (!this.tabs.Contains(tab))
            {
                this.tabs.Add(tab);
            }
        }

        public void switchTab(UITab tab)
        {
            foreach (UITab tab2 in this.tabs)
            {
                if (tab2 == tab)
                {
                    tab2.setActive(true);
                    if (this.titleSprite != null)
                    {
                        this.titleSprite.spriteName = tab2.gameObject.name;
                    }
                }
                else
                {
                    tab2.setActive(false);
                }
            }
        }

        public void switchTab(int n)
        {
            if ((n >= 0) && (n < this.tabs.Count))
            {
                this.switchTab(this.tabs[n]);
            }
        }
    }
}

