﻿namespace com.game.module.UpdateAnnounce
{
    using com.game;
    using com.game.module.core;
    using System;

    public class UpdateAnnounceControl : BaseControl<UpdateAnnounceControl>
    {
        public void GetAnnounce()
        {
            AppNet.main.GetAnnounce(Singleton<UpdateAnnounceMode>.Instance.URL);
        }

        public void ShowAnnounce()
        {
            if (Singleton<UpdateAnnounceMode>.Instance.CanShowAnnounce)
            {
                Singleton<UpdateAnnounceView>.Instance.ShowWindow(Singleton<UpdateAnnounceMode>.Instance.Date, Singleton<UpdateAnnounceMode>.Instance.Content);
            }
        }
    }
}

