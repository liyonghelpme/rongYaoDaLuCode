﻿namespace com.game.module.UpdateAnnounce
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class UpdateAnnounceMode : BaseMode<UpdateAnnounceMode>
    {
        private string content;
        private string curAnnounce;
        private string date;
        private string lastAnnounce = PlayerPrefs.GetString("Announce");
        private string pageName = "xml/updated_board.xml";

        private void ParseAnnounce()
        {
            XMLNode node = XMLParser.Parse(this.curAnnounce);
            this.date = node.GetValue("note>0>date>0>_text");
            this.content = node.GetValue("note>0>content>0>_text");
        }

        public void SaveAnnounce(string announce)
        {
            this.curAnnounce = announce;
            this.ParseAnnounce();
            PlayerPrefs.SetString("Announce", announce);
        }

        public bool CanShowAnnounce
        {
            get
            {
                return !this.lastAnnounce.Equals(this.curAnnounce);
            }
        }

        public string Content
        {
            get
            {
                return this.content;
            }
        }

        public string Date
        {
            get
            {
                return this.date;
            }
        }

        private string serverHost
        {
            get
            {
                if ((Application.platform != RuntimePlatform.WindowsEditor) && (Application.platform != RuntimePlatform.WindowsPlayer))
                {
                    return "http://gateway.mxqy.4399sy.com/";
                }
                return "http://172.16.10.140/";
            }
        }

        public string URL
        {
            get
            {
                int num = UnityEngine.Random.Range(1, 0x5f5e0ff);
                return (this.serverHost + this.pageName + "?" + num.ToString());
            }
        }
    }
}

