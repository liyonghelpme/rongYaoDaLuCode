﻿namespace com.game.module.UpdateAnnounce
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class UpdateAnnounceView : BaseView<UpdateAnnounceView>
    {
        private string announce;
        private Button btnClose;
        private Button btnOk;
        private string date;
        private UILabel labAnnounce;
        private UILabel labTime;

        private void CloseOnClick(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.labTime.text = this.date;
            this.labAnnounce.text = this.announce;
        }

        protected override void Init()
        {
            this.btnClose = base.FindInChild<Button>("btn_close");
            this.btnOk = base.FindInChild<Button>("btn_ok");
            this.labTime = base.FindInChild<UILabel>("center/updatetime/time");
            this.labAnnounce = base.FindInChild<UILabel>("center/content/announce");
            this.btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.CloseOnClick);
            this.btnOk.onClick = new UIWidgetContainer.VoidDelegate(this.OkOnClick);
        }

        private void OkOnClick(GameObject go)
        {
            this.CloseView();
        }

        public void ShowWindow(string date, string announce)
        {
            this.date = date;
            this.announce = announce;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/UpdateAnnounce/UpdateAnnounceView.assetbundle";
            }
        }
    }
}

