﻿namespace com.game.module.Utility
{
    using com.game.utils;
    using System;
    using System.Collections.Generic;

    public class AwardInfo
    {
        public List<int> counts = new List<int>();
        public Dictionary<int, int> currencyDic = new Dictionary<int, int>();
        public List<uint> ids = new List<uint>();

        public int GetCurrency(int type)
        {
            return (((this.currencyDic == null) || !this.currencyDic.ContainsKey(type)) ? 0 : this.currencyDic[type]);
        }

        public static AwardInfo Parse(string str)
        {
            AwardInfo info = new AwardInfo();
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(str);
            int num = arrayStringToInt.Length / 2;
            int index = 0;
            for (int i = 0; i < num; i++)
            {
                index = 2 * i;
                if (arrayStringToInt[index] < 0x186a0)
                {
                    if (info.currencyDic.ContainsKey(arrayStringToInt[index]))
                    {
                        Dictionary<int, int> dictionary;
                        int num4;
                        num4 = dictionary[num4];
                        (dictionary = info.currencyDic)[num4 = arrayStringToInt[index]] = num4 + arrayStringToInt[index + 1];
                    }
                    else
                    {
                        info.currencyDic.Add(arrayStringToInt[index], arrayStringToInt[index + 1]);
                    }
                }
                else
                {
                    info.ids.Add((uint) arrayStringToInt[index]);
                    info.counts.Add(arrayStringToInt[index + 1]);
                }
            }
            return info;
        }

        public int gold
        {
            get
            {
                return this.GetCurrency(3);
            }
        }

        public int stone
        {
            get
            {
                return this.GetCurrency(1);
            }
        }
    }
}

