﻿namespace com.game.module.VIP
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class VIPControl : BaseControl<VIPControl>
    {
        private void Fun_32_1(INetData data)
        {
            VipInfoMsg_32_1 msg = new VipInfoMsg_32_1();
            msg.read(data.GetMemoryStream());
            Singleton<VIPMode>.Instance.Update(msg);
        }

        private void Fun_32_2(INetData data)
        {
            VipGetRewardMsg_32_2 g__ = new VipGetRewardMsg_32_2();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("VIP.SuccessToGetAward"));
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("8193", new NetMsgCallback(this.Fun_32_1));
            AppNet.main.addCMD("8194", new NetMsgCallback(this.Fun_32_2));
        }
    }
}

