﻿namespace com.game.module.VIP
{
    using com.game;
    using com.game.module.core;
    using PCustomDataType;
    using Proto;
    using System;
    using System.IO;

    public class VIPMode : BaseMode<VIPMode>
    {
        private int _awardState;
        private int _numFilled;
        private int _vip = -1;

        public bool HasGotAward(int level)
        {
            int num = ((int) 1) << (level - 1);
            return ((this._awardState & num) > 0);
        }

        public void ReqGetAward(int level)
        {
            MemoryStream msdata = new MemoryStream();
            Module_32.write_32_2(msdata, (byte) level);
            AppNet.gameNet.send(msdata, 0x20, 2);
        }

        public void ReqInitVipInfo()
        {
            MemoryStream msdata = new MemoryStream();
            Module_32.write_32_1(msdata);
            AppNet.gameNet.send(msdata, 0x20, 1);
        }

        public void Update(VipInfoMsg_32_1 msg)
        {
            this._vip = msg.vip;
            this._numFilled = (int) msg.totalDiam;
            this._awardState = 0;
            foreach (PVipRewardGetInfo info in msg.rewardInfoList)
            {
                if (info.state > 0)
                {
                    this._awardState += ((int) 1) << (info.vipLevel - 1);
                }
            }
            VIPUtil.UpdateVipLevel(this._vip);
            Singleton<VIPMode>.Instance.DataUpdate(3);
        }

        public int AwardState
        {
            get
            {
                return this._awardState;
            }
            set
            {
                if (this._awardState != value)
                {
                    this._awardState = value;
                    Singleton<VIPMode>.Instance.DataUpdate(2);
                }
            }
        }

        public int numFilled
        {
            get
            {
                return this._numFilled;
            }
            set
            {
                this._numFilled = value;
                Singleton<VIPMode>.Instance.DataUpdate(3);
            }
        }

        public int vip
        {
            get
            {
                return this._vip;
            }
            set
            {
                if (value != this._vip)
                {
                    this._vip = value;
                    VIPUtil.UpdateVipLevel(this._vip);
                    Singleton<VIPMode>.Instance.DataUpdate(1);
                }
            }
        }

        public class Event
        {
            public const int AWARD = 2;
            public const int LEVEL = 1;
            public const int UPDATE = 3;
        }
    }
}

