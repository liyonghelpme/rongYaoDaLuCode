﻿namespace com.game.module.VIP
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.Utility;
    using com.game.utils;
    using System;
    using System.Collections.Generic;

    public class VIPUtil
    {
        private static Dictionary<uint, AwardInfo> _awardDic;

        public static AwardInfo GetAwardInfo(uint vipLevel)
        {
            if (_awardDic == null)
            {
                _awardDic = new Dictionary<uint, AwardInfo>();
            }
            if (!_awardDic.ContainsKey(vipLevel))
            {
                SysVipInfoVo vIPInfo = BaseDataMgr.instance.GetVIPInfo(vipLevel);
                if (vIPInfo != null)
                {
                    _awardDic.Add(vipLevel, AwardInfo.Parse(vIPInfo.awards));
                }
            }
            return (!_awardDic.ContainsKey(vipLevel) ? null : _awardDic[vipLevel]);
        }

        public static void UpdateVipLevel(int v)
        {
            foreach (SysVipDroitVo vo in BaseDataMgr.instance.GetVipDroits((uint) v))
            {
                switch (vo.type)
                {
                    case 1:
                        VarMgr.NumBuyVigourMax = vo.value;
                        break;

                    case 3:
                        VarMgr.numBuyGoldMax = vo.value;
                        break;

                    case 5:
                        VarMgr.NumBuySkillPointMax = vo.value;
                        break;

                    case 6:
                        VarMgr.NumBuyArenalMax = vo.value;
                        break;

                    case 7:
                        VarMgr.VipSweep = vo.value > 0;
                        break;

                    case 8:
                        VarMgr.VipSweep10 = vo.value > 0;
                        break;

                    case 9:
                        VarMgr.numBuyEnterEliteDugeon = vo.value;
                        break;
                }
            }
        }
    }
}

