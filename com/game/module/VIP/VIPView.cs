﻿namespace com.game.module.VIP
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using System;
    using UnityEngine;

    public class VIPView : BaseView<VIPView>
    {
        private Button _btnClose;
        private Button _btnFill;
        private Button _btnLeft;
        private Button _btnRight;
        private int _curLevel;
        private UILabel _fillCountToNextVip;
        private VipLeftView _leftView;
        private UILabel _myNextVip;
        private UILabel _myVip;
        private GameObject _nextVipGo;
        private Transform _progressTrans;
        private VipRightView _rightView;
        private UILabel _slidderLabel;
        private UISlider _slider;
        private UILabel _vipLevel;

        public override void CancelUpdateHandler()
        {
            this._btnLeft.onClick = (UIWidgetContainer.VoidDelegate) (this._btnRight.onClick = null);
            this._btnFill.onClick = (UIWidgetContainer.VoidDelegate) (this._btnClose.onClick = null);
            Singleton<VIPMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnVipEvent);
        }

        protected override void HandleAfterOpenView()
        {
            this._curLevel = (Singleton<VIPMode>.Instance.vip > 0) ? Singleton<VIPMode>.Instance.vip : 1;
            this.UpdateView();
        }

        protected override void HandleBeforeCloseView()
        {
            this._curLevel = 0;
        }

        protected override void Init()
        {
            this._btnClose = base.FindInChild<Button>("btnClose");
            this._btnFill = base.FindInChild<Button>("head/btnFill");
            this._myVip = base.FindInChild<UILabel>("head/progress/vipIcon/label");
            this._slider = base.FindInChild<UISlider>("head/progress/slider");
            this._slidderLabel = base.FindInChild<UILabel>("head/progress/slider/label");
            this._myNextVip = base.FindInChild<UILabel>("head/nextVip/label3");
            this._vipLevel = base.FindInChild<UILabel>("level/label");
            this._btnLeft = base.FindInChild<Button>("btnLeft");
            this._btnRight = base.FindInChild<Button>("btnRight");
            this._fillCountToNextVip = base.FindInChild<UILabel>("head/nextVip/label");
            this._leftView = new VipLeftView(base.FindChild("leftView"));
            this._rightView = new VipRightView(base.FindChild("rightView"));
            this._nextVipGo = base.FindChild("head/nextVip");
            this._progressTrans = base.FindInChild<Transform>("head/progress");
        }

        private void OnClick(GameObject go)
        {
            if (this._btnClose.gameObject == go)
            {
                this.CloseView();
            }
            else if (this._btnFill.gameObject == go)
            {
                Singleton<AlertPanel>.Instance.Show("请充值！", null, null);
            }
        }

        private void OnClickLRLevel(GameObject go)
        {
            if (this._btnLeft.gameObject == go)
            {
                this._curLevel--;
            }
            else
            {
                this._curLevel++;
            }
            this.UpdateView();
        }

        private void OnVipEvent(object sender, int code)
        {
            this.UpdateView();
        }

        public override void RegisterUpdateHandler()
        {
            this._btnLeft.onClick = this._btnRight.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickLRLevel);
            this._btnFill.onClick = this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
            Singleton<VIPMode>.Instance.dataUpdated += new DataUpateHandler(this.OnVipEvent);
        }

        private void UpdateHead()
        {
            uint vip = (uint) Singleton<VIPMode>.Instance.vip;
            SysVipInfoVo vIPInfo = BaseDataMgr.instance.GetVIPInfo(vip);
            bool flag = vip >= VarMgr.VipMax;
            this._nextVipGo.SetActive(!flag);
            this._myVip.text = vip.ToString();
            if (flag)
            {
                this._progressTrans.localPosition = base.FindInChild<Transform>("head/hot").localPosition;
                this._slider.value = 1f;
                this._slidderLabel.text = Singleton<VIPMode>.Instance.numFilled + "/" + vIPInfo.diam;
            }
            else
            {
                SysVipInfoVo vo2 = BaseDataMgr.instance.GetVIPInfo(vip + 1);
                this._progressTrans.localPosition = Vector3.zero;
                this._myNextVip.text = (vip + 1).ToString();
                this._slider.value = ((float) Singleton<VIPMode>.Instance.numFilled) / ((float) vo2.diam);
                this._slidderLabel.text = Singleton<VIPMode>.Instance.numFilled + "/" + vo2.diam;
                this._fillCountToNextVip.text = LanguageManager.GetWord("Game.FillMore", Math.Max(0, vo2.diam - Singleton<VIPMode>.Instance.numFilled).ToString());
            }
        }

        private void UpdateView()
        {
            this.UpdateHead();
            this._vipLevel.text = LanguageManager.GetWord("VIP.VipLevel", this._curLevel.ToString());
            this._btnLeft.SetActive(this._curLevel > 1);
            this._btnLeft.label.text = LanguageManager.GetWord("VIP.PreViewVip", (this._curLevel - 1).ToString());
            this._btnRight.SetActive(this._curLevel < VarMgr.VipMax);
            this._btnRight.label.text = LanguageManager.GetWord("VIP.PreViewVip", (this._curLevel + 1).ToString());
            this._leftView.Update(this._curLevel);
            this._rightView.Update(this._curLevel);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/VIP/VipPanel.assetbundle";
            }
        }
    }
}

