﻿namespace com.game.module.VIP
{
    using System;
    using UnityEngine;

    public class VipDroitItemView
    {
        private BoxCollider _boxCollider;
        private GameObject _go;
        private UILabel _label;

        public VipDroitItemView(GameObject go)
        {
            this._go = go;
            this._label = NGUITools.FindInChild<UILabel>(this._go, "label");
            this._boxCollider = this._go.GetComponent<BoxCollider>();
        }

        public void SetActive(bool b)
        {
            this._go.SetActive(b);
        }

        public void SetText(string v)
        {
            this._label.text = v;
            Vector3 size = this._boxCollider.size;
            size.x = this._label.width;
            size.y = this._label.height;
            this._boxCollider.size = size;
            size = this._boxCollider.center;
            size.x = this._label.width / 2;
            this._boxCollider.center = size;
        }

        public void SetY(float v)
        {
            Vector3 localPosition = this._go.transform.localPosition;
            localPosition.y = v;
            this._go.transform.localPosition = localPosition;
        }

        public float height
        {
            get
            {
                return (Mathf.Max(VipLeftView.IconHeight, (float) this._label.height) + 10f);
            }
        }
    }
}

