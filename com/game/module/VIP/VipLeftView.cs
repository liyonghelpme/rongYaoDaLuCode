﻿namespace com.game.module.VIP
{
    using com.game.data;
    using com.game.manager;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class VipLeftView
    {
        private UILabel _condition;
        private GameObject _go;
        private GameObject _gridGo;
        private Transform _gridTrans;
        private GameObject _itemGo;
        private List<VipDroitItemView> _itemViews = new List<VipDroitItemView>();
        public static float IconHeight;

        public VipLeftView(GameObject go)
        {
            this._go = go;
            this._gridGo = NGUITools.FindChild(this._go, "droits");
            this._gridTrans = this._gridGo.transform;
            this._itemGo = NGUITools.FindChild(this._go, "droits/item");
            this._condition = NGUITools.FindInChild<UILabel>(this._go, "droits/condition/label");
            IconHeight = NGUITools.FindInChild<UISprite>(this._go, "droits/condition/icon").height;
            this._itemViews.Add(new VipDroitItemView(this._itemGo));
        }

        public void Update(int level)
        {
            SysVipInfoVo vIPInfo = BaseDataMgr.instance.GetVIPInfo((uint) level);
            this._condition.text = LanguageManager.GetWord("VIP.TotalFillTo", vIPInfo.diam.ToString());
            char[] separator = new char[] { '\r', '\n' };
            string[] strArray = vIPInfo.describe.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            int index = 0;
            while (this._itemViews.Count < strArray.Length)
            {
                GameObject go = NGUITools.AddChild(this._gridGo, this._itemGo);
                this._itemViews.Add(new VipDroitItemView(go));
            }
            float v = -IconHeight;
            for (index = 0; index < this._itemViews.Count; index++)
            {
                VipDroitItemView view = this._itemViews[index];
                if (index >= strArray.Length)
                {
                    view.SetActive(false);
                }
                else
                {
                    view.SetActive(true);
                    view.SetText(strArray[index]);
                    view.SetY(v);
                    v -= view.height;
                }
            }
        }
    }
}

