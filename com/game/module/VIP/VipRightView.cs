﻿namespace com.game.module.VIP
{
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.module.Utility;
    using System;
    using UnityEngine;

    public class VipRightView
    {
        private Button _btn;
        private GameObject _go;
        private UISprite _gotIcon;
        private UIGrid _grid;
        private GameObject _gridGo;
        private Transform _gridTransform;
        private GameObject _itemGo;
        private int _level;

        public VipRightView(GameObject go)
        {
            this._go = go;
            this._gridGo = NGUITools.FindChild(this._go, "grid");
            this._itemGo = NGUITools.FindChild(this._go, "grid/item");
            this._gridTransform = this._gridGo.transform;
            this._grid = this._gridGo.GetComponent<UIGrid>();
            this._btn = NGUITools.FindInChild<Button>(this._go, "btnAward");
            this._gotIcon = NGUITools.FindInChild<UISprite>(this._go, "gotIcon");
            this._btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        private void OnClick(GameObject go)
        {
            Singleton<VIPMode>.Instance.ReqGetAward(this._level);
        }

        public void Update(int level)
        {
            this._level = level;
            AwardInfo awardInfo = VIPUtil.GetAwardInfo((uint) this._level);
            int index = 0;
            for (index = 0; index < awardInfo.ids.Count; index++)
            {
                GameObject gameObject;
                if (index < this._gridTransform.childCount)
                {
                    gameObject = this._gridTransform.GetChild(index).gameObject;
                }
                else
                {
                    gameObject = NGUITools.AddChild(this._gridGo, this._itemGo);
                }
                gameObject.AddMissingComponent<SysItemVoCell>();
            }
            int childCount = this._gridTransform.childCount;
            for (index = 0; index < childCount; index++)
            {
                SysItemVoCell component = this._gridTransform.GetChild(index).GetComponent<SysItemVoCell>();
                if (index >= awardInfo.ids.Count)
                {
                    component.SetActive(false);
                }
                else
                {
                    component.SetActive(true);
                    component.sysVoId = awardInfo.ids[index];
                    component.count = awardInfo.counts[index];
                }
            }
            this._grid.Reposition();
            bool state = Singleton<VIPMode>.Instance.HasGotAward(this._level);
            this._gotIcon.SetActive(state);
            this._btn.SetActive(!state && (Singleton<VIPMode>.Instance.vip >= this._level));
        }
    }
}

