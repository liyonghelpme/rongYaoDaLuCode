﻿namespace com.game.module
{
    using System;
    using UnityEngine;

    [AddComponentMenu("NGUI/UI/ViewTree")]
    public class ViewTree : MonoBehaviour
    {
        public static GameObject battle;
        public static GameObject city;
        public static GameObject go;

        private void Awake()
        {
            go = base.gameObject;
        }

        public static void SetSubObj()
        {
            if (go != null)
            {
                GameObject prefab = new GameObject("battle");
                NGUITools.AddChild(go, prefab);
                GameObject obj3 = new GameObject("city");
                NGUITools.AddChild(go, obj3);
                battle = go.transform.FindChild("battle(Clone)").gameObject;
                city = go.transform.FindChild("city(Clone)").gameObject;
                UnityEngine.Object.DestroyImmediate(prefab);
                UnityEngine.Object.DestroyImmediate(obj3);
            }
        }
    }
}

