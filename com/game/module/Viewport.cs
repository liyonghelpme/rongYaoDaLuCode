﻿namespace com.game.module
{
    using System;
    using UnityEngine;

    [AddComponentMenu("NGUI/UI/Viewport")]
    public class Viewport : MonoBehaviour
    {
        public static GameObject go;

        private void Awake()
        {
            go = base.gameObject;
        }
    }
}

