﻿namespace com.game.module.Waiting
{
    using com.game.manager;
    using com.game.module.core;
    using System;

    public class WaitingView : BaseView<WaitingView>
    {
        private UISprite fillSprite;
        public AssetBundleLoader loadProcess;
        private TweenAlphas playAlphas;
        private TweenPlay playTween;

        protected override void HandleAfterOpenView()
        {
            this.playAlphas.ResetToBeginning();
            this.playTween.PlayForward();
        }

        protected override void HandleBeforeCloseView()
        {
            this.playTween.ResetToBeginning();
            this.loadProcess = null;
        }

        protected override void Init()
        {
            base.firstOpen = false;
            this.playTween = base.FindInChild<TweenPlay>("node/waiting");
            this.playAlphas = base.FindInChild<TweenAlphas>("node");
            base.closeTween = this.playAlphas;
            this.playAlphas.duration = 0.2f;
            this.playTween.style = UITweener.Style.PingPong;
            this.fillSprite = base.FindInChild<UISprite>("node/waiting/foreground");
        }

        public override void Update()
        {
            if (this.loadProcess != null)
            {
                float progress = this.loadProcess.progress;
                float fillAmount = this.fillSprite.fillAmount;
                if (fillAmount != progress)
                {
                    this.fillSprite.fillAmount = fillAmount + 0.01f;
                }
                if (progress > 0.999f)
                {
                    this.fillSprite.fillAmount = 1f;
                }
            }
            else
            {
                this.fillSprite.fillAmount = 1f;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Waiting/WaitingView.assetbundle";
            }
        }
    }
}

