﻿namespace com.game.module.WiFiPvP
{
    using System;

    public class FightCounter
    {
        public int Key;
        public int Value;

        public FightCounter()
        {
        }

        public FightCounter(int key, int value)
        {
            this.Key = key;
            this.Value = value;
        }
    }
}

