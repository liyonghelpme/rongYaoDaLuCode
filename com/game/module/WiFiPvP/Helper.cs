﻿namespace com.game.module.WiFiPvP
{
    using System;
    using System.Runtime.CompilerServices;

    public static class Helper
    {
        public static FightCounter AddDeadCount(this FightCounter kv)
        {
            kv.Value++;
            return kv;
        }

        public static FightCounter AddKillCount(this FightCounter kv)
        {
            kv.Key++;
            return kv;
        }
    }
}

