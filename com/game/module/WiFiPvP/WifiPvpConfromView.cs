﻿namespace com.game.module.WiFiPvP
{
    using com.game.manager;
    using com.game.module.core;
    using com.net.wifi_pvp;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class WifiPvpConfromView : BaseView<WifiPvpConfromView>
    {
        [CompilerGenerated]
        private static Func<GameObject, bool> <>f__am$cacheB;
        private Button change_btn;
        private GameObject heroBoxItem;
        private GameObject heroItem;
        private int maxGap = 160;
        private int MaxItemCount = 3;
        private IList<GameObject> needDestroyList = new List<GameObject>();
        private IList<UISprite> needResetGrayList = new List<UISprite>();
        private IDictionary<string, List<UITexture>> spriteList = new Dictionary<string, List<UITexture>>();
        private Button start_btn;
        private GameObject teamA;
        private GameObject teamB;

        public override void CancelUpdateHandler()
        {
            Singleton<WifiPvpMode>.Instance.dataUpdated += new DataUpateHandler(this.DataUpdated);
        }

        public override void DataUpdated(object sender, int code)
        {
        }

        private List<UITexture> getTexture(string id)
        {
            if (this.spriteList.ContainsKey(id))
            {
                return this.spriteList[id];
            }
            return null;
        }

        protected override void HandleAfterOpenView()
        {
            if (Singleton<GeneralPanel>.Instance.IsOpened)
            {
                Singleton<GeneralPanel>.Instance.CloseView();
            }
            if (Singleton<GeneralInfoPanel>.Instance.IsOpened)
            {
                Singleton<GeneralInfoPanel>.Instance.CloseView();
            }
            this.loadModeData();
        }

        protected override void HandleBeforeCloseView()
        {
            if (<>f__am$cacheB == null)
            {
                <>f__am$cacheB = go => go != null;
            }
            IEnumerator<GameObject> enumerator = this.needDestroyList.Where<GameObject>(<>f__am$cacheB).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    GameObject current = enumerator.Current;
                    UnityEngine.Object.Destroy(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.needDestroyList.Clear();
            this.needResetGrayList.Clear();
        }

        protected override void Init()
        {
            this.start_btn = base.FindInChild<Button>("buttons/start");
            this.change_btn = base.FindInChild<Button>("buttons/change");
            this.heroItem = base.FindChild("hero");
            this.teamA = base.FindChild("teamA");
            this.teamB = base.FindChild("teamB");
            this.heroBoxItem = base.FindChild("heroBoxItem");
        }

        private void InitFighter(IList<PVPGeneralBrief> list, bool isMe)
        {
            GameObject parent = !(Singleton<WifiPvpMode>.Instance.IsMaster ^ isMe) ? this.teamA : this.teamB;
            GameObject item = NGUITools.AddChild(parent, this.heroBoxItem);
            item.name = !(Singleton<WifiPvpMode>.Instance.IsMaster ^ isMe) ? "heroBoxA" : "heroBoxB";
            item.transform.localPosition = new Vector3();
            item.SetActive(true);
            this.needDestroyList.Add(item);
            for (int i = 0; i < list.Count; i++)
            {
                PVPGeneralBrief itemInfo = list[i];
                this.InitTeamItem(item, this.heroItem, i, itemInfo);
            }
        }

        private void InitInfo(PVPAccountBrief accountInfo, bool isMe)
        {
            GameObject parent = !(Singleton<WifiPvpMode>.Instance.IsMaster ^ isMe) ? this.teamA : this.teamB;
            UILabel label = NGUITools.FindInChild<UILabel>(parent, "name");
            UILabel label2 = NGUITools.FindInChild<UILabel>(parent, "lv");
            UILabel label3 = NGUITools.FindInChild<UILabel>(parent, "fight/num");
            label.text = accountInfo.accountName;
            label2.text = accountInfo.accountLevel;
            label3.text = accountInfo.accountFightEffect;
        }

        private void InitTeamItem(GameObject parent, GameObject prefab, int index, PVPGeneralBrief itemInfo)
        {
            GameObject obj2 = NGUITools.AddChild(parent, prefab);
            obj2.SetActive(true);
            if (parent.name == "heroBoxA")
            {
                index = this.MaxItemCount - (index + 1);
            }
            obj2.transform.localPosition = new Vector3((float) (index * this.maxGap), 0f, 0f);
            UILabel label = NGUITools.FindInChild<UILabel>(obj2, "name");
            UILabel label2 = NGUITools.FindInChild<UILabel>(obj2, "lv");
            UISprite sprite = NGUITools.FindInChild<UISprite>(obj2, "type");
            UITexture item = NGUITools.FindInChild<UITexture>(obj2, "icon");
            if (this.spriteList.ContainsKey(itemInfo.generalTid))
            {
                this.spriteList[itemInfo.generalTid].Add(item);
            }
            else
            {
                List<UITexture> list = new List<UITexture> {
                    item
                };
                this.spriteList.Add(itemInfo.generalTid, list);
            }
            label.text = itemInfo.name;
            label2.text = itemInfo.level;
            sprite.spriteName = itemInfo.job;
            AssetManager.Instance.LoadAsset<Texture2D>("Sources/Kapai/smallIcon/" + itemInfo.generalTid + ".assetbundle", new LoadAssetFinish<Texture2D>(this.kapaiComplete), null, false, true);
        }

        private void kapaiComplete(Texture2D textrue)
        {
            if (textrue != null)
            {
                foreach (UITexture texture in this.getTexture(textrue.name))
                {
                    if (texture != null)
                    {
                        texture.mainTexture = textrue;
                        texture.width = 0x7c;
                        texture.height = 0xa5;
                    }
                }
            }
        }

        private void loadModeData()
        {
            PVPFighter myFighterInfo = Singleton<WifiPvpMode>.Instance.myFighterInfo;
            PVPFighter enemyFighterInfo = Singleton<WifiPvpMode>.Instance.enemyFighterInfo;
            if ((myFighterInfo == null) || (enemyFighterInfo == null))
            {
                Debug.LogError("PVPFighter 有问题,找雄爷,实在不行弹XX");
            }
            else
            {
                if (!Singleton<WifiPvpMode>.Instance.IsMaster)
                {
                    this.UpdateButtonState(com.game.module.WiFiPvP.ButtonType.ChangeButton, false, true, string.Empty, null);
                    this.UpdateButtonState(com.game.module.WiFiPvP.ButtonType.StartButton, true, true, LanguageManager.GetWord("WifiPvp.ConfirmPrepare"), null);
                }
                else
                {
                    this.UpdateButtonState(com.game.module.WiFiPvP.ButtonType.ChangeButton, false, true, string.Empty, null);
                    this.UpdateButtonState(com.game.module.WiFiPvP.ButtonType.StartButton, true, false, LanguageManager.GetWord("WifiPvp.ConfirmWait"), null);
                }
                this.InitFighter(myFighterInfo.genList, true);
                this.InitFighter(enemyFighterInfo.genList, false);
                this.InitInfo(myFighterInfo.acc, true);
                this.InitInfo(enemyFighterInfo.acc, false);
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<WifiPvpMode>.Instance.dataUpdated -= new DataUpateHandler(this.DataUpdated);
        }

        public void UpdateButtonState(com.game.module.WiFiPvP.ButtonType button = 2, bool isActive = true, bool isEnabled = true, string label = "", UIWidgetContainer.VoidDelegate callback = null)
        {
            Button button2 = (button != com.game.module.WiFiPvP.ButtonType.ChangeButton) ? this.start_btn : this.change_btn;
            UILabel label2 = NGUITools.FindInChild<UILabel>(button2.gameObject, "Label");
            UISprite item = button2.FindInChild<UISprite>("background");
            button2.SetActive(isActive);
            if (!isEnabled)
            {
                item.ShowAsGray();
            }
            else
            {
                item.ShowAsMyself();
            }
            if (this.needResetGrayList.Contains(item))
            {
                this.needResetGrayList.Remove(item);
            }
            this.needResetGrayList.Add(item);
            button2.isEnabled = isEnabled;
            if ((label != string.Empty) && (label2 != null))
            {
                label2.text = label;
            }
            if (callback != null)
            {
                button2.onClick = callback;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

