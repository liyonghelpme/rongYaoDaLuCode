﻿namespace com.game.module.WiFiPvP
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.hud;
    using com.game.module.map;
    using com.game.module.Role;
    using com.game.Public.Message;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using com.net;
    using com.net.interfaces;
    using com.net.wifi_pvp;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class WifiPvpControl : BaseControl<WifiPvpControl>
    {
        private readonly IDictionary<ulong, MeDisplay> myGeneralDic = new Dictionary<ulong, MeDisplay>();
        private readonly IList<ulong> roleEnterId = new List<ulong>();

        private void AddBaseAndTower_14_14(INetData receiveData)
        {
            WifiPvpAddMonMsg_14_14 g__ = new WifiPvpAddMonMsg_14_14();
            g__.read(receiveData.GetMemoryStream());
            Singleton<MapMode>.Instance.AddMonInCurrentPVPMap(g__.monList);
        }

        private void AddingPrivate_14_16(INetData receiveData)
        {
            WifiPvpSyncPrivateMsg_14_16 g__ = new WifiPvpSyncPrivateMsg_14_16();
            g__.read(receiveData.GetMemoryStream());
            Singleton<MapMode>.Instance.AddMonInCurrentPVPMap(g__.privateList);
        }

        private void AddNewPeer_14_12(INetData receiveData)
        {
            WifiPvpAddIntranetPlayerMsg_14_12 g__ = new WifiPvpAddIntranetPlayerMsg_14_12();
            g__.read(receiveData.GetMemoryStream());
            WifiLAN.Instance.AddNewPeerToIpList(g__.player);
        }

        private void AddPlayer_14_3(INetData data)
        {
            Debug.LogWarning("收到PVP属性同步协议");
            WifiPvpRequestAttrsMsg_14_3 g__ = new WifiPvpRequestAttrsMsg_14_3();
            g__.read(data.GetMemoryStream());
            if (AppMap.Instance.IsInWifiPVP && ((WifiLAN.Instance.IsMaster && (g__.sender == 0)) || (!WifiLAN.Instance.IsMaster && (g__.sender == 1))))
            {
                Singleton<MapMode>.Instance.AddPlayerInCurrentPVPMap(g__.generalAttr, g__.coordinator, g__.generalEquipAttr);
                this.roleEnterId.Clear();
                foreach (PNewAttr attr in g__.generalAttr)
                {
                    this.roleEnterId.Add(attr.id);
                }
                vp_Timer.In(1f, new vp_Timer.Callback(this.DoAddEnemyToDic), null);
            }
        }

        public void ClearGeneralDicWhenQuitPVP()
        {
            this.myGeneralDic.Clear();
        }

        private void DamageSync_14_6(INetData data)
        {
            if (AppMap.Instance.IsInWifiPVP && (AppMap.Instance.me != null))
            {
                WifiPvpDamageSyncMsg_14_6 skillDamageSynInfo = new WifiPvpDamageSyncMsg_14_6();
                skillDamageSynInfo.read(data.GetMemoryStream());
                this.DoSyncDamage(skillDamageSynInfo);
            }
        }

        private void Dead_14_7(INetData receiveData)
        {
            WifiPvpDeadMsg_14_7 g__ = new WifiPvpDeadMsg_14_7();
            g__.read(receiveData.GetMemoryStream());
            if (AppMap.Instance.IsInWifiPVP)
            {
                PlayerDisplay player = AppMap.Instance.GetPlayer(g__.id + string.Empty);
                if (((player != null) && (player.GoBase != null)) && (player.GetVo().Camp != 0))
                {
                    this.DoDead(player, new Vector3(g__.x * 0.001f, g__.y * 0.001f, g__.z * 0.001f));
                }
            }
        }

        private void DoAddEnemyToDic()
        {
            IEnumerator<ulong> enumerator = this.roleEnterId.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay player = AppMap.Instance.GetPlayer(enumerator.Current + string.Empty);
                    WifiPvpManager.Instance.AddEnemy(player);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void DoAddHp(ulong id, byte type, uint delta, uint endHp)
        {
            if ((delta == 0) && (type == 2))
            {
                PlayerDisplay player = AppMap.Instance.GetPlayer(id.ToString());
                if ((player != null) && (player.GoBase != null))
                {
                    Log.AI(player.GoBase, " SyncHp When Near Spring " + endHp);
                    player.GetVo().SetHp(endHp);
                }
            }
            else
            {
                SkillSynType type2 = (SkillSynType) type;
                if (type2 == SkillSynType.Player)
                {
                    PlayerDisplay display = AppMap.Instance.GetPlayer(id.ToString());
                    if (((display != null) && (display.Controller != null)) && (display.GetVo().Camp != 0))
                    {
                        int currentStatu = display.Controller.StatuController.CurrentStatu;
                        display.GetVo().AddHp(delta);
                        HudView.Instance.AddPlayerHealthBarWhenEmpty(display);
                        HudView.Instance.recoverBlood(display.Controller.GoHp, (float) display.GetVo().Hp, (float) display.GetVo().CurHp, (float) delta, true);
                        switch (currentStatu)
                        {
                            case 11:
                            case 0x13:
                                CommandHandler.AddCommandStatic(display.GoBase, "idle", 1f);
                                break;
                        }
                    }
                }
                else
                {
                    Debug.LogWarning("不科学的同步血量信息");
                }
            }
        }

        private void DoDead(PlayerDisplay play, Vector3 destPos)
        {
            Debug.LogWarning("收到敌方单位死亡协议");
            play.GetVo().ReduceHpToPercent((uint) 0);
            play.Controller.DisposeHudView();
            play.GoBase.transform.position = destPos;
            if (!play.Controller.StatuController.IsDead)
            {
                Debug.Log("当前不是死亡, 强行死");
                play.Controller.DisposeHudView();
                ActionVo vo = new ActionVo {
                    ActionType = "death"
                };
                play.Controller.AttackController.AddAttackList(vo, true, false);
            }
        }

        public void DoReborn(PlayerDisplay play, Vector3 destPos)
        {
            if (play != null)
            {
                play.GoBase.transform.position = destPos;
                play.GetVo().AddHpToPercent((uint) 100);
                MeControler controler = play.Controller as MeControler;
                BuffController buffController = play.Controller.buffController;
                if (controler != null)
                {
                    controler.DoRelive();
                    buffController.Reborn();
                }
                else
                {
                    PlayerStatuController statuController = play.Controller.StatuController as PlayerStatuController;
                    CommandHandler.AddCommandStatic(statuController.gameObject, "idle relive", 1f);
                    buffController.Reborn();
                    BaseRoleVo vo = play.GetVo();
                    HudView.Instance.AddPlayerHealthBarWhenEmpty(play);
                    play.Controller.GoHp.GetComponent<UpdateHpBar>().SetHp(vo.Hp, vo.CurHp, false);
                }
            }
        }

        private void DoSyncDamage(WifiPvpDamageSyncMsg_14_6 skillDamageSynInfo)
        {
            int displayType = DamageCheck.GetDisplayType(skillDamageSynInfo.actorType);
            ulong actorId = skillDamageSynInfo.actorId;
            ActionDisplay actionDisplayByDisplayTypeAndId = SkillControl.GetActionDisplayByDisplayTypeAndId(displayType, actorId);
            if (((actionDisplayByDisplayTypeAndId != null) && (actionDisplayByDisplayTypeAndId.GoBase != null)) && (actionDisplayByDisplayTypeAndId.Controller != null))
            {
                Vector3 position = actionDisplayByDisplayTypeAndId.GoBase.transform.position;
                SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(skillDamageSynInfo.skillId);
                bool isPlayer = actionDisplayByDisplayTypeAndId is PlayerDisplay;
                bool isHeal = sysSkillBaseVo.subtype == 1;
                foreach (PDamage damage in skillDamageSynInfo.damageList)
                {
                    ActionVo vo13;
                    bool isDodge = false;
                    bool isCrit = false;
                    bool isParry = false;
                    bool isKnockDown = false;
                    bool isKnockBack = false;
                    List<PDamageBuff> buffList = damage.buffList;
                    DamageCheck.SetStateTypeAndDamageType(damage.dmgType, damage.stateType, ref isDodge, ref isCrit, ref isKnockDown, ref isKnockBack);
                    switch (DamageCheck.GetDisplayType(damage.type))
                    {
                        case 100:
                            if (this.GetMyGenerals().ContainsKey(damage.id))
                            {
                                MeDisplay enemyDisplay = this.myGeneralDic[damage.id];
                                if (!enemyDisplay.Controller.StatuController.IsDead && !enemyDisplay.GetVo().IsEmptyHp)
                                {
                                    if (!isDodge)
                                    {
                                        ActionVo attackVo = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, enemyDisplay, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                        if (attackVo == null)
                                        {
                                            break;
                                        }
                                        attackVo = DamageCheck.FixHurtType(attackVo, isKnockBack, isKnockDown);
                                        enemyDisplay.Controller.BeAttackedController.BeAttacked(attackVo);
                                    }
                                    if (enemyDisplay.GetVo().CurHp > damage.dmg)
                                    {
                                        enemyDisplay.GetVo().ReduceHp(damage.dmg);
                                        if (actionDisplayByDisplayTypeAndId is PlayerDisplay)
                                        {
                                            WifiPvpManager.Instance.MyHeroBeAttackByEnemyHero(enemyDisplay, actionDisplayByDisplayTypeAndId as PlayerDisplay);
                                        }
                                    }
                                    else
                                    {
                                        enemyDisplay.GetVo().ReduceHpToPercent((uint) 0);
                                        if (!enemyDisplay.GetVo().IsEmptyHp)
                                        {
                                            ActionVo vo3 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, enemyDisplay, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                            if (vo3 == null)
                                            {
                                                break;
                                            }
                                            vo3 = DamageCheck.FixHurtType(vo3, isKnockBack, isKnockDown);
                                            enemyDisplay.Controller.BeAttackedController.BeAttacked(vo3);
                                            return;
                                        }
                                        if (enemyDisplay.Controller.buffController != null)
                                        {
                                            enemyDisplay.Controller.buffController.Dead();
                                        }
                                        enemyDisplay.Controller.DisposeHudView();
                                        vo13 = new ActionVo {
                                            ActionType = "death",
                                            SkillId = skillDamageSynInfo.skillId
                                        };
                                        ActionVo vo = vo13;
                                        enemyDisplay.Controller.AttackController.AddAttackList(vo, true, false);
                                        Debug.Log("我方英雄被杀");
                                        WifiPvpManager.Instance.MyHeroDie(enemyDisplay);
                                        if (actionDisplayByDisplayTypeAndId is PlayerDisplay)
                                        {
                                            Debug.Log("敌方英雄杀人数+1");
                                            WifiPvpManager.Instance.EnemyIncreaseKillCount(actionDisplayByDisplayTypeAndId);
                                        }
                                    }
                                    Vector3 target = enemyDisplay.Controller.SkillController.MeController.transform.position;
                                    Singleton<BattleRightView>.Instance.UpdateHeadBloodBar(enemyDisplay.GetVo());
                                    HudView.Instance.CreateDamageHudText(target, isDodge, isCrit, isParry, damage.dmgForShow, false, isPlayer, isHeal, false, enemyDisplay, actionDisplayByDisplayTypeAndId);
                                    DamageCheck.UpdateHealthBarRelated(enemyDisplay, damage.dmg);
                                }
                            }
                            else
                            {
                                PlayerDisplay player = AppMap.Instance.GetPlayer(damage.id + string.Empty);
                                if ((player != null) && (player.GoBase != null))
                                {
                                    BaseRoleVo vo5 = player.GetVo();
                                    if (((vo5 != null) && !vo5.IsEmptyHp) && !player.Controller.StatuController.IsDead)
                                    {
                                        if (vo5.CurHp > damage.dmg)
                                        {
                                            if (!isDodge)
                                            {
                                                ActionVo vo6 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, player, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                                if (vo6 == null)
                                                {
                                                    break;
                                                }
                                                vo6 = DamageCheck.FixHurtType(vo6, isKnockBack, isKnockDown);
                                                player.Controller.BeAttackedController.BeAttacked(vo6);
                                            }
                                            vo5.ReduceHp(damage.dmg);
                                        }
                                        else
                                        {
                                            vo5.ReduceHpToPercent((uint) 0);
                                            if (!vo5.IsEmptyHp)
                                            {
                                                ActionVo vo7 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, player, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                                if (vo7 == null)
                                                {
                                                    break;
                                                }
                                                vo7 = DamageCheck.FixHurtType(vo7, isKnockBack, isKnockDown);
                                                player.Controller.BeAttackedController.BeAttacked(vo7);
                                                return;
                                            }
                                            if (player.Controller.buffController != null)
                                            {
                                                player.Controller.buffController.Dead();
                                            }
                                            vo13 = new ActionVo {
                                                ActionType = "death",
                                                SkillId = skillDamageSynInfo.skillId
                                            };
                                            ActionVo vo8 = vo13;
                                            player.Controller.AttackController.AddAttackList(vo8, true, false);
                                            player.Controller.DisposeHudView();
                                        }
                                        DamageCheck.ShowBuffEffects(actionDisplayByDisplayTypeAndId, player, buffList);
                                        Vector3 vector3 = player.Controller.SkillController.MeController.transform.position;
                                        HudView.Instance.CreateDamageHudText(vector3, isDodge, isCrit, isParry, damage.dmgForShow, false, isPlayer, isHeal, false, player, actionDisplayByDisplayTypeAndId);
                                    }
                                }
                            }
                            break;

                        case 400:
                        {
                            MonsterDisplay monster = AppMap.Instance.GetMonster(damage.id + string.Empty);
                            if ((monster == null) || (monster.GoBase == null))
                            {
                                return;
                            }
                            MonsterVo meVoByType = monster.GetMeVoByType<MonsterVo>();
                            if ((meVoByType == null) || (meVoByType.Camp != 0))
                            {
                                return;
                            }
                            if (meVoByType.CurHp > damage.dmg)
                            {
                                if (!isDodge)
                                {
                                    ActionVo vo10 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, monster, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                    if (vo10 == null)
                                    {
                                        break;
                                    }
                                    vo10 = DamageCheck.FixHurtType(vo10, isKnockBack, isKnockDown);
                                    vo10.IsFromNet = true;
                                    monster.Controller.BeAttackedController.BeAttacked(vo10);
                                }
                                meVoByType.ReduceHp(damage.dmg);
                            }
                            else
                            {
                                meVoByType.ReduceHpToPercent((uint) 0);
                                if (!meVoByType.IsEmptyHp)
                                {
                                    ActionVo vo11 = DamageCheck.GetSkillActionVo(sysSkillBaseVo, position, monster, actionDisplayByDisplayTypeAndId, 0, 1f, null);
                                    if (vo11 == null)
                                    {
                                        break;
                                    }
                                    vo11 = DamageCheck.FixHurtType(vo11, isKnockBack, isKnockDown);
                                    vo11.IsFromNet = true;
                                    monster.Controller.BeAttackedController.BeAttacked(vo11);
                                    return;
                                }
                                if (monster.Controller.buffController != null)
                                {
                                    monster.Controller.buffController.Dead();
                                }
                                float num4 = (sysSkillBaseVo.hurt_fly_velocity_max * (((float) damage.dmg) / ((float) meVoByType.Hp))) * 0.001f;
                                monster.Controller.MoveController.DeathFlySpeed = num4;
                                vo13 = new ActionVo {
                                    ActionType = "deathfly"
                                };
                                ActionVo vo12 = vo13;
                                monster.Controller.AttackController.AddAttackList(vo12, true, false);
                                monster.Controller.DisposeHudView();
                                if (AppMap.Instance.IsInWifiPVP && (meVoByType.Camp == 0))
                                {
                                    string targetEffectId = DamageCheck.AddEffectInPvp(monster);
                                    DamageCheck.DoAddEffectInPvp(monster, targetEffectId);
                                    WifiPvpManager.Instance.RemoveMyTowerOrBase(monster);
                                }
                            }
                            DamageCheck.ShowBuffEffects(actionDisplayByDisplayTypeAndId, monster, buffList);
                            Vector3 vector4 = Tools.find(monster.Controller.gameObject, "pivot").transform.position;
                            HudView.Instance.CreateDamageHudText(vector4, isDodge, isCrit, isParry, damage.dmgForShow, false, isPlayer, isHeal, false, monster, actionDisplayByDisplayTypeAndId);
                            DamageCheck.UpdateHealthBarRelated(monster, damage.dmg);
                            break;
                        }
                    }
                }
            }
        }

        private static void DoSyncPos(ActionDisplay display, int x, int y, int z, int rotateY)
        {
            if (!display.Controller.StatuController.IsGonnaDie)
            {
                display.ChangeDire((rotateY * 0.001f) * 0.001f);
                Vector3 vector = new Vector3(x * 0.001f, y * 0.001f, z * 0.001f);
                Log.Net(display.GoBase + " syndisplay pos " + vector);
                if (DamageCheck.GetDistance(vector, display.GoBase.transform.position) >= 0.3f)
                {
                    Log.Net(display.GoBase + " walkto " + vector);
                    (display.Controller as ActionControler).WalkTo(vector, null, false, false);
                }
                display.SetSortingOrder(false);
            }
        }

        private void EnemyRelive_14_8(INetData receiveData)
        {
            Debug.Log("收到敌方单位复活协议");
            WifiPvpReliveMsg_14_8 g__ = new WifiPvpReliveMsg_14_8();
            g__.read(receiveData.GetMemoryStream());
            PlayerDisplay player = AppMap.Instance.GetPlayer(g__.id + string.Empty);
            if ((player != null) && (player.GoBase != null))
            {
                this.DoReborn(player, new Vector3(g__.x * 0.001f, g__.y * 0.001f, g__.z * 0.001f));
            }
        }

        private void EnterPVP_14_1(INetData data)
        {
            WifiPvpRequestEnterMsg_14_1 g__ = new WifiPvpRequestEnterMsg_14_1();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                WifiPvpManager.Instance.Enter(g__.pvpId, g__.isMaster);
                Singleton<MapMode>.Instance.changeScene_4_2();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void FindPeerResult_14_10(INetData receiveData)
        {
            WifiPvpStartFlushMsg_14_10 g__ = new WifiPvpStartFlushMsg_14_10();
            g__.read(receiveData.GetMemoryStream());
            if (g__.playerList.Count > 0)
            {
                WifiLAN.Instance.StartFindPeer(g__.playerList);
            }
            else
            {
                WifiLAN.Instance.StartFindPeer();
            }
        }

        private void ForceCoordinatorSync_14_4(INetData data)
        {
            WifiPvpForceMoveSyncMsg_14_4 g__ = new WifiPvpForceMoveSyncMsg_14_4();
            g__.read(data.GetMemoryStream());
            bool isForce = g__.isForce;
            foreach (PCoordinator coordinator in g__.coordinator)
            {
                PlayerDisplay player = AppMap.Instance.GetPlayer(coordinator.id.ToString());
                if (((player == null) || (player.GoBase == null)) || (player.GetVo().Camp == 0))
                {
                    break;
                }
                object[] args = new object[] { (coordinator.rotateY * 0.001f) * 0.001f, coordinator.x * 0.001f, coordinator.y * 0.001f, coordinator.z * 0.001f };
                CommandHandler.AddCommandStatic(player.GoBase, string.Format("networkCommand moveTo {0} {1} {2} {3}", args), 1f);
            }
        }

        private void ForceFixCoordinator(ulong p1, int p2, int p3, int p4, int p5)
        {
            <ForceFixCoordinator>c__AnonStoreyEE yee = new <ForceFixCoordinator>c__AnonStoreyEE {
                p1 = p1
            };
            PlayerDisplay display = AppMap.Instance.playerList.FirstOrDefault<PlayerDisplay>(new Func<PlayerDisplay, bool>(yee.<>m__C7));
            if (display != null)
            {
                Vector3 position = display.GoBase.transform.position;
                Vector3 y = new Vector3(p2.FromProtocol(), p3.FromProtocol(), p4.FromProtocol());
                if (DamageCheck.GetDistance(position, y) > 0.5f)
                {
                    display.GoBase.transform.position = y;
                    display.ChangeDire(p5.FromProtocol());
                }
            }
        }

        private static void ForceSyncPos(PlayerDisplay display, int x, int y, int z, int rotateY, bool changeCoorDirectly)
        {
            display.ChangeDire((rotateY * 0.001f) * 0.001f);
            Vector3 vector = new Vector3(x * 0.001f, y * 0.001f, z * 0.001f);
            if (changeCoorDirectly)
            {
                display.GoBase.transform.position = vector;
            }
            else if (DamageCheck.GetDistance(display.GoBase.transform.position, vector) <= 0.3f)
            {
                display.GoBase.transform.position = vector;
            }
            else
            {
                (display.Controller as PlayerControler).WalkTo(vector, null, false, false);
            }
        }

        private IDictionary<ulong, MeDisplay> GetMyGenerals()
        {
            if (this.myGeneralDic.Count == 0)
            {
                this.myGeneralDic.Clear();
                IEnumerator<MeDisplay> enumerator = AppMap.Instance.SelfplayerList.Cast<MeDisplay>().GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        MeDisplay current = enumerator.Current;
                        ulong id = current.GetVo().Id;
                        if (!this.myGeneralDic.ContainsKey(id))
                        {
                            this.myGeneralDic.Add(id, current);
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
            return this.myGeneralDic;
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("3585", new NetMsgCallback(this.EnterPVP_14_1));
            AppNet.main.addCMD("3586", new NetMsgCallback(this.QuitPVP_14_2));
            AppNet.main.addCMD("3587", new NetMsgCallback(this.AddPlayer_14_3));
            AppNet.main.addCMD("3588", new NetMsgCallback(this.ForceCoordinatorSync_14_4));
            AppNet.main.addCMD("3589", new NetMsgCallback(this.UseSkill14_5));
            AppNet.main.addCMD("3590", new NetMsgCallback(this.DamageSync_14_6));
            AppNet.main.addCMD("3591", new NetMsgCallback(this.Dead_14_7));
            AppNet.main.addCMD("3592", new NetMsgCallback(this.EnemyRelive_14_8));
            AppNet.main.addCMD("3593", new NetMsgCallback(this.SyncPosRegularly_14_9));
            AppNet.main.addCMD("3594", new NetMsgCallback(this.FindPeerResult_14_10));
            AppNet.main.addCMD("3596", new NetMsgCallback(this.AddNewPeer_14_12));
            AppNet.main.addCMD("3597", new NetMsgCallback(this.RemovePeer_14_13));
            AppNet.main.addCMD("3598", new NetMsgCallback(this.AddBaseAndTower_14_14));
            AppNet.main.addCMD("3600", new NetMsgCallback(this.AddingPrivate_14_16));
            AppNet.main.addCMD("3601", new NetMsgCallback(this.SyncAddHp_14_17));
            AppNet.main.addCMD("3602", new NetMsgCallback(this.SyncSoul_14_18));
            AppNet.main.addCMD("3604", new NetMsgCallback(this.SyncAddBattleObject_14_20));
            AppNet.main.addCMD("3605", new NetMsgCallback(this.SyncPicked_14_21));
            AppNet.main.addCMD("3606", new NetMsgCallback(this.SummonSync));
        }

        private void QuitPVP_14_2(INetData data)
        {
            WifiPvpEndMsg_14_2 g__ = new WifiPvpEndMsg_14_2();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<MapMode>.Instance.changeScene_4_2();
            }
            else
            {
                MessageManager.Show("切出PVP报错, 错误码" + g__.code);
            }
        }

        private void RemovePeer_14_13(INetData receiveData)
        {
            WifiPvpDelIntranetPlayerMsg_14_13 g__ = new WifiPvpDelIntranetPlayerMsg_14_13();
            g__.read(receiveData.GetMemoryStream());
            WifiLAN.Instance.RemovePeerFromIpList(g__.player);
        }

        private void SummonSync(INetData receiveData)
        {
            WifiPvpAddSummonMsg_14_22 g__ = new WifiPvpAddSummonMsg_14_22();
            g__.read(receiveData.GetMemoryStream());
            ActionDisplay actionDisplayByDisplayTypeAndId = SkillControl.GetActionDisplayByDisplayTypeAndId(DamageCheck.GetDisplayType(g__.syncType), g__.summorId);
            if (actionDisplayByDisplayTypeAndId != null)
            {
                Singleton<MapMode>.Instance.AddSummonMonInMap(g__.monList, actionDisplayByDisplayTypeAndId.GetMeVoByType<BaseRoleVo>().Camp);
            }
        }

        private void SyncAddBattleObject_14_20(INetData receiveData)
        {
            WifiPvpSyncBattleObjectMsg_14_20 g__ = new WifiPvpSyncBattleObjectMsg_14_20();
            g__.read(receiveData.GetMemoryStream());
            Singleton<MapMode>.Instance.AddBattleObject(g__.battleObjectList);
        }

        private void SyncAddHp_14_17(INetData receiveData)
        {
            WifiPvpAddHpMsg_14_17 g__ = new WifiPvpAddHpMsg_14_17();
            g__.read(receiveData.GetMemoryStream());
            this.DoAddHp(g__.id, g__.type, g__.delta, g__.endHp);
        }

        private void SyncPicked_14_21(INetData receiveData)
        {
            WifiPvpBattleObjectPickedMsg_14_21 g__ = new WifiPvpBattleObjectPickedMsg_14_21();
            g__.read(receiveData.GetMemoryStream());
            BattleObjectMgr.Instance.PickedByOthers(g__.battleObjectId, g__.generalId);
        }

        private void SyncPosRegularly_14_9(INetData receiveData)
        {
            WifiPvpPosFixMsg_14_9 g__ = new WifiPvpPosFixMsg_14_9();
            g__.read(receiveData.GetMemoryStream());
            foreach (PCoordinator coordinator in g__.coordinatorList)
            {
                int type = coordinator.type;
                if (type != 1)
                {
                    if ((type == 2) && (coordinator.id != AppMap.Instance.me.GetVo().Id))
                    {
                        PlayerDisplay player = AppMap.Instance.GetPlayer(coordinator.id + string.Empty);
                        if ((((player != null) && (player.GoBase != null)) && (player.Controller != null)) && (player.GetVo().Camp != 0))
                        {
                            object[] args = new object[] { (coordinator.rotateY * 0.001f) * 0.001f, coordinator.x * 0.001f, coordinator.y * 0.001f, coordinator.z * 0.001f };
                            CommandHandler.AddCommandStatic(player.GoBase, string.Format("networkCommand moveTo {0} {1} {2} {3}", args), 1f);
                        }
                    }
                }
                else
                {
                    MonsterDisplay monster = AppMap.Instance.GetMonster(coordinator.id + string.Empty);
                    Log.Net("syncpos of monster " + coordinator.id);
                    if ((((monster != null) && (monster.GoBase != null)) && (monster.Controller != null)) && (monster.GetMeVoByType<BaseRoleVo>().Camp != 0))
                    {
                        object[] objArray2 = new object[] { (coordinator.rotateY * 0.001f) * 0.001f, coordinator.x * 0.001f, coordinator.y * 0.001f, coordinator.z * 0.001f };
                        CommandHandler.AddCommandStatic(monster.GoBase, string.Format("networkCommand moveTo {0} {1} {2} {3}", objArray2), 1f);
                    }
                }
            }
        }

        private void SyncSoul_14_18(INetData receiveData)
        {
            WifiPvpSyncSoulMsg_14_18 g__ = new WifiPvpSyncSoulMsg_14_18();
            g__.read(receiveData.GetMemoryStream());
            WifiPvpManager.Instance.UpdateEnemySoul(Convert.ToUInt32(g__.soul));
        }

        private void UseSkill14_5(INetData data)
        {
            ActionDisplay display;
            WifiPvpUseSkillSyncMsg_14_5 g__ = new WifiPvpUseSkillSyncMsg_14_5();
            g__.read(data.GetMemoryStream());
            byte type = g__.type;
            if (type == 1)
            {
                MonsterDisplay monster = AppMap.Instance.GetMonster(g__.id + string.Empty);
                if ((((monster != null) && (monster.GoBase != null)) && (monster.Controller != null)) && (monster.GetMeVoByType<BaseRoleVo>().Camp != 0))
                {
                    if (monster.GetMeVoByType<MonsterVo>().MonsterVO.type != 3)
                    {
                        if (monster.GoBase.GetComponent<NetworkCommandHandler>() != null)
                        {
                            byte targetType = g__.targetType;
                            ulong targetId = g__.targetId;
                            int controllerId = -1;
                            switch (targetType)
                            {
                                case 1:
                                {
                                    MonsterDisplay display7 = AppMap.Instance.GetMonster(targetId + string.Empty);
                                    if ((display7 != null) && (display7.Controller != null))
                                    {
                                        controllerId = display7.Controller.GetControllerId();
                                    }
                                    break;
                                }
                                case 2:
                                {
                                    PlayerDisplay player = AppMap.Instance.GetPlayer(targetId + string.Empty);
                                    if (player != null)
                                    {
                                        controllerId = player.Controller.GetControllerId();
                                    }
                                    break;
                                }
                            }
                            CommandHandler.AddCommandStatic(monster.GoBase, string.Format("networkCommand skill {0} {1} {2}", g__.skillId, controllerId, (g__.eularY * 0.001f) * 0.001f), 1f);
                        }
                        else if (((monster.GoBase != null) && (monster.Controller != null)) && (monster.Controller.SkillController != null))
                        {
                            monster.ChangeDire((g__.eularY * 0.001f) * 0.001f);
                            monster.Controller.SkillController.UseSkillById(g__.skillId, out display, false, false, true, false, null, null, null, -1, null);
                        }
                        else
                        {
                            Log.Net("Enemy monsetr is Null " + monster);
                        }
                    }
                    else
                    {
                        ActionDisplay display8 = (g__.targetType != 1) ? ((ActionDisplay) AppMap.Instance.GetPlayer(g__.targetId + string.Empty)) : ((ActionDisplay) AppMap.Instance.GetMonster(g__.targetId + string.Empty));
                        if (display8 != null)
                        {
                            ActionDisplay targetDisplay = display8;
                            monster.Controller.SkillController.UseSkillById(g__.skillId, out display, false, false, false, true, null, null, targetDisplay, -1, null);
                        }
                    }
                }
            }
            else if ((type == 2) && !this.GetMyGenerals().ContainsKey(g__.id))
            {
                PlayerDisplay display2 = AppMap.Instance.GetPlayer(g__.id + string.Empty);
                if (((display2 != null) && (display2.GoBase != null)) && (display2.Controller != null))
                {
                    if (display2.GoBase.GetComponent<NetworkCommandHandler>() == null)
                    {
                        Log.AI(display2.GoBase, " network go old Fashion use skill ");
                        float num5 = (g__.eularY * 0.001f) * 0.001f;
                        display2.ChangeDire((g__.eularY * 0.001f) * 0.001f);
                        display2.Controller.SkillController.UseSkillById(g__.skillId, out display, false, true, true, true, null, null, null, -1, null);
                    }
                    else
                    {
                        byte num = g__.targetType;
                        ulong num2 = g__.targetId;
                        int num3 = -1;
                        uint skillId = g__.skillId;
                        switch (num)
                        {
                            case 2:
                            {
                                PlayerDisplay display3 = AppMap.Instance.GetPlayer(num2 + string.Empty);
                                if (display3 != null)
                                {
                                    num3 = display3.Controller.GetControllerId();
                                }
                                break;
                            }
                            case 1:
                            {
                                MonsterDisplay display4 = AppMap.Instance.GetMonster(num2 + string.Empty);
                                if (display4 != null)
                                {
                                    num3 = display4.Controller.GetControllerId();
                                }
                                break;
                            }
                        }
                        Log.AI(display2.GoBase, string.Concat(new object[] { " network player Use Skill ", num3, " targetType ", num }));
                        CommandHandler.AddCommandStatic(display2.GoBase, string.Format("networkCommand skill {0} {1} {2}", g__.skillId, num3, (g__.eularY * 0.001f) * 0.001f), 1f);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ForceFixCoordinator>c__AnonStoreyEE
        {
            internal ulong p1;

            internal bool <>m__C7(PlayerDisplay x)
            {
                return (x.GetVo().Id == this.p1);
            }
        }
    }
}

