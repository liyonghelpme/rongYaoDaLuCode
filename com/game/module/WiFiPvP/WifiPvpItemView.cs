﻿namespace com.game.module.WiFiPvP
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net.wifi_pvp;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class WifiPvpItemView : UIWidgetContainer
    {
        private UISprite headIcon;
        private UILabel lv;
        private UILabel name;
        private LobbyServerItem serverItemInfo;
        private GameObject state1;
        private GameObject state2;
        private List<GameObject> strengthList;

        private void itemClickHandler(GameObject go)
        {
            if (this.serverItemInfo != null)
            {
                if (this.serverItemInfo.isAvailable)
                {
                    Singleton<WifiPvpMode>.Instance.SendInvitation(this.serverItemInfo);
                }
                else
                {
                    MessageManager.Show(LanguageManager.GetWord("WifiPvp.EnemyBusy"));
                }
            }
        }

        public void setItemInfo(LobbyServerItem info)
        {
            this.serverItemInfo = info;
            this.headIcon = base.FindInChild<UISprite>("head/icon");
            this.headIcon.spriteName = info.onFightGeneralId.ToString();
            this.name = base.FindInChild<UILabel>("name");
            this.lv = base.FindInChild<UILabel>("lv");
            this.state1 = base.FindChild("states/state1");
            this.state2 = base.FindChild("states/state2");
            this.strengthList = new List<GameObject>();
            this.strengthList.Add(base.FindChild("signal_strength/strength1"));
            this.strengthList.Add(base.FindChild("signal_strength/strength2"));
            this.strengthList.Add(base.FindChild("signal_strength/strength3"));
            this.strengthList.Add(base.FindChild("signal_strength/strength4"));
            this.name.text = info.accountName;
            this.lv.text = "LV. " + info.level;
            this.showState(info.isAvailable);
            this.showSignalStrength((int) info.signal);
            base.onClick = new UIWidgetContainer.VoidDelegate(this.itemClickHandler);
        }

        private void showSignalStrength(int signal)
        {
            for (int i = 0; i < this.strengthList.Count; i++)
            {
                if (i <= signal)
                {
                    this.strengthList[i].SetActive(true);
                }
                else
                {
                    this.strengthList[i].SetActive(false);
                }
            }
        }

        private void showState(bool isAvailable)
        {
            this.state1.SetActive(isAvailable);
            this.state2.SetActive(!isAvailable);
        }
    }
}

