﻿namespace com.game.module.WiFiPvP
{
    using com.game;
    using com.game.consts;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.fight.arpg;
    using com.game.vo;
    using com.liyong;
    using com.net.wifi_pvp;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.display.vo;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using TNet;
    using UnityEngine;
    using UnityLog;

    public class WifiPvpMode : BaseMode<WifiPvpMode>
    {
        private ViewMode _currentMode = ViewMode.selecet;
        [CompilerGenerated]
        private static Func<PGeneralFightAttr, PNewAttr> <>f__am$cache11;
        private System.Collections.Generic.List<PMapMon> baseAndTowerList = new System.Collections.Generic.List<PMapMon>();
        private readonly Dictionary<ActionDisplay, Vector3> coorDic = new Dictionary<ActionDisplay, Vector3>();
        public PVPFighter enemyFighterInfo = new PVPFighter();
        private readonly System.Collections.Generic.List<PCoordinator> forceSyncCoorList = new System.Collections.Generic.List<PCoordinator>();
        private readonly vp_Timer.Handle handle = new vp_Timer.Handle();
        private float lastUpdateTime;
        private readonly System.Collections.Generic.List<LobbyServerItem> lsList = new System.Collections.Generic.List<LobbyServerItem>();
        public PVPFighter myFighterInfo = new PVPFighter();
        private readonly System.Collections.Generic.List<ActionDisplay> needRegularSyncPosList = new System.Collections.Generic.List<ActionDisplay>();
        private System.Collections.Generic.List<PMapMon> privateList = new System.Collections.Generic.List<PMapMon>();
        private readonly System.Collections.Generic.List<PCoordinator> regularSyncCoorList = new System.Collections.Generic.List<PCoordinator>();
        public WifiPvpConst.PVPEndState result;
        private bool startedSync;
        private int syncCount;
        private GeneralFightAttrWifiMsg_2_9 WifiMyHeroInfo;

        private bool CanEnterWiFiPVP()
        {
            bool isInWifiPVP = AppMap.Instance.IsInWifiPVP;
            bool isInDungeon = DungeonMgr.Instance.isInDungeon;
            return (isInWifiPVP && !isInDungeon);
        }

        public void ClearWhenEndPVP()
        {
            this.MyPrivate.Clear();
            this.MyBaseAndTower.Clear();
        }

        public void DamageSync(uint skillId, System.Collections.Generic.List<PDamage> damage, ulong actorId, int actorType)
        {
            MemoryStream msdata = new MemoryStream();
            ushort newSkillSeq = Singleton<SkillMode>.Instance.GetNewSkillSeq();
            Module_14.write_14_6(msdata, newSkillSeq, actorId, DamageCheck.GetSynType(actorType), skillId, damage);
            WifiLAN.Instance.Send(msdata, 14, 6);
        }

        public void Dead(ulong id, Vector3 pos)
        {
            MemoryStream msdata = new MemoryStream();
            int x = pos.x.ToProtocolInt();
            int y = pos.y.ToProtocolInt();
            int z = pos.z.ToProtocolInt();
            Module_14.write_14_7(msdata, id, x, y, z);
            WifiLAN.Instance.Send(msdata, 14, 7);
        }

        private void DelayStartSyncPos()
        {
            vp_Timer.In(1f, new vp_Timer.Callback(this.DoDelayStartSyncPos), null);
        }

        private void DoDelayStartSyncPos()
        {
            ((MeControler) AppMap.Instance.me.Controller).SendPosInWifiPvp();
        }

        public void EnterPVPScence(bool isMaster, int pvpId = 1)
        {
            if (this.CanEnterWiFiPVP())
            {
                MemoryStream msdata = new MemoryStream();
                Vector3 position = AppMap.Instance.me.GoBase.transform.position;
                int x = position.x.ToProtocolInt();
                int y = position.y.ToProtocolInt();
                int z = position.z.ToProtocolInt();
                Module_14.write_14_1(msdata, (uint) pvpId, isMaster, x, y, z);
                AppNet.gameNet.send(msdata, 14, 1);
            }
        }

        public void FirstForceMoveSync(ActionControler ac)
        {
            MemoryStream msdata = new MemoryStream();
            ushort level = (ushort) ac.Me.GetMeVoByType<PlayerVo>().Level;
            ulong id = ac.Me.GetVo().Id;
            int num3 = ac.transform.position.x.ToProtocolInt();
            int num4 = ac.transform.position.y.ToProtocolInt();
            int num5 = ac.transform.position.z.ToProtocolInt();
            int eularAngelsForProtocol = DamageCheck.GetEularAngelsForProtocol(ac.Me.CurDire);
            System.Collections.Generic.List<PCoordinator> list2 = new System.Collections.Generic.List<PCoordinator>();
            PCoordinator item = new PCoordinator {
                id = id,
                x = num3,
                y = num4,
                z = num5,
                lv = level,
                rotateY = eularAngelsForProtocol
            };
            list2.Add(item);
            System.Collections.Generic.List<PCoordinator> coordinator = list2;
            Module_14.write_14_4(msdata, true, coordinator);
            WifiLAN.Instance.Send(msdata, 14, 4);
            if (!this.startedSync)
            {
                this.syncCount = 0;
                this.startedSync = true;
                vp_Timer.In(0.35f, new vp_Timer.Callback(this.SyncPosRegularly), 0, this.handle);
            }
        }

        public int GetEnemyTeamLevel()
        {
            return Convert.ToInt32(this.enemyFighterInfo.acc.accountLevel);
        }

        public int GetMyTeamLevel()
        {
            return Convert.ToInt32(this.myFighterInfo.acc.accountLevel);
        }

        public void PVPEndAndTellOther(WifiPvpConst.PVPEndState state, bool endRightNow)
        {
            this.startedSync = false;
            vp_Timer.CancelTimerByHandle(this.handle);
            WifiLAN.Instance.PVPEndAndTellOther(state, endRightNow);
        }

        public void Relive(ulong id, Vector3 pos)
        {
            MemoryStream msdata = new MemoryStream();
            int x = pos.x.ToProtocolInt();
            int y = pos.y.ToProtocolInt();
            int z = pos.z.ToProtocolInt();
            Module_14.write_14_8(msdata, id, x, y, z);
            WifiLAN.Instance.Send(msdata, 14, 8);
        }

        public void SendInvitation(LobbyServerItem item)
        {
            if (item != null)
            {
                WifiLAN.Instance.SendInvitation(item.IPEP);
            }
        }

        private void SendPrivateBorn()
        {
            this.SendPrivateBorn(this.MyPrivate);
        }

        public void SendPrivateBorn(System.Collections.Generic.List<PMapMon> myPrivate)
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_16(msdata, myPrivate);
            WifiLAN.Instance.Send(msdata, 14, 0x10);
        }

        public void SetHeroInfo(GeneralFightAttrWifiMsg_2_9 msg)
        {
            this.WifiMyHeroInfo = msg;
        }

        public void SetLobbyServerList(TNet.List<ServerList.Entry> list)
        {
            if (((Time.time - this.lastUpdateTime) > 3f) || (list.Count != this.lsList.Count))
            {
                this.lsList.Clear();
                IEnumerator<ServerList.Entry> enumerator = list.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        ServerList.Entry current = enumerator.Current;
                        this.lsList.Add(LobbyServerItem.GetLobbyServerItem(current));
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                base.DataUpdate(3);
                this.lastUpdateTime = Time.time;
            }
        }

        public void ShowResultAndSwitchToCity(WifiPvpConst.PVPEndState status)
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_2(msdata, (byte) status);
            AppNet.gameNet.send(msdata, 14, 2);
        }

        public void StartFindPeer()
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_10(msdata);
            AppNet.gameNet.send(msdata, 14, 10);
        }

        public void StopFindingPeer()
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_11(msdata);
            AppNet.gameNet.send(msdata, 14, 11);
            WifiLAN.Instance.StopFindingPeer();
        }

        public void SyncAddHp(MeDisplay display, uint delta)
        {
            MemoryStream msdata = new MemoryStream();
            ulong id = display.GetVo().Id;
            byte synType = (byte) DamageCheck.GetSynType(display.Type);
            uint hp = display.GetVo().Hp;
            Module_14.write_14_17(msdata, id, synType, delta, hp);
            WifiLAN.Instance.Send(msdata, 14, 0x11);
        }

        public void SyncAddSummon(MapMonBornSightMsg_4_23 msg)
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_22(msdata, msg.summorId, msg.syncType, msg.monList);
            WifiLAN.Instance.Send(msdata, 14, 0x16);
        }

        public void SyncBattleObjectToOthers(System.Collections.Generic.List<PBattleObject> list)
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_20(msdata, list);
            WifiLAN.Instance.Send(msdata, 14, 20);
        }

        private void SyncHpRegularly()
        {
            foreach (PlayerDisplay display in AppMap.Instance.SelfplayerList)
            {
                this.SyncHpVal(display, display.GetVo().CurHp);
            }
        }

        public void SyncHpVal(ActionDisplay display, uint curHp)
        {
            Log.AI(display.GoBase, " SyncHpVal " + curHp);
            MemoryStream msdata = new MemoryStream();
            ulong id = display.GetVo().Id;
            byte synType = (byte) DamageCheck.GetSynType(display.Type);
            uint endHp = curHp;
            Module_14.write_14_17(msdata, id, synType, 0, endHp);
            WifiLAN.Instance.Send(msdata, 14, 0x11);
        }

        private System.Collections.Generic.List<PCoordinator> SyncMyHeroCoord()
        {
            System.Collections.Generic.List<PCoordinator> list = new System.Collections.Generic.List<PCoordinator>();
            foreach (PlayerDisplay display in AppMap.Instance.SelfplayerList)
            {
                PlayerVo meVoByType = display.GetMeVoByType<PlayerVo>();
                Vector3 position = display.GoBase.transform.position;
                WifiPvpManager.Instance.AddPlayer(display);
                PCoordinator item = new PCoordinator {
                    id = meVoByType.Id,
                    lv = Convert.ToUInt16(meVoByType.Level),
                    x = position.x.ToProtocolInt(),
                    y = position.y.ToProtocolInt(),
                    z = position.z.ToProtocolInt(),
                    type = DamageCheck.GetSynType(display.Type),
                    rotateY = DamageCheck.GetEularAngelsForProtocol(display.CurDire)
                };
                list.Add(item);
            }
            return list;
        }

        private void SyncPos(ActionDisplay display)
        {
            PCoordinator item = new PCoordinator {
                id = display.GetVo().Id,
                type = DamageCheck.GetSynType(display.Type),
                lv = Convert.ToUInt16(display.GetMeVoByType<BaseRoleVo>().Level),
                x = display.GoBase.transform.position.x.ToProtocolInt(),
                y = display.GoBase.transform.position.y.ToProtocolInt(),
                z = display.GoBase.transform.position.z.ToProtocolInt(),
                rotateY = DamageCheck.GetEularAngelsForProtocol(display.CurDire)
            };
            this.regularSyncCoorList.Clear();
            this.regularSyncCoorList.Add(item);
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_9(msdata, this.regularSyncCoorList);
            WifiLAN.Instance.Send(msdata, 14, 9);
        }

        private void SyncPosRegularly()
        {
            PCoordinator coordinator;
            if (!AppMap.Instance.IsInWifiPVP)
            {
                vp_Timer.CancelTimerByHandle(this.handle);
                this.startedSync = false;
            }
            this.syncCount++;
            this.forceSyncCoorList.Clear();
            this.regularSyncCoorList.Clear();
            this.needRegularSyncPosList.Clear();
            if ((this.syncCount % 3) == 0)
            {
                foreach (PlayerDisplay display in AppMap.Instance.SelfplayerList)
                {
                    PlayerVo meVoByType = display.GetMeVoByType<PlayerVo>();
                    Vector3 position = display.GoBase.transform.position;
                    coordinator = new PCoordinator {
                        id = meVoByType.Id,
                        lv = Convert.ToUInt16(meVoByType.Level),
                        x = position.x.ToProtocolInt(),
                        y = position.y.ToProtocolInt(),
                        z = position.z.ToProtocolInt(),
                        rotateY = DamageCheck.GetEularAngelsForProtocol(display.CurDire),
                        type = DamageCheck.GetSynType(display.Type)
                    };
                    this.forceSyncCoorList.Add(coordinator);
                }
                this.syncCount = 0;
            }
            else
            {
                foreach (PlayerDisplay display2 in AppMap.Instance.SelfplayerList)
                {
                    this.needRegularSyncPosList.Add(display2);
                }
            }
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    MonsterVo vo2 = current.GetMeVoByType<MonsterVo>();
                    if ((vo2.Camp == 0) && ((vo2.MonsterVO.type == 4) || (vo2.MonsterVO.type == 6)))
                    {
                        this.needRegularSyncPosList.Add(current);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            foreach (ActionDisplay display4 in this.needRegularSyncPosList)
            {
                if (!this.coorDic.ContainsKey(display4))
                {
                    this.coorDic.Add(display4, display4.GoBase.transform.position);
                }
                else if (DamageCheck.GetDistance(this.coorDic[display4], display4.GoBase.transform.position) >= 0.3f)
                {
                    this.coorDic.Remove(display4);
                    this.coorDic.Add(display4, display4.GoBase.transform.position);
                    coordinator = new PCoordinator {
                        id = display4.GetVo().Id,
                        type = DamageCheck.GetSynType(display4.Type),
                        lv = Convert.ToUInt16(display4.GetMeVoByType<BaseRoleVo>().Level),
                        x = display4.GoBase.transform.position.x.ToProtocolInt(),
                        y = display4.GoBase.transform.position.y.ToProtocolInt(),
                        z = display4.GoBase.transform.position.z.ToProtocolInt(),
                        rotateY = DamageCheck.GetEularAngelsForProtocol(display4.CurDire)
                    };
                    this.regularSyncCoorList.Add(coordinator);
                }
            }
            if (this.regularSyncCoorList.Count > 0)
            {
                MemoryStream msdata = new MemoryStream();
                Module_14.write_14_9(msdata, this.regularSyncCoorList);
                WifiLAN.Instance.Send(msdata, 14, 9);
            }
            if (this.forceSyncCoorList.Count > 0)
            {
                MemoryStream stream2 = new MemoryStream();
                Module_14.write_14_4(stream2, false, this.forceSyncCoorList);
                WifiLAN.Instance.Send(stream2, 14, 4);
            }
        }

        public void SyncSoul(uint soul)
        {
            MemoryStream msdata = new MemoryStream();
            byte num = Convert.ToByte(MeVo.instance.soul);
            Module_14.write_14_18(msdata, num);
            WifiLAN.Instance.Send(msdata, 14, 0x12);
        }

        private void SyncTowerBase(System.Collections.Generic.List<PMapMon> baseAndTower)
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_14(msdata, baseAndTower);
            WifiLAN.Instance.Send(msdata, 14, 14);
            foreach (PMapMon mon in this.MyBaseAndTower)
            {
                SysMonsterVo vo = BaseDataMgr.instance.getSysMonsterVo(mon.monid);
                if (vo.type == 5)
                {
                    MonsterDisplay monster = AppMap.Instance.GetMonster(mon.id.ToString());
                    if (monster != null)
                    {
                        WifiPvpManager.Instance.AddMyBase(monster);
                        ObjectManager.AddHealthBar(monster);
                    }
                    else
                    {
                        Debug.LogError("注册我方基地时为null");
                    }
                }
                else if (vo.type == 3)
                {
                    MonsterDisplay tower = AppMap.Instance.GetMonster(mon.id.ToString());
                    if (tower != null)
                    {
                        WifiPvpManager.Instance.AddMyTower(tower);
                        ObjectManager.AddHealthBar(tower);
                    }
                    else
                    {
                        Debug.LogError("注册我方防御塔时为null");
                    }
                }
            }
        }

        public void TellMyHeroTowerAndBaseToOther(WifiPvpConst.Sender sender)
        {
            this.TellMyHeroTowerAndBaseToOther(sender, this.WifiMyHeroInfo, this.MyBaseAndTower);
        }

        public void TellMyHeroTowerAndBaseToOther(WifiPvpConst.Sender sender, GeneralFightAttrWifiMsg_2_9 wifiAttribute, System.Collections.Generic.List<PMapMon> baseAndTower)
        {
            System.Collections.Generic.List<PCoordinator> coordinator = this.SyncMyHeroCoord();
            if (<>f__am$cache11 == null)
            {
                <>f__am$cache11 = info => info.attr;
            }
            System.Collections.Generic.List<PNewAttr> generalAttr = wifiAttribute.generalAttr.Select<PGeneralFightAttr, PNewAttr>(<>f__am$cache11).ToList<PNewAttr>();
            System.Collections.Generic.List<PNewAttr> generalEquipAttr = wifiAttribute.generalEquipAttr;
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_3(msdata, (byte) sender, coordinator, generalAttr, generalEquipAttr);
            WifiLAN.Instance.Send(msdata, 14, 3);
            this.SyncTowerBase(baseAndTower);
            this.DelayStartSyncPos();
        }

        public void TellOthersBattleObjectPicked(ActionDisplay me, BattleObjectVo vo)
        {
            MemoryStream msdata = new MemoryStream();
            ulong id = me.GetVo().Id;
            int[] numArray = me.GoBase.transform.position.ToProtocolInt();
            int rotateY = me.CurDire.ToProtocolInt();
            Module_14.write_14_21(msdata, id, numArray[0], numArray[1], numArray[2], rotateY, vo.Id);
            WifiLAN.Instance.Send(msdata, 14, 0x15);
        }

        public void UseSkillSync(ulong id, byte type, uint skillid, Vector3 eular, ulong target_id, byte target_type, ActionDisplay attacker)
        {
            Log.Net(string.Concat(new object[] { id, " UseSkillSync ", type, " target ", target_id, " skillId ", skillid }));
            this.SyncPos(attacker);
            MemoryStream msdata = new MemoryStream();
            int eularAngelsForProtocol = DamageCheck.GetEularAngelsForProtocol(eular.y);
            Module_14.write_14_5(msdata, id, type, skillid, eularAngelsForProtocol, target_id, target_type);
            WifiLAN.Instance.Send(msdata, 14, 5);
        }

        public ViewMode currentMode
        {
            get
            {
                return this._currentMode;
            }
            set
            {
                this._currentMode = value;
            }
        }

        public bool IsMaster
        {
            get
            {
                return WifiLAN.Instance.IsMaster;
            }
            set
            {
                WifiLAN.Instance.IsMaster = value;
            }
        }

        public System.Collections.Generic.List<PMapMon> MyBaseAndTower
        {
            get
            {
                return this.baseAndTowerList;
            }
            set
            {
                this.baseAndTowerList = value;
            }
        }

        public System.Collections.Generic.List<PMapMon> MyPrivate
        {
            private get
            {
                return this.privateList;
            }
            set
            {
                this.privateList = value;
                this.SendPrivateBorn();
            }
        }

        public System.Collections.Generic.List<LobbyServerItem> PeerList
        {
            get
            {
                return this.lsList;
            }
        }

        public LobbyServerItem selecetServerItem { get; set; }

        public enum ViewMode
        {
            confrom = 2,
            pop = 3,
            selecet = 1,
            win = 4
        }
    }
}

