﻿namespace com.game.module.WiFiPvP
{
    using com.game.manager;
    using com.game.module.core;
    using com.net.wifi_pvp;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class WifiPvpSelecetView : BaseView<WifiPvpSelecetView>
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map1D;
        private Button about_btn;
        private UIGrid grid;
        private UILabel label_random;
        private UILabel label_wifi;
        private UIPanel listPanel;
        private Button pop_close;
        private Button pvp_btn;
        private GameObject pvpItem;
        private Button random_btn;
        private GameObject wifi_pop;

        public override void CancelUpdateHandler()
        {
            Singleton<WifiPvpMode>.Instance.dataUpdated -= new DataUpateHandler(this.DataUpdated);
        }

        private void clearGridItemView()
        {
            for (int i = 1; i < this.grid.transform.childCount; i++)
            {
                GameObject gameObject = this.grid.transform.GetChild(i).gameObject;
                if (gameObject != null)
                {
                    UnityEngine.Object.Destroy(gameObject);
                }
            }
        }

        private void createGridItemView(LobbyServerItem info)
        {
            GameObject obj2 = NGUITools.AddChild(this.grid.gameObject, this.pvpItem);
            obj2.SetActive(true);
            WifiPvpItemView view = obj2.AddComponent<WifiPvpItemView>();
            view.onClick = new UIWidgetContainer.VoidDelegate(this.itemViewClickHandler);
            view.setItemInfo(info);
        }

        public override void DataUpdated(object sender, int code)
        {
            this.clearGridItemView();
            IEnumerator<LobbyServerItem> enumerator = Singleton<WifiPvpMode>.Instance.PeerList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    LobbyServerItem current = enumerator.Current;
                    this.createGridItemView(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.grid.repositionNow = true;
        }

        protected override void HandleAfterOpenView()
        {
            this.popPanelState(false);
        }

        protected override void HandleBeforeCloseView()
        {
            this.popPanelState(false);
            this.clearGridItemView();
        }

        private void HandleBeforePopPanel()
        {
            this.popPanelState(true);
        }

        protected override void Init()
        {
            this.random_btn = base.FindInChild<Button>("buttons/random");
            this.label_random = base.FindInChild<UILabel>("buttons/random/Label");
            this.label_random.text = LanguageManager.GetWord("WifiPvp.SelectRandomBtn");
            this.pvp_btn = base.FindInChild<Button>("buttons/wifi");
            this.label_wifi = base.FindInChild<UILabel>("buttons/wifi/Label");
            this.label_wifi.text = LanguageManager.GetWord("WifiPvp.SelectWifiBtn");
            this.about_btn = base.FindInChild<Button>("buttons/about");
            this.wifi_pop = base.FindChild("wifi_pop");
            this.pop_close = base.FindInChild<Button>("wifi_pop/close");
            this.grid = base.FindInChild<UIGrid>("wifi_pop/list/Grid");
            this.listPanel = base.FindInChild<UIPanel>("wifi_pop/list");
            this.pvpItem = base.FindChild("wifi_pop/list/Grid/item");
            this.random_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.pvp_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.about_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.pop_close.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.popPanelState(false);
        }

        private void itemViewClickHandler(GameObject go)
        {
            WifiPvpItemView component = go.GetComponent<WifiPvpItemView>();
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map1D == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                    dictionary.Add("random", 0);
                    dictionary.Add("wifi", 1);
                    dictionary.Add("about", 2);
                    dictionary.Add("close", 3);
                    <>f__switch$map1D = dictionary;
                }
                if (<>f__switch$map1D.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 1:
                            this.HandleBeforePopPanel();
                            break;

                        case 3:
                            this.popPanelState(false);
                            break;
                    }
                }
            }
        }

        private void popPanelState(bool state)
        {
            this.wifi_pop.SetActive(state);
            this.pop_close.SetActive(state);
            if (state)
            {
                Singleton<WifiPvpMode>.Instance.StartFindPeer();
            }
            else
            {
                Singleton<WifiPvpMode>.Instance.StopFindingPeer();
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<WifiPvpMode>.Instance.dataUpdated += new DataUpateHandler(this.DataUpdated);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

