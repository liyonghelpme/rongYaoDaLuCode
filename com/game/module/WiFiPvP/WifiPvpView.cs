﻿namespace com.game.module.WiFiPvP
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class WifiPvpView : BaseView<WifiPvpView>
    {
        private MainToolView _toolBarView;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map1E;
        private GameObject backgroud;
        private Button close_btn;
        private GameObject commonPanel;

        protected override void HandleAfterOpenView()
        {
            this.HandleBeforeCloseView();
            this._toolBarView.isActive = true;
            this.backgroud.SetActive(true);
            switch (Singleton<WifiPvpMode>.Instance.currentMode)
            {
                case WifiPvpMode.ViewMode.selecet:
                    this.commonPanel.SetActive(true);
                    this.close_btn.SetActive(true);
                    Singleton<WifiPvpSelecetView>.Instance.OpenView();
                    return;

                case WifiPvpMode.ViewMode.confrom:
                    this.commonPanel.SetActive(true);
                    this.close_btn.SetActive(true);
                    Singleton<WifiPvpConfromView>.Instance.OpenView();
                    return;

                case WifiPvpMode.ViewMode.win:
                    this.commonPanel.SetActive(false);
                    this.close_btn.SetActive(true);
                    Singleton<WifiPvpWinView>.Instance.OpenView();
                    return;
            }
            Debug.LogError("意外的ViewMode");
        }

        protected override void HandleBeforeCloseView()
        {
            this._toolBarView.isActive = false;
            this.commonPanel.SetActive(false);
            this.backgroud.SetActive(false);
            Singleton<WifiPvpSelecetView>.Instance.CloseView();
            Singleton<WifiPvpConfromView>.Instance.CloseView();
            Singleton<WifiPvpWinView>.Instance.CloseView();
        }

        protected override void Init()
        {
            this.close_btn = base.FindInChild<Button>("close");
            this.commonPanel = base.FindChild("commonPanel");
            this.backgroud = base.FindChild("background");
            Singleton<WifiPvpSelecetView>.Instance.gameObject = base.FindChild("commonPanel/selecet");
            Singleton<WifiPvpConfromView>.Instance.gameObject = base.FindChild("commonPanel/confrom");
            Singleton<WifiPvpWinView>.Instance.gameObject = base.FindChild("winPanel");
            this.close_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this._toolBarView = new MainToolView(base.FindChild("commonPanel/info"));
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map1E == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(1);
                    dictionary.Add("close", 0);
                    <>f__switch$map1E = dictionary;
                }
                if (<>f__switch$map1E.TryGetValue(name, out num) && (num == 0))
                {
                    switch (Singleton<WifiPvpMode>.Instance.currentMode)
                    {
                        case WifiPvpMode.ViewMode.confrom:
                            WifiPvpManager.Instance.GiveUp();
                            return;

                        case WifiPvpMode.ViewMode.win:
                            WifiPvpManager.Instance.HandleCloseEndView();
                            this.CloseView();
                            Singleton<WifiPvpMode>.Instance.ShowResultAndSwitchToCity(Singleton<WifiPvpMode>.Instance.result);
                            return;
                    }
                    this.CloseView();
                }
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/WIFIPVP/WIFIPVPView.assetbundle";
            }
        }
    }
}

