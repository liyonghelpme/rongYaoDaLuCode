﻿namespace com.game.module.WiFiPvP
{
    using com.game.consts;
    using com.game.manager;
    using com.game.module.core;
    using com.net.wifi_pvp;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class WifiPvpWinView : BaseView<WifiPvpWinView>
    {
        [CompilerGenerated]
        private static UIWidgetContainer.VoidDelegate <>f__am$cacheA;
        private GameObject award;
        private Button clickToClose;
        private UILabel lb_award_exp;
        private UILabel lb_award_gold;
        private UILabel lb_pvpname;
        private UILabel lb_timecost;
        private GameObject states;
        private GameObject teamA;
        private GameObject teamB;
        private GameObject title;

        private string GetKillCountString(FightCounter fc)
        {
            string[] textArray1 = new string[] { "(", fc.Key.ToString(), " / ", fc.Value.ToString(), ")" };
            return string.Concat(textArray1);
        }

        protected override void HandleAfterOpenView()
        {
            if (Singleton<SystemSettingsView>.Instance.IsOpened)
            {
                Singleton<SystemSettingsView>.Instance.CloseView();
            }
            PVPFighter myFighterInfo = Singleton<WifiPvpMode>.Instance.myFighterInfo;
            PVPFighter enemyFighterInfo = Singleton<WifiPvpMode>.Instance.enemyFighterInfo;
            if (((myFighterInfo == null) || (enemyFighterInfo == null)) || ((myFighterInfo.genList.Count == 0) || (enemyFighterInfo.genList.Count == 0)))
            {
                Debug.LogError("PVPFighter 有问题");
            }
            else
            {
                this.InitTitle();
                this.InitAward();
                this.InitFighter(myFighterInfo.genList, true);
                this.InitFighter(enemyFighterInfo.genList, false);
                this.ShowWinOrLose(Singleton<WifiPvpMode>.Instance.result);
            }
        }

        protected override void Init()
        {
            this.title = base.FindChild("title");
            this.states = base.FindChild("states");
            this.teamA = base.FindChild("teamA");
            this.teamB = base.FindChild("teamB");
            this.award = base.FindChild("award");
            this.clickToClose = base.FindInChild<Button>("mask");
            this.lb_award_exp = NGUITools.FindInChild<UILabel>(this.award, "exp/num");
            this.lb_award_gold = NGUITools.FindInChild<UILabel>(this.award, "gold/num");
            this.lb_timecost = NGUITools.FindInChild<UILabel>(this.title, "time");
            this.lb_pvpname = NGUITools.FindInChild<UILabel>(this.title, "pvp_name");
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = delegate (GameObject go) {
                    WifiPvpManager.Instance.HandleCloseEndView();
                    Singleton<WifiPvpView>.Instance.CloseView();
                    Singleton<WifiPvpMode>.Instance.ShowResultAndSwitchToCity(Singleton<WifiPvpMode>.Instance.result);
                };
            }
            this.clickToClose.onClick = <>f__am$cacheA;
        }

        private void InitAward()
        {
            this.lb_award_exp.text = UnityEngine.Random.Range(300, 0x3e8).ToString();
            this.lb_award_gold.text = UnityEngine.Random.Range(0x2435, 0x1ff08).ToString();
        }

        private void InitFighter(IEnumerable<PVPGeneralBrief> list, bool isMe)
        {
            GameObject parent = !(Singleton<WifiPvpMode>.Instance.IsMaster ^ isMe) ? this.teamA : this.teamB;
            GameObject obj3 = NGUITools.FindChild(parent, "hero1");
            GameObject obj4 = NGUITools.FindChild(parent, "hero2");
            GameObject obj5 = NGUITools.FindChild(parent, "hero3");
            UILabel label = NGUITools.FindInChild<UILabel>(parent, "num");
            GameObject[] objArray = new GameObject[] { obj3, obj4, obj5 };
            foreach (GameObject obj6 in objArray)
            {
                obj6.SetActive(false);
            }
            int num2 = 0;
            IEnumerator<PVPGeneralBrief> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PVPGeneralBrief current = enumerator.Current;
                    FightCounter pvpStaticByTid = WifiPvpManager.Instance.GetPvpStaticByTid(Convert.ToUInt64(current.id), isMe);
                    GameObject obj7 = objArray[num2++];
                    UILabel label2 = NGUITools.FindInChild<UILabel>(obj7, "name");
                    UISprite sprite = NGUITools.FindInChild<UISprite>(obj7, "head/icon");
                    UILabel label3 = NGUITools.FindInChild<UILabel>(obj7, "kill/num");
                    label2.text = current.name;
                    sprite.spriteName = current.generalTid;
                    if (pvpStaticByTid == null)
                    {
                        label3.text = "(0 / 0)";
                        Debug.Log("本应出现在结算面板中, 但取数据为空, GeneralTid = " + current.generalTid);
                    }
                    else
                    {
                        label3.text = this.GetKillCountString(pvpStaticByTid);
                    }
                    obj7.SetActive(true);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            label.text = (!isMe ? WifiPvpManager.Instance.EnemyTotalKill : WifiPvpManager.Instance.TotalKill).ToString();
        }

        private void InitTitle()
        {
            int timecost = WifiPvpManager.Instance.GetTimecost();
            string str = (timecost / 60).ToString();
            string str2 = (timecost % 60).ToString();
            string[] param = new string[] { str.ToString(), str2.ToString() };
            this.lb_timecost.text = LanguageManager.GetWord("WifiPvp.WinTitleTimecost", param);
            if (WifiPvpManager.Instance.PvpTemplate != null)
            {
                string[] textArray2 = new string[] { WifiPvpManager.Instance.PvpTemplate.name };
                this.lb_pvpname.text = LanguageManager.GetWord("WifiPvp.WinTitleBattlename", textArray2);
            }
        }

        private void ShowWinOrLose(WifiPvpConst.PVPEndState state)
        {
            UISprite rect = NGUITools.FindInChild<UISprite>(this.states, "state1");
            UISprite sprite2 = NGUITools.FindInChild<UISprite>(this.states, "state2");
            if (state != WifiPvpConst.PVPEndState.fail)
            {
                rect.SetActive(true);
                sprite2.SetActive(false);
            }
            else
            {
                rect.SetActive(false);
                sprite2.SetActive(true);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

