﻿namespace com.game.module.bag
{
    using System;

    public class BagCell : ItemCell
    {
        public int realCount;

        protected override void UpdateLabel()
        {
            if (null != base.label)
            {
                if ((base.itemInfo != null) && (base.itemInfo.count > 1))
                {
                    base.label.text = this.realCount.ToString();
                }
                else
                {
                    base.label.text = string.Empty;
                }
            }
        }
    }
}

