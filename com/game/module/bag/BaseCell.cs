﻿namespace com.game.module.bag
{
    using com.game.module.core;
    using System;

    public abstract class BaseCell : UIWidgetContainer
    {
        public UISprite background;
        public UISprite highLight;
        public UISprite icon;
        public UILabel label;

        protected BaseCell()
        {
        }

        protected virtual void AfterUpdateIcon()
        {
        }

        protected UIAtlas AtlasOfName(string atlasName)
        {
            return Singleton<AtlasManager>.Instance.GetAtlas(atlasName);
        }

        private void Awake()
        {
            this.InitView();
        }

        protected virtual void BeforeUpdateIcon()
        {
        }

        protected virtual void InitView()
        {
            if (this.background == null)
            {
                this.background = base.FindInChild<UISprite>("background");
            }
            if (this.highLight == null)
            {
                this.highLight = base.FindInChild<UISprite>("highlight");
            }
            if (this.label == null)
            {
                this.label = base.FindInChild<UILabel>("label");
            }
            if (null == this.icon)
            {
                this.icon = NGUITools.FindInChild<UISprite>(base.gameObject, this.iconPath);
            }
        }

        public void UpdateCell()
        {
            this.BeforeUpdateIcon();
            this.UpdateIcon();
            this.AfterUpdateIcon();
            this.UpdateLabel();
        }

        protected virtual void UpdateIcon()
        {
            if (null == this.icon)
            {
                this.icon = NGUITools.FindInChild<UISprite>(base.gameObject, this.iconPath);
            }
            if (null != this.icon)
            {
                if (string.IsNullOrEmpty(this.iconPath))
                {
                    this.icon.atlas = null;
                }
                else
                {
                    this.icon.atlas = this.AtlasOfName(this.iconAtlasPath);
                }
                this.icon.spriteName = this.iconSpriteName;
            }
        }

        protected virtual void UpdateLabel()
        {
        }

        protected abstract string iconAtlasPath { get; }

        protected virtual string iconPath
        {
            get
            {
                return "icon";
            }
        }

        protected abstract string iconSpriteName { get; }
    }
}

