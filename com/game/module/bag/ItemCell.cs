﻿namespace com.game.module.bag
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Equipment.components;
    using com.game.module.Equipment.utils;
    using PCustomDataType;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ItemCell : BaseCell
    {
        private PItems _itemInfo;
        private SysItemsVo _sysItemsVo;
        private string[,] background_color;
        private readonly string[,] color;
        private const int ITEM_ALL = 0;
        private const int ITEM_SUIPIAN = 1;
        public UISprite quality;
        public UISprite tagType;

        public ItemCell()
        {
            string[] textArray1 = new string[,] { { "wupinkuang", "wupinkuang" }, { "wupinkuang-G-zhezhao", "wupinkuang-G-zhezhao" }, { "wupinkuang-B-zhezhao", "wupinkuang-B-zhezhao" }, { "wupinkuang-P-zhezhao", "wupinkuang-P-zhezhao" }, { "wupinkuang-O-zhezhao", "wupinkuang-O-zhezhao" }, { "wupinkuang-Y-zhezhao", "wupinkuang-Y-zhezhao" } };
            this.color = textArray1;
            string[] textArray2 = new string[,] { { "wupinkuang", "wupinkuang" }, { "wupinkuang-G", "wupinkuang-G" }, { "wupinkuang-B", "wupinkuang-B" }, { "wupinkuang-P", "wupinkuang-P" }, { "wupinkuang-O", "wupinkuang-O" }, { "wupinkuang-Y", "wupinkuang-Y" } };
            this.background_color = textArray2;
        }

        protected override void AfterUpdateIcon()
        {
            if (base.background == null)
            {
                base.background = NGUITools.FindInChild<UISprite>(base.gameObject, "background");
            }
            if (base.background != null)
            {
                base.background.spriteName = this.GetQualityName("background");
                base.background.SetActive(true);
            }
            if (null == this.quality)
            {
                this.quality = NGUITools.FindInChild<UISprite>(base.gameObject, this.borderPath);
            }
            if (null != this.quality)
            {
                this.quality.spriteName = this.GetQualityName("quality");
                if (this.itemVo != null)
                {
                    this.quality.SetActive(this.itemVo.color > 1);
                }
            }
            if (this.tagType == null)
            {
                this.tagType = NGUITools.FindInChild<UISprite>(base.gameObject, "type");
            }
            if (this.tagType != null)
            {
                this.tagType.SetActive(false);
                if ((this.itemVo != null) && (this.itemVo.tagtype == 3))
                {
                    this.tagType.SetActive(true);
                }
            }
        }

        public string GetQualityName(string type = "background")
        {
            SysItemsVo itemVo = this.itemVo;
            string str = !(type == "background") ? this.color[0, 0] : this.background_color[0, 0];
            if (null == this.quality)
            {
                return str;
            }
            if (itemVo == null)
            {
                return string.Empty;
            }
            int num = 0;
            if (itemVo.tagtype == 3)
            {
                num = 1;
            }
            return (!(type == "background") ? this.color[itemVo.color - 1, num] : this.background_color[itemVo.color - 1, num]);
        }

        public SysItemsVo GetSysVo()
        {
            if (this._itemInfo == null)
            {
                return null;
            }
            if (this._sysItemsVo == null)
            {
                this._sysItemsVo = this._sysItemsVo;
                return BaseDataMgr.instance.GetDataById<SysItemsVo>(this._itemInfo.templateId);
            }
            return this._sysItemsVo;
        }

        public string GetTips()
        {
            if ((this.itemVo.type != 1) && (this.itemVo.type != 2))
            {
                return this.itemVo.use_tips;
            }
            return AttTips.GetNoAddAttriContent(ParseStringUtils.ParseAddAttri(this.itemVo.property, 1f));
        }

        protected override void InitView()
        {
            base.InitView();
            if (base.onClick == null)
            {
                base.onClick = new UIWidgetContainer.VoidDelegate(this.ShowTips);
            }
        }

        public void SetItemsVo(SysItemsVo vo)
        {
            this._sysItemsVo = vo;
        }

        public void ShowTips(GameObject go)
        {
            if (this.itemVo != null)
            {
                Singleton<ItemCellTipsView>.Instance.SetInfo(this.itemVo, this);
            }
        }

        protected override void UpdateLabel()
        {
            if (null != base.label)
            {
                if ((this.itemInfo != null) && (this.itemInfo.count > 1))
                {
                    base.label.text = this.itemInfo.count.ToString();
                }
                else
                {
                    base.label.text = string.Empty;
                }
            }
        }

        protected virtual string borderPath
        {
            get
            {
                return "qualityFrame";
            }
        }

        protected override string iconAtlasPath
        {
            get
            {
                if (this._itemInfo == null)
                {
                    return string.Empty;
                }
                return "ItemIconAtlas";
            }
        }

        protected override string iconSpriteName
        {
            get
            {
                SysItemsVo sysVo = this.GetSysVo();
                return (string.Empty + sysVo.icon);
            }
        }

        public PItems itemInfo
        {
            get
            {
                return this._itemInfo;
            }
            set
            {
                if (value != this._itemInfo)
                {
                    this._itemInfo = value;
                    base.UpdateCell();
                }
            }
        }

        public virtual SysItemsVo itemVo
        {
            get
            {
                return this.GetSysVo();
            }
        }
    }
}

