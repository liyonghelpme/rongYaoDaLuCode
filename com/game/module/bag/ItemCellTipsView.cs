﻿namespace com.game.module.bag
{
    using com.game.data;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using UnityEngine;

    internal class ItemCellTipsView : BaseView<ItemCellTipsView>
    {
        private UISprite backGround;
        private Button bgBtn;
        private UILabel descLabel;
        private ItemCell itemCell;
        private UIPanel itemCellPanel;
        private UISprite itemIcon;
        private PItems itemInfo;
        private UILabel itemNameLabel;
        private SysItemsVo itemTemplate;
        private UISprite qualityFrame;
        private UILabel tipsLabel;

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        private void EquipBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            this.itemIcon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("ItemIconAtlas");
            this.itemIcon.spriteName = this.itemTemplate.icon.ToString();
            this.itemIcon.MakePixelPerfect();
            this.itemNameLabel.text = this.itemTemplate.name;
            this.descLabel.text = this.itemTemplate.desc;
            this.qualityFrame.spriteName = this.itemCell.GetQualityName("background");
            this.tipsLabel.text = this.itemCell.GetTips();
            this.RecountAllPosition();
            base.HandleAfterOpenView();
        }

        protected override void Init()
        {
            this.itemIcon = base.FindInChild<UISprite>("panel/itemCell/icon");
            this.qualityFrame = base.FindInChild<UISprite>("panel/itemCell/background");
            this.itemNameLabel = base.FindInChild<UILabel>("panel/itemCell/Label");
            this.tipsLabel = base.FindInChild<UILabel>("panel/tips");
            this.descLabel = base.FindInChild<UILabel>("panel/desc");
            this.bgBtn = base.FindInChild<Button>("panel/bgBtn");
            this.backGround = base.FindInChild<UISprite>("panel/bg");
            this.itemCellPanel = base.FindInChild<UIPanel>("panel/itemCell");
            this.InitEvent();
        }

        private void InitEvent()
        {
            this.bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHandler);
        }

        private void RecountAllPosition()
        {
            int num = this.itemIcon.height + 10;
            int height = this.descLabel.height;
            int num3 = this.tipsLabel.height;
            int num4 = ((num + height) + num3) + 15;
            this.backGround.height = num4;
            int num5 = (num4 - num) / 2;
            Vector3 localPosition = this.itemCellPanel.transform.localPosition;
            localPosition.y = num5 - 2;
            this.itemCellPanel.transform.localPosition = localPosition;
            Vector3 vector2 = this.backGround.transform.localPosition;
            vector2.y = 0f;
            this.backGround.transform.localPosition = vector2;
            Vector3 vector3 = this.tipsLabel.transform.localPosition;
            vector3.y = ((localPosition.y - (num / 2)) - (num3 / 2)) - 5f;
            this.tipsLabel.transform.localPosition = vector3;
            Vector3 vector4 = this.descLabel.transform.localPosition;
            vector4.y = ((vector3.y - (num3 / 2)) - (height / 2)) - 3f;
            this.descLabel.transform.localPosition = vector4;
        }

        public void SetInfo(SysItemsVo inTemplate, ItemCell inItemCell)
        {
            this.itemCell = inItemCell;
            this.itemTemplate = inTemplate;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Tips/ItemCellTipsView.assetbundle";
            }
        }
    }
}

