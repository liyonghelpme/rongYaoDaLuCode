﻿namespace com.game.module.bag
{
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class ItemDetailPanel : BaseView<ItemDetailPanel>
    {
        private Button _btnClose;
        private ItemCell _cell;
        private UILabel _count;
        private UILabel _desp;
        private PItems _itemInfo;
        private UILabel _name;
        private UILabel _price;

        protected override void HandleAfterOpenView()
        {
            if (this._itemInfo != null)
            {
                this._cell.itemInfo = this._itemInfo;
            }
            this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickClose);
        }

        protected override void Init()
        {
            this._cell = base.FindChild("item").AddComponent<ItemCell>();
            this._name = base.FindInChild<UILabel>("item/name");
            this._count = base.FindInChild<UILabel>("item/count");
            this._desp = base.FindInChild<UILabel>("desp");
            this._price = base.FindInChild<UILabel>("prices/price");
            this._btnClose = base.FindInChild<Button>("btns/btnClose");
        }

        private void OnClickClose(GameObject go)
        {
            this.CloseView();
        }

        public void ShowTip(PItems itemInfo)
        {
            if (itemInfo != null)
            {
                this._itemInfo = itemInfo;
                this.OpenView();
            }
        }

        public void ShowTip(object args)
        {
            PItems itemInfo = (PItems) args;
            this.ShowTip(itemInfo);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Bag/ItemDetailPanel.assetbundle";
            }
        }
    }
}

