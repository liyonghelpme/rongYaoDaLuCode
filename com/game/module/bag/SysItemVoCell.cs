﻿namespace com.game.module.bag
{
    using com.game.data;
    using com.game.manager;
    using System;

    public class SysItemVoCell : ItemCell
    {
        private int _count;
        private SysItemsVo _sysItemVo;
        private int _sysVoId;
        public bool alwaysShowNum;

        protected override void UpdateLabel()
        {
            if (null != base.label)
            {
                if ((this._sysItemVo == null) || (this._count < 0))
                {
                    base.label.text = string.Empty;
                }
                else if (this.alwaysShowNum || (this._count >= 1))
                {
                    base.label.text = this._count.ToString();
                }
                else
                {
                    base.label.text = string.Empty;
                }
            }
        }

        public int count
        {
            get
            {
                return this._count;
            }
            set
            {
                this._count = value;
                base.UpdateCell();
            }
        }

        protected override string iconAtlasPath
        {
            get
            {
                return "ItemIconAtlas";
            }
        }

        protected override string iconSpriteName
        {
            get
            {
                if (this._sysItemVo == null)
                {
                    return "100000";
                }
                return (string.Empty + this._sysItemVo.icon);
            }
        }

        public override SysItemsVo itemVo
        {
            get
            {
                return this._sysItemVo;
            }
        }

        public int sysVoId
        {
            get
            {
                return this._sysVoId;
            }
            set
            {
                if (value != this._sysVoId)
                {
                    this._sysVoId = value;
                    this._sysItemVo = BaseDataMgr.instance.getGoodsVo((uint) this._sysVoId);
                    base.SetItemsVo(this._sysItemVo);
                    base.UpdateCell();
                }
            }
        }
    }
}

