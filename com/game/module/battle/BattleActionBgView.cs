﻿namespace com.game.module.battle
{
    using com.game.module.core;
    using System;

    public class BattleActionBgView : BaseView<BattleActionBgView>
    {
        private UIPanel panel;

        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            this.panel.alpha = 0.2f;
            base.HandleAfterOpenView();
            vp_Timer.In(0.1f, new vp_Timer.Callback(this.updateBattleAlpha), -1, 0.07f, null);
        }

        protected override void HandleBeforeCloseView()
        {
        }

        protected override void Init()
        {
            this.panel = base.FindChild("BattleActionBgView/").GetComponent<UIPanel>();
        }

        public override void RegisterUpdateHandler()
        {
        }

        private void updateBattleAlpha()
        {
            if (this.panel.alpha < 1f)
            {
                this.panel.alpha += 0.7f;
            }
            else
            {
                this.panel.alpha = 1f;
                vp_Timer.CancelAll("updateBattleAlpha");
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.LowLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Battle/BattleActionBgView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

