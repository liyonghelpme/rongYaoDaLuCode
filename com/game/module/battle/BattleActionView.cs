﻿namespace com.game.module.battle
{
    using com.game.basic;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.u3d.bases.joystick;
    using System;
    using UnityEngine;

    public class BattleActionView : BaseView<BattleActionView>
    {
        private bool _isAuto;
        private UIWidget action_bg;
        private Button btn_skip;

        public override void CancelUpdateHandler()
        {
        }

        private void CloseBattleActionBgView()
        {
            Camera.main.gameObject.AddMissingComponent<MouseOrbit>().ReSetTarget();
            this.action_bg.SetActive(false);
            Singleton<BattleActionView>.Instance.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            GlobalAPI.facade.Notify(0x11, 0, 0, null);
            base.HandleAfterOpenView();
            if (DungeonMgr.Instance.IsFirstPlay)
            {
                this.btn_skip.SetActive(false);
            }
            else
            {
                this.btn_skip.SetActive(true);
            }
        }

        protected override void HandleBeforeCloseView()
        {
            GlobalAPI.facade.Notify(0x12, 0, 0, null);
        }

        protected override void Init()
        {
            base.firstOpen = false;
            this.btn_skip = base.FindInChild<Button>("BattleActionView/Btn_Skip");
            this.action_bg = base.FindInChild<UIWidget>("BattleActionView/ActionBg");
            this.btn_skip.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickSkip);
            this.action_bg.alpha = 0.2f;
            this.action_bg.SetActive(false);
        }

        private void OnClickSkip(GameObject go)
        {
            this.action_bg.SetActive(true);
            vp_Timer.In(0.1f, new vp_Timer.Callback(this.updateBattleAlpha), -1, 0.07f, null);
            JoystickController.instance.Reset();
            Singleton<BattleView>.Instance.gameObject.SetActive(true);
            this.btn_skip.SetActive(false);
            Singleton<BattleMode>.Instance.SetAllSelfAndMonstersAI(Singleton<BattleMode>.Instance.IsAi, true);
            Singleton<MonsterMgr>.Instance.bossBornManager.Exit();
            vp_Timer.In(1f, new vp_Timer.Callback(this.CloseBattleActionBgView), 1, 0.01f, null);
        }

        public override void RegisterUpdateHandler()
        {
        }

        private void updateBattleAlpha()
        {
            if (this.action_bg.alpha < 1f)
            {
                this.action_bg.alpha += 0.7f;
            }
            else
            {
                this.action_bg.alpha = 1f;
                vp_Timer.CancelAll("updateBattleAlpha");
            }
        }

        public bool isAuto
        {
            get
            {
                return this._isAuto;
            }
            set
            {
                this._isAuto = value;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Battle/BattleActionView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

