﻿namespace com.game.module.battle
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.General;
    using com.game.module.Manager;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.controller;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    internal class BattleBottomRightView : BaseView<BattleBottomRightView>
    {
        private Button _attack;
        private readonly bool[] _canUse = new bool[4];
        private readonly List<GameObject> _effectList = new List<GameObject>();
        private int _enableSkillBtnCount = 3;
        private readonly bool[] _isIncd = new bool[4];
        private int _lastAtkOrSkillNumber = -1;
        private Button _skill1;
        private GameObject _skill1Cd;
        private UILabel _skill1CdTimeLabel;
        private UISprite _skill1Icon;
        private Button _skill2;
        private GameObject _skill2Cd;
        private UILabel _skill2CdTimeLabel;
        private UISprite _skill2Icon;
        private Button _skill3;
        private GameObject _skill3Cd;
        private UILabel _skill3CdTimeLabel;
        private UISprite _skill3Icon;
        private Button _skill4;
        private GameObject _skill4Cd;
        private UILabel _skill4CdTimeLabel;
        private UISprite _skill4Icon;
        private UILabel _skill4PauseLabel;
        private UIAtlas _skillAtlas;
        private GameObject[] _skillCdGameObjects;
        private UILabel[] _skillCdTimeLabels;
        private UISprite[] _skillIcons;
        private readonly List<Button> _skillList = new List<Button>();
        [CompilerGenerated]
        private static Func<uint, <>__AnonType0<uint, SysSkillBaseVo>> <>f__am$cache28;
        [CompilerGenerated]
        private static Func<<>__AnonType0<uint, SysSkillBaseVo>, SysSkillBaseVo> <>f__am$cache29;
        [CompilerGenerated]
        private static Func<UISprite, bool> <>f__am$cache2A;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map1F;
        private int CurrentSkillId = -1;
        private TouchState currentState;
        private UISprite descBgSprite;
        private const int HealSkillIndex = 3;
        private const float labelHeight = 24f;
        private List<SysSkillBaseVo> learnedSkillList;
        private vp_Timer.Handle pressHandle = new vp_Timer.Handle();
        private GameObject skillInfo;
        private UILabel skillInfoLabel;
        private List<PGeneralSkillInfo> skillInfos;
        private const float spriteHeight = 60f;
        private bool startUnbeat;
        private int timer;

        public override void CancelUpdateHandler()
        {
        }

        private bool CheckActive(GameObject go)
        {
            return go.transform.FindChild("CD").gameObject.activeSelf;
        }

        private void CheckClearMapAiPath()
        {
            if ((this._lastAtkOrSkillNumber == 0) && AppMap.Instance.me.Controller.IsAStarMove)
            {
                AppMap.Instance.me.Controller.IsAStarMove = false;
                AppMap.Instance.me.Controller.ClearPath();
            }
        }

        private void ClearEffect(object ob)
        {
            this._effectList[(ob as Objes).index].SetActive(false);
        }

        public void ContinueSkill4WhenOutOfBattle()
        {
            this.IsPvpInBattle = false;
        }

        private static void DisableAI(GameObject go)
        {
            if (!MeVo.instance.stateInfo.BeTaunted && !MeVo.instance.stateInfo.BeTemptated)
            {
                AiManagerBase.DisableAIAfterTranscation();
            }
        }

        public void DisableSkill4()
        {
            this._skillIcons[3].gameObject.SetActive(false);
            this._skillList[3].gameObject.SetActive(false);
            this._skillCdTimeLabels[3].gameObject.SetActive(false);
            this.IsPvpInBattle = false;
        }

        public void EnableSkill4()
        {
            this._skillIcons[3].gameObject.SetActive(true);
            this._skillList[3].gameObject.SetActive(true);
            this._skillCdTimeLabels[3].gameObject.SetActive(true);
            this.IsPvpInBattle = false;
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.LoadSkillIcon();
            for (int i = 0; i < this._isIncd.Length; i++)
            {
                this._isIncd[i] = false;
            }
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
            this._skillAtlas = null;
            this.IsPvpInBattle = false;
            this._skillList[3].gameObject.SetActive(false);
            this.SkillIconPressHandler(null, false);
        }

        protected override void Init()
        {
            this._attack = base.FindInChild<Button>("copys/BtnAttack");
            this.skillInfoLabel = base.FindInChild<UILabel>("copys/Skill_info/Label");
            this.descBgSprite = base.FindInChild<UISprite>("copys/Skill_info/bg");
            this.skillInfo = base.FindChild("copys/Skill_info");
            this.skillInfo.SetActive(false);
            this._skillList.Add(this._skill1 = base.FindInChild<Button>("copys/BtnSkill1"));
            this._skillList.Add(this._skill2 = base.FindInChild<Button>("copys/BtnSkill2"));
            this._skillList.Add(this._skill3 = base.FindInChild<Button>("copys/BtnSkill3"));
            this._skillList.Add(this._skill4 = base.FindInChild<Button>("copys/BtnSkill4"));
            this._effectList.Add(base.FindChild("copys/BtnSkill1/GLOW"));
            this._effectList.Add(base.FindChild("copys/BtnSkill2/GLOW"));
            this._effectList.Add(base.FindChild("copys/BtnSkill3/GLOW"));
            this._effectList.Add(base.FindChild("copys/BtnSkill4/GLOW"));
            this._skill1Cd = Tools.find(this.gameObject, "copys/BtnSkill1/CD");
            this._skill2Cd = Tools.find(this.gameObject, "copys/BtnSkill2/CD");
            this._skill3Cd = Tools.find(this.gameObject, "copys/BtnSkill3/CD");
            this._skill4Cd = Tools.find(this.gameObject, "copys/BtnSkill4/CD");
            this._skill1CdTimeLabel = Tools.find(this.gameObject, "copys/BtnSkill1/Number").GetComponent<UILabel>();
            this._skill2CdTimeLabel = Tools.find(this.gameObject, "copys/BtnSkill2/Number").GetComponent<UILabel>();
            this._skill3CdTimeLabel = Tools.find(this.gameObject, "copys/BtnSkill3/Number").GetComponent<UILabel>();
            this._skill4CdTimeLabel = Tools.find(this.gameObject, "copys/BtnSkill4/Number").GetComponent<UILabel>();
            this._skill4PauseLabel = Tools.find(this.gameObject, "copys/BtnSkill4/healSkillMask").GetComponent<UILabel>();
            this._skill1Icon = Tools.find(this.gameObject, "copys/BtnSkill1/Icon").GetComponent<UISprite>();
            this._skill2Icon = Tools.find(this.gameObject, "copys/BtnSkill2/Icon").GetComponent<UISprite>();
            this._skill3Icon = Tools.find(this.gameObject, "copys/BtnSkill3/Icon").GetComponent<UISprite>();
            this._skill4Icon = Tools.find(this.gameObject, "copys/BtnSkill4/Icon").GetComponent<UISprite>();
            this.InitSkillIcon();
            this._skillCdGameObjects = new GameObject[] { this._skill1Cd, this._skill2Cd, this._skill3Cd, this._skill4Cd };
            this._skillCdTimeLabels = new UILabel[] { this._skill1CdTimeLabel, this._skill2CdTimeLabel, this._skill3CdTimeLabel, this._skill4CdTimeLabel };
            this._attack.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._attack.onClick, new UIWidgetContainer.VoidDelegate(this.PressAttack));
            this.skillInfoLabel.onChange = new UIWidget.OnDimensionsChanged(this.SkillInfoLabelChange);
            this._attack.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._attack.onClick, new UIWidgetContainer.VoidDelegate(BattleBottomRightView.DisableAI));
        }

        private void InitSkillIcon()
        {
            this._skillIcons = new UISprite[] { this._skill1Icon, this._skill2Icon, this._skill3Icon, this._skill4Icon };
            for (int i = 0; i < this._skillIcons.Length; i++)
            {
                string str = MeVo.instance.job + "0" + i;
                this._skillIcons[i].spriteName = str;
            }
        }

        private void InitSkillIsInCd()
        {
            Dictionary<int, CDTimeVo> realCdDic = AppMap.Instance.me.Controller.SkillController.CdConroller.GetRealCdDic();
            for (int i = 0; i < this._enableSkillBtnCount; i++)
            {
                if (realCdDic.ContainsKey(i + 4))
                {
                    this._isIncd[i] = realCdDic[i + 4].IsInCd();
                }
            }
        }

        private void LoadAtlas(UIAtlas atlas)
        {
            if (this._skillAtlas == null)
            {
                this._skillAtlas = atlas;
            }
            this.SetSkillIcon();
        }

        private void LoadSkillIcon()
        {
            if (this._skillAtlas == null)
            {
                Singleton<AtlasManager>.Instance.LoadAtlasHold("UI/Icon/SkillIcon/SkillIconHold.assetbundle", "SkillIcon", new LoadCallBack(this.LoadAtlas), true);
            }
            else
            {
                this.SetSkillIcon();
            }
        }

        public void PauseSKill4WhenEnterBattle()
        {
            this.IsPvpInBattle = true;
        }

        private void PressAttack(GameObject go)
        {
            if (!AppMap.Instance.me.GetMeVoByType<BaseRoleVo>().stateInfo.ShouldPauseJoystickAndAtkBtn)
            {
                ActionVo vo = new ActionVo {
                    ActionType = "attack",
                    Attacker = AppMap.Instance.me,
                    ByHand = true
                };
                AppMap.Instance.me.Controller.AttackController.AddAttackList(vo, false, false);
                this._lastAtkOrSkillNumber = 0;
            }
        }

        private void PressTimer()
        {
            this.timer++;
            this.currentState = TouchState.UseSkill;
            if (this.timer >= 0x19)
            {
                vp_Timer.CancelTimerByHandle(this.pressHandle);
                this.currentState = TouchState.PressTips;
                GeneralFightInfo info = Singleton<GeneralMode>.Instance.generalsFightAttrList[AppMap.Instance.me.GetVo().Id];
                this.UpdateSkillDesc(this.learnedSkillList[this.CurrentSkillId - 4], info);
            }
        }

        private void RegisterEveneter()
        {
            this._skill1.onPress = new UIWidgetContainer.BoolDelegate(this.SkillIconPressHandler);
            this._skill2.onPress = new UIWidgetContainer.BoolDelegate(this.SkillIconPressHandler);
            this._skill3.onPress = new UIWidgetContainer.BoolDelegate(this.SkillIconPressHandler);
            this._skill4.onPress = new UIWidgetContainer.BoolDelegate(this.SkillIconPressHandler);
        }

        public override void RegisterUpdateHandler()
        {
        }

        public void SetSkillIcon()
        {
            uint num = AiConfig.ROLE_RECOVERY_SKILL_ID_IN_PVP;
            List<uint> source = AppMap.Instance.me.GetMeVoByType<MeVo>().GetSkillList().ToList<uint>();
            int count = Mathf.Min(source.Count, 4);
            if (count > 0)
            {
                source.RemoveRange(0, count);
            }
            if (AppMap.Instance.IsInWifiPVP)
            {
                this._enableSkillBtnCount = 4;
                Debug.Log("PVP场景 手动设置Skill4的技能Id=" + num);
                source[3] = num;
            }
            else
            {
                this._enableSkillBtnCount = 3;
            }
            if (<>f__am$cache28 == null)
            {
                <>f__am$cache28 = id => new <>__AnonType0<uint, SysSkillBaseVo>(id, BaseDataMgr.instance.GetSysSkillBaseVo(id));
            }
            if (<>f__am$cache29 == null)
            {
                <>f__am$cache29 = <>__TranspIdent0 => <>__TranspIdent0.skillBaseVo;
            }
            this.learnedSkillList = source.Select<uint, <>__AnonType0<uint, SysSkillBaseVo>>(<>f__am$cache28).Select<<>__AnonType0<uint, SysSkillBaseVo>, SysSkillBaseVo>(<>f__am$cache29).ToList<SysSkillBaseVo>();
            for (int i = 0; i < this._enableSkillBtnCount; i++)
            {
                this._skillList[i].gameObject.SetActive(false);
                if ((i < this.learnedSkillList.Count) && (this.learnedSkillList[i] != null))
                {
                    this._skillList[i].gameObject.SetActive(this.learnedSkillList[i].skill_lvl > 0);
                    if (<>f__am$cache2A == null)
                    {
                        <>f__am$cache2A = uiSprite => ((uiSprite.name != "Background") && (uiSprite.name != "guang")) && (uiSprite.name != "CD");
                    }
                    IEnumerator<UISprite> enumerator = this._skillList[i].transform.GetComponentsInChildren<UISprite>(true).Where<UISprite>(<>f__am$cache2A).GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            UISprite current = enumerator.Current;
                            current.atlas = this._skillAtlas;
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                    UISprite component = this._skillList[i].transform.FindChild("Icon").GetComponent<UISprite>();
                    component.spriteName = this.learnedSkillList[i].icon.ToString(CultureInfo.InvariantCulture);
                    this._canUse[i] = true;
                    int num4 = 50;
                    component.height = num4;
                    component.width = num4;
                }
            }
            this.InitSkillIsInCd();
            this.RegisterEveneter();
        }

        private void SkillIconPressHandler(GameObject go, bool state)
        {
            if (go == null)
            {
                this.skillInfo.SetActive(state);
            }
            else
            {
                vp_Timer.CancelTimerByHandle(this.pressHandle);
                go.GetComponent<Button>().TweenScaleState(go, state);
                if (!state)
                {
                    this.skillInfo.SetActive(false);
                    if (this.currentState == TouchState.UseSkill)
                    {
                        DisableAI(go);
                        if (!this.CheckActive(go) && !AppMap.Instance.me.GetMeVoByType<BaseRoleVo>().stateInfo.ShouldPauseJoystickAndAtkBtn)
                        {
                            this.CheckClearMapAiPath();
                            AppMap.Instance.me.Controller.SkillController.UseSkillByHand(this.CurrentSkillId);
                            this._lastAtkOrSkillNumber = this.CurrentSkillId;
                            this.CurrentSkillId = -1;
                        }
                    }
                }
                else
                {
                    this.CurrentSkillId = -1;
                    string name = go.name;
                    if (name != null)
                    {
                        int num;
                        if (<>f__switch$map1F == null)
                        {
                            Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                            dictionary.Add("BtnSkill1", 0);
                            dictionary.Add("BtnSkill2", 1);
                            dictionary.Add("BtnSkill3", 2);
                            dictionary.Add("BtnSkill4", 3);
                            <>f__switch$map1F = dictionary;
                        }
                        if (<>f__switch$map1F.TryGetValue(name, out num))
                        {
                            switch (num)
                            {
                                case 0:
                                    this.CurrentSkillId = 4;
                                    break;

                                case 1:
                                    this.CurrentSkillId = 5;
                                    break;

                                case 2:
                                    this.CurrentSkillId = 6;
                                    break;

                                case 3:
                                    this.CurrentSkillId = 7;
                                    Log.AI(null, " Use Heal Skill ");
                                    break;
                            }
                        }
                    }
                    this.currentState = TouchState.None;
                    this.timer = 0;
                    vp_Timer.In(0f, new vp_Timer.Callback(this.PressTimer), 0, 0.01f, this.pressHandle);
                }
            }
        }

        private void SkillInfoLabelChange()
        {
            float num = (((float) this.skillInfoLabel.height) / 24f) - 1f;
            this.descBgSprite.height = (int) (60f + (num * 24f));
        }

        public override void Update()
        {
            if (this.startUnbeat)
            {
                this.PressAttack(null);
            }
            if (((AppMap.Instance.me != null) && (AppMap.Instance.me.Controller != null)) && (AppMap.Instance.me.Controller.SkillController.CdConroller != null))
            {
                Dictionary<int, CDTimeVo> realCdDic = AppMap.Instance.me.Controller.SkillController.CdConroller.GetRealCdDic();
                for (int i = 0; i < this._enableSkillBtnCount; i++)
                {
                    if (realCdDic.ContainsKey(i + 4))
                    {
                        CDTimeVo vo = realCdDic[i + 4];
                        if (vo == null)
                        {
                            return;
                        }
                        if (vo.currentCd > (vo.totalCd - 0.2))
                        {
                            this._isIncd[i] = true;
                        }
                        if (vo.showMask)
                        {
                            this._skillCdGameObjects[i].GetComponent<UISprite>().fillAmount = vo.GetFillAmount();
                            if (this._isIncd[i] && (vo.currentCd <= 0f))
                            {
                                this._isIncd[i] = false;
                                this._skillIcons[i].ShowAsMyself();
                                this._skillCdGameObjects[i].SetActive(false);
                                this.UpdateEffect(i);
                            }
                            this.UpdateSkillObjects(i, vo.currentCd);
                        }
                        else
                        {
                            this._skillCdGameObjects[i].GetComponent<UISprite>().fillAmount = 0f;
                            this.UpdateSkillObjects(i, 0f);
                        }
                    }
                }
            }
        }

        private void UpdateEffect(int value)
        {
            this._effectList[value].SetActive(true);
            Objes arguments = new Objes {
                index = value
            };
            vp_Timer.In(0.4f, new vp_Timer.ArgCallback(this.ClearEffect), arguments, 1, 0.01f, null);
        }

        private void UpdateSkillDesc(SysSkillBaseVo baseVo, GeneralFightInfo info)
        {
            SysSkillLevelVo sysSkillLevelVo = BaseDataMgr.instance.GetSysSkillLevelVo((uint) baseVo.id);
            if (sysSkillLevelVo != null)
            {
                uint num;
                GameFormula.CalcDamage(baseVo, (baseVo.att_type != 1) ? ((float) info.attr.attM) : ((float) info.attr.attP), out num);
                object[] objArray1 = new object[] { baseVo.name, "\n等级: ", sysSkillLevelVo.skill_lvl, " \n", baseVo.desc };
                string word = string.Concat(objArray1);
                string[] valueListFromString = StringUtils.GetValueListFromString(("[" + num + ",") + sysSkillLevelVo.desc.Replace("[", string.Empty), ',');
                this.skillInfoLabel.text = LanguageManager.GetStringToWord(word, valueListFromString);
                this.skillInfo.SetActive(true);
            }
        }

        private void UpdateSkillObjects(int index, float cdTime)
        {
            if (cdTime > 0f)
            {
                this._skillIcons[index].ShowAsGray();
                this._skillCdGameObjects[index].SetActive(true);
                this._skillCdTimeLabels[index].gameObject.SetActive(true);
                this._skillCdTimeLabels[index].text = Mathf.Ceil(cdTime) + string.Empty;
                if (index == 3)
                {
                    this._skill4PauseLabel.gameObject.SetActive(false);
                }
            }
            else
            {
                this._skillCdGameObjects[index].GetComponent<UISprite>().fillAmount = 0f;
                if (index != 3)
                {
                    this._skillIcons[index].ShowAsMyself();
                    this._skillCdGameObjects[index].gameObject.SetActive(false);
                    this._skillCdTimeLabels[index].gameObject.SetActive(false);
                }
                else if (this.IsPvpInBattle)
                {
                    this._skillIcons[index].ShowAsGray();
                    this._skillCdTimeLabels[index].gameObject.SetActive(false);
                    this._skillCdGameObjects[index].gameObject.SetActive(true);
                }
                else
                {
                    this._skillIcons[index].ShowAsMyself();
                    this._skillCdGameObjects[index].gameObject.SetActive(false);
                    this._skillCdTimeLabels[index].gameObject.SetActive(false);
                    this._skill4PauseLabel.gameObject.SetActive(false);
                }
            }
        }

        public Button AttackButton
        {
            get
            {
                return this._attack;
            }
        }

        public bool IsPvpInBattle { get; set; }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public List<Button> SkillButtonList
        {
            get
            {
                return this._skillList;
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }

        private class Objes : UnityEngine.Object
        {
            public int index;
        }

        private enum TouchState
        {
            None,
            UseSkill,
            PressTips
        }
    }
}

