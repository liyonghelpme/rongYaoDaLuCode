﻿namespace com.game.module.battle
{
    using com.game;
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.vo;
    using System;
    using UnityEngine;

    public class BattleCenterView : BaseView<BattleCenterView>
    {
        private UIWidget battlePanel;
        private ColorCorrectionCurves curves;
        private UIWidget effectGroup;
        private GameObject intoBattle;
        private bool isAlpha;
        private GameObject outBattle;
        private UISprite sprite;

        public void CancelHpWarning()
        {
            this.show(false);
        }

        public override void CancelUpdateHandler()
        {
            GlobalAPI.facade.Remove(0x10, new NoticeListener(this.OnChangeScene));
        }

        protected override void Init()
        {
            this.effectGroup = base.FindChild("effect").GetComponent<UIWidget>();
            this.battlePanel = base.FindChild("battle").GetComponent<UIWidget>();
            this.intoBattle = base.FindChild("battle/into_battle");
            this.outBattle = base.FindChild("battle/out_battle");
            this.sprite = base.FindInChild<UISprite>("effect/attckEffect");
            this.sprite.SetActive(false);
            this.intoBattle.SetActive(false);
            this.outBattle.SetActive(false);
        }

        private void OnChangeScene(int type, int v1, int v2, object data)
        {
            if (!GlobalData.isInCopy)
            {
                this.CancelHpWarning();
                this.updateBattleState(false);
            }
        }

        public override void RegisterUpdateHandler()
        {
            GlobalAPI.facade.Add(0x10, new NoticeListener(this.OnChangeScene));
        }

        private void show(bool isShow)
        {
            bool state = isShow && GlobalData.isInCopy;
            if ((this.sprite != null) && (!state || !this.sprite.gameObject.activeSelf))
            {
                this.sprite.SetActive(state);
                if (!state)
                {
                    vp_Timer.CancelAll("updateAlpha");
                }
                else
                {
                    this.effectGroup.alpha = 1f;
                    vp_Timer.In(0f, new vp_Timer.Callback(this.updateAlpha), -1, 0.07f, null);
                }
            }
        }

        private void updateAlpha()
        {
            if (!this.isAlpha)
            {
                if (this.effectGroup.alpha <= 0.5f)
                {
                    this.isAlpha = true;
                }
                else
                {
                    this.effectGroup.alpha -= 0.05f;
                }
            }
            else if (this.effectGroup.alpha >= 1f)
            {
                this.isAlpha = false;
            }
            else
            {
                this.effectGroup.alpha += 0.05f;
            }
        }

        private void updateBattleAlpha()
        {
            if (this.battlePanel.alpha < 0.1f)
            {
                this.battlePanel.alpha = 0f;
                vp_Timer.CancelAll("updateBattleAlpha");
                this.intoBattle.SetActive(false);
                this.outBattle.SetActive(false);
            }
            else
            {
                this.battlePanel.alpha -= 0.05f;
            }
        }

        public void updateBattleState(bool isBattle)
        {
            this.intoBattle.SetActive(false);
            this.outBattle.SetActive(false);
            this.battlePanel.alpha = 1f;
            vp_Timer.CancelAll("updateBattleAlpha");
            if (GlobalData.isInCopy)
            {
                if (isBattle)
                {
                    this.intoBattle.SetActive(true);
                }
                else
                {
                    this.outBattle.SetActive(true);
                }
                vp_Timer.In(0.5f, new vp_Timer.Callback(this.updateBattleAlpha), -1, 0.07f, null);
            }
        }

        public void updateDeathEffect()
        {
            bool flag = false;
            if ((AppMap.Instance.me.Controller.StatuController.CurrentStatu == 11) || (AppMap.Instance.me.Controller.StatuController.CurrentStatu == 0x13))
            {
                Singleton<BattleRightView>.Instance.updateEffect(true);
                this.show(false);
                flag = true;
            }
            if (flag)
            {
                if (this.curves == null)
                {
                    this.curves = Camera.main.gameObject.GetComponent<ColorCorrectionCurves>();
                }
                this.curves.enabled = true;
                this.curves.saturation = 0f;
            }
            else if (this.curves != null)
            {
                this.curves.saturation = 1f;
                this.curves.enabled = false;
            }
        }

        public void updateHp(PlayerVo meVo)
        {
            SysDungeonMissionVo curMissionSysVo = DungeonMgr.Instance.curMissionSysVo;
            if (curMissionSysVo != null)
            {
                float num = (curMissionSysVo.low_hp_warning * 0.0001f) * 100f;
                this.show(((((float) meVo.CurHp) / ((float) meVo.Hp)) * 100f) < num);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }
    }
}

