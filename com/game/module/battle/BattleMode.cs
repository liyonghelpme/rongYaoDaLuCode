﻿namespace com.game.module.battle
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.ai;
    using com.u3d.bases.display.character;
    using com.u3d.bases.joystick;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BattleMode : BaseMode<BattleMode>
    {
        private bool _isAutoSystem;
        private string _stageInfo;
        [CompilerGenerated]
        private static Func<MonsterDisplay, bool> <>f__am$cacheD;
        public float CurrentHpRate;
        public string HpString;
        public bool IsAi;
        public bool IsLeft;
        public bool IsRight;
        public int LeftCount;
        public int MonsterIcon;
        public int MonsterLvl;
        public string MonsterName;
        public int MonsterType;
        public float PreviousHpRate;
        public const int UpdataAutoSystemStatu = 1;
        public const int UpdateBattleView = 6;
        public const int UpdateMonsterDeath = 4;
        public const int UpdateStageInfo = 3;
        public const int UPdateSwitchPlayer = 5;
        public const int UpdateTopRightHpBar = 2;

        public void CloseBattleView()
        {
            this.IsAi = AppMap.Instance.me.Controller.AiController.IsAiEnable;
            JoystickController.instance.Reset();
            if (Singleton<BattleView>.Instance.gameObject != null)
            {
                Singleton<BattleView>.Instance.gameObject.SetActive(false);
            }
            Singleton<BattleActionView>.Instance.OpenView();
            Singleton<BattleMode>.Instance.SetAllSelfAndMonstersAI(false, false);
        }

        public void DoMonsterDeath()
        {
            base.DataUpdate(4);
        }

        public void OpenBattleView()
        {
            JoystickController.instance.Reset();
            Singleton<BattleView>.Instance.gameObject.SetActive(true);
            Singleton<BattleActionView>.Instance.CloseView();
            Singleton<BattleMode>.Instance.SetAllSelfAndMonstersAI(Singleton<BattleMode>.Instance.IsAi, true);
        }

        public void SetAllSelfAndMonstersAI(bool setSelfAi, bool setMonAi)
        {
            foreach (PlayerDisplay display in AppMap.Instance.SelfplayerList)
            {
                if (display.Controller != null)
                {
                    AiControllerBase aiController = display.Controller.AiController;
                    if (aiController != null)
                    {
                        aiController.SetAi((display.GetVo().Id != AppMap.Instance.me.GetVo().Id) ? setMonAi : setSelfAi);
                    }
                }
            }
            if (<>f__am$cacheD == null)
            {
                <>f__am$cacheD = monsterDisplay => (bool) monsterDisplay.Controller.AiController;
            }
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.Where<MonsterDisplay>(<>f__am$cacheD).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    current.Controller.AiController.SetAi(setMonAi);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void SetCurrentHp(string monsterName, uint maxHp, uint previousHp, uint currentHp, int lvl, int monsterType, int monsterIcon, int maxCount = 1)
        {
            int num = Mathf.CeilToInt(((float) maxHp) / ((float) maxCount));
            int num2 = (int) (currentHp / num);
            int num3 = (int) (((ulong) currentHp) % ((long) num));
            int num4 = (int) (((ulong) previousHp) % ((long) num));
            if (num3 != 0)
            {
                num2++;
            }
            string str = currentHp + "/" + maxHp;
            float num5 = ((float) num3) / ((float) num);
            float num6 = ((float) num4) / ((float) num);
            if (currentHp == maxHp)
            {
                num5 = 1f;
            }
            if (previousHp == maxHp)
            {
                num6 = 1f;
            }
            this.MonsterName = monsterName;
            this.HpString = str;
            this.PreviousHpRate = num6;
            this.CurrentHpRate = num5;
            this.LeftCount = num2;
            this.MonsterLvl = lvl;
            this.MonsterType = monsterType;
            this.MonsterIcon = monsterIcon;
            base.DataUpdate(2);
        }

        public void UpdateSwitchPlayer()
        {
            base.DataUpdate(5);
        }

        public bool IsAutoSystem
        {
            get
            {
                return this._isAutoSystem;
            }
            set
            {
                this._isAutoSystem = value;
                base.DataUpdate(1);
            }
        }

        public string StageInfo
        {
            get
            {
                return this._stageInfo;
            }
            set
            {
                this._stageInfo = value;
                base.DataUpdate(3);
            }
        }
    }
}

