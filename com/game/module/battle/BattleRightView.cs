﻿namespace com.game.module.battle
{
    using com.game;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.vo;
    using com.u3d.bases.display.character;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    internal class BattleRightView : BaseView<BattleRightView>
    {
        private ulong curClickId;
        private byte curClickPlace = 1;
        private ulong currentPlayerId;
        private List<GameObject> effectList = new List<GameObject>();
        private Dictionary<UISprite, bool> playerBloodGreyDic = new Dictionary<UISprite, bool>();
        private Dictionary<uint, UISprite> playerBloodList = new Dictionary<uint, UISprite>();
        private Button playerHead;
        private Dictionary<uint, Button> playerHeadButtonList = new Dictionary<uint, Button>();
        private Dictionary<uint, UISprite> playerIconList = new Dictionary<uint, UISprite>();

        public override void CancelUpdateHandler()
        {
            Singleton<GeneralMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateHeadIconView);
            Singleton<GeneralMode>.Instance.dataUpdatedWithParam -= new com.game.module.core.DataUpateHandlerWithParam(this.UpdateHeadIconByHp);
        }

        private void clearEffect()
        {
            foreach (GameObject obj2 in this.effectList)
            {
                if (obj2 != null)
                {
                    obj2.SetActive(false);
                }
            }
        }

        protected override void HandleAfterOpenView()
        {
            this.curClickPlace = Singleton<GeneralMode>.Instance.curSelectedPlace;
            this.setHeadIcon();
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
            this.playerBloodGreyDic.Clear();
            this.playerHeadButtonList.Clear();
            this.playerIconList.Clear();
            this.playerBloodList.Clear();
            this.effectList.Clear();
        }

        protected override void Init()
        {
            this.InitIcon();
        }

        private void InitIcon()
        {
            for (uint i = 1; i <= 3; i++)
            {
                GeneralHeadItemInfo component;
                this.playerHead = base.FindInChild<Button>("head/player" + i.ToString());
                this.playerHeadButtonList.Add(i, this.playerHead);
                this.playerHead.onClick = new UIWidgetContainer.VoidDelegate(this.PlayerHeadClickHandler);
                GeneralHeadItemInfo local1 = this.playerHead.gameObject.AddMissingComponent<GeneralHeadItemInfo>();
                if (local1 != null)
                {
                    component = local1;
                }
                else
                {
                    component = this.playerHead.gameObject.GetComponent<GeneralHeadItemInfo>();
                }
                UISprite sprite = base.FindInChild<UISprite>("head/player" + i.ToString() + "/Icon");
                this.playerIconList.Add(i, sprite);
                sprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                sprite.MakePixelPerfect();
                UISprite sprite2 = base.FindInChild<UISprite>("head/player" + i.ToString() + "/blood");
                this.playerBloodList.Add(i, sprite2);
                this.effectList.Add(base.FindChild("head/player" + i.ToString() + "/touxiang"));
                if (sprite.isShowAsGray)
                {
                    sprite.ShowAsMyself();
                }
                Log.AI(this.playerHead, string.Concat(new object[] { "  blood icon ", sprite, " blood ", sprite2 }));
            }
            this.updateEffect(false);
        }

        private bool isDeath(int id)
        {
            IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.playerList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    if ((current.GetMeVoByType<PlayerVo>().generalTemplateInfo.general_id == id) && ((current.Controller.StatuController.CurrentStatu == 11) || (current.Controller.StatuController.CurrentStatu == 0x13)))
                    {
                        return true;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return false;
        }

        private void PlayerHeadClickHandler(GameObject go)
        {
            GeneralHeadItemInfo component = go.GetComponent<GeneralHeadItemInfo>();
            Log.AI(go, " On PlayerHead " + component.general_id);
            if (!this.isDeath(component.general_id))
            {
                this.updateEffect(false);
                Log.AI(go, string.Concat(new object[] { " currentPlayerId  ", this.currentPlayerId, " itemid ", component.id }));
                if (component.id != this.currentPlayerId)
                {
                    PSetGeneralInfo info2 = new PSetGeneralInfo {
                        id = component.id
                    };
                    this.curClickPlace = component.place;
                    this.curClickId = component.id;
                    Singleton<GeneralMode>.Instance.SendFightGenral(info2.id);
                }
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<GeneralMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateHeadIconView);
            Singleton<GeneralMode>.Instance.dataUpdatedWithParam += new com.game.module.core.DataUpateHandlerWithParam(this.UpdateHeadIconByHp);
        }

        private void setHeadIcon()
        {
            if (this.playerHeadButtonList.Count == 0)
            {
                this.InitIcon();
            }
            if (this.curClickPlace != Singleton<GeneralMode>.Instance.currentFightMinPlace)
            {
                Singleton<GeneralMode>.Instance.ChangeFightGeneralsPlace(this.curClickId, this.curClickPlace);
            }
            foreach (Button button in this.playerHeadButtonList.Values)
            {
                button.SetActive(false);
            }
            Dictionary<ulong, GeneralFightInfo> generalsFightAttrList = Singleton<GeneralMode>.Instance.generalsFightAttrList;
            uint num = 1;
            foreach (GeneralFightInfo info in generalsFightAttrList.Values)
            {
                this.playerHead = this.playerHeadButtonList[num];
                this.playerHead.SetActive(true);
                info.battlePlace = num;
                GeneralHeadItemInfo component = this.playerHead.gameObject.GetComponent<GeneralHeadItemInfo>();
                component.general_id = info.genralTemplateInfo.general_id;
                component.id = info.attr.id;
                component.place = info.place;
                component.icon_id = info.genralTemplateInfo.icon_id;
                if (component.place == this.curClickPlace)
                {
                    this.currentPlayerId = component.id;
                }
                UISprite sprite = this.playerIconList[num];
                sprite.spriteName = component.icon_id.ToString();
                int num2 = 0x38;
                sprite.height = num2;
                sprite.width = num2;
                sprite.MakePixelPerfect();
                UISprite sprite2 = this.playerBloodList[num];
                sprite2.fillAmount = ((float) info.attr.hp) / ((float) info.attr.hpFull);
                Button button2 = this.playerHeadButtonList[info.battlePlace];
                if (info.place == this.curClickPlace)
                {
                    button2.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else
                {
                    button2.transform.localScale = new Vector3(0.82f, 0.82f, 0.82f);
                }
                num++;
            }
        }

        public void updateEffect(bool value)
        {
            this.clearEffect();
            if (value)
            {
                Dictionary<ulong, GeneralFightInfo> generalsFightAttrList = Singleton<GeneralMode>.Instance.generalsFightAttrList;
                int num = 0;
                foreach (GeneralFightInfo info in generalsFightAttrList.Values)
                {
                    if (!this.isDeath(info.genralTemplateInfo.general_id))
                    {
                        this.effectList[num].SetActive(value);
                    }
                    num++;
                }
            }
        }

        public void UpdateHeadBloodBar(BaseRoleVo vo)
        {
            <UpdateHeadBloodBar>c__AnonStoreyF2 yf = new <UpdateHeadBloodBar>c__AnonStoreyF2 {
                vo = vo
            };
            GeneralFightInfo info = Singleton<GeneralMode>.Instance.generalsFightAttrList.Values.FirstOrDefault<GeneralFightInfo>(new Func<GeneralFightInfo, bool>(yf.<>m__D4));
            if ((info != null) && this.playerBloodList.ContainsKey(info.battlePlace))
            {
                UISprite key = this.playerIconList[info.battlePlace];
                UISprite sprite2 = this.playerBloodList[info.battlePlace];
                float curHpPercent = yf.vo.CurHpPercent;
                uint curHp = yf.vo.CurHp;
                if (curHp > 0)
                {
                    curHpPercent = Mathf.Max(curHpPercent, 0.015f);
                }
                sprite2.fillAmount = curHpPercent;
                if (curHp <= 0)
                {
                    key.ShowAsGray();
                    if (!this.playerBloodGreyDic.ContainsKey(key))
                    {
                        this.playerBloodGreyDic.Add(key, true);
                    }
                }
                else if (this.playerBloodGreyDic.ContainsKey(key) && (curHp > 0))
                {
                    this.playerIconList[info.battlePlace].ShowAsMyself();
                    this.playerBloodGreyDic.Remove(key);
                }
            }
        }

        [Obsolete("Please use the other overloading instead")]
        public void UpdateHeadBloodBar(uint curHp, ulong id)
        {
            foreach (GeneralFightInfo info in Singleton<GeneralMode>.Instance.generalsFightAttrList.Values)
            {
                if ((info.attr.id == id) && this.playerBloodList.ContainsKey(info.battlePlace))
                {
                    UISprite key = this.playerIconList[info.battlePlace];
                    UISprite sprite = this.playerBloodList[info.battlePlace];
                    float a = ((float) curHp) / ((float) info.attr.hpFull);
                    sprite.fillAmount = Mathf.Min(a, 0.05f);
                    if (curHp <= 0)
                    {
                        key.ShowAsGray();
                        if (!this.playerBloodGreyDic.ContainsKey(key))
                        {
                            this.playerBloodGreyDic.Add(key, true);
                        }
                    }
                    else if (this.playerBloodGreyDic.ContainsKey(key) && (curHp > 0))
                    {
                        this.playerIconList[info.battlePlace].ShowAsMyself();
                        this.playerBloodGreyDic.Remove(key);
                    }
                }
            }
        }

        private void updateHeadIcon()
        {
            vp_Timer.CancelAll("recoverBlood");
            Singleton<GeneralMode>.Instance.ChangeFightGeneralsPlace(this.curClickId, this.curClickPlace);
            Dictionary<ulong, GeneralFightInfo> generalsFightAttrList = Singleton<GeneralMode>.Instance.generalsFightAttrList;
            uint num = 1;
            foreach (GeneralFightInfo info in generalsFightAttrList.Values)
            {
                this.playerHead = this.playerHeadButtonList[num];
                this.playerHead.SetActive(true);
                info.battlePlace = num;
                GeneralHeadItemInfo component = this.playerHead.gameObject.GetComponent<GeneralHeadItemInfo>();
                component.general_id = info.genralTemplateInfo.general_id;
                component.id = info.attr.id;
                component.place = info.place;
                component.icon_id = info.genralTemplateInfo.icon_id;
                if (component.place == this.curClickPlace)
                {
                    this.currentPlayerId = component.id;
                    uint id = (uint) info.genralTemplateInfo.model_id;
                    if (MeVo.instance.Controller != null)
                    {
                        MeVo.instance.Controller.SwitchGeneralController.SwitchGeneral(id);
                    }
                }
                num++;
                Button button = this.playerHeadButtonList[info.battlePlace];
                if (info.place == this.curClickPlace)
                {
                    button.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else
                {
                    button.transform.localScale = new Vector3(0.82f, 0.82f, 0.82f);
                }
            }
        }

        private void UpdateHeadIconByHp(object sender, int code, object parm)
        {
            if (code == 8)
            {
                this.curClickPlace = (byte) parm;
                this.updateHeadIcon();
            }
        }

        private void UpdateHeadIconView(object sender, int code)
        {
            if (code == 3)
            {
                this.updateHeadIcon();
            }
        }

        public Dictionary<uint, Button> HeroList
        {
            get
            {
                return this.playerHeadButtonList;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateHeadBloodBar>c__AnonStoreyF2
        {
            internal BaseRoleVo vo;

            internal bool <>m__D4(GeneralFightInfo x)
            {
                return (x.attr.id == this.vo.Id);
            }
        }
    }
}

