﻿namespace com.game.module.battle
{
    using com.game;
    using com.game.manager;
    using com.game.module.Arena;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.u3d.bases.ai;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    internal class BattleTopLeftView : BaseView<BattleTopLeftView>
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map20;
        private Button about_btn;
        private Button auto_attck;
        private GameObject autoEffect;
        private UISprite autoSprite;
        private Button camera_btn;
        private UILabel cameraLabel;
        private Button hunZhiQiu;
        private Button seting_btn;

        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            this.cameraLabel.text = Singleton<MapMode>.Instance.GetCameraTypeCount().ToString();
            this.InitButtonVisability();
        }

        public void HideAutoAttackBtn(bool state)
        {
            this.auto_attck.SetActive(state);
            this.seting_btn.SetActive(state);
            this.about_btn.SetActive(state);
            this.camera_btn.SetActive(state);
        }

        protected override void Init()
        {
            this.seting_btn = base.FindInChild<Button>("downList/seting");
            this.auto_attck = base.FindInChild<Button>("downList/auto_attck");
            this.about_btn = base.FindInChild<Button>("downList/about");
            this.autoSprite = base.FindInChild<UISprite>("downList/auto_attck/Background");
            this.camera_btn = base.FindInChild<Button>("downList/camera");
            this.autoEffect = base.FindChild("downList/auto_attck/AUTO");
            this.cameraLabel = base.FindInChild<UILabel>("downList/camera/label");
            this.hunZhiQiu = base.FindInChild<Button>("downList/hunZhiQiu");
            this.autoEffect.SetActive(false);
            this.seting_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.auto_attck.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.camera_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.about_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void InitButtonVisability()
        {
            if (AppMap.Instance.IsInArena)
            {
                Button[] list = new Button[] { this.about_btn, this.seting_btn, this.auto_attck };
                SetActiveFalse(list);
                Button[] buttonArray2 = new Button[] { this.camera_btn };
                SetActiveTrue(buttonArray2);
            }
            else if (AppMap.Instance.IsInWifiPVP)
            {
                Button[] buttonArray3 = new Button[] { this.about_btn };
                SetActiveFalse(buttonArray3);
                Button[] buttonArray4 = new Button[] { this.camera_btn, this.seting_btn, this.auto_attck };
                SetActiveTrue(buttonArray4);
            }
            else
            {
                Button[] buttonArray5 = new Button[] { this.auto_attck, this.about_btn, this.seting_btn, this.camera_btn };
                SetActiveTrue(buttonArray5);
            }
        }

        private void KillMonster()
        {
            IList<MonsterDisplay> monsterList = AppMap.Instance.monsterList;
            if (monsterList != null)
            {
                IEnumerator<MonsterDisplay> enumerator = monsterList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        MonsterDisplay current = enumerator.Current;
                        Singleton<MapMode>.Instance.MonsterDeath((ulong) ((uint) current.Monster.Id));
                        Singleton<MapMode>.Instance.SyncAttackEnergy();
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        private void onClickCamera()
        {
            Singleton<MapMode>.Instance.AddCameraType();
            this.cameraLabel.text = Singleton<MapMode>.Instance.GetCameraTypeCount().ToString();
            Singleton<MapMode>.Instance.ShowMapCameraEffect();
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map20 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                    dictionary.Add("seting", 0);
                    dictionary.Add("auto_attck", 1);
                    dictionary.Add("about", 2);
                    dictionary.Add("camera", 3);
                    <>f__switch$map20 = dictionary;
                }
                if (<>f__switch$map20.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            Singleton<SystemSettingsView>.Instance.OpenView();
                            break;

                        case 1:
                            if (!AppMap.Instance.IsInArena || !ArenaManager.Instance.IsNeedHideBattleView)
                            {
                                PlayerAiController.ClickAutoAttack();
                                break;
                            }
                            MessageManager.Show(LanguageManager.GetWord("Arena.AllAIMode"));
                            break;

                        case 2:
                            if (!Singleton<DungeonTaskView>.Instance.IsOpened)
                            {
                                Singleton<DungeonTaskView>.Instance.OpenView();
                                break;
                            }
                            Singleton<DungeonTaskView>.Instance.CloseView();
                            break;

                        case 3:
                            this.onClickCamera();
                            break;
                    }
                }
            }
        }

        public override void RegisterUpdateHandler()
        {
        }

        private static void SetActiveFalse(params Button[] list)
        {
            foreach (Button button in list)
            {
                button.SetActive(false);
            }
        }

        private static void SetActiveTrue(params Button[] list)
        {
            foreach (Button button in list)
            {
                button.SetActive(true);
            }
        }

        public void updateAutoEffect(bool isAuto)
        {
            Log.AI(null, "UpdateAutoEffect ");
            if (this.autoEffect != null)
            {
                this.autoEffect.SetActive(isAuto);
            }
            if (this.autoSprite != null)
            {
                string str = this.autoSprite.spriteName.Replace("-down", string.Empty);
                string str2 = !isAuto ? str : ((this.autoSprite.atlas.GetSprite(str + "-down") == null) ? str : (str + "-down"));
                this.autoSprite.spriteName = str2;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }
    }
}

