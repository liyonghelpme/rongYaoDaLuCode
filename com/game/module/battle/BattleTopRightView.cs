﻿namespace com.game.module.battle
{
    using com.game;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.hud;
    using com.game.utils;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class BattleTopRightView : BaseView<BattleTopRightView>
    {
        private Button _btnCollapse;
        private readonly IDictionary<ActionDisplay, Transform> _enemySide = new Dictionary<ActionDisplay, Transform>();
        private bool _isExpanded;
        private UILabel _labelMasterKill;
        private UILabel _labelSlaveKill;
        private readonly IList<Vector3> _mapCornerPos = new List<Vector3>();
        private readonly IDictionary<ActionDisplay, Transform> _mySide = new Dictionary<ActionDisplay, Transform>();
        private GameObject _panelNow;
        private readonly IDictionary<ActionDisplay, UISprite> _posDic = new Dictionary<ActionDisplay, UISprite>();
        private GameObject _posIconObj;
        private UIWidget _uiBackgroundForShow;
        private readonly IList<Vector3> _uiCornerPos = new List<Vector3>();
        private float _uiMapLengthRatio;
        private float _uiMapWidthRatio;
        private GameObject _wholeView;
        [CompilerGenerated]
        private static Func<string, Vector3> <>f__am$cache10;
        private const int BOTTOM_LEFT = 2;
        private const int BOTTOM_RIGHT = 3;
        private readonly vp_Timer.Handle timeHandle = new vp_Timer.Handle();
        private const int TOP_LEFT = 0;
        private const int TOP_RIGHT = 1;

        public void AddPlayer(PlayerDisplay dis, bool isMySide)
        {
            if (base.IsOpened)
            {
                this.HandleAdd(dis, isMySide);
            }
        }

        public void AddTowerOrBase(MonsterDisplay dis, int monType, bool isMySide)
        {
            if (base.IsOpened)
            {
                this.HandleAdd(dis, isMySide);
            }
        }

        private void ClearData()
        {
            Log.AI(null, " Clear WifiPvp UI Data ");
            IEnumerator<UISprite> enumerator = this._posDic.Values.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    UISprite current = enumerator.Current;
                    Log.AI(null, " Clear spriteName  " + current.gameObject);
                    UnityEngine.Object.Destroy(current.gameObject);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this._labelMasterKill.text = "0";
            this._labelSlaveKill.text = "0";
            this._posDic.Clear();
            this._mySide.Clear();
            this._enemySide.Clear();
            this._mapCornerPos.Clear();
            this._uiCornerPos.Clear();
            this._uiMapWidthRatio = this._uiMapLengthRatio = 0f;
        }

        private string GetSpriteName(ActionDisplay dis, bool isMySide)
        {
            return (!isMySide ? (!(dis is PlayerDisplay) ? "ta1" : "hongD") : (!(dis is PlayerDisplay) ? "ta0" : "lvD"));
        }

        private void HandleAdd(ActionDisplay dis, bool isMySide)
        {
            if (dis == null)
            {
                Debug.LogError("传入NULL");
            }
            else
            {
                IDictionary<ActionDisplay, Transform> dictionary = !isMySide ? this._enemySide : this._mySide;
                if (dictionary.ContainsKey(dis))
                {
                    dictionary[dis] = dis.GoBase.transform;
                }
                else
                {
                    dictionary.Add(dis, dis.GoBase.transform);
                }
                GameObject obj2 = NGUITools.AddChild(this._panelNow, this._posIconObj);
                obj2.SetActive(true);
                Log.AI(null, string.Concat(new object[] { " UpdateMap Handle Add ", dis, " isMySide ", isMySide }));
                UISprite component = obj2.GetComponent<UISprite>();
                component.spriteName = this.GetSpriteName(dis, isMySide);
                Vector3 position = dis.GoBase.transform.position;
                if (this.RealPosToMapPos(ref position))
                {
                    obj2.transform.localPosition = position;
                    if (dis is MonsterDisplay)
                    {
                        int type = dis.GetMeVoByType<MonsterVo>().MonsterVO.type;
                        this.SetZoom(ref component, type);
                    }
                }
                else
                {
                    Debug.LogError("HandleAdd报错");
                    return;
                }
                if (this._posDic.ContainsKey(dis))
                {
                    Log.AI(null, " Clear OldDisplayData " + dis);
                    UnityEngine.Object.Destroy(this._posDic[dis].gameObject);
                    this._posDic.Remove(dis);
                }
                Log.AI(null, string.Concat(new object[] { " Add Pos ", dis, " sp  ", component.spriteName }));
                this._posDic.Add(dis, component);
            }
        }

        protected override void HandleAfterOpenView()
        {
            this._isExpanded = true;
            if (AppMap.Instance.IsInWifiPVP)
            {
                this.UpdateCollapseView();
                this.LoadData();
                this.UpdateMapRatio();
                vp_Timer.In(0.25f, new vp_Timer.Callback(this.UpdatePos), 0, 0.1f, this.timeHandle);
            }
            else
            {
                this.CloseView();
            }
        }

        protected override void HandleBeforeCloseView()
        {
            vp_Timer.CancelTimerByHandle(this.timeHandle);
            this.ClearData();
        }

        private void HandleDelete(ActionDisplay ad, bool isMySide)
        {
            if (ad == null)
            {
                Debug.LogError("移除时, 发现ad不存在");
            }
            else
            {
                (!isMySide ? this._enemySide : this._mySide).Remove(ad);
                if (ad is MonsterDisplay)
                {
                    this._posDic[ad].ShowAsGray();
                }
                else
                {
                    UnityEngine.Object.Destroy(this._posDic[ad]);
                    this._posDic.Remove(ad);
                }
            }
        }

        protected override void Init()
        {
            this._wholeView = base.FindChild("map");
            this._labelMasterKill = base.FindInChild<UILabel>("map/left/label");
            this._labelSlaveKill = base.FindInChild<UILabel>("map/rgiht/label");
            this._btnCollapse = base.FindInChild<Button>("map/expand");
            this._panelNow = base.FindChild("map/now_pos");
            this._uiBackgroundForShow = base.FindInChild<UIWidget>("map/now_pos/background");
            this._posIconObj = base.FindChild("map/now_pos/posIcon");
            this._btnCollapse.onClick = delegate (GameObject go) {
                this._isExpanded = !this._isExpanded;
                this.UpdateCollapseView();
            };
        }

        private bool IsNeedZoom(int monType)
        {
            return (monType == 5);
        }

        private void LoadData()
        {
            IList<Vector3> originalPos = ParseWorldPos(Singleton<MapMode>.Instance.CurrentSceneVo);
            this._mapCornerPos.Clear();
            IEnumerator<Vector3> enumerator = originalPos.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Vector3 current = enumerator.Current;
                    this._mapCornerPos.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            IEnumerable<Vector3> enumerable = WorldPosToUIPos(originalPos);
            this._uiCornerPos.Clear();
            IEnumerator<Vector3> enumerator2 = enumerable.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    Vector3 item = enumerator2.Current;
                    this._uiCornerPos.Add(item);
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
        }

        private static IList<Vector3> ParseWorldPos(SysSceneVo scence)
        {
            string[] source = new string[] { scence.map_top_left_pos, scence.map_top_right_pos, scence.map_bottom_left_pos, scence.map_bottom_right_pos };
            if (<>f__am$cache10 == null)
            {
                <>f__am$cache10 = str => StringUtils.DivideWith1000(StringUtils.StringToVector3(str));
            }
            return source.Select<string, Vector3>(<>f__am$cache10).ToList<Vector3>();
        }

        private bool RealPosToMapPos(ref Vector3 pos)
        {
            if ((this._uiMapLengthRatio == 0f) || (this._uiMapWidthRatio == 0f))
            {
                return false;
            }
            Vector3 vector2 = this._mapCornerPos[0];
            float num = pos.x - vector2.x;
            Vector3 vector3 = this._mapCornerPos[0];
            float num2 = pos.y - vector3.y;
            Vector3 vector4 = this._mapCornerPos[1];
            Vector3 vector5 = this._mapCornerPos[0];
            float num3 = vector4.x - vector5.x;
            Vector3 vector6 = this._mapCornerPos[2];
            Vector3 vector7 = this._mapCornerPos[0];
            float num4 = vector6.z - vector7.z;
            int width = this._uiBackgroundForShow.width;
            int height = this._uiBackgroundForShow.height;
            Vector3 localPosition = this._uiBackgroundForShow.gameObject.transform.localPosition;
            pos.x = (((num / num3) * width) + localPosition.x) - (((float) width) / 2f);
            pos.y = (((num2 / num4) * height) - localPosition.y) + (((float) height) / 2f);
            pos.z = 0f;
            return true;
        }

        public void RemoveTowerOrBase(MonsterDisplay dis, bool isMySide)
        {
            if (base.IsOpened && (dis != null))
            {
                this.HandleDelete(dis, isMySide);
            }
        }

        public void RemoveTowerOrBase(ulong id, bool isMySide)
        {
            <RemoveTowerOrBase>c__AnonStoreyF3 yf = new <RemoveTowerOrBase>c__AnonStoreyF3 {
                id = id
            };
            if (base.IsOpened)
            {
                ActionDisplay display = !isMySide ? this._enemySide.Keys.FirstOrDefault<ActionDisplay>(new Func<ActionDisplay, bool>(yf.<>m__D8)) : this._mySide.Keys.FirstOrDefault<ActionDisplay>(new Func<ActionDisplay, bool>(yf.<>m__D7));
                this.RemoveTowerOrBase(display as MonsterDisplay, isMySide);
            }
        }

        private void SetZoom(ref UISprite sp, int monType)
        {
            if (this.IsNeedZoom(monType))
            {
                sp.SetDimensions(0x1a, 0x27);
            }
            else
            {
                sp.SetDimensions(0x15, 0x1f);
            }
        }

        private void UpdateCollapseView()
        {
            float num = !this._isExpanded ? 127f : -127f;
            object[] args = new object[] { "x", num, "delay", 0.001f, "time", 0.5f, "isLocal", true };
            iTween.MoveTo(this._wholeView, iTween.Hash(args));
        }

        private void UpdateMapRatio()
        {
            Vector3 vector = this._uiCornerPos[2];
            Vector3 vector2 = this._uiCornerPos[0];
            float num = Mathf.Abs((float) (vector.y - vector2.y));
            Vector3 vector3 = this._uiCornerPos[1];
            Vector3 vector4 = this._uiCornerPos[0];
            float num2 = Mathf.Abs((float) (vector3.x - vector4.x));
            int width = this._uiBackgroundForShow.width;
            int height = this._uiBackgroundForShow.height;
            this._uiMapLengthRatio = ((float) width) / num2;
            this._uiMapWidthRatio = ((float) height) / num;
        }

        private void UpdatePos()
        {
            if (base.IsOpened)
            {
                IEnumerator<ActionDisplay> enumerator = this._posDic.Keys.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        ActionDisplay current = enumerator.Current;
                        if (current is PlayerDisplay)
                        {
                            Vector3 position = current.GoBase.transform.position;
                            if (this.RealPosToMapPos(ref position))
                            {
                                this._posDic[current].gameObject.transform.localPosition = position;
                            }
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        public void UpdateStat(int master, int slave)
        {
            this.UpdateStat(master.ToString(), slave.ToString());
        }

        public void UpdateStat(string master, string slave)
        {
            this._labelMasterKill.text = master;
            this._labelSlaveKill.text = slave;
        }

        private static IEnumerable<Vector3> WorldPosToUIPos(IEnumerable<Vector3> originalPos)
        {
            List<Vector3> list = new List<Vector3>();
            IEnumerator<Vector3> enumerator = originalPos.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Vector3 current = enumerator.Current;
                    GameObject obj2 = new GameObject {
                        transform = { position = current }
                    };
                    list.Add(HudView.Instance.WorldToUIPoint(obj2.transform.position));
                    UnityEngine.Object.Destroy(obj2);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return list;
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }

        [CompilerGenerated]
        private sealed class <RemoveTowerOrBase>c__AnonStoreyF3
        {
            internal ulong id;

            internal bool <>m__D7(ActionDisplay x)
            {
                return (x.GetVo().Id == this.id);
            }

            internal bool <>m__D8(ActionDisplay x)
            {
                return (x.GetVo().Id == this.id);
            }
        }
    }
}

