﻿namespace com.game.module.battle
{
    using com.game;
    using com.game.module.Arena;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.WiFiPvP;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class BattleTopView : BaseView<BattleTopView>
    {
        private GameObject _bossBloodGo;
        private UISprite background;
        private UISlider bossBloodSlider;
        private UISprite bossHeadIcon;
        private float bossLastHp;
        private UILabel bossName;
        private UILabel countLabel;
        private float cutWidth;
        private int DupTotalSec;
        private UISprite foreground;
        private int frames;
        private float itweenTime;
        private int remainTime;
        private UILabel time;
        private UISprite tweenForeGround;

        public void AddBossBloodBar(MonsterDisplay item)
        {
            if (!this._bossBloodGo.activeInHierarchy)
            {
                this._bossBloodGo.SetActive(true);
                this.ResetBossBlood();
                this.countLabel.text = "5";
                this.bossName.text = item.MonsterVo.name;
                this.bossHeadIcon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                this.bossHeadIcon.spriteName = item.MonsterVo.icon.ToString();
            }
        }

        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            this.hideBossBlood();
        }

        protected override void HandleBeforeCloseView()
        {
        }

        private void hideBossBlood()
        {
            if (this._bossBloodGo.activeSelf)
            {
                this._bossBloodGo.SetActive(false);
            }
        }

        [DebuggerHidden]
        private IEnumerator HideBossHealthBar(float hp, float curHp)
        {
            return new <HideBossHealthBar>c__Iterator33 { hp = hp, curHp = curHp, <$>hp = hp, <$>curHp = curHp, <>f__this = this };
        }

        protected override void Init()
        {
            this.time = base.FindInChild<UILabel>("infos/cur_info/time/num");
            this._bossBloodGo = base.FindChild("bossblood");
            this.bossName = base.FindInChild<UILabel>("bossblood/nameLabel");
            this.countLabel = base.FindInChild<UILabel>("bossblood/countLabel");
            this.background = base.FindInChild<UISprite>("bossblood/background");
            this.foreground = base.FindInChild<UISprite>("bossblood/foreground");
            this.tweenForeGround = base.FindInChild<UISprite>("bossblood/tweenGround");
            this.bossHeadIcon = base.FindInChild<UISprite>("bossblood/headIcon");
            this.bossBloodSlider = this._bossBloodGo.GetComponent<UISlider>();
            this._bossBloodGo.SetActive(false);
            this._bossBloodGo.AddComponent<UpdateBossHealthBar>();
        }

        private void onTweenComplete()
        {
            if (this.itweenTime >= 0.7f)
            {
                vp_Timer.CancelAll("onTweenComplete");
                this.tweenForeGround.SetActive(false);
            }
            this.itweenTime += 0.05f;
            this.tweenForeGround.width = (int) (this.cutWidth * ((0.7f - this.itweenTime) / 0.7f));
        }

        public void recoverBloodBar(float fullHp, float curHp)
        {
            if (curHp <= 0f)
            {
                this.ResetBossBlood();
                this._bossBloodGo.SetActive(false);
            }
            else
            {
                if (!this._bossBloodGo.activeSelf)
                {
                    this._bossBloodGo.SetActive(true);
                }
                if (this.bossBloodSlider != null)
                {
                    if ((curHp % (fullHp / 5f)) == 0f)
                    {
                        this.bossBloodSlider.value = 1f;
                    }
                    else
                    {
                        this.bossBloodSlider.value = (curHp % (fullHp / 5f)) / (fullHp / 5f);
                    }
                    int num = ((int) (((double) (curHp / fullHp)) / 0.2)) + 1;
                    if (this.foreground != null)
                    {
                        if (curHp == fullHp)
                        {
                            this.foreground.spriteName = "blood";
                        }
                        else
                        {
                            this.foreground.spriteName = "blood" + num.ToString();
                        }
                        this.foreground.MakePixelPerfect();
                    }
                    if (this.background != null)
                    {
                        if (num > 1)
                        {
                            this.background.spriteName = "blood" + ((num - 1)).ToString();
                        }
                        else if (num == 1)
                        {
                            this.background.spriteName = "bossHpdi";
                        }
                        this.background.SetActive(true);
                        this.background.MakePixelPerfect();
                    }
                    if (this.countLabel != null)
                    {
                        if (num >= 6)
                        {
                            this.countLabel.text = (num - 1).ToString();
                        }
                        else
                        {
                            this.countLabel.text = num.ToString();
                        }
                    }
                }
            }
        }

        public override void RegisterUpdateHandler()
        {
        }

        public void ResetBossBlood()
        {
            this.tweenForeGround.SetActive(false);
            this.bossName.text = string.Empty;
            this.countLabel.text = "5";
            this.background.spriteName = "bossHp4";
            this.background.MakePixelPerfect();
            this.foreground.spriteName = "bossHp5";
            this.foreground.MakePixelPerfect();
            this.bossBloodSlider.value = 1f;
            this._bossBloodGo.GetComponent<UpdateBossHealthBar>().Reset();
        }

        private void SetDungeonTime(int leftSecond)
        {
            if (leftSecond < 0)
            {
                leftSecond = 0;
            }
            int num = leftSecond / 60;
            string str = (num >= 10) ? (num + string.Empty) : ("0" + num);
            int num2 = leftSecond % 60;
            string str2 = (num2 >= 10) ? (num2 + string.Empty) : ("0" + num2);
            this.time.text = str + ":" + str2;
        }

        public override void Update()
        {
            if (base.IsOpened && ((null != this.gameObject) && this.gameObject.activeInHierarchy))
            {
                if (AppMap.Instance.IsInWifiPVP)
                {
                    this.SetDungeonTime(WifiPvpManager.Instance.GetTimecost());
                }
                else if (AppMap.Instance.IsInArena)
                {
                    this.SetDungeonTime(Mathf.FloorToInt(ArenaManager.Instance.TotalTime));
                }
                else
                {
                    this.SetDungeonTime(Mathf.CeilToInt(DungeonMgr.Instance.time));
                }
            }
        }

        public void updateBossBloodBar(float hp, float curHp, float demageHp)
        {
            if (curHp <= 0f)
            {
                if (this._bossBloodGo.activeInHierarchy)
                {
                    this._bossBloodGo.GetComponent<UpdateBossHealthBar>().StartCoroutine(this.HideBossHealthBar(hp, curHp));
                }
            }
            else
            {
                if (!this._bossBloodGo.activeSelf)
                {
                    this._bossBloodGo.SetActive(true);
                }
                this._bossBloodGo.GetComponent<UpdateBossHealthBar>().SetHp((int) hp, (int) curHp);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }

        [CompilerGenerated]
        private sealed class <HideBossHealthBar>c__Iterator33 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal float <$>curHp;
            internal float <$>hp;
            internal BattleTopView <>f__this;
            internal float curHp;
            internal float hp;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<>f__this._bossBloodGo.GetComponent<UpdateBossHealthBar>().SetHp((int) this.hp, (int) this.curHp);
                        this.$current = new WaitForSeconds(2f);
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.<>f__this.ResetBossBlood();
                        this.<>f__this._bossBloodGo.SetActive(false);
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

