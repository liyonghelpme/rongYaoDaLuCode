﻿namespace com.game.module.battle
{
    using com.game.module.core;
    using System;

    public class BattleView : BaseView<BattleView>
    {
        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            Singleton<BattleBottomLeftView>.Instance.OpenView();
            Singleton<BattleBottomRightView>.Instance.OpenView();
            Singleton<BattleTopLeftView>.Instance.OpenView();
            Singleton<BattleRightView>.Instance.OpenView();
            Singleton<BattleTopView>.Instance.OpenView();
            Singleton<BattleCenterView>.Instance.OpenView();
            Singleton<BattleLeftView>.Instance.OpenView();
            Singleton<BattleTopRightView>.Instance.OpenView();
        }

        protected override void HandleBeforeCloseView()
        {
            Singleton<BattleBottomLeftView>.Instance.CloseView();
            Singleton<BattleBottomRightView>.Instance.CloseView();
            Singleton<BattleTopLeftView>.Instance.CloseView();
            Singleton<BattleRightView>.Instance.CloseView();
            Singleton<BattleTopView>.Instance.CloseView();
            Singleton<BattleCenterView>.Instance.CloseView();
            Singleton<BattleLeftView>.Instance.CloseView();
            Singleton<BattleTopRightView>.Instance.CloseView();
        }

        public void HideBattleViewWhenAllAIMode()
        {
            Singleton<BattleBottomLeftView>.Instance.CloseView();
            Singleton<BattleBottomRightView>.Instance.CloseView();
            Singleton<BattleLeftView>.Instance.CloseView();
            Singleton<BattleTopRightView>.Instance.CloseView();
        }

        protected override void Init()
        {
            Singleton<BattleBottomLeftView>.Instance.gameObject = base.FindChild("bottomleft");
            Singleton<BattleBottomRightView>.Instance.gameObject = base.FindChild("bottomright");
            Singleton<BattleRightView>.Instance.gameObject = base.FindChild("right");
            Singleton<BattleTopLeftView>.Instance.gameObject = base.FindChild("topleft");
            Singleton<BattleTopView>.Instance.gameObject = base.FindChild("top");
            Singleton<BattleCenterView>.Instance.gameObject = base.FindChild("center");
            Singleton<BattleLeftView>.Instance.gameObject = base.FindChild("Left");
            Singleton<BattleTopRightView>.Instance.gameObject = base.FindChild("topright");
        }

        public override void RegisterUpdateHandler()
        {
        }

        public void ResetBattleView()
        {
            this.CloseView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.LowLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Battle/BattleView.assetbundle";
            }
        }

        public override ViewType viewType
        {
            get
            {
                return ViewType.BattleView;
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

