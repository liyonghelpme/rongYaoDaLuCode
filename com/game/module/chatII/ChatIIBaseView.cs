﻿namespace com.game.module.chatII
{
    using System;
    using UnityEngine;

    public class ChatIIBaseView
    {
        protected GameObject _gameObject;
        private bool _isActive;

        public ChatIIBaseView(GameObject go)
        {
            this._gameObject = go;
            this.Init();
        }

        public virtual void CancelUpdateHandler()
        {
        }

        public virtual void HandleAfterOpenView()
        {
        }

        public virtual void HandleBeforeCloseView()
        {
        }

        protected virtual void Init()
        {
        }

        public virtual void RegisterUpdateHandler()
        {
        }

        public bool active
        {
            get
            {
                return this._isActive;
            }
            set
            {
                this._isActive = value;
                this._gameObject.SetActive(value);
            }
        }

        public GameObject gameObject
        {
            get
            {
                return this._gameObject;
            }
        }
    }
}

