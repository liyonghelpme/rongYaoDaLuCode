﻿namespace com.game.module.chatII
{
    using System;

    public static class ChatIIConfig
    {
        public static int MAIN_VIEW_MSG_NUM = 4;
        public static int MAX_CHARACTOR_NUM = 0x30;
        public static int MAX_GRID_VIEW_SIZE = 30;
        public static int MAX_RECORD_TIME = 15;
        public static int MAX_VOICE_QUEUE_NUM = 15;
        public static int MIN_BG_HEIGHT = 0x2a;
        public static int MIN_BG_WIDTH = 0x22;
        public static ChatIIType[] TypeList = new ChatIIType[] { ChatIIType.Public, ChatIIType.Private, ChatIIType.Guild, ChatIIType.Team };
    }
}

