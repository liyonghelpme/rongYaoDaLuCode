﻿namespace com.game.module.chatII
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;
    using UnityEngine;

    public class ChatIIControl : BaseControl<ChatIIControl>
    {
        private void Fun_10_1(INetData data)
        {
            ChatReqMsg_10_1 g__ = new ChatReqMsg_10_1();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void Fun_10_11(INetData data)
        {
            ChatReqVoiceMsg_10_11 g__ = new ChatReqVoiceMsg_10_11();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void Fun_10_12(INetData data)
        {
        }

        private void Fun_10_13(INetData data)
        {
            ChatVoiceInfoReqMsg_10_13 g__ = new ChatVoiceInfoReqMsg_10_13();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                Debug.Log(string.Concat(new object[] { "Voice   : id = ", g__.voiceId, "receiceMessage", g__.contentVoice.ToArray().Length }));
                Singleton<ChatIIMode>.Instance.ReceiveVoiceInfo(g__.voiceId, g__.contentVoice.ToArray());
            }
        }

        private void Fun_10_14(INetData data)
        {
            ChatVoiceContentPushMsg_10_14 g__ = new ChatVoiceContentPushMsg_10_14();
            g__.read(data.GetMemoryStream());
            if (this.IsValidMsg(g__.chatType))
            {
                ChatIIVo recChat = new ChatIIVo {
                    chatType = g__.chatType,
                    senderId = g__.senderId,
                    serverId = g__.serverId,
                    senderName = g__.senderName,
                    senderSex = g__.senderSex,
                    senderJob = g__.senderJob,
                    senderLvl = g__.senderLvl,
                    senderVip = g__.senderVip,
                    content = g__.contentText,
                    senderRoleIcon = g__.senderRoleIcon,
                    nationId = g__.senderNation,
                    voiceId = g__.voiceId,
                    voiceTime = g__.voiceTime,
                    msgType = MessageType.SOUND
                };
                this.SendChatMsg(recChat);
            }
        }

        private void Fun_10_2(INetData data)
        {
        }

        private void Fun_10_3(INetData data)
        {
            ChatContentPushMsg_10_3 g__ = new ChatContentPushMsg_10_3();
            g__.read(data.GetMemoryStream());
            if (this.IsValidMsg(g__.chatType))
            {
                ChatIIVo recChat = new ChatIIVo {
                    chatType = g__.chatType,
                    senderId = g__.senderId,
                    serverId = g__.serverId,
                    senderName = g__.senderName,
                    senderSex = g__.senderSex,
                    senderJob = g__.senderJob,
                    senderLvl = g__.senderLvl,
                    senderVip = g__.senderVip,
                    content = g__.content,
                    senderRoleIcon = g__.senderRoleIcon,
                    goods = (g__.goodsList.Count <= 0) ? null : g__.goodsList[0],
                    nationId = g__.senderNation,
                    msgType = MessageType.TEXT
                };
                this.SendChatMsg(recChat);
            }
        }

        private void Fun_10_6(INetData data)
        {
        }

        private void Fun_10_7(INetData data)
        {
        }

        private bool IsValidMsg(byte msgType)
        {
            bool flag = msgType == 1;
            bool flag2 = msgType == 3;
            bool flag3 = msgType == 7;
            bool flag4 = msgType == 8;
            return (((flag || flag2) || flag3) || flag4);
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("2561", new NetMsgCallback(this.Fun_10_1));
            AppNet.main.addCMD("2562", new NetMsgCallback(this.Fun_10_2));
            AppNet.main.addCMD("2563", new NetMsgCallback(this.Fun_10_3));
            AppNet.main.addCMD("2566", new NetMsgCallback(this.Fun_10_6));
            AppNet.main.addCMD("2567", new NetMsgCallback(this.Fun_10_7));
            AppNet.main.addCMD("2571", new NetMsgCallback(this.Fun_10_11));
            AppNet.main.addCMD("2572", new NetMsgCallback(this.Fun_10_12));
            AppNet.main.addCMD("2573", new NetMsgCallback(this.Fun_10_13));
            AppNet.main.addCMD("2574", new NetMsgCallback(this.Fun_10_14));
        }

        private void SendChatMsg(ChatIIVo recChat)
        {
            Singleton<ChatIIMode>.Instance.AddChatMsg(recChat);
        }
    }
}

