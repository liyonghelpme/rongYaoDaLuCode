﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class ChatIIDiscussView : BaseView<ChatIIDiscussView>
    {
        private List<IChatIIMessageView> _chatViewList = new List<IChatIIMessageView>();
        private ChatIIGridView _gridView;
        private ChatIIInputView _inputView;
        private Dictionary<MessageOwnerType, GameObject> _itemGoDic = new Dictionary<MessageOwnerType, GameObject>();
        private GameObject _otherItemGo;
        private GameObject _selfItemGo;

        public void AddText(ChatIIVo addMsg, bool immediately = false)
        {
            MessageOwnerType oTHER;
            if (addMsg.senderName != MeVo.instance.Name)
            {
                oTHER = MessageOwnerType.OTHER;
            }
            else
            {
                oTHER = MessageOwnerType.SELF;
            }
            IChatIIMessageView item = this._gridView.Add(this._itemGoDic[oTHER], oTHER);
            this._chatViewList.Add(item);
            item.SetInfo(addMsg, immediately);
        }

        public override void CancelUpdateHandler()
        {
            Singleton<ChatMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateMsg);
        }

        public void HandleAfterOpenView()
        {
            this.SwitchChatType();
            this._inputView.HandleAfterOpenView();
        }

        public void HandleBeforeCloseView()
        {
            this._inputView.HandleBeforeCloseView();
        }

        protected override void Init()
        {
            this._selfItemGo = base.FindChild("list/grid/myChatItem");
            this._selfItemGo.transform.parent = this.gameObject.transform;
            this._selfItemGo.SetActive(false);
            this._otherItemGo = base.FindChild("list/grid/otherChatItem");
            this._otherItemGo.transform.parent = this.gameObject.transform;
            this._otherItemGo.SetActive(false);
            this._gridView = base.FindInChild<ChatIIGridView>("list/grid");
            this._inputView = new ChatIIInputView(base.FindChild("inputView"));
            this.InitDic();
        }

        private void InitDic()
        {
            this._itemGoDic[MessageOwnerType.SELF] = this._selfItemGo;
            this._itemGoDic[MessageOwnerType.OTHER] = this._otherItemGo;
        }

        private void ShowCacheMsg()
        {
            List<ChatIIVo> cacheMsgList = Singleton<ChatIIMode>.Instance.cacheMsgList;
            List<ChatIIVo> list2 = cacheMsgList;
            lock (list2)
            {
                int count = cacheMsgList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.AddText(cacheMsgList[i], false);
                }
                cacheMsgList.Clear();
            }
        }

        public void ShowInputView(bool active)
        {
            this._inputView.gameObject.SetActive(active);
        }

        public void SwitchChatType()
        {
            this._gridView.Clear();
            Singleton<ChatIIMode>.Instance.cacheMsgList.Clear();
            List<ChatIIVo> currentTypeMsgList = Singleton<ChatIIMode>.Instance.GetCurrentTypeMsgList();
            int count = currentTypeMsgList.Count;
            int num2 = (count >= ChatIIConfig.MAX_GRID_VIEW_SIZE) ? ChatIIConfig.MAX_GRID_VIEW_SIZE : count;
            for (int i = 0; i < num2; i++)
            {
                this.AddText(currentTypeMsgList[(count - num2) + i], true);
            }
            this._gridView.SetActive(false);
            this._gridView.SetActive(true);
        }

        public void Update()
        {
            if (this._inputView.gameObject.activeInHierarchy)
            {
                this._inputView.Update();
            }
        }

        public void UpdateMsg(object sender, int code)
        {
            if (code == Singleton<ChatIIMode>.Instance.UPDATE_CHAT_MSG)
            {
                this.ShowCacheMsg();
            }
        }
    }
}

