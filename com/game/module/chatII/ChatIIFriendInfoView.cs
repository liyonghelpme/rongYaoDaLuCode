﻿namespace com.game.module.chatII
{
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class ChatIIFriendInfoView : BaseView<ChatIIFriendInfoView>
    {
        private Button _chatBtn;
        private Button _closeBtn;
        private UILabel _fight;
        private UILabel _fightLabel;
        private Button _friendBtn;
        private List<ChatIIHeadView> _generalHeadViewList = new List<ChatIIHeadView>();
        private UILabel _guildName;
        private UILabel _guildNameLabel;
        private Button _maskBtn;
        private UILabel _name;
        private ChatIIHeadView _roleHeadView;
        private const int GENERAL_HEAD_VIEW_NUM = 3;

        protected override void Init()
        {
            this._name = base.FindInChild<UILabel>("name");
            this._guildNameLabel = base.FindInChild<UILabel>("labels/1");
            this._guildName = base.FindInChild<UILabel>("labels/guildName");
            this._fightLabel = base.FindInChild<UILabel>("labels/2");
            this._fight = base.FindInChild<UILabel>("labels/fight");
            this._roleHeadView = new ChatIIHeadView(base.FindChild("headView"), null);
            this._friendBtn = base.FindInChild<Button>("friendBtn");
            this._chatBtn = base.FindInChild<Button>("chatBtn");
            this._closeBtn = base.FindInChild<Button>("closeBtn");
            this._maskBtn = base.FindInChild<Button>("mask");
            this.InitHeadList();
            this.InitBtnsHandler();
            this.InitLanguage();
        }

        private void InitBtnsHandler()
        {
            this._friendBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnFriendBtnClick);
            this._chatBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnChatBtnClick);
            this._closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnCloseBtnClick);
            this._maskBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnCloseBtnClick);
        }

        private void InitHeadList()
        {
            for (int i = 0; i < 3; i++)
            {
                ChatIIHeadView item = new ChatIIHeadView(base.FindChild("items/headView" + i.ToString()), null);
                this._generalHeadViewList.Add(item);
            }
        }

        private void InitLanguage()
        {
            this._friendBtn.label.text = LanguageManager.GetWord("ChatII.addFriend");
            this._chatBtn.label.text = LanguageManager.GetWord("ChatII.chat");
        }

        private void OnChatBtnClick(GameObject go)
        {
        }

        private void OnCloseBtnClick(GameObject go)
        {
            this.CloseView();
        }

        private void OnFriendBtnClick(GameObject go)
        {
        }

        public void SetInfo()
        {
            this._name.text = "111";
            this._guildName.text = "12234";
            this._fight.text = "ddd";
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }
    }
}

