﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class ChatIIFriendItemView : ChatIIBaseView
    {
        private Button _chatBtn;
        private ChatIIHeadView _headView;
        private Button _infoBtn;
        private UILabel _nameLabel;
        private UILabel _timeLabel;

        public ChatIIFriendItemView(GameObject go) : base(go)
        {
        }

        protected override void Init()
        {
            if (base._gameObject == null)
            {
                Debug.LogError("ChatIIFriendItemView is null");
            }
            else
            {
                this._headView = new ChatIIHeadView(NGUITools.FindChild(base._gameObject, "headView"), null);
                this._nameLabel = NGUITools.FindInChild<UILabel>(base._gameObject, "name");
                this._timeLabel = NGUITools.FindInChild<UILabel>(base._gameObject, "timeLabel");
                this._infoBtn = NGUITools.FindInChild<Button>(base._gameObject, "infoBtn");
                this._infoBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnInfoBtnClick);
                this._chatBtn = NGUITools.FindInChild<Button>(base._gameObject, string.Empty);
                this._chatBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnChatBtnClick);
            }
        }

        private void OnChatBtnClick(GameObject go)
        {
            Singleton<ChatIIMainPanel>.Instance.ShowFriendView(false);
        }

        private void OnInfoBtnClick(GameObject go)
        {
            Singleton<ChatIIFriendInfoView>.Instance.OpenView();
            Singleton<ChatIIFriendInfoView>.Instance.SetInfo();
        }

        public void SetInfo()
        {
            this._nameLabel.text = "ddddd";
            this._timeLabel.text = "19:12";
        }
    }
}

