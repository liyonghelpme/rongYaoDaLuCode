﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class ChatIIFriendView : BaseView<ChatIIFriendView>
    {
        private UIGrid _gird;
        private UIInput _inputField;
        private GameObject _itemGo;
        private List<ChatIIFriendItemView> _itemList = new List<ChatIIFriendItemView>();
        private Button _searchBtn;
        private ChatIIFriendItemView _templateItem;

        protected override void Init()
        {
            this._searchBtn = base.FindInChild<Button>("search/searchBtn");
            this._inputField = base.FindInChild<UIInput>("search/input");
            this._gird = base.FindInChild<UIGrid>("list/grid");
            this._itemGo = base.FindChild("list/grid/item");
            this._templateItem = new ChatIIFriendItemView(this._itemGo);
            this._itemList.Add(this._templateItem);
        }

        public void UpdateFriendsView()
        {
            for (int i = 0; i < 5; i++)
            {
                ChatIIFriendItemView item = new ChatIIFriendItemView(NGUITools.AddChild(this._gird.gameObject, this._itemGo));
                this._itemList.Add(item);
            }
            this._gird.Reposition();
        }
    }
}

