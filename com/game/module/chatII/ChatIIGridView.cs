﻿namespace com.game.module.chatII
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ChatIIGridView : UIGrid
    {
        private Dictionary<MessageOwnerType, List<IChatIIMessageView>> _catchedItemsDic = new Dictionary<MessageOwnerType, List<IChatIIMessageView>>();
        private int _currentY;
        private int _lastItemHeight;
        private Vector2 _scorllOffset;
        private UIScrollView _scrollView;
        private Vector3 _scrollViewInitPos;
        private float _scrollViewY;
        private float _totalHight;
        private Queue<IChatIIMessageView> _viewQueue = new Queue<IChatIIMessageView>();

        public IChatIIMessageView Add(GameObject item, MessageOwnerType type)
        {
            IChatIIMessageView view = this.CreateMessageViewItem(item, type);
            view.gameObject.SetActive(true);
            return view;
        }

        private void Awake()
        {
            this.InitView();
        }

        public void Clear()
        {
            foreach (IChatIIMessageView view in this._viewQueue)
            {
                view.gameObject.transform.localPosition = new Vector3(0f, 0f, view.gameObject.transform.localPosition.z);
                view.gameObject.SetActive(false);
                view.gameObject.transform.parent = null;
                this._catchedItemsDic[view.GetType()].Add(view);
            }
            this._scrollView.transform.localPosition = this._scrollViewInitPos;
            this._scrollView.panel.clipOffset = this._scorllOffset;
            this._currentY = 0;
            this._lastItemHeight = 0;
            this._viewQueue.Clear();
            this._totalHight = 0f;
            this._scrollViewY = this._scrollViewInitPos.y;
        }

        private IChatIIMessageView CreateMessageViewItem(GameObject item, MessageOwnerType type)
        {
            IChatIIMessageView view;
            if (this._catchedItemsDic[type].Count > 0)
            {
                view = this._catchedItemsDic[type][0];
                this._catchedItemsDic[type].RemoveAt(0);
                view.gameObject.SetActive(true);
                view.gameObject.transform.parent = base.transform;
            }
            else
            {
                GameObject go = NGUITools.AddChild(base.gameObject, item);
                if (type == MessageOwnerType.SELF)
                {
                    view = go.AddMissingComponent<ChatIISelfMessageItemView>();
                }
                else if (type == MessageOwnerType.OTHER)
                {
                    view = go.AddMissingComponent<ChatIIOtherMessageItemView>();
                }
                else
                {
                    Debug.LogError("add ichatview error");
                    view = null;
                }
            }
            if (view != null)
            {
                view.gridView = this;
                this._viewQueue.Enqueue(view);
            }
            return view;
        }

        public void InitView()
        {
            this._scrollView = NGUITools.FindInParents<UIScrollView>(base.gameObject);
            this._scrollViewInitPos = this._scrollView.transform.localPosition;
            this._scorllOffset = this._scrollView.panel.clipOffset;
            this._scrollViewY = this._scrollViewInitPos.y;
            this._catchedItemsDic[MessageOwnerType.SELF] = new List<IChatIIMessageView>();
            this._catchedItemsDic[MessageOwnerType.OTHER] = new List<IChatIIMessageView>();
        }

        [ContextMenu("Execute")]
        public override void Reposition()
        {
            if ((Application.isPlaying && !base.mInitDone) && NGUITools.GetActive(this))
            {
                base.mReposition = true;
            }
            else
            {
                if (!base.mInitDone)
                {
                    this.Init();
                }
                Transform target = base.transform;
                if (base.keepWithinPanel && (base.mPanel != null))
                {
                    base.mPanel.ConstrainTargetToBounds(target, true);
                }
                if (base.onReposition != null)
                {
                    base.onReposition();
                }
            }
        }

        private void RepositionItemsY(int y)
        {
            foreach (IChatIIMessageView view in this._viewQueue)
            {
                Vector3 localPosition = view.gameObject.transform.localPosition;
                view.gameObject.transform.localPosition = new Vector3(localPosition.x, localPosition.y + y, localPosition.z);
            }
            this._currentY += y;
        }

        public void SetItemPosition(IChatIIMessageView view, bool immediately)
        {
            this.SetPosition(view, immediately);
            this.UpdateItemQueue();
        }

        private void SetPosition(IChatIIMessageView view, bool immediately = false)
        {
            Transform transform = view.gameObject.transform;
            float z = transform.localPosition.z;
            Vector3 vector = new Vector3(0f, (float) (this._currentY -= this._lastItemHeight), z);
            this._totalHight += view.GetHeight();
            this._lastItemHeight = view.GetHeight();
            transform.localPosition = vector;
            Vector3 pos = new Vector3(0f, 0f, 0f);
            float y = 0f;
            bool flag = false;
            if ((this._totalHight >= this._scrollView.panel.height) && (this._totalHight < (this._scrollView.panel.height + this._lastItemHeight)))
            {
                y = this._totalHight - this._scrollView.panel.height;
                pos = new Vector3(0f, this._scrollViewY + y, this._scrollView.gameObject.transform.localPosition.z);
                flag = true;
            }
            else if (this._totalHight >= (this._scrollView.panel.height + this._lastItemHeight))
            {
                pos = new Vector3(0f, this._scrollViewY + this._lastItemHeight, this._scrollView.gameObject.transform.localPosition.z);
                y = this._lastItemHeight;
                flag = true;
            }
            if (flag)
            {
                if (immediately)
                {
                    this._scrollView.MoveRelative(new Vector3(0f, y, 0f));
                    this._scrollViewY = this._scrollView.gameObject.transform.localPosition.y;
                }
                else
                {
                    SpringPanel.Begin(this._scrollView.gameObject, pos, 15f);
                    this._scrollViewY = pos.y;
                }
            }
        }

        private void UpdateItemQueue()
        {
            if (this._viewQueue.Count > ChatIIConfig.MAX_GRID_VIEW_SIZE)
            {
                IChatIIMessageView item = this._viewQueue.Dequeue();
                item.Clear();
                item.gameObject.transform.parent = base.gameObject.transform.parent;
                item.gameObject.SetActive(false);
                this._catchedItemsDic[item.GetType()].Add(item);
            }
        }
    }
}

