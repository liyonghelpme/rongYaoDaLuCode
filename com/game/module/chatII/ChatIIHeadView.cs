﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using com.game.module.General;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ChatIIHeadView : ChatIIBaseView
    {
        private UISprite _background;
        private Button _button;
        private Action<GeneralInfo> _clickHandler;
        private GeneralInfo _genenralInfo;
        private UISprite _headIcon;
        private int _level;
        private UILabel _levelLabel;
        private short _roleIconId;

        public ChatIIHeadView(GameObject go, Action<GeneralInfo> clickHandler = null) : base(go)
        {
            this._clickHandler = clickHandler;
        }

        public void Clear()
        {
            this.level = 0;
            this.roleIconId = 0;
        }

        public int GetHeight()
        {
            return this._background.height;
        }

        public int GetWidth()
        {
            return this._background.width;
        }

        protected override void Init()
        {
            if (base._gameObject == null)
            {
                Debug.LogError("ChatIIHeadView = null");
            }
            else
            {
                this._headIcon = NGUITools.FindInChild<UISprite>(base._gameObject, "headIcon");
                this._headIcon.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
                this._background = NGUITools.FindInChild<UISprite>(base._gameObject, "headBg");
                this._levelLabel = NGUITools.FindInChild<UILabel>(base._gameObject, "level");
                this._button = NGUITools.FindInChild<Button>(base._gameObject, string.Empty);
                if (this._button != null)
                {
                    this._button.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
                }
            }
        }

        private void OnClick(GameObject go)
        {
            if ((this._clickHandler != null) && (this._genenralInfo != null))
            {
                this._clickHandler(this._genenralInfo);
            }
        }

        private void SetHeadIcon()
        {
            this._headIcon.spriteName = this._roleIconId.ToString();
        }

        public int level
        {
            set
            {
                this._level = value;
                this._levelLabel.text = "Lv." + value.ToString();
            }
        }

        public short roleIconId
        {
            set
            {
                this._roleIconId = value;
                this.SetHeadIcon();
            }
        }
    }
}

