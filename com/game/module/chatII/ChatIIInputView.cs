﻿namespace com.game.module.chatII
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.module.core;
    using System;
    using System.IO;
    using System.Linq;
    using UnityEngine;

    internal class ChatIIInputView : ChatIIBaseView
    {
        private UISprite _bg;
        private float _bgInitHeight;
        private Button _iconBtn;
        private bool _isTalking;
        private double _lastPressTime;
        private string _lastString;
        private ChatIISoundBtn _soundBtn;
        private UISprite _textBg;
        private ChatIIUIInput _textInput;
        private UILabel _textLabel;
        private UIToggle _toggle;
        private MessageType _type;
        private byte _voiceTime;

        public ChatIIInputView(GameObject go) : base(go)
        {
            this._type = MessageType.TEXT;
        }

        public override void HandleAfterOpenView()
        {
            GlobalAPI.facade.Add(0x65, new NoticeListener(this.OnReceiceText));
            GlobalAPI.facade.Add(0x67, new NoticeListener(this.OnAutoEndTalk));
            GlobalAPI.facade.Add(0x66, new NoticeListener(this.OnVoiceComplete));
            GlobalAPI.facade.Add(0x68, new NoticeListener(this.OnVoicePlayedOver));
        }

        public override void HandleBeforeCloseView()
        {
            GlobalAPI.facade.Remove(0x65, new NoticeListener(this.OnReceiceText));
            GlobalAPI.facade.Remove(0x67, new NoticeListener(this.OnAutoEndTalk));
            GlobalAPI.facade.Remove(0x66, new NoticeListener(this.OnVoiceComplete));
            GlobalAPI.facade.Remove(0x68, new NoticeListener(this.OnVoicePlayedOver));
        }

        protected override void Init()
        {
            this._toggle = NGUITools.FindInChild<UIToggle>(base._gameObject, "modeBtn");
            this._iconBtn = NGUITools.FindInChild<Button>(base._gameObject, "iconBtn");
            this._soundBtn = NGUITools.FindInChild<ChatIISoundBtn>(base._gameObject, "inputArea/soundBtn");
            this._textInput = NGUITools.FindInChild<ChatIIUIInput>(base._gameObject, "inputArea/input");
            this._textInput.characterLimit = ChatIIConfig.MAX_CHARACTOR_NUM;
            this._textLabel = NGUITools.FindInChild<UILabel>(base._gameObject, "inputArea/input/label");
            this._textBg = NGUITools.FindInChild<UISprite>(base._gameObject, "inputArea/input/inputBg");
            this._bg = NGUITools.FindInChild<UISprite>(base._gameObject, "inputArea/bg");
            this._bgInitHeight = this._bg.height;
            this.InitHandler();
        }

        private void InitHandler()
        {
            EventDelegate.Add(this._toggle.onChange, new EventDelegate.Callback(this.OnValueChange));
            this._iconBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnIconBtnClick);
            this._soundBtn.onPress = (UIWidgetContainer.BoolDelegate) Delegate.Combine(this._soundBtn.onPress, new UIWidgetContainer.BoolDelegate(this.OnSoundBtnPress));
            EventDelegate.Add(this._textInput.onChange, new EventDelegate.Callback(this.OnChange));
        }

        private void OnAutoEndTalk(int type, int v1, int v2, object data)
        {
            Debug.Log("endTalk");
            this._soundBtn.SetPress(false);
            this._isTalking = false;
            this._soundBtn.label.text = "按住 说话";
            this.SendVoice();
        }

        private void OnChange()
        {
            this._textInput.CheckEnter();
            this._textBg.height = ((int) this._textLabel.printedSize.y) + 14;
            this._bg.height = ((int) this._textLabel.printedSize.y) + 0x1f;
            float y = (this._bg.height - this._bgInitHeight) / 2f;
            this._iconBtn.transform.localPosition = new Vector3(this._iconBtn.transform.localPosition.x, y, 0f);
            this._toggle.transform.localPosition = new Vector3(this._toggle.transform.localPosition.x, y, 0f);
        }

        private void OnIconBtnClick(GameObject go)
        {
            if (((this._textLabel.text != string.Empty) && (this._textLabel.text != this._textInput.defaultText)) && (this._textLabel.text != "点击输入"))
            {
                this.SendTextMessage(this._textInput.value);
                this._textInput.value = string.Empty;
                this._textLabel.text = string.Empty;
            }
        }

        private void OnReceiceText(int type, int v1, int v2, object data)
        {
            Singleton<ChatIIMode>.Instance.sendMessage.content = Singleton<ChatIIMode>.Instance.sendMessage.content + data.ToString();
        }

        private void OnSoundBtnPress(GameObject go, bool press)
        {
            if (press)
            {
                ChatIIVoiceManeger.Instance.BeginRecord();
                this._soundBtn.label.text = "松开 结束";
                this._isTalking = true;
                this._lastPressTime = RealTime.time;
            }
            else if (this._isTalking)
            {
                ChatIIVoiceManeger.Instance.StopRecord();
                this._isTalking = false;
                this._soundBtn.label.text = "按住 说话";
                this._voiceTime = (byte) Math.Floor((double) (RealTime.time - this._lastPressTime));
            }
        }

        private void OnValueChange()
        {
            if (this._toggle.value)
            {
                this._type = MessageType.SOUND;
                this._lastString = this._textLabel.text;
                this._textLabel.text = string.Empty;
            }
            else
            {
                this._type = MessageType.TEXT;
                this._textLabel.text = this._lastString;
            }
        }

        private void OnVoiceComplete(int type, int v1, int v2, object data)
        {
            Debug.Log("voiceComplete");
            Singleton<ChatIIMode>.Instance.currentPlayedId = 0;
        }

        private void OnVoicePlayedOver(int type, int v1, int v2, object data)
        {
            Singleton<ChatIIMode>.Instance.currentPlayedId = 0;
        }

        private void SendTextMessage(string message)
        {
            Singleton<ChatIIMode>.Instance.sendMessage.content = message;
            Singleton<ChatIIMode>.Instance.SendPublicMsg();
        }

        private void SendVoice()
        {
            if (!File.Exists(Application.persistentDataPath + "/voice/wavaudio.amr"))
            {
                Debug.LogError("打开音频文件出错");
            }
            else
            {
                Singleton<ChatIIMode>.Instance.sendMessage.voiceContent = File.ReadAllBytes(Application.persistentDataPath + "/voice/wavaudio.amr").ToList<byte>();
                Singleton<ChatIIMode>.Instance.sendMessage.voiceTime = this._voiceTime;
                Singleton<ChatIIMode>.Instance.SendVoiceMsg();
            }
        }

        public void Update()
        {
            if (this._isTalking && ((RealTime.time - this._lastPressTime) > ChatIIConfig.MAX_RECORD_TIME))
            {
                this.OnAutoEndTalk(0, 0, 0, null);
            }
        }
    }
}

