﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ChatIIMainPanel : BaseView<ChatIIMainPanel>
    {
        private Button _closeBtn;
        private UIToggle _currentToggle;
        private Button _friendReturnBtn;
        private Button _mask;
        private List<ChatIIToggleBtnView> _tabBtns = new List<ChatIIToggleBtnView>();
        private ChatIIType _type = ChatIIType.Public;
        private const int TAB_COUNT = 4;

        private int GetCurrentToggleIndex(UIToggle toggle)
        {
            for (int i = 0; i < this._tabBtns.Count; i++)
            {
                if (this._tabBtns[i].toggle == toggle)
                {
                    return i;
                }
            }
            return -1;
        }

        public void HandleAfterOpenView()
        {
            Singleton<ChatIIMode>.Instance.currentChatType = ChatIIType.Public;
            this._tabBtns[0].toggle.value = true;
            Singleton<ChatIIDiscussView>.Instance.HandleAfterOpenView();
        }

        public void HandleBeforeCloseView()
        {
            Singleton<ChatIIDiscussView>.Instance.HandleBeforeCloseView();
        }

        protected override void Init()
        {
            Singleton<ChatIIFriendView>.Instance.gameObject = base.FindChild("contents/friends");
            Singleton<ChatIIDiscussView>.Instance.gameObject = base.FindChild("contents/chatview");
            this._mask = base.FindInChild<Button>("mask");
            this._mask.onClick = new UIWidgetContainer.VoidDelegate(this.OnCloseClick);
            for (int i = 0; i < 4; i++)
            {
                ChatIIToggleBtnView item = new ChatIIToggleBtnView(base.FindChild("tabBtns/tabBtn" + i), i) {
                    type = ChatIIConfig.TypeList[i]
                };
                EventDelegate.Add(item.toggle.onChange, new EventDelegate.Callback(this.OnTableChange));
                this._tabBtns.Add(item);
            }
            this._currentToggle = this._tabBtns[0].toggle;
            this._closeBtn = base.FindInChild<Button>("closeBtn");
            this._closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnCloseClick);
            this._friendReturnBtn = base.FindInChild<Button>("returnBtn");
            this._friendReturnBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnReturnClick);
            this._friendReturnBtn.SetActive(false);
            this._tabBtns[0].toggle.value = true;
            this.UpdateContent();
            Singleton<ChatIIDiscussView>.Instance.gameObject.SetActive(true);
        }

        private void OnCloseClick(GameObject go)
        {
            Singleton<ChatIIPanel>.Instance.CloseView();
        }

        private void OnReturnClick(GameObject go)
        {
            this.ShowFriendView(true);
        }

        private void OnTableChange()
        {
            UIToggle current = UIToggle.current;
            if (current.value && (current != this._currentToggle))
            {
                this._currentToggle = current;
                Singleton<ChatIIMode>.Instance.currentChatType = ChatIIConfig.TypeList[this.GetCurrentToggleIndex(current)];
                this._type = Singleton<ChatIIMode>.Instance.currentChatType;
                this.UpdateContent();
            }
        }

        public void ShowFriendView(bool show)
        {
            Singleton<ChatIIDiscussView>.Instance.gameObject.SetActive(!show);
            Singleton<ChatIIFriendView>.Instance.gameObject.SetActive(show);
            this._friendReturnBtn.SetActive(!show);
        }

        public void Update()
        {
            Singleton<ChatIIDiscussView>.Instance.Update();
        }

        private void UpdateContent()
        {
            if (this._type == ChatIIType.Private)
            {
                this.ShowFriendView(true);
                Singleton<ChatIIFriendView>.Instance.UpdateFriendsView();
            }
            else
            {
                this._friendReturnBtn.SetActive(false);
                Singleton<ChatIIDiscussView>.Instance.gameObject.SetActive(true);
                Singleton<ChatIIDiscussView>.Instance.SwitchChatType();
            }
        }

        public void UpdateUnreadMsgCount(object sender, int code)
        {
            foreach (ChatIIToggleBtnView view in this._tabBtns)
            {
                view.SetMessageNum(Singleton<ChatIIMode>.Instance.unreadMsgNumDic[view.type]);
            }
        }
    }
}

