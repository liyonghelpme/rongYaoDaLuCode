﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class ChatIIMessageSoundView : ChatIIBaseView
    {
        private UISprite _soundIcon;
        private UILabel _soundTimeLabel;
        private Vector3 _textInitPos;
        private UILabel _textLabel;
        private MessageOwnerType _type;
        private uint _voiceId;
        private const int ONE_LINE_HIGHT = 0x23;
        private const int OTHER_TIME_LABEL_OFFSET = 15;
        private const int SELF_TIME_LABEL_OFFSET = 5;

        public ChatIIMessageSoundView(GameObject go, MessageOwnerType type) : base(go)
        {
            this._type = type;
        }

        public int GetHeight()
        {
            return (0x23 + ((int) this._textLabel.printedSize.y));
        }

        public int GetWidth()
        {
            return (int) this._textLabel.printedSize.x;
        }

        protected override void Init()
        {
            if (base._gameObject == null)
            {
                Debug.LogError("ChatIIMessageSoundView is null");
            }
            else
            {
                this._soundTimeLabel = NGUITools.FindInChild<UILabel>(base._gameObject, "soundTime");
                this._soundIcon = NGUITools.FindInChild<UISprite>(base._gameObject, "soundIcon");
                this._textLabel = NGUITools.FindInChild<UILabel>(base._gameObject, "text");
                this._textInitPos = this._textLabel.transform.localPosition;
            }
        }

        public void Play()
        {
            Singleton<ChatIIMode>.Instance.PlayVoiceById(this._voiceId);
        }

        protected virtual void RetTextPostion()
        {
            if (this._type == MessageOwnerType.SELF)
            {
                float x = this._textLabel.printedSize.x - 5f;
                this._textLabel.transform.localPosition = this._textInitPos - new Vector3(x, 0f, 0f);
            }
        }

        private void SetSoundTimeLabelPos()
        {
            Vector3 localPosition = this._textLabel.transform.localPosition;
            float x = (this._type != MessageOwnerType.SELF) ? ((localPosition.x + this._textLabel.printedSize.x) + 15f) : (localPosition.x - 5f);
            this._soundTimeLabel.transform.localPosition = new Vector3(x, this._soundTimeLabel.transform.localPosition.y, this._soundTimeLabel.transform.localPosition.z);
        }

        public void SetVo(ChatIIVo chatVo)
        {
            this._soundTimeLabel.text = string.Format("{0}\"", chatVo.voiceTime);
            this._textLabel.text = chatVo.content;
            this._voiceId = chatVo.voiceId;
            this.RetTextPostion();
            this.SetSoundTimeLabelPos();
            Debug.Log("Voice : currentReceiveId" + chatVo.voiceId);
        }
    }
}

