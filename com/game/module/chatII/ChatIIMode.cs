﻿namespace com.game.module.chatII
{
    using com.game;
    using com.game.module.core;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using UnityEngine;

    public class ChatIIMode : BaseMode<ChatIIMode>
    {
        private ChatIIType _currentChatType = ChatIIType.Public;
        private bool _hasNumInit;
        private readonly Queue<uint> _voiceIdQueue = new Queue<uint>();
        private readonly Dictionary<uint, byte[]> _voiceResDic = new Dictionary<uint, byte[]>();
        public List<ChatIIVo> cacheMsgList = new List<ChatIIVo>();
        public Dictionary<ChatIIType, List<ChatIIVo>> chatResDic = new Dictionary<ChatIIType, List<ChatIIVo>>();
        public uint currentPlayedId;
        public List<string> emoList = new List<string>();
        public string[] FilterString;
        public List<PChatGoods> goodsList = new List<PChatGoods>();
        public Queue<ChatIIVo> mainViewMsgQueue = new Queue<ChatIIVo>();
        public readonly int PLAY_VOICE = 3;
        public SendMessageVo sendMessage = new SendMessageVo();
        public Dictionary<ChatIIType, int> unreadMsgNumDic = new Dictionary<ChatIIType, int>();
        public readonly int UPDATE_CHAT_MSG = 1;
        public readonly int UPDATE_MAIN_CONTENT = 4;
        public readonly int UPDATE_UNREAD_NUM = 2;

        private void AddCacheMsg(ChatIIVo addMsg)
        {
            if ((((ChatIIType) addMsg.chatType) == this.currentChatType) || !Singleton<ChatIIPanel>.Instance.IsOpened)
            {
                List<ChatIIVo> cacheMsgList = this.cacheMsgList;
                lock (cacheMsgList)
                {
                    this.cacheMsgList.Add(addMsg);
                }
                base.DataUpdate(this.UPDATE_CHAT_MSG);
            }
        }

        public void AddChatMsg(ChatIIVo addMsg)
        {
            this.AddResMsg(addMsg);
            this.AddCacheMsg(addMsg);
            this.UpdateUnreadCount(addMsg);
            this.AddMainViewMsg(addMsg);
        }

        public void AddMainViewMsg(ChatIIVo msg)
        {
            this.mainViewMsgQueue.Enqueue(msg);
            base.DataUpdate(this.UPDATE_MAIN_CONTENT);
        }

        private void AddResMsg(ChatIIVo addMsg)
        {
            if (!this.chatResDic.ContainsKey((ChatIIType) addMsg.chatType))
            {
                this.chatResDic[(ChatIIType) addMsg.chatType] = new List<ChatIIVo>();
            }
            this.chatResDic[(ChatIIType) addMsg.chatType].Add(addMsg);
        }

        public void ClearMainViewMsgQueue()
        {
            this.mainViewMsgQueue.Clear();
        }

        private void ClearSendMessage()
        {
            this.sendMessage.content = string.Empty;
            this.sendMessage.voiceContent = null;
            this.sendMessage.voiceTime = 0;
        }

        public void ClearUnreadCount(ChatIIType type)
        {
            if (this.unreadMsgNumDic.ContainsKey(type))
            {
                this.unreadMsgNumDic[type] = 0;
                base.DataUpdate(this.UPDATE_UNREAD_NUM);
            }
        }

        public bool ContainsFilter(string str)
        {
            for (int i = 0; i < Singleton<ChatIIMode>.Instance.FilterString.Length; i++)
            {
                if (str.Contains(Singleton<ChatIIMode>.Instance.FilterString[i]))
                {
                    return true;
                }
            }
            return false;
        }

        private string FilterContent(string str)
        {
            foreach (string str2 in Singleton<ChatIIMode>.Instance.FilterString)
            {
                if (str.Contains(str2))
                {
                    str = str.Replace(str2, "*");
                }
            }
            return str;
        }

        private void FilterMsg()
        {
            string content = this.sendMessage.content;
            content = this.FilterContent(content);
            if (string.Empty != content)
            {
                this.sendMessage.content = content;
            }
        }

        public List<ChatIIVo> GetCurrentTypeMsgList()
        {
            return this.chatResDic[this.currentChatType];
        }

        public ChatIIVo GetMainViewMsg()
        {
            if (this.mainViewMsgQueue.Count >= 1)
            {
                return this.mainViewMsgQueue.Dequeue();
            }
            return null;
        }

        public void Init()
        {
            if (!this._hasNumInit)
            {
                this.InitUnreadNum();
                this.InitChatResDic();
                this.InitEmoList();
                this._hasNumInit = true;
            }
        }

        private void InitChatResDic()
        {
            foreach (ChatIIType type in ChatIIConfig.TypeList)
            {
                if (!this.chatResDic.ContainsKey(type))
                {
                    this.chatResDic[type] = new List<ChatIIVo>();
                }
            }
        }

        private void InitEmoList()
        {
            for (int i = 1; i < 0x20; i++)
            {
                this.emoList.Add(string.Format("{0:D3}", i));
            }
        }

        private void InitUnreadNum()
        {
            for (int i = 0; i < ChatIIConfig.TypeList.Length; i++)
            {
                this.unreadMsgNumDic[ChatIIConfig.TypeList[i]] = 0;
            }
        }

        public void PlayVoiceById(uint voiceId)
        {
            if (voiceId != this.currentPlayedId)
            {
                this.currentPlayedId = voiceId;
                if (this._voiceResDic.ContainsKey(voiceId))
                {
                    Debug.Log("Voice    :  缓存" + voiceId);
                    ChatIIVoiceManeger.Instance.Play(this._voiceResDic[voiceId]);
                }
                else
                {
                    this.SendVoiceRequest(voiceId);
                }
            }
        }

        public void ReceiveVoiceInfo(uint voiceId, byte[] res)
        {
            if (!this._voiceResDic.ContainsKey(voiceId))
            {
                if (this._voiceIdQueue.Count >= ChatIIConfig.MAX_VOICE_QUEUE_NUM)
                {
                    uint key = this._voiceIdQueue.Dequeue();
                    this._voiceResDic.Remove(key);
                }
                this._voiceIdQueue.Enqueue(voiceId);
                this._voiceResDic[voiceId] = res;
                ChatIIVoiceManeger.Instance.Play(res);
            }
        }

        public void SendPublicMsg()
        {
            this.FilterMsg();
            MemoryStream msdata = new MemoryStream();
            Module_10.write_10_1(msdata, this.sendMessage.sendChatType, this.sendMessage.content, this.goodsList);
            AppNet.gameNet.send(msdata, 10, 1);
            this.ClearSendMessage();
        }

        public void SendVoiceMsg()
        {
            this.FilterMsg();
            Debug.Log("Voice   sendMesage stringlength =" + this.sendMessage.voiceContent.Count);
            MemoryStream msdata = new MemoryStream();
            Module_10.write_10_11(msdata, this.sendMessage.sendChatType, this.sendMessage.content, this.sendMessage.voiceTime, this.sendMessage.voiceContent);
            AppNet.gameNet.send(msdata, 10, 11);
            this.ClearSendMessage();
        }

        public void SendVoiceRequest(uint voiceId)
        {
            Debug.Log("Voice  : requestVoiceId" + voiceId);
            MemoryStream msdata = new MemoryStream();
            Module_10.write_10_13(msdata, voiceId);
            AppNet.gameNet.send(msdata, 10, 13);
        }

        private void UpdateUnreadCount(ChatIIVo addMsg)
        {
            if (((ChatIIType) addMsg.chatType) != this.currentChatType)
            {
                Dictionary<ChatIIType, int> dictionary;
                ChatIIType type;
                int num = dictionary[type];
                (dictionary = this.unreadMsgNumDic)[type = (ChatIIType) addMsg.chatType] = num + 1;
                base.DataUpdate(this.UPDATE_UNREAD_NUM);
            }
        }

        public ChatIIType currentChatType
        {
            get
            {
                return this._currentChatType;
            }
            set
            {
                this._currentChatType = value;
                this.sendMessage.sendChatType = (byte) value;
                this.ClearUnreadCount(value);
            }
        }
    }
}

