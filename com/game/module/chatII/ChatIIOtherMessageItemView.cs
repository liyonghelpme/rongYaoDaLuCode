﻿namespace com.game.module.chatII
{
    using System;
    using UnityEngine;

    internal class ChatIIOtherMessageItemView : ChatIISelfMessageItemView
    {
        private UILabel _name;

        public override void Clear()
        {
            base.Clear();
            this._name.text = string.Empty;
        }

        public override int GetHeight()
        {
            return ((base._headView.GetHeight() + base._background.height) - ChatIIConfig.MIN_BG_HEIGHT);
        }

        public override MessageOwnerType GetType()
        {
            return MessageOwnerType.OTHER;
        }

        protected override void Init()
        {
            base.Init();
            this._name = NGUITools.FindInChild<UILabel>(base.gameObject, "name");
        }

        protected override void RetTextPostion()
        {
        }

        protected override void SetBoxCollider()
        {
            base._box.size = new Vector3((float) base._background.width, (float) base._background.height, 1f);
            base._box.center = new Vector3((float) (base._background.width / 2), (float) (-base._background.height / 2), 0f);
        }

        public override void SetInfo(ChatIIVo msg, bool immediately)
        {
            base.SetInfo(msg, immediately);
            this._name.text = msg.senderName;
        }
    }
}

