﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using System;

    public class ChatIIPanel : BaseView<ChatIIPanel>
    {
        public override void CancelUpdateHandler()
        {
            Singleton<ChatIIMode>.Instance.dataUpdated -= new DataUpateHandler(Singleton<ChatIIDiscussView>.Instance.UpdateMsg);
            Singleton<ChatIIMode>.Instance.dataUpdated -= new DataUpateHandler(Singleton<ChatIIMainPanel>.Instance.UpdateUnreadMsgCount);
        }

        protected override void HandleAfterOpenView()
        {
            Singleton<ChatIIMainPanel>.Instance.HandleAfterOpenView();
        }

        protected override void HandleBeforeCloseView()
        {
            Singleton<ChatIIMainPanel>.Instance.HandleBeforeCloseView();
        }

        protected override void Init()
        {
            if (!ChatIIVoiceManeger.Instance.hasInit)
            {
                ChatIIVoiceManeger.Instance.Init();
            }
            Singleton<ChatIIMainPanel>.Instance.gameObject = base.FindChild("chatIIMainView");
            Singleton<ChatIIFriendInfoView>.Instance.gameObject = base.FindChild("friendTipsView");
            if (Singleton<ChatIIFriendInfoView>.Instance.IsOpened)
            {
                Singleton<ChatIIFriendInfoView>.Instance.CloseView();
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<ChatIIMode>.Instance.dataUpdated += new DataUpateHandler(Singleton<ChatIIDiscussView>.Instance.UpdateMsg);
            Singleton<ChatIIMode>.Instance.dataUpdated += new DataUpateHandler(Singleton<ChatIIMainPanel>.Instance.UpdateUnreadMsgCount);
        }

        public override void Update()
        {
            Singleton<ChatIIMainPanel>.Instance.Update();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/ChatII/ChatIIPanel.assetbundle";
            }
        }
    }
}

