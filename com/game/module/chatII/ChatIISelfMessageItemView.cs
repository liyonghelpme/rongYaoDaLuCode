﻿namespace com.game.module.chatII
{
    using com.game.module.core;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using UnityEngine;

    public class ChatIISelfMessageItemView : UIWidgetContainer, IChatIIMessageView
    {
        protected UISprite _background;
        protected Button _bgBtn;
        protected BoxCollider _box;
        protected ChatIIGridView _gridView;
        protected bool _hasInit;
        protected ChatIIHeadView _headView;
        protected bool _immediately;
        protected UILabel _messageLabel;
        protected ChatIIMessageSoundView _soundView;
        private string _text;
        private Vector3 _textInitPos;
        protected MessageType _type = MessageType.TEXT;

        private void Awake()
        {
            this.Init();
        }

        public virtual void Clear()
        {
            this.text = string.Empty;
        }

        GameObject IChatIIMessageView.get_gameObject()
        {
            return base.gameObject;
        }

        protected virtual float GetEmotionX(string newLine, UILabel content)
        {
            return ((NGUIText.CalculatePrintedSize(newLine).x % ((float) content.width)) - 20f);
        }

        protected virtual float GetEmotionY(string newLine)
        {
            return (-NGUIText.CalculatePrintedSize(newLine).y + 10f);
        }

        public virtual int GetHeight()
        {
            return ((this._headView.GetHeight() <= this._background.height) ? this._background.height : this._headView.GetHeight());
        }

        public virtual MessageOwnerType GetType()
        {
            return MessageOwnerType.SELF;
        }

        protected virtual void Init()
        {
            if (!this._hasInit)
            {
                this._hasInit = true;
                if (base.gameObject == null)
                {
                    Debug.LogError("ChatIISelfMessageItemView is null");
                }
                else
                {
                    this._messageLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "chatContent/text");
                    this._textInitPos = this._messageLabel.transform.localPosition;
                    this._headView = new ChatIIHeadView(NGUITools.FindChild(base.gameObject, "headView0"), null);
                    this._background = NGUITools.FindInChild<UISprite>(base.gameObject, "chatContent/bg");
                    this._bgBtn = NGUITools.FindInChild<Button>(base.gameObject, "chatContent/bg");
                    this._bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
                    this._soundView = new ChatIIMessageSoundView(NGUITools.FindChild(base.gameObject, "chatContent/sound"), this.GetType());
                    this._box = NGUITools.FindInChild<BoxCollider>(base.gameObject, "chatContent/bg");
                    this.ShowTextOrSound();
                }
            }
        }

        private void OnClick(GameObject go)
        {
            if (this._type == MessageType.SOUND)
            {
                this._soundView.Play();
            }
        }

        private void ResizeBgBySound()
        {
            this._background.width = this._soundView.GetWidth() + ChatIIConfig.MIN_BG_WIDTH;
            this._background.height = this._soundView.GetHeight() + (ChatIIConfig.MIN_BG_HEIGHT / 2);
            this.SetBoxCollider();
        }

        private void ResizeBgByText()
        {
            this._background.width = ((int) this._messageLabel.printedSize.x) + ChatIIConfig.MIN_BG_WIDTH;
            this._background.height = ((int) this._messageLabel.printedSize.y) + (ChatIIConfig.MIN_BG_HEIGHT / 2);
            this.SetBoxCollider();
        }

        protected virtual void RetTextPostion()
        {
            float x = this._messageLabel.printedSize.x - 5f;
            this._messageLabel.transform.localPosition = this._textInitPos - new Vector3(x, 0f, 0f);
        }

        protected virtual void SetBoxCollider()
        {
            this._box.size = new Vector3((float) this._background.width, (float) this._background.height, 1f);
            this._box.center = new Vector3((float) (-this._background.width / 2), (float) (-this._background.height / 2), 0f);
        }

        public virtual void SetInfo(ChatIIVo msg, bool immediately)
        {
            if (!this._hasInit)
            {
                this.Init();
            }
            this._immediately = immediately;
            this._headView.level = msg.senderLvl;
            this._headView.roleIconId = msg.senderRoleIcon;
            this._type = msg.msgType;
            this.ShowTextOrSound();
            if (msg.msgType == MessageType.TEXT)
            {
                this.text = msg.content;
                this._bgBtn.enabled = false;
            }
            else
            {
                this._soundView.SetVo(msg);
                this._bgBtn.enabled = true;
                this.ResizeBgBySound();
            }
            this._gridView.SetItemPosition(this, this._immediately);
        }

        private void ShowEmotion(UILabel content, string cont)
        {
            List<EmotionInfo> list = new List<EmotionInfo>();
            float emotionX = 0f;
            float y = 0f;
            string str = string.Empty;
            string newLine = string.Empty;
            string item = string.Empty;
            float num3 = 0f;
            string str4 = Regex.Replace(cont, @"\[u\]|\[/u\]", string.Empty);
            int length = cont.Length;
            for (int i = 0; i < length; i++)
            {
                if (cont[i] == '\n')
                {
                    newLine = "\n";
                    str = str + cont[i];
                }
                else
                {
                    int prev = (i != 0) ? cont[i - 1] : 0;
                    num3 += NGUIText.GetGlyphWidth(cont[i], prev);
                    if (num3 >= content.width)
                    {
                        Debug.Log(cont[i]);
                        newLine = "\n";
                        num3 = 0f;
                    }
                    if (cont[i] == '/')
                    {
                        item = string.Empty;
                        int num7 = (((cont.Length - 1) - i) <= 2) ? ((cont.Length - 1) - i) : 3;
                        int num8 = 1;
                        num8 = 1;
                        while (num8 <= num7)
                        {
                            if (cont[i + num8] == '\n')
                            {
                                num7++;
                                str = str + '\n';
                                newLine = "\n";
                            }
                            else
                            {
                                item = item + cont[i + num8];
                            }
                            num8++;
                        }
                        i = (i + num8) - 1;
                        if (Singleton<ChatIIMode>.Instance.emoList.Contains(item))
                        {
                            str = str + "：";
                            newLine = newLine + "：";
                            emotionX = this.GetEmotionX(newLine, content);
                            y = this.GetEmotionY(newLine);
                            list.Add(new EmotionInfo(new Vector3(emotionX + 10f, y, 0f), "bq_" + item));
                            num3 += NGUIText.CalculatePrintedSize("：").x;
                        }
                        else
                        {
                            str = str + "/" + item;
                            newLine = newLine + "/" + item;
                        }
                    }
                    else
                    {
                        str = str + cont[i];
                        newLine = newLine + cont[i];
                    }
                    content.text = str;
                }
            }
            int childCount = content.transform.childCount;
            Transform component = content.transform.FindChild("soundIcon").GetComponent<Transform>();
            component.GetComponent<UISprite>().atlas = Singleton<AtlasManager>.Instance.GetAtlas("ChatAtlas");
            while (content.transform.childCount < list.Count)
            {
                (UnityEngine.Object.Instantiate(component.gameObject, component.position, component.rotation) as GameObject).transform.parent = component.parent;
            }
            int num10 = 0;
            IEnumerator enumerator = content.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    if (num10 < list.Count)
                    {
                        current.localPosition = list[num10].loacalPos;
                        current.localRotation = new Quaternion(0f, 0f, 0f, 0f);
                        current.localScale = new Vector3(1f, 1f, 1f);
                        current.gameObject.SetActive(true);
                        current.GetComponent<UISprite>().spriteName = list[num10].emoName;
                        num10++;
                    }
                    else
                    {
                        current.gameObject.SetActive(false);
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }

        private void ShowTextOrSound()
        {
            bool state = this._type == MessageType.TEXT;
            this._messageLabel.SetActive(state);
            this._soundView.active = !state;
        }

        public ChatIIGridView gridView
        {
            get
            {
                return this._gridView;
            }
            set
            {
                this._gridView = value;
            }
        }

        public string text
        {
            set
            {
                this._text = value;
                this._messageLabel.text = value;
                if (value != string.Empty)
                {
                    this.ShowEmotion(this._messageLabel, this._messageLabel.processedText);
                    this.RetTextPostion();
                    this.ResizeBgByText();
                }
            }
        }
    }
}

