﻿namespace com.game.module.chatII
{
    using System;

    public class ChatIISoundBtn : Button
    {
        private void OnPress(bool isPressed)
        {
            if (base.onPress != null)
            {
                UIWidgetContainer.current = this;
                base.onPress(base.gameObject, isPressed);
            }
        }

        public void SetPress(bool isPressed)
        {
            this.OnPress(isPressed);
        }
    }
}

