﻿namespace com.game.module.chatII
{
    using com.game.manager;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ChatIIToggleBtnView : ChatIIBaseView
    {
        private int _index;
        private UILabel _label;
        private int _messageCount;
        private ChatMessageNumView _messageNumView;
        private UIToggle _toggle;
        private const int INIT_INDEX = -1;
        private const int MAX_MESSAGE_NUM = 0x63;
        private const int MIN_MESSAGE_NUM = 0;
        public ChatIIType type;

        public ChatIIToggleBtnView(GameObject go, int index = -1) : base(go)
        {
            this._index = -1;
            this._index = index;
            this.SetLabelLanguage();
        }

        private string GetRootName()
        {
            string str = string.Empty;
            if (this._index != -1)
            {
                str = this._index.ToString();
            }
            return ("tabBtn" + str);
        }

        protected override void Init()
        {
            if (base._gameObject == null)
            {
                Debug.LogError("ChatToggleBtnView is null");
            }
            else
            {
                this._toggle = NGUITools.FindInChild<UIToggle>(base._gameObject, string.Empty);
                GameObject go = NGUITools.FindChild(base._gameObject, "nums");
                this._messageNumView = new ChatMessageNumView(go);
                this._label = NGUITools.FindInChild<UILabel>(base._gameObject, "Label");
            }
        }

        private void SetLabelLanguage()
        {
            this._label.text = LanguageManager.GetWord("ChatII.tabBtn" + this._index.ToString());
        }

        public void SetMessageNum(int num)
        {
            string str;
            this._messageCount = num;
            if (num <= 0)
            {
                str = string.Empty;
                this._messageNumView.active = false;
            }
            else
            {
                this._messageNumView.active = true;
                if (num >= 0x63)
                {
                    str = num.ToString() + "+";
                }
                else
                {
                    str = num.ToString();
                }
            }
            this._messageNumView.SetLabel(str);
        }

        public int messageCount
        {
            get
            {
                return this._messageCount;
            }
        }

        public UIToggle toggle
        {
            get
            {
                return this._toggle;
            }
        }
    }
}

