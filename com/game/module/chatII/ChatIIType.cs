﻿namespace com.game.module.chatII
{
    using System;

    public enum ChatIIType
    {
        Guild = 3,
        Private = 7,
        Public = 1,
        Team = 8
    }
}

