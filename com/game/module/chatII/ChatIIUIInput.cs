﻿namespace com.game.module.chatII
{
    using System;
    using System.Text.RegularExpressions;
    using UnityEngine;

    public class ChatIIUIInput : UIInput
    {
        public void CheckEnter()
        {
            if (base.label.text.Contains("\n"))
            {
                base.label.text = Regex.Replace(base.label.text, @"\n", string.Empty);
                base.value = base.label.text;
                TouchScreenKeyboard.hideInput = true;
            }
        }

        protected override void OnSelect(bool isSelected)
        {
            if (isSelected)
            {
                this.OnSelectEvent();
            }
            else
            {
                base.OnDeselectEvent();
            }
        }

        protected void OnSelectEvent()
        {
            UIInput.selection = this;
            if (base.mDoInit)
            {
                base.Init();
            }
            if ((base.label != null) && NGUITools.GetActive(this))
            {
                if (base.label.text == "点击输入")
                {
                    Debug.Log("CHAT   : defaultText" + base.defaultText);
                    base.label.text = string.Empty;
                    base.defaultText = string.Empty;
                    base.value = string.Empty;
                    base.label.alignment = NGUIText.Alignment.Left;
                }
                base.label.color = base.activeTextColor;
                if ((Application.platform == RuntimePlatform.IPhonePlayer) || (Application.platform == RuntimePlatform.Android))
                {
                    UIInput.mKeyboard = (base.inputType != UIInput.InputType.Password) ? TouchScreenKeyboard.Open(base.mValue, (TouchScreenKeyboardType) base.keyboardType, base.inputType == UIInput.InputType.AutoCorrect, false, false, false, base.defaultText) : TouchScreenKeyboard.Open(base.mValue, TouchScreenKeyboardType.Default, false, false, true);
                }
                else
                {
                    Vector2 vector = ((UICamera.current == null) || (UICamera.current.cachedCamera == null)) ? base.label.worldCorners[0] : UICamera.current.cachedCamera.WorldToScreenPoint(base.label.worldCorners[0]);
                    vector.y = Screen.height - vector.y;
                    Input.imeCompositionMode = IMECompositionMode.On;
                    Input.compositionCursorPos = vector;
                    UIInput.mDrawStart = 0;
                }
                base.UpdateLabel();
            }
        }
    }
}

