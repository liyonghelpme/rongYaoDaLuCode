﻿namespace com.game.module.chatII
{
    using PCustomDataType;
    using System;

    public class ChatIIVo
    {
        public byte chatType;
        public string content = string.Empty;
        public PChatPushGoods goods = new PChatPushGoods();
        public MessageType msgType = MessageType.TEXT;
        public byte nationId;
        public ulong senderId;
        public byte senderJob;
        public byte senderLvl;
        public string senderName = string.Empty;
        public short senderRoleIcon;
        public byte senderSex;
        public byte senderVip;
        public ushort serverId;
        public uint voiceId;
        public byte voiceTime;
    }
}

