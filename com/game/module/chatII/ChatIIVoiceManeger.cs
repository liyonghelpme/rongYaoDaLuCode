﻿namespace com.game.module.chatII
{
    using System;
    using System.IO;
    using UnityEngine;

    public class ChatIIVoiceManeger
    {
        private string _fileName = "wavaudio.amr";
        private bool _hasInit;
        private AndroidJavaClass _javaClass;
        private AndroidJavaObject _javaObject;
        private string _storePath = (Application.persistentDataPath + "/voice/");
        public static readonly ChatIIVoiceManeger Instance;

        static ChatIIVoiceManeger()
        {
            if (Instance == null)
            {
            }
            Instance = new ChatIIVoiceManeger();
        }

        public void BeginRecord()
        {
            if (this._javaObject != null)
            {
                this._javaObject.Call("BeginRecord", new object[0]);
            }
        }

        public void Init()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (!Directory.Exists(this._storePath))
                {
                    Directory.CreateDirectory(this._storePath);
                }
                this._javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                this._javaObject = this._javaClass.GetStatic<AndroidJavaObject>("currentActivity");
                object[] args = new object[] { this._storePath };
                this._javaObject.Call("SetParam", args);
                this._hasInit = true;
            }
        }

        public void Play(byte[] voice)
        {
            Debug.Log("Voice    :   playlength  " + voice.Length);
            if (this._javaObject != null)
            {
                string str = this._storePath + this._fileName;
                object[] args = new object[] { str, voice };
                this._javaObject.Call("play", args);
            }
        }

        public void StopRecord()
        {
            if (this._javaObject != null)
            {
                this._javaObject.Call("stopTalk", new object[0]);
            }
        }

        public bool hasInit
        {
            get
            {
                return this._hasInit;
            }
        }
    }
}

