﻿namespace com.game.module.chatII
{
    using System;
    using UnityEngine;

    public class ChatMessageNumView : ChatIIBaseView
    {
        private UISprite _backGround;
        private UILabel _numLabel;
        private const int MAX_MESSAGE_NUM = 0x63;
        private const int MIN_MESSAGE_NUM = 0;

        public ChatMessageNumView(GameObject go) : base(go)
        {
        }

        protected override void Init()
        {
            if (base._gameObject == null)
            {
                Debug.LogError("ChatMessageMumView is Null");
            }
            else
            {
                this._backGround = NGUITools.FindInChild<UISprite>(base._gameObject, string.Empty);
                this._numLabel = NGUITools.FindInChild<UILabel>(base._gameObject, "num");
            }
        }

        public void SetLabel(string text)
        {
            this._numLabel.text = text;
        }
    }
}

