﻿namespace com.game.module.chatII
{
    using System;
    using UnityEngine;

    public class EmotionInfo
    {
        public string emoName;
        public Vector3 loacalPos;

        public EmotionInfo(Vector3 Pos, string Name)
        {
            this.loacalPos = Pos;
            this.emoName = Name;
        }
    }
}

