﻿namespace com.game.module.chatII
{
    using System;
    using UnityEngine;

    public interface IBaseMessageView
    {
        void Clear();
        int GetHeight();
        void SetInfo(params object[] infos);

        GameObject gameObject { get; }

        UIGrid gridView { get; set; }
    }
}

