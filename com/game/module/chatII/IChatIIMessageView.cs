﻿namespace com.game.module.chatII
{
    using System;
    using UnityEngine;

    public interface IChatIIMessageView
    {
        void Clear();
        int GetHeight();
        MessageOwnerType GetType();
        void SetInfo(ChatIIVo msg, bool immediately);

        GameObject gameObject { get; }

        ChatIIGridView gridView { get; set; }

        string text { set; }
    }
}

