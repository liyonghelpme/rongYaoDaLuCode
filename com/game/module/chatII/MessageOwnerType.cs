﻿namespace com.game.module.chatII
{
    using System;

    public enum MessageOwnerType
    {
        OTHER = 2,
        SELF = 1
    }
}

