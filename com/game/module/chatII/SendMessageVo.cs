﻿namespace com.game.module.chatII
{
    using System;
    using System.Collections.Generic;

    public class SendMessageVo
    {
        public string content = string.Empty;
        public string privateChatName;
        public ulong privateChatRoleId;
        public byte sendChatType = 1;
        public List<byte> voiceContent = new List<byte>();
        public byte voiceTime;
    }
}

