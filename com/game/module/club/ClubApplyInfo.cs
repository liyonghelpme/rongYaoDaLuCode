﻿namespace com.game.module.club
{
    using System;
    using System.Collections.Generic;

    public class ClubApplyInfo
    {
        private List<ClubApplyItemVo> _list = new List<ClubApplyItemVo>();

        public void Add(ClubApplyItemVo apply)
        {
            int index = this.IndexOf(apply.applicantId);
            if (index < 0)
            {
                this._list.Add(apply);
            }
            else
            {
                this._list[index] = apply;
            }
        }

        public void Clear()
        {
            this._list.Clear();
        }

        public ClubApplyItemVo GetApply(int id)
        {
            int index = this.IndexOf(id);
            return ((index < 0) ? null : this._list[index]);
        }

        private int IndexOf(int applicantId)
        {
            for (int i = 0; i < this._list.Count; i++)
            {
                if (this._list[i].applicantId == applicantId)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Remove(int applicantId)
        {
            int index = this.IndexOf(applicantId);
            if (index >= 0)
            {
                this._list.RemoveAt(index);
            }
        }

        public List<ClubApplyItemVo> list
        {
            get
            {
                return this._list;
            }
        }
    }
}

