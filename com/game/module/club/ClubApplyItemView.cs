﻿namespace com.game.module.club
{
    using System;
    using UnityEngine;

    public class ClubApplyItemView
    {
        private Button _btnAccept;
        private Button _btnReject;
        private GameObject _go;
        private UISprite _icon;
        private UILabel _lv;
        private UILabel _name;
        private ClubApplyItemVo _vo;

        public ClubApplyItemView(GameObject go)
        {
            this._go = go;
            this._icon = NGUITools.FindInChild<UISprite>(this._go, "icon");
            this._name = NGUITools.FindInChild<UILabel>(this._go, "name");
            this._lv = NGUITools.FindInChild<UILabel>(this._go, "lv");
            this._btnAccept = NGUITools.FindInChild<Button>(this._go, "btnAccept");
            this._btnReject = NGUITools.FindInChild<Button>(this._go, "btnReject");
        }

        private void OnClickBtn(GameObject go)
        {
            if ((go != this._btnAccept.gameObject) && (go == this._btnReject.gameObject))
            {
            }
        }

        private void UpdateView()
        {
            if (this._vo == null)
            {
                this._go.SetActive(false);
                this._btnAccept.onClick = this._btnReject.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            }
            this._go.SetActive(true);
            this._btnAccept.onClick = (UIWidgetContainer.VoidDelegate) (this._btnReject.onClick = null);
        }

        public ClubApplyItemVo vo
        {
            get
            {
                return this._vo;
            }
            set
            {
                this._vo = value;
                this.UpdateView();
            }
        }
    }
}

