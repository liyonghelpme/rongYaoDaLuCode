﻿namespace com.game.module.club
{
    using System;

    public class ClubApplyItemVo
    {
        public int applicantId;
        public int generalId;
        public int level;
        public string name;
    }
}

