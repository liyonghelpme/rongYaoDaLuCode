﻿namespace com.game.module.club
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;

    public class ClubApplyView : BaseView<ClubApplyView>
    {
        private Button _btnClose;
        private UIGrid _grid;
        private List<ClubApplyItemView> _list = new List<ClubApplyItemView>();
        private UIScrollView _scroll;

        protected override void Init()
        {
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Club/ClubApplyView.assetbundle";
            }
        }
    }
}

