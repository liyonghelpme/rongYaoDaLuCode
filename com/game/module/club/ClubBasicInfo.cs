﻿namespace com.game.module.club
{
    using System;

    public class ClubBasicInfo
    {
        public int chairmanId;
        public string chairmanName;
        public int clubContribution;
        public string declaration;
        public int level;
        public int myContributionToday;
        public int myContributionTotal;
        public string name;
        public int numFunds;
        public int numMember;
        public int numMemberMax;

        public void Clear()
        {
            this.name = string.Empty;
            this.level = 0;
            this.chairmanName = string.Empty;
            this.declaration = string.Empty;
            this.numMember = 0;
            this.numMemberMax = 0;
            this.numFunds = 0;
            this.clubContribution = 0;
            this.myContributionTotal = 0;
            this.myContributionToday = 0;
        }
    }
}

