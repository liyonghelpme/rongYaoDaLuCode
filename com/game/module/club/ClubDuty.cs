﻿namespace com.game.module.club
{
    using System;

    public class ClubDuty
    {
        public const int ELDER = 1;
        public const int NONE = 0;
        public const int PRESIDENT = 3;
        public const int VP = 2;

        public static bool CanDissolveClub(int duty)
        {
            return (3 == duty);
        }

        public static bool CanExitClub(int duty)
        {
            return (3 != duty);
        }

        public static bool CanKickOut(int duty)
        {
            return (duty >= 3);
        }
    }
}

