﻿namespace com.game.module.club
{
    using System;

    public class ClubEvent
    {
        public const int ClubAppliesUpdate = 8;
        public const int ClubInfoUpdate = 6;
        public const int ClubListUpdate = 1;
        public const int CreateClub = 2;
        public const int DissolveClub = 3;
        public const int EnterClub = 4;
        public const int ExitClub = 5;
        public const int MembersUpdate = 7;
    }
}

