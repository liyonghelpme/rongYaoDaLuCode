﻿namespace com.game.module.club
{
    using com.game.manager;
    using System;
    using UnityEngine;

    public class ClubItemView
    {
        private Button _btnApply;
        private GameObject _go;
        private UISprite _icon;
        private UILabel _lblContribution;
        private UILabel _lblDeclaration;
        private UILabel _lblLimt;
        private UILabel _lblMaster;
        private UILabel _lblMemberCount;
        private UILabel _lblNameLv;
        private ClubItemVo _vo;

        public ClubItemView(GameObject go)
        {
            this._go = go;
            this._icon = NGUITools.FindInChild<UISprite>(this._go, "icon");
            this._lblNameLv = NGUITools.FindInChild<UILabel>(this._go, "name_level");
            this._lblMaster = NGUITools.FindInChild<UILabel>(this._go, "materName");
            this._lblMemberCount = NGUITools.FindInChild<UILabel>(this._go, "memberCount");
            this._lblContribution = NGUITools.FindInChild<UILabel>(this._go, "contribution");
            this._lblDeclaration = NGUITools.FindInChild<UILabel>(this._go, "declaration");
            this._lblLimt = NGUITools.FindInChild<UILabel>(this._go, "condition");
            this._btnApply = NGUITools.FindInChild<Button>(this._go, "btnApply");
        }

        public void Dispose(bool destroy)
        {
            if (null != this._go)
            {
                if (destroy)
                {
                    UnityEngine.Object.Destroy(this._go);
                    this._btnApply.onClick = null;
                    this._go = null;
                }
                else
                {
                    this._go.SetActive(false);
                }
            }
        }

        private void OnClickBtn(GameObject go)
        {
            ClubMode.Instance.ReqEnterClub(this._vo.id);
        }

        public void Update(ClubItemVo vo)
        {
            this._vo = vo;
            bool flag = null == this._vo;
            this._go.SetActive(!flag);
            if (flag)
            {
                this._btnApply.onClick = null;
            }
            else
            {
                string[] param = new string[] { this._vo.name, this._vo.level.ToString() };
                this._lblNameLv.text = LanguageManager.GetWord("Club.NameLevel", param);
                this._lblMaster.text = LanguageManager.GetWord("Club.Master", this._vo.presidentName);
                string[] textArray2 = new string[] { this._vo.numMember.ToString(), this._vo.numMemberMax.ToString() };
                this._lblMemberCount.text = LanguageManager.GetWord("Club.Member", textArray2);
                this._lblContribution.text = LanguageManager.GetWord("Club.Contribution", this._vo.contribution.ToString());
                this._lblDeclaration.text = LanguageManager.GetWord("Club.Declaration", this._vo.declaration);
                this._lblLimt.text = this._vo.joinLimit.ToString();
                this._btnApply.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            }
        }

        public GameObject go
        {
            get
            {
                return this._go;
            }
        }
    }
}

