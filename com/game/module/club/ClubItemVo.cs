﻿namespace com.game.module.club
{
    using System;

    public class ClubItemVo
    {
        public int contribution;
        public string declaration;
        public int id;
        public int joinLimit;
        public int level;
        public string name;
        public int numFunds;
        public int numMember;
        public int numMemberMax;
        public int presidentId;
        public int presidentLevel;
        public string presidentName;
    }
}

