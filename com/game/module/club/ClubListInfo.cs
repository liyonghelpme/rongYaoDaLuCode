﻿namespace com.game.module.club
{
    using System;
    using System.Collections.Generic;

    public class ClubListInfo
    {
        private List<ClubItemVo> _clubList = new List<ClubItemVo>();

        public void Add(ClubItemVo club)
        {
            int index = this.IndexOf(club.id);
            if (index < 0)
            {
                this._clubList.Add(club);
            }
            else
            {
                this._clubList[index] = club;
            }
        }

        public void Clear()
        {
            this._clubList.Clear();
        }

        public ClubItemVo GetClub(int clubId)
        {
            int index = this.IndexOf(clubId);
            return ((index < 0) ? null : this._clubList[index]);
        }

        private int IndexOf(int id)
        {
            for (int i = 0; i < this._clubList.Count; i++)
            {
                if (this._clubList[i].id == id)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Remove(int clubId)
        {
            int index = this.IndexOf(clubId);
            if (index >= 0)
            {
                this._clubList.RemoveAt(index);
            }
        }

        public void Reset()
        {
            this._clubList.Clear();
        }

        public List<ClubItemVo> list
        {
            get
            {
                return this._clubList;
            }
        }
    }
}

