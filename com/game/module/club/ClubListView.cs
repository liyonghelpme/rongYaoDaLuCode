﻿namespace com.game.module.club
{
    using com.game.basic.events;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ClubListView : BaseView<ClubListView>
    {
        private Button _btnClose;
        private Button _btnCreate;
        private Button _btnSearch;
        private UIGrid _grid;
        private UIInput _input;
        private List<ClubItemView> _list = new List<ClubItemView>();
        private UIScrollView _scrollView;

        public override void CancelUpdateHandler()
        {
            this._btnSearch.onClick = this._btnCreate.onClick = (UIWidgetContainer.VoidDelegate) (this._btnClose.onClick = null);
            ClubMode.Instance.Remove(1, new NoticeListener(this.OnClubListUpdate));
            ClubMode.Instance.Remove(2, new NoticeListener(this.OnEnterClub));
            ClubMode.Instance.Remove(4, new NoticeListener(this.OnEnterClub));
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdateView(ClubMode.Instance.clubListInfo.list);
        }

        protected override void HandleBeforeCloseView()
        {
            this._list[0].Dispose(false);
            for (int i = this._list.Count - 1; i >= 1; i--)
            {
                this._list[i].Dispose(true);
                this._list.RemoveAt(i);
            }
        }

        protected override void Init()
        {
            this._btnSearch = base.FindInChild<Button>("Layer2/btnSearch");
            this._btnCreate = base.FindInChild<Button>("Layer2/btnCreate");
            this._btnClose = base.FindInChild<Button>("Layer2/btnClose");
            this._input = base.FindInChild<UIInput>("Layer2/inputText");
            this._scrollView = base.FindInChild<UIScrollView>("Layer1");
            this._grid = base.FindInChild<UIGrid>("Layer1/Grid");
            ClubItemView item = new ClubItemView(base.FindChild("Layer1/Grid/item"));
            item.Update(null);
            this._list.Add(item);
        }

        private void OnClickBtn(GameObject go)
        {
            if (go.name == this._btnCreate.name)
            {
                Singleton<CreateClubView>.Instance.OpenView();
            }
            else if ((go.name != this._btnSearch.name) && (go.name == this._btnClose.name))
            {
                this.CloseView();
            }
        }

        private void OnClubListUpdate(int type, int v1, int v2, object data)
        {
            this.UpdateView(ClubMode.Instance.clubListInfo.list);
        }

        private void OnEnterClub(int type, int v1, int v2, object data)
        {
            this.CloseView();
            Singleton<ClubMainView>.Instance.OpenView();
        }

        public override void RegisterUpdateHandler()
        {
            this._btnSearch.onClick = this._btnCreate.onClick = this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            ClubMode.Instance.Add(1, new NoticeListener(this.OnClubListUpdate));
            ClubMode.Instance.Add(2, new NoticeListener(this.OnEnterClub));
            ClubMode.Instance.Add(4, new NoticeListener(this.OnEnterClub));
        }

        private void UpdateView(List<ClubItemVo> clubList)
        {
            while (this._list.Count < clubList.Count)
            {
                this._list.Add(new ClubItemView(NGUITools.AddChild(this._grid.gameObject, this._list[0].go)));
            }
            int num = 0;
            num = 0;
            while (num < clubList.Count)
            {
                this._list[num].Update(clubList[num]);
                num++;
            }
            while (num < this._list.Count)
            {
                this._list[num].Update(null);
                num++;
            }
            this._scrollView.ResetPosition();
            this._grid.Reposition();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Club/ClubListView.assetbundle";
            }
        }
    }
}

