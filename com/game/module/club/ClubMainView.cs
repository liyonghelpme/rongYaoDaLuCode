﻿namespace com.game.module.club
{
    using com.game.basic.events;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class ClubMainView : BaseView<ClubMainView>
    {
        private Button _btnClose;
        private Button _btnMembers;
        private UILabel _lblCount;
        private UILabel _lblId;
        private UILabel _lblName;

        public override void CancelUpdateHandler()
        {
            this._btnMembers.onClick = (UIWidgetContainer.VoidDelegate) (this._btnClose.onClick = null);
            ClubMode.Instance.Remove(3, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Remove(5, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Remove(6, new NoticeListener(this.OnClubInfoUpdate));
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdateView();
        }

        protected override void HandleBeforeCloseView()
        {
        }

        protected override void Init()
        {
            this._lblName = base.FindInChild<UILabel>("Layer2/lblNameLv");
            this._lblId = base.FindInChild<UILabel>("Layer2/lblID");
            this._lblCount = base.FindInChild<UILabel>("Layer2/lblCount");
            this._btnMembers = base.FindInChild<Button>("Layer2/btnMembers");
            this._btnClose = base.FindInChild<Button>("Layer2/btnClose");
        }

        private void OnClickBtn(GameObject go)
        {
            if (go == this._btnMembers.gameObject)
            {
                this.CloseView();
                Singleton<ClubMemberView>.Instance.OpenView();
            }
            else if (go == this._btnClose.gameObject)
            {
                this.CloseView();
            }
        }

        private void OnClubInfoUpdate(int type, int v1, int v2, object body)
        {
            this.UpdateView();
        }

        private void OnExitClub(int type, int v1, int v2, object body)
        {
            this.CloseView();
            Singleton<ClubListView>.Instance.OpenView();
        }

        public override void OpenView()
        {
            if (ClubMode.Instance.clubId <= 0)
            {
                Singleton<ClubListView>.Instance.OpenView();
            }
            else
            {
                base.OpenView();
            }
        }

        public override void RegisterUpdateHandler()
        {
            this._btnMembers.onClick = this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            ClubMode.Instance.Add(3, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Add(5, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Add(6, new NoticeListener(this.OnClubInfoUpdate));
        }

        private void UpdateView()
        {
            this._lblName.text = ClubMode.Instance.basicInfo.name;
            this._lblId.text = ClubMode.Instance.basicInfo.name;
            this._lblCount.text = ClubMode.Instance.basicInfo.numMember + "/" + ClubMode.Instance.basicInfo.numMemberMax;
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Club/ClubMainView.assetbundle";
            }
        }
    }
}

