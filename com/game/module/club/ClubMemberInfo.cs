﻿namespace com.game.module.club
{
    using System;
    using System.Collections.Generic;

    public class ClubMemberInfo
    {
        private List<ClubMemberItemVo> _list = new List<ClubMemberItemVo>();

        public void Add(ClubMemberItemVo member)
        {
            int index = this.IndexOf(member.id);
            if (index < 0)
            {
                this._list.Add(member);
            }
            else
            {
                this._list[index] = member;
            }
        }

        public void Clear()
        {
            this._list.Clear();
        }

        public ClubMemberItemVo GetMember(int memberId)
        {
            int index = this.IndexOf(memberId);
            return ((index < 0) ? null : this._list[index]);
        }

        private int IndexOf(int memberId)
        {
            for (int i = 0; i < this._list.Count; i++)
            {
                if (this._list[i].id == memberId)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Remove(int memberId)
        {
            int index = this.IndexOf(memberId);
            if (index >= 0)
            {
                this._list.RemoveAt(index);
            }
        }

        public List<ClubMemberItemVo> list
        {
            get
            {
                return this._list;
            }
        }
    }
}

