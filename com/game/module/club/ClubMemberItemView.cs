﻿namespace com.game.module.club
{
    using System;
    using UnityEngine;

    public class ClubMemberItemView
    {
        private Button _btnKo;
        private UILabel _contribution;
        private UILabel _duty;
        private GameObject _go;
        private UILabel _lv;
        private UILabel _name;
        private ClubMemberItemVo _vo;

        public ClubMemberItemView(GameObject go)
        {
            this._go = go;
            this._name = NGUITools.FindInChild<UILabel>(this._go, "name");
            this._lv = NGUITools.FindInChild<UILabel>(this._go, "level");
            this._duty = NGUITools.FindInChild<UILabel>(this._go, "duty");
            this._contribution = NGUITools.FindInChild<UILabel>(this._go, "contribution");
            this._btnKo = NGUITools.FindInChild<Button>(this._go, "btnKo");
            this.UpdateView();
        }

        public void Clear(bool destroy)
        {
            this._vo = null;
            if (null != this._go)
            {
                if (destroy)
                {
                    UnityEngine.Object.Destroy(this._go);
                    this._go = null;
                }
                else
                {
                    this._go.SetActive(false);
                }
            }
        }

        private void OnClickBtn(GameObject go)
        {
            ClubMode.Instance.ReqKickOut(this._vo.id);
        }

        private void UpdateView()
        {
            if (this._vo == null)
            {
                this._go.SetActive(false);
                this._btnKo.onClick = null;
            }
            else
            {
                this._go.SetActive(true);
                this._btnKo.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
                this._name.text = this._vo.name;
                this._lv.text = this._vo.level.ToString();
                this._duty.text = this._vo.duty.ToString();
                object[] objArray1 = new object[] { "total:", this._vo.contributionTotal, "\ntoday:", this._vo.contributionToday };
                this._contribution.text = string.Concat(objArray1);
            }
        }

        public ClubMemberItemVo vo
        {
            get
            {
                return this._vo;
            }
            set
            {
                this._vo = value;
                this.UpdateView();
            }
        }
    }
}

