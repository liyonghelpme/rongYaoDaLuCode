﻿namespace com.game.module.club
{
    using System;

    public class ClubMemberItemVo
    {
        public int contributionToday;
        public int contributionTotal;
        public int duty;
        public int generalId;
        public int id;
        public bool isOnline;
        public int lastOfflienTime;
        public int level;
        public string name;
    }
}

