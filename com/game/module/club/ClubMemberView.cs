﻿namespace com.game.module.club
{
    using com.game.basic.events;
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ClubMemberView : BaseView<ClubMemberView>
    {
        private List<Button> _btns;
        private UIGrid _grid;
        private GameObject _itemGo;
        private List<ClubMemberItemView> _items;
        private UIScrollView _scroll;

        public override void CancelUpdateHandler()
        {
            ClubMode.Instance.Remove(3, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Remove(5, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Remove(7, new NoticeListener(this.OnMembersUpdate));
        }

        protected override void HandleAfterOpenView()
        {
            this.UpdateView(ClubMode.Instance.memberInfo.list);
        }

        protected override void HandleBeforeCloseView()
        {
            if (this._items.Count > 0)
            {
                this._items[0].Clear(false);
                for (int i = this._items.Count - 1; i >= 1; i--)
                {
                    this._items[i].Clear(true);
                    this._items.RemoveAt(i);
                }
            }
        }

        protected override void Init()
        {
            this._btns = new List<Button>();
            this._items = new List<ClubMemberItemView>();
            this._scroll = base.FindInChild<UIScrollView>("Layer1");
            this._grid = base.FindInChild<UIGrid>("Layer1/Grid");
            this._itemGo = base.FindChild("Layer1/Grid/item");
            for (int i = 0; i < 5; i++)
            {
                this._btns.Add(base.FindInChild<Button>("Layer2/btn" + i));
            }
            ClubMemberItemView item = new ClubMemberItemView(this._itemGo);
            this._items.Add(item);
        }

        private void OnClickBtn(GameObject go)
        {
            <OnClickBtn>c__AnonStoreyD6 yd = new <OnClickBtn>c__AnonStoreyD6 {
                go = go
            };
            switch (this._btns.FindIndex(new Predicate<Button>(yd.<>m__66)))
            {
                case 0:
                    this.CloseView();
                    break;

                case 1:
                    ClubMode.Instance.ReqDissolveClub();
                    break;

                case 2:
                    ClubMode.Instance.ReqExitClub();
                    break;

                case 4:
                    this.CloseView();
                    Singleton<ClubListView>.Instance.OpenView();
                    break;

                case 5:
                    this.CloseView();
                    break;
            }
        }

        private void OnExitClub(int type, int v1, int v2, object data)
        {
            this.CloseView();
            Singleton<ClubListView>.Instance.OpenView();
        }

        private void OnMembersUpdate(int type, int v1, int v2, object data)
        {
            this.UpdateView(ClubMode.Instance.memberInfo.list);
        }

        public override void RegisterUpdateHandler()
        {
            foreach (Button button in this._btns)
            {
                button.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
            }
            ClubMode.Instance.Add(3, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Add(5, new NoticeListener(this.OnExitClub));
            ClubMode.Instance.Add(7, new NoticeListener(this.OnMembersUpdate));
        }

        private void UpdateView(List<ClubMemberItemVo> list)
        {
            while (this._items.Count < list.Count)
            {
                this._items.Add(new ClubMemberItemView(NGUITools.AddChild(this._grid.gameObject, this._itemGo)));
            }
            int num = 0;
            num = 0;
            while (num < list.Count)
            {
                this._items[num].vo = list[num];
                num++;
            }
            while (num < this._items.Count)
            {
                this._items[num].vo = null;
                num++;
            }
            bool flag = ClubMode.Instance.basicInfo.chairmanId == ((int) MeVo.instance.Id);
            this._scroll.ResetPosition();
            this._grid.Reposition();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Club/ClubMemberView.assetbundle";
            }
        }

        [CompilerGenerated]
        private sealed class <OnClickBtn>c__AnonStoreyD6
        {
            internal GameObject go;

            internal bool <>m__66(Button t)
            {
                return (t.gameObject == this.go);
            }
        }
    }
}

