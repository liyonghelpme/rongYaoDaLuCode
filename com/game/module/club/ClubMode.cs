﻿namespace com.game.module.club
{
    using com.game.basic.events;
    using com.game.Public.Message;
    using com.game.vo;
    using System;
    using System.Runtime.CompilerServices;

    public class ClubMode : Notifier
    {
        private static ClubMode _inst;
        public ClubApplyInfo applyList = new ClubApplyInfo();
        public ClubBasicInfo basicInfo = new ClubBasicInfo();
        public static int CLUB;
        public ClubListInfo clubListInfo = new ClubListInfo();
        public ClubMemberInfo memberInfo = new ClubMemberInfo();

        private ClubMode()
        {
        }

        public void ReqAnswerApply(int id, bool letIn)
        {
            if ((this.applyList.GetApply(id) != null) && !letIn)
            {
            }
        }

        public void ReqCreateClub(string clubName)
        {
            if (Instance.clubId > 0)
            {
                MessageManager.ShowLang("Club.AlreadyInClub");
            }
            else
            {
                ClubItemVo club = new ClubItemVo();
                int num = ++CLUB;
                this.clubId = num;
                club.id = num;
                club.name = clubName;
                club.level = 0;
                club.presidentId = (int) MeVo.instance.Id;
                club.presidentName = MeVo.instance.Name;
                club.presidentLevel = MeVo.instance.Level;
                club.declaration = "我以公会为荣";
                club.numMember = 1;
                club.numMemberMax = 0x7fffffff;
                this.clubListInfo.Add(club);
                this.basicInfo.Clear();
                this.basicInfo.name = club.name;
                this.basicInfo.level = club.level;
                this.basicInfo.chairmanId = club.presidentId;
                this.basicInfo.chairmanName = club.presidentName;
                this.basicInfo.declaration = club.declaration;
                this.basicInfo.numMember = club.numMember;
                this.basicInfo.numMemberMax = club.numMemberMax;
                this.memberInfo.Clear();
                ClubMemberItemVo member = new ClubMemberItemVo {
                    id = (int) MeVo.instance.Id,
                    name = MeVo.instance.Name,
                    level = MeVo.instance.Level,
                    duty = 3,
                    isOnline = true
                };
                this.memberInfo.Add(member);
                this.RespCreateClub();
            }
        }

        public void ReqDissolveClub()
        {
            if (this.clubId <= 0)
            {
                MessageManager.ShowLang("Club.NotInClub");
            }
            else if (((int) MeVo.instance.Id) != this.basicInfo.chairmanId)
            {
                MessageManager.ShowLang("Club.DutyNotAllowedToDissolve");
            }
            else
            {
                ClubItemVo club = this.clubListInfo.GetClub(this.clubId);
                if (club != null)
                {
                    if (((ulong) club.presidentId) == MeVo.instance.Id)
                    {
                        this.clubListInfo.Remove(this.clubId);
                        this.RespClubListUpdate();
                    }
                    this.memberInfo.Clear();
                    this.basicInfo.Clear();
                    this.clubId = 0;
                    this.RespDissolveClub();
                }
            }
        }

        public void ReqEnterClub(int clubId)
        {
            if (this.clubId > 0)
            {
                MessageManager.ShowLang("Club.AlreadyInClub");
            }
            else
            {
                ClubItemVo club = this.clubListInfo.GetClub(clubId);
                if (club != null)
                {
                    if (club.numMember >= club.numMemberMax)
                    {
                        MessageManager.ShowLang("Club.ClubFull");
                    }
                    else
                    {
                        club.numMember++;
                        this.basicInfo.name = club.name;
                        this.basicInfo.level = club.level;
                        this.basicInfo.chairmanName = club.presidentName;
                        this.basicInfo.declaration = club.declaration;
                        this.basicInfo.numMember = club.numMember;
                        this.basicInfo.numMemberMax = club.numMemberMax;
                        this.memberInfo.Clear();
                        ClubMemberItemVo member = new ClubMemberItemVo {
                            id = (int) MeVo.instance.Id,
                            name = MeVo.instance.Name,
                            level = MeVo.instance.Level,
                            duty = 2,
                            isOnline = true
                        };
                        this.memberInfo.Add(member);
                        this.clubId = clubId;
                        this.RespEnterClub();
                    }
                }
            }
        }

        public void ReqExitClub()
        {
            if (this.clubId <= 0)
            {
                MessageManager.ShowLang("Club.NotInClub");
            }
            else if (((int) MeVo.instance.Id) != this.basicInfo.chairmanId)
            {
                ClubItemVo club = this.clubListInfo.GetClub(this.clubId);
                if (club != null)
                {
                    if (((ulong) club.presidentId) == MeVo.instance.Id)
                    {
                        this.clubListInfo.Remove(this.clubId);
                        this.RespClubListUpdate();
                    }
                    this.memberInfo.Clear();
                    this.basicInfo.Clear();
                    this.clubId = 0;
                    this.RespExitClub();
                    this.RespDissolveClub();
                }
            }
        }

        public void ReqKickOut(int memberId)
        {
            if (this.clubId <= 0)
            {
                MessageManager.ShowLang("Club.NotInClub");
            }
            else if ((memberId != this.basicInfo.chairmanId) && (this.memberInfo.GetMember(memberId) != null))
            {
                this.memberInfo.Remove(memberId);
                this.RespKickOut();
            }
        }

        public void RespClubBasicInfo()
        {
            this.Notify(6, 0, 0, null);
        }

        public void RespClubListUpdate()
        {
            this.Notify(1, 0, 0, null);
        }

        public void RespCreateClub()
        {
            this.Notify(2, 0, 0, null);
        }

        public void RespDissolveClub()
        {
            this.Notify(3, 0, 0, null);
        }

        public void RespEnterClub()
        {
            this.Notify(4, 0, 0, null);
        }

        public void RespExitClub()
        {
            this.Notify(5, 0, 0, null);
        }

        public void RespKickOut()
        {
            this.Notify(7, 0, 0, null);
        }

        public int clubId { get; set; }

        public static ClubMode Instance
        {
            get
            {
                if (_inst == null)
                {
                    _inst = new ClubMode();
                }
                return _inst;
            }
        }
    }
}

