﻿namespace com.game.module.club
{
    using com.game.module.core;
    using com.game.Public.Message;
    using System;
    using UnityEngine;

    public class CreateClubView : BaseView<CreateClubView>
    {
        private Button _btnCancel;
        private Button _btnOk;
        private UIInput _input;
        private UILabel _lblCost;

        public override void CancelUpdateHandler()
        {
            this._btnOk.onClick = (UIWidgetContainer.VoidDelegate) (this._btnCancel.onClick = null);
        }

        protected override void HandleAfterOpenView()
        {
        }

        protected override void HandleBeforeCloseView()
        {
        }

        protected override void Init()
        {
            this._input = base.FindInChild<UIInput>("Layer2/input");
            this._btnOk = base.FindInChild<Button>("Layer2/btnOk");
            this._btnCancel = base.FindInChild<Button>("Layer2/btnCancel");
            this._lblCost = base.FindInChild<UILabel>("Layer2/stoneLabel");
        }

        private void OnClickBtn(GameObject go)
        {
            if (go.name == this._btnCancel.name)
            {
                this.CloseView();
            }
            else if (string.IsNullOrEmpty(this._input.value))
            {
                MessageManager.ShowLang("Club.InputClubNameFirst");
            }
            else
            {
                ClubMode.Instance.ReqCreateClub(this._input.value);
                this.CloseView();
            }
        }

        public override void RegisterUpdateHandler()
        {
            this._btnOk.onClick = this._btnCancel.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBtn);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Club/CreateClubView.assetbundle";
            }
        }
    }
}

