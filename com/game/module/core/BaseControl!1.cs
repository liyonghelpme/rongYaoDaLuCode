﻿namespace com.game.module.core
{
    using System;

    public class BaseControl<T> : Singleton<T> where T: new()
    {
        protected BaseControl()
        {
            this.NetListener();
        }

        protected virtual void NetListener()
        {
        }
    }
}

