﻿namespace com.game.module.core
{
    using System;
    using System.Runtime.InteropServices;

    public class BaseMode<T> : Singleton<T> where T: new()
    {
        private DataUpateHandler handlList;
        private DataUpateHandlerWithParam handlListWithParam;

        public event DataUpateHandler dataUpdated
        {
            add
            {
                this.handlList = (DataUpateHandler) Delegate.Remove(this.handlList, value);
                this.handlList = (DataUpateHandler) Delegate.Combine(this.handlList, value);
            }
            remove
            {
                this.handlList = (DataUpateHandler) Delegate.Remove(this.handlList, value);
            }
        }

        public event DataUpateHandlerWithParam dataUpdatedWithParam
        {
            add
            {
                this.handlListWithParam = (DataUpateHandlerWithParam) Delegate.Remove(this.handlListWithParam, value);
                this.handlListWithParam = (DataUpateHandlerWithParam) Delegate.Combine(this.handlListWithParam, value);
            }
            remove
            {
                this.handlListWithParam = (DataUpateHandlerWithParam) Delegate.Remove(this.handlListWithParam, value);
            }
        }

        public void DataUpdate(int code = 0)
        {
            DataUpateHandler handlList = this.handlList;
            if (handlList != null)
            {
                foreach (DataUpateHandler handler2 in handlList.GetInvocationList())
                {
                    handler2(this, code);
                }
            }
        }

        public void DataUpdateWithParam(int code = 0, object param = null)
        {
            DataUpateHandlerWithParam handlListWithParam = this.handlListWithParam;
            if (handlListWithParam != null)
            {
                foreach (DataUpateHandlerWithParam param3 in handlListWithParam.GetInvocationList())
                {
                    param3(this, code, param);
                }
            }
        }

        public virtual bool ShowTips
        {
            get
            {
                return false;
            }
        }
    }
}

