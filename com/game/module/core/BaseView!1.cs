﻿namespace com.game.module.core
{
    using com.game.basic;
    using com.game.manager;
    using com.game.module;
    using com.game.sound;
    using com.game.utils;
    using com.u3d.bases.loader;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BaseView<T> : Singleton<T>, IView where T: new()
    {
        public OpenViewGuideDelegate AfterOpenGuideDelegate;
        private AssetBundleLoader assetLoader;
        protected IPlay closeTween;
        protected bool firstOpen;
        private bool isInit;
        public bool IsOpened;
        private bool isSecondOpen;
        private PanelType lastOpenedPanelType;
        private GameObject mGameObject;
        private bool openState;
        public OpenViewCallBack OpenViewCallback;
        public com.game.module.core.RecoverGuideDelegate RecoverGuideDelegate;
        protected TweenPlay showTween;

        public BaseView()
        {
            this.firstOpen = true;
        }

        public virtual void CancelUpdateHandler()
        {
        }

        public virtual void CloseView()
        {
            if (this.gameObject != null)
            {
                this.openState = false;
                if (this.RecoverGuideDelegate != null)
                {
                    this.RecoverGuideDelegate();
                }
                this.HandleBeforeCloseView();
                this.CancelUpdateHandler();
                ViewManager.UnRegister(this);
                GlobalAPI.facade.Notify(20, 0, 0, this);
                if (this.closeTween != null)
                {
                    EventDelegate.Add(this.closeTween.OnEnd, new EventDelegate.Callback(this.CloseViewHelp));
                    this.closeTween.Begin();
                }
                else if (this.showTween != null)
                {
                    EventDelegate.Add(this.showTween.onFinished, new EventDelegate.Callback(this.CloseViewHelp));
                    this.showTween.PlayReverse();
                }
                else
                {
                    this.CloseViewHelp();
                }
                if (this.playClosedSound)
                {
                    SoundMgr.Instance.PlayUIAudio("3005", 0f);
                }
            }
            this.IsOpened = false;
        }

        private void CloseViewHelp()
        {
            if (!this.openState)
            {
                if (this.gameObject != null)
                {
                    this.gameObject.SetActive(false);
                }
                if (this.isDestroy)
                {
                    this.Destroy();
                    this.DestoryHelp();
                    this.isInit = false;
                }
                if (this.isUnloadDelay && (this.assetLoader != null))
                {
                    this.assetLoader.UnloadAssetBundle(true);
                    this.assetLoader = null;
                }
                if (this.showTween != null)
                {
                    EventDelegate.Remove(this.showTween.onFinished, new EventDelegate.Callback(this.CloseViewHelp));
                }
            }
        }

        public virtual void DataUpdated(object sender, int code)
        {
        }

        private void DestoryHelp()
        {
            if (this.gameObject != null)
            {
                UnityEngine.Object.Destroy(this.gameObject);
                this.isInit = false;
                Resources.UnloadUnusedAssets();
            }
        }

        public virtual void Destroy()
        {
        }

        public GameObject FindChild(string path)
        {
            if (this.transform.Find(path) != null)
            {
                return this.transform.Find(path).gameObject;
            }
            return null;
        }

        public T FindInChild<T>(string path = null) where T: Component
        {
            if (string.IsNullOrEmpty(path))
            {
                return this.gameObject.GetComponent<T>();
            }
            return NGUITools.FindInChild<T>(this.gameObject, path);
        }

        protected virtual void HandleAfterOpenView()
        {
        }

        protected virtual void HandleBeforeCloseView()
        {
        }

        protected virtual void Init()
        {
        }

        protected void LoadChildComplete()
        {
            this.gameObject.SetActive(this.firstOpen);
            if (this.waiting && (base.GetType() != Singleton<WaitingView>.Instance.GetType()))
            {
                Singleton<WaitingView>.Instance.CloseView();
            }
            if (this.firstOpen)
            {
                this.OpenViewHelp();
            }
        }

        private void LoadView()
        {
            if (this.isInit)
            {
                this.isSecondOpen = true;
            }
            else
            {
                this.isInit = true;
                if (this.waiting && (base.GetType() != Singleton<WaitingView>.Instance.GetType()))
                {
                    Singleton<WaitingView>.Instance.OpenView();
                }
                if (this.isAssetbundle)
                {
                    Singleton<WaitingView>.Instance.loadProcess = AssetManager.Instance.LoadAsset<GameObject>(this.url, new LoadAssetFinish<GameObject>(this.LoadViewCallBack), this.viewName, false, this.isAsyncLoad);
                    if (this.isUnloadDelay)
                    {
                        this.assetLoader = Singleton<WaitingView>.Instance.loadProcess;
                        this.assetLoader.delayUnload = true;
                    }
                }
                else
                {
                    this.LoadViewCallBack(ResMgr.instance.load(this.url.Replace(".assetbundle", string.Empty), null) as GameObject);
                }
            }
        }

        protected void LoadViewCallBack(GameObject prefab)
        {
            switch (this.viewType)
            {
                case ViewType.BattleView:
                    this.gameObject = NGUITools.AddChild(com.game.module.ViewTree.battle, prefab);
                    break;

                case ViewType.CityView:
                    this.gameObject = NGUITools.AddChild(com.game.module.ViewTree.city, prefab);
                    break;

                default:
                    this.gameObject = NGUITools.AddChild(com.game.module.ViewTree.go, prefab);
                    break;
            }
            if ((this.layerType == ViewLayer.HighLayer) || (this.layerType == ViewLayer.TopLayer))
            {
                NGUITools.SetLayer(this.gameObject, LayerMask.NameToLayer("Viewport"));
            }
            this.gameObject.SetActive(false);
            this.Init();
            if (this.isSecondOpen)
            {
                this.firstOpen = true;
            }
            if (!this.IsLoadChild)
            {
                this.LoadChildComplete();
            }
        }

        public virtual void OpenView()
        {
            this.IsOpened = true;
            if (this.gameObject == null)
            {
                this.LoadView();
            }
            else
            {
                if (!this.gameObject.activeInHierarchy)
                {
                    this.gameObject.SetActive(true);
                }
                this.OpenViewHelp();
            }
        }

        public virtual void OpenView(PanelType type)
        {
            this.lastOpenedPanelType = type;
            this.OpenView();
        }

        private void OpenViewHelp()
        {
            this.openState = true;
            ViewManager.Register(this);
            this.RegisterUpdateHandler();
            this.HandleAfterOpenView();
            GlobalAPI.facade.Notify(0x13, 0, 0, this);
            if (this.showTween != null)
            {
                this.showTween.PlayForward();
            }
            if (this.AfterOpenGuideDelegate != null)
            {
                this.AfterOpenGuideDelegate();
                this.AfterOpenGuideDelegate = null;
            }
            if (this.OpenViewCallback != null)
            {
                this.OpenViewCallback();
                this.OpenViewCallback = null;
            }
        }

        public virtual void RegisterUpdateHandler()
        {
        }

        protected void ReturnToToggerPanel()
        {
            IView panelByPannelType = PanelTypeUtils.GetPanelByPannelType(this.lastOpenedPanelType);
            if (panelByPannelType != null)
            {
                panelByPannelType.OpenView();
                this.lastOpenedPanelType = PanelType.NONE;
            }
        }

        public virtual void TweenCallback()
        {
        }

        public virtual void TweenCallbackParams(object oncompleteparams)
        {
        }

        public virtual void Update()
        {
        }

        public GameObject gameObject
        {
            get
            {
                return this.mGameObject;
            }
            set
            {
                if ((this.mGameObject == null) && (value != null))
                {
                    this.mGameObject = value;
                    this.transform = this.mGameObject.transform;
                    if (!this.isInit)
                    {
                        this.Init();
                        this.isInit = true;
                    }
                }
            }
        }

        public virtual bool isAssetbundle
        {
            get
            {
                return true;
            }
        }

        public virtual bool isAsyncLoad
        {
            get
            {
                return true;
            }
        }

        public virtual bool isDestroy
        {
            get
            {
                return false;
            }
        }

        public virtual bool IsFullUI
        {
            get
            {
                return false;
            }
        }

        public virtual bool IsLoadChild
        {
            get
            {
                return false;
            }
        }

        public virtual bool isUnloadDelay
        {
            get
            {
                return false;
            }
        }

        public PanelType LastOpenedPanelType
        {
            get
            {
                return this.lastOpenedPanelType;
            }
        }

        public virtual ViewLayer layerType
        {
            get
            {
                return ViewLayer.BaseLayer;
            }
        }

        public virtual PanelType panelType
        {
            get
            {
                return PanelType.NONE;
            }
        }

        public virtual bool playClosedSound
        {
            get
            {
                return true;
            }
        }

        public Transform transform { get; private set; }

        public virtual string url
        {
            get
            {
                return null;
            }
        }

        public virtual string viewName
        {
            get
            {
                return null;
            }
        }

        public virtual ViewType viewType
        {
            get
            {
                return ViewType.NormalView;
            }
        }

        public virtual bool waiting
        {
            get
            {
                return true;
            }
            set
            {
            }
        }
    }
}

