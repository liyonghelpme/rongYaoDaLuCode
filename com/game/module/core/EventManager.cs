﻿namespace com.game.module.core
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using UnityEngine;

    public static class EventManager
    {
        private static Dictionary<int, EventListener> _eventList = new Dictionary<int, EventListener>();
        private static List<int> _eventQueue = new List<int>();
        private static List<object> _paramQueue = new List<object>();
        private static string LAG_KEY = "event_manager_update()";
        private static string TMP_KEY = string.Empty;

        public static string DumpListeners()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("EventManager.DumpListeners\n", new object[0]);
            foreach (KeyValuePair<int, EventListener> pair in _eventList)
            {
                builder.AppendFormat("  event: {0}, count: {1}\n", pair.Key, pair.Value.listeners.Count);
                foreach (EventProcessInterface interface2 in pair.Value.listeners)
                {
                    builder.AppendFormat("    {0}\n", interface2);
                }
            }
            return builder.ToString();
        }

        public static int GetListenersCount()
        {
            int num = 0;
            foreach (KeyValuePair<int, EventListener> pair in _eventList)
            {
                num += pair.Value.listeners.Count;
            }
            return num;
        }

        public static void Init()
        {
            _eventList = new Dictionary<int, EventListener>();
            _eventQueue = new List<int>();
        }

        public static void PostCombineEvent(int evt, object param)
        {
            if (_eventList.ContainsKey(evt))
            {
                int index = _eventQueue.IndexOf(evt);
                if ((index < 0) || (_paramQueue[index] != param))
                {
                    _eventQueue.Add(evt);
                    _paramQueue.Add(param);
                }
            }
        }

        public static void PostEvent(int evt)
        {
            PostEvent(evt, null);
        }

        public static void PostEvent(int evt, object param)
        {
            if (_eventList.ContainsKey(evt))
            {
                _eventQueue.Add(evt);
                _paramQueue.Add(param);
            }
        }

        public static void RegisterEvent(int evt, EventProcessInterface processor)
        {
            EventListener listener = null;
            if (!_eventList.TryGetValue(evt, out listener))
            {
                listener = new EventListener();
                _eventList.Add(evt, listener);
            }
            if (listener.listeners.Contains(processor))
            {
                Debug.LogError("事件重复添加!");
            }
            else
            {
                listener.listeners.Add(processor);
                IEventUnregisterable unregisterable = processor as IEventUnregisterable;
                if (unregisterable != null)
                {
                    unregisterable.HasRegisterEvent = true;
                }
            }
        }

        public static void SendEvent(int evt)
        {
            SendEvent(evt, null);
        }

        public static void SendEvent(int evt, object param)
        {
            if (_eventList.ContainsKey(evt))
            {
                foreach (EventProcessInterface interface2 in _eventList[evt].listeners.ToArray())
                {
                    interface2.ProcessEvent(evt, param);
                }
            }
        }

        public static void UnRegisterAll(IEventUnregisterable processor)
        {
            if (processor.HasRegisterEvent)
            {
                foreach (KeyValuePair<int, EventListener> pair in _eventList)
                {
                    List<EventProcessInterface> listeners = pair.Value.listeners;
                    if (listeners.Contains(processor))
                    {
                        List<EventProcessInterface> list2 = new List<EventProcessInterface>();
                        foreach (EventProcessInterface interface2 in listeners)
                        {
                            if (interface2 != processor)
                            {
                                list2.Add(interface2);
                            }
                        }
                        pair.Value.listeners = list2;
                    }
                }
            }
        }

        public static void UnRegisterEvent(int evt, EventProcessInterface processor)
        {
            EventListener listener = null;
            if (_eventList.TryGetValue(evt, out listener))
            {
                listener.listeners.Remove(processor);
            }
        }

        public static void Update()
        {
            if (_eventQueue.Count != 0)
            {
                List<int> list = _eventQueue;
                List<object> list2 = _paramQueue;
                _eventQueue = new List<int>();
                _paramQueue = new List<object>();
                for (int i = 0; i < list.Count; i++)
                {
                    int evt = list[i];
                    object param = list2[i];
                    foreach (EventProcessInterface interface2 in _eventList[evt].listeners.ToArray())
                    {
                        try
                        {
                            interface2.ProcessEvent(evt, param);
                        }
                        catch (Exception exception)
                        {
                            string str = string.Empty;
                            if (exception.InnerException != null)
                            {
                                str = exception.InnerException.Message + "\n" + exception.InnerException.StackTrace;
                            }
                            else
                            {
                                str = exception.Message + "\n" + exception.StackTrace;
                            }
                        }
                    }
                }
            }
        }

        private class EventListener
        {
            public List<EventManager.EventProcessInterface> listeners = new List<EventManager.EventProcessInterface>();
        }

        public interface EventProcessInterface
        {
            void ProcessEvent(int evt, object param);
        }

        public interface IEventUnregisterable : EventManager.EventProcessInterface
        {
            bool HasRegisterEvent { get; set; }
        }
    }
}

