﻿namespace com.game.module.core
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface IView
    {
        void CancelUpdateHandler();
        void CloseView();
        void DataUpdated(object sender, int code);
        void OpenView();
        void RegisterUpdateHandler();
        void TweenCallback();
        void TweenCallbackParams(object oncompleteparams = null);
        void Update();

        GameObject gameObject { get; set; }

        bool IsFullUI { get; }

        ViewLayer layerType { get; }

        Transform transform { get; }

        ViewType viewType { get; }
    }
}

