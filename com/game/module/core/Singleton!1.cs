﻿namespace com.game.module.core
{
    using System;

    public class Singleton<T> where T: new()
    {
        private static T instance;

        static Singleton()
        {
            Singleton<T>.instance = (default(T) != null) ? default(T) : Activator.CreateInstance<T>();
        }

        protected Singleton()
        {
        }

        public static T Instance
        {
            get
            {
                return Singleton<T>.instance;
            }
        }
    }
}

