﻿namespace com.game.module.core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class Task : IPlay
    {
        public List<EventDelegate> onFinish = new List<EventDelegate>();
        private TaskManager.TaskState task;

        public event FinishedHandler Finished;

        public Task(IEnumerator c, bool autoStart = true)
        {
            this.task = TaskManager.CreateTask(c);
            this.task.Finished += new TaskManager.TaskState.FinishedHandler(this.TaskFinished);
            if (autoStart)
            {
                this.Start();
            }
        }

        public void Begin()
        {
            this.task.Start();
        }

        public void Pause()
        {
            this.task.Pause();
        }

        public void Start()
        {
            this.task.Start();
        }

        public void Stop()
        {
            this.task.Stop();
        }

        private void TaskFinished(bool manual)
        {
            FinishedHandler finished = this.Finished;
            if (finished != null)
            {
                finished(manual);
            }
            EventDelegate.Execute(this.onFinish);
        }

        public void Unpause()
        {
            this.task.Unpause();
        }

        public List<EventDelegate> OnEnd
        {
            get
            {
                return this.onFinish;
            }
        }

        public bool Paused
        {
            get
            {
                return this.task.Paused;
            }
        }

        public Coroutine Routine
        {
            get
            {
                return this.task.Routine;
            }
        }

        public bool Running
        {
            get
            {
                return this.task.Running;
            }
        }

        public delegate void FinishedHandler(bool manual);
    }
}

