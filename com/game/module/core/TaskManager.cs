﻿namespace com.game.module.core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class TaskManager : MonoBehaviour
    {
        private static TaskManager singleton;

        public static TaskState CreateTask(IEnumerator coroutine)
        {
            if (singleton == null)
            {
                GameObject target = new GameObject("TaskManager");
                UnityEngine.Object.DontDestroyOnLoad(target);
                singleton = target.AddComponent<TaskManager>();
            }
            return new TaskState(coroutine);
        }

        public class TaskState
        {
            private IEnumerator coroutine;
            private bool paused;
            private Coroutine routine;
            private bool running;
            private bool stopped;

            public event FinishedHandler Finished;

            public TaskState(IEnumerator c)
            {
                this.coroutine = c;
            }

            [DebuggerHidden]
            private IEnumerator CallWrapper()
            {
                return new <CallWrapper>c__Iterator3E { <>f__this = this };
            }

            public void Pause()
            {
                this.paused = true;
            }

            public void Start()
            {
                this.running = true;
                this.routine = TaskManager.singleton.StartCoroutine(this.CallWrapper());
            }

            public void Stop()
            {
                this.stopped = true;
                this.running = false;
            }

            public void Unpause()
            {
                this.paused = false;
            }

            public bool Paused
            {
                get
                {
                    return this.paused;
                }
            }

            public Coroutine Routine
            {
                get
                {
                    return this.routine;
                }
            }

            public bool Running
            {
                get
                {
                    return this.running;
                }
            }

            [CompilerGenerated]
            private sealed class <CallWrapper>c__Iterator3E : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal object $current;
                internal int $PC;
                internal TaskManager.TaskState <>f__this;
                internal IEnumerator <e>__0;
                internal TaskManager.TaskState.FinishedHandler <handler>__1;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$current = null;
                            this.$PC = 1;
                            goto Label_010E;

                        case 1:
                            this.<e>__0 = this.<>f__this.coroutine;
                            break;

                        case 2:
                        case 3:
                            break;

                        default:
                            goto Label_010C;
                    }
                    while (this.<>f__this.running)
                    {
                        if (this.<>f__this.paused)
                        {
                            this.$current = null;
                            this.$PC = 2;
                            goto Label_010E;
                        }
                        if ((this.<e>__0 != null) && this.<e>__0.MoveNext())
                        {
                            this.$current = this.<e>__0.Current;
                            this.$PC = 3;
                            goto Label_010E;
                        }
                        this.<>f__this.running = false;
                    }
                    this.<handler>__1 = this.<>f__this.Finished;
                    if (this.<handler>__1 != null)
                    {
                        this.<handler>__1(this.<>f__this.stopped);
                    }
                    this.$PC = -1;
                Label_010C:
                    return false;
                Label_010E:
                    return true;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current
                {
                    [DebuggerHidden]
                    get
                    {
                        return this.$current;
                    }
                }

                object IEnumerator.Current
                {
                    [DebuggerHidden]
                    get
                    {
                        return this.$current;
                    }
                }
            }

            public delegate void FinishedHandler(bool manual);
        }
    }
}

