﻿namespace com.game.module.core
{
    using com.game;
    using com.game.data;
    using com.game.module.Arena;
    using com.game.module.Dungeon;
    using com.u3d.bases.joystick;
    using System;

    public class UIController : Singleton<UIController>
    {
        private void ClearUI()
        {
            if (GlobalData.isInCopy)
            {
                ViewType[] types = new ViewType[2];
                types[1] = ViewType.CityView;
                ViewManager.ClosePanles(types);
            }
            else
            {
                ViewManager.ClosePanles(ViewType.BattleView);
            }
        }

        private void InitUI()
        {
            bool isInCopy = GlobalData.isInCopy;
            if (AppMap.Instance.IsInArena)
            {
                if (ArenaManager.Instance.IsNeedHideBattleView)
                {
                    BattleView instance = Singleton<BattleView>.Instance;
                    instance.OpenViewCallback = (OpenViewCallBack) Delegate.Combine(instance.OpenViewCallback, new OpenViewCallBack(Singleton<BattleView>.Instance.HideBattleViewWhenAllAIMode));
                }
                Singleton<BattleView>.Instance.OpenView();
            }
            else if (isInCopy)
            {
                if (DungeonMgr.Instance.curDungeonId != Singleton<ConfigConst>.Instance.GetConfigData("NEW_GUIDE_DUPLICATE"))
                {
                    Singleton<BattleView>.Instance.OpenView();
                }
            }
            else
            {
                Singleton<MainView>.Instance.OpenView();
            }
        }

        public void OnChangeScene()
        {
            this.ClearUI();
            this.InitUI();
            if (Singleton<MapMode>.Instance.IsFirstInToScene)
            {
                Singleton<MapMode>.Instance.IsFirstInToScene = false;
                Singleton<BattleActionView>.Instance.OpenView();
            }
            JoystickController.instance.Reset();
            Singleton<StartLoadingView>.Instance.CloseView();
            Singleton<BattleCenterView>.Instance.CancelHpWarning();
        }
    }
}

