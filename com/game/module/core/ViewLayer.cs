﻿namespace com.game.module.core
{
    using System;

    public enum ViewLayer
    {
        BaseLayer,
        LowLayer,
        MiddleLayer,
        HighLayer,
        TopLayer,
        TopUILayer,
        NoneLayer
    }
}

