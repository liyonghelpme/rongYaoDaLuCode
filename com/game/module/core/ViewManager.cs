﻿namespace com.game.module.core
{
    using com.game.module;
    using com.game.module.Guide;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public static class ViewManager
    {
        public const int NUM_LAYER_PANELS_MAX = 0x200;
        public static List<IView> openViewList = new List<IView>();

        private static void AddView(IView view)
        {
            int index = openViewList.IndexOf(view);
            if (view.layerType != ViewLayer.NoneLayer)
            {
                if (index >= 0)
                {
                    openViewList.RemoveAt(index);
                    index = -1;
                }
                int num2 = GetMaxDepth(view.layerType) + 1;
                UIPanel[] componentsInChildren = view.gameObject.GetComponentsInChildren<UIPanel>(true);
                Array.Sort<UIPanel>(componentsInChildren, new Comparison<UIPanel>(ViewManager.DepthCompareFunc));
                foreach (UIPanel panel in componentsInChildren)
                {
                    panel.depth = num2;
                    num2 += 3;
                }
            }
            if (index < 0)
            {
                openViewList.Add(view);
            }
        }

        public static void ClosePanles(ViewType type)
        {
            List<IView> openViewList = ViewManager.openViewList;
            lock (openViewList)
            {
                for (int i = ViewManager.openViewList.Count - 1; i >= 0; i--)
                {
                    if ((type == ViewManager.openViewList[i].viewType) && (Singleton<GuideView>.Instance != ViewManager.openViewList[i]))
                    {
                        ViewManager.openViewList[i].CloseView();
                    }
                    if (i > ViewManager.openViewList.Count)
                    {
                        i = ViewManager.openViewList.Count;
                    }
                }
                ViewManager.openViewList.Clear();
            }
        }

        public static void ClosePanles(ViewType[] types)
        {
            if ((types != null) && (types.Length > 0))
            {
                List<IView> openViewList = ViewManager.openViewList;
                lock (openViewList)
                {
                    for (int i = ViewManager.openViewList.Count - 1; i >= 0; i--)
                    {
                        if ((Singleton<GuideView>.Instance != ViewManager.openViewList[i]) && (Array.IndexOf<ViewType>(types, ViewManager.openViewList[i].viewType) >= 0))
                        {
                            ViewManager.openViewList[i].CloseView();
                        }
                        if (i > ViewManager.openViewList.Count)
                        {
                            i = ViewManager.openViewList.Count;
                        }
                    }
                    ViewManager.openViewList.Clear();
                }
            }
        }

        private static int DepthCompareFunc(UIPanel left, UIPanel right)
        {
            if (left.depth == right.depth)
            {
                return 0;
            }
            if (left.depth > right.depth)
            {
                return 1;
            }
            return -1;
        }

        private static int GetMaxDepth(ViewLayer layer)
        {
            int num = (int) (((ViewLayer) 0x200) * layer);
            int num2 = 0x7fffffff;
            int depth = -2147483648;
            foreach (IView view in openViewList)
            {
                if (view.layerType == layer)
                {
                    foreach (UIPanel panel in view.gameObject.GetComponentsInChildren<UIPanel>(true))
                    {
                        if (depth < panel.depth)
                        {
                            depth = panel.depth;
                        }
                        if (num2 > panel.depth)
                        {
                            num2 = panel.depth;
                        }
                    }
                }
            }
            foreach (IView view2 in openViewList)
            {
                if (view2.layerType == layer)
                {
                    foreach (UIPanel panel2 in view2.gameObject.GetComponentsInChildren<UIPanel>(true))
                    {
                        panel2.depth = (panel2.depth - num2) + num;
                    }
                }
            }
            if (((depth - num2) + num) < num)
            {
                return num;
            }
            return ((depth - num2) + num);
        }

        private static void HidePrevFullUI(IView topObj)
        {
            for (int i = openViewList.Count - 1; i >= 0; i--)
            {
                IView view = openViewList[i];
                if (null != view.gameObject)
                {
                    GameObject gameObject = view.gameObject.transform.parent.gameObject;
                    if (((view != topObj) && (((gameObject == com.game.module.ViewTree.go) || (gameObject == com.game.module.ViewTree.city)) || (gameObject == com.game.module.ViewTree.battle))) && !(view is GuideView))
                    {
                        view.gameObject.SetActive(false);
                    }
                }
            }
        }

        public static void Register(IView obj)
        {
            List<IView> openViewList = ViewManager.openViewList;
            lock (openViewList)
            {
                if (obj.IsFullUI)
                {
                    HidePrevFullUI(obj);
                }
                AddView(obj);
            }
        }

        private static void ShowPrevFullUI()
        {
            for (int i = openViewList.Count - 1; i >= 0; i--)
            {
                IView view = openViewList[i];
                if (null != view.gameObject)
                {
                    GameObject gameObject = view.gameObject.transform.parent.gameObject;
                    if ((((gameObject == com.game.module.ViewTree.go) || (gameObject == com.game.module.ViewTree.city)) || (gameObject == com.game.module.ViewTree.battle)) && !(view is GuideView))
                    {
                        view.gameObject.SetActive(true);
                    }
                    if (view.IsFullUI && view.gameObject.transform.parent.gameObject.activeSelf)
                    {
                        return;
                    }
                }
            }
        }

        public static void UnRegister(IView obj)
        {
            List<IView> openViewList = ViewManager.openViewList;
            lock (openViewList)
            {
                ViewManager.openViewList.Remove(obj);
                if (ViewManager.openViewList.Contains(obj))
                {
                }
                if (obj.IsFullUI)
                {
                    ShowPrevFullUI();
                }
            }
        }

        public static void Update()
        {
            List<IView> openViewList = ViewManager.openViewList;
            lock (openViewList)
            {
                int count = ViewManager.openViewList.Count;
                for (int i = 0; i < count; i++)
                {
                    int num3 = ViewManager.openViewList.Count;
                    ViewManager.openViewList[i].Update();
                    count = ViewManager.openViewList.Count;
                    if ((num3 - count) > 0)
                    {
                        i -= num3 - count;
                        if (i < 0)
                        {
                            i = 0;
                        }
                    }
                }
            }
        }
    }
}

