﻿namespace com.game.module.core
{
    using System;

    public enum ViewType
    {
        BattleView = 1,
        CityView = 3,
        NormalView = 0
    }
}

