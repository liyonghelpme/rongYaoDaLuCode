﻿namespace com.game.module.effect
{
    using System;
    using UnityEngine;

    public class AreaDamageEffectAction : BaseBulletEffectAction
    {
        protected override void Init()
        {
            if (base._curEffect.EnemyDisplay == null)
            {
                Vector3 vector = Vector3.Normalize(base._selfTransform.TransformDirection(Vector3.forward));
                base._curEffect.BasePosition += (Vector3) (vector * 2f);
                base._selfTransform.position = base._curEffect.BasePosition;
            }
            else
            {
                base._curEffect.BasePosition = base._curEffect.EnemyDisplay.GoBase.transform.position;
                base._selfTransform.position = base._curEffect.BasePosition;
            }
            base.Init();
        }
    }
}

