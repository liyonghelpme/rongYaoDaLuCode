﻿namespace com.game.module.effect
{
    using com.game.utils;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class BaseBulletEffectAction : BaseEffectAction
    {
        private float _buffCheckTime;
        protected float _calculateInterval;
        protected float _curTravelDistance;
        private const float BUFF_CHECK_INTERNAL = 0.3f;
        private GameObject trailGo;

        protected virtual void AddAreaBuff()
        {
            if (base._curEffect.SkillController != null)
            {
                this._buffCheckTime += Time.deltaTime;
                if (this._buffCheckTime >= 0.3f)
                {
                    this._buffCheckTime = 0f;
                    uint camp = base._curEffect.SkillController.MeController.GetMeVo().Camp;
                    List<ActionDisplay> list = SkillController.GetEnemyDisplays(base._selfTransform, camp, base._curEffect.SkillVo.shape_x * 0.001f);
                    List<PDamageBuff> damageBuffListFromString = StringUtils.GetDamageBuffListFromString(base._curEffect.SkillVo.param_list);
                    foreach (ActionDisplay display in list)
                    {
                        display.Controller.buffController.AddBuff(damageBuffListFromString, null);
                    }
                }
            }
        }

        protected virtual void CheckDamageAndClear()
        {
            this.DefaultCheckDemageAndClear();
        }

        protected void CheckSkillSubtype()
        {
            if ((base._curEffect.SkillVo != null) && (base._curEffect.SkillVo.subtype == 15))
            {
                this.AddAreaBuff();
            }
        }

        protected void DefaultCheckDemageAndClear()
        {
            if (base._curEffect.SkillController != null)
            {
                this._calculateInterval += Time.deltaTime;
                if (this._calculateInterval > base._nextCheckTime)
                {
                    if (base._curEffect.CheckInterval == 0f)
                    {
                        base._nextCheckTime = 1000000f;
                    }
                    else
                    {
                        base._nextCheckTime = this._calculateInterval + base._curEffect.CheckInterval;
                    }
                    bool flag = base._curEffect.SkillController.CheckDamage(base._selfTransform.position, base._curEffect.SkillVo, base._curEffect.EffectIndex, base._selfTransform, base._curEffect.EnemyDisplay, 0, 0x3e8);
                }
            }
        }

        protected void DefaultMove_Forward()
        {
            if (base._curEffect.Speed > 0f)
            {
                Vector3 translation = (Vector3) ((Vector3.forward * base._curEffect.Speed) * Time.deltaTime);
                this._curTravelDistance += translation.magnitude;
                base.selfGo.transform.Translate(translation);
                if ((base._curEffect.MaxTravelDistance > 0f) && (this._curTravelDistance > base._curEffect.MaxTravelDistance))
                {
                    this.Clear();
                }
            }
        }

        protected override void DelayPrepare()
        {
            this.trailGo = base.FindGOInAllChildren<TrailRenderer>(base.selfGo);
            if (this.trailGo != null)
            {
                this.trailGo.SetActive(false);
            }
            base.orgPos = base._curEffect.BasePosition;
            base.selfGo.transform.position = base.HIDDEN_POS;
        }

        protected override void Init()
        {
            this._curTravelDistance = 0f;
            base.Init();
        }

        protected virtual void Move()
        {
        }

        protected override void StartUse()
        {
            if (base.selfGo != null)
            {
                base.selfGo.SetActive(false);
                base.selfGo.transform.position = base.orgPos;
                if (this.trailGo != null)
                {
                    this.trailGo.SetActive(true);
                }
                base.selfGo.SetActive(true);
                this.Init();
                base._canPlay = true;
            }
        }

        public override void Update()
        {
            if (base.CanPlayCheck())
            {
                this.CheckDamageAndClear();
                if (!base._isClearing)
                {
                    this.Move();
                    this.CheckSkillSubtype();
                }
            }
        }
    }
}

