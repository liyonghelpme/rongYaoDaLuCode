﻿namespace com.game.module.effect
{
    using System;
    using System.Collections;
    using UnityEngine;

    public class BaseEffectAction : IEffectAction
    {
        protected bool _canPlay;
        protected com.game.module.effect.Effect _curEffect;
        protected float _deltaTime;
        public bool _isChangeScene;
        protected bool _isClearing;
        protected float _nextCheckTime;
        protected Transform _selfTransform;
        private float _waitTime;
        protected readonly Vector3 HIDDEN_POS = ((Vector3) (Vector3.one * 10000f));
        protected Vector3 orgPos;
        protected GameObject selfGo;

        protected bool CanPlayCheck()
        {
            return (this.InitCheck() && this._canPlay);
        }

        protected void ChangeEffectPlaySpeed(float speed)
        {
            ParticleSystem[] componentsInChildren = this.selfGo.GetComponentsInChildren<ParticleSystem>(true);
            Animator[] animatorArray = this.selfGo.GetComponentsInChildren<Animator>(true);
            foreach (ParticleSystem system in componentsInChildren)
            {
                Debug.Log(system.startSpeed);
                if (system.startSpeed > 0f)
                {
                    system.startSpeed = speed;
                }
            }
            foreach (Animator animator in animatorArray)
            {
                Debug.Log(animator.speed);
                animator.speed = speed;
            }
            this._curEffect.LastTime /= speed;
        }

        protected void CheckBeforeStartUse()
        {
            if (!this._isChangeScene)
            {
                this.StartUse();
            }
        }

        public virtual void Clear()
        {
            this._isClearing = true;
            if (this._curEffect.PlayedCallback != null)
            {
                this._curEffect.PlayedCallback(this.selfGo);
            }
            if (this._curEffect.AutoDestroy)
            {
                EffectMgr.Instance.RemoveEffect(this._curEffect.DictKey);
            }
            else
            {
                this.selfGo.GetComponent<EffectControlerII>().enabled = false;
                this.selfGo.SetActive(false);
            }
            this.ClearOtherData();
        }

        protected virtual void ClearOtherData()
        {
        }

        protected virtual void DelayPrepare()
        {
        }

        protected GameObject FindGOInAllChildren<T>(GameObject parent) where T: Component
        {
            IEnumerator enumerator = parent.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    if (current.GetComponent<T>() != null)
                    {
                        return current.gameObject;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            IEnumerator enumerator2 = parent.transform.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    Transform transform2 = (Transform) enumerator2.Current;
                    GameObject obj2 = this.FindGOInAllChildren<T>(transform2.gameObject);
                    if (obj2 != null)
                    {
                        return obj2;
                    }
                }
            }
            finally
            {
                IDisposable disposable2 = enumerator2 as IDisposable;
                if (disposable2 == null)
                {
                }
                disposable2.Dispose();
            }
            return null;
        }

        protected virtual void Init()
        {
        }

        protected bool InitCheck()
        {
            return ((this._curEffect != null) && (this.selfGo != null));
        }

        protected void InitDelay()
        {
            if (this._curEffect.DelayTime > 0f)
            {
                this.selfGo.SetActive(false);
                this._canPlay = false;
                this.DelayPrepare();
                vp_Timer.In(this._curEffect.DelayTime, new vp_Timer.Callback(this.CheckBeforeStartUse), null);
            }
            else
            {
                this._canPlay = true;
                this.Init();
            }
        }

        public void SetChangeScene(bool change)
        {
            this._isChangeScene = change;
        }

        public virtual void SetVo(com.game.module.effect.Effect vo, GameObject selfGo)
        {
            this._curEffect = vo;
            this.selfGo = selfGo;
            this._waitTime = vo.LastTime;
            this._deltaTime = 0f;
            this._selfTransform = selfGo.transform;
            this._isChangeScene = false;
            this._nextCheckTime = vo.DetectTime;
            this.InitDelay();
        }

        public virtual void Start()
        {
        }

        protected virtual void StartUse()
        {
            if (this.selfGo != null)
            {
                this.selfGo.SetActive(true);
                this._canPlay = true;
                this.Init();
            }
        }

        public void Tick()
        {
            if (this.CanPlayCheck() && (this._waitTime > 0f))
            {
                this._deltaTime += Time.deltaTime;
                if (this._deltaTime >= this._waitTime)
                {
                    this.Clear();
                }
            }
        }

        public virtual void Update()
        {
            if (!this.CanPlayCheck())
            {
            }
        }
    }
}

