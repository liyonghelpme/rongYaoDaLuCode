﻿namespace com.game.module.effect
{
    using com.game.module.fight.vo;
    using System;

    internal class CacheEffectInfo
    {
        public BuffVo buff;
        public Effect effect;
    }
}

