﻿namespace com.game.module.effect
{
    using com.game.module.fight.arpg;
    using com.game.utils;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class CatapultEffectAction : BaseBulletEffectAction
    {
        private bool _canRepeat;
        private int _catapultCount;
        private ActionDisplay _nextEnemy;
        private int[] _paramslist;
        private SkillController.CatapultCheckNextEnemy evaluateFunc;
        private DamageCheckOptions option;

        public CatapultEffectAction()
        {
            DamageCheckOptions options = new DamageCheckOptions {
                excludeList = new List<ActionDisplay>()
            };
            this.option = options;
        }

        protected override void CheckDamageAndClear()
        {
            if (this.CheckDistance() && base._curEffect.SkillController.CheckDamage1(base._selfTransform.position, base._curEffect.SkillVo, ref this.option, base._curEffect.EffectIndex, base._selfTransform, this._nextEnemy, this._catapultCount, 0x3e8))
            {
                base._curEffect.Speed = this._paramslist[2] * 0.001f;
                this._catapultCount++;
                if (this.evaluateFunc != null)
                {
                    this._nextEnemy = this.evaluateFunc(base._selfTransform.position, base._curEffect.SkillController.MeController.GetMeVo().Camp, this._paramslist[1] * 0.001f, this.option.excludeList, this._nextEnemy, true);
                }
                if (this.option.excludeList.Contains(this._nextEnemy))
                {
                    this.option.excludeList.Remove(this._nextEnemy);
                }
                if ((this._catapultCount >= this._paramslist[0]) || (this._nextEnemy == null))
                {
                    this.Clear();
                }
            }
        }

        private bool CheckDistance()
        {
            if ((this._nextEnemy == null) || (this._nextEnemy.GoBase == null))
            {
                return false;
            }
            return ((DamageCheck.GetDistance(base._selfTransform.position, this._nextEnemy.GoBase.transform.position) - this._nextEnemy.BoundDistance) <= 0.5f);
        }

        protected override void Init()
        {
            base.Init();
            if ((base._curEffect.SkillController == null) || (base._curEffect.SkillVo == null))
            {
                this.Clear();
            }
            else
            {
                this._paramslist = StringUtils.GetArrayStringToInt(base._curEffect.SkillVo.param_list);
                if (this._paramslist.Length < 3)
                {
                    Debug.LogError("skillId = " + base._curEffect.SkillVo.id + "param_list error");
                    this.Clear();
                }
                else
                {
                    this._canRepeat = (this._paramslist.Length < 4) || (this._paramslist[3] == 1);
                    switch (base._curEffect.SkillVo.target_type)
                    {
                        case 2:
                            this.evaluateFunc = new SkillController.CatapultCheckNextEnemy(base._curEffect.SkillController.GetNearestTeammateForCatapult);
                            break;

                        case 3:
                            this.evaluateFunc = new SkillController.CatapultCheckNextEnemy(base._curEffect.SkillController.GetNearestAttackableTargetForCatapult);
                            break;
                    }
                    if (this.evaluateFunc != null)
                    {
                        this._nextEnemy = this.evaluateFunc(base._selfTransform.position, base._curEffect.SkillController.MeController.GetMeVo().Camp, (float) this._paramslist[1], this.option.excludeList, this._nextEnemy, true);
                    }
                    if (this._nextEnemy == null)
                    {
                        this.Clear();
                    }
                }
            }
        }

        protected override void Move()
        {
            if (base._curEffect.Speed > 0f)
            {
                if ((this._nextEnemy == null) || (this._nextEnemy.GoBase == null))
                {
                    base.DefaultMove_Forward();
                }
                else
                {
                    base._selfTransform.LookAt(this._nextEnemy.GoBase.transform.position);
                    Vector3 translation = (Vector3) ((Vector3.forward * base._curEffect.Speed) * Time.deltaTime);
                    base._curTravelDistance += translation.magnitude;
                    base._selfTransform.Translate(translation);
                }
            }
        }
    }
}

