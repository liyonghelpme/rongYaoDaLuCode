﻿namespace com.game.module.effect
{
    using com.game.data;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class Effect
    {
        private SysEffectTimeVo _effectconfigVo;
        public bool AutoDestroy;
        public Vector3 BasePosition;
        public int BulletType;
        public float CheckInterval;
        public EffectCreateCallback CreatedCallback;
        public float DelayTime;
        public float DetectTime;
        public string DictKey;
        public int EffectIndex;
        public com.game.module.effect.EffectType EffectType;
        public ActionDisplay EnemyDisplay;
        public string EnvironmentEff;
        public Vector3 EulerAngles;
        public uint Id;
        public float LastTime;
        public float MaxTravelDistance;
        public string MountPath;
        public Vector3 offset;
        public EffectCallback PlayedCallback;
        public com.u3d.bases.controller.SkillController SkillController;
        public SysSkillBaseVo SkillVo;
        public float Speed;
        public GameObject Target;
        public bool UIEffect;
        public string URL;
        public float Zoom;

        private void InitTimeVo(bool auto)
        {
            if (this._effectconfigVo != null)
            {
                this.LastTime = !auto ? 0f : (this._effectconfigVo.duration * 0.001f);
                this.offset = new Vector3(this._effectconfigVo.offset_x * 0.001f, this._effectconfigVo.offset_y * 0.001f, this._effectconfigVo.offset_z * 0.001f);
                this.MountPath = StringUtils.GetValueStringWithBlank(this._effectconfigVo.mount_path);
            }
            else
            {
                this.LastTime = 0f;
                this.offset = Vector3.zero;
                this.MountPath = string.Empty;
            }
        }

        public void SetEffectConfigVo(SysEffectTimeVo vo, bool autoGetTime)
        {
            this._effectconfigVo = vo;
            this.InitTimeVo(autoGetTime);
        }

        public BaseRoleVo EnemyRoleVo
        {
            get
            {
                if (this.EnemyDisplay == null)
                {
                    return null;
                }
                return (this.EnemyDisplay.GetVo() as BaseRoleVo);
            }
        }

        public delegate void EffectCallback(GameObject go);

        public delegate void EffectCreateCallback(EffectControlerII controller);
    }
}

