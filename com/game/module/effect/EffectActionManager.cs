﻿namespace com.game.module.effect
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class EffectActionManager
    {
        private readonly Dictionary<int, System.Type> _effectActionDic = new Dictionary<int, System.Type>();
        public static readonly EffectActionManager Instance;

        static EffectActionManager()
        {
            if (Instance == null)
            {
            }
            Instance = new EffectActionManager();
        }

        public IEffectAction GetBulletEffectAction(int id)
        {
            if (this._effectActionDic.ContainsKey(id))
            {
                return (IEffectAction) Activator.CreateInstance(this._effectActionDic[id]);
            }
            Debug.Log("id = " + id);
            Debug.LogError("特效子弹类型配置错误");
            return null;
        }

        public IEffectAction GetIEffectAction(Effect vo)
        {
            switch (vo.EffectType)
            {
                case EffectType.Normal:
                    return this.GetNormalEffectAction();

                case EffectType.Bullet:
                {
                    IEffectAction bulletEffectAction = this.GetBulletEffectAction(vo.BulletType);
                    if (bulletEffectAction == null)
                    {
                        bulletEffectAction = this.GetNormalEffectAction();
                    }
                    return bulletEffectAction;
                }
            }
            return this.GetNormalEffectAction();
        }

        public IEffectAction GetNormalEffectAction()
        {
            return new NormalEffectAction();
        }

        public void InitDic()
        {
            this._effectActionDic.Add(0, typeof(TrackEffectAction));
            this._effectActionDic.Add(3, typeof(PenetrateEffectAction));
            this._effectActionDic.Add(10, typeof(AreaDamageEffectAction));
            this._effectActionDic.Add(11, typeof(StoneThrowEffectAction));
            this._effectActionDic.Add(12, typeof(SelfCenterAreaDamageEffectAction));
            this._effectActionDic.Add(4, typeof(CatapultEffectAction));
            this._effectActionDic.Add(13, typeof(ShipSkillEffectAction));
        }
    }
}

