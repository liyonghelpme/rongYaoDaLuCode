﻿namespace com.game.module.effect
{
    using System;
    using UnityEngine;

    public class EffectControlerII : MonoBehaviour
    {
        private IEffectAction effectAction;

        public void Clear()
        {
            this.effectAction.Clear();
        }

        public void SetChangeScene(bool change)
        {
            this.effectAction.SetChangeScene(change);
        }

        public void SetVo(Effect vo)
        {
            this.effectAction = EffectActionManager.Instance.GetIEffectAction(vo);
            this.effectAction.SetVo(vo, base.gameObject);
            this.effectAction.Start();
        }

        private void Start()
        {
        }

        private void Update()
        {
            this.effectAction.Tick();
            if (this.effectAction != null)
            {
                this.effectAction.Update();
            }
        }
    }
}

