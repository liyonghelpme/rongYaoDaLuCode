﻿namespace com.game.module.effect
{
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class EffectFactory
    {
        public static Effect CreateBuffEffect(string effectId, GameObject meGo, Vector3 position, bool atuoGetTime, Effect.EffectCallback playedCallBack = null, Effect.EffectCreateCallback createCallBack = null, bool autoDestory = true)
        {
            Effect.EffectCallback callback = playedCallBack;
            Effect.EffectCreateCallback callback2 = createCallBack;
            bool flag = atuoGetTime;
            bool flag2 = autoDestory;
            return CreateEffect(effectId, meGo, null, position, EffectType.Buff, null, 0f, 0, null, flag2, callback, flag, callback2, null);
        }

        public static Effect CreateEffect(string effectId, GameObject meGo, SkillController controller, Vector3 basePosition, EffectType effectType = 0, SysSkillActionVo actionVo = null, float delayTime = 0, int effectIndex = 0, ActionDisplay enemyDisplay = null, bool autoDestory = false, Effect.EffectCallback playedCallBack = null, bool atuoGetTime = true, Effect.EffectCreateCallback createCallBack = null, SysSkillBaseVo skillVo = null)
        {
            Effect eff = new Effect();
            if (effectId == null)
            {
                return null;
            }
            eff.Id = uint.Parse(effectId);
            eff.URL = UrlUtils.GetEffectUrl(effectId, effectType);
            if (meGo != null)
            {
                eff.EulerAngles = meGo.transform.eulerAngles;
                eff.BasePosition = meGo.transform.position;
            }
            eff.SetEffectConfigVo(BaseDataMgr.instance.GetSysEffectTimeVo(eff.Id), atuoGetTime);
            if (basePosition != new Vector3(0f, 0f, 0f))
            {
                eff.BasePosition = basePosition;
            }
            eff.SkillController = controller;
            eff.EnemyDisplay = enemyDisplay;
            eff.DelayTime = delayTime;
            eff.EffectIndex = effectIndex;
            eff.AutoDestroy = autoDestory;
            eff.CreatedCallback = createCallBack;
            eff.PlayedCallback = playedCallBack;
            eff.SkillVo = skillVo;
            SetEffectWithActionVo(eff, actionVo, effectType, skillVo);
            eff.Target = meGo;
            if (effectType == EffectType.UI)
            {
                eff.UIEffect = true;
            }
            return eff;
        }

        public static Effect CreateStaticEffect(string effectId, Vector3 postion, Effect.EffectCreateCallback createCallBack, Effect.EffectCallback playedCallBack = null, bool autoDestroy = false)
        {
            Effect.EffectCallback callback = playedCallBack;
            Effect.EffectCreateCallback callback2 = createCallBack;
            bool autoDestory = autoDestroy;
            return CreateEffect(effectId, null, null, postion, EffectType.Buff, null, 0f, 0, null, autoDestory, callback, false, callback2, null);
        }

        public static Effect CreateUIEffect(string effectId, Vector3 pos, Effect.EffectCallback playedCallBack = null, Effect.EffectCreateCallback createdCallback = null)
        {
            Effect.EffectCallback callback = playedCallBack;
            Effect.EffectCreateCallback createCallBack = createdCallback;
            return CreateEffect(effectId, null, null, pos, EffectType.UI, null, 0f, 0, null, true, callback, true, createCallBack, null);
        }

        private static void GetBulletTypeBySkillSubType(SysSkillActionVo vo, SysSkillBaseVo skillvo)
        {
            if (skillvo != null)
            {
                switch (skillvo.subtype)
                {
                    case 0x11:
                        vo.BulletType = 4;
                        return;

                    case 0x13:
                        vo.BulletType = 13;
                        return;
                }
            }
        }

        private static void SetEffectWithActionVo(Effect eff, SysSkillActionVo vo, EffectType effectType, SysSkillBaseVo skillVo)
        {
            if (((effectType == EffectType.Bullet) && (vo != null)) && (eff != null))
            {
                GetBulletTypeBySkillSubType(vo, skillVo);
                eff.EffectType = EffectType.Bullet;
                eff.Speed = vo.bullet_speed;
                eff.BulletType = vo.BulletType;
                eff.CheckInterval = vo.CheckInterval * 0.001f;
                eff.MaxTravelDistance = vo.BulletTravelDistance * 0.001f;
                eff.DetectTime = vo.DetectTime * 0.001f;
            }
        }
    }
}

