﻿namespace com.game.module.effect
{
    using System;

    public class EffectId
    {
        public const string Main_AutoSerachRoad = "20006";
        public const string Main_BossFootBuff = "20017";
        public const string Main_BossScene = "20011";
        public const string Main_BossWarning = "20018";
        public const string Main_CityClick = "20002";
        public const string Main_CylinderStandby = "20012";
        public const string Main_CylinderTrigger = "20013";
        public const string Main_FootSmoke = "20008";
        public const string Main_MonsterSpawn = "20003";
        public const string Main_Pet1Attack = "20014";
        public const string Main_Pet2Attack = "20015";
        public const string Main_Pet3Attack = "20016";
        public const string Main_Resurrection = "20007";
        public const string Main_RoleToTeleport = "20009";
        public const string Main_RoleUpgrade = "20004";
        public const string Main_TaskFinish = "20005";
        public const string Main_TaskReceive = "20010";
        public const string Main_Teleport = "20001";
        public static readonly string[] PVPEffects = new string[] { "20042", "20046", "20040", "20041", "20045", "20047", "20047", "20044" };
        public const string Skill_Butterfly_Bom = "14041";
        public const string Skill_Butterfly_Bullet = "14031";
        public const string Skill_MonsterDeathFog = "11115";
        public const string Skill_MonsterDefense = "11111";
        public const string Skill_MonsterFrantic = "11112";
        public const string Skill_StoneDiam = "11114";
        public const string Skill_StoneMoney = "11116";
        public const string Skill_Tornado = "21031";
        public const string State_Death = "10021";
        public const string State_Dizzness = "10011";
        public const string TIANHUI_BASE_BOOM = "20042";
        public const string TIANHUI_BASE_IDLE = "20046";
        public const string TIANHUI_TOWER_BOOM = "20040";
        public const string TIANHUI_TOWER_IDLE = "20041";
        public const string UI_AutoBattle = "30037";
        public const string UI_AutoSearchGold = "30038";
        public const string UI_BossComingWarning = "30029";
        public const string UI_ChallengeFail = "30011";
        public const string UI_ChallengeOk = "30010";
        public const string UI_Clearance = "30004";
        public const string UI_EquipStressFail = "30002";
        public const string UI_EquipStressOk = "30001";
        public const string UI_FightCountDown = "30007";
        public const string UI_GoGo = "30013";
        public const string UI_GoldBurst = "30014";
        public const string UI_Guide = "30036";
        public const string UI_LargeCrit = "30017";
        public const string UI_Loading = "30020";
        public const string UI_LuckWand = "30018";
        public const string UI_MovieStart1 = "30028";
        public const string UI_MovieStart2 = "30034";
        public const string UI_MovieStart3 = "30035";
        public const string UI_NewWorldPoint = "30003";
        public const string UI_PetAnimation = "30024";
        public const string UI_PetBurst = "30021";
        public const string UI_PetLight = "30025";
        public const string UI_PropBurst = "30022";
        public const string UI_RewardClick = "30006";
        public const string UI_RewardStar = "30012";
        public const string UI_SeaWater = "30019";
        public const string UI_SecondStage = "30008";
        public const string UI_SelectRoleBurst = "30039";
        public const string UI_SelectRoleFeedback = "30033";
        public const string UI_SelectRoleStage1 = "30031";
        public const string UI_SelectRoleStage2 = "30032";
        public const string UI_SilverBurst = "30015";
        public const string UI_SkillIcon = "30005";
        public const string UI_SmallCrit = "30016";
        public const string UI_ThirdStage = "30009";
        public const string UI_TopPropOrange = "30023";
        public const string UI_TopPropPurple = "30026";
        public const string UI_TopPropRed = "30027";
        public const string UI_Win = "30030";
        public const string YEYAN_BASE_BOOM = "20045";
        public const string YEYAN_BASE_IDLE = "20047";
        public const string YEYAN_TOWER_BOOM = "20043";
        public const string YEYAN_TOWER_IDLE = "20044";
    }
}

