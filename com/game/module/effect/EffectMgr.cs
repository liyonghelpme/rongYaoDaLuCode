﻿namespace com.game.module.effect
{
    using com.game.data;
    using com.game.manager;
    using com.game.module;
    using com.game.module.core;
    using com.game.module.fight.vo;
    using com.game.preloader;
    using com.game.utils;
    using com.liyong;
    using com.u3d.bases.display;
    using com.u3d.bases.fsmUtil;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityLog;

    public class EffectMgr : IPreloader
    {
        private IList<com.game.module.effect.Effect> autoDestroyEffectList = new List<com.game.module.effect.Effect>();
        private BuffVo curBuffVo;
        private string curEffectUrl;
        private com.game.module.effect.Effect curEffectVo;
        private string curPreLoadEffectUrl;
        private IList<CacheEffectInfo> effectCache = new List<CacheEffectInfo>();
        private IDictionary<string, GameObject> effectDictionary = new Dictionary<string, GameObject>();
        public static EffectMgr Instance;
        public List<uint> LoadedGeneralIdList = new List<uint>();
        private const string MAIN_EFFECT_LAYER = "Default";
        private float mainCameraEffectZOff = -31f;
        private GameObject mainEffectRoot = new GameObject("MainEffectRoot");
        private List<MountEffect> mountEffectList = new List<MountEffect>();
        private int preloadIndex;
        private IList<string> preLoadList;
        private IDictionary<string, GameObject> resDictionary = new Dictionary<string, GameObject>();
        private GameObject shadowEffect;
        private const string UI_EFFECT_LAYER = "UIEffect";
        private GameObject uiEffectRoot;

        static EffectMgr()
        {
            if (Instance == null)
            {
            }
            Instance = new EffectMgr();
        }

        private EffectMgr()
        {
            NGUITools.SetLayer(this.mainEffectRoot, LayerMask.NameToLayer("Default"));
            UnityEngine.Object.DontDestroyOnLoad(this.mainEffectRoot);
            this.uiEffectRoot = NGUITools.AddChild(com.game.module.ViewTree.go);
            this.uiEffectRoot.name = "UIEffectRoot";
            NGUITools.SetLayer(this.uiEffectRoot, LayerMask.NameToLayer("UIEffect"));
            UnityEngine.Object.DontDestroyOnLoad(this.uiEffectRoot);
        }

        private GameObject CheckHasCacheEffect(int id, string mountPath, string url)
        {
            foreach (MountEffect effect in this.mountEffectList)
            {
                if (effect.IsEqual(id, mountPath, url))
                {
                    if (effect.effectObj.activeInHierarchy)
                    {
                        UnityEngine.Debug.LogError("相同挂载特效不应该同时释放两次");
                        return null;
                    }
                    return effect.effectObj;
                }
            }
            return null;
        }

        private void ClearMountEffectList()
        {
            this.mountEffectList.Clear();
        }

        public void CreateBuffEffect(string effectId, GameObject meGo, com.game.module.effect.Effect.EffectCreateCallback createCallBack = null, BuffVo buff = null, bool autoGetTime = false, bool atuoDestory = true, com.game.module.effect.Effect.EffectCallback playedCallBack = null)
        {
            com.game.module.effect.Effect vo = EffectFactory.CreateBuffEffect(effectId, meGo, meGo.transform.position, autoGetTime, playedCallBack, createCallBack, atuoDestory);
            this.CreateEffect(vo, buff);
        }

        public void CreateBuffEffect(string effectId, GameObject meGo, Vector3 position, com.game.module.effect.Effect.EffectCreateCallback createCallBack = null, BuffVo buff = null, bool autoGetTime = false, bool atuoDestory = true, com.game.module.effect.Effect.EffectCallback playedCallBack = null)
        {
            com.game.module.effect.Effect vo = EffectFactory.CreateBuffEffect(effectId, meGo, position, autoGetTime, playedCallBack, createCallBack, atuoDestory);
            this.CreateEffect(vo, buff);
        }

        private void CreateEffect(com.game.module.effect.Effect vo, BuffVo buff = null)
        {
            if (StringUtils.IsValidConfigParam(vo.URL))
            {
                CacheEffectInfo item = new CacheEffectInfo {
                    effect = vo,
                    buff = buff
                };
                IList<CacheEffectInfo> effectCache = this.effectCache;
                lock (effectCache)
                {
                    this.effectCache.Add(item);
                    if (vo.AutoDestroy)
                    {
                        this.autoDestroyEffectList.Add(vo);
                    }
                    if (this.effectCache.Count == 1)
                    {
                        this.CreateInternalEffect(vo, buff);
                    }
                }
            }
        }

        private void CreateInternalEffect(com.game.module.effect.Effect vo, BuffVo buff = null)
        {
            try
            {
                this.curEffectVo = vo;
                this.curEffectUrl = vo.URL;
                this.curBuffVo = buff;
                if (!this.resDictionary.ContainsKey(this.curEffectUrl))
                {
                    AssetBundleLoader loader = AssetManager.Instance.LoadAsset<GameObject>(this.curEffectUrl, new LoadAssetFinish<GameObject>(this.EffectLoaded), null, false, true);
                }
                else
                {
                    this.EffectLoaded(this.resDictionary[this.curEffectUrl]);
                }
            }
            catch (Exception exception)
            {
                UnityLog.Log.Critical("CreateEffect error, exception is: " + exception.Message);
            }
        }

        private void CreateInternalMainEffect(string url, Vector3 pos, bool autoDestroy, com.game.module.effect.Effect.EffectCallback playedCallBack, float lastTime, com.game.module.effect.Effect.EffectCreateCallback createdCallBack)
        {
            if (!StringUtils.isEmpty(url))
            {
                com.game.module.effect.Effect vo = new com.game.module.effect.Effect {
                    URL = url,
                    BasePosition = pos
                };
                vo.BasePosition.z = this.GetMainCameraEffectZ(pos.y);
                vo.AutoDestroy = autoDestroy;
                vo.PlayedCallback = playedCallBack;
                vo.CreatedCallback = createdCallBack;
                vo.LastTime = lastTime;
                this.CreateEffect(vo, null);
            }
        }

        private void CreateInternalUIEffect(string url, Vector3 pos, com.game.module.effect.Effect.EffectCallback playedCallBack, com.game.module.effect.Effect.EffectCreateCallback createdCallBack)
        {
            if (!StringUtils.isEmpty(url))
            {
                if (!Singleton<EffectCameraMgr>.Instance.CameraEnabled)
                {
                    Singleton<EffectCameraMgr>.Instance.OpenCamera();
                }
                com.game.module.effect.Effect vo = EffectFactory.CreateUIEffect(url, pos, playedCallBack, createdCallBack);
                this.CreateEffect(vo, null);
            }
        }

        public void CreateMainEffect(string effectId, Vector3 pos, bool autoDestroy = true, com.game.module.effect.Effect.EffectCallback playedCallBack = null, float lastTime = 0)
        {
            string mainEffectUrl = UrlUtils.GetMainEffectUrl(effectId);
            this.CreateInternalMainEffect(mainEffectUrl, pos, autoDestroy, playedCallBack, lastTime, null);
        }

        public void CreateMainFollowEffect(string effectId, GameObject target, Vector3 offset, bool autoDestroy = true, com.game.module.effect.Effect.EffectCallback playedCallBack = null, com.game.module.effect.Effect.EffectCreateCallback createdCallBack = null)
        {
            string mainEffectUrl = UrlUtils.GetMainEffectUrl(effectId);
            if (!StringUtils.isEmpty(mainEffectUrl))
            {
                com.game.module.effect.Effect vo = new com.game.module.effect.Effect {
                    Id = uint.Parse(effectId),
                    URL = mainEffectUrl,
                    BasePosition = target.transform.position
                };
                vo.BasePosition.z = this.GetMainCameraEffectZ(vo.BasePosition.y);
                vo.Target = target;
                vo.AutoDestroy = autoDestroy;
                vo.CreatedCallback = createdCallBack;
                vo.PlayedCallback = playedCallBack;
                this.CreateEffect(vo, null);
            }
        }

        private void CreatePreloadEffectObj(string effectUrl, GameObject effectObj)
        {
            if (!this.effectDictionary.ContainsKey(effectUrl))
            {
                GameObject obj2 = UnityEngine.Object.Instantiate(effectObj) as GameObject;
                GameObject go = new GameObject(effectUrl);
                obj2.transform.parent = go.transform;
                NGUITools.SetLayer(go, LayerMask.NameToLayer("Default"));
                go.SetActive(false);
                UnityEngine.Object.DontDestroyOnLoad(go);
                go.transform.parent = this.mainEffectRoot.transform;
                IDictionary<string, GameObject> effectDictionary = this.effectDictionary;
                lock (effectDictionary)
                {
                    this.effectDictionary.Add(effectUrl, go);
                }
            }
        }

        public void CreateSceneEffect(string effectId, Vector3 position, GameObject target = null, bool atuoGetTime = true, bool autoDestroy = true, com.game.module.effect.Effect.EffectCreateCallback createCallback = null, com.game.module.effect.Effect.EffectCallback playedCallBack = null)
        {
            bool autoDestory = autoDestroy;
            bool flag2 = atuoGetTime;
            com.game.module.effect.Effect.EffectCreateCallback createCallBack = createCallback;
            com.game.module.effect.Effect.EffectCallback callback2 = playedCallBack;
            com.game.module.effect.Effect vo = EffectFactory.CreateEffect(effectId, target, null, position, EffectType.Scene, null, 0f, 0, null, autoDestory, callback2, flag2, createCallBack, null);
            this.CreateEffect(vo, null);
        }

        public void CreateSkillEffect(com.game.module.effect.Effect vo)
        {
            this.CreateEffect(vo, null);
        }

        public void CreateStateEffect(string id, GameObject go, Vector3 postion)
        {
            com.game.module.effect.Effect vo = EffectFactory.CreateEffect(id, go, null, postion, EffectType.State, null, 0f, 0, null, false, null, true, null, null);
            this.CreateEffect(vo, null);
        }

        public void CreateStaticBuffEffect(string effectId, Vector3 position, com.game.module.effect.Effect.EffectCreateCallback createCallback, com.game.module.effect.Effect.EffectCallback playedCallback = null, bool autoDestory = true)
        {
            com.game.module.effect.Effect vo = EffectFactory.CreateStaticEffect(effectId, position, createCallback, playedCallback, autoDestory);
            this.CreateEffect(vo, null);
        }

        public void CreateStoryMovieEffect(string effectId, Vector3 pos, com.game.module.effect.Effect.EffectCallback playedCallBack = null)
        {
            string storyMovieEffectUrl = UrlUtils.GetStoryMovieEffectUrl(effectId);
            this.CreateInternalUIEffect(storyMovieEffectUrl, pos, playedCallBack, null);
        }

        public void CreateStorySceneEffect(string effectId, Vector3 pos, com.game.module.effect.Effect.EffectCallback playedCallBack = null, com.game.module.effect.Effect.EffectCreateCallback createdCallBack = null)
        {
            string storySceneEffectUrl = UrlUtils.GetStorySceneEffectUrl(effectId);
            this.CreateInternalMainEffect(storySceneEffectUrl, pos, true, playedCallBack, 0f, createdCallBack);
        }

        public void CreateUIEffect(string effectId, Vector3 pos, com.game.module.effect.Effect.EffectCallback playedCallBack = null, com.game.module.effect.Effect.EffectCreateCallback createdCallBack = null)
        {
            this.CreateInternalUIEffect(effectId, pos, playedCallBack, createdCallBack);
        }

        private void EffectLoaded(GameObject effectObj)
        {
            try
            {
                if (!this.resDictionary.ContainsKey(this.curEffectUrl) && (null != effectObj))
                {
                    this.resDictionary.Add(this.curEffectUrl, effectObj);
                }
                if (null == effectObj)
                {
                    UnityLog.Log.Critical("特效加载出错：特效url" + this.curEffectVo.URL);
                }
                else
                {
                    GameObject mountEffectObj;
                    GameObject obj3 = effectObj;
                    if ((this.curEffectVo.Target != null) && (this.curEffectVo.MountPath != string.Empty))
                    {
                        mountEffectObj = this.GetMountEffectObj(this.curEffectUrl, obj3);
                        if (mountEffectObj == null)
                        {
                            UnityEngine.Debug.Log("挂载特效有问题！！");
                            return;
                        }
                    }
                    else
                    {
                        mountEffectObj = this.GetHierEffectObj(this.curEffectUrl, obj3);
                    }
                    if (null != mountEffectObj)
                    {
                        mountEffectObj.SetActive(false);
                        if (this.curEffectVo.MountPath != string.Empty)
                        {
                            mountEffectObj.transform.localPosition = this.curEffectVo.offset;
                        }
                        else
                        {
                            mountEffectObj.transform.position = this.curEffectVo.BasePosition;
                        }
                        Quaternion identity = Quaternion.identity;
                        Vector3 vector = new Vector3(0f, this.curEffectVo.EulerAngles.y, 0f);
                        identity.eulerAngles = vector;
                        mountEffectObj.transform.localRotation = identity;
                        Vector3 offset = this.curEffectVo.offset;
                        mountEffectObj.transform.GetChild(0).localPosition = offset;
                        Vector3 vector2 = offset;
                        if (this.curEffectVo.Zoom > 0f)
                        {
                            mountEffectObj.transform.localScale = (Vector3) (Vector3.one * this.curEffectVo.Zoom);
                        }
                        mountEffectObj.SetActive(true);
                        EffectControlerII component = mountEffectObj.GetComponent<EffectControlerII>();
                        if (null == component)
                        {
                            component = mountEffectObj.AddComponent<EffectControlerII>();
                        }
                        else
                        {
                            component.enabled = true;
                        }
                        component.SetVo(this.curEffectVo);
                        if (this.curEffectVo.CreatedCallback != null)
                        {
                            this.curEffectVo.CreatedCallback(component);
                        }
                        if (this.curBuffVo != null)
                        {
                            if (this.curEffectVo.Id == this.curBuffVo.BuffTemp.buffEffectId)
                            {
                                if ((!this.curBuffVo.buffController.workEffectFlag.ContainsKey(this.curBuffVo.BuffId) || (this.curBuffVo.buffController.workEffectFlag[this.curBuffVo.BuffId] == 0)) || (this.curBuffVo.buffController.workEffectMap.ContainsKey(this.curBuffVo.BuffId) && (this.curBuffVo.buffController.workEffectMap[this.curBuffVo.BuffId] != null)))
                                {
                                    UnityEngine.Debug.Log("[clear]");
                                    component.Clear();
                                }
                                else
                                {
                                    this.curBuffVo.effectController = component;
                                    this.curBuffVo.buffController.workEffectMap[this.curBuffVo.BuffId] = component;
                                }
                            }
                            this.curBuffVo = null;
                        }
                        this.ResetParticleSystem(mountEffectObj);
                    }
                }
            }
            catch (Exception exception)
            {
                UnityEngine.Debug.Log("CreateEffect error, exception is: " + exception.StackTrace + "Message is：" + exception.Message);
                UnityLog.Log.Critical("CreateEffect error, exception is: " + exception.StackTrace + "Message is：" + exception.Message);
            }
            finally
            {
                this.SortEffectCachePlayed(this.curEffectVo);
            }
        }

        private GameObject FindGOInAllChildren<T>(GameObject parent) where T: Component
        {
            IEnumerator enumerator = parent.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    if (current.GetComponent<T>() != null)
                    {
                        return current.gameObject;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            IEnumerator enumerator2 = parent.transform.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    Transform transform2 = (Transform) enumerator2.Current;
                    GameObject obj2 = this.FindGOInAllChildren<T>(transform2.gameObject);
                    if (obj2 != null)
                    {
                        return obj2;
                    }
                }
            }
            finally
            {
                IDisposable disposable2 = enumerator2 as IDisposable;
                if (disposable2 == null)
                {
                }
                disposable2.Dispose();
            }
            return null;
        }

        private List<string> GetActionUseEffectId(uint skillGroup)
        {
            List<string> list = new List<string>();
            SysSkillActionVo dataByTypeAndId = BaseDataMgr.instance.GetDataByTypeAndId<SysSkillActionVo>("SysSkillActionVo", skillGroup);
            if (dataByTypeAndId != null)
            {
                list.Add(this.GetUseEffId(dataByTypeAndId));
                list.AddRange(this.GetProcessEffId(dataByTypeAndId));
                list.AddRange(this.GetBeAttackEffId(dataByTypeAndId));
            }
            return list;
        }

        private List<uint> GetAddList(List<uint> loadedGeneralList, List<uint> preLoadGeneralList)
        {
            List<uint> list = new List<uint>();
            foreach (uint num in preLoadGeneralList)
            {
                if (!loadedGeneralList.Contains(num))
                {
                    list.Add(num);
                }
            }
            uint currentFightGeneralId = Singleton<GeneralMode>.Instance.GetCurrentFightGeneralId();
            if ((currentFightGeneralId != 0) && list.Contains(currentFightGeneralId))
            {
                list.Remove(currentFightGeneralId);
                list.Insert(0, currentFightGeneralId);
            }
            return list;
        }

        private List<string> GetAllActionUseEffectId(List<uint> skillGroups)
        {
            List<string> list = new List<string>();
            foreach (uint num in skillGroups)
            {
                list.AddRange(this.GetActionUseEffectId(num));
            }
            return list;
        }

        private List<string> GetBeAttackEffId(SysSkillActionVo vo)
        {
            return new List<string>(StringUtils.GetValueListFromString(vo.tar_eff, ','));
        }

        public static Vector3 GetBuffPosition(BaseDisplay display, int type)
        {
            if (display == null)
            {
                return new Vector3(0f, 0f, 0f);
            }
            Vector3 position = display.GoBase.transform.position;
            switch (type)
            {
                case 1:
                    return position;

                case 2:
                    return (display.Controller.gameObject.transform.FindChild("blood").position + new Vector3(0f, 1f, 0f));

                case 3:
                {
                    float y = display.GoCloth.GetComponentInChildren<SkinnedMeshRenderer>().bounds.extents.magnitude / 2f;
                    return (position + new Vector3(0f, y, 0f));
                }
            }
            return position;
        }

        private GameObject GetEffectGameObject(string url)
        {
            if (!StringUtils.isEmpty(url) && this.effectDictionary.ContainsKey(url))
            {
                return this.effectDictionary[url];
            }
            return null;
        }

        private GameObject GetHierEffectObj(string baseEffectUrl, GameObject effectObj)
        {
            if (!this.effectDictionary.ContainsKey(baseEffectUrl) || this.effectDictionary[baseEffectUrl].activeInHierarchy)
            {
                if (!this.effectDictionary.ContainsKey(baseEffectUrl))
                {
                    this.curEffectVo.DictKey = baseEffectUrl;
                }
                else
                {
                    int num = 1;
                    string key = baseEffectUrl + "_" + num.ToString();
                    while (this.effectDictionary.ContainsKey(key) && this.effectDictionary[key].activeInHierarchy)
                    {
                        num++;
                        if (num > 30)
                        {
                            UnityLog.Log.Critical("特效没有消除!! +" + key);
                            return null;
                        }
                        key = baseEffectUrl + "_" + num.ToString();
                    }
                    this.curEffectVo.DictKey = key;
                }
                if (!this.effectDictionary.ContainsKey(this.curEffectVo.DictKey))
                {
                    GameObject go = NGUITools.AddChild(this.curEffectRoot);
                    go.name = this.curEffectVo.DictKey;
                    GameObject obj3 = UnityEngine.Object.Instantiate(effectObj) as GameObject;
                    obj3.transform.localPosition = this.curEffectVo.offset;
                    obj3.transform.parent = go.transform;
                    NGUITools.SetLayer(go, LayerMask.NameToLayer(this.curLayer));
                    if (!this.curEffectVo.AutoDestroy)
                    {
                        UnityEngine.Object.DontDestroyOnLoad(go);
                    }
                    IDictionary<string, GameObject> effectDictionary = this.effectDictionary;
                    lock (effectDictionary)
                    {
                        this.effectDictionary.Add(this.curEffectVo.DictKey, go);
                    }
                    return go;
                }
                return this.effectDictionary[this.curEffectVo.DictKey];
            }
            this.curEffectVo.DictKey = baseEffectUrl;
            return this.effectDictionary[this.curEffectVo.DictKey];
        }

        public float GetMainCameraEffectZ(float y)
        {
            return ((y * 5f) + this.mainCameraEffectZOff);
        }

        public GameObject GetMainEffectGameObject(string effectId, string suffix = "")
        {
            string url = UrlUtils.GetMainEffectUrl(effectId) + suffix;
            return this.GetEffectGameObject(url);
        }

        private GameObject GetMountEffectObj(string baseEffectUrl, GameObject effectObj)
        {
            GameObject go = this.CheckHasCacheEffect(this.curEffectVo.Target.GetInstanceID(), this.curEffectVo.MountPath, baseEffectUrl);
            this.curEffectVo.DictKey = baseEffectUrl;
            if (go == null)
            {
                GameObject mountParent = this.GetMountParent(this.curEffectVo.Target, this.curEffectVo.MountPath);
                if (mountParent == null)
                {
                    return null;
                }
                go = NGUITools.AddChild(mountParent);
                go.name = this.curEffectVo.DictKey;
                GameObject obj4 = (GameObject) UnityEngine.Object.Instantiate(effectObj);
                obj4.transform.parent = go.transform;
                obj4.transform.localRotation = mountParent.transform.localRotation;
                NGUITools.SetLayer(go, LayerMask.NameToLayer(this.curLayer));
                MountEffect item = new MountEffect {
                    parentGoId = this.curEffectVo.Target.GetInstanceID(),
                    mountPath = this.curEffectVo.MountPath,
                    url = baseEffectUrl,
                    effectObj = go
                };
                this.mountEffectList.Add(item);
            }
            return go;
        }

        private GameObject GetMountParent(GameObject target, string mountPath)
        {
            GameObject parent = null;
            GameObject gameObject;
            for (int i = 0; i < target.transform.childCount; i++)
            {
                Transform child = target.transform.GetChild(i);
                if (child.name.Contains("(Clone)"))
                {
                    parent = child.gameObject;
                    break;
                }
            }
            if (parent == null)
            {
                UnityEngine.Debug.LogError("找不到挂载点：" + target.name);
                return null;
            }
            if (Regex.IsMatch(mountPath, "Point", RegexOptions.IgnoreCase))
            {
                gameObject = Util.FindChildRecursive(parent.transform, mountPath).gameObject;
            }
            else
            {
                gameObject = NGUITools.FindChild(parent, mountPath);
            }
            if (gameObject == null)
            {
                UnityEngine.Debug.LogError("挂载路径出错：" + target.name + "path：" + mountPath);
            }
            return gameObject;
        }

        public List<string> GetNeedPreloadList(List<uint> generalList)
        {
            List<uint> addList = this.GetAddList(this.LoadedGeneralIdList, generalList);
            List<uint> skillListByGeneralId = this.GetSkillListByGeneralId(addList);
            return this.GetAllActionUseEffectId(skillListByGeneralId);
        }

        private List<string> GetProcessEffId(SysSkillActionVo vo)
        {
            List<string> list = new List<string>();
            foreach (string str in StringUtils.SplitVoString(vo.process_eff, "],["))
            {
                string[] valueListFromString = StringUtils.GetValueListFromString(str, ',');
                if (valueListFromString.Length >= 2)
                {
                    list.Add(valueListFromString[0]);
                }
            }
            return list;
        }

        private List<uint> GetRemoveList(List<uint> loadedGeneralList, List<uint> preLoadGeneralList)
        {
            <GetRemoveList>c__AnonStoreyF4 yf = new <GetRemoveList>c__AnonStoreyF4 {
                preLoadGeneralList = preLoadGeneralList
            };
            return loadedGeneralList.Filter<uint>(new Predicate<uint>(yf.<>m__DA)).ToList<uint>();
        }

        public GameObject GetSkillEffectGameObject(string effectId)
        {
            string skillEffectUrl = UrlUtils.GetSkillEffectUrl(effectId);
            return this.GetEffectGameObject(skillEffectUrl);
        }

        private List<uint> GetSkillListByGeneralId(List<uint> generalIdList)
        {
            List<uint> list = new List<uint>();
            foreach (uint num in generalIdList)
            {
                list.AddRange(Singleton<GeneralMode>.Instance.GetFightGeneralSkillListById(num));
            }
            return list;
        }

        public GameObject GetStorySceneEffectGameObject(string effectId)
        {
            string storySceneEffectUrl = UrlUtils.GetStorySceneEffectUrl(effectId);
            return this.GetEffectGameObject(storySceneEffectUrl);
        }

        public GameObject GetUIEffectGameObject(string effectId)
        {
            string uIEffectUrl = UrlUtils.GetUIEffectUrl(effectId);
            return this.GetEffectGameObject(uIEffectUrl);
        }

        private string GetUseEffId(SysSkillActionVo vo)
        {
            return StringUtils.GetValueListFromString(vo.use_eff, ',')[0];
        }

        private bool IsAutoDestroyEffectStopped(string url)
        {
            IEnumerator<com.game.module.effect.Effect> enumerator = this.autoDestroyEffectList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    com.game.module.effect.Effect current = enumerator.Current;
                    if (current.URL.Equals(url))
                    {
                        return false;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return true;
        }

        private bool IsInTheSameGrid(Vector3 pos1, Vector3 pos2)
        {
            int x = (int) pos1.x;
            int y = (int) pos1.y;
            int num3 = (int) pos2.x;
            int num4 = (int) pos2.y;
            return ((x == num3) && (y == num4));
        }

        public bool IsStoryMovieEffectStopped(string effectId)
        {
            string storyMovieEffectUrl = UrlUtils.GetStoryMovieEffectUrl(effectId);
            return this.IsAutoDestroyEffectStopped(storyMovieEffectUrl);
        }

        public bool IsStorySceneEffectStopped(string effectId)
        {
            string storySceneEffectUrl = UrlUtils.GetStorySceneEffectUrl(effectId);
            return this.IsAutoDestroyEffectStopped(storySceneEffectUrl);
        }

        public void LoadShadowEffect()
        {
            AssetManager.Instance.LoadAsset<GameObject>("Effect/shadow/ShadowEffct.assetbundle", new LoadAssetFinish<GameObject>(this.ShadowEffectComplete), null, false, true);
        }

        private void MainEffectPreLoaded(GameObject effectObj)
        {
            if (null != effectObj)
            {
                string mainEffectUrl = UrlUtils.GetMainEffectUrl(effectObj.name);
                if (!this.resDictionary.ContainsKey(mainEffectUrl))
                {
                    this.resDictionary.Add(mainEffectUrl, effectObj);
                }
                this.CreatePreloadEffectObj(mainEffectUrl, effectObj);
            }
        }

        public void PreloadMainEffect(string effectId)
        {
            string mainEffectUrl = UrlUtils.GetMainEffectUrl(effectId);
            if (!StringUtils.isEmpty(mainEffectUrl) && !this.resDictionary.ContainsKey(mainEffectUrl))
            {
                UnityLog.Log.Minor("preload effect asset " + mainEffectUrl);
                AssetManager.Instance.LoadAsset<GameObject>(mainEffectUrl, new LoadAssetFinish<GameObject>(this.MainEffectPreLoaded), null, false, true);
            }
        }

        private void PreloadResource()
        {
            try
            {
                while (this.preloadIndex < this.preLoadList.Count)
                {
                    if (this.preLoadList[this.preloadIndex] == string.Empty)
                    {
                        this.preloadIndex++;
                    }
                    else
                    {
                        this.curPreLoadEffectUrl = "Effect/Skill/" + this.preLoadList[this.preloadIndex] + ".assetbundle";
                        if (!this.resDictionary.ContainsKey(this.curPreLoadEffectUrl))
                        {
                            AssetManager.Instance.LoadAsset<GameObject>(this.curPreLoadEffectUrl, new LoadAssetFinish<GameObject>(this.SkillEffectPreLoaded), null, false, true);
                            return;
                        }
                        this.preloadIndex++;
                    }
                }
            }
            catch (Exception exception)
            {
                UnityLog.Log.Critical("preloadResource error, exception is: " + exception.Message);
            }
        }

        public IEnumerator PreloadResourceList(IList<SysReadyLoadVo> preLoadList)
        {
            return null;
        }

        [DebuggerHidden]
        public IEnumerator PreloadResourceList(IList<string> preLoadList)
        {
            return new <PreloadResourceList>c__Iterator34 { preLoadList = preLoadList, <$>preLoadList = preLoadList, <>f__this = this };
        }

        public void PreloadUIEffect(string effectId)
        {
            string uIEffectUrl = UrlUtils.GetUIEffectUrl(effectId);
            if (!StringUtils.isEmpty(uIEffectUrl) && !this.resDictionary.ContainsKey(uIEffectUrl))
            {
                UnityLog.Log.Minor("preload effect asset " + uIEffectUrl);
                AssetManager.Instance.LoadAsset<GameObject>(uIEffectUrl, new LoadAssetFinish<GameObject>(this.UIEffectPreLoaded), null, false, true);
            }
        }

        public void RemoveAllUIEffect()
        {
            for (int i = this.autoDestroyEffectList.Count - 1; i >= 0; i--)
            {
                com.game.module.effect.Effect effect = this.autoDestroyEffectList[i];
                if (effect.UIEffect)
                {
                    this.RemoveEffect(effect.DictKey);
                }
            }
        }

        public void RemoveEffect(string effectUrl)
        {
            if (effectUrl != null)
            {
                if (this.effectDictionary.ContainsKey(effectUrl))
                {
                    UnityEngine.Object.Destroy(this.effectDictionary[effectUrl]);
                }
                IDictionary<string, GameObject> effectDictionary = this.effectDictionary;
                lock (effectDictionary)
                {
                    this.effectDictionary.Remove(effectUrl);
                }
                IEnumerator<com.game.module.effect.Effect> enumerator = this.autoDestroyEffectList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        com.game.module.effect.Effect current = enumerator.Current;
                        if ((current.DictKey != null) && current.DictKey.Equals(effectUrl))
                        {
                            this.autoDestroyEffectList.Remove(current);
                            return;
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        public void RemoveSceneEffect(string effectId)
        {
            string effectUrl = "Effect/Scene/" + effectId + ".assetbundle";
            this.RemoveEffect(effectUrl);
        }

        private void removeSkillResDict(string effectUrl)
        {
            if (effectUrl != null)
            {
                IDictionary<string, GameObject> resDictionary = this.resDictionary;
                lock (resDictionary)
                {
                    this.resDictionary.Remove(effectUrl);
                }
            }
        }

        public void RemoveUnusedEffect(List<uint> generalList)
        {
            List<uint> removeList = this.GetRemoveList(this.LoadedGeneralIdList, generalList);
            List<uint> skillListByGeneralId = this.GetSkillListByGeneralId(removeList);
            foreach (string str in this.GetAllActionUseEffectId(skillListByGeneralId))
            {
                this.RemoveEffect("Effect/Skill/" + str + ".assetbundle");
            }
        }

        public void ResetEffectWhileChangeScene()
        {
            List<string> list = new List<string>();
            IEnumerator<KeyValuePair<string, GameObject>> enumerator = this.effectDictionary.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    KeyValuePair<string, GameObject> current = enumerator.Current;
                    list.Add(current.Key);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            foreach (string str in list)
            {
                if (str.Contains("_"))
                {
                    this.RemoveEffect(str);
                }
                else
                {
                    EffectControlerII component = this.effectDictionary[str].GetComponent<EffectControlerII>();
                    if (component != null)
                    {
                        component.SetChangeScene(true);
                        if (component.gameObject.activeInHierarchy)
                        {
                            component.Clear();
                        }
                    }
                    else
                    {
                        this.effectDictionary[str].SetActive(false);
                    }
                }
                this.ClearMountEffectList();
            }
        }

        public void ResetParticleSystem(GameObject effectObj)
        {
            IEnumerator enumerator = effectObj.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    foreach (ParticleSystem system in current.GetComponents<ParticleSystem>())
                    {
                        system.Clear();
                        system.Simulate(0f, false, true);
                        system.Play();
                    }
                    foreach (ParticleEmitter emitter in current.GetComponents<ParticleEmitter>())
                    {
                        emitter.ClearParticles();
                        emitter.Simulate(0f);
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            IEnumerator enumerator2 = effectObj.transform.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    Transform transform2 = (Transform) enumerator2.Current;
                    this.ResetParticleSystem(transform2.gameObject);
                }
            }
            finally
            {
                IDisposable disposable2 = enumerator2 as IDisposable;
                if (disposable2 == null)
                {
                }
                disposable2.Dispose();
            }
        }

        public void SetLoadedGeneralList(List<uint> curGenenralList)
        {
            this.LoadedGeneralIdList.Clear();
            curGenenralList.ForEach(i => this.LoadedGeneralIdList.Add(i));
        }

        private void ShadowEffectComplete(GameObject go)
        {
            this.shadowEffect = go;
        }

        private void SkillEffectPreLoaded(GameObject effectObj)
        {
            try
            {
                if (null != effectObj)
                {
                    if (!this.resDictionary.ContainsKey(this.curPreLoadEffectUrl))
                    {
                        this.resDictionary.Add(this.curPreLoadEffectUrl, effectObj);
                    }
                    this.CreatePreloadEffectObj(this.curPreLoadEffectUrl, effectObj);
                }
            }
            catch (Exception exception)
            {
                UnityLog.Log.Critical("SkillEffectPreLoaded error, exception is: " + exception.Message);
            }
            finally
            {
                this.preloadIndex++;
                this.PreloadResource();
            }
        }

        private void SortEffectCachePlayed(com.game.module.effect.Effect vo)
        {
            CacheEffectInfo item = null;
            IEnumerator<CacheEffectInfo> enumerator = this.effectCache.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    CacheEffectInfo current = enumerator.Current;
                    if (current.effect == vo)
                    {
                        item = current;
                        goto Label_0048;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        Label_0048:
            if (item != null)
            {
                IList<CacheEffectInfo> effectCache = this.effectCache;
                lock (effectCache)
                {
                    this.effectCache.Remove(item);
                }
            }
            if (this.effectCache.Count > 0)
            {
                this.CreateInternalEffect(this.effectCache[0].effect, this.effectCache[0].buff);
            }
        }

        private void UIEffectPreLoaded(GameObject effectObj)
        {
            if (null != effectObj)
            {
                string uIEffectUrl = UrlUtils.GetUIEffectUrl(effectObj.name);
                if (!this.resDictionary.ContainsKey(uIEffectUrl))
                {
                    this.resDictionary.Add(uIEffectUrl, effectObj);
                }
            }
        }

        private GameObject curEffectRoot
        {
            get
            {
                if (this.curEffectVo.UIEffect)
                {
                    return this.uiEffectRoot;
                }
                return this.mainEffectRoot;
            }
        }

        private string curLayer
        {
            get
            {
                if (this.curEffectVo.UIEffect)
                {
                    return "UIEffect";
                }
                return "Default";
            }
        }

        public GameObject MainEffectRoot
        {
            get
            {
                return this.mainEffectRoot;
            }
        }

        public GameObject ShadowEffect
        {
            get
            {
                return this.shadowEffect;
            }
        }

        private int uiEffectRemain
        {
            get
            {
                int num = 0;
                IEnumerator<com.game.module.effect.Effect> enumerator = this.autoDestroyEffectList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        com.game.module.effect.Effect current = enumerator.Current;
                        if (current.UIEffect)
                        {
                            num++;
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                return num;
            }
        }

        [CompilerGenerated]
        private sealed class <GetRemoveList>c__AnonStoreyF4
        {
            internal List<uint> preLoadGeneralList;

            internal bool <>m__DA(uint x)
            {
                return !this.preLoadGeneralList.Contains(x);
            }
        }

        [CompilerGenerated]
        private sealed class <PreloadResourceList>c__Iterator34 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal IList<string> <$>preLoadList;
            internal EffectMgr <>f__this;
            internal IList<string> preLoadList;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<>f__this.preLoadList = this.preLoadList;
                        this.<>f__this.preloadIndex = 0;
                        this.<>f__this.PreloadResource();
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_008D;
                }
                if (this.<>f__this.preloadIndex < this.<>f__this.preLoadList.Count)
                {
                    this.$current = 0;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_008D:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

