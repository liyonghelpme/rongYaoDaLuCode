﻿namespace com.game.module.effect
{
    using System;
    using UnityEngine;

    public class EffectRandom : MonoBehaviour
    {
        public float MaxX = 1f;
        public float MaxY = 1f;
        public float MinX = -1f;
        public float MinY = -1f;
        public bool RandomPos;
        public float RotateZMax;
        public float RotateZMin;
        private float z;
        public float ZoomMax = 1f;
        public float ZoomMin = 1f;

        private void OnDrawGizmosSelected()
        {
            if (this.RandomPos)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(new Vector3(this.MinX, this.MaxY, this.z), new Vector3(this.MaxX, this.MaxY, this.z));
                Gizmos.DrawLine(new Vector3(this.MaxX, this.MaxY, this.z), new Vector3(this.MaxX, this.MinY, this.z));
                Gizmos.DrawLine(new Vector3(this.MaxX, this.MinY, this.z), new Vector3(this.MinX, this.MinY, this.z));
                Gizmos.DrawLine(new Vector3(this.MinX, this.MinY, this.z), new Vector3(this.MinX, this.MaxY, this.z));
            }
        }

        public void Start()
        {
            float x = UnityEngine.Random.Range(this.ZoomMin, this.ZoomMax);
            float z = UnityEngine.Random.Range(this.RotateZMin, this.RotateZMax);
            Vector3 vector = new Vector3(x, x, x);
            Vector3 vector2 = new Vector3(0f, 0f, z);
            Quaternion identity = Quaternion.identity;
            identity.eulerAngles = vector2;
            base.transform.localScale = vector;
            base.transform.localRotation = identity;
            if (this.RandomPos)
            {
                this.z = base.transform.position.z;
                float num3 = base.transform.localPosition.z;
                float num4 = UnityEngine.Random.Range(this.MinX, this.MaxX);
                float y = UnityEngine.Random.Range(this.MinY, this.MaxY);
                Vector3 vector3 = new Vector3(num4, y, num3);
                base.transform.localPosition = vector3;
            }
        }
    }
}

