﻿namespace com.game.module.effect
{
    using System;

    public enum EffectType
    {
        Normal,
        Bullet,
        Scene,
        Buff,
        State,
        UI
    }
}

