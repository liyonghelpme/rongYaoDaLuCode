﻿namespace com.game.module.effect
{
    using System;
    using UnityEngine;

    public interface IEffectAction
    {
        void Clear();
        void SetChangeScene(bool change);
        void SetVo(Effect vo, GameObject selfGo);
        void Start();
        void Tick();
        void Update();
    }
}

