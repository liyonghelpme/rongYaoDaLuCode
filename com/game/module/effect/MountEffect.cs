﻿namespace com.game.module.effect
{
    using System;
    using UnityEngine;

    internal class MountEffect
    {
        public GameObject effectObj;
        public string mountPath;
        public int parentGoId;
        public string url;

        public bool IsEqual(int id, string path, string url)
        {
            return (((this.parentGoId == id) && (path == this.mountPath)) && (url == this.url));
        }
    }
}

