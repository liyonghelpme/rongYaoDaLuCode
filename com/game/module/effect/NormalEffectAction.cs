﻿namespace com.game.module.effect
{
    using System;
    using UnityEngine;

    public class NormalEffectAction : BaseEffectAction
    {
        private Vector3 offset = new Vector3(0f, 0f, 0f);
        private Transform targetTranform;

        protected override void Init()
        {
            if ((base._curEffect != null) && (base._curEffect.Target != null))
            {
                this.targetTranform = base._curEffect.Target.transform;
                this.offset = base._curEffect.BasePosition - this.targetTranform.position;
            }
        }

        public override void Update()
        {
            if (base.InitCheck() && ((this.targetTranform != null) && (base._curEffect.MountPath == string.Empty)))
            {
                base.selfGo.transform.position = this.targetTranform.position + this.offset;
            }
        }
    }
}

