﻿namespace com.game.module.effect
{
    using com.u3d.bases.controller;
    using System;
    using System.Collections.Generic;

    public class PenetrateEffectAction : BaseBulletEffectAction
    {
        private DamageCheckOptions option;

        public PenetrateEffectAction()
        {
            DamageCheckOptions options = new DamageCheckOptions {
                excludeList = new List<ActionDisplay>()
            };
            this.option = options;
        }

        protected override void CheckDamageAndClear()
        {
            base._curEffect.SkillController.CheckDamage1(base.selfGo.transform.position, base._curEffect.SkillVo, ref this.option, 0, null, null, 0, 0x3e8);
        }

        protected override void Move()
        {
            base.DefaultMove_Forward();
        }
    }
}

