﻿namespace com.game.module.effect
{
    using System;

    public class SelfCenterAreaDamageEffectAction : BaseBulletEffectAction
    {
        protected override void Move()
        {
            if (((base._curEffect != null) && (base._curEffect.SkillController != null)) && (base._curEffect.Target != null))
            {
                base._selfTransform.position = base._curEffect.Target.transform.position;
            }
        }
    }
}

