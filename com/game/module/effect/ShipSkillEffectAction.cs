﻿namespace com.game.module.effect
{
    using com.game.utils;
    using System;
    using UnityEngine;

    internal class ShipSkillEffectAction : PenetrateEffectAction
    {
        protected override void Init()
        {
            base.Init();
            Vector3 vector = Vector3.Normalize(base._selfTransform.TransformDirection(Vector3.forward));
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(base._curEffect.SkillVo.param_list);
            float num = 0f;
            if (arrayStringToInt.Length < 1)
            {
                num = 0f;
            }
            else
            {
                num = arrayStringToInt[0] * 0.001f;
            }
            base._curEffect.BasePosition += (Vector3) (vector * num);
            base._selfTransform.position = base._curEffect.BasePosition;
        }
    }
}

