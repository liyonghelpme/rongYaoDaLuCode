﻿namespace com.game.module.effect
{
    using System;
    using UnityEngine;

    public class StoneThrowEffectAction : BaseBulletEffectAction
    {
        private Vector3 tarPos;

        protected override void Move()
        {
            base._selfTransform.LookAt(this.tarPos);
            Vector3 translation = (Vector3) ((Vector3.forward * base._curEffect.Speed) * Time.deltaTime);
            base._curTravelDistance += translation.magnitude;
            base._selfTransform.Translate(translation);
        }

        public override void Start()
        {
            if (base._curEffect.EnemyDisplay != null)
            {
                float num = Vector3.Distance(base._curEffect.BasePosition, base._curEffect.EnemyDisplay.GoBase.transform.position);
                base._curEffect.Speed = num / base._curEffect.LastTime;
                this.tarPos = base._curEffect.EnemyDisplay.GoBase.transform.position;
            }
        }
    }
}

