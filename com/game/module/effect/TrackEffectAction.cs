﻿namespace com.game.module.effect
{
    using com.game.module.fight.arpg;
    using System;
    using UnityEngine;

    public class TrackEffectAction : BaseBulletEffectAction
    {
        private void CheckClear()
        {
            if ((base._curEffect.MaxTravelDistance > 0f) && (base._curTravelDistance > base._curEffect.MaxTravelDistance))
            {
                this.Clear();
            }
            else if ((base._curEffect.EnemyRoleVo != null) && base._curEffect.EnemyRoleVo.IsEmptyHp)
            {
                base._curEffect.EnemyDisplay = null;
            }
            else
            {
                Vector3 position = base._selfTransform.position;
                Vector3 y = base._curEffect.EnemyDisplay.GoBase.transform.position;
                if (((DamageCheck.GetDistance(position, y) - base._curEffect.EnemyDisplay.BoundDistance) <= 0.5f) && base._curEffect.SkillController.CheckDamage(base._selfTransform.position, base._curEffect.SkillVo, base._curEffect.EffectIndex, base._selfTransform, base._curEffect.EnemyDisplay, 0, 0x3e8))
                {
                    this.Clear();
                }
            }
        }

        protected override void CheckDamageAndClear()
        {
            if (((base._curEffect != null) && (base._curEffect.EnemyDisplay != null)) && (base._curEffect.EnemyDisplay.GoBase != null))
            {
                this.CheckClear();
            }
        }

        protected override void Move()
        {
            if ((base._curEffect.EnemyDisplay == null) || (base._curEffect.EnemyDisplay.GoBase == null))
            {
                base.DefaultMove_Forward();
            }
            else
            {
                this.TrackMove();
            }
        }

        private void TrackMove()
        {
            if (base._curEffect.Speed > 0f)
            {
                base._selfTransform.LookAt(base._curEffect.EnemyDisplay.GoBase.transform.position);
                Vector3 translation = (Vector3) ((Vector3.forward * base._curEffect.Speed) * Time.deltaTime);
                base._curTravelDistance += translation.magnitude;
                base._selfTransform.Translate(translation);
            }
        }
    }
}

