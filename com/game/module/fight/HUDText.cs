﻿namespace com.game.module.fight
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class HUDText : MonoBehaviour
    {
        private float alpha;
        public readonly AnimationCurve alphaCurve;
        private readonly List<string> colors;
        public UISprite comSprite;
        private float offset;
        public readonly AnimationCurve offsetCurve;
        private readonly List<Color> outLineColors;
        private UIPanel panel;
        private float positionY;
        public readonly AnimationCurve scaleCurve;
        private float stay;
        private float textTime;
        public UILabel uiLabel;

        public HUDText()
        {
            Keyframe[] keys = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(0f, 30f) };
            this.offsetCurve = new AnimationCurve(keys);
            Keyframe[] keyframeArray2 = new Keyframe[] { new Keyframe(0f, 1f), new Keyframe(1f, 0f) };
            this.alphaCurve = new AnimationCurve(keyframeArray2);
            Keyframe[] keyframeArray3 = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(0.25f, 1f) };
            this.scaleCurve = new AnimationCurve(keyframeArray3);
            this.colors = new List<string> { "[ffffff]", "[68ff39]", "[fff845]", "[fe9212]" };
            this.outLineColors = new List<Color> { new Color(0f, 0f, 0f), new Color(0.1411765f, 0.04313726f, 0f), new Color(0.2666667f, 0.08235294f, 0f), new Color(0.1411765f, 0.04313726f, 0f) };
        }

        public void Add(object obj, float stayDuration, int type)
        {
            string str;
            base.gameObject.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            this.stay = stayDuration;
            this.textTime = Time.realtimeSinceStartup;
            this.uiLabel.effectStyle = UILabel.Effect.Outline;
            this.uiLabel.effectDistance = new Vector2(1f, 1f);
            this.uiLabel.effectColor = this.outLineColors[type];
            this.comSprite.gameObject.SetActive(false);
            this.uiLabel.transform.localPosition = new Vector3(0f, 0f, 0f);
            if ((type == 2) || (type == 3))
            {
                this.uiLabel.transform.localPosition = new Vector3(40f, 0f, 0f);
                this.comSprite.gameObject.SetActive(true);
                this.comSprite.spriteName = (type != 2) ? "combo_2" : "combo_1";
            }
            if (type == 1)
            {
                object[] objArray1 = new object[] { this.colors[type], " + ", obj, "[-]" };
                str = string.Concat(objArray1);
            }
            else
            {
                str = this.colors[type] + obj + "[-]";
            }
            this.uiLabel.text = str;
        }

        private void Start()
        {
            this.panel = base.GetComponent<UIPanel>();
        }

        private void Update()
        {
            float realtimeSinceStartup = Time.realtimeSinceStartup;
            float time = this.offsetCurve.keys[this.offsetCurve.keys.Length - 1].time;
            float b = this.alphaCurve.keys[this.alphaCurve.keys.Length - 1].time;
            float num5 = Mathf.Max(this.scaleCurve.keys[this.scaleCurve.keys.Length - 1].time, Mathf.Max(time, b));
            float num6 = realtimeSinceStartup - this.movementStart;
            this.offset = this.offsetCurve.Evaluate(num6);
            this.alpha = this.alphaCurve.Evaluate(num6);
            float x = this.scaleCurve.Evaluate(realtimeSinceStartup - this.textTime);
            if (x < 0.01f)
            {
                x = 0.01f;
            }
            base.gameObject.transform.localScale = new Vector3(x, x, x);
            if (this.panel != null)
            {
                this.panel.alpha = this.alpha;
            }
            this.positionY = Mathf.Max(this.positionY, this.offset + 10f);
            base.gameObject.transform.localPosition = new Vector3(base.gameObject.transform.localPosition.x, this.positionY, base.gameObject.transform.localPosition.z);
            this.positionY += base.gameObject.transform.localScale.y;
            if ((num6 > num5) && (base.gameObject != null))
            {
                UnityEngine.Object.Destroy(base.gameObject);
            }
        }

        private float movementStart
        {
            get
            {
                return (this.textTime + this.stay);
            }
        }
    }
}

