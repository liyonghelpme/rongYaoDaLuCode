﻿namespace com.game.module.fight.arpg
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.effect;
    using com.game.module.fight.vo;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.skills;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class BuffController : MonoBehaviour
    {
        private string _attrStr = string.Empty;
        private ActionControler _meController;
        private TempVo _tempVo = new TempVo();
        public List<BuffVo> AbortDamageBuffList = new List<BuffVo>();
        private float accTime;
        public List<BuffVo> AddAttrBuffList = new List<BuffVo>();
        public Dictionary<uint, List<BuffVo>> AllBuffList = new Dictionary<uint, List<BuffVo>>();
        private ActionDisplay attackDisplay;
        public List<SimpleBuffVo> BuffNeedAddList = new List<SimpleBuffVo>();
        public List<SimpleBuffVo> BuffNeedRemoveList = new List<SimpleBuffVo>();
        public List<BuffVo> ChangeShapeBuffList = new List<BuffVo>();
        public List<BuffVo> ControllBuffList = new List<BuffVo>();
        private ControlVo controlVo = new ControlVo();
        private IdGeneralFactory idFactory = new IdGeneralFactory();
        private bool isDead;
        private bool IsPause;
        private NeedCacFlags needCacFlags = new NeedCacFlags();
        private Dictionary<int, int> nowAttacAddBuffList = new Dictionary<int, int>();
        private ControlVo preControlVo = new ControlVo();
        public List<BuffVo> RecorverBuffList = new List<BuffVo>();
        private List<BuffVo> waitRemoveBuffList = new List<BuffVo>();
        public List<SimpleBuffVo> waitSyncRemoveList = new List<SimpleBuffVo>();
        public Dictionary<uint, uint> workEffectFlag = new Dictionary<uint, uint>();
        public Dictionary<uint, EffectControlerII> workEffectMap = new Dictionary<uint, EffectControlerII>();

        public void AddBuff(List<SimpleBuffVo> addBuffList, ActionDisplay inAttackDisplay = null)
        {
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo>();
            foreach (SimpleBuffVo vo in addBuffList)
            {
                simpleBuffVoList.Add(new SimpleBuffVo(vo.BuffId, vo.BuffLevel, inAttackDisplay));
            }
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        public void AddBuff(List<PDamageBuff> addBuffList, ActionDisplay inAttackDisplay = null)
        {
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo>();
            foreach (PDamageBuff buff in addBuffList)
            {
                simpleBuffVoList.Add(new SimpleBuffVo(buff.buffId, buff.lvl, inAttackDisplay));
            }
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        public void AddBuff(int buffId, int buffLvl, ActionDisplay inAttackDisplay = null)
        {
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo> {
                new SimpleBuffVo((uint) buffId, (uint) buffLvl, inAttackDisplay)
            };
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        public void AddBuff(uint buffId, uint buffLvl, ActionDisplay inAttackDisplay = null)
        {
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo> {
                new SimpleBuffVo(buffId, buffLvl, inAttackDisplay)
            };
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        public void AddBuffBroadCast(List<SimpleBuffVo> simpleBuffVoList)
        {
            if (!this.isDead)
            {
                List<SimpleBuffVo> simpleBuffList = new List<SimpleBuffVo>();
                foreach (SimpleBuffVo vo in simpleBuffVoList)
                {
                    if (this.DoCheckBuffCanAdd(vo))
                    {
                        vo.syncUnikey = this.idFactory.GenId();
                        this.BuffNeedAddList.Add(vo);
                        simpleBuffList.Add(vo);
                    }
                }
                Singleton<BuffMode>.Instance.SyncBuffAdd(this.meController, simpleBuffList);
            }
        }

        public void AddBuffList(List<SimpleBuffVo> addBuffList, ActionDisplay inAttackDisplay = null)
        {
            Log.AI(base.gameObject, " AddBuffList " + addBuffList.Count);
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo>();
            foreach (SimpleBuffVo vo in addBuffList)
            {
                simpleBuffVoList.Add(new SimpleBuffVo(vo.BuffId, vo.BuffLevel, inAttackDisplay));
            }
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        public void AddBuffNoBroadCast(List<SimpleBuffVo> simpleBuffVoList)
        {
            foreach (SimpleBuffVo vo in simpleBuffVoList)
            {
                this.BuffNeedAddList.Add(vo);
            }
        }

        public void AddBuffNoBroadCast(uint buffId, uint buffLevel, int recvSyncUnikey, int value2 = 0, int buffVal1 = 0, int buffVal2 = 0, ActionDisplay attackDisplay = null)
        {
            if (!this.isDead)
            {
                SimpleBuffVo item = new SimpleBuffVo(buffId, buffLevel, attackDisplay) {
                    syncUnikey = recvSyncUnikey
                };
                item.SetHpRate(value2);
                item.val1 = buffVal1;
                item.val2 = buffVal2;
                this.BuffNeedAddList.Add(item);
            }
        }

        public void AddBuffWithAbortDamage(int buffId, int buffLevel, uint abortNum)
        {
            this.AddBuffWithAbortDamage(Convert.ToUInt32(buffId), Convert.ToUInt32(buffLevel), Convert.ToInt32(abortNum));
        }

        public void AddBuffWithAbortDamage(uint buffId, uint buffLevel, int abortNum)
        {
            SimpleBuffVo vo = new SimpleBuffVo(buffId, buffLevel, null) {
                val1 = abortNum
            };
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo> {
                vo
            };
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        public void AddBuffWithAttr(uint buffId, uint buffLevel, int type, int value)
        {
            SimpleBuffVo vo = new SimpleBuffVo(buffId, buffLevel, null) {
                val1 = type,
                val2 = value
            };
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo> {
                vo
            };
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        public void AddBuffWithHp(uint buffId, uint buffLevel, int hpValue)
        {
            SimpleBuffVo vo = new SimpleBuffVo(buffId, buffLevel, null);
            vo.SetHpRate(hpValue);
            List<SimpleBuffVo> simpleBuffVoList = new List<SimpleBuffVo> {
                vo
            };
            this.AddBuffBroadCast(simpleBuffVoList);
        }

        private List<BuffVo> BeenWorked(List<BuffVo> oldList)
        {
            List<BuffVo> list = new List<BuffVo>();
            foreach (BuffVo vo in oldList)
            {
                if (vo.isFirstEffect)
                {
                    list.Add(vo);
                }
            }
            return list;
        }

        public int CalcAbortDamage(int nowDamage)
        {
            int num = nowDamage;
            List<BuffVo> list = new List<BuffVo>();
            foreach (BuffVo vo in this.AbortDamageBuffList)
            {
                list.Add(vo);
            }
            for (int i = 0; (nowDamage > 0) && (i < list.Count); i++)
            {
                BuffVo removeBuffVo = list[i];
                if (removeBuffVo.abortDamage != 0)
                {
                    if (nowDamage <= removeBuffVo.abortDamage)
                    {
                        removeBuffVo.abortDamage -= nowDamage;
                        nowDamage = 0;
                    }
                    else
                    {
                        nowDamage -= removeBuffVo.abortDamage;
                        removeBuffVo.abortDamage = 0;
                    }
                    if (removeBuffVo.abortDamage == 0)
                    {
                        this.DoRemoveBuffBroadCast(removeBuffVo);
                    }
                }
                else
                {
                    Debug.LogError(string.Concat(new object[] { "This should never run here: [buffId]:", removeBuffVo.BuffId, " [buffLevel]:", removeBuffVo.BuffLevel }));
                }
            }
            return nowDamage;
        }

        private void CheckDifference2ControlVo(ControlVo pre, ControlVo now)
        {
            StateVo stateInfo = this.meController.Me.GetMeVoByType<BaseRoleVo>().stateInfo;
            if (pre.notBeenFrozing != now.notBeenFrozing)
            {
                if (now.notBeenFrozing == 0)
                {
                    this.meController.StatuController.StopAnimation();
                }
                else
                {
                    this.meController.StatuController.ResetAnimation();
                }
            }
            if (pre.canAttack != now.canAttack)
            {
                if (now.canAttack == 0)
                {
                    stateInfo.AddState(StateVo.CANT_ATTACK);
                }
                else
                {
                    stateInfo.RemoveState(StateVo.CANT_ATTACK);
                }
            }
            if (pre.canMove != now.canMove)
            {
                if (now.canMove == 0)
                {
                    stateInfo.AddState(StateVo.CANT_MOVE);
                    CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                }
                else
                {
                    stateInfo.RemoveState(StateVo.CANT_MOVE);
                    CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                }
            }
            if (now.canSkill == 0)
            {
                if (pre.canSkill != now.canSkill)
                {
                    stateInfo.AddState(StateVo.CANT_SKILL1);
                    stateInfo.AddState(StateVo.CANT_SKILL2);
                    stateInfo.AddState(StateVo.CANT_SKILL3);
                    stateInfo.AddState(StateVo.CANT_SKILL4);
                }
            }
            else
            {
                if (now.canSkill1 == 1)
                {
                    stateInfo.RemoveState(StateVo.CANT_SKILL1);
                }
                if (now.canSkill2 == 1)
                {
                    stateInfo.RemoveState(StateVo.CANT_SKILL2);
                }
                if (now.canSkill3 == 1)
                {
                    stateInfo.RemoveState(StateVo.CANT_SKILL3);
                }
                if (now.canSkill4 == 1)
                {
                    stateInfo.RemoveState(StateVo.CANT_SKILL4);
                }
            }
            if (now.canAddDebuff != pre.canAddDebuff)
            {
                if (now.canAddDebuff == 0)
                {
                    stateInfo.AddState(StateVo.CANT_ADD_DEBUFF);
                }
                else
                {
                    stateInfo.RemoveState(StateVo.CANT_ADD_DEBUFF);
                }
            }
            if (now.canBeat != pre.canBeat)
            {
                if (now.canBeat == 0)
                {
                    stateInfo.AddState(StateVo.CANT_BEAT);
                }
                else
                {
                    stateInfo.RemoveState(StateVo.CANT_BEAT);
                }
            }
            if (now.canHeal != pre.canHeal)
            {
                if (now.canHeal == 0)
                {
                    stateInfo.AddState(StateVo.CANT_HEAL);
                }
                else
                {
                    stateInfo.RemoveState(StateVo.CANT_HEAL);
                }
            }
            if (now.notBeenTemptation != pre.notBeenTemptation)
            {
                if (now.notBeenTemptation == 0)
                {
                    stateInfo.BeTemptated = true;
                    SkillUtil.StartTemptation(this.meController.Me.GetMeByType<ActionDisplay>(), this.attackDisplay);
                }
                else
                {
                    stateInfo.BeTemptated = false;
                    SkillUtil.EndTemptation(this.meController.Me.GetMeByType<ActionDisplay>());
                }
            }
            if (now.canSee != pre.canSee)
            {
                if (now.canSee == 0)
                {
                    stateInfo.AddState(StateVo.CANT_SEE);
                }
                else
                {
                    stateInfo.RemoveState(StateVo.CANT_SEE);
                }
            }
            if (((now.notBeenHit == 0) && (now.hitBuff != null)) && (now.hitBuff != pre.hitBuff))
            {
                if ((now.hitBuff.BuffTemp.value2 == 0) || (now.hitBuff.BuffTemp.value3 == 0))
                {
                    SkillUtil.StartFloat(this.meController, now.hitBuff.BuffTemp.value1 * 0.001f, now.hitBuff.BuffTemp.totalTime * 0.001f);
                }
                else
                {
                    SkillUtil.StartFloatAndDamageAgain(this.meController, now.hitBuff.BuffTemp.value1 * 0.001f, now.hitBuff.BuffTemp.totalTime * 0.001f, now.hitBuff.attackDisplay, (uint) now.hitBuff.BuffTemp.value2, now.hitBuff.BuffTemp.value3);
                }
            }
            if ((now.notBeenTaunt == 0) && (this.attackDisplay != null))
            {
                SkillUtil.StartTaunted(this.meController, this.attackDisplay);
            }
            else
            {
                SkillUtil.EndTauntedAndCancelAttack(this.meController);
            }
            if (now.notLifeProtect != pre.notLifeProtect)
            {
                if (now.notLifeProtect == 0)
                {
                    SkillUtil.StartHpProtected(this.meController, this._tempVo.lifeProType, this._tempVo.lifeProValue);
                }
                else
                {
                    SkillUtil.EndHpProtected(this.meController);
                }
            }
            if (now.notAddAtacDis != pre.notAddAtacDis)
            {
                if (now.notAddAtacDis == 0)
                {
                    SkillUtil.ExtendBullet(this.meController, (float) this._tempVo.addAttacDis);
                }
                else
                {
                    SkillUtil.CancelExtendBullet(this.meController);
                }
            }
            if (now.notBeenBati != pre.notBeenBati)
            {
                if (now.notBeenBati == 0)
                {
                    this.DoRemoveController();
                    SkillUtil.StartEndured(this.meController);
                }
                else
                {
                    SkillUtil.EndEndured(this.meController);
                }
            }
        }

        private void CopyControlVo(ControlVo vo1, ControlVo vo2)
        {
            vo1.canAttack = vo2.canAttack;
            vo1.canMove = vo2.canMove;
            vo1.canSkill = vo2.canSkill;
            vo1.canSkill1 = vo2.canSkill1;
            vo1.canSkill2 = vo2.canSkill2;
            vo1.canSkill3 = vo2.canSkill3;
            vo1.canSkill4 = vo2.canSkill4;
            vo1.canAddDebuff = vo2.canAddDebuff;
            vo1.canBeat = vo2.canBeat;
            vo1.canHeal = vo2.canHeal;
            vo1.canSee = vo2.canSee;
            vo1.notBeenFrozing = vo2.notBeenFrozing;
            vo1.notBeenHit = vo2.notBeenHit;
            vo1.notBeenTaunt = vo2.notBeenTaunt;
            vo1.notBeenTemptation = vo2.notBeenTemptation;
            vo1.notLifeProtect = vo2.notLifeProtect;
            vo1.notAddAtacDis = vo2.notAddAtacDis;
            vo1.notBeenBati = vo2.notBeenBati;
        }

        public BuffVo CreateANewBuff(SimpleBuffVo simpleBuff)
        {
            BuffVo vo = this.CreateANewBuff(simpleBuff.BuffId, simpleBuff.BuffLevel, simpleBuff.attackDisplay);
            vo.syncUnikey = simpleBuff.syncUnikey;
            return vo;
        }

        public BuffVo CreateANewBuff(uint buffId, uint level, ActionDisplay inActionDisplay)
        {
            return new BuffVo(buffId, level, this, inActionDisplay);
        }

        public void Dead()
        {
            if (!this.isDead)
            {
                this.DoRemoveAllBuffList();
                this.DoRemoveAllEffect();
                this.isDead = true;
            }
        }

        private void DoAddBuff(SimpleBuffVo simpleBuff)
        {
            if (BaseDataMgr.instance.GetSysBuffVo(simpleBuff.BuffId, simpleBuff.BuffLevel) == null)
            {
                Debug.Log("buff模版不存在:" + simpleBuff.BuffId.ToString() + " " + simpleBuff.BuffLevel.ToString());
            }
            else
            {
                BuffVo item = this.CreateANewBuff(simpleBuff);
                if (this.AllBuffList.ContainsKey(simpleBuff.UniKey))
                {
                    this.AllBuffList[simpleBuff.UniKey].Add(item);
                }
                else
                {
                    List<BuffVo> list2 = new List<BuffVo> {
                        item
                    };
                    this.AllBuffList.Add(simpleBuff.UniKey, list2);
                }
                switch (item.BuffTemp.type)
                {
                    case 1:
                        this.ControllBuffList.Add(item);
                        break;

                    case 2:
                        this.ChangeShapeBuffList.Add(item);
                        break;

                    case 3:
                        if ((item.BuffTemp.subtype == 2L) && (simpleBuff.val2 != 0))
                        {
                            string[] textArray1 = new string[] { "[[", simpleBuff.val1.ToString(), ",", simpleBuff.val2.ToString(), "]]" };
                            item.attrStr = string.Concat(textArray1);
                        }
                        this.AddAttrBuffList.Add(item);
                        break;

                    case 4:
                        if (item.BuffTemp.subtype == 4L)
                        {
                            item.abortDamage = simpleBuff.val1;
                        }
                        this.AbortDamageBuffList.Add(item);
                        break;

                    case 5:
                        this.RecorverBuffList.Add(item);
                        break;
                }
                if (simpleBuff.ownHpFlag)
                {
                    item.addHp = simpleBuff._hp;
                    item.addHpRate = 0f;
                }
                this.DoAddBuffEffect(item);
            }
        }

        private void DoAddBuffEffect(BuffVo buffVo)
        {
        }

        private void DoAddWaitBuffList()
        {
            foreach (SimpleBuffVo vo in this.BuffNeedAddList)
            {
                if (this.AllBuffList.ContainsKey(vo.UniKey))
                {
                    List<BuffVo> source = this.AllBuffList[vo.UniKey];
                    if ((source != null) && (source.Count > 0))
                    {
                        BuffVo removeBuff = source.First<BuffVo>();
                        switch (removeBuff.BuffTemp.overlying)
                        {
                            case 1:
                                this.DoAddBuff(vo);
                                break;

                            case 2:
                                this.DoRemoveBuff(removeBuff);
                                this.DoAddBuff(vo);
                                break;
                        }
                    }
                    else
                    {
                        this.DoAddBuff(vo);
                    }
                }
                else
                {
                    this.DoAddBuff(vo);
                }
            }
            this.BuffNeedAddList.Clear();
        }

        public bool DoCheckBuffCanAdd(SimpleBuffVo simpleBuff)
        {
            SysBuffVo sysBuffVo = BaseDataMgr.instance.GetSysBuffVo(simpleBuff.BuffId, simpleBuff.BuffLevel);
            if (sysBuffVo != null)
            {
                if ((this.controlVo.notBeenBati == 0) && (sysBuffVo.logic_type == 1))
                {
                    return false;
                }
                return true;
            }
            Debug.LogError(string.Concat(new object[] { "找不到buff配表 ", simpleBuff.BuffId, " ", simpleBuff.BuffLevel }));
            return false;
        }

        private void DoCheckUnRemovedBuff()
        {
            foreach (uint num in this.workEffectMap.Keys)
            {
                if ((this.workEffectMap[num] != null) && (!this.workEffectFlag.ContainsKey(num) || (this.workEffectFlag[num] == 0)))
                {
                    this.workEffectMap[num].Clear();
                    this.workEffectMap[num] = null;
                }
            }
        }

        private void DoHandleBuffLoop()
        {
            float accTime = this.accTime;
            List<List<BuffVo>> list = this.AllBuffList.Values.ToList<List<BuffVo>>();
            this.waitRemoveBuffList.Clear();
            foreach (List<BuffVo> list2 in list)
            {
                foreach (BuffVo vo in list2)
                {
                    vo.firstEffectTime -= accTime;
                    if (vo.firstEffectTime <= 0f)
                    {
                        if (!vo.isFirstEffect)
                        {
                            this.DoWorkBuffEffect(vo);
                            vo.intervalLeftTime = ((float) vo.BuffTemp.intervalTime) / 1000f;
                            vo.totalTime = (vo.BuffTemp.totalTime - vo.BuffTemp.firstEffectTime) / 1000f;
                            this.DoWorkBuff(vo, true);
                            vo.isFirstEffect = true;
                        }
                        else
                        {
                            vo.totalTime -= accTime;
                            if (vo.totalTime <= 0f)
                            {
                                this.waitRemoveBuffList.Add(vo);
                            }
                            else
                            {
                                vo.intervalLeftTime -= accTime;
                                if (vo.intervalLeftTime <= 0f)
                                {
                                    vo.intervalLeftTime = ((float) vo.BuffTemp.intervalTime) / 1000f;
                                    this.DoWorkBuff(vo, false);
                                }
                            }
                        }
                    }
                }
            }
            foreach (BuffVo vo2 in this.waitRemoveBuffList)
            {
                this.DoRemoveBuff(vo2);
            }
        }

        private void DoReCaculateProperty()
        {
            if (this.needCacFlags.NeedCacAttacedAddBuffList)
            {
                this.nowAttacAddBuffList.Clear();
                foreach (List<BuffVo> list in this.AllBuffList.Values)
                {
                    foreach (BuffVo vo in this.BeenWorked(list))
                    {
                        if (BuffUtil.CheckRecacAttacAddBuffList(vo.BuffTemp.attackedAddBuffList))
                        {
                            string[] strArray = BuffUtil.ParserAttackedAddBuffList(vo.BuffTemp.attackedAddBuffList);
                            for (int i = 0; i < strArray.Length; i++)
                            {
                                char[] separator = new char[] { ',' };
                                string[] strArray2 = strArray[i].Split(separator);
                                if (strArray2.Length >= 3)
                                {
                                    int key = (int) SimpleBuffVo.getUnikey((uint) int.Parse(strArray2[0]), (uint) int.Parse(strArray2[1]));
                                    if (!this.nowAttacAddBuffList.ContainsKey(key))
                                    {
                                        this.nowAttacAddBuffList[key] = int.Parse(strArray2[2]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (this.needCacFlags.NeedCacCol)
            {
                this.CopyControlVo(this.preControlVo, this.controlVo);
                this.controlVo.ResetVoAllTrue();
                foreach (BuffVo vo2 in this.BeenWorked(this.ControllBuffList))
                {
                    switch (vo2.BuffTemp.subtype)
                    {
                        case 1:
                            this.controlVo.canAttack = 0;
                            this.controlVo.canMove = 0;
                            this.controlVo.canSkill = 0;
                            break;

                        case 2:
                            this.controlVo.canAttack = 0;
                            this.controlVo.canMove = 0;
                            this.controlVo.canSkill = 0;
                            this.controlVo.notBeenHit = 0;
                            this.controlVo.hitBuff = vo2;
                            break;

                        case 3:
                            this.controlVo.canSkill2 = 0;
                            this.controlVo.canSkill3 = 0;
                            this.controlVo.canSkill4 = 0;
                            this.controlVo.notBeenTaunt = 0;
                            if (vo2.attackDisplay != null)
                            {
                                this.attackDisplay = vo2.attackDisplay;
                            }
                            break;

                        case 5:
                            this.controlVo.canMove = 0;
                            break;

                        case 9:
                            this.controlVo.canAttack = 0;
                            this.controlVo.canMove = 0;
                            this.controlVo.canSkill = 0;
                            this.controlVo.notBeenFrozing = 0;
                            break;

                        case 10:
                            this.controlVo.notBeenTemptation = 0;
                            if (vo2.attackDisplay != null)
                            {
                                this.attackDisplay = vo2.attackDisplay;
                            }
                            break;

                        case 11:
                            this.controlVo.notLifeProtect = 0;
                            this._tempVo.lifeProType = vo2.BuffTemp.value1;
                            this._tempVo.lifeProValue = vo2.BuffTemp.value2;
                            break;

                        case 12:
                            this.controlVo.notAddAtacDis = 0;
                            this._tempVo.addAttacDis = vo2.BuffTemp.value1;
                            break;

                        case 13:
                            this.controlVo.notBeenBati = 0;
                            break;
                    }
                }
                this.CheckDifference2ControlVo(this.preControlVo, this.controlVo);
            }
            if (this.needCacFlags.NeedCacShape)
            {
                int num3 = -1;
                BuffVo vo3 = null;
                foreach (BuffVo vo4 in this.BeenWorked(this.ChangeShapeBuffList))
                {
                    if (vo4.BuffTemp.value2 > num3)
                    {
                        num3 = vo4.BuffTemp.value2;
                        vo3 = vo4;
                    }
                }
                if (num3 == -1)
                {
                    this.meController.SkillController.DoRoleModelChange(0);
                }
                else
                {
                    this.meController.SkillController.DoRoleModelChange((uint) vo3.BuffTemp.value1);
                }
            }
            if (this.needCacFlags.NeedCacAttrPro)
            {
                this._attrStr = string.Empty;
                foreach (BuffVo vo5 in this.BeenWorked(this.AddAttrBuffList))
                {
                    this._attrStr = this._attrStr + vo5.attrStr;
                }
                this._meController.GetMeVo().UpdateAttr();
            }
        }

        public void DoRemoveAllBuffList()
        {
            if (!this.isDead)
            {
                this.ResetControlFlags();
                List<BuffVo> list = new List<BuffVo>();
                foreach (List<BuffVo> list2 in this.AllBuffList.Values)
                {
                    foreach (BuffVo vo in list2)
                    {
                        list.Add(vo);
                    }
                }
                foreach (BuffVo vo2 in list)
                {
                    this.DoRemoveBuffBroadCast(vo2);
                }
                this.AllBuffList.Clear();
                this.RecorverBuffList.Clear();
                this.ControllBuffList.Clear();
                this.AbortDamageBuffList.Clear();
                this.ChangeShapeBuffList.Clear();
                this.AddAttrBuffList.Clear();
                this.BuffNeedAddList.Clear();
                this.BuffNeedRemoveList.Clear();
                this.workEffectFlag.Clear();
                this.DoReCaculateProperty();
            }
        }

        public void DoRemoveAllEffect()
        {
            foreach (uint num in this.workEffectMap.Keys)
            {
                if (this.workEffectMap[num] != null)
                {
                    this.workEffectMap[num].Clear();
                }
            }
            this.workEffectMap.Clear();
        }

        private void DoRemoveBuff(BuffVo removeBuff)
        {
            if (this.AllBuffList.ContainsKey(removeBuff.UniKey))
            {
                this.AllBuffList[removeBuff.UniKey].Remove(removeBuff);
            }
            if (removeBuff != null)
            {
                this.DoRemoveBuffEffect(removeBuff);
            }
            List<BuffVo> controllBuffList = null;
            switch (removeBuff.BuffTemp.type)
            {
                case 1:
                    this.needCacFlags.NeedCacCol = true;
                    controllBuffList = this.ControllBuffList;
                    break;

                case 2:
                    this.needCacFlags.NeedCacShape = true;
                    controllBuffList = this.ChangeShapeBuffList;
                    break;

                case 3:
                    this.needCacFlags.NeedCacAttrPro = true;
                    controllBuffList = this.AddAttrBuffList;
                    break;

                case 4:
                    controllBuffList = this.AbortDamageBuffList;
                    break;

                case 5:
                    controllBuffList = this.RecorverBuffList;
                    break;
            }
            if (BuffUtil.CheckRecacAttacAddBuffList(removeBuff.BuffTemp.attackedAddBuffList))
            {
                this.needCacFlags.NeedCacAttacedAddBuffList = true;
            }
            controllBuffList.Remove(removeBuff);
            removeBuff = null;
        }

        public void DoRemoveBuffBroadCast(BuffVo removeBuffVo)
        {
            List<SimpleBuffVo> list = new List<SimpleBuffVo>();
            SimpleBuffVo item = new SimpleBuffVo(removeBuffVo.BuffId, removeBuffVo.BuffLevel, null) {
                syncUnikey = removeBuffVo.syncUnikey
            };
            this.waitSyncRemoveList.Add(item);
            this.DoRemoveBuff(removeBuffVo);
        }

        private void DoRemoveBuffEffect(BuffVo buffVo)
        {
            if ((!this.isDead && (buffVo.BuffTemp.buffEffectId != 0)) && buffVo.isFirstEffect)
            {
                Dictionary<uint, uint> dictionary;
                uint num;
                num = dictionary[num];
                (dictionary = this.workEffectFlag)[num = buffVo.BuffId] = num - 1;
                if (((this.workEffectFlag[buffVo.BuffId] == 0) && this.workEffectMap.ContainsKey(buffVo.BuffId)) && (this.workEffectMap[buffVo.BuffId] != null))
                {
                    if ((buffVo.BuffTemp.removeEffectId != 0) && (this.workEffectMap[buffVo.BuffId] != null))
                    {
                        EffectMgr.Instance.CreateBuffEffect(buffVo.BuffTemp.removeEffectId.ToString(), this._meController.Me.GoBase.gameObject, null, buffVo, true, false, null);
                    }
                    this.workEffectMap[buffVo.BuffId].Clear();
                    this.workEffectMap[buffVo.BuffId] = null;
                }
            }
        }

        public void DoRemoveBuffList()
        {
            List<BuffVo> list = new List<BuffVo>();
            foreach (SimpleBuffVo vo in this.BuffNeedRemoveList)
            {
                if (this.AllBuffList.ContainsKey(vo.UniKey))
                {
                    List<BuffVo> list2 = this.AllBuffList[vo.UniKey];
                    foreach (BuffVo vo2 in list2)
                    {
                        list.Add(vo2);
                    }
                    this.AllBuffList.Remove(vo.UniKey);
                }
            }
            this.BuffNeedRemoveList.Clear();
            foreach (BuffVo vo3 in list)
            {
                this.DoRemoveBuffBroadCast(vo3);
            }
        }

        public void DoRemoveBuffNoBroadCast(SimpleBuffVo simpleBuff)
        {
            if (this.AllBuffList.ContainsKey(simpleBuff.UniKey))
            {
                List<BuffVo> list = this.AllBuffList[simpleBuff.UniKey];
                for (int i = 0; i < list.Count; i++)
                {
                    if (simpleBuff.syncUnikey == list[i].syncUnikey)
                    {
                        this.DoRemoveBuff(list[i]);
                        break;
                    }
                }
            }
        }

        public void DoRemoveController()
        {
            List<BuffVo> list = new List<BuffVo>();
            foreach (List<BuffVo> list2 in this.AllBuffList.Values)
            {
                foreach (BuffVo vo in list2)
                {
                    SysBuffVo buffTemp = vo.BuffTemp;
                    if ((buffTemp.logic_type == 1) && ((buffTemp.type != 1L) || (buffTemp.subtype != 2L)))
                    {
                        list.Add(vo);
                    }
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                this.BuffNeedRemoveList.Add(list[i]);
            }
        }

        private void DoWorkAddHp(int addHp, BuffVo buffVo)
        {
            if (addHp > 0)
            {
                this.meController.Me.GetMeVoByType<BaseRoleVo>().AddHp((float) addHp);
            }
            else
            {
                this.meController.Me.GetMeVoByType<BaseRoleVo>().ReduceHp((float) -addHp);
            }
        }

        private void DoWorkAddHpRe(float addHpRate, BuffVo buffVo)
        {
            BaseRoleVo vo = this.meController.Me.GetVo() as BaseRoleVo;
            int curHp = (int) vo.CurHp;
            float num2 = addHpRate;
            int addHp = (int) (vo.Hp * num2);
            this.DoWorkAddHp(addHp, buffVo);
        }

        private void DoWorkBuff(BuffVo buffVo, bool firstFlag)
        {
            if (buffVo.addHp != 0)
            {
                this.DoWorkAddHp(buffVo.addHp, buffVo);
            }
            if (buffVo.addHpRate != 0f)
            {
                this.DoWorkAddHpRe(buffVo.addHpRate, buffVo);
            }
            switch (buffVo.BuffTemp.type)
            {
                case 1:
                    this.needCacFlags.NeedCacCol = true;
                    break;

                case 2:
                    this.needCacFlags.NeedCacShape = true;
                    break;

                case 3:
                    this.needCacFlags.NeedCacAttrPro = true;
                    if ((buffVo.BuffTemp.value1 != 0) && !firstFlag)
                    {
                        buffVo.attrStr = buffVo.attrStr + buffVo.BuffTemp.attrArray;
                    }
                    break;
            }
            if (BuffUtil.CheckRecacAttacAddBuffList(buffVo.BuffTemp.attackedAddBuffList))
            {
                this.needCacFlags.NeedCacAttacedAddBuffList = true;
            }
        }

        private void DoWorkBuffEffect(BuffVo buffVo)
        {
            if (!this.isDead && (buffVo.BuffTemp.buffEffectId != 0))
            {
                uint num;
                if (!this.workEffectFlag.ContainsKey(buffVo.BuffId))
                {
                    this.workEffectFlag[buffVo.BuffId] = 0;
                }
                if (this.workEffectFlag[buffVo.BuffId] == 0)
                {
                    Dictionary<uint, uint> dictionary;
                    num = dictionary[num];
                    (dictionary = this.workEffectFlag)[num = buffVo.BuffId] = num + 1;
                    if (!this.workEffectMap.ContainsKey(buffVo.BuffId) || (this.workEffectMap[buffVo.BuffId] == null))
                    {
                        Vector3 buffPosition = EffectMgr.GetBuffPosition(this.meController.Me, buffVo.BuffTemp.effectPosition);
                        EffectMgr.Instance.CreateBuffEffect(buffVo.BuffTemp.buffEffectId.ToString(), this._meController.Me.GoBase.gameObject, buffPosition, null, buffVo, false, false, null);
                    }
                }
                else
                {
                    Dictionary<uint, uint> dictionary2;
                    num = dictionary2[num];
                    (dictionary2 = this.workEffectFlag)[num = buffVo.BuffId] = num + 1;
                }
            }
        }

        public void EnterMap()
        {
        }

        public List<SimpleBuffVo> GetAttacAddBuffList()
        {
            List<SimpleBuffVo> list = new List<SimpleBuffVo>();
            foreach (int num in this.nowAttacAddBuffList.Keys)
            {
                KeyValuePair<uint, uint> pair = SimpleBuffVo.ParseUnikey(num);
                SimpleBuffVo item = new SimpleBuffVo(pair.Key, pair.Value, null) {
                    attcedAddRate = this.nowAttacAddBuffList[num]
                };
                list.Add(item);
            }
            return list;
        }

        public string GetAttrStr()
        {
            return this._attrStr;
        }

        private void KeyDown()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                if (!(this.meController.Me is PlayerDisplay))
                {
                    return;
                }
                this.AddBuffWithAttr(0xa2c2c, 1, 20, 0x186a0);
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                if (!(this.meController.Me is PlayerDisplay))
                {
                    return;
                }
                List<SimpleBuffVo> addBuffList = new List<SimpleBuffVo> {
                    new SimpleBuffVo(0xa2c2e, 1, null)
                };
                this.AddBuffList(addBuffList, null);
            }
            if (Input.GetKeyDown(KeyCode.N))
            {
                List<SimpleBuffVo> list2 = new List<SimpleBuffVo> {
                    new SimpleBuffVo(7, 1, null)
                };
                this.AddBuffList(list2, null);
            }
        }

        public void LeaveMap()
        {
        }

        public int Max(int a, int b)
        {
            if (a < b)
            {
                return b;
            }
            return a;
        }

        public int Min(int a, int b)
        {
            if (a < b)
            {
                return a;
            }
            return b;
        }

        public void OffLine()
        {
        }

        private void OnDestroy()
        {
            this.DoRemoveAllEffect();
        }

        public void OnLine()
        {
        }

        public void Reborn()
        {
            this.isDead = false;
        }

        public void RemoveAllBuff()
        {
            this.RemoveAllBuffList();
        }

        public void RemoveAllBuffList()
        {
            this.needCacFlags.NeedRemoveAllBuffs = true;
        }

        public void RemoveAllDebuff()
        {
            List<BuffVo> list = new List<BuffVo>();
            foreach (List<BuffVo> list2 in this.AllBuffList.Values)
            {
                foreach (BuffVo vo in list2)
                {
                    if (vo.BuffTemp.isDebuff == 1)
                    {
                        list.Add(vo);
                    }
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                this.DoRemoveBuffBroadCast(list[i]);
            }
        }

        public void RemoveBuffList(List<SimpleBuffVo> buffList)
        {
            foreach (SimpleBuffVo vo in buffList)
            {
                this.BuffNeedRemoveList.Add(new SimpleBuffVo(vo.BuffId, vo.BuffLevel, null));
            }
        }

        public void RemoveBuffList(List<PDamageBuff> buffList)
        {
            foreach (PDamageBuff buff in buffList)
            {
                this.BuffNeedRemoveList.Add(new SimpleBuffVo(buff.buffId, buff.lvl, null));
            }
        }

        public void RemoveControlBuffList(List<KeyValuePair<int, int>> expList = null)
        {
            List<BuffVo> list = new List<BuffVo>();
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            int num = 0x3e8;
            foreach (KeyValuePair<int, int> pair in expList)
            {
                dictionary[(pair.Key * num) + pair.Value] = 1;
            }
            foreach (List<BuffVo> list2 in this.AllBuffList.Values)
            {
                foreach (BuffVo vo in list2)
                {
                    SysBuffVo buffTemp = vo.BuffTemp;
                    int key = (buffTemp.type * num) + buffTemp.subtype;
                    if ((buffTemp.logic_type == 1) && !dictionary.ContainsKey(key))
                    {
                        list.Add(vo);
                    }
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                this.DoRemoveBuffBroadCast(list[i]);
            }
        }

        public void RemoveDamageBuffList(List<PDamageBuff> damangeBuffList)
        {
            foreach (PDamageBuff buff in damangeBuffList)
            {
                this.BuffNeedRemoveList.Add(new SimpleBuffVo(buff.buffId, buff.lvl, null));
            }
        }

        private void ResetControlFlags()
        {
            this.needCacFlags.ResetAll(false);
        }

        private void ShowAllBuffList()
        {
            string str = string.Empty;
            foreach (List<BuffVo> list2 in this.AllBuffList.Values.ToList<List<BuffVo>>())
            {
                foreach (BuffVo vo in list2)
                {
                    string[] textArray1 = new string[] { str, " |", vo.BuffId.ToString(), "+", vo.BuffLevel.ToString() };
                    str = string.Concat(textArray1);
                }
            }
            Debug.Log("BuffList:" + str);
        }

        private void Start()
        {
            this.isDead = false;
            this.needCacFlags.ResetAll(false);
        }

        private void Update()
        {
            if (this.isDead)
            {
                this.DoRemoveAllEffect();
            }
            if (!this.IsPause && !this.isDead)
            {
                this.accTime += Time.deltaTime;
                if (this.accTime >= 0.1f)
                {
                    if (!this.needCacFlags.NeedRemoveAllBuffs)
                    {
                        this.DoRemoveBuffList();
                        this.DoAddWaitBuffList();
                        this.DoHandleBuffLoop();
                    }
                    else
                    {
                        this.DoRemoveAllBuffList();
                    }
                    this.DoReCaculateProperty();
                    this.ResetControlFlags();
                    this.accTime = 0f;
                    Singleton<BuffMode>.Instance.SyncBuffRemove(this.meController, this.waitSyncRemoveList);
                    this.waitSyncRemoveList.Clear();
                    this.DoCheckUnRemovedBuff();
                }
            }
        }

        public void WorkBuffEffectSync(IEnumerable<SimpleBuffVo> list)
        {
        }

        public void WorkRemoveEffectSync(IEnumerable<SimpleBuffVo> list)
        {
        }

        public ActionControler meController
        {
            get
            {
                return this._meController;
            }
            set
            {
                this._meController = value;
            }
        }

        public class ControlVo
        {
            public int canAddDebuff = 1;
            public int canAttack = 1;
            public int canBeat = 1;
            public int canHeal = 1;
            public int canMove = 1;
            public int canSee = 1;
            public int canSkill = 1;
            public int canSkill1 = 1;
            public int canSkill2 = 1;
            public int canSkill3 = 1;
            public int canSkill4 = 1;
            public BuffVo hitBuff;
            public int notAddAtacDis = 1;
            public int notBeenBati = 1;
            public int notBeenFrozing = 1;
            public int notBeenHit = 1;
            public int notBeenTaunt = 1;
            public int notBeenTemptation = 1;
            public int notLifeProtect = 1;

            public ControlVo()
            {
                this.ResetVoAllTrue();
            }

            public void ResetVoAllTrue()
            {
                this.notBeenFrozing = 1;
                this.canMove = 1;
                this.canSkill = 1;
                this.canAttack = 1;
                this.canSkill = 1;
                this.canSkill1 = 1;
                this.canSkill2 = 1;
                this.canSkill3 = 1;
                this.canSkill4 = 1;
                this.canHeal = 1;
                this.canSee = 1;
                this.canAddDebuff = 1;
                this.canBeat = 1;
                this.notBeenHit = 1;
                this.notBeenTaunt = 1;
                this.hitBuff = null;
                this.notBeenTemptation = 1;
                this.notLifeProtect = 1;
                this.notAddAtacDis = 1;
                this.notBeenBati = 1;
            }
        }

        public class IdGeneralFactory
        {
            private int _id;

            public int GenId()
            {
                return ++this._id;
            }
        }

        public class NeedCacFlags
        {
            public bool NeedCacAttacedAddBuffList = true;
            public bool NeedCacAttrPro = true;
            public bool NeedCacCol = true;
            public bool NeedCacShape = true;
            public bool NeedRemoveAllBuffs = true;

            public void ResetAll(bool flag)
            {
                this.NeedRemoveAllBuffs = flag;
                this.NeedCacAttrPro = flag;
                this.NeedCacCol = flag;
                this.NeedCacShape = flag;
                this.NeedCacAttrPro = flag;
                this.NeedCacAttacedAddBuffList = flag;
            }
        }

        public class TempVo
        {
            public int addAttacDis;
            public int lifeProType;
            public int lifeProValue;
        }
    }
}

