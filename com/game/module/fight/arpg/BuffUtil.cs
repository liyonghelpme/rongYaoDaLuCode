﻿namespace com.game.module.fight.arpg
{
    using com.game.data;
    using com.game.manager;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class BuffUtil
    {
        public static bool CheckRecacAttacAddBuffList(string tmpStr)
        {
            string[] strArray = ParserAttackedAddBuffList(tmpStr);
            return ((strArray.Length >= 1) && !strArray[0].Equals("[]"));
        }

        public static float GetChangeBuffCd(List<PDamageBuff> buffList, float defaultCd)
        {
            string str = string.Empty;
            foreach (PDamageBuff buff in buffList)
            {
                SysBuffVo sysBuffVo = BaseDataMgr.instance.GetSysBuffVo(buff.buffId, buff.lvl);
                if (sysBuffVo != null)
                {
                    if (sysBuffVo.type == 2)
                    {
                        return (float) (sysBuffVo.totalTime - sysBuffVo.firstEffectTime);
                    }
                }
                else
                {
                    Debug.LogError(string.Concat(new object[] { "没有这个buff配表", buff.buffId, " ", buff.lvl }));
                }
                str = str + " " + buff.buffId.ToString();
            }
            Debug.LogError("can't find the change shape buff " + str);
            return defaultCd;
        }

        public static float GetChangeBuffId(List<PDamageBuff> buffList)
        {
            return GetChangeBuffCd(buffList, 20f);
        }

        public static string[] ParserAttackedAddBuffList(string tmpStr)
        {
            tmpStr = tmpStr.Replace("[[", string.Empty);
            tmpStr = tmpStr.Replace("]]", string.Empty);
            tmpStr = tmpStr.Replace("],[", "|");
            tmpStr = tmpStr.Replace(" ", string.Empty);
            char[] separator = new char[] { '|' };
            return tmpStr.Split(separator);
        }
    }
}

