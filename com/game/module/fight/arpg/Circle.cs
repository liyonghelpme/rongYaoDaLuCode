﻿namespace com.game.module.fight.arpg
{
    using System;
    using UnityEngine;

    public class Circle : Shape
    {
        protected int _prevDetail;
        protected float _prevRadius;
        public int detail = 0x20;
        public static readonly int MIN_DETAIL = 3;
        public float radius = 10f;

        protected override void CreateIB()
        {
            int numTriangles = this.numTriangles;
            base._triangles = new int[3 * numTriangles];
            int num2 = 0;
            for (int i = 0; i < numTriangles; i++)
            {
                base._triangles[num2++] = 0;
                base._triangles[num2++] = i + 1;
                base._triangles[num2++] = i + 2;
            }
        }

        protected override void CreateVB()
        {
            int numVexs = this.numVexs;
            int num2 = numVexs - 1;
            base._vertices = new Vector3[numVexs];
            base._vertices[0] = new Vector3(0f, 0f, 0f);
            base._uvs = new Vector2[numVexs];
            base._uvs[0] = new Vector2(0.5f, 0.5f);
            for (int i = 1; i < numVexs; i++)
            {
                float f = this.GetDegreeByTriangleIndex(i) * 0.01745329f;
                float num5 = Mathf.Sin(f);
                float num6 = Mathf.Cos(f);
                base._vertices[i] = new Vector3(this.radius * num5, 0f, this.radius * num6);
                base._uvs[i] = new Vector2 { x = 0.5f + (0.5f * num5), y = 0.5f + (0.5f * num6) };
            }
        }

        protected virtual float GetDegreeByTriangleIndex(int idx)
        {
            float num = 360f / ((float) this.detail);
            return ((idx - 1) * num);
        }

        protected override bool freshMesh
        {
            get
            {
                if (this.detail < MIN_DETAIL)
                {
                    this.detail = MIN_DETAIL;
                }
                if (this._prevDetail != this.detail)
                {
                    this._prevDetail = this.detail;
                    return true;
                }
                if (this.radius < 0f)
                {
                    this.radius *= -1f;
                }
                else if (this.radius == 0f)
                {
                    this.radius = 1f;
                }
                if (Mathf.Abs((float) (this._prevRadius - this.radius)) > 0.01)
                {
                    this._prevRadius = this.radius;
                    return true;
                }
                return false;
            }
        }

        protected int numTriangles
        {
            get
            {
                return this.detail;
            }
        }

        protected int numVexs
        {
            get
            {
                return (this.detail + 2);
            }
        }

        public override ShapeType shapeType
        {
            get
            {
                return ShapeType.CIRCLE;
            }
        }
    }
}

