﻿namespace com.game.module.fight.arpg
{
    using com.game;
    using com.game.data;
    using com.game.module.Arena;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.effect;
    using com.game.module.fight.vo;
    using com.game.module.hud;
    using com.game.module.Role;
    using com.game.module.WiFiPvP;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.consts;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.display.vo;
    using com.u3d.bases.skills;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public sealed class DamageCheck
    {
        [CompilerGenerated]
        private static Func<PDamage, long> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<PDamage, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<PlayerDisplay, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<PlayerDisplay, BuffController> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<KV, float> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<KV, float> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<SimpleBuffVo, <>__AnonType1<SimpleBuffVo, int>> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<<>__AnonType1<SimpleBuffVo, int>, bool> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<<>__AnonType1<SimpleBuffVo, int>, SimpleBuffVo> <>f__am$cacheA;
        public static readonly DamageCheck Instance;
        public SelfInjuryDelegate SelfInjuryGuideCallback;

        static DamageCheck()
        {
            if (Instance == null)
            {
            }
            Instance = new DamageCheck();
        }

        private static void AddBuffNow(ActionDisplay ad, ActionDisplay enemyDisplay, List<PDamageBuff> dmgBuffList)
        {
            enemyDisplay.Controller.buffController.AddBuff(dmgBuffList, ad);
        }

        public static void AddBuffToMySide(SysSkillBaseVo skillvo, ActionDisplay meDisplay, IEnumerable<ActionDisplay> targetList)
        {
            List<PDamageBuff> damageBuffListFromString = StringUtils.GetDamageBuffListFromString(skillvo.buff_list);
            Transform ownerTransform = meDisplay.GoBase.transform;
            int num = skillvo.target_num;
            IEnumerator<ActionDisplay> enumerator = targetList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ActionDisplay current = enumerator.Current;
                    GameObject goBase = current.GoBase;
                    if (goBase != null)
                    {
                        float boundDistance = current.BoundDistance;
                        if (IsSkillCovered3D(skillvo, ownerTransform, goBase.transform.position, boundDistance) && (num > 0))
                        {
                            num--;
                            current.Controller.buffController.AddBuff(damageBuffListFromString, null);
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static string AddEffectInPvp(MonsterVo vo)
        {
            int type = vo.MonsterVO.type;
            int id = vo.MonsterVO.id;
            string str = string.Empty;
            switch (type)
            {
                case 3:
                    switch (id)
                    {
                        case 0xf4259:
                            if (vo.IsEmptyHp)
                            {
                                EffectMgr.Instance.RemoveSceneEffect("20041");
                                return "20040";
                            }
                            if (!vo.IsFullHp)
                            {
                                return str;
                            }
                            return "20041";

                        case 0xf425a:
                            if (vo.IsEmptyHp)
                            {
                                EffectMgr.Instance.RemoveSceneEffect("20044");
                                return "20043";
                            }
                            if (!vo.IsFullHp)
                            {
                                return str;
                            }
                            return "20044";
                    }
                    return str;

                case 4:
                    return str;

                case 5:
                    switch (id)
                    {
                        case 0x3e9008:
                            if (vo.IsEmptyHp)
                            {
                                EffectMgr.Instance.RemoveSceneEffect("20046");
                                return "20042";
                            }
                            if (!vo.IsFullHp)
                            {
                                return str;
                            }
                            return "20046";

                        case 0x3e9009:
                            if (vo.IsEmptyHp)
                            {
                                EffectMgr.Instance.RemoveSceneEffect("20047");
                                return "20045";
                            }
                            if (!vo.IsFullHp)
                            {
                                return str;
                            }
                            return "20047";
                    }
                    return str;
            }
            return str;
        }

        public static string AddEffectInPvp(ActionDisplay enemyDisplay)
        {
            MonsterVo meVoByType = enemyDisplay.GetMeVoByType<MonsterVo>();
            if (meVoByType == null)
            {
                return string.Empty;
            }
            return AddEffectInPvp(meVoByType);
        }

        private static void CheckAddBuffIfEnemyDies(BaseDisplay baseDisplay, SysSkillBaseVo skillVo, IEnumerable<PDamage> damageList)
        {
            if (skillVo.killing_feedback.Length >= "[[1,2,3,[4,5]]]".Length)
            {
                if (<>f__am$cache3 == null)
                {
                    <>f__am$cache3 = x => x.hp == 0;
                }
                int cnt = damageList.Count<PDamage>(<>f__am$cache3);
                if (cnt > 0)
                {
                    IEnumerable<AddBuffEvent> addBuffEventList = GetAddBuffEventList(skillVo.killing_feedback);
                    DoHandleBuffEvent(baseDisplay, addBuffEventList, cnt);
                }
            }
        }

        private static void CheckAddBuffIfEnemyHurts(BaseDisplay baseDisplay, SysSkillBaseVo skillVo, IEnumerable<PDamage> damageList)
        {
            if (skillVo.buff_list_to_self_after_damage.Length >= "[[1,2,3,[4,5]]]".Length)
            {
                int cnt = damageList.Count<PDamage>();
                if (cnt != 0)
                {
                    IEnumerable<AddBuffEvent> addBuffEventList = GetAddBuffEventList(skillVo.buff_list_to_self_after_damage);
                    DoHandleBuffEvent(baseDisplay, addBuffEventList, cnt);
                }
            }
        }

        private static void CheckAddHpIfEnemyHurts(BaseDisplay baseDisplay, SysSkillBaseVo skillVo, ICollection<PDamage> damageList)
        {
            if ((skillVo.subtype == 0x12) && (skillVo.param_list.Length >= "[1]".Length))
            {
                int[] arrayStringToInt = StringUtils.GetArrayStringToInt(skillVo.param_list);
                int num = arrayStringToInt[0];
                if (<>f__am$cache2 == null)
                {
                    <>f__am$cache2 = x => (long) x.dmg;
                }
                int hpValue = Convert.ToInt32((float) ((damageList.Sum<PDamage>(<>f__am$cache2) * num) * 0.001f));
                if (arrayStringToInt.Length >= 3)
                {
                    BuffController buffController = baseDisplay.Controller.buffController;
                    if (buffController != null)
                    {
                        uint buffId = Convert.ToUInt32(arrayStringToInt[1]);
                        uint buffLevel = Convert.ToUInt32(arrayStringToInt[2]);
                        buffController.AddBuffWithHp(buffId, buffLevel, hpValue);
                    }
                }
                else
                {
                    baseDisplay.GetMeVoByType<BaseRoleVo>().AddHp((float) hpValue);
                }
            }
        }

        public bool CheckMeInjured(ActionControler ac, SysSkillBaseVo skillVo, Vector3 effectPos, Transform ownerTransform, IEnumerable<ActionDisplay> enemyDisplays, int checkedTime, ref DamageCheckOptions opt, int index = 0, bool needSyn = false, int hurtCount = 0, int ratio = 0x3e8)
        {
            PlayerVo meVoByType = ac.Me.GetMeVoByType<PlayerVo>();
            if (meVoByType.IsEmptyHp)
            {
                return false;
            }
            PlayerDisplay meByType = ac.Me.GetMeByType<PlayerDisplay>();
            int maxDamageNum = (skillVo.target_num == 0) ? 0x3e7 : skillVo.target_num;
            List<PDamage> damageList = new List<PDamage>();
            List<PDamage> damageEventList = new List<PDamage>();
            bool result = false;
            int damageNum = 0;
            switch (skillVo.target_type)
            {
                case 1:
                case 2:
                {
                    List<ActionDisplay> targetDisplays = (skillVo.target_type != 1) ? SkillController.GetTeamDisplay() : new List<ActionDisplay> { meByType };
                    DoDamageCheckOnMySide(meByType, targetDisplays, skillVo, ref result, ref effectPos, ownerTransform, checkedTime, index, needSyn, maxDamageNum, ref damageList, ref damageNum, hurtCount);
                    break;
                }
                case 3:
                    this.DoDamageCheckOnEnemy(meByType, enemyDisplays, skillVo, ref result, ref effectPos, ownerTransform, checkedTime, index, needSyn, maxDamageNum, ref damageList, ref damageEventList, ref damageNum, hurtCount, ref opt, ratio);
                    break;
            }
            HandleSyncDamageData(skillVo, ac, needSyn, meVoByType, damageList);
            HandleFeedBackEvent(skillVo, meByType, damageEventList);
            if ((damageList.Count > 0) && (ac.AiController != null))
            {
                ac.AiController.SkilledSuccessfully(skillVo, damageEventList, Time.time);
            }
            return result;
        }

        public static bool CheckPlayerInjured(PlayerVo playerVo, SysSkillBaseVo skillVo, Vector3 effectPos, Transform ownerTransform, IEnumerable<ActionDisplay> enemyDisplays, int checkedTime, ref DamageCheckOptions opt, int index = 0, bool needSyn = false, int hurtCount = 0, int ratio = 0x3e8)
        {
            int maxDamageNum = (skillVo.target_num == 0) ? 0x3e7 : skillVo.target_num;
            List<PDamage> damageList = new List<PDamage>();
            bool result = false;
            int damageNum = 0;
            if (playerVo.IsEmptyHp)
            {
                return false;
            }
            switch (skillVo.target_type)
            {
                case 1:
                {
                    ActionDisplay meByType = playerVo.Controller.Me.GetMeByType<ActionDisplay>();
                    List<ActionDisplay> targetDisplays = new List<ActionDisplay> {
                        meByType
                    };
                    DoDamageCheckOnMySide(meByType, targetDisplays, skillVo, ref result, ref effectPos, ownerTransform, checkedTime, index, needSyn, maxDamageNum, ref damageList, ref damageNum, hurtCount);
                    return result;
                }
                case 2:
                    return result;

                case 3:
                    DoDamageCheckOnEnemyForOtherPlayers(playerVo, skillVo, ref effectPos, ownerTransform, enemyDisplays, checkedTime, index, needSyn, ref result, hurtCount, ref opt, ratio);
                    return result;
            }
            return result;
        }

        public static void DoAddEffectInPvp(ActionDisplay enemyDisplay, string targetEffectId)
        {
            if (((targetEffectId != string.Empty) && (enemyDisplay != null)) && (enemyDisplay.GoBase != null))
            {
                EffectMgr.Instance.CreateSceneEffect(targetEffectId, enemyDisplay.GoBase.transform.position, null, true, true, null, null);
            }
        }

        private void DoDamageCheckOnEnemy(ActionDisplay attackerDisplay, IEnumerable<ActionDisplay> enemyDisplays, SysSkillBaseVo skillVo, ref bool result, ref Vector3 effectPos, Transform ownerTransform, int checkedTime, int index, bool needSyn, int maxDamageNum, ref List<PDamage> damageList, ref List<PDamage> damageEventList, ref int damageNum, int hurtCount, ref DamageCheckOptions opt, int ratio)
        {
            BaseRoleVo meVoByType = attackerDisplay.GetMeVoByType<BaseRoleVo>();
            IEnumerator<ActionDisplay> enumerator = enemyDisplays.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ActionDisplay current = enumerator.Current;
                    if (SkillTargetCheck.IsTargetLegit(current, skillVo))
                    {
                        BaseRoleVo der = current.GetMeVoByType<BaseRoleVo>();
                        if (!der.IsEmptyHp && (current.Controller != null))
                        {
                            Vector3 position = current.GoBase.transform.position;
                            float boundDistance = current.BoundDistance;
                            if (IsSkillCovered3D(skillVo, ownerTransform, position, boundDistance))
                            {
                                int num2;
                                uint num3;
                                uint num4;
                                result = true;
                                if (++damageNum > maxDamageNum)
                                {
                                    return;
                                }
                                bool isDodge = false;
                                bool isCrit = false;
                                bool isParry = false;
                                bool isKnockDown = false;
                                bool isKnockBack = false;
                                GameFormula.CalcDamage(meVoByType, der, skillVo, out num2, out num4, out num3);
                                if (num2 != 0)
                                {
                                    int damageForShow = HandleCalcDamage(attackerDisplay, current, skillVo, hurtCount, ref num2, ratio);
                                    SetStateTypeAndDamageType(num4, num3, ref isDodge, ref isCrit, ref isKnockDown, ref isKnockBack);
                                    uint cutHp = Convert.ToUInt32(num2);
                                    List<PDamageBuff> damageBuffListFromString = new List<PDamageBuff>();
                                    List<PDamageBuff> anotherList = attackerDisplay.Controller.SkillController.CheckAddAdditionalBuff(skillVo);
                                    if (!SkillUtil.ShouldBlockThisSkillSubtype(skillVo, current))
                                    {
                                        damageBuffListFromString = StringUtils.GetDamageBuffListFromString(skillVo.buff_list);
                                    }
                                    damageBuffListFromString.Merge(anotherList);
                                    PDamage item = GetSkillPDamage(der, current, cutHp, damageForShow, isDodge, isCrit, damageBuffListFromString);
                                    damageEventList.Add(item);
                                    if (opt.excludeList != null)
                                    {
                                        opt.excludeList.Add(current);
                                    }
                                    if (needSyn || AppMap.Instance.IsInWifiPVP)
                                    {
                                        damageList.Add(item);
                                    }
                                    if (!needSyn || AppMap.Instance.IsInWifiPVP)
                                    {
                                        this.HandleAfterHurt(attackerDisplay, current, skillVo, cutHp, ref effectPos, ownerTransform, checkedTime, index, isDodge, isCrit, isParry, isKnockDown, isKnockBack, damageForShow);
                                        AddBuffNow(attackerDisplay, current, damageBuffListFromString);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private static void DoDamageCheckOnEnemyForOtherPlayers(BaseRoleVo playerVo, SysSkillBaseVo skillVo, ref Vector3 effectPos, Transform ownerTransform, IEnumerable<ActionDisplay> enemyDisplays, int checkedTime, int index, bool needSyn, ref bool result, int hurtCount, ref DamageCheckOptions opt, int ratio)
        {
            Log.AI(null, "DoDamage Check For Other Players");
            ActionDisplay meByType = playerVo.Controller.Me.GetMeByType<ActionDisplay>();
            IEnumerator<ActionDisplay> enumerator = enemyDisplays.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ActionDisplay current = enumerator.Current;
                    Log.AI(null, "who attack who " + current.GoBase);
                    if (SkillTargetCheck.IsTargetLegit(current, skillVo))
                    {
                        BaseRoleVo meVoByType = current.GetMeVoByType<BaseRoleVo>();
                        if (((current.GoBase != null) && (current.Controller != null)) && !meVoByType.IsEmptyHp)
                        {
                            Vector3 position = current.GoBase.transform.position;
                            float boundDistance = current.BoundDistance;
                            if (IsSkillCovered3D(skillVo, ownerTransform, position, boundDistance))
                            {
                                result = true;
                                if (opt.excludeList != null)
                                {
                                    opt.excludeList.Add(current);
                                }
                                if (!AppMap.Instance.IsInWifiPVP)
                                {
                                    ActionVo vo = GetSkillActionVo(skillVo, effectPos, current, meByType, checkedTime, meVoByType.MoveRatio, null);
                                    if ((vo != null) && !needSyn)
                                    {
                                        int num2;
                                        uint num3;
                                        uint num4;
                                        bool isDodge = false;
                                        bool isCrit = false;
                                        bool isParry = false;
                                        bool isKnockDown = false;
                                        bool isKnockBack = false;
                                        GameFormula.CalcDamage(playerVo, meVoByType, skillVo, out num2, out num3, out num4);
                                        if (num2 != 0)
                                        {
                                            int cutHp = HandleCalcDamage(meByType, current, skillVo, hurtCount, ref num2, ratio);
                                            uint delta = Convert.ToUInt32(num2);
                                            List<PDamageBuff> damageBuffListFromString = new List<PDamageBuff>();
                                            List<PDamageBuff> anotherList = meByType.Controller.SkillController.CheckAddAdditionalBuff(skillVo);
                                            if (!SkillUtil.ShouldBlockThisSkillSubtype(skillVo, current))
                                            {
                                                damageBuffListFromString = StringUtils.GetDamageBuffListFromString(skillVo.buff_list);
                                            }
                                            damageBuffListFromString.Merge(anotherList);
                                            vo.HurtEffectIndex = index;
                                            vo.HurtType = 0x3e9;
                                            SetStateTypeAndDamageType(num3, num4, ref isDodge, ref isCrit, ref isKnockDown, ref isKnockBack);
                                            if (meVoByType.CurHp <= delta)
                                            {
                                                vo.ActionType = "death";
                                                if (current.Type == 400)
                                                {
                                                    float num7 = (skillVo.hurt_fly_velocity_max * (((float) delta) / ((float) meVoByType.Hp))) * 0.001f;
                                                    if (num7 > 0f)
                                                    {
                                                        vo.ActionType = "deathfly";
                                                        current.Controller.MoveController.DeathFlySpeed = num7;
                                                    }
                                                    else
                                                    {
                                                        current.Controller.MoveController.DeathFlySpeed = 0f;
                                                    }
                                                    MonsterMgr instance = Singleton<MonsterMgr>.Instance;
                                                    instance.LiveMonsterNumber--;
                                                }
                                                meVoByType.ReduceHpToPercent((uint) 0);
                                                current.Controller.DisposeHudView();
                                                current.Controller.AttackController.AddAttackList(vo, true, false);
                                                if (AppMap.Instance.IsInArena && (current is MeDisplay))
                                                {
                                                    ArenaManager.Instance.EnemyIncreaseKillCount(meByType);
                                                    ArenaManager.Instance.IncreaseDeadCount(current);
                                                }
                                            }
                                            else
                                            {
                                                if (!isDodge)
                                                {
                                                    vo = FixHurtType(vo, isKnockBack, isKnockDown);
                                                    current.Controller.BeAttackedController.BeAttacked(vo);
                                                }
                                                meVoByType.lastHp = meVoByType.CurHp;
                                                meVoByType.ReduceHp(delta);
                                            }
                                            Vector3 target = current.GoBase.transform.position;
                                            Transform transform = Util.FindChildRecursive(current.GoBase.transform, "blood");
                                            if (transform != null)
                                            {
                                                target = transform.position;
                                            }
                                            HudView.Instance.CreateDamageHudText(target, isDodge, isCrit, isParry, cutHp, playerVo.Id == AppMap.Instance.me.GetVo().Id, true, false, true, current, meByType.GoBase);
                                            UpdateHealthBarRelated(current, delta);
                                            AddBuffNow(playerVo.Controller.GetMeByType<ActionDisplay>(), current, damageBuffListFromString);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private static void DoDamageCheckOnMySide(ActionDisplay meDisplay, IEnumerable<ActionDisplay> targetDisplays, SysSkillBaseVo skillVo, ref bool result, ref Vector3 effectPos, Transform ownerTransform, int checkedTime, int index, bool needSyn, int maxDamageNum, ref List<PDamage> damageList, ref int damageNum, int hurtCount)
        {
            SkillConst.HeroSkillSubtype subtype = (SkillConst.HeroSkillSubtype) skillVo.subtype;
            if (subtype == SkillConst.HeroSkillSubtype.heal)
            {
                SkillUtil.Heal(skillVo, meDisplay, targetDisplays, needSyn);
            }
            else if (subtype == SkillConst.HeroSkillSubtype.shield)
            {
                SkillUtil.Shield(skillVo, meDisplay, targetDisplays, needSyn);
            }
            else
            {
                AddBuffToMySide(skillVo, meDisplay, targetDisplays);
            }
        }

        private static void DoHandleBuffEvent(BaseDisplay baseDisplay, IEnumerable<AddBuffEvent> list, int cnt)
        {
            <DoHandleBuffEvent>c__AnonStoreyF5 yf = new <DoHandleBuffEvent>c__AnonStoreyF5 {
                cnt = cnt
            };
            AddBuffEvent event2 = list.FirstOrDefault<AddBuffEvent>(new Func<AddBuffEvent, bool>(yf.<>m__DD));
            if (event2 != null)
            {
                if (!event2.for_all_team)
                {
                    BuffController buffController = baseDisplay.Controller.buffController;
                    if (buffController != null)
                    {
                        buffController.AddBuff(event2.buff_id, event2.buff_lvl, null);
                    }
                }
                else if (baseDisplay is MeDisplay)
                {
                    if (<>f__am$cache4 == null)
                    {
                        <>f__am$cache4 = x => x.Controller.buffController != null;
                    }
                    if (<>f__am$cache5 == null)
                    {
                        <>f__am$cache5 = x => x.Controller.buffController;
                    }
                    IEnumerator<BuffController> enumerator = AppMap.Instance.SelfplayerList.Where<PlayerDisplay>(<>f__am$cache4).Select<PlayerDisplay, BuffController>(<>f__am$cache5).GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            enumerator.Current.AddBuff(event2.buff_id, event2.buff_lvl, null);
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                }
            }
        }

        public static ActionVo FixHurtType(ActionVo attackVo, bool isKnockBack, bool isKnockDown)
        {
            if (isKnockDown)
            {
                attackVo.HurtType = 0x3ee;
                return attackVo;
            }
            if (isKnockBack)
            {
                attackVo.HurtType = 0x3ef;
            }
            return attackVo;
        }

        private static IEnumerable<AddBuffEvent> GetAddBuffEventList(string str)
        {
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(str);
            ICollection<AddBuffEvent> is2 = new Collection<AddBuffEvent>();
            for (int i = 0; i < arrayStringToInt.Length; i += 5)
            {
                AddBuffEvent item = new AddBuffEvent {
                    min = arrayStringToInt[i],
                    max = arrayStringToInt[i + 1],
                    for_all_team = Convert.ToBoolean(arrayStringToInt[i + 2]),
                    buff_id = arrayStringToInt[i + 3],
                    buff_lvl = arrayStringToInt[i + 4]
                };
                is2.Add(item);
            }
            return is2;
        }

        private static int GetDamageType(bool isDodge, bool isCrit)
        {
            int num = 3;
            if (isCrit)
            {
                num = 2;
            }
            if (isDodge)
            {
                num = 0;
            }
            return num;
        }

        public static Vector3 GetDestPosFromNowPos(Transform trans, Vector3 enemyPos, float radius)
        {
            return GetDestPosFromNowPos(trans.position, enemyPos, radius);
        }

        public static Vector3 GetDestPosFromNowPos(Vector3 myPos, Transform trans, float radius)
        {
            return GetDestPosFromNowPos(myPos, trans.position, radius);
        }

        public static Vector3 GetDestPosFromNowPos(Vector3 myPos, Vector3 enemyPos, float radius)
        {
            myPos.y = 0f;
            enemyPos.y = 0f;
            Vector3 vector3 = myPos - enemyPos;
            Vector3 vector = (Vector3) (vector3.normalized * radius);
            Vector3 point = enemyPos + vector;
            if (!Util.GetPointMovable(ref point))
            {
                Debug.LogError("GetDestPos NoGoodPos To Check " + point);
                return point;
            }
            Log.AI(null, "good Pos Is " + point);
            return point;
        }

        public static Vector3 GetDestPosFromNowPosNOTFIX(Vector3 myPos, Vector3 enemyPos, float radius)
        {
            myPos.y = 0f;
            enemyPos.y = 0f;
            Vector3 vector3 = myPos - enemyPos;
            Vector3 vector = (Vector3) (vector3.normalized * radius);
            return (enemyPos + vector);
        }

        public static int GetDisplayType(int synType)
        {
            int num = 100;
            switch (synType)
            {
                case 1:
                    num = 400;
                    break;

                case 2:
                    num = 100;
                    break;
            }
            return Convert.ToInt32(num);
        }

        public static float GetDistance(Transform x, Transform y)
        {
            Vector3 position = x.position;
            Vector3 vector2 = y.position;
            return GetDistance(position, vector2);
        }

        public static float GetDistance(Transform x, Vector3 y)
        {
            return GetDistance(x.position, y);
        }

        public static float GetDistance(Vector3 x, Transform y)
        {
            return GetDistance(x, y.position);
        }

        public static float GetDistance(Vector3 x, Vector3 y)
        {
            x.y = 0f;
            y.y = 0f;
            Vector3 vector = x - y;
            return vector.magnitude;
        }

        public static Vector3 GetEscapePosFromNowPos(Vector3 myPos, Vector3 towerPos, float radius)
        {
            if (radius < GetDistance(myPos, towerPos))
            {
                return myPos;
            }
            return GetDestPosFromNowPos(myPos, towerPos, radius);
        }

        public static int GetEularAngelsForProtocol(float eularY)
        {
            return Convert.ToInt32((float) ((eularY * 1000f) * 1000f));
        }

        public static Vector3 GetFinalDestinationBeforeChase(Vector3 dest, Transform selfTransform, float atkRange)
        {
            return GetFinalDestinationBeforeChase(dest, selfTransform.position, atkRange);
        }

        public static Vector3 GetFinalDestinationBeforeChase(Vector3 dest, Vector3 selfPos, float atkRange)
        {
            float num2 = atkRange * 1f;
            Vector3 forward = selfPos - dest;
            float num3 = Mathf.Clamp(num2, 0f, forward.magnitude);
            forward.y = 0f;
            forward.Normalize();
            float y = Quaternion.LookRotation(forward).eulerAngles.y;
            Vector3 point = dest + ((Vector3) (forward * num2));
            for (float i = 30f; !Util.CheckMovable(point) && (i < 360f); i += 30f)
            {
                point = dest + ((Vector3) ((Quaternion.Euler(new Vector3(0f, y + i, 0f)) * Vector3.forward) * num2));
            }
            return point;
        }

        public static ActionVo GetSkillActionVo(SysSkillBaseVo skillVo, Vector3 excutePos, ActionDisplay enemyDisplay, ActionDisplay attacker, int checkedTime = 0, float moveRatio = 1, Transform lookAtTarget = null)
        {
            ActionVo vo = new ActionVo {
                ActionType = "injured",
                SkillId = skillVo.unikey,
                LookAtDestination = lookAtTarget,
                Attacker = attacker
            };
            if (enemyDisplay.GoBase == null)
            {
                return null;
            }
            Vector3 position = enemyDisplay.GoBase.transform.position;
            if (checkedTime == 0)
            {
                Vector3 vector2 = enemyDisplay.GoBase.transform.position - excutePos;
                vector2.Normalize();
                position += (Vector3) ((vector2 * skillVo.back_dis) * 0.001f);
            }
            position.y = enemyDisplay.GoBase.transform.position.y;
            vo.Destination = position;
            return vo;
        }

        private static PDamage GetSkillPDamage(BaseRoleVo enemyVo, BaseDisplay enemyDisplay, uint cutHp, int damageForShow, bool isDodge, bool isCrit, List<PDamageBuff> pBuff = null)
        {
            uint num = (enemyVo.CurHp <= cutHp) ? 0 : (enemyVo.CurHp - cutHp);
            PDamage damage = new PDamage {
                id = enemyVo.Id,
                type = Convert.ToByte(GetSynType(enemyDisplay.Type)),
                x = Convert.ToInt32((float) (enemyVo.X * 1000f)),
                y = Convert.ToInt32((float) (enemyVo.Y * 1000f)),
                z = Convert.ToInt32((float) (enemyVo.Z * 1000f)),
                dmg = cutHp,
                dmgForShow = damageForShow,
                hp = Convert.ToUInt32(num),
                dmgType = Convert.ToByte(GetDamageType(isDodge, isCrit))
            };
            if (pBuff != null)
            {
                damage.buffList = pBuff;
            }
            return damage;
        }

        public static int GetSynType(int displayType)
        {
            SkillSynType unavailable = SkillSynType.Unavailable;
            switch (displayType)
            {
                case 100:
                    unavailable = SkillSynType.Player;
                    break;

                case 400:
                    unavailable = SkillSynType.Monster;
                    break;
            }
            return Convert.ToInt32(unavailable);
        }

        private void HandleAfterHurt(ActionDisplay attackerDisplay, ActionDisplay enemyDisplay, SysSkillBaseVo skillVo, uint cutHp, ref Vector3 effectPos, Transform ownerTransform, int checkedTime, int index, bool isDodge, bool isCrit, bool isParry, bool isKnockDown, bool isKnockBack, int damageValForShow)
        {
            PlayerVo meVoByType = attackerDisplay.GetMeVoByType<PlayerVo>();
            BaseRoleVo enemyVo = enemyDisplay.GetMeVoByType<BaseRoleVo>();
            Vector3 position = enemyDisplay.GoBase.transform.position;
            if (enemyVo.CurHp <= cutHp)
            {
                HandleTargetWillDie(skillVo, ref effectPos, ownerTransform, checkedTime, index, enemyDisplay, enemyVo, cutHp, ref position, attackerDisplay, isKnockBack, isKnockDown);
                if (AppMap.Instance.IsInWifiPVP && (enemyDisplay is PlayerDisplay))
                {
                    WifiPvpManager.Instance.IncreaseKillCount(attackerDisplay);
                }
                if (AppMap.Instance.IsInArena)
                {
                    ArenaManager.Instance.IncreaseKillCount(attackerDisplay);
                    ArenaManager.Instance.EnemyIncreaseDeadCount(enemyDisplay);
                }
                if (AppMap.Instance.IsInWifiPVP)
                {
                    MonsterDisplay display = enemyDisplay as MonsterDisplay;
                    if ((display != null) && (enemyVo.Camp != 0))
                    {
                        string targetEffectId = AddEffectInPvp(display);
                        DoAddEffectInPvp(display, targetEffectId);
                        WifiPvpManager.Instance.RemoveEnemyTowerOrBase(display);
                    }
                }
                else if (enemyDisplay is MonsterDisplay)
                {
                    string str2 = AddEffectInPvp(enemyDisplay);
                    DoAddEffectInPvp(enemyDisplay, str2);
                }
            }
            else
            {
                HandleTargetHurts(skillVo, ref effectPos, ownerTransform, checkedTime, index, enemyDisplay, enemyVo, isDodge, isKnockDown, isKnockBack, cutHp, ref position, attackerDisplay);
            }
            PlayerShakeCamera(skillVo, meVoByType, isCrit, isDodge);
            if (!(enemyVo is MonsterVo) || (((MonsterVo) enemyVo).MonsterVO.type != 2))
            {
                HudView.Instance.CreateDamageHudText(position, isDodge, isCrit, isParry, damageValForShow, meVoByType.Id == AppMap.Instance.me.GetVo().Id, true, skillVo.subtype == 1, true, enemyDisplay, attackerDisplay.GoBase);
            }
            UpdateHealthBarRelated(enemyDisplay, cutHp);
        }

        private static int HandleCalcDamage(BaseDisplay attackerDisplay, BaseDisplay enemyDisplay, SysSkillBaseVo skillVo, int hurtCount, ref int damageVal, int ratio)
        {
            Log.AI(attackerDisplay, " HandleCalcDamage " + enemyDisplay);
            BaseRoleVo meVoByType = enemyDisplay.GetMeVoByType<BaseRoleVo>();
            damageVal = Convert.ToInt32((float) ((ratio * 0.001f) * ((float) damageVal)));
            damageVal = SkillUtil.DamageDecayCheck(skillVo, hurtCount, damageVal);
            damageVal = SkillUtil.CullingBladeCheck(skillVo, meVoByType, damageVal);
            int num = damageVal;
            damageVal = enemyDisplay.Controller.buffController.CalcAbortDamage(damageVal);
            BuffController buffController = enemyDisplay.Controller.buffController;
            if (buffController != null)
            {
                if (<>f__am$cache8 == null)
                {
                    <>f__am$cache8 = item => new <>__AnonType1<SimpleBuffVo, int>(item, MathUtils.Random(0, 0x2710));
                }
                if (<>f__am$cache9 == null)
                {
                    <>f__am$cache9 = <>__TranspIdent1 => <>__TranspIdent1.rand <= <>__TranspIdent1.item.attcedAddRate;
                }
                if (<>f__am$cacheA == null)
                {
                    <>f__am$cacheA = <>__TranspIdent1 => <>__TranspIdent1.item;
                }
                IEnumerator<SimpleBuffVo> enumerator = buffController.GetAttacAddBuffList().Select<SimpleBuffVo, <>__AnonType1<SimpleBuffVo, int>>(<>f__am$cache8).Where<<>__AnonType1<SimpleBuffVo, int>>(<>f__am$cache9).Select<<>__AnonType1<SimpleBuffVo, int>, SimpleBuffVo>(<>f__am$cacheA).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        SimpleBuffVo current = enumerator.Current;
                        attackerDisplay.Controller.buffController.AddBuff(current.BuffId, current.BuffLevel, null);
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
            return num;
        }

        private static void HandleFeedBackEvent(SysSkillBaseVo skillVo, BaseDisplay baseDisplay, ICollection<PDamage> damageList)
        {
            CheckAddHpIfEnemyHurts(baseDisplay, skillVo, damageList);
            CheckAddBuffIfEnemyHurts(baseDisplay, skillVo, damageList);
            CheckAddBuffIfEnemyDies(baseDisplay, skillVo, damageList);
        }

        [Obsolete]
        public static void HandleFloatingEffect(SysSkillBaseVo skillVo, ActionDisplay attacker, ActionDisplay enemyDisplay)
        {
        }

        private static void HandleSyncDamageData(SysSkillBaseVo skillVo, BaseControler ac, bool needSyn, DisplayVo meVo, List<PDamage> damageList)
        {
            if (needSyn && (damageList.Count > 0))
            {
                Singleton<SkillMode>.Instance.SendHeroSkillDamageList(skillVo.unikey, damageList, meVo.Id, ac.Me.Type);
            }
            if (AppMap.Instance.IsInWifiPVP && (damageList.Count > 0))
            {
                Singleton<WifiPvpMode>.Instance.DamageSync(skillVo.unikey, damageList, meVo.Id, ac.Me.Type);
            }
        }

        private static void HandleTargetHurts(SysSkillBaseVo skillVo, ref Vector3 effectPos, Transform ownerTransform, int checkedTime, int index, ActionDisplay enemyDisplay, BaseRoleVo enemyVo, bool isDodge, bool isKnockDown, bool isKnockBack, uint cutHp, ref Vector3 targetPos, ActionDisplay attacker)
        {
            ActionVo attackVo = GetSkillActionVo(skillVo, effectPos, enemyDisplay, attacker, checkedTime, enemyVo.MoveRatio, ownerTransform);
            if (attackVo != null)
            {
                attackVo.HurtEffectIndex = index;
                attackVo.HurtType = 0x3e9;
                if (!isDodge)
                {
                    attackVo = FixHurtType(attackVo, isKnockBack, isKnockDown);
                    enemyDisplay.Controller.BeAttackedController.BeAttacked(attackVo);
                }
                uint curHp = enemyVo.CurHp;
                enemyVo.lastHp = enemyVo.CurHp;
                enemyVo.ReduceHp(cutHp);
                if (enemyDisplay.Type == 400)
                {
                    targetPos = UpdateHpOnMonster(enemyDisplay, enemyVo, targetPos, curHp);
                }
            }
        }

        private static void HandleTargetWillDie(SysSkillBaseVo skillVo, ref Vector3 effectPos, Transform ownerTransform, int checkedTime, int index, ActionDisplay enemyDisplay, BaseRoleVo enemyVo, uint cutHp, ref Vector3 targetPos, ActionDisplay attacker, bool isKnockBack, bool isKnockDown)
        {
            ActionVo attackVo = GetSkillActionVo(skillVo, effectPos, enemyDisplay, attacker, checkedTime, enemyVo.MoveRatio, ownerTransform);
            if (attackVo != null)
            {
                attackVo.HurtEffectIndex = index;
                attackVo.ActionType = "death";
                if (enemyDisplay.Type == 400)
                {
                    float num = (skillVo.hurt_fly_velocity_max * (((float) cutHp) / ((float) enemyVo.Hp))) * 0.001f;
                    if (num > 0f)
                    {
                        attackVo.ActionType = "deathfly";
                        enemyDisplay.Controller.MoveController.DeathFlySpeed = num;
                    }
                    else
                    {
                        enemyDisplay.Controller.MoveController.DeathFlySpeed = 0f;
                    }
                    MonsterMgr instance = Singleton<MonsterMgr>.Instance;
                    instance.LiveMonsterNumber--;
                    targetPos = UpdateHpOnMonster(enemyDisplay, enemyVo, targetPos, enemyVo.CurHp);
                }
                if (!AppMap.Instance.IsInWifiPVP || !(enemyDisplay is PlayerDisplay))
                {
                    enemyVo.ReduceHpToPercent((uint) 0);
                    if (!enemyVo.IsEmptyHp)
                    {
                        attackVo.HurtEffectIndex = index;
                        attackVo.HurtType = 0x3e9;
                        attackVo = FixHurtType(attackVo, isKnockBack, isKnockDown);
                        enemyDisplay.Controller.BeAttackedController.BeAttacked(attackVo);
                    }
                    else
                    {
                        if (enemyDisplay.Controller.buffController != null)
                        {
                            enemyDisplay.Controller.buffController.Dead();
                        }
                        enemyDisplay.Controller.DisposeHudView();
                        enemyDisplay.Controller.AttackController.AddAttackList(attackVo, true, false);
                    }
                }
            }
        }

        public static bool IsSkillCovered3D(SysSkillBaseVo skillVo, Transform ownerTransform, Vector3 targetPosRaw, float boundDistance)
        {
            if (ownerTransform == null)
            {
                return false;
            }
            Log.AI(ownerTransform, " SkillCover3D " + skillVo.name);
            Vector3 position = ownerTransform.position;
            Quaternion rotation = ownerTransform.rotation;
            float distance = GetDistance(position, targetPosRaw);
            Vector3 targetPos = ((boundDistance <= 0f) || (distance <= boundDistance)) ? targetPosRaw : GetDestPosFromNowPosNOTFIX(position, targetPosRaw, boundDistance);
            Log.AI(null, string.Concat(new object[] { " Cover In 3d ", skillVo.name, " actualDistance ", distance, " targetPos ", targetPos, " targetPosRaw ", targetPosRaw }));
            Log.AI(null, string.Concat(new object[] { " skillType cover ", skillVo.effect_shape, " shape_x ", skillVo.shape_x, " shapeY ", skillVo.shape_y }));
            Log.AI(null, string.Concat(new object[] { " ownPos targetPos ", position, "  targetPos  ", targetPos, " radius ", skillVo.shape_x, " ", 0 }));
            switch (skillVo.effect_shape)
            {
                case 0:
                {
                    float radius = skillVo.shape_x * 0.001f;
                    return SkillRangeCheck.isInCircle(position, targetPos, radius);
                }
                case 1:
                {
                    float num3 = skillVo.shape_x * 0.001f;
                    float angle = 90f;
                    return SkillRangeCheck.isInSector(position, rotation, targetPos, num3, angle);
                }
                case 2:
                {
                    float num5 = skillVo.shape_x * 0.001f;
                    float num6 = skillVo.shape_y * 0.0005f;
                    return SkillRangeCheck.isInSector(position, rotation, targetPos, num5, num6);
                }
                case 3:
                {
                    float length = skillVo.shape_x * 0.001f;
                    float width = skillVo.shape_y * 0.0005f;
                    object[] objArray4 = new object[6];
                    objArray4[0] = " skill length width ";
                    objArray4[1] = length;
                    objArray4[2] = " width ";
                    objArray4[3] = width;
                    objArray4[4] = " targetPos ";
                    Vector3 vector3 = targetPos - position;
                    objArray4[5] = vector3.sqrMagnitude;
                    Log.AI(null, string.Concat(objArray4));
                    return SkillRangeCheck.isInRectangle(position, rotation, targetPos, length, width);
                }
            }
            return true;
        }

        public bool MonsterCheckInjured(ActionControler ac, SysSkillBaseVo skillVo, Vector3 effectPos, Transform ownerTransform, IEnumerable<ActionDisplay> enemyDisplays, bool isSend, int hurtCount, int ratio = 0x3e8)
        {
            MonsterVo meVoByType = ac.Me.GetMeVoByType<MonsterVo>();
            MonsterDisplay meByType = ac.Me.GetMeByType<MonsterDisplay>();
            if (meVoByType.IsEmptyHp)
            {
                return false;
            }
            int num = (skillVo.target_num == 0) ? 0x3e7 : skillVo.target_num;
            List<PDamage> list = new List<PDamage>();
            List<PDamage> damageList = new List<PDamage>();
            bool flag = false;
            int num2 = 0;
            if ((skillVo.target_type == 3) && (!AppMap.Instance.IsInWifiPVP || (meVoByType.Camp != 1)))
            {
                IEnumerator<ActionDisplay> enumerator = enemyDisplays.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        ActionDisplay current = enumerator.Current;
                        if (SkillTargetCheck.IsTargetLegit(current, skillVo))
                        {
                            BaseRoleVo der = current.GetMeVoByType<BaseRoleVo>();
                            if ((!der.IsEmptyHp && (current.GoBase != null)) && ((der == null) || !der.IsUnbeatable))
                            {
                                Vector3 position = current.GoBase.transform.position;
                                float boundDistance = current.BoundDistance;
                                if (IsSkillCovered3D(skillVo, ownerTransform, position, boundDistance))
                                {
                                    int num4;
                                    uint num5;
                                    uint num6;
                                    flag = true;
                                    if (++num2 > num)
                                    {
                                        goto Label_05EA;
                                    }
                                    bool isDodge = false;
                                    bool isCrit = false;
                                    bool isParry = false;
                                    bool isKnockDown = false;
                                    bool isKnockBack = false;
                                    GameFormula.CalcDamage(meVoByType, der, skillVo, out num4, out num5, out num6);
                                    if (num4 != 0)
                                    {
                                        int damageForShow = HandleCalcDamage(meByType, current, skillVo, hurtCount, ref num4, ratio);
                                        uint cutHp = Convert.ToUInt32(num4);
                                        List<PDamageBuff> damageBuffListFromString = new List<PDamageBuff>();
                                        List<PDamageBuff> anotherList = ac.SkillController.CheckAddAdditionalBuff(skillVo);
                                        if (!SkillUtil.ShouldBlockThisSkillSubtype(skillVo, current))
                                        {
                                            damageBuffListFromString = StringUtils.GetDamageBuffListFromString(skillVo.buff_list);
                                        }
                                        damageBuffListFromString.Merge(anotherList);
                                        SetStateTypeAndDamageType(num5, num6, ref isDodge, ref isCrit, ref isKnockDown, ref isKnockBack);
                                        List<PDamageBuff> pBuff = damageBuffListFromString;
                                        PDamage item = GetSkillPDamage(der, current, cutHp, damageForShow, isDodge, isCrit, pBuff);
                                        damageList.Add(item);
                                        if (isSend)
                                        {
                                            list.Add(item);
                                        }
                                        AddBuffNow(ac.Me as ActionDisplay, current, damageBuffListFromString);
                                        int num9 = int.Parse(Singleton<ConfigConst>.Instance.GetConfigDataString("NEW_GUIDE_DUPLICATE"));
                                        if (((der.Hp * 0.1f) >= (der.CurHp - cutHp)) && (DungeonMgr.Instance.curDungeonId == num9))
                                        {
                                            if (!isDodge)
                                            {
                                                ActionVo attackVo = GetSkillActionVo(skillVo, effectPos, current, ac.Me as ActionDisplay, 0, 1f, null);
                                                if (attackVo == null)
                                                {
                                                    continue;
                                                }
                                                attackVo.HurtType = 0x3e9;
                                                attackVo = FixHurtType(attackVo, isKnockBack, isKnockDown);
                                                current.Controller.BeAttackedController.BeAttacked(attackVo);
                                            }
                                        }
                                        else if (der.CurHp <= cutHp)
                                        {
                                            der.ReduceHpToPercent((uint) 0);
                                            if (!der.IsEmptyHp)
                                            {
                                                ActionVo vo4 = GetSkillActionVo(skillVo, effectPos, current, ac.Me as ActionDisplay, 0, 1f, null);
                                                if (vo4 == null)
                                                {
                                                    continue;
                                                }
                                                vo4.HurtType = 0x3e9;
                                                vo4 = FixHurtType(vo4, isKnockBack, isKnockDown);
                                                current.Controller.BeAttackedController.BeAttacked(vo4);
                                                goto Label_05EA;
                                            }
                                            if (current.Controller.buffController != null)
                                            {
                                                current.Controller.buffController.Dead();
                                            }
                                            ActionVo vo = new ActionVo {
                                                ActionType = "death",
                                                SkillId = Convert.ToUInt32(skillVo.id)
                                            };
                                            if (current.Type == 400)
                                            {
                                                float num10 = (skillVo.hurt_fly_velocity_max * (((float) cutHp) / ((float) der.Hp))) * 0.001f;
                                                if (num10 > 0f)
                                                {
                                                    vo.ActionType = "deathfly";
                                                    current.Controller.MoveController.DeathFlySpeed = num10;
                                                }
                                                else
                                                {
                                                    current.Controller.MoveController.DeathFlySpeed = 0f;
                                                }
                                            }
                                            if (!AppMap.Instance.IsInWifiPVP || !(current is PlayerDisplay))
                                            {
                                                current.Controller.DisposeHudView();
                                                current.Controller.AttackController.AddAttackList(vo, true, false);
                                            }
                                            if (AppMap.Instance.IsInWifiPVP)
                                            {
                                                if (current is PlayerDisplay)
                                                {
                                                }
                                                if ((current is MonsterDisplay) && (der.Camp != 0))
                                                {
                                                    string targetEffectId = AddEffectInPvp(current);
                                                    DoAddEffectInPvp(current, targetEffectId);
                                                    WifiPvpManager.Instance.RemoveEnemyTowerOrBase(current as MonsterDisplay);
                                                }
                                            }
                                            else if (current is MonsterDisplay)
                                            {
                                                string str2 = AddEffectInPvp(current);
                                                DoAddEffectInPvp(current, str2);
                                            }
                                        }
                                        else
                                        {
                                            if (!isDodge)
                                            {
                                                ActionVo vo6 = GetSkillActionVo(skillVo, effectPos, current, ac.Me as ActionDisplay, 0, 1f, null);
                                                if (vo6 == null)
                                                {
                                                    continue;
                                                }
                                                vo6.HurtType = 0x3e9;
                                                vo6 = FixHurtType(vo6, isKnockBack, isKnockDown);
                                                current.Controller.BeAttackedController.BeAttacked(vo6);
                                            }
                                            der.ReduceHp(cutHp);
                                        }
                                        MonsterShakeCamera(skillVo, isDodge);
                                        Transform transform = Util.FindChildRecursive(current.GoBase.transform, "blood");
                                        if (transform != null)
                                        {
                                            position = transform.position;
                                        }
                                        HudView.Instance.CreateDamageHudText(position, isDodge, isCrit, isParry, damageForShow, false, false, false, meVoByType.Camp == 0, current, ac.gameObject);
                                        Singleton<BattleRightView>.Instance.UpdateHeadBloodBar(der);
                                        if (der.Controller.Me == AppMap.Instance.me)
                                        {
                                            Singleton<BattleCenterView>.Instance.updateHp(der as PlayerVo);
                                        }
                                        if (this.SelfInjuryGuideCallback != null)
                                        {
                                            this.SelfInjuryGuideCallback(meVoByType.monsterId);
                                        }
                                        UpdateHealthBarRelated(current, cutHp);
                                    }
                                }
                            }
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        Label_05EA:
            if (AppMap.Instance.IsInWifiPVP && (list.Count > 0))
            {
                Singleton<WifiPvpMode>.Instance.DamageSync(skillVo.unikey, list, meVoByType.Id, ac.Me.Type);
            }
            if ((damageList.Count > 0) && (ac.AiController != null))
            {
                ac.AiController.SkilledSuccessfully(skillVo, damageList, Time.time);
            }
            return flag;
        }

        private static void MonsterShakeCamera(SysSkillBaseVo skillVo, bool isDodge)
        {
            if (!isDodge)
            {
                CameraShake component = Camera.main.GetComponent<CameraShake>();
                if ((component != null) && (skillVo.shake_id != 0))
                {
                    component.play(skillVo.shake_id, 3);
                }
            }
        }

        private static void PlayerShakeCamera(SysSkillBaseVo skillVo, PlayerVo vo, bool isCrit, bool isDodge)
        {
            if ((vo.Id == AppMap.Instance.me.GetVo().Id) && !isDodge)
            {
                CameraShake component = Camera.main.GetComponent<CameraShake>();
                if (component != null)
                {
                    if (isCrit)
                    {
                        component.play(1, 1);
                    }
                    if (skillVo.shake_id != 0)
                    {
                        component.play(skillVo.shake_id, 2);
                    }
                }
            }
        }

        public static void SetStateTypeAndDamageType(uint damageType, uint stateType, ref bool isDodge, ref bool isCrit, ref bool isKnockDown, ref bool isKnockBack)
        {
            switch (damageType)
            {
                case 0:
                    isDodge = true;
                    break;

                case 2:
                    isCrit = true;
                    break;
            }
            switch (stateType)
            {
                case 2:
                    isKnockDown = true;
                    break;

                case 3:
                    isKnockBack = true;
                    break;
            }
        }

        [Obsolete("this method was abadoned")]
        public static void ShowBuffEffects(ActionDisplay attacker, ActionDisplay target, IEnumerable<PDamageBuff> buffList)
        {
        }

        public static List<KV> SortAscending(List<KV> list)
        {
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = x => x.distance;
            }
            list = list.OrderBy<KV, float>(<>f__am$cache6).ToList<KV>();
            return list;
        }

        public static List<KV> SortDescending(List<KV> list)
        {
            if (<>f__am$cache7 == null)
            {
                <>f__am$cache7 = x => x.distance;
            }
            list = list.OrderByDescending<KV, float>(<>f__am$cache7).ToList<KV>();
            return list;
        }

        public static void UpdateHealthBarRelated(ActionDisplay enemyDisplay, uint cutHp)
        {
            BaseRoleVo meVoByType;
            switch (enemyDisplay.Type)
            {
                case 100:
                    meVoByType = enemyDisplay.GetMeVoByType<PlayerVo>();
                    HudView.Instance.AddPlayerHealthBarWhenEmpty(enemyDisplay);
                    enemyDisplay.Controller.GoHp.GetComponent<UpdateHpBar>().SetHp(meVoByType.Hp, meVoByType.CurHp, false);
                    break;

                case 400:
                    meVoByType = enemyDisplay.GetMeVoByType<MonsterVo>();
                    switch (((MonsterVo) meVoByType).MonsterVO.quality)
                    {
                        case 1:
                            if (enemyDisplay.Controller.GoHp != null)
                            {
                                enemyDisplay.Controller.GoHp.GetComponent<UpdateHpBar>().SetHp(meVoByType.Hp, meVoByType.CurHp, false);
                                return;
                            }
                            HudView.Instance.AddMonsterHealthBarWhenEmpty(enemyDisplay, meVoByType, (float) meVoByType.Hp, (float) meVoByType.CurHp, (float) cutHp);
                            return;

                        case 2:
                            return;

                        case 3:
                            if (!AppMap.Instance.IsInWifiPVP)
                            {
                                if (enemyDisplay.GetMeVoByType<MonsterVo>().MonsterVO.born_camera == 0)
                                {
                                    Singleton<BattleTopView>.Instance.AddBossBloodBar(enemyDisplay as MonsterDisplay);
                                }
                                Singleton<BattleTopView>.Instance.updateBossBloodBar((float) meVoByType.Hp, (float) meVoByType.CurHp, (float) cutHp);
                                return;
                            }
                            if (meVoByType.Camp == 1)
                            {
                                Singleton<BattleTopView>.Instance.AddBossBloodBar(enemyDisplay as MonsterDisplay);
                                Singleton<BattleTopView>.Instance.updateBossBloodBar((float) meVoByType.Hp, (float) meVoByType.CurHp, (float) cutHp);
                            }
                            return;
                    }
                    break;
            }
        }

        private static Vector3 UpdateHpOnMonster(ActionDisplay enemyDisplay, BaseRoleVo enemyVo, Vector3 targetPos, uint previousHp)
        {
            MonsterVo vo = enemyVo as MonsterVo;
            GameObject obj2 = Tools.find(enemyDisplay.Controller.gameObject, "pivot");
            targetPos = (obj2 == null) ? targetPos : obj2.transform.position;
            if (vo != null)
            {
                Singleton<BattleMode>.Instance.SetCurrentHp(vo.MonsterVO.name, enemyVo.Hp, previousHp, enemyVo.CurHp, vo.Level, vo.MonsterVO.quality, vo.MonsterVO.icon, vo.MonsterVO.hp_count);
            }
            return targetPos;
        }

        [CompilerGenerated]
        private sealed class <DoHandleBuffEvent>c__AnonStoreyF5
        {
            internal int cnt;

            internal bool <>m__DD(DamageCheck.AddBuffEvent x)
            {
                return ((x.min <= this.cnt) && (x.max >= this.cnt));
            }
        }

        private sealed class AddBuffEvent
        {
            public int buff_id;
            public int buff_lvl;
            public bool for_all_team;
            public int max;
            public int min;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct KV
        {
            public Transform trans;
            public float distance;
            public KV(Transform transform, float dis)
            {
                this.trans = transform;
                this.distance = dis;
            }
        }

        public delegate void SelfInjuryDelegate(uint monId);
    }
}

