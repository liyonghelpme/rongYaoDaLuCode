﻿namespace com.game.module.fight.arpg
{
    using System;

    public class HalfCircle : Circle
    {
        protected virtual float GetDegreeByTriangleIndex(int idx)
        {
            float num = 180f / ((float) base.detail);
            return ((idx - 1) * num);
        }

        public override ShapeType shapeType
        {
            get
            {
                return ShapeType.HALF_CIRCLE;
            }
        }
    }
}

