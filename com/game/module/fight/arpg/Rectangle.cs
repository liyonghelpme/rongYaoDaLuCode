﻿namespace com.game.module.fight.arpg
{
    using System;
    using UnityEngine;

    public class Rectangle : Shape
    {
        private float _preHeight;
        private float _preWidth;
        public float height = 2f;
        public float width = 2f;

        protected override void CreateIB()
        {
            base._triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        }

        protected override void CreateVB()
        {
            float x = this.width / 2f;
            float z = this.height / 2f;
            base._vertices = new Vector3[] { new Vector3(-x, 0f, -z), new Vector3(-x, 0f, z), new Vector3(x, 0f, z), new Vector3(x, 0f, -z) };
            base._uvs = new Vector2[] { new Vector2(), new Vector2(0f, 1f), new Vector2(1f, 1f), new Vector2(1f, 0f) };
        }

        protected override bool freshMesh
        {
            get
            {
                float num = 0.1f;
                if ((Mathf.Abs((float) (this.width - this._preWidth)) < num) && (Mathf.Abs((float) (this.height - this._preHeight)) < num))
                {
                    return false;
                }
                this._preWidth = this.width;
                this._preHeight = this.height;
                return true;
            }
        }

        public override ShapeType shapeType
        {
            get
            {
                return ShapeType.RECTANGLE;
            }
        }
    }
}

