﻿namespace com.game.module.fight.arpg
{
    using System;
    using UnityEngine;

    public class Sector : Circle
    {
        private float _prevDegree;
        public float degree = 60f;

        protected override float GetDegreeByTriangleIndex(int idx)
        {
            float num = this.degree / ((float) base.detail);
            return (((idx - 1) * num) - (this.degree / 2f));
        }

        protected override bool freshMesh
        {
            get
            {
                if (base.freshMesh)
                {
                    return true;
                }
                if (this.degree < 0f)
                {
                    this.degree *= -1f;
                }
                if (Mathf.Abs((float) (this._prevDegree - this.degree)) > 1f)
                {
                    this._prevDegree = this.degree;
                    return true;
                }
                return false;
            }
        }

        public override ShapeType shapeType
        {
            get
            {
                return ShapeType.SECTOR;
            }
        }
    }
}

