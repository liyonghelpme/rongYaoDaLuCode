﻿namespace com.game.module.fight.arpg
{
    using com.game.manager;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShapeMgr : MonoBehaviour
    {
        private static GameObject _go;
        private static ShapeMgr _main;
        private Dictionary<ShapeType, GameObject> _prefabDic = new Dictionary<ShapeType, GameObject>();
        private Dictionary<ShapeType, List<Shape>> _shapeDic = new Dictionary<ShapeType, List<Shape>>();
        private GameObject _shapeRoot;
        private const int DEF_DETAIL = 0x20;
        private readonly string HALF_CIRCLE_URL = "UI/SkillArea/HalfCircle.assetbundle";
        private readonly string RECTANGLEURL = "UI/SkillArea/Rectangle.assetbundle";
        private readonly string SECTORURL = "UI/SkillArea/Sector.assetbundle";

        private void Awake()
        {
            if (null == _go)
            {
                _go = base.gameObject;
            }
            this._shapeRoot = new GameObject("ShapeRoot");
            this.MakeParentChild(_go, this._shapeRoot);
            AssetManager.Instance.LoadAsset<GameObject>(this.SECTORURL, new LoadAssetFinish<GameObject>(this.OnPrefabLoaded), null, false, true);
            AssetManager.Instance.LoadAsset<GameObject>(this.RECTANGLEURL, new LoadAssetFinish<GameObject>(this.OnPrefabLoaded), null, false, true);
            AssetManager.Instance.LoadAsset<GameObject>(this.HALF_CIRCLE_URL, new LoadAssetFinish<GameObject>(this.OnPrefabLoaded), null, false, true);
        }

        public Circle GetCircle(float radius = 1f, GameObject parent = null, int detail = 0x20)
        {
            Circle circle = this.GetShape<Circle>(ShapeType.CIRCLE, parent, 0x20);
            if (null != circle)
            {
                circle.radius = radius;
                circle.detail = detail;
            }
            return circle;
        }

        private GameObject GetClone(ShapeType type, GameObject parent = null)
        {
            if (this._prefabDic == null)
            {
                return null;
            }
            if (!this._prefabDic.ContainsKey(type))
            {
                return null;
            }
            return NGUITools.AddChild(parent, this._prefabDic[type]);
        }

        public HalfCircle GetHalfCircle(float radius = 1f, GameObject parent = null, int detail = 0x20)
        {
            HalfCircle circle = this.GetShape<HalfCircle>(ShapeType.HALF_CIRCLE, parent, 0x20);
            if (null != circle)
            {
                circle.radius = radius;
                circle.detail = detail;
            }
            return circle;
        }

        public Rectangle GetRect(float width = 2f, float height = 2f, GameObject parent = null)
        {
            Rectangle rectangle = this.GetShape<Rectangle>(ShapeType.RECTANGLE, parent, 0x20);
            if (null != rectangle)
            {
                rectangle.width = width;
                rectangle.height = height;
            }
            return rectangle;
        }

        public Sector GetSector(float degree, float radius = 1f, GameObject parent = null, int detail = 0x20)
        {
            Sector sector = this.GetShape<Sector>(ShapeType.SECTOR, parent, 0x20);
            if (null != sector)
            {
                sector.radius = radius;
                sector.degree = degree;
                sector.detail = detail;
            }
            return sector;
        }

        private T GetShape<T>(ShapeType type, GameObject go = null, int detail = 0x20) where T: Shape
        {
            T component;
            if (this._shapeDic == null)
            {
                return null;
            }
            if (this._shapeDic.ContainsKey(type))
            {
                List<Shape> list = this._shapeDic[type];
                int count = list.Count;
                if (count > 0)
                {
                    component = list[count - 1] as T;
                    list.RemoveAt(count - 1);
                    if (go != null)
                    {
                        this.MakeParentChild(go, component.go);
                    }
                    component.go.SetActive(true);
                    return component;
                }
            }
            GameObject clone = this.GetClone(type, go);
            if (null == clone)
            {
                return null;
            }
            component = clone.GetComponent<T>();
            if (null == component)
            {
                component = clone.AddComponent<T>();
            }
            return component;
        }

        private void MakeParentChild(GameObject parent, GameObject child)
        {
            if ((parent != null) && (null != child))
            {
                Transform transform = child.transform;
                transform.parent = parent.transform;
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
                transform.localScale = Vector3.one;
                child.layer = parent.layer;
            }
        }

        private void OnEnable()
        {
            if (null != _main)
            {
                if (this != _main)
                {
                    UnityEngine.Object.Destroy(this);
                }
            }
            else
            {
                _main = this;
            }
        }

        private void OnPrefabLoaded(GameObject go)
        {
            Dictionary<string, List<ShapeType>> dictionary2 = new Dictionary<string, List<ShapeType>>();
            dictionary2.Add("Sector", new List<ShapeType> { 3, 1 });
            dictionary2.Add("Rectangle", new List<ShapeType> { 4 });
            dictionary2.Add("HalfCircle", new List<ShapeType> { 2 });
            Dictionary<string, List<ShapeType>> dictionary = dictionary2;
            string name = go.name;
            if (dictionary.ContainsKey(name))
            {
                List<ShapeType> list = dictionary[name];
                foreach (ShapeType type in list)
                {
                    this._prefabDic.Add(type, go);
                }
            }
        }

        public void Recycle(Shape shape)
        {
            if (this._shapeDic != null)
            {
                if (!this._shapeDic.ContainsKey(shape.shapeType))
                {
                    this._shapeDic.Add(shape.shapeType, new List<Shape>());
                }
                List<Shape> list = this._shapeDic[shape.shapeType];
                shape.gameObject.SetActive(false);
                this.MakeParentChild(this._shapeRoot, shape.gameObject);
                if (list.IndexOf(shape) < 0)
                {
                    list.Add(shape);
                }
            }
        }

        public static ShapeMgr main
        {
            get
            {
                if (null == _main)
                {
                    _go = new GameObject("ShapeMgr");
                    _main = _go.AddComponent<ShapeMgr>();
                }
                return _main;
            }
        }
    }
}

