﻿namespace com.game.module.fight.arpg
{
    using System;

    public enum ShapeType
    {
        NONE,
        CIRCLE,
        HALF_CIRCLE,
        SECTOR,
        RECTANGLE
    }
}

