﻿namespace com.game.module.fight.arpg
{
    using System;
    using UnityEngine;
    using UnityLog;

    public sealed class SkillRangeCheck
    {
        public static bool doCheckInRectangle(Vector3 targetPos, Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3)
        {
            float x = targetPos.x;
            float z = targetPos.z;
            float num3 = v0.x;
            float num4 = v0.z;
            float num5 = v1.x;
            float num6 = v1.z;
            float num7 = v2.x;
            float num8 = v2.z;
            float num9 = v3.x;
            float num10 = v3.z;
            return (((Multiply(x, z, num3, num4, num5, num6) * Multiply(x, z, num9, num10, num7, num8)) <= 0f) && ((Multiply(x, z, num9, num10, num3, num4) * Multiply(x, z, num7, num8, num5, num6)) <= 0f));
        }

        public static bool isInCircle(Vector3 ownerPos, Vector3 targetPos, float radius)
        {
            Vector3 a = ownerPos - Vector3.Project(ownerPos, Vector3.up);
            Vector3 b = targetPos - Vector3.Project(targetPos, Vector3.up);
            return (Vector3.Distance(a, b) <= radius);
        }

        public static bool isInRectangle(Vector3 ownerPos, Quaternion r, Vector3 targetPos, float length, float width)
        {
            Vector3 vector = ownerPos + ((Vector3) ((r * Vector3.left) * width));
            Vector3 vector2 = ownerPos + ((Vector3) ((r * Vector3.right) * width));
            Vector3 vector3 = vector + ((Vector3) ((r * Vector3.forward) * length));
            Vector3 vector4 = vector2 + ((Vector3) ((r * Vector3.forward) * length));
            Log.AI(null, string.Concat(new object[] { " Rectangle ", ownerPos, " targetPos ", targetPos, " left ", vector, " right ", vector2, " leftEnd ", vector3, " rightEnd ", vector4 }));
            return doCheckInRectangle(targetPos, vector3, vector4, vector2, vector);
        }

        public static bool isInSector(Vector3 position, Quaternion rotation, Vector3 targetPos, float radius, float angle)
        {
            Vector3 vector = position + ((Vector3) ((rotation * Vector3.forward) * radius));
            Quaternion quaternion = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y - angle, rotation.eulerAngles.z);
            Quaternion quaternion2 = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y + angle, rotation.eulerAngles.z);
            Vector3 vector2 = position + ((Vector3) ((quaternion * Vector3.forward) * radius));
            Vector3 vector3 = position + ((Vector3) ((quaternion2 * Vector3.forward) * radius));
            bool flag = isInTriangle(targetPos, position, vector2, vector);
            bool flag2 = isInTriangle(targetPos, position, vector3, vector);
            return (flag || flag2);
        }

        public static bool isInTriangle(Vector3 targetPos, Vector3 v0, Vector3 v1, Vector3 v2)
        {
            float x = targetPos.x;
            float z = targetPos.z;
            float num3 = v0.x;
            float num4 = v0.z;
            float num5 = v1.x;
            float num6 = v1.z;
            float num7 = v2.x;
            float num8 = v2.z;
            float num9 = triangleArea(num3, num4, num5, num6, num7, num8);
            float num10 = (triangleArea(num3, num4, num5, num6, x, z) + triangleArea(num3, num4, x, z, num7, num8)) + triangleArea(x, z, num5, num6, num7, num8);
            return (Mathf.Abs((float) (num9 - num10)) <= 0.1f);
        }

        private static float Multiply(float p1x, float p1z, float p2x, float p2z, float p0x, float p0z)
        {
            return (((p1x - p0x) * (p2z - p0z)) - ((p2x - p0x) * (p1z - p0z)));
        }

        public static float triangleArea(float v0x, float v0z, float v1x, float v1z, float v2x, float v2z)
        {
            return Mathf.Abs((float) (((((((v0x * v1z) + (v1x * v2z)) + (v2x * v0z)) - (v1x * v0z)) - (v2x * v1z)) - (v0x * v2z)) / 2f));
        }
    }
}

