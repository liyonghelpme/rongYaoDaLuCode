﻿namespace com.game.module.fight.arpg
{
    using com.game.data;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;

    public sealed class SkillTargetCheck
    {
        public static bool IsTargetLegit(ActionDisplay enemyDisplay, SysSkillBaseVo skillvo)
        {
            if (enemyDisplay.GoBase == null)
            {
                return false;
            }
            bool flag = false;
            bool flag2 = skillvo.is_skill;
            if (enemyDisplay is MonsterDisplay)
            {
                switch (enemyDisplay.GetMeVoByType<MonsterVo>().MonsterVO.type)
                {
                    case 3:
                    case 5:
                        flag = true;
                        break;
                }
            }
            return !(flag && flag2);
        }
    }
}

