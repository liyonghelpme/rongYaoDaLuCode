﻿namespace com.game.module.fight.arpg
{
    using com.game.module.core;
    using com.game.vo;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;
    using UnityLog;

    public sealed class Soul
    {
        public static readonly Soul Instance;
        private static int player_vs_player;
        private static int player_vs_pvp_private;
        private static int player_vs_pvp_tower;
        private static int player_vs_wild_monster;

        static Soul()
        {
            if (Instance == null)
            {
            }
            Instance = new Soul();
        }

        private Soul()
        {
            player_vs_player = this.GetConfig("PLAYER_VS_PLAYER");
            player_vs_pvp_tower = this.GetConfig("PLAYER_VS_PVP_TOWER");
            player_vs_pvp_private = this.GetConfig("PLAYER_VS_PVP_PRIVATE");
            player_vs_wild_monster = this.GetConfig("PLAYER_VS_WILD_MONSTER");
            Log.AI(null, string.Concat(new object[] { " Initial soul ", player_vs_player, " tower ", player_vs_pvp_tower, " private ", player_vs_pvp_private, " wild ", player_vs_wild_monster }));
        }

        public void AddSoul(ActionDisplay ad)
        {
            Log.AI(null, "AddSoul " + ad);
            this.AddSoul(MeVo.instance, ad);
        }

        private void AddSoul(MeVo vo, ActionDisplay ad)
        {
            if (ad is PlayerDisplay)
            {
                this.AddSoulPlayer(vo);
            }
            else if (ad is MonsterDisplay)
            {
                int type = ad.GetMeVoByType<MonsterVo>().MonsterVO.type;
                this.AddSoulMonster(vo, type);
            }
        }

        private void AddSoulMonster(MeVo vo, int mon_type)
        {
            switch (mon_type)
            {
                case 1:
                    vo.AddSoul(player_vs_wild_monster);
                    break;

                case 3:
                    vo.AddSoul(player_vs_pvp_tower);
                    break;

                case 4:
                    vo.AddSoul(player_vs_pvp_private);
                    break;
            }
        }

        public void AddSoulPlayer(MeVo vo)
        {
            Log.AI(null, "add soul player");
            vo.AddSoul(player_vs_player);
        }

        private int GetConfig(string conf)
        {
            return Singleton<ConfigConst>.Instance.GetConfigData("SOUL_ADD_" + conf);
        }
    }
}

