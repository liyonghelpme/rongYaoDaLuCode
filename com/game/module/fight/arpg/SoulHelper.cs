﻿namespace com.game.module.fight.arpg
{
    using com.game.vo;
    using System;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public static class SoulHelper
    {
        public static void AddSoul(this MeVo vo, int delta)
        {
            Log.AI(null, string.Concat(new object[] { "add soul is ", delta, " vosoul ", vo.soul }));
            if ((delta > 0) && (vo.soul < 100))
            {
                vo.soul += Convert.ToUInt32(delta);
                if (vo.soul >= 100)
                {
                    vo.soul = 100;
                }
            }
        }

        public static void ClearSoul(this MeVo vo)
        {
            vo.soul = 0;
        }

        public static bool IsSoulEmpty(this MeVo vo)
        {
            return (vo.soul == 0);
        }

        public static bool IsSoulMax(this MeVo vo)
        {
            return (vo.soul == 100);
        }
    }
}

