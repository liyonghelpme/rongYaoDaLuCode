﻿namespace com.game.module.fight.vo
{
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class ActionVo
    {
        public string ActionType;
        public ActionDisplay Attacker;
        public bool ByHand;
        public MoveEndCallback Callback;
        public Vector3 Destination;
        public int HurtEffectIndex;
        public int HurtType = 0x3e9;
        public bool IsAStarMove;
        public bool IsFromNet;
        public bool IsIgnoreDistance;
        public bool IsMoveDirectly;
        public Transform LookAtDestination;
        public PreMoveCallBack Preparework;
        public uint SkillId;
        public int SkillType = -1;
        public ActionDisplay Target;
    }
}

