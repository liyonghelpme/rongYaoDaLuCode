﻿namespace com.game.module.fight.vo
{
    using com.u3d.bases.display;
    using System;
    using UnityEngine;

    public class AttackVo
    {
        public Vector3 attackPoint;
        public string attackType;
        public float dire = -1f;
        public ActionDisplay followDisplay;
        public float injuredTime;
        public bool isFromMe;
        public bool isMe;
        public bool isMove;
        public Vector3 MoveToPoint;
        public Vector3 point;
        public uint skillId;
        public string state;
        public GameObject target;
        public Vector3 targetPoint;
        public float time;
        public int useType;

        public void clear()
        {
            this.state = null;
            this.time = 0f;
            this.point = Vector3.zero;
        }
    }
}

