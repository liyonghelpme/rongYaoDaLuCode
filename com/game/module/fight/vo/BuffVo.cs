﻿namespace com.game.module.fight.vo
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.effect;
    using com.game.module.fight.arpg;
    using com.u3d.bases.display;
    using System;
    using UnityEngine;

    public class BuffVo : SimpleBuffVo
    {
        public int abortDamage;
        public int addHp;
        public float addHpRate;
        public string attrStr;
        public BuffController buffController;
        public SysBuffVo BuffTemp;
        public EffectControlerII effectController;
        public float firstEffectTime;
        public float intervalLeftTime;
        public bool isFirstEffect;
        public float totalTime;

        public BuffVo(uint buffId, uint buffLevel, BuffController buffController, ActionDisplay inActionDisplay) : base(buffId, buffLevel, inActionDisplay)
        {
            this.attrStr = string.Empty;
            this.BuffTemp = BaseDataMgr.instance.GetSysBuffVo(buffId, buffLevel);
            if (this.BuffTemp != null)
            {
                this.buffController = buffController;
                this.totalTime = ((float) this.BuffTemp.totalTime) / 1000f;
                this.intervalLeftTime = ((float) this.BuffTemp.intervalTime) / 1000f;
                this.firstEffectTime = ((float) this.BuffTemp.firstEffectTime) / 1000f;
                this.isFirstEffect = false;
                this.abortDamage = this.BuffTemp.value1;
                this.attrStr = this.BuffTemp.attrArray;
                this.HeadHpAttr(this.BuffTemp.attrArray);
            }
            else
            {
                Debug.LogError(string.Concat(new object[] { "没有这个buff配表", buffId, " ", buffLevel }));
            }
        }

        private void HeadHpAttr(string attrVo)
        {
            attrVo = attrVo.Replace("[[", string.Empty);
            attrVo = attrVo.Replace("]]", string.Empty);
            attrVo = attrVo.Replace("],[", "|");
            char[] separator = new char[] { '|' };
            string[] strArray = attrVo.Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                try
                {
                    char[] chArray2 = new char[] { ',' };
                    string[] strArray2 = strArray[i].Split(chArray2);
                    int num2 = 0;
                    if (strArray2.Length == 2)
                    {
                        num2 = int.Parse(strArray2[1].ToString());
                        if (int.Parse(strArray2[0]) == 10)
                        {
                            this.addHp += num2;
                        }
                    }
                    else if (strArray2.Length == 3)
                    {
                        float num3 = int.Parse(strArray2[1]);
                        float num4 = int.Parse(strArray2[2]);
                        if (int.Parse(strArray2[0]) == 10)
                        {
                            this.addHpRate += num3 / num4;
                        }
                    }
                    else if (strArray2.Length != 1)
                    {
                        Debug.LogError(string.Concat(new object[] { strArray2.Length, "buff策划配表有错: id:", base.BuffId, " ", base.BuffLevel, " ", attrVo }));
                    }
                }
                catch (Exception)
                {
                    Debug.LogError(string.Concat(new object[] { "buff策划配表有错: id:", base.BuffId, " ", base.BuffLevel, " ", attrVo }));
                }
            }
        }
    }
}

