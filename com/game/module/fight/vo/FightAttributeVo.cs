﻿namespace com.game.module.fight.vo
{
    using com.game.vo;
    using System;
    using UnityEngine;

    public class FightAttributeVo
    {
        private int _abHurt;
        private float _atkCdDecRate = 1f;
        private float _atkNormalAddRate = 1f;
        private float _atkSpeedAddRate = 1f;
        private float _attMVamRate = 1f;
        private float _attPVamRate = 1f;
        private int _attrMagSum;
        private int _attrPhySum;
        private float _critAddRate = 1f;
        private float _critRate = 1f;
        private int _defMagSum;
        private int _defPhySum;
        private float _dmgBackMRate = 1f;
        private float _dmgBackPRate = 1f;
        private float _dodgeRate = 1f;
        private float _flexRate = 1f;
        private float _hitRate = 1f;
        private int _hpFullSum;
        private float _hpReRate = 1f;
        private int _hpSum;
        private float _moveSpeedRate = 1f;

        public static FightAttributeVo AttributeAddValue(FightAttributeVo att, int type, int value)
        {
            FightAttributeVo vo = att;
            switch (type)
            {
                case 10:
                    vo.Hp += value;
                    return vo;

                case 11:
                    vo.HpFull += value;
                    return vo;

                case 12:
                    vo.AttrPhySum += value;
                    return vo;

                case 13:
                    vo.AttrMagSum += value;
                    return vo;

                case 14:
                    vo.DefPhySum += value;
                    return vo;

                case 15:
                    vo.DefMagSum += value;
                    return vo;

                case 0x10:
                    vo.HitRate = MutilPDec(vo.HitRate, value);
                    return vo;

                case 0x11:
                    vo.DodgeRate = MutilPAdd(vo.DodgeRate, value);
                    return vo;

                case 0x12:
                case 0x15:
                case 0x17:
                case 0x19:
                case 0x1a:
                case 0x21:
                case 0x22:
                    return vo;

                case 0x13:
                    vo.FlexRate = MutilPDec(vo.FlexRate, value);
                    return vo;

                case 20:
                    vo.AbHurt = value;
                    return vo;

                case 0x16:
                    vo.HpReRate = MutilPAdd(vo.HpReRate, value);
                    return vo;

                case 0x18:
                    vo.MoveSpeedRate = MutilPAdd(vo.MoveSpeedRate, value);
                    return vo;

                case 0x1b:
                    vo.AttPVamRate = MutilPAdd(vo.AttPVamRate, value);
                    return vo;

                case 0x1c:
                    vo.AttMVamRate = MutilPAdd(vo.AttMVamRate, value);
                    return vo;

                case 0x1d:
                    vo.CritAddRate = MutilPAdd(vo.CritAddRate, value);
                    return vo;

                case 30:
                    vo.AtkSpeedAddRate = MutilPAdd(vo.AtkSpeedAddRate, value);
                    return vo;

                case 0x1f:
                    vo.AtkCdDecRate = MutilPAdd(vo.AtkCdDecRate, value);
                    return vo;

                case 0x20:
                    vo.AtkNormalAddRate = MutilPAdd(vo.AtkNormalAddRate, value);
                    return vo;

                case 0x23:
                    vo.DmgBackPRate = MutilPAdd(vo.DmgBackPRate, value);
                    return vo;

                case 0x24:
                    vo.DmgBackMRate = MutilPAdd(vo.DmgBackMRate, value);
                    return vo;
            }
            return vo;
        }

        public static FightAttributeVo AttributeAddValueByPer(FightAttributeVo att, BaseRoleVo baseVo, int type, int value1, int value2)
        {
            FightAttributeVo vo = att;
            switch (type)
            {
                case 10:
                    vo.Hp += CalcPenValue(baseVo.CurHp, value1, value2);
                    return vo;

                case 11:
                    vo.HpFull += CalcPenValue(baseVo.Hp, value1, value2);
                    return vo;

                case 12:
                    vo.AttrPhySum += CalcPenValue(baseVo.AttrPhy, value1, value2);
                    return vo;

                case 13:
                    vo.AttrMagSum += CalcPenValue(baseVo.AttrMag, value1, value2);
                    return vo;

                case 14:
                    vo.DefPhySum += CalcPenValue(baseVo.DefP, value1, value2);
                    return vo;

                case 15:
                    vo.DefMagSum += CalcPenValue(baseVo.DefM, value1, value2);
                    return vo;
            }
            return vo;
        }

        public static int CalcPenValue(uint value, int value1, int value2)
        {
            return Convert.ToInt32(Mathf.Round((float) ((value * value1) / ((long) value2))));
        }

        public void Clear()
        {
            this._attrPhySum = 0;
            this._attrMagSum = 0;
            this._defPhySum = 0;
            this._defMagSum = 0;
            this._hpSum = 0;
            this._hpFullSum = 0;
            this._moveSpeedRate = 1f;
            this._attPVamRate = 1f;
            this._attMVamRate = 1f;
            this._hitRate = 1f;
            this._dodgeRate = 1f;
            this._critRate = 1f;
            this._flexRate = 1f;
            this._critAddRate = 1f;
            this._atkSpeedAddRate = 1f;
            this._atkCdDecRate = 1f;
            this._atkNormalAddRate = 1f;
            this._dmgBackPRate = 1f;
            this._dmgBackMRate = 1f;
            this._hpReRate = 1f;
        }

        public static FightAttributeVo GetAttributeInfo(string str, BaseRoleVo baseVo)
        {
            FightAttributeVo fightAttVo = baseVo.fightAttVo;
            char[] separator = new char[] { '|' };
            string[] strArray = str.Replace("[[", string.Empty).Replace("]]", string.Empty).Replace("],[", "|").Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] chArray2 = new char[] { ',' };
                string[] strArray2 = strArray[i].Split(chArray2);
                int num2 = 0;
                if (strArray2.Length == 2)
                {
                    num2 = int.Parse(strArray2[1].ToString());
                    fightAttVo = AttributeAddValue(fightAttVo, int.Parse(strArray2[0].ToString()), num2);
                }
                else if (strArray2.Length == 3)
                {
                    int.Parse(strArray2[1].ToString());
                    int.Parse(strArray2[2].ToString());
                    fightAttVo = AttributeAddValueByPer(fightAttVo, baseVo, int.Parse(strArray2[0].ToString()), int.Parse(strArray2[1].ToString()), int.Parse(strArray2[2].ToString()));
                }
            }
            return fightAttVo;
        }

        public static float MutilPAdd(float val, int value)
        {
            float num = value * 0.0001f;
            return (val * (1f + num));
        }

        public static float MutilPAdd(float vall, float val2)
        {
            return (vall * val2);
        }

        public static float MutilPDec(float val, int value)
        {
            float num = value * 0.0001f;
            return (val * (1f - num));
        }

        public FightAttributeVo ShadowCopy()
        {
            return (FightAttributeVo) base.MemberwiseClone();
        }

        public int AbHurt
        {
            get
            {
                return this._abHurt;
            }
            set
            {
                this._abHurt = value;
            }
        }

        public float AtkCdDecRate
        {
            get
            {
                return this._atkCdDecRate;
            }
            set
            {
                this._atkCdDecRate = value;
            }
        }

        public float AtkNormalAddRate
        {
            get
            {
                return this._atkNormalAddRate;
            }
            set
            {
                this._atkNormalAddRate = value;
            }
        }

        public float AtkSpeedAddRate
        {
            get
            {
                return this._atkSpeedAddRate;
            }
            set
            {
                this._atkSpeedAddRate = value;
            }
        }

        public float AttMVamRate
        {
            get
            {
                return this._attMVamRate;
            }
            set
            {
                this._attMVamRate = value;
            }
        }

        public float AttPVamRate
        {
            get
            {
                return this._attPVamRate;
            }
            set
            {
                this._attPVamRate = value;
            }
        }

        public int AttrMagSum
        {
            get
            {
                return this._attrMagSum;
            }
            set
            {
                this._attrMagSum = value;
            }
        }

        public int AttrPhySum
        {
            get
            {
                return this._attrPhySum;
            }
            set
            {
                this._attrPhySum = value;
            }
        }

        public float CritAddRate
        {
            get
            {
                return this._critAddRate;
            }
            set
            {
                this._critAddRate = value;
            }
        }

        public float CritRate
        {
            get
            {
                return this._critRate;
            }
            set
            {
                this._critRate = value;
            }
        }

        public int DefMagSum
        {
            get
            {
                return this._defMagSum;
            }
            set
            {
                this._defMagSum = value;
            }
        }

        public int DefPhySum
        {
            get
            {
                return this._defPhySum;
            }
            set
            {
                this._defPhySum = value;
            }
        }

        public float DmgBackMRate
        {
            get
            {
                return this._dmgBackMRate;
            }
            set
            {
                this._dmgBackMRate = value;
            }
        }

        public float DmgBackPRate
        {
            get
            {
                return this._dmgBackPRate;
            }
            set
            {
                this._dmgBackPRate = value;
            }
        }

        public float DodgeRate
        {
            get
            {
                return this._dodgeRate;
            }
            set
            {
                this._dodgeRate = value;
            }
        }

        public float FlexRate
        {
            get
            {
                return this._flexRate;
            }
            set
            {
                this._flexRate = value;
            }
        }

        public float HitRate
        {
            get
            {
                return this._hitRate;
            }
            set
            {
                this._hitRate = value;
            }
        }

        public int Hp
        {
            get
            {
                return this._hpSum;
            }
            set
            {
                this._hpSum = value;
            }
        }

        public int HpFull
        {
            get
            {
                return this._hpFullSum;
            }
            set
            {
                this._hpFullSum = value;
            }
        }

        public float HpReRate
        {
            get
            {
                return this._hpReRate;
            }
            set
            {
                this._hpReRate = value;
            }
        }

        public float MoveSpeedRate
        {
            get
            {
                return this._moveSpeedRate;
            }
            set
            {
                this._moveSpeedRate = value;
            }
        }
    }
}

