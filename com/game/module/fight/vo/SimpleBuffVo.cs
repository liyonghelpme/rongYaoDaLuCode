﻿namespace com.game.module.fight.vo
{
    using com.u3d.bases.display;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class SimpleBuffVo
    {
        public int _hp;
        private const int _UINIKEY_RATE = 0x3e8;
        public ActionDisplay attackDisplay;
        public int attcedAddRate;
        public uint BuffId;
        public uint BuffLevel;
        public bool ownHpFlag;
        public int syncUnikey;
        public uint UniKey;
        public int val1;
        public int val2;

        public SimpleBuffVo(PDamageBuff buff)
        {
            this.BuffId = buff.buffId;
            this.BuffLevel = buff.lvl;
            this.UniKey = getUnikey(buff.buffId, buff.lvl);
        }

        public SimpleBuffVo(uint buffId, uint buffLevel, ActionDisplay inAttackDisplay = null)
        {
            if (inAttackDisplay != null)
            {
                this.attackDisplay = inAttackDisplay;
            }
            this.BuffId = buffId;
            this.BuffLevel = buffLevel;
            this.UniKey = getUnikey(buffId, buffLevel);
        }

        public static uint getUnikey(uint buffId, uint buffLevel)
        {
            return ((buffId * 0x3e8) + buffLevel);
        }

        public static KeyValuePair<uint, uint> ParseUnikey(int unikey)
        {
            uint key = (uint) (unikey / 0x3e8);
            return new KeyValuePair<uint, uint>(key, (uint) (unikey % 0x3e8));
        }

        public void SetHpRate(int hp)
        {
            this._hp = hp;
            if (hp > 0)
            {
                this.ownHpFlag = true;
            }
        }
    }
}

