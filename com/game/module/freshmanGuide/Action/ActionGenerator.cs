﻿namespace com.game.module.freshmanGuide.Action
{
    using System;

    public class ActionGenerator
    {
        public static IAction GetActionInstance(int type)
        {
            IAction action = null;
            switch (type)
            {
                case 0:
                    return new BaseRoleAction();

                case 1:
                    return action;

                case 2:
                    return new CameraMoveAction();

                case 3:
                    return new DialogAction();

                case 4:
                    return new EffectAction();

                case 5:
                    return new GuideAction();

                case 6:
                    return new CreateMonsterAction();

                case 7:
                    return new KillMonsterAction();
            }
            return action;
        }
    }
}

