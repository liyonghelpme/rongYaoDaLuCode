﻿namespace com.game.module.freshmanGuide.Action
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class ActionManager : BaseAction, IAction, IActionManager
    {
        protected List<IAction> actions;

        public ActionManager() : base(0)
        {
            this.actions = new List<IAction>();
        }

        public void AddAction(IAction action)
        {
            if ((action != null) && (this.actions.IndexOf(action) == -1))
            {
                this.actions.Add(action);
            }
        }

        public void Dispose()
        {
            foreach (IAction action in this.actions)
            {
                action.Dispose();
            }
            this.actions = null;
        }

        public IAction GetAction(int actionType)
        {
            return null;
        }

        public void RemoveAction(IAction action)
        {
            if (action != null)
            {
                this.actions.Remove(action);
            }
        }

        public virtual void Update(int times, float dt = 0.04f)
        {
            if (this.actions != null)
            {
                int count = this.actions.Count;
                for (int i = 0; i < count; i++)
                {
                    IAction item = this.actions[i];
                    if (item.HadDoInit)
                    {
                        item.Init();
                    }
                    if (!item.IsFinished)
                    {
                        item.Update(times, dt);
                    }
                    if (item.IsFinished)
                    {
                        item.ManagerClear();
                        this.actions.Remove(item);
                    }
                }
            }
        }

        public List<IAction> Actions
        {
            get
            {
                return this.actions;
            }
        }
    }
}

