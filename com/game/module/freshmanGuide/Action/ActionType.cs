﻿namespace com.game.module.freshmanGuide.Action
{
    using System;

    public class ActionType
    {
        public const int cameraAction = 2;
        public const int createMonster = 6;
        public const int dialogAction = 3;
        public const int effectAction = 4;
        public const int guideAction = 5;
        public const int killMonster = 7;
        public const int monsterAction = 1;
        public const int playerAction = 0;
    }
}

