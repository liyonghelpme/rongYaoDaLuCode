﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game.data;
    using com.game.module.core;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class BaseAction : IAction
    {
        protected bool hadDoInit;
        protected bool isFinish;
        protected int level;
        protected int life;
        protected IActionManager manager;

        public BaseAction(int level = 0)
        {
            level = 0;
            this.isFinish = false;
            this.life = 0x7fffffff;
        }

        public virtual void Configure(SysStoryVo template, string scripts)
        {
            this.life = template.time * 0x19;
            if (template.time == 0x5dc)
            {
                this.life = 0x7fffffff;
            }
            this.StoryId = template.id;
        }

        public void Dispose()
        {
        }

        public void DoRemove(IActionManager manager)
        {
        }

        public virtual void Exit()
        {
            Singleton<FreshmanGuideMode>.Instance.SendStoryProgress(this.StoryId);
        }

        public virtual void Init()
        {
            this.hadDoInit = true;
        }

        public void ManagerClear()
        {
            this.manager = null;
        }

        public void SetManager(IActionManager manager)
        {
            this.manager = manager;
        }

        public void Skip()
        {
            this.isFinish = true;
        }

        public virtual void Update(int times, float dt = 0.04f)
        {
            this.life -= times;
            if (this.life <= 0)
            {
                this.isFinish = true;
            }
            if (this.isFinish)
            {
                this.Exit();
            }
        }

        public virtual int GetType
        {
            get
            {
                return 0;
            }
        }

        public bool HadDoInit
        {
            get
            {
                return this.hadDoInit;
            }
        }

        public bool IsFinished
        {
            get
            {
                return this.isFinish;
            }
            set
            {
                this.isFinish = value;
            }
        }

        public int Level
        {
            get
            {
                return this.level;
            }
            set
            {
                this.level = value;
            }
        }

        public uint StoryId { get; set; }
    }
}

