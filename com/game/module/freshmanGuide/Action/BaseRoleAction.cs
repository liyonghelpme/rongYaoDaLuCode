﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game;
    using com.game.data;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.fsmUtil;
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BaseRoleAction : BaseAction
    {
        private int actionId;
        private Vector3 endPos;
        private object param;
        private BaseDisplay player;
        private int roleId;
        private Vector3 startPos;

        public BaseRoleAction() : base(0)
        {
        }

        private void CheckDelayStopAI()
        {
            if (this.player.Controller.AiController.IsAiEnable)
            {
                Main.mainObj.StartCoroutine(this.DelayStopAI());
            }
        }

        public override void Configure(SysStoryVo template, string scripts)
        {
            base.Configure(template, scripts);
            string script = ParseActionScript.ParseStoryScript(scripts)[1];
            string[] strArray = ParseActionScript.ParseAnimationScript(script);
            this.actionId = int.Parse(strArray[0]);
            this.player = AppMap.Instance.me;
        }

        private IEnumerator DelayStopAI()
        {
            Main.mainObj.StartCoroutine(tools.WaitforSeveralFrames(10));
            this.player.Controller.AiController.SetAi(false);
            ActionControler controller = this.player.Controller as ActionControler;
            return null;
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Init()
        {
            base.Init();
            this.CheckDelayStopAI();
            this.player.Controller.StatuController.SetStatu(this.actionId);
        }

        public override void Update(int times, float dt = 0.04f)
        {
            base.Update(times, dt);
        }

        private void WalkEnd(BaseControler controller)
        {
            this.IsFinished = true;
        }
    }
}

