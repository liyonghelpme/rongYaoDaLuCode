﻿namespace com.game.module.freshmanGuide.Action
{
    using System;

    public class CameraEffectType
    {
        public const uint LOCK_TARGET = 0;
        public const uint SHAKE = 1;
        public const uint WANDER = 2;
    }
}

