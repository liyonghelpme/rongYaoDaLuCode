﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.freshmanGuide;
    using com.game.utils;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CameraMoveAction : BaseAction
    {
        private MouseOrbit camera;
        private uint cameraActionType;
        private Vector3[] paths;
        private GameObject target;

        public CameraMoveAction() : base(0)
        {
        }

        public override void Configure(SysStoryVo template, string scripts)
        {
            base.Configure(template, scripts);
            string[] strArray = ParseActionScript.ParseCameraScript(ParseActionScript.ParseStoryScript(scripts)[1]);
            this.cameraActionType = uint.Parse(strArray[0]);
            if (this.cameraActionType == 2)
            {
                for (int i = 1; i < strArray.Length; i++)
                {
                    string s = strArray[i];
                    SysCameraInfoVo cameraInfoVo = BaseDataMgr.instance.GetCameraInfoVo(uint.Parse(s));
                    this.paths[i - 1] = StringUtils.StringToVector3(cameraInfoVo.cameraPos);
                }
            }
        }

        public override void Exit()
        {
            base.Exit();
            this.target = AppMap.Instance.me.GoBase;
            this.camera.setTarget(this.target, false);
        }

        public override void Init()
        {
            base.Init();
            this.camera = Camera.main.gameObject.GetComponent<MouseOrbit>();
            switch (this.cameraActionType)
            {
                case 0:
                    this.target = AppMap.Instance.monsterList[0].GoBase;
                    this.camera.setTarget(this.target, false);
                    break;

                case 1:
                    this.camera.ShakeCamera(new Vector3(0.5f, 0.25f, 0.2f), 3f);
                    break;

                case 2:
                    CameraMoveController.StartController(this.paths);
                    break;
            }
        }

        public override void Update(int times, float dt = 0.04f)
        {
            base.Update(times, dt);
        }
    }
}

