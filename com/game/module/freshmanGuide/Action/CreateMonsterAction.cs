﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game.data;
    using com.game.module.core;
    using System;
    using System.Runtime.InteropServices;

    public class CreateMonsterAction : BaseAction
    {
        public CreateMonsterAction() : base(0)
        {
        }

        public override void Configure(SysStoryVo template, string scripts)
        {
            base.Configure(template, scripts);
        }

        public override void Exit()
        {
            base.Exit();
        }

        public override void Init()
        {
            base.Init();
            Singleton<MonsterActivator>.Instance.mobCache.NextStage();
        }

        public override void Update(int times, float dt = 0.04f)
        {
            base.Update(times, dt);
        }
    }
}

