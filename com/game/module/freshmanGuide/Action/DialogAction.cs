﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.NpcDialog;
    using com.game.vo;
    using com.u3d.bases.consts;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.joystick;
    using System;
    using System.Runtime.InteropServices;

    public class DialogAction : BaseAction
    {
        private string dialogContent;
        private FinishDialogCallback endCallback;
        private object param;

        public DialogAction() : base(0)
        {
        }

        public override void Configure(SysStoryVo template, string scripts)
        {
            base.Configure(template, scripts);
            uint id = ParseActionScript.ParseDialogScript(ParseActionScript.ParseStoryScript(scripts)[1]);
            this.dialogContent = DescriptManager.GetText(id);
        }

        public override void Exit()
        {
            base.Exit();
        }

        private void FinishDilogCallback()
        {
            this.IsFinished = true;
            this.Exit();
            if (Singleton<MTaskMgr>.Instance.mainTask.isFinished)
            {
                DungeonMgr.Instance.SendOver();
            }
        }

        public override void Init()
        {
            uint clientCurProgress = Singleton<FreshmanGuideMode>.Instance.clientCurProgress;
            if (Singleton<FreshmanGuideController>.Instance.root.storyRoot.Actions.Count > 1)
            {
                ParallerActionManager manager = Singleton<FreshmanGuideController>.Instance.root.storyRoot.Actions[1] as ParallerActionManager;
                GuideAction action = manager.Actions[0] as GuideAction;
                if ((action != null) && (action.guideType == 5))
                {
                    if (!Singleton<BattleView>.Instance.IsOpened)
                    {
                        Singleton<BattleView>.Instance.OpenView();
                        Singleton<BattleBottomRightView>.Instance.SkillButtonList[2].SetActive(false);
                        Singleton<BattleTopLeftView>.Instance.HideAutoAttackBtn(false);
                    }
                    MonsterVo vo = AppMap.Instance.monsterList[0].GetVo() as MonsterVo;
                    if (vo.CurHp > (vo.Hp * 0.8))
                    {
                        return;
                    }
                    ActionControler controller = AppMap.Instance.monsterList[0].Controller as ActionControler;
                    controller.StatuController.SetStatu(8);
                    controller.StatuController.Animator.Play(Status.NAME_HASH_SKILL2);
                }
            }
            base.Init();
            Singleton<BattleView>.Instance.CloseView();
            JoystickController.instance.Reset();
            (AppMap.Instance.me.Controller as ActionControler).StopWalk();
            Singleton<DialogAnimPanel>.Instance.Show(this.dialogContent, new FinishDialogCallback(this.FinishDilogCallback));
        }

        public override void Update(int times, float dt = 0.04f)
        {
            base.Update(times, dt);
        }
    }
}

