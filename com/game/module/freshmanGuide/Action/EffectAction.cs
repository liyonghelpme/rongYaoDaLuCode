﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game.data;
    using com.game.module.effect;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class EffectAction : BaseAction
    {
        private List<EffectControlerII> effectControllerList;
        private List<EffectInfo> effectInfolist;

        public EffectAction() : base(0)
        {
            this.effectInfolist = new List<EffectInfo>();
        }

        public override void Configure(SysStoryVo template, string scripts)
        {
            base.Configure(template, scripts);
            this.effectInfolist = ParseActionScript.ParseEffectScript(ParseActionScript.ParseStoryScript(scripts)[1]);
        }

        private void CreateCallBack(EffectControlerII controller)
        {
            this.effectControllerList.Add(controller);
        }

        public override void Exit()
        {
            base.Exit();
            foreach (EffectControlerII rii in this.effectControllerList)
            {
                rii.Clear();
            }
        }

        public override void Init()
        {
            base.Init();
            this.effectControllerList = new List<EffectControlerII>();
            foreach (EffectInfo info in this.effectInfolist)
            {
                EffectMgr.Instance.CreateSceneEffect(info.id, info.pos, null, true, true, new Effect.EffectCreateCallback(this.CreateCallBack), null);
            }
        }

        public override void Update(int times, float dt = 0.04f)
        {
            base.Update(times, dt);
        }
    }
}

