﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game;
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Guide;
    using com.game.module.map;
    using com.game.vo;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class GuideAction : BaseAction
    {
        public int guideType;

        public GuideAction() : base(0)
        {
        }

        private void Callback()
        {
            if (!this.IsFinished)
            {
                this.IsFinished = true;
                if (((this.guideType == 2) || (this.guideType == 3)) || ((this.guideType == 4) || (this.guideType == 5)))
                {
                    this.IsFinished = false;
                }
                if (this.IsFinished)
                {
                    this.Exit();
                }
            }
        }

        public override void Configure(SysStoryVo template, string scripts)
        {
            base.Configure(template, scripts);
            this.guideType = ParseActionScript.ParseMoveScript(ParseActionScript.ParseStoryScript(scripts)[1]);
        }

        public override void Exit()
        {
            base.Exit();
        }

        private void GuideSkillByMonsterGroupId(int groupId)
        {
            if (!this.IsFinished && AppMap.Instance.isAllDeadByGroup(groupId))
            {
                this.IsFinished = true;
                Singleton<BattleMode>.Instance.SetAllSelfAndMonstersAI(false, false);
                (AppMap.Instance.me.Controller as ActionControler).StopWalk();
                GlobalAPI.facade.Remove(1, new NoticeListener(this.OnKillMonster));
            }
        }

        public override void Init()
        {
            if (this.guideType <= 5)
            {
                base.Init();
                Singleton<GuideMode>.Instance.StartGuide(this.guideType, new GuideBase.GuideFinishCallBack(this.Callback));
                Singleton<BattleView>.Instance.OpenView();
                GlobalAPI.facade.Add(1, new NoticeListener(this.OnKillMonster));
                if (this.guideType != 1)
                {
                    IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            enumerator.Current.ReadyToUseAi(0f);
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                }
                if (this.guideType == 5)
                {
                    MapCameraScriptData.setCameraByScriptId(0x5f45a6c);
                }
            }
            else
            {
                base.Init();
                Singleton<GuideMode>.Instance.StartGuide(this.guideType, new GuideBase.GuideFinishCallBack(this.Callback));
            }
        }

        private void OnKillMonster(int type, int v1, int v2, object data)
        {
            MonsterVo vo = data as MonsterVo;
            this.GuideSkillByMonsterGroupId(vo.groupIndex);
        }

        public override void Update(int times, float dt = 0.04f)
        {
            base.Update(times, dt);
        }
    }
}

