﻿namespace com.game.module.freshmanGuide.Action
{
    using System;

    public class GuideType
    {
        public const int GUIDE_BOX = 7;
        public const int GUIDE_DUNGEON_1 = 6;
        public const int GUIDE_DUNGEON_2 = 9;
        public const int GUIDE_DUNGEON_3 = 11;
        public const int GUIDE_HERO_1 = 8;
        public const int GUIDE_HERO_2 = 10;
        public const int MOVE = 1;
        public const int NORMAL_ATTACK = 2;
        public const int SKILL1 = 3;
        public const int SKILL2 = 4;
        public const int SKILL3 = 5;
    }
}

