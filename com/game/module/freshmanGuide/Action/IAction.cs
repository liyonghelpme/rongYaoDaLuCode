﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game.data;
    using System;
    using System.Runtime.InteropServices;

    public interface IAction
    {
        void Configure(SysStoryVo template, string script);
        void Dispose();
        void DoRemove(IActionManager manager);
        void Init();
        void ManagerClear();
        void SetManager(IActionManager manager);
        void Skip();
        void Update(int times, float dt = 0.04f);

        int GetType { get; }

        bool HadDoInit { get; }

        bool IsFinished { get; set; }

        int Level { get; set; }
    }
}

