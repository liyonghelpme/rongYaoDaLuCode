﻿namespace com.game.module.freshmanGuide.Action
{
    using System;
    using System.Collections.Generic;

    public interface IActionManager : IAction
    {
        void AddAction(IAction action);
        void Dispose();
        IAction GetAction(int actionType);
        void RemoveAction(IAction action);

        List<IAction> Actions { get; }
    }
}

