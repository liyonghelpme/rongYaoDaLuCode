﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game;
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.vo;
    using System;
    using System.Runtime.InteropServices;

    public class KillMonsterAction : BaseAction
    {
        public KillMonsterAction() : base(0)
        {
        }

        public override void Configure(SysStoryVo template, string scripts)
        {
            base.Configure(template, scripts);
        }

        public override void Exit()
        {
            base.Exit();
        }

        private void GuideSkillByMonsterGroupId(int groupId)
        {
            if (AppMap.Instance.isAllDeadByGroup(groupId))
            {
                this.IsFinished = true;
            }
        }

        public override void Init()
        {
            base.Init();
            GlobalAPI.facade.Add(1, new NoticeListener(this.OnKillMonster));
        }

        private void OnKillMonster(int type, int v1, int v2, object data)
        {
            MonsterVo vo = data as MonsterVo;
            this.GuideSkillByMonsterGroupId(vo.groupIndex);
        }

        public override void Update(int times, float dt = 0.04f)
        {
            base.Update(times, dt);
        }
    }
}

