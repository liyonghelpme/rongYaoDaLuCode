﻿namespace com.game.module.freshmanGuide.Action
{
    using System;
    using System.Runtime.InteropServices;

    public class ParallerActionManager : ActionManager
    {
        public override void Update(int times, float dt = 0.04f)
        {
            if (base.actions.Count == 0)
            {
                this.IsFinished = true;
            }
            if (base.actions != null)
            {
                for (int i = 0; i < base.actions.Count; i++)
                {
                    IAction item = base.actions[i];
                    if (!item.HadDoInit)
                    {
                        item.Init();
                    }
                    if (!item.IsFinished)
                    {
                        item.Update(times, dt);
                    }
                    if (item.IsFinished)
                    {
                        item.ManagerClear();
                        base.actions.Remove(item);
                    }
                }
            }
        }
    }
}

