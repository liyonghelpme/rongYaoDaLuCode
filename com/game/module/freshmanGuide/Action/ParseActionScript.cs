﻿namespace com.game.module.freshmanGuide.Action
{
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ParseActionScript
    {
        public static string[] ParseAnimationScript(string script)
        {
            script = StringUtils.GetValueString(script);
            script = script.Replace("][", "|");
            char[] separator = new char[] { '|' };
            return script.Split(separator);
        }

        public static string[] ParseCameraScript(string script)
        {
            script = StringUtils.GetValueString(script);
            char[] separator = new char[] { ',' };
            return script.Split(separator);
        }

        public static uint ParseDialogScript(string script)
        {
            script = StringUtils.GetValueString(script);
            return Convert.ToUInt32(script);
        }

        public static List<EffectInfo> ParseEffectScript(string script)
        {
            List<EffectInfo> list = new List<EffectInfo>();
            char[] separator = new char[] { '#' };
            foreach (string str in script.Split(separator))
            {
                char[] chArray2 = new char[] { '|' };
                string[] strArray2 = StringUtils.GetValueString(str).Replace("][", "|").Split(chArray2);
                char[] chArray3 = new char[] { ',' };
                string[] strArray3 = strArray2[1].Split(chArray3);
                float x = float.Parse(strArray3[0]) / 1000f;
                float y = float.Parse(strArray3[1]) / 1000f;
                float z = float.Parse(strArray3[2]) / 1000f;
                EffectInfo item = new EffectInfo {
                    id = strArray2[0],
                    pos = new Vector3(x, y, z)
                };
                list.Add(item);
            }
            return list;
        }

        public static int ParseMoveScript(string script)
        {
            script = StringUtils.GetValueString(script);
            return Convert.ToInt32(script);
        }

        public static string[] ParseParallerStory(string script)
        {
            char[] separator = new char[] { '$' };
            return script.Split(separator);
        }

        public static string[] ParseStoryScript(string script)
        {
            char[] separator = new char[] { '|' };
            return script.Split(separator);
        }
    }
}

