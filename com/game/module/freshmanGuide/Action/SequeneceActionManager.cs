﻿namespace com.game.module.freshmanGuide.Action
{
    using System;
    using System.Runtime.InteropServices;

    public class SequeneceActionManager : ActionManager
    {
        public override void Update(int times, float dt = 0.04f)
        {
            if (base.actions.Count > 0)
            {
                IAction action = base.actions[0];
                if (!action.HadDoInit)
                {
                    action.Init();
                }
                if (!action.IsFinished)
                {
                    action.Update(times, dt);
                }
                if (action.IsFinished)
                {
                    action.ManagerClear();
                    action = null;
                    base.actions.RemoveAt(0);
                }
            }
            else
            {
                this.IsFinished = true;
            }
        }
    }
}

