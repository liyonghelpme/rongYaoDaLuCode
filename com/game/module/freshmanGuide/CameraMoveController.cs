﻿namespace com.game.module.freshmanGuide
{
    using System;
    using System.Collections;
    using UnityEngine;

    public class CameraMoveController
    {
        public static void StartController(Vector3[] paths)
        {
            Hashtable args = new Hashtable();
            args.Add("path", paths);
            args.Add("easeType", iTween.EaseType.linear);
            args.Add("speed", 5f);
            iTween.MoveTo(Camera.main.gameObject, args);
        }
    }
}

