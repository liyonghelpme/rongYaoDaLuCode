﻿namespace com.game.module.freshmanGuide
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class FreshmanGuideController : BaseControl<FreshmanGuideController>
    {
        public StroyActionRoot root;

        private void GetStoryProgress(INetData data)
        {
            RoleGetPlotMsg_3_13 g__ = new RoleGetPlotMsg_3_13();
            g__.read(data.GetMemoryStream());
            Singleton<FreshmanGuideMode>.Instance.UpdateStoryProgress(g__.plot);
        }

        public void InitFreshmanGuide()
        {
            if (this.root == null)
            {
                this.root = new StroyActionRoot();
                Singleton<FreshmanGuideMode>.Instance.RequestProgress();
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("781", new NetMsgCallback(this.GetStoryProgress));
            AppNet.main.addCMD("782", new NetMsgCallback(this.UpdataProgress));
        }

        private void UpdataProgress(INetData data)
        {
            RoleSetPlotMsg_3_14 g__ = new RoleSetPlotMsg_3_14();
            g__.read(data.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError((ushort) g__.code);
            }
        }
    }
}

