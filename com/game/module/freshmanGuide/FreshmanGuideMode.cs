﻿namespace com.game.module.freshmanGuide
{
    using com.game;
    using com.game.module.core;
    using Proto;
    using System;
    using System.IO;

    public class FreshmanGuideMode : BaseMode<FreshmanGuideMode>
    {
        public uint clientCurProgress;
        public uint currentProgress;
        public readonly int UPDATE_PROGRESS = 1;

        public void RequestProgress()
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_13(msdata);
            AppNet.gameNet.send(msdata, 3, 13);
        }

        public void SendStoryProgress(uint storyId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_3.write_3_14(msdata, storyId);
            AppNet.gameNet.send(msdata, 3, 14);
        }

        public void UpdateStoryProgress(uint id)
        {
            this.currentProgress = id;
            base.DataUpdate(this.UPDATE_PROGRESS);
        }
    }
}

