﻿namespace com.game.module.freshmanGuide
{
    using com.game.basic;
    using com.game.data;
    using com.game.Interface.Tick;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.freshmanGuide.Action;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class StroyActionRoot : ITick
    {
        public IActionManager storyRoot;
        private int timers;

        public StroyActionRoot()
        {
            GlobalAPI.tickManager.addTick(this);
            this.Register();
        }

        private void ConfigureStory()
        {
            Dictionary<uint, SysStoryVo> storyListByType = this.GetStoryListByType(MeVo.instance.stylebin.id);
            this.storyRoot = new SequeneceActionManager();
            uint currentProgress = Singleton<FreshmanGuideMode>.Instance.currentProgress;
            if (MeVo.instance.needEnterDuplicate)
            {
                currentProgress = 0;
            }
            foreach (SysStoryVo vo in storyListByType.Values)
            {
                string[] strArray = ParseActionScript.ParseParallerStory(vo.script);
                ParallerActionManager manager = new ParallerActionManager();
                for (int i = 0; i < strArray.Length; i++)
                {
                    IAction actionInstance = ActionGenerator.GetActionInstance(int.Parse(ParseActionScript.ParseStoryScript(strArray[i])[0]));
                    actionInstance.Configure(vo, strArray[i]);
                    if (currentProgress < vo.id)
                    {
                        manager.AddAction(actionInstance);
                    }
                }
                if (currentProgress < vo.id)
                {
                    this.storyRoot.AddAction(manager);
                }
            }
        }

        private Dictionary<uint, SysStoryVo> GetStoryListByType(int type)
        {
            Dictionary<uint, SysStoryVo> dictionary = new Dictionary<uint, SysStoryVo>();
            foreach (object obj2 in BaseDataMgr.instance.GetStoryVoList().Values)
            {
                SysStoryVo vo = obj2 as SysStoryVo;
                if (!MeVo.instance.needEnterDuplicate && (DungeonMgr.Instance.curDungeonId != Singleton<ConfigConst>.Instance.GetConfigData("NEW_GUIDE_DUPLICATE")))
                {
                    if (vo.type == 0)
                    {
                        dictionary.Add(vo.id, vo);
                    }
                }
                else if ((vo.type == type) || (vo.type == 0))
                {
                    dictionary.Add(vo.id, vo);
                }
            }
            return dictionary;
        }

        private void Register()
        {
            Singleton<FreshmanGuideMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateProgress);
        }

        public void Update(int times, float dt = 0.04f)
        {
            this.timers += times;
            if (((this.timers >= 0x19) && (this.storyRoot != null)) && !this.storyRoot.IsFinished)
            {
                this.storyRoot.Update(times, dt);
            }
            if ((this.storyRoot != null) && this.storyRoot.IsFinished)
            {
                GlobalAPI.tickManager.removeTick(this);
            }
        }

        private void UpdateProgress(object sender, int type)
        {
            if (Singleton<FreshmanGuideMode>.Instance.UPDATE_PROGRESS == type)
            {
                this.ConfigureStory();
            }
        }
    }
}

