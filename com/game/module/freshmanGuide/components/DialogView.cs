﻿namespace com.game.module.freshmanGuide.components
{
    using com.game.module.core;
    using System;

    public class DialogView : BaseView<DialogView>
    {
        private Button _btn;
        private UISprite _icon1;
        private UISprite _icon2;
        private UILabel _label;
        private string des;
        private int headId;

        public override void CancelUpdateHandler()
        {
        }

        public override void CloseView()
        {
            base.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            this._label.text = this.des;
        }

        protected override void Init()
        {
            this._label = base.FindInChild<UILabel>("label");
            this._btn = base.FindInChild<Button>("btn");
            this._icon1 = base.FindInChild<UISprite>("headIncon1");
            this._icon2 = base.FindInChild<UISprite>("headIncon1");
        }

        public override void RegisterUpdateHandler()
        {
        }

        public void Show(int headId, string descript)
        {
            this.headId = headId;
            this.des = descript;
            this.OpenView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopUILayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/DialogAnim/DialogAnimPanel.assetbundle";
            }
        }
    }
}

