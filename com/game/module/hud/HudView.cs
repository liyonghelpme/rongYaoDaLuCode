﻿namespace com.game.module.hud
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class HudView : MonoBehaviour
    {
        private readonly Dictionary<string, GameObject> _url2PrefabDic = new Dictionary<string, GameObject>();
        protected GameObject BossMsgGo;
        public const string BossMsgUrl = "UI/Hud/Widget/BossMsg.assetbundle";
        protected GameObject ExclamationGo;
        private const string FilterZhUrl = "Xml/filters_zh.assetbundle";
        public const string HeadInfo = "UI/Hud/Widget/HeadInfo.assetbundle";
        public const string HeadInfoItemUrl = "UI/Hud/Widget/HeadInfoItem.assetbundle";
        protected GameObject HealthGo;
        public const string HealthUrl = "UI/Hud/Widget/Health.assetbundle";
        private const string hudText = "UI/Hud/Widget/HUDText.assetbundle";
        protected GameObject hudTextGo;
        protected GameObject InfoGo;
        public static HudView Instance;
        protected Dictionary<string, UIFont> Maps = new Dictionary<string, UIFont>();
        protected GameObject MonsterBloodGo;
        public const string MonsterBloodUrl = "UI/Hud/Widget/Blood.assetbundle";
        public GameObject MountPoint;
        protected GameObject NicknameGo;
        public const string NicknameUrl = "UI/Hud/Widget/Nickname.assetbundle";
        protected GameObject PlayerBloodGo;
        public const string PlayerBloodUrl = "UI/Hud/Widget/PlayerBlood.assetbundle";
        public const string Prefix = "UI/Hud";
        protected GameObject QuestionGo;
        protected GameObject QuestionGo2;
        public const string WarEffectUrl = "Effect/Buff/10091.assetbundle";

        private void AddDamage(Vector3 point, string damage, ActionDisplay enemyDisplay, int type = 0)
        {
            Transform transform = null;
            if (enemyDisplay != null)
            {
                if ((type == 2) || (type == 3))
                {
                    transform = enemyDisplay.GoBase.transform.Find("CriticalText");
                }
                else if (type == 1)
                {
                    transform = enemyDisplay.GoBase.transform.Find("HealtHudText");
                }
                else
                {
                    transform = enemyDisplay.GoBase.transform.Find("MyHudText");
                }
            }
            if (transform != null)
            {
                if ((type == 2) || (type == 3))
                {
                    transform.GetComponent<MyHudText>().AddCriticalText(string.Empty + damage, point, 0);
                }
                else if (type == 1)
                {
                    transform.GetComponent<MyHudText>().AddText("+" + damage, point, null);
                }
                else
                {
                    transform.GetComponent<MyHudText>().AddText(string.Empty + damage, point, null);
                }
            }
            else
            {
                GameObject obj2 = NGUITools.AddChild(HUDRoot.go, HUDRoot.instane.prefab);
                obj2.transform.position = this.WorldToUIPoint(point);
                obj2.GetComponent<HUDText>().Add(damage, 0.1f, type);
            }
        }

        public HeadAboveComponents AddHeadComponent(Transform target, PlayerVo playerVo)
        {
            GameObject go = NGUITools.AddChild(this.MountPoint);
            HeadAboveComponents components = go.AddMissingComponent<HeadAboveComponents>();
            go.name = "HeadComponents_" + playerVo.Id;
            UIFollowTarget target2 = go.AddMissingComponent<UIFollowTarget>();
            target2.target = target;
            target2.disableIfInvisible = true;
            return components;
        }

        public void AddHealthBar(BaseDisplay display)
        {
            if (display.Controller.GoHp == null)
            {
                GameObject obj2 = NGUITools.AddChild(display.Controller.gameObject);
                obj2.name = "blood";
                Transform target = obj2.transform;
                target.localPosition = new Vector3(0f, 2f, 0f);
                GameObject obj3 = Instance.AddMonsterBloodBar(target);
                display.Controller.GoHp = obj3;
            }
        }

        public void AddHealthBarAfterLoadingFinish(BaseDisplay display)
        {
            BaseRoleVo meVoByType = display.GetMeVoByType<BaseRoleVo>();
            if (meVoByType == null)
            {
                UnityEngine.Debug.LogError("加载血条时vo为null");
            }
            else
            {
                UnityLog.Log.AI(display.GoBase, "AddHealthBar ");
                if (display.Controller.GoHp == null)
                {
                    if (display is PlayerDisplay)
                    {
                        GameObject obj2 = NGUITools.AddChild(display.Controller.gameObject);
                        obj2.name = "blood";
                        Transform target = obj2.transform;
                        SkinnedMeshRenderer componentInChildren = display.GoCloth.GetComponentInChildren<SkinnedMeshRenderer>();
                        if (componentInChildren == null)
                        {
                            UnityEngine.Debug.LogError("SkinnedMeshRenderer取不到 报错");
                            UnityEngine.Debug.LogError(string.Concat(new object[] { "display = ", display.Key, " GoCloth: ", display.GoCloth.transform.position }));
                        }
                        else
                        {
                            float magnitude = componentInChildren.bounds.extents.magnitude;
                            target.localPosition = new Vector3(0f, magnitude, 0f);
                            GameObject obj3 = null;
                            obj3 = Instance.AddPlayerBloodBar(target, display);
                            display.Controller.GoHp = obj3;
                        }
                    }
                    else if (display is MonsterDisplay)
                    {
                        MonsterVo vo2 = (MonsterVo) meVoByType;
                        GameObject obj4 = NGUITools.AddChild(display.Controller.gameObject);
                        obj4.name = "blood";
                        Transform transform = obj4.transform;
                        transform.localPosition = new Vector3(0f, 1f, 0f);
                        GameObject obj5 = null;
                        int type = vo2.MonsterVO.type;
                        int quality = vo2.MonsterVO.quality;
                        bool flag = (type == 1) && (quality == 3);
                        if (AppMap.Instance.IsInWifiPVP)
                        {
                            UnityLog.Log.AI(display.GoBase, " Add HealthBar in WifiPvp");
                            switch (type)
                            {
                                case 5:
                                case 3:
                                    transform.localPosition = new Vector3(0f, 1.5f, 0f);
                                    obj5 = Instance.AddTowerBloodBar(transform);
                                    display.Controller.GoHp = obj5;
                                    return;
                            }
                            obj5 = Instance.AddMonsterBloodBar(transform);
                            display.Controller.GoHp = obj5;
                        }
                        else if (((type == 1) && !flag) || ((type == 4) || (type == 6)))
                        {
                            obj5 = Instance.AddMonsterBloodBar(transform);
                            display.Controller.GoHp = obj5;
                        }
                    }
                }
                else if (!display.Controller.GoHp.activeSelf)
                {
                    display.Controller.GoHp.SetActive(true);
                    this.RestoreToFullHp(display.Controller.GoHp);
                }
            }
        }

        public static void AddHpText(float cutHp, ActionDisplay display)
        {
            AddHpTextWithGameObject(Convert.ToInt32(cutHp), display, null);
        }

        public static void AddHpText(int cutHp, ActionDisplay enemyDisplay, ActionDisplay attackerDisplay)
        {
            GameObject whoAttack = (attackerDisplay != null) ? attackerDisplay.GoBase : null;
            AddHpTextWithGameObject(cutHp, enemyDisplay, whoAttack);
        }

        public static void AddHpTextWithGameObject(int cutHp, ActionDisplay enemyDisplay, GameObject whoAttack)
        {
            if (enemyDisplay.GoBase != null)
            {
                Vector3 position = enemyDisplay.GoBase.transform.position;
                Transform transform = Util.FindChildRecursive(enemyDisplay.GoBase.transform, "blood");
                if (transform != null)
                {
                    position = transform.position;
                }
                else
                {
                    Transform transform2 = Util.FindChildRecursive(enemyDisplay.GoBase.transform, "pivot");
                    if (transform2 != null)
                    {
                        position = transform2.transform.position;
                    }
                }
                Instance.CreateDamageHudText(position, false, false, false, cutHp, false, false, true, true, enemyDisplay, whoAttack);
            }
        }

        public GameObject AddItem(string itemName, Transform target, Type type)
        {
            if (target == null)
            {
                return null;
            }
            GameObject go = null;
            if (type == Type.Player)
            {
                go = NGUITools.AddChild(this.MountPoint, this.NicknameGo);
                go.AddMissingComponent<HeadAboveComponents>();
                go.name = "HeadComponents";
            }
            else if (type == Type.Npc)
            {
                go = NGUITools.AddChild(this.MountPoint, this.NicknameGo);
                UILabel componentInChildren = go.GetComponentInChildren<UILabel>();
                componentInChildren.color = Color.yellow;
                componentInChildren.text = itemName;
            }
            else if ((type == Type.Monster) && (itemName != "msg"))
            {
                go = NGUITools.AddChild(this.MountPoint, this.HealthGo);
            }
            else if ((type == Type.Monster) && (itemName == "msg"))
            {
                go = NGUITools.AddChild(this.MountPoint, this.BossMsgGo);
            }
            go.AddComponent<UIFollowTarget>().target = target;
            Vector3 vector = new Vector3(-1000f, 0f, 0f);
            go.transform.position = vector;
            return go;
        }

        public GameObject AddMonsterBloodBar(Transform target)
        {
            return this.NewAddMonsterBloodBar(target);
        }

        public void AddMonsterHealthBarWhenEmpty(ActionDisplay display, BaseRoleVo enemyVo, float hp, float curHp, float demageHp)
        {
            if (display.Controller.GoHp == null)
            {
                GameObject obj2 = NGUITools.AddChild(display.Controller.gameObject);
                obj2.name = "blood";
                Transform target = obj2.transform;
                target.localPosition = new Vector3(0f, 1f, 0f);
                GameObject go = null;
                if ((enemyVo as MonsterVo).MonsterVO.type != 2)
                {
                    go = Instance.AddMonsterBloodBar(target);
                }
                display.Controller.GoHp = go;
                base.StartCoroutine(this.UpdateHpAfterFrame(go, hp, curHp, demageHp));
            }
        }

        public GameObject AddPlayerBloodBar(Transform target, BaseDisplay display)
        {
            return this.NewAddPlayerBloodBar(target, display);
        }

        public void AddPlayerHealthBarWhenEmpty(ActionDisplay display)
        {
            if (display.Controller.GoHp == null)
            {
                GameObject obj2 = NGUITools.AddChild(display.Controller.gameObject);
                obj2.name = "blood";
                Transform target = obj2.transform;
                SkinnedMeshRenderer componentInChildren = display.GoCloth.GetComponentInChildren<SkinnedMeshRenderer>();
                if (componentInChildren == null)
                {
                    UnityEngine.Debug.LogError("SkinnedMeshRenderer取不到 报错");
                    UnityEngine.Debug.LogError(string.Concat(new object[] { "display = ", display.Key, " GoCloth: ", display.GoCloth.transform.position }));
                    return;
                }
                float magnitude = componentInChildren.bounds.extents.magnitude;
                int id = display.GetMeVoByType<PlayerVo>().generalTemplateInfo.id;
                float num3 = BaseDataMgr.instance.GetSysGeneralVo((uint) id).pivot_localposition_y * 0.001f;
                magnitude += num3;
                target.localPosition = new Vector3(0f, magnitude, 0f);
                GameObject obj3 = null;
                obj3 = Instance.AddPlayerBloodBar(target, display);
                display.Controller.GoHp = obj3;
            }
            if (!display.Controller.GoHp.activeSelf)
            {
                display.Controller.GoHp.SetActive(true);
            }
        }

        public GameObject AddTowerBloodBar(Transform target)
        {
            UnityLog.Log.AI(target, " AddTower BloodBar " + target);
            if (target == null)
            {
                return null;
            }
            GameObject g = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("bloodPlayer")) as GameObject;
            g.transform.parent = target;
            Util.InitGameObject(g);
            g.GetComponent<MyHealthBar>().SetHealthBar();
            return g.GetComponent<MyHealthBar>().healthBar;
        }

        private void Awake()
        {
            this.Init();
        }

        public void CreateDamageHudText(Vector3 target, bool isDodge, bool isCrit, bool isParry, int cutHp, bool isAppMe, bool isPlayer, bool isHeal, bool isFromMyCamp, ActionDisplay enemyDisplay, ActionDisplay whoAttack)
        {
            GameObject goBase;
            if ((whoAttack == null) || (whoAttack.GoBase == null))
            {
                goBase = null;
            }
            else
            {
                goBase = whoAttack.GoBase;
            }
            this.CreateDamageHudText(target, isDodge, isCrit, isParry, cutHp, isAppMe, isPlayer, isHeal, isFromMyCamp, enemyDisplay, goBase);
        }

        public void CreateDamageHudText(Vector3 target, bool isDodge, bool isCrit, bool isParry, int cutHp, bool isAppMe, bool isPlayer, bool isHeal, bool isFromMyCamp, ActionDisplay enemyDisplay, GameObject whoAttack)
        {
            if (((whoAttack != null) && (enemyDisplay != null)) && (enemyDisplay.GoBase != null))
            {
                UpdateAttribute component = enemyDisplay.GoBase.GetComponent<UpdateAttribute>();
                if (component != null)
                {
                    component.SetLastAttackMe(whoAttack);
                }
            }
            int type = 0;
            if (!isFromMyCamp)
            {
                type = 0;
            }
            else if (isHeal)
            {
                type = 1;
            }
            else if ((isCrit && isAppMe) && isPlayer)
            {
                type = 2;
            }
            else if ((isCrit && !isAppMe) && isPlayer)
            {
                type = 3;
            }
            UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " CreateDamageHudText ", target, " type ", type, " isFromMyCamp ", isFromMyCamp, " isHeal ", isHeal, " isCrit ", isCrit }));
            this.AddDamage(target, cutHp.ToString(), enemyDisplay, type);
        }

        public GameObject GetHudPrefab(string path)
        {
            return (!this._url2PrefabDic.ContainsKey(path) ? null : this._url2PrefabDic[path]);
        }

        private void HudTextCallback(GameObject go)
        {
            this.MountPoint.AddComponent<HUDRoot>().prefab = go;
        }

        protected void Init()
        {
            Instance = this;
        }

        private void LateUpdate()
        {
            HealthBarManager.Instance.ClearUpdateNum();
        }

        private void LoadBossMsgCallback(GameObject gObject)
        {
            this.BossMsgGo = gObject;
        }

        private void LoadFilterZhCallback(TextAsset filterText)
        {
            char[] separator = new char[] { '、' };
            Singleton<ChatIIMode>.Instance.FilterString = filterText.ToString().Split(separator);
        }

        private void LoadHealthCallback(GameObject gObject)
        {
            this.HealthGo = gObject;
        }

        private void LoadMonsterBloodCallback(GameObject gObject)
        {
            this.MonsterBloodGo = gObject;
        }

        private void LoadNickNameCallback(GameObject gObject)
        {
            this.NicknameGo = gObject;
        }

        private void LoadPlayerBloodCallback(GameObject gObject)
        {
            this.PlayerBloodGo = gObject;
        }

        private void LoadPrefab(string path)
        {
            <LoadPrefab>c__AnonStoreyF6 yf = new <LoadPrefab>c__AnonStoreyF6 {
                path = path,
                <>f__this = this
            };
            AssetManager.Instance.LoadAsset<GameObject>(yf.path, new LoadAssetFinish<GameObject>(yf.<>m__E5), null, false, true);
        }

        private GameObject NewAddMonsterBloodBar(Transform target)
        {
            UnityLog.Log.AI(target, " NewAddMonsterBloodBar " + target);
            if (target == null)
            {
                return null;
            }
            GameObject g = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("bloodMonster")) as GameObject;
            g.transform.parent = target;
            Util.InitGameObject(g);
            g.GetComponent<MyHealthBar>().SetHealthBar();
            GameObject healthBar = g.GetComponent<MyHealthBar>().healthBar;
            healthBar.GetComponent<UISlider>().value = 1f;
            return healthBar;
        }

        private GameObject NewAddPlayerBloodBar(Transform target, BaseDisplay display)
        {
            UnityLog.Log.AI(null, "NewAddPlayerBloodBar " + target);
            if (target == null)
            {
                return null;
            }
            GameObject g = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("bloodPlayer")) as GameObject;
            g.transform.parent = target;
            Util.InitGameObject(g);
            g.GetComponent<MyHealthBar>().SetHealthBar();
            return g.GetComponent<MyHealthBar>().healthBar;
        }

        public void recoverBlood(GameObject go, float fullHp, float curHp, float addHp, bool isPlayer = false)
        {
            UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " recoverBlood ", fullHp, " curHp ", curHp, " addHp ", addHp }));
            if (go != null)
            {
                go.GetComponent<UpdateHpBar>().SetHp((uint) fullHp, (uint) curHp, false);
            }
        }

        public void RestoreToFullHp(GameObject go)
        {
            UnityLog.Log.AI(base.gameObject, " restore full hp ");
            if (go != null)
            {
                UISlider componentInChildren = go.GetComponentInChildren<UISlider>();
                if (componentInChildren != null)
                {
                    componentInChildren.value = 1f;
                }
            }
        }

        [DebuggerHidden]
        private IEnumerator ShowBlood(GameObject child)
        {
            return new <ShowBlood>c__Iterator35 { child = child, <$>child = child };
        }

        public void StartLoadAsset()
        {
            AssetManager.Instance.LoadAsset<GameObject>("UI/Hud/Widget/Nickname.assetbundle", new LoadAssetFinish<GameObject>(this.LoadNickNameCallback), null, false, true);
            AssetManager.Instance.LoadAsset<GameObject>("UI/Hud/Widget/Health.assetbundle", new LoadAssetFinish<GameObject>(this.LoadHealthCallback), null, false, true);
            AssetManager.Instance.LoadAsset<GameObject>("UI/Hud/Widget/HUDText.assetbundle", new LoadAssetFinish<GameObject>(this.HudTextCallback), null, false, true);
            AssetManager.Instance.LoadAsset<GameObject>("UI/Hud/Widget/BossMsg.assetbundle", new LoadAssetFinish<GameObject>(this.LoadBossMsgCallback), null, false, true);
            AssetManager.Instance.LoadAsset<GameObject>("UI/Hud/Widget/Blood.assetbundle", new LoadAssetFinish<GameObject>(this.LoadMonsterBloodCallback), null, false, true);
            AssetManager.Instance.LoadAsset<GameObject>("UI/Hud/Widget/PlayerBlood.assetbundle", new LoadAssetFinish<GameObject>(this.LoadPlayerBloodCallback), null, false, true);
            AssetManager.Instance.LoadAsset<TextAsset>("Xml/filters_zh.assetbundle", new LoadAssetFinish<TextAsset>(this.LoadFilterZhCallback), null, false, true);
            this.LoadPrefab("Effect/Buff/10091.assetbundle");
        }

        public void stopRecoverBlood(GameObject go)
        {
            if (go == null)
            {
            }
        }

        [DebuggerHidden]
        private IEnumerator UpdateHpAfterFrame(GameObject go, float hp, float curHp, float demageHp)
        {
            return new <UpdateHpAfterFrame>c__Iterator36 { go = go, hp = hp, curHp = curHp, <$>go = go, <$>hp = hp, <$>curHp = curHp };
        }

        public Vector3 WorldToUIPoint(Vector3 point)
        {
            Vector3 position = Camera.main.WorldToScreenPoint(point);
            position.z = 0f;
            return UICamera.currentCamera.ScreenToWorldPoint(position);
        }

        [CompilerGenerated]
        private sealed class <LoadPrefab>c__AnonStoreyF6
        {
            internal HudView <>f__this;
            internal string path;

            internal void <>m__E5(GameObject go)
            {
                if (this.<>f__this._url2PrefabDic.ContainsKey(this.path))
                {
                    UnityEngine.Debug.Log("资源重复加载：" + this.path);
                }
                this.<>f__this._url2PrefabDic[this.path] = go;
            }
        }

        [CompilerGenerated]
        private sealed class <ShowBlood>c__Iterator35 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal GameObject <$>child;
            internal GameObject child;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.child.SetActive(true);
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateHpAfterFrame>c__Iterator36 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal float <$>curHp;
            internal GameObject <$>go;
            internal float <$>hp;
            internal float curHp;
            internal GameObject go;
            internal float hp;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.go.GetComponent<UpdateHpBar>().SetHp((uint) this.hp, (uint) this.curHp, false);
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        public class RecoverBloodObj
        {
            public float addHp;
            public float curHp;
            public float fullHp;
            public GameObject go;
            public UISlider slider;
        }

        public enum Type
        {
            Player,
            Npc,
            Monster
        }
    }
}

