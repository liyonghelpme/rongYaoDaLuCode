﻿namespace com.game.module.hud.stub
{
    using System;

    public class ColorPreds
    {
        public const string BLUE = "Blue";
        public const string GREEN = "Green";
        public const string ORANGE = "Orange";
        public const string OTHERS = "Others";
        public const string RED = "Red";
        public const string YELLOW = "Yellow";
    }
}

