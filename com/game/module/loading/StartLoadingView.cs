﻿namespace com.game.module.loading
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.Speech;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class StartLoadingView : BaseView<StartLoadingView>
    {
        private GameObject _bottomGameObject;
        private int _currentStatu;
        private GameObject _loadAnimationModel;
        private int index;
        private int interal = 30;
        private UILabel labelLoading;
        private int lastComplete;
        public List<AssetBundleLoader> loaderList = new List<AssetBundleLoader>();
        private UISlider slider;

        public override void CloseView()
        {
            this.slider.value = 0f;
            this.loaderList.Clear();
            this.PreLoadNum = 0;
            this.PreLoadedNum = 0;
            base.CloseView();
        }

        public override void Destroy()
        {
            this.slider = null;
            this.labelLoading = null;
            this.loaderList.Clear();
            base.Destroy();
        }

        private float getProgress()
        {
            if (this.loaderList == null)
            {
                return (1f - (this.PreLoadNum * 0.01f));
            }
            int count = this.loaderList.Count;
            float num2 = 0f;
            int num3 = 0;
            if (count != 0)
            {
                foreach (AssetBundleLoader loader in this.loaderList)
                {
                    if (loader.progress == 1f)
                    {
                        num3++;
                    }
                }
                if (num3 != this.lastComplete)
                {
                    this.index = 0;
                    this.lastComplete = num3;
                }
                else
                {
                    this.index++;
                    if (this.index == this.interal)
                    {
                        this.index = this.interal - 1;
                    }
                }
                num2 = (((float) this.lastComplete) / ((float) count)) + ((((float) this.index) / ((float) this.interal)) / ((float) count));
                if (num2 > 1f)
                {
                    num2 = 1f;
                }
            }
            return (num2 * (1f - (this.PreLoadNum * 0.01f)));
        }

        protected override void HandleAfterOpenView()
        {
            this.slider.value = 0f;
            this.labelLoading.text = "游戏资源正在加载：0%";
        }

        protected override void HandleBeforeCloseView()
        {
            base.HandleBeforeCloseView();
            if (this._loadAnimationModel != null)
            {
                this.ResetModelStatu();
                vp_Timer.CancelAll("ResetModelStatu");
            }
        }

        protected override void Init()
        {
            this.slider = Tools.find(this.gameObject, "center/offset/Bottom/loading/silder").GetComponent<UISlider>();
            this.labelLoading = Tools.find(this.gameObject, "center/offset/Bottom/LoadingInfo/loadinglabel").GetComponent<UILabel>();
            this._bottomGameObject = base.FindChild("center/offset/Bottom");
            NGUITools.SetLayer(this._bottomGameObject, LayerMask.NameToLayer("TopUI"));
            this.slider.value = 0f;
        }

        private void OnRightChestButtonClick(GameObject go)
        {
            if (this._loadAnimationModel != null)
            {
                if (this._currentStatu != 3)
                {
                    this._currentStatu = 3;
                    this._loadAnimationModel.GetComponentInChildren<Animator>().SetInteger("State", 3);
                }
                else
                {
                    this._currentStatu = 4;
                    this._loadAnimationModel.GetComponentInChildren<Animator>().SetInteger("State", 4);
                }
                vp_Timer.In(0.1f, new vp_Timer.Callback(this.ResetModelStatu), null);
                SpeechMgr.Instance.PlaySpeech(12);
            }
        }

        private void ResetModelStatu()
        {
            if ((this._loadAnimationModel != null) && (this._loadAnimationModel.GetComponentInChildren<Animator>() != null))
            {
                this._loadAnimationModel.GetComponentInChildren<Animator>().SetInteger("State", 0);
            }
        }

        public override void Update()
        {
            float num = this.getProgress() + (this.PreLoadedNum * 0.01f);
            this.slider.value = num;
            this.labelLoading.text = "游戏资源正在加载：" + ((int) (this.slider.value * 100f)) + "%";
        }

        public override bool isDestroy
        {
            get
            {
                return false;
            }
        }

        public override bool IsFullUI
        {
            get
            {
                return true;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.HighLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public int PreLoadedNum { get; set; }

        public int PreLoadNum { get; set; }

        public override string url
        {
            get
            {
                return "UI/Loading/LoadingView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

