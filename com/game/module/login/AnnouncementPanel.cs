﻿namespace com.game.module.login
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class AnnouncementPanel : BaseView<AnnouncementPanel>
    {
        [CompilerGenerated]
        private static Func<KeyValuePair<uint, object>, uint> <>f__am$cacheA;
        [CompilerGenerated]
        private static Func<SysAnnouncementVo, <>__AnonType2<SysAnnouncementVo, uint>> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<<>__AnonType2<SysAnnouncementVo, uint>, bool> <>f__am$cacheC;
        [CompilerGenerated]
        private static Func<<>__AnonType2<SysAnnouncementVo, uint>, SysAnnouncementVo> <>f__am$cacheD;
        private Button closeBtn;
        private int curPageIndex;
        private UIToggle curToggle;
        private UIScrollView scrollView;
        private GameObject tabBtnGo;
        private UIGrid tabBtnGrid;
        private List<UIToggle> tabBtnList = new List<UIToggle>();
        private List<SysAnnouncementVo> templateList = new List<SysAnnouncementVo>();
        private UILabel versionContentLabel;
        private UILabel versionTitleLabel;

        private void CloneTabBtn(string tabBtnName)
        {
            if (null == this.tabBtnGo)
            {
                this.tabBtnGo = base.FindChild("panel/tabBtns/grid/item");
            }
            GameObject parent = NGUITools.AddChild(this.tabBtnGrid.gameObject, this.tabBtnGo);
            UIToggle component = parent.GetComponent<UIToggle>();
            component.SetActive(true);
            NGUITools.FindInChild<UILabel>(parent, "Label").text = tabBtnName;
            this.tabBtnList.Add(component);
        }

        private void CloseBtnClickHhandler(GameObject go)
        {
            this.CloseView();
        }

        protected override void Init()
        {
            this.closeBtn = base.FindInChild<Button>("panel/closeBtn");
            this.tabBtnGrid = base.FindInChild<UIGrid>("panel/tabBtns/grid");
            this.versionTitleLabel = base.FindInChild<UILabel>("panel/versionTitleLabel");
            this.versionContentLabel = base.FindInChild<UILabel>("panel/versionContentLabel");
            this.scrollView = base.FindInChild<UIScrollView>("panel/tabBtns");
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = pair => pair.Key;
            }
            IOrderedEnumerable<KeyValuePair<uint, object>> enumerable = BaseDataMgr.instance.GetAnnouncementTemplateList().OrderByDescending<KeyValuePair<uint, object>, uint>(<>f__am$cacheA);
            int num = 0;
            IEnumerator<KeyValuePair<uint, object>> enumerator = enumerable.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    KeyValuePair<uint, object> current = enumerator.Current;
                    num++;
                    if (num <= 5)
                    {
                        SysAnnouncementVo item = (SysAnnouncementVo) current.Value;
                        this.templateList.Add(item);
                        this.CloneTabBtn(item.name);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            UIToggle.current = this.curToggle = this.tabBtnList[0];
            this.tabBtnList[0].value = true;
            this.versionTitleLabel.text = this.templateList[0].title;
            this.versionContentLabel.text = this.templateList[0].descript;
            this.InitEvent();
            if (<>f__am$cacheB == null)
            {
                <>f__am$cacheB = s => new <>__AnonType2<SysAnnouncementVo, uint>(s, s.id);
            }
            if (<>f__am$cacheC == null)
            {
                <>f__am$cacheC = <>__TranspIdent2 => (<>__TranspIdent2.index % 2) == 0;
            }
            if (<>f__am$cacheD == null)
            {
                <>f__am$cacheD = <>__TranspIdent2 => <>__TranspIdent2.s;
            }
            IEnumerable<SysAnnouncementVo> enumerable2 = this.templateList.Select<SysAnnouncementVo, <>__AnonType2<SysAnnouncementVo, uint>>(<>f__am$cacheB).Where<<>__AnonType2<SysAnnouncementVo, uint>>(<>f__am$cacheC).Select<<>__AnonType2<SysAnnouncementVo, uint>, SysAnnouncementVo>(<>f__am$cacheD);
        }

        private void InitEvent()
        {
            this.closeBtn.onClick = new UIWidgetContainer.VoidDelegate(this.CloseBtnClickHhandler);
            for (int i = 0; i < this.tabBtnList.Count; i++)
            {
                EventDelegate.Add(this.tabBtnList[i].onChange, new EventDelegate.Callback(this.TabBtnClickHandler));
            }
        }

        private void SetIndex(int index)
        {
            SysAnnouncementVo vo = this.templateList[index];
            this.versionTitleLabel.text = vo.title;
            this.versionContentLabel.text = vo.descript;
        }

        private void TabBtnClickHandler()
        {
            UIToggle current = UIToggle.current;
            if (current.value && (current != this.curToggle))
            {
                this.curToggle = current;
                this.curPageIndex = this.tabBtnList.IndexOf(current);
                this.SetIndex(this.curPageIndex);
            }
        }

        public override string url
        {
            get
            {
                return "UI/Login/AnnouncementPanel.assetbundle";
            }
        }
    }
}

