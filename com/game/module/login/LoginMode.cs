﻿namespace com.game.module.login
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.LocalVar;
    using com.game.start;
    using com.u3d.bases.debug;
    using Proto;
    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LoginMode : BaseMode<LoginMode>
    {
        private string _platformName;
        private int _serverId;
        private string _signStr;
        private string _suid;
        private string _targetServer;
        private string _timestamp;
        private string _verifyToken;
        public bool IsOpenLoginView;
        public string platform = "4399";
        public readonly int UPDATE_SERVER = 1;

        public void createRole(int carser, int sex, string roleName, uint generalId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_1.write_1_3(msdata, roleName, (byte) carser, (byte) sex, generalId, (uint) this.serverId);
            Log.debug(this, string.Concat(new object[] { "-createRole 发送创建角色消息,carser:", carser, " sex:", sex, " roleName:", roleName, " platforName:", this._platformName }));
            AppNet.gameNet.send(msdata, 1, 3);
        }

        public void getRoleList()
        {
            MemoryStream msdata = new MemoryStream();
            byte fcm = 1;
            string ipAddress = Network.player.ipAddress;
            Debug.Log("local ip:" + ipAddress);
            if (AppStart.RunMode == 0)
            {
                Log.info(this, "发送1-1协议");
                Module_1.write_1_1(msdata, this.userName, string.Empty, fcm, "ios", string.Empty, 0, ipAddress);
            }
            else if (AppStart.RunMode != 0)
            {
                Log.info(this, "发送1-1协议");
                Module_1.write_1_1(msdata, this._suid, this._signStr, fcm, this.platform, this._verifyToken, uint.Parse(this._timestamp), ipAddress);
            }
            AppNet.gameNet.send(msdata, 1, 1);
        }

        public void SendClientInfo()
        {
            string operatingSystem = SystemInfo.operatingSystem;
            string osVer = "2.3.4";
            string deviceModel = SystemInfo.deviceModel;
            string deviceType = SystemInfo.deviceType.ToString();
            string screen = Screen.width + "*" + Screen.height;
            string mno = "中国移动";
            string nm = "wifi";
            Log.info(this, string.Concat(new object[] { 
                "发送1-6客户端信息给服务端：", operatingSystem, ",", osVer, ",", deviceModel, ",", deviceType, ",", screen, ",", mno, ",", nm, ",", this.platform, 
                ",", this.serverId, ","
             }));
            MemoryStream msdata = new MemoryStream();
            this.platform = (AppStart.RunMode != 2) ? "4399" : "91";
            Module_1.write_1_6(msdata, operatingSystem, osVer, deviceModel, deviceType, screen, mno, nm, this.platform, (uint) this.serverId);
            AppNet.gameNet.send(msdata, 1, 6);
        }

        public void UpdateLoginInfo(string suid, string verifyToken, string platformName = "", string timestamp = "0", string signStr = "", string targetServer = "")
        {
            this._platformName = platformName;
            this._timestamp = timestamp;
            this._signStr = signStr;
            this._suid = suid;
            this._targetServer = targetServer;
            this._verifyToken = verifyToken;
        }

        public string Ip { get; set; }

        public bool loginSuc { get; set; }

        public string platformName
        {
            get
            {
                return this._platformName;
            }
        }

        public int Port { get; set; }

        public int serverId
        {
            get
            {
                return this._serverId;
            }
            set
            {
                this._serverId = value;
                LocalVarManager.SetInt("Login_Server", this.serverId);
                base.DataUpdate(this.UPDATE_SERVER);
            }
        }

        public string userName { get; set; }
    }
}

