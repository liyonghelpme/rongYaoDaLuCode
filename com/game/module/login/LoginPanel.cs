﻿namespace com.game.module.login
{
    using com.game;
    using com.game.consts;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.sound;
    using com.game.start;
    using com.game.utils;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class LoginPanel : BaseView<LoginPanel>
    {
        private Button _btnChangeServer;
        private Button _btnServer;
        private UIGrid _centerGrid;
        private Button _gonggaoBtn;
        private Button _loginGo;
        private UIInput _nameInput;
        private GameObject _serverlist;
        private List<GameObject> _serverObjs;
        private UILabel _usernameLab;
        public ServerInfo Info;
        private int serverId = -1;
        private ServerListInfo serverList;
        private UIWidgetContainer widget;

        public override void CancelUpdateHandler()
        {
            Singleton<SelectServerMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateLoginData);
        }

        [DebuggerHidden]
        private IEnumerator CloseServerList()
        {
            return new <CloseServerList>c__Iterator37 { <>f__this = this };
        }

        private void ConnectServer()
        {
            Singleton<LoginMode>.Instance.userName = this._nameInput.value;
            AppNet.ip = this.Info.IP;
            AppNet.port = this.Info.Port;
            AppNet.gameNet.connect(this.Info.IP, this.Info.Port);
            Singleton<LoginMode>.Instance.serverId = this.Info.Id;
            Singleton<RoleCreateView>.Instance.SetServer(this.Info.Id + "区" + this.Info.Name);
        }

        private GameObject GetServerObj(int index)
        {
            if (index < this._serverObjs.Count)
            {
                return this._serverObjs[index];
            }
            GameObject item = UnityEngine.Object.Instantiate(this._serverObjs[0]) as GameObject;
            item.transform.parent = this._serverObjs[0].transform.parent;
            item.transform.localPosition = this._serverObjs[0].transform.localPosition;
            item.name = (0x270f - index).ToString();
            item.transform.localScale = Vector3.one;
            item.GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.OnServerChoseClick);
            this._serverObjs.Add(item);
            return item;
        }

        private Color GetStateColor(ServerInfo info)
        {
            Color color = ColorConst.FONT_YELLOW;
            if (info.State == 1)
            {
                return ColorConst.FONT_YELLOW;
            }
            if (info.State == 2)
            {
                return ColorConst.FONT_BLUE;
            }
            if (info.State == 3)
            {
                return ColorConst.FONT_RED;
            }
            return ColorConst.FONT_GRAY;
        }

        private void GongGaoBtnClickHandler(GameObject go)
        {
            Singleton<AnnouncementPanel>.Instance.OpenView();
        }

        protected override void HandleAfterOpenView()
        {
            Singleton<LoginMode>.Instance.IsOpenLoginView = true;
            if (AppStart.RunMode != 0)
            {
                this._nameInput.value = Singleton<LoginMode>.Instance.platformName;
            }
            this.LoadSetting();
            this.serverList.GetServerInfo();
        }

        protected override void Init()
        {
            this._usernameLab = base.FindInChild<UILabel>("panel/username/nameInputLab/Label");
            this._nameInput = base.FindInChild<UIInput>("panel/username/nameInputLab");
            this._btnChangeServer = base.FindInChild<Button>("panel/btnChangeServer");
            this._loginGo = base.FindInChild<Button>("panel/startGameBtn");
            this._gonggaoBtn = base.FindInChild<Button>("panel/gonggaolanBtn");
            this._serverObjs = new List<GameObject>();
            this._serverObjs.Add(base.FindChild("panel/serverlist/allServer/list/grid/item"));
            this._serverObjs[0].SetActive(false);
            this._serverObjs[0].name = "9999";
            this._centerGrid = base.FindInChild<UIGrid>("panel/serverlist/allServer/list/grid");
            this._serverlist = base.FindChild("panel/serverlist");
            this._serverlist.SetActive(false);
            this.widget = base.FindInChild<UIWidgetContainer>("panel/serverlist/bg/mainBg");
            this.widget.onClick = new UIWidgetContainer.VoidDelegate(this.OnCloseServerList);
            this.serverList = this.gameObject.AddComponent<ServerListInfo>();
            this.InitEvent();
        }

        private void InitEvent()
        {
            this._loginGo.onClick = new UIWidgetContainer.VoidDelegate(this.LoginOnClick);
            this._btnChangeServer.onClick = new UIWidgetContainer.VoidDelegate(this.ServerOnClick);
            this._serverObjs[0].GetComponent<Button>().onClick = new UIWidgetContainer.VoidDelegate(this.OnServerChoseClick);
            this._gonggaoBtn.onClick = new UIWidgetContainer.VoidDelegate(this.GongGaoBtnClickHandler);
        }

        private void InitServerObjs()
        {
            foreach (GameObject obj2 in this._serverObjs)
            {
                obj2.SetActive(false);
            }
        }

        private void LoadSetting()
        {
            if (AppStart.RunMode == 0)
            {
                this._nameInput.value = PlayerPrefs.GetString("UserName");
                string s = PlayerPrefs.GetString("UserServer");
                if ((s != null) && s.Equals(string.Empty))
                {
                    this.serverId = 0;
                }
                else
                {
                    this.serverId = int.Parse(s);
                }
            }
        }

        private void LoginOnClick(GameObject go)
        {
            if (this.Info == null)
            {
                MessageManager.Show("无服务器信息");
            }
            else if (StringUtils.isEmpty(this._nameInput.value))
            {
                this._usernameLab.text = "请输入账号名";
                MessageManager.Show("请输入账号名");
            }
            else if (this.Info.State > 3)
            {
                MessageManager.Show("此服务器维护中");
                this.serverList.GetServerStateInfo();
            }
            else
            {
                this.SaveSetting();
                SoundMgr.Instance.PlayUIAudio("3015", 0f);
                this.ConnectServer();
            }
        }

        private void OnCloseServerList(GameObject obj)
        {
            this._serverlist.SetActive(false);
        }

        private void OnServerChoseClick(GameObject obj)
        {
            int serverid = int.Parse(NGUITools.FindInChild<UILabel>(obj, "num").text.Replace("区", string.Empty));
            ServerInfo serverInfoByServerId = Singleton<SelectServerMode>.Instance.GetServerInfoByServerId(serverid);
            if (serverInfoByServerId != null)
            {
                this.Info = serverInfoByServerId;
                this.SetCurrentServerInfo();
                this._serverlist.SetActive(false);
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<SelectServerMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateLoginData);
        }

        private void SaveSetting()
        {
            if ((AppStart.RunMode == 0) && (!this._nameInput.value.Equals(string.Empty) && (this.Info != null)))
            {
                PlayerPrefs.SetString("UserName", this._nameInput.value);
                PlayerPrefs.SetString("UserServer", this.Info.Id.ToString());
                PlayerPrefs.Save();
            }
        }

        private void ServerOnClick(GameObject go)
        {
            if (Singleton<SelectServerMode>.Instance.serverList.Count == 0)
            {
                this.serverList.GetServerInfo();
            }
            else
            {
                this.serverList.GetServerInfo();
                this._serverlist.SetActive(true);
            }
        }

        private void SetCurrentServerInfo()
        {
            if (this.Info != null)
            {
                Color stateColor = this.GetStateColor(this.Info);
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/name").text = this.Info.Name + "(" + this.Info.ServerStateStr() + ")";
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/num").text = this.Info.Id + "区";
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/name").color = stateColor;
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/num").color = stateColor;
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/tips").SetActive(false);
                NGUITools.FindInChild<Button>(this.gameObject, "panel/currentServer").SetActive(true);
            }
            else
            {
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/name").text = string.Empty;
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/num").text = string.Empty;
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/tips").text = "无服务器信息";
                NGUITools.FindInChild<UILabel>(this.gameObject, "panel/currentServer/tips").SetActive(true);
                NGUITools.FindInChild<Button>(this.gameObject, "panel/btnChangeServer").SetActive(false);
            }
        }

        private void SetServerInfo(GameObject obj, ServerInfo info)
        {
            NGUITools.FindInChild<UILabel>(obj, "name").text = info.Name;
            NGUITools.FindInChild<UILabel>(obj, "num").text = info.Id + "区";
            Color stateColor = this.GetStateColor(info);
            NGUITools.FindInChild<UILabel>(obj, "name").color = stateColor;
            NGUITools.FindInChild<UILabel>(obj, "num").color = stateColor;
            Singleton<RoleCreateView>.Instance.SetServer(info.Id + "区" + info.Name);
        }

        public void SetServerTips(string tips)
        {
        }

        private void UpdateLoginData(object send, int code)
        {
            if ((send == Singleton<SelectServerMode>.Instance) && (code == 1))
            {
                this.InitServerObjs();
                if (this.Info != null)
                {
                    this.Info = Singleton<SelectServerMode>.Instance.GetServerInfoByServerId(this.Info.Id);
                }
                if (this.Info == null)
                {
                    this.Info = Singleton<SelectServerMode>.Instance.GetServerInfoByServerId(this.serverId);
                }
                if (this.Info == null)
                {
                    this.Info = Singleton<SelectServerMode>.Instance.GetDefaultServer();
                }
                if (this.Info != null)
                {
                    this.SetCurrentServerInfo();
                    this.SetServerInfo(NGUITools.FindChild(this._serverlist, "last"), this.Info);
                }
                int maxServerId = Singleton<SelectServerMode>.Instance.maxServerId;
                int index = 0;
                for (int i = maxServerId; i >= 0; i--)
                {
                    ServerInfo serverInfoByServerId = Singleton<SelectServerMode>.Instance.GetServerInfoByServerId(i);
                    if (serverInfoByServerId != null)
                    {
                        GameObject serverObj = this.GetServerObj(index);
                        this.SetServerInfo(serverObj, serverInfoByServerId);
                        serverObj.SetActive(true);
                        index++;
                    }
                }
                this._centerGrid.repositionNow = true;
            }
        }

        public override bool isDestroy
        {
            get
            {
                return true;
            }
        }

        public override bool isUnloadDelay
        {
            get
            {
                return true;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Login/LoginPanel.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }

        [CompilerGenerated]
        private sealed class <CloseServerList>c__Iterator37 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal LoginPanel <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(0.1f);
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.<>f__this._serverlist.SetActive(false);
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

