﻿namespace com.game.module.login
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;

    public class SelectServerMode : BaseMode<SelectServerMode>
    {
        public int maxServerId;
        public Dictionary<int, ServerInfo> serverList = new Dictionary<int, ServerInfo>();
        public const int ServerList = 1;

        public ServerInfo GetDefaultServer()
        {
            if (this.serverList != null)
            {
                int num = this.serverList.Count - 1;
                for (int i = num; i >= 0; i--)
                {
                    if (((this.serverList[i] != null) && (this.serverList[i].State == 1)) || (this.serverList[i].State == 2))
                    {
                        return this.serverList[i];
                    }
                }
                for (int j = num; j >= 0; j--)
                {
                    if ((this.serverList[j] != null) && (this.serverList[j].State == 3))
                    {
                        return this.serverList[j];
                    }
                }
                for (int k = num; k >= 0; k--)
                {
                    if ((this.serverList[k] != null) && (this.serverList[k].State == 4))
                    {
                        return this.serverList[k];
                    }
                }
            }
            return null;
        }

        public ServerInfo GetServerInfoByServerId(int serverid)
        {
            if (this.serverList.ContainsKey(serverid))
            {
                return this.serverList[serverid];
            }
            return null;
        }

        public void SetServerListInfo(string[] list)
        {
            if ((list != null) && (list.Length > 0))
            {
                this.serverList.Clear();
                for (int i = 0; i < list.Length; i++)
                {
                    char[] separator = new char[] { ',' };
                    string[] serverInfo = list[i].Split(separator);
                    if (serverInfo.Length == 5)
                    {
                        ServerInfo info = new ServerInfo(serverInfo);
                        if (this.maxServerId < info.Id)
                        {
                            this.maxServerId = info.Id;
                        }
                        this.serverList.Add(info.Id, info);
                    }
                }
            }
            base.DataUpdate(1);
        }
    }
}

