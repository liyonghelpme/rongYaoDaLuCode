﻿namespace com.game.module.login
{
    using com.game.module.core;
    using com.u3d.bases.debug;
    using System;
    using UnityEngine;

    public class SelectServerView : BaseView<SelectServerView>
    {
        private Button btn_area;
        private Button btn_fanhui;
        private Button btn_lastserver;
        private Button btn_recommendserver;
        private Button btn_server;
        private UIGrid grid_area;
        private UIGrid grid_server;

        private void AddArea(int num)
        {
            for (int i = 0; i < num; i++)
            {
                NGUITools.AddChild(this.grid_area.gameObject, this.btn_area.gameObject);
            }
        }

        private void AddServer(int num)
        {
            for (int i = 0; i < num; i++)
            {
                NGUITools.AddChild(this.grid_server.gameObject, this.btn_server.gameObject);
            }
        }

        private void FanHuiOnClick(GameObject go)
        {
            this.CloseView();
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
        }

        protected override void Init()
        {
            this.btn_lastserver = base.FindInChild<Button>("center/xia/shangci/btn_lastserver");
            this.btn_fanhui = base.FindInChild<Button>("btn_fanhui");
            this.btn_lastserver.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this.btn_lastserver.onClick, new UIWidgetContainer.VoidDelegate(this.RecommendSeverOnClick));
            this.btn_fanhui.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this.btn_fanhui.onClick, new UIWidgetContainer.VoidDelegate(this.FanHuiOnClick));
        }

        private void RecommendSeverOnClick(GameObject go)
        {
            Log.info(this, "this is RecommendSeverOnClick");
            this.CloseView();
            Singleton<LoginMode>.Instance.serverId = 0x3e7;
            this.CloseView();
        }

        public override bool isDestroy
        {
            get
            {
                return true;
            }
        }

        public override bool isUnloadDelay
        {
            get
            {
                return true;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/SelectServer/SelectServerView.assetbundle";
            }
        }
    }
}

