﻿namespace com.game.module.login
{
    using System;
    using System.Runtime.CompilerServices;

    public class ServerInfo
    {
        public ServerInfo(string[] serverInfo)
        {
            this.Id = int.Parse(serverInfo[0]);
            this.Name = serverInfo[1];
            this.State = int.Parse(serverInfo[2]);
            this.IP = serverInfo[3];
            this.Port = int.Parse(serverInfo[4]);
        }

        public string ServerStateStr()
        {
            if (this.State == 1)
            {
                return "新服";
            }
            if (this.State == 2)
            {
                return "推荐";
            }
            if (this.State == 3)
            {
                return "繁忙";
            }
            return "维护";
        }

        public int Id { get; private set; }

        public string IP { get; private set; }

        public string Name { get; private set; }

        public int Port { get; private set; }

        public int State { get; private set; }
    }
}

