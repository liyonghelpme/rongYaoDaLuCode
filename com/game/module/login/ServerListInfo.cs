﻿namespace com.game.module.login
{
    using com.game.module.core;
    using com.game.Public.Message;
    using com.u3d.bases.debug;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ServerListInfo : MonoBehaviour
    {
        public string GetStateURL = "api/get_server_list2.php";
        public string GetURL = "get_server_list.php";
        private float setTime;
        public string SetURL = "api/set_my_server.php";
        private float timeGap = 10f;

        public void GetServerInfo()
        {
            if ((this.setTime == 0f) || ((RealTime.time - this.setTime) > this.timeGap))
            {
                base.StartCoroutine(this.RequestServerInfo(this.ServerHost + this.GetURL));
            }
            this.setTime = RealTime.time;
        }

        public void GetServerStateInfo()
        {
            if ((this.setTime == 0f) || ((RealTime.time - this.setTime) > this.timeGap))
            {
                base.StartCoroutine(this.RequestServerInfo(this.ServerHost + this.GetURL));
            }
        }

        [DebuggerHidden]
        private IEnumerator RequestServerInfo(string url)
        {
            return new <RequestServerInfo>c__Iterator39 { url = url, <$>url = url, <>f__this = this };
        }

        public void SetServerInfo(string url, string accoutName, string serverId)
        {
        }

        public string ServerHost
        {
            get
            {
                if ((Application.platform != RuntimePlatform.WindowsEditor) && (Application.platform != RuntimePlatform.WindowsPlayer))
                {
                    return "http://122.224.249.46/";
                }
                return "http://192.168.6.202/";
            }
        }

        [CompilerGenerated]
        private sealed class <RequestServerInfo>c__Iterator39 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal string <$>url;
            internal ServerListInfo <>f__this;
            internal WWW <request>__0;
            internal string <txt>__1;
            internal string url;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<request>__0 = new WWW(this.url);
                        this.$current = this.<request>__0;
                        this.$PC = 1;
                        return true;

                    case 1:
                    {
                        if (this.<request>__0.error != null)
                        {
                            MessageManager.Show(this.<request>__0.error);
                            MessageManager.Show("获取服务器列表失败!");
                            Singleton<LoginPanel>.Instance.SetServerTips("获取失败，点击重试");
                            break;
                        }
                        this.<txt>__1 = this.<request>__0.text;
                        com.u3d.bases.debug.Log.info(this.<>f__this, this.<txt>__1);
                        char[] separator = new char[] { '|' };
                        Singleton<SelectServerMode>.Instance.SetServerListInfo(this.<txt>__1.Split(separator));
                        this.<request>__0.Dispose();
                        break;
                    }
                    default:
                        goto Label_00DC;
                }
                this.$PC = -1;
            Label_00DC:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

