﻿namespace com.game.module.login.rolecreate
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Role;
    using com.game.Public.Message;
    using com.game.utils;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Text;
    using UnityEngine;

    public class RoleCreatePanel : BaseView<RoleCreatePanel>
    {
        private string[] _familyName;
        private string[] _manDoubleName;
        private string[] _manSingleName;
        private string[] _specialName;
        private string[] _womanDoubleName;
        private string[] _womanSingleName;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map21;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map22;
        private int count;
        private int currentSelectedIndex = 1;
        private List<int> generalIdList;
        private UILabel heroLabel;
        private UIInput input;
        private int job;
        private readonly int JOB = 1;
        private string[][] laynameLayout;
        private Button leftBtn;
        private List<uint> modelIdList;
        private readonly int NAME_LEN_MAX = 14;
        private readonly int NAME_LEN_MIN = 4;
        private const string RANDOM_NAME_URL = "Xml/RandomName.assetbundle";
        private Button rightBtn;
        private Dictionary<uint, RoleDisplay> roleDic;
        private Button saiziBtn;
        private Button selectedRoleBtn;
        private readonly int SEX = 1;
        private Button skill1Btn;
        private Button skill2Btn;
        private Button skill3Btn;
        private UILabel skillDesLabel;

        public RoleCreatePanel()
        {
            string[][] textArrayArray1 = new string[8][];
            textArrayArray1[0] = new string[] { "s", "s", "d" };
            textArrayArray1[1] = new string[] { "s", "t", "d" };
            textArrayArray1[2] = new string[] { "d", "s", "s" };
            textArrayArray1[3] = new string[] { "t", "s", "s", "d" };
            textArrayArray1[4] = new string[] { "s", "s", "t", "d" };
            textArrayArray1[5] = new string[] { "t", "s", "d", "t" };
            textArrayArray1[6] = new string[] { "d", "t", "s" };
            textArrayArray1[7] = new string[] { "t", "s", "s", "d", "t" };
            this.laynameLayout = textArrayArray1;
            this.roleDic = new Dictionary<uint, RoleDisplay>();
            this.job = 1;
            this.generalIdList = new List<int>();
            this.modelIdList = new List<uint>();
        }

        private void CloseBtnClickHandler(GameObject go)
        {
            this.CloseView();
        }

        public override void CloseView()
        {
            CoroutineManager.StartCoroutine(this.DelayClose(), true);
        }

        [DebuggerHidden]
        private IEnumerator DelayClose()
        {
            return new <DelayClose>c__Iterator3A { <>f__this = this };
        }

        private int GetByteLength(string roleName)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(roleName);
            int num = 0;
            for (int i = 0; i < bytes.GetLength(0); i++)
            {
                if ((i % 2) == 0)
                {
                    num++;
                }
                else if (bytes[i] > 0)
                {
                    num++;
                }
            }
            return num;
        }

        public string getRandomName()
        {
            string[] strArray;
            string[] strArray2;
            if ((Time.realtimeSinceStartup % 2f) > 0f)
            {
                strArray = this._manSingleName;
                strArray2 = this._manDoubleName;
            }
            else
            {
                strArray = this._womanSingleName;
                strArray2 = this._womanDoubleName;
            }
            if ((strArray == null) || (strArray2 == null))
            {
                return string.Empty;
            }
            System.Random random = new System.Random();
            string[] strArray3 = this.laynameLayout[random.Next(0, this.laynameLayout.Length - 1)];
            string str = string.Empty;
            foreach (string str2 in strArray3)
            {
                string key = str2;
                if (key != null)
                {
                    int num2;
                    if (<>f__switch$map22 == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                        dictionary.Add("s", 0);
                        dictionary.Add("d", 1);
                        dictionary.Add("t", 2);
                        <>f__switch$map22 = dictionary;
                    }
                    if (<>f__switch$map22.TryGetValue(key, out num2))
                    {
                        switch (num2)
                        {
                            case 0:
                                str = str + strArray[random.Next(0, strArray.Length - 1)];
                                break;

                            case 1:
                                str = str + strArray2[random.Next(0, strArray2.Length - 1)];
                                break;

                            case 2:
                                str = str + this._specialName[random.Next(0, this._specialName.Length - 1)];
                                break;
                        }
                    }
                }
            }
            return str;
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
        }

        protected override void Init()
        {
            this.saiziBtn = base.FindInChild<Button>("panel/name/saiziBtn");
            this.selectedRoleBtn = base.FindInChild<Button>("panel/selectedRoleBtn");
            this.skillDesLabel = base.FindInChild<UILabel>("panel/left/skillDesLabel");
            this.heroLabel = base.FindInChild<UILabel>("panel/right/heroLabel");
            this.input = base.FindInChild<UIInput>("panel/name/nameLabel");
            this.leftBtn = base.FindInChild<Button>("panel/selectBtn/leftBtn");
            this.rightBtn = base.FindInChild<Button>("panel/selectBtn/rightBtn");
            this.skill1Btn = base.FindInChild<Button>("panel/left/skillGroup/skill1");
            this.skill2Btn = base.FindInChild<Button>("panel/left/skillGroup/skill2");
            this.skill3Btn = base.FindInChild<Button>("panel/left/skillGroup/skill3");
            NGUITools.SetLayer(this.gameObject, LayerMask.NameToLayer("Mode"));
            this.InitEvent();
            this.LoadRandomNameData();
            this.generalIdList = Singleton<ConfigConst>.Instance.GetConfigDatas("INIT_HERO_LIST");
            for (int i = 0; i < this.generalIdList.Count; i++)
            {
                uint generalUniKey = GlobalData.GetGeneralUniKey(this.generalIdList[i], 1);
                this.modelIdList.Add(generalUniKey);
                this.LoadRoleMode(generalUniKey);
            }
            SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo(this.generalIdList[this.currentSelectedIndex], 0);
            this.heroLabel.text = LanguageManager.GetWord("Login.General" + this.generalIdList[this.currentSelectedIndex].ToString(), generalVo.name);
            string[] param = new string[] { LanguageManager.GetWord("Login.GeneralSkill20031"), LanguageManager.GetWord("Login.GeneralSkill20031Des") };
            this.skillDesLabel.text = LanguageManager.GetWord("Login.GeneralSkillDes", param);
            this.skill1Btn.label.text = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "1");
            this.skill2Btn.label.text = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "2");
            this.skill3Btn.label.text = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "3");
        }

        private void InitEvent()
        {
            this.saiziBtn.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this.saiziBtn.onClick, new UIWidgetContainer.VoidDelegate(this.RandomName));
            this.selectedRoleBtn.onClick = new UIWidgetContainer.VoidDelegate(this.StartGame);
            this.leftBtn.onClick = new UIWidgetContainer.VoidDelegate(this.LeftBtnClickHandler);
            this.rightBtn.onClick = new UIWidgetContainer.VoidDelegate(this.RightBtnClickHandler);
            this.skill1Btn.onClick = new UIWidgetContainer.VoidDelegate(this.SkillLabBtnClickHandler);
            this.skill2Btn.onClick = new UIWidgetContainer.VoidDelegate(this.SkillLabBtnClickHandler);
            this.skill3Btn.onClick = new UIWidgetContainer.VoidDelegate(this.SkillLabBtnClickHandler);
        }

        private void LeftBtnClickHandler(GameObject go)
        {
            if (this.currentSelectedIndex >= 2)
            {
                this.currentSelectedIndex = 0;
            }
            else
            {
                this.currentSelectedIndex++;
            }
            this.SwapAllPosition(0);
        }

        private void LoadCallback(GameObject go)
        {
            this.SetModelPosition(go);
            this.ShowMode(this.job);
        }

        private void LoadRandomNameCallback(TextAsset nameText)
        {
            XMLNode node = XMLParser.Parse(nameText.ToString());
            string str2 = node.GetValue("root>0>special>0>_text");
            char[] separator = new char[] { ',' };
            this._specialName = str2.Split(separator);
            string str3 = node.GetValue("root>0>man>0>single>0>_text");
            char[] chArray2 = new char[] { ',' };
            this._manSingleName = str3.Split(chArray2);
            string str4 = node.GetValue("root>0>man>0>double>0>_text");
            char[] chArray3 = new char[] { ',' };
            this._manDoubleName = str4.Split(chArray3);
            string str5 = node.GetValue("root>0>women>0>single>0>_text");
            char[] chArray4 = new char[] { ',' };
            this._womanSingleName = str5.Split(chArray4);
            string str6 = node.GetValue("root>0>women>0>double>0>_text");
            char[] chArray5 = new char[] { ',' };
            this._womanDoubleName = str6.Split(chArray5);
            this.RandomName(null);
            this.gameObject.SetActive(true);
        }

        private void LoadRandomNameData()
        {
            AssetManager.Instance.LoadAsset<TextAsset>("Xml/RandomName.assetbundle", new LoadAssetFinish<TextAsset>(this.LoadRandomNameCallback), null, false, true);
        }

        private void LoadRoleMode(uint job)
        {
            if (!this.roleDic.ContainsKey(job))
            {
                this.roleDic[job] = new RoleDisplay();
                this.roleDic[job].CreateRole((int) job, new com.game.module.Role.ModelControl(this.LoadCallback), false);
            }
        }

        private void RandomName(GameObject go)
        {
            this.input.value = this.getRandomName();
        }

        private void RightBtnClickHandler(GameObject go)
        {
            if (this.currentSelectedIndex <= 0)
            {
                this.currentSelectedIndex = 2;
            }
            else
            {
                this.currentSelectedIndex--;
            }
            this.SwapAllPosition(1);
        }

        private void SetModelPosition(GameObject go)
        {
            this.count++;
            go.transform.parent = NGUITools.FindChild(this.gameObject, "panel/mode/hero" + this.count.ToString()).transform;
            go.transform.localPosition = new Vector3(0f, -50f, -100f);
            go.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
            if (this.count == 2)
            {
                go.transform.localScale = new Vector3(145f, 145f, 145f);
            }
            else
            {
                go.transform.localScale = new Vector3(100f, 100f, 100f);
            }
        }

        private void ShowMode(int roleJob)
        {
        }

        private void SkillLabBtnClickHandler(GameObject go)
        {
            string[] param = new string[2];
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map21 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                    dictionary.Add("skill1", 0);
                    dictionary.Add("skill2", 1);
                    dictionary.Add("skill3", 2);
                    <>f__switch$map21 = dictionary;
                }
                if (<>f__switch$map21.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            param[0] = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "1");
                            param[1] = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "1Des");
                            break;

                        case 1:
                            param[0] = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "2");
                            param[1] = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "2Des");
                            break;

                        case 2:
                            param[0] = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "3");
                            param[1] = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "3Des");
                            break;
                    }
                }
            }
            this.skillDesLabel.text = LanguageManager.GetWord("Login.GeneralSkillDes", param);
        }

        private void StartGame(GameObject go)
        {
            string roleName = this.input.value.Replace(" ", string.Empty);
            this.input.label.text = roleName;
            int byteLength = this.GetByteLength(roleName);
            if (StringUtils.isEmpty(roleName) || roleName.Equals("请输入角色名称"))
            {
                this.input.label.text = "请输入角色名称";
            }
            else if ((byteLength < this.NAME_LEN_MIN) || (byteLength > this.NAME_LEN_MAX))
            {
                MessageManager.Show("战队名字为4-14个字符（2-7个汉字）哦~~, 当前为" + byteLength.ToString());
            }
            else
            {
                Singleton<RoleMode>.Instance.roleName = roleName;
                if (Singleton<ChatIIMode>.Instance.ContainsFilter(roleName))
                {
                    MessageManager.Show("名字中包含敏感词汇！");
                    this.input.label.text = "请输入角色名称";
                }
                else
                {
                    Singleton<LoginMode>.Instance.createRole(this.JOB, this.SEX, roleName, this.generalIdList[this.currentSelectedIndex]);
                }
            }
        }

        private void SwapAllPosition(int dir)
        {
            Vector3 position = this.roleDic[this.modelIdList[0]].GoBase.transform.position;
            Vector3 vector2 = this.roleDic[this.modelIdList[1]].GoBase.transform.position;
            Vector3 vector3 = this.roleDic[this.modelIdList[2]].GoBase.transform.position;
            List<Vector3> list = new List<Vector3> {
                vector3,
                position,
                vector2
            };
            List<Vector3> list2 = new List<Vector3> {
                vector2,
                vector3,
                position
            };
            List<Vector3> list3 = new List<Vector3>();
            if (dir == 1)
            {
                list3 = list2;
            }
            else
            {
                list3 = list;
            }
            for (int i = 0; i < this.modelIdList.Count; i++)
            {
                new Hashtable().Add("position", list3[i]);
                iTween.MoveTo(this.roleDic[this.modelIdList[i]].GoBase, list3[i], 0.05f);
                iTween.ScaleTo(this.roleDic[this.modelIdList[i]].GoBase, new Vector3(100f, 100f, 100f), 1f);
            }
            iTween.ScaleTo(this.roleDic[this.modelIdList[this.currentSelectedIndex]].GoBase, new Vector3(145f, 145f, 145f), 1f);
            SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo(this.generalIdList[this.currentSelectedIndex], 0);
            this.heroLabel.text = LanguageManager.GetWord("Login.General" + this.generalIdList[this.currentSelectedIndex].ToString(), generalVo.name);
            string[] param = new string[] { LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "1"), LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "1Des") };
            this.skillDesLabel.text = LanguageManager.GetWord("Login.GeneralSkillDes", param);
            this.skill1Btn.label.text = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "1");
            this.skill2Btn.label.text = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "2");
            this.skill3Btn.label.text = LanguageManager.GetWord("Login.GeneralSkill" + this.generalIdList[this.currentSelectedIndex].ToString() + "3");
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Login/createRolePanel.assetbundle";
            }
        }

        [CompilerGenerated]
        private sealed class <DelayClose>c__Iterator3A : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal RoleCreatePanel <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(1.5f);
                        this.$PC = 1;
                        return true;

                    case 1:
                        Singleton<LoginControl>.Instance.ChangeToFirstScene();
                        CoroutineManager.StopAllCoroutine();
                        this.<>f__this.CloseView();
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

