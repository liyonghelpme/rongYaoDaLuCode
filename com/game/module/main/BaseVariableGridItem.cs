﻿namespace com.game.module.main
{
    using System;
    using UnityEngine;

    public class BaseVariableGridItem : MonoBehaviour
    {
        public virtual int GetHeight()
        {
            return 0;
        }

        public virtual float GetWidth()
        {
            return 0f;
        }
    }
}

