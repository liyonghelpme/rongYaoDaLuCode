﻿namespace com.game.module.main
{
    using com.game.module.chatII;
    using com.game.module.core;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;
    using UnityEngine;

    public class MainBottomChatItemView : BaseVariableGridItem, IBaseMessageView
    {
        private ChatIIVo _chatVo;
        private BoxCollider _collider;
        private UIWidgetContainer _container;
        private MainChatGridView _gridView;
        private UILabel _messageLabel;
        private UISprite _soundIcon;

        private void Awake()
        {
            this.Init();
        }

        public void Clear()
        {
            this._messageLabel.text = string.Empty;
        }

        GameObject IBaseMessageView.get_gameObject()
        {
            return base.gameObject;
        }

        private string GetChannelTypeString(ChatIIType type)
        {
            switch (type)
            {
                case ChatIIType.Public:
                    return "[综合]";

                case ChatIIType.Guild:
                    return "[公会]";

                case ChatIIType.Private:
                    return "[私聊]";

                case ChatIIType.Team:
                    return "[队伍]";
            }
            return string.Empty;
        }

        public override int GetHeight()
        {
            return (((int) this._messageLabel.printedSize.y) + 5);
        }

        private void Init()
        {
            this._messageLabel = NGUITools.FindInChild<UILabel>(base.gameObject, "text");
            this._messageLabel.supportEncoding = true;
            this._soundIcon = NGUITools.FindInChild<UISprite>(base.gameObject, "text/soundIcon");
            this._soundIcon.SetActive(false);
            this._collider = NGUITools.FindInChild<BoxCollider>(base.gameObject, "text");
            this._container = NGUITools.FindInChild<UIWidgetContainer>(base.gameObject, "text");
            this._container.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
        }

        private void OnClick(GameObject go)
        {
            Debug.Log(this._chatVo.senderName);
        }

        private void SetBoxCollider(ChatIIVo vo)
        {
            this._collider.size = (Vector3) NGUIText.CalculatePrintedSize(vo.senderName);
            float x = NGUIText.CalculatePrintedSize(this.GetChannelTypeString((ChatIIType) vo.chatType)).x;
            this._collider.center = new Vector3(((-this._messageLabel.width / 2) + x) + (this._collider.size.x / 2f), -this._collider.size.y / 2f);
        }

        public void SetInfo(params object[] infos)
        {
            ChatIIVo vo = (ChatIIVo) infos[0];
            this._chatVo = vo;
            StringBuilder builder = new StringBuilder();
            builder.Append(this.GetChannelTypeString((ChatIIType) vo.chatType));
            builder.Append(vo.senderName);
            this.SetBoxCollider(vo);
            if (vo.msgType == MessageType.SOUND)
            {
                builder.Append("/031");
            }
            builder.Append(vo.content);
            this._messageLabel.text = builder.ToString();
            this.ShowEmotion(this._messageLabel, this._messageLabel.processedText);
            this._messageLabel.text = Regex.Replace(this._messageLabel.text, this._chatVo.senderName, "[u]" + this._chatVo.senderName + ":[/u]");
            if (infos.Length > 1)
            {
                this._gridView.SetItemPosition(this, (bool) infos[1]);
            }
            else
            {
                this._gridView.SetItemPosition(this, true);
            }
        }

        private void ShowEmotion(UILabel content, string cont)
        {
            List<EmotionInfo> list = new List<EmotionInfo>();
            float num = 0f;
            float y = 0f;
            string str = string.Empty;
            string text = string.Empty;
            string item = string.Empty;
            char[] chArray = Regex.Replace(cont, @"\[u\]|\[/u\]", string.Empty).ToCharArray();
            for (int i = 0; i < chArray.Length; i++)
            {
                if (chArray[i] != '\n')
                {
                    if (chArray[i] == '/')
                    {
                        item = string.Empty;
                        int num4 = (((chArray.Length - 1) - i) <= 2) ? ((chArray.Length - 1) - i) : 3;
                        int num5 = 1;
                        num5 = 1;
                        while (num5 <= num4)
                        {
                            if (chArray[i + num5] == '\n')
                            {
                                num4++;
                                str = str + '\n';
                                text = "\n";
                            }
                            else
                            {
                                item = item + chArray[i + num5];
                            }
                            num5++;
                        }
                        i = (i + num5) - 1;
                        if (Singleton<ChatIIMode>.Instance.emoList.Contains(item))
                        {
                            num = ((NGUIText.CalculatePrintedSize(text).x % ((float) content.width)) - (content.width / 2)) + 10f;
                            y = -NGUIText.CalculatePrintedSize(text).y + 10f;
                            list.Add(new EmotionInfo(new Vector3(num + 10f, y, 0f), "bq_" + item));
                            str = str + "：";
                            text = text + "：";
                        }
                        else
                        {
                            str = str + "/" + item;
                            text = text + "/" + item;
                        }
                    }
                    else
                    {
                        str = str + chArray[i];
                        text = text + chArray[i];
                    }
                    content.text = str;
                }
            }
            int childCount = content.transform.childCount;
            Transform component = content.transform.FindChild("soundIcon").GetComponent<Transform>();
            component.GetComponent<UISprite>().atlas = Singleton<AtlasManager>.Instance.GetAtlas("ChatAtlas");
            while (content.transform.childCount < list.Count)
            {
                (UnityEngine.Object.Instantiate(component.gameObject, component.position, component.rotation) as GameObject).transform.parent = component.parent;
            }
            int num7 = 0;
            IEnumerator enumerator = content.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    if (num7 < list.Count)
                    {
                        current.localPosition = list[num7].loacalPos;
                        current.localRotation = new Quaternion(0f, 0f, 0f, 0f);
                        current.localScale = new Vector3(1f, 1f, 1f);
                        current.gameObject.SetActive(true);
                        current.GetComponent<UISprite>().spriteName = list[num7].emoName;
                        num7++;
                    }
                    else
                    {
                        current.gameObject.SetActive(false);
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
        }

        public UIGrid gridView
        {
            get
            {
                return this._gridView;
            }
            set
            {
                this._gridView = (MainChatGridView) value;
            }
        }
    }
}

