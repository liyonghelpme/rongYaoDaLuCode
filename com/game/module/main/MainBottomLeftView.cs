﻿namespace com.game.module.main
{
    using com.game.module.core;
    using com.u3d.bases.joystick;
    using System;
    using UnityEngine;

    public class MainBottomLeftView : BaseView<MainBottomLeftView>
    {
        private GameObject _joystick;
        private NGUIJoystick joystick;

        protected override void HandleAfterOpenView()
        {
            JoystickController.instance.setCurrentJoystick(this.joystick);
        }

        protected override void Init()
        {
            this._joystick = base.FindChild("Joystick");
            this.joystick = base.FindInChild<NGUIJoystick>("Joystick/button").GetComponent<NGUIJoystick>();
        }

        public override bool isDestroy
        {
            get
            {
                return false;
            }
        }

        public GameObject Joystick
        {
            get
            {
                return this._joystick;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

