﻿namespace com.game.module.main
{
    using com.game;
    using com.game.module.core;
    using com.game.module.map.NPC;
    using com.game.Public.Message;
    using com.liyong;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MainBottomRightView : BaseView<MainBottomRightView>
    {
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache10;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache11;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache12;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache13;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache14;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cacheF;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map24;
        private Button achieve_btn;
        private UISprite backgroundState1;
        private UISprite backgroundState2;
        private Button bag_btn;
        private Button club_btn;
        private Button email_btn;
        private Button forge_btn;
        private Button friend_btn;
        public bool isOpen;
        private GameObject list;
        private GameObject listII;
        private Button open_btn;
        private Button rank_btn;
        private Button team_btn;

        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            this.updateBtnState();
        }

        protected override void Init()
        {
            this.isOpen = false;
            this.team_btn = base.FindInChild<Button>("itemPanel/list/heros");
            this.bag_btn = base.FindInChild<Button>("itemPanel/list/bag");
            this.forge_btn = base.FindInChild<Button>("itemPanel/list/forge");
            this.achieve_btn = base.FindInChild<Button>("itemPanel/list/achieve");
            this.rank_btn = base.FindInChild<Button>("itemPanel/list/rank");
            this.email_btn = base.FindInChild<Button>("itemPanel/listII/email");
            this.club_btn = base.FindInChild<Button>("itemPanel/listII/guild");
            this.open_btn = base.FindInChild<Button>("itemPanel/open_btn");
            this.friend_btn = base.FindInChild<Button>("itemPanel/listII/friends");
            this.backgroundState1 = base.FindInChild<UISprite>("itemPanel/open_btn/Backgroundstate1");
            this.backgroundState2 = base.FindInChild<UISprite>("itemPanel/open_btn/Backgroundstate2");
            this.list = base.FindChild("itemPanel/list");
            this.listII = base.FindChild("itemPanel/listII");
            this.team_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.bag_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.forge_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.achieve_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.rank_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.club_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.open_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.email_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.friend_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map24 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(9);
                    dictionary.Add("heros", 0);
                    dictionary.Add("bag", 1);
                    dictionary.Add("forge", 2);
                    dictionary.Add("achieve", 3);
                    dictionary.Add("rank", 4);
                    dictionary.Add("guild", 5);
                    dictionary.Add("friends", 6);
                    dictionary.Add("email", 7);
                    dictionary.Add("open_btn", 8);
                    <>f__switch$map24 = dictionary;
                }
                if (<>f__switch$map24.TryGetValue(name, out num))
                {
                    BaseSceneNPC npcByModuleId;
                    switch (num)
                    {
                        case 0:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(2);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cacheF == null)
                                {
                                    <>f__am$cacheF = () => Singleton<GeneralPanel>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cacheF);
                            }
                            else
                            {
                                Singleton<GeneralPanel>.Instance.OpenView();
                            }
                            break;

                        case 1:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(0x24);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache10 == null)
                                {
                                    <>f__am$cache10 = () => Singleton<BagView>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache10);
                            }
                            else
                            {
                                Singleton<BagView>.Instance.OpenView();
                            }
                            break;

                        case 2:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(0x2a);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache11 == null)
                                {
                                    <>f__am$cache11 = () => Singleton<EquipmentMainPanel>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache11);
                            }
                            else
                            {
                                Singleton<EquipmentMainPanel>.Instance.OpenView();
                            }
                            break;

                        case 3:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(0x2b);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache12 == null)
                                {
                                    <>f__am$cache12 = () => Singleton<AchievementMainPanel>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache12);
                            }
                            else
                            {
                                Singleton<AchievementMainPanel>.Instance.OpenView();
                            }
                            break;

                        case 4:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(0x15);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache13 == null)
                                {
                                    <>f__am$cache13 = () => Singleton<RankView>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache13);
                            }
                            else
                            {
                                Singleton<RankView>.Instance.OpenView();
                            }
                            break;

                        case 5:
                            MessageManager.Show("此功能暂未开放");
                            break;

                        case 6:
                            MessageManager.Show("此功能暂未开放");
                            break;

                        case 7:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(12);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache14 == null)
                                {
                                    <>f__am$cache14 = () => Singleton<MailView>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache14);
                            }
                            else
                            {
                                Singleton<MailView>.Instance.OpenView();
                            }
                            break;

                        case 8:
                            this.isOpen = !this.isOpen;
                            this.updateBtnState();
                            break;
                    }
                }
            }
        }

        public override void RegisterUpdateHandler()
        {
        }

        private void updateBtnState()
        {
            this.backgroundState1.SetActive(!this.isOpen);
            this.backgroundState2.SetActive(this.isOpen);
            float num = !this.isOpen ? 50f : -520f;
            object[] args = new object[] { "x", num, "dealy", 0.001f, "time", 0.5f, "isLocal", true };
            iTween.MoveTo(this.list, iTween.Hash(args));
            float num2 = !this.isOpen ? -50f : 350f;
            object[] objArray2 = new object[] { "y", num2, "dealy", 0.001f, "time", 0.5f, "isLocal", true };
            iTween.MoveTo(this.listII, iTween.Hash(objArray2));
            Singleton<MainBottomView>.Instance.updateState(!this.isOpen);
        }

        private void UpdateItemView()
        {
        }

        public Button AchieveButton
        {
            get
            {
                return this.club_btn;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public Button OpenButton
        {
            get
            {
                return this.open_btn;
            }
        }

        public Button TeamButton
        {
            get
            {
                return this.team_btn;
            }
        }
    }
}

