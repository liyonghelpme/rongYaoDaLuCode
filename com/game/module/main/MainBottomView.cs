﻿namespace com.game.module.main
{
    using com.game.module.chatII;
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class MainBottomView : BaseView<MainBottomView>
    {
        private GameObject _baseItem;
        private MainChatGridView _gird;
        private Button _messageBtn;
        private GameObject _messagePanel;
        private UIScrollView _scrollView;
        private List<IBaseMessageView> _viewList = new List<IBaseMessageView>();
        private const int CACHED_ITEM_COUNT = 4;

        public void AddText(ChatIIVo addMsg)
        {
            IBaseMessageView item = this._gird.Add<MainBottomChatItemView>(this._baseItem);
            object[] infos = new object[] { addMsg };
            item.SetInfo(infos);
            this._viewList.Add(item);
        }

        public override void CancelUpdateHandler()
        {
            Singleton<ChatIIMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateMsg);
        }

        private void ClearList()
        {
            foreach (IBaseMessageView view in this._viewList)
            {
                view.Clear();
            }
        }

        protected override void HandleAfterOpenView()
        {
            Singleton<ChatIIMode>.Instance.ClearMainViewMsgQueue();
        }

        protected override void Init()
        {
            Singleton<ChatIIMode>.Instance.Init();
            this.InitView();
            this.InitEvent();
        }

        private void InitEvent()
        {
            this._messageBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnMessageClick);
        }

        private void InitView()
        {
            this._baseItem = base.FindChild("messages/list/grid/item");
            this._baseItem.transform.parent = this.transform.parent;
            this._baseItem.SetActive(false);
            this._messagePanel = base.FindChild("messages");
            this._messageBtn = base.FindInChild<Button>("messages/message");
            this._scrollView = base.FindInChild<UIScrollView>("messages/list");
            this._gird = base.FindInChild<MainChatGridView>("messages/list/grid");
            this._gird.maxItemCount = 4;
        }

        private void OnMessageClick(GameObject go)
        {
            Singleton<ChatIIPanel>.Instance.OpenView();
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<ChatIIMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateMsg);
        }

        private void UpdateMsg(object sender, int code)
        {
            if (code == Singleton<ChatIIMode>.Instance.UPDATE_MAIN_CONTENT)
            {
                ChatIIVo mainViewMsg = Singleton<ChatIIMode>.Instance.GetMainViewMsg();
                if (mainViewMsg != null)
                {
                    this.AddText(mainViewMsg);
                }
            }
        }

        public void updateState(bool isOpen)
        {
            float num = !isOpen ? ((float) (-50)) : ((float) 50);
            object[] args = new object[] { "y", num, "dealy", 0.001f, "time", 0.5f, "isLocal", true };
            iTween.MoveTo(this._messagePanel, iTween.Hash(args));
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

