﻿namespace com.game.module.main
{
    using com.game.module.chatII;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MainChatGridView : UIGrid
    {
        private List<IBaseMessageView> _catchdItemsList = new List<IBaseMessageView>();
        private int _currentY;
        private int _lastItemHeight;
        private Vector2 _scorllOffset;
        private UIScrollView _scrollView;
        private Vector3 _scrollViewInitPos;
        private float _scrollViewY;
        private float _totalHight;
        private Queue<IBaseMessageView> _viewQueue = new Queue<IBaseMessageView>();
        public int maxItemCount;

        public IBaseMessageView Add<T>(GameObject item) where T: Component, IBaseMessageView
        {
            IBaseMessageView view = this.CreateMessageViewItem<T>(item);
            view.gameObject.SetActive(true);
            return view;
        }

        private void Awake()
        {
            this.InitView();
        }

        public void Clear()
        {
            foreach (IBaseMessageView view in this._viewQueue)
            {
                view.gameObject.transform.localPosition = new Vector3(0f, 0f, view.gameObject.transform.localPosition.z);
                view.gameObject.SetActive(false);
                view.gameObject.transform.parent = null;
                this._catchdItemsList.Add(view);
            }
            this._scrollView.transform.localPosition = this._scrollViewInitPos;
            this._scrollView.panel.clipOffset = this._scorllOffset;
            this._currentY = 0;
            this._lastItemHeight = 0;
            this._viewQueue.Clear();
            this._scrollViewY = this._scrollViewInitPos.y;
        }

        private IBaseMessageView CreateMessageViewItem<T>(GameObject item) where T: Component, IBaseMessageView
        {
            IBaseMessageView view;
            if (this._catchdItemsList.Count > 0)
            {
                view = this._catchdItemsList[0];
                this._catchdItemsList.RemoveAt(0);
                view.gameObject.SetActive(true);
                view.gameObject.transform.parent = base.transform;
            }
            else
            {
                view = NGUITools.AddChild(base.gameObject, item).AddMissingComponent<T>();
            }
            if (view != null)
            {
                view.gridView = this;
                this._viewQueue.Enqueue(view);
            }
            return view;
        }

        public void InitView()
        {
            this._scrollView = NGUITools.FindInParents<UIScrollView>(base.gameObject);
            this._scrollViewInitPos = this._scrollView.transform.localPosition;
            this._scorllOffset = this._scrollView.panel.clipOffset;
            this._scrollViewY = this._scrollViewInitPos.y;
        }

        [ContextMenu("Execute")]
        public override void Reposition()
        {
            if ((Application.isPlaying && !base.mInitDone) && NGUITools.GetActive(this))
            {
                base.mReposition = true;
            }
            else
            {
                if (!base.mInitDone)
                {
                    this.Init();
                }
                Transform target = base.transform;
                if (base.keepWithinPanel && (base.mPanel != null))
                {
                    base.mPanel.ConstrainTargetToBounds(target, true);
                }
                if (base.onReposition != null)
                {
                    base.onReposition();
                }
            }
        }

        public void SetItemPosition(IBaseMessageView view, bool immediately = true)
        {
            this.SetPosition(view, immediately);
            this.UpdateItemQueue();
        }

        private void SetPosition(IBaseMessageView view, bool immediately = false)
        {
            Transform transform = view.gameObject.transform;
            float z = transform.localPosition.z;
            Vector3 vector = new Vector3(0f, (float) (this._currentY -= this._lastItemHeight), z);
            this._totalHight += view.GetHeight();
            this._lastItemHeight = view.GetHeight();
            transform.localPosition = vector;
            Vector3 pos = new Vector3(0f, 0f, 0f);
            float y = 0f;
            bool flag = false;
            if ((this._totalHight >= this._scrollView.panel.height) && (this._totalHight < (this._scrollView.panel.height + this._lastItemHeight)))
            {
                y = this._totalHight - this._scrollView.panel.height;
                pos = new Vector3(0f, this._scrollViewY + y, this._scrollView.gameObject.transform.localPosition.z);
                flag = true;
            }
            else if (this._totalHight >= (this._scrollView.panel.height + this._lastItemHeight))
            {
                pos = new Vector3(0f, this._scrollViewY + this._lastItemHeight, this._scrollView.gameObject.transform.localPosition.z);
                y = this._lastItemHeight;
                flag = true;
            }
            if (flag)
            {
                if (immediately)
                {
                    this._scrollView.MoveRelative(new Vector3(0f, y, 0f));
                    this._scrollViewY = this._scrollView.gameObject.transform.localPosition.y;
                }
                else
                {
                    SpringPanel.Begin(this._scrollView.gameObject, pos, 15f);
                    this._scrollViewY = pos.y;
                }
            }
        }

        private void UpdateItemQueue()
        {
            if (this._viewQueue.Count > this.maxItemCount)
            {
                IBaseMessageView item = this._viewQueue.Dequeue();
                item.Clear();
                item.gameObject.transform.parent = base.gameObject.transform.parent;
                item.gameObject.SetActive(false);
                this._catchdItemsList.Add(item);
            }
        }
    }
}

