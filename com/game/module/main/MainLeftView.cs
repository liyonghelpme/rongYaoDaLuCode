﻿namespace com.game.module.main
{
    using com.game.module.core;
    using System;

    public class MainLeftView : BaseView<MainLeftView>
    {
        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
        }

        protected override void Init()
        {
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }
    }
}

