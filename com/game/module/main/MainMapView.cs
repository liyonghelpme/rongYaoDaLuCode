﻿namespace com.game.module.main
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class MainMapView : BaseView<MainMapView>
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map25;
        private Button close_btn;
        private Button club_btn;
        private Button copy_btn;
        private Button fight_btn;
        private Button trade_btn;

        protected override void Init()
        {
            this.close_btn = base.FindInChild<Button>("map/close");
            this.club_btn = base.FindInChild<Button>("map/list/club");
            this.copy_btn = base.FindInChild<Button>("map/list/copy");
            this.fight_btn = base.FindInChild<Button>("map/list/fight");
            this.trade_btn = base.FindInChild<Button>("map/list/trade");
            this.club_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.close_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.fight_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.copy_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.trade_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map25 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                    dictionary.Add("club", 0);
                    dictionary.Add("copy", 1);
                    dictionary.Add("fight", 2);
                    dictionary.Add("trade", 3);
                    <>f__switch$map25 = dictionary;
                }
                if (<>f__switch$map25.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            Singleton<MapMode>.Instance.pathType = 3;
                            Singleton<MapControlII>.Instance.updateArea(new Vector3(-27f, 1f, -22f));
                            break;

                        case 1:
                            Singleton<MapMode>.Instance.pathType = 1;
                            Singleton<MapControlII>.Instance.updateArea(new Vector3(-25f, 1f, 22f));
                            break;

                        case 3:
                            Singleton<MapMode>.Instance.pathType = 4;
                            Singleton<MapControlII>.Instance.updateArea(new Vector3(-30f, 1f, 0f));
                            break;
                    }
                }
            }
            this.CloseView();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Main/MainMapView.assetbundle";
            }
        }
    }
}

