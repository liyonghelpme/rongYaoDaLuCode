﻿namespace com.game.module.main
{
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.utils;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MainTopLeftView : BaseView<MainTopLeftView>
    {
        private UIEventListener _vipHot;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map26;
        private Button camera_btn;
        private UILabel cameraLabel;
        private UISlider expSlider;
        private Button fuli_btn;
        private UIEventListener head_btn;
        private UILabel labExp;
        private UILabel labFight;
        private UILabel labLevel;
        private int preLevel;
        private Button recharge_btn;
        private Button sign_btn;
        private UISprite sprHead;
        private UISprite vipNumI;
        private UISprite vipNumII;

        private void ButtonTipsHandle(object sender, int code)
        {
        }

        public override void CancelUpdateHandler()
        {
            Singleton<RoleMode>.Instance.dataUpdated -= new DataUpateHandler(this.updateDataHandler);
            Singleton<CorpsMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateRoleIcon);
            Singleton<CorpsMode>.Instance.dataUpdatedWithParam -= new com.game.module.core.DataUpateHandlerWithParam(this.UpdateRoleName);
            Singleton<VIPMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateVipInfo);
        }

        protected override void HandleAfterOpenView()
        {
            this.updateInfo();
            this.UpdateGetAwardInfo();
        }

        protected override void Init()
        {
            this.sign_btn = base.FindInChild<Button>("function/sign");
            this.recharge_btn = base.FindInChild<Button>("function/recharge");
            this.fuli_btn = base.FindInChild<Button>("function/fuli");
            this.vipNumI = base.FindInChild<UISprite>("head/vip/num");
            this.vipNumII = base.FindInChild<UISprite>("head/vip/numII");
            this.sprHead = base.FindInChild<UISprite>("head/icon");
            this.labLevel = base.FindInChild<UILabel>("head/level/num");
            this.labExp = base.FindInChild<UILabel>("head/exp/num");
            this.labFight = base.FindInChild<UILabel>("head/fight/num");
            this.camera_btn = base.FindInChild<Button>("camera");
            this._vipHot = base.FindChild("head/vip").AddMissingComponent<UIEventListener>();
            this.head_btn = base.FindChild("head").AddComponent<UIEventListener>();
            this.expSlider = base.FindInChild<UISlider>("head/exp/expbar");
            this.sign_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.head_btn.onClick = new UIEventListener.VoidDelegate(this.onClickHandler);
            this.recharge_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.camera_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this._vipHot.onClick = new UIEventListener.VoidDelegate(this.onClickHandler);
            this.fuli_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void onClickCamera()
        {
            Singleton<MapMode>.Instance.AddCameraType();
            Singleton<MapMode>.Instance.ShowMapCameraEffect();
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map26 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(6);
                    dictionary.Add("sign", 0);
                    dictionary.Add("recharge", 1);
                    dictionary.Add("head", 2);
                    dictionary.Add("camera", 3);
                    dictionary.Add("vip", 4);
                    dictionary.Add("fuli", 5);
                    <>f__switch$map26 = dictionary;
                }
                if (<>f__switch$map26.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            MessageManager.Show("此功能暂未开放");
                            break;

                        case 1:
                            MessageManager.Show("此功能暂未开放");
                            break;

                        case 2:
                            Singleton<RoleMode>.Instance.RequestOtherRoleInfo(MeVo.instance.Id);
                            Singleton<CorpsPanel>.Instance.OpenView(PanelType.MAIN_VIEW);
                            break;

                        case 3:
                            this.onClickCamera();
                            break;

                        case 4:
                            Singleton<VIPView>.Instance.OpenView();
                            break;

                        case 5:
                            MessageManager.Show("此功能暂未开放");
                            break;
                    }
                }
            }
        }

        private void OpenVIPView(GameObject go)
        {
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<RoleMode>.Instance.dataUpdated += new DataUpateHandler(this.updateDataHandler);
            Singleton<CorpsMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateRoleIcon);
            Singleton<CorpsMode>.Instance.dataUpdatedWithParam += new com.game.module.core.DataUpateHandlerWithParam(this.UpdateRoleName);
            Singleton<VIPMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateVipInfo);
        }

        public void setRoleName(string changeName)
        {
        }

        public void setVip(int vipLv)
        {
            this.vipNumI.SetActive(false);
            this.vipNumII.SetActive(false);
            if (vipLv < 10)
            {
                this.vipNumI.SetActive(true);
                this.vipNumI.spriteName = vipLv + string.Empty;
            }
            else
            {
                this.vipNumI.SetActive(true);
                this.vipNumII.SetActive(true);
                char[] chArray = vipLv.ToString().Distinct<char>().ToArray<char>();
                this.vipNumI.spriteName = chArray[0] + string.Empty;
                this.vipNumII.spriteName = chArray[1] + string.Empty;
            }
            this.sprHead.SetDimensions(0x11c, 0x56);
        }

        private void updateDataHandler(object sender, int code)
        {
            if ((Singleton<RoleMode>.Instance.PLAYER_UPGRADE_INFO == code) || (code == 8))
            {
                this.updateGrade();
            }
        }

        private void UpdateGetAwardInfo()
        {
        }

        private void updateGrade()
        {
            this.preLevel = MeVo.instance.Level;
            this.labLevel.text = MeVo.instance.Level.ToString();
            this.labExp.text = MeVo.instance.exp + "/" + MeVo.instance.expFull;
            this.labFight.text = MeVo.instance.Fight.ToString();
            this.expSlider.value = ((float) MeVo.instance.exp) / ((float) MeVo.instance.expFull);
        }

        private void UpdateHeadSprite(string headIcon)
        {
            this.sprHead.atlas = Singleton<AtlasManager>.Instance.GetAtlas("BigHeader");
            this.sprHead.spriteName = headIcon;
            this.sprHead.MakePixelPerfect();
            this.sprHead.SetDimensions(0x11c, 0x56);
        }

        private void updateInfo()
        {
            this.setVip(Singleton<VIPMode>.Instance.vip);
            this.updateGrade();
            string headIcon = Singleton<GeneralMode>.Instance.generalInfoList[MeVo.instance.generalId].genralTemplateInfo.big_icon_id.ToString();
            this.UpdateHeadSprite(headIcon);
        }

        private void UpdateRoleIcon(object sender, int code)
        {
            if (Singleton<CorpsMode>.Instance.UPDATE_ROLEICON == code)
            {
                this.UpdateHeadSprite(Singleton<CorpsMode>.Instance.RoleIconId.ToString());
            }
        }

        private void UpdateRoleName(object sender, int code, object param)
        {
            if (Singleton<CorpsMode>.Instance.UPDATE_ROLENAME == code)
            {
                this.setRoleName((string) param);
            }
        }

        private void UpdateVipInfo(object o, int code)
        {
            this.setVip(Singleton<VIPMode>.Instance.vip);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

