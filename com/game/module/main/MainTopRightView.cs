﻿namespace com.game.module.main
{
    using com.game;
    using com.game.module.core;
    using com.game.module.map.NPC;
    using com.liyong;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MainTopRightView : BaseView<MainTopRightView>
    {
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache6;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache7;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache8;
        [CompilerGenerated]
        private static Util.VoidDelegate <>f__am$cache9;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map27;
        private Button copy_btn;
        private Button exercise_btn;
        private Button luck_btn;
        private Button shop_btn;
        private Button task_btn;

        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
        }

        protected override void Init()
        {
            this.shop_btn = base.FindInChild<Button>("navigationBar/shop");
            this.luck_btn = base.FindInChild<Button>("navigationBar/luck");
            this.exercise_btn = base.FindInChild<Button>("navigationBar/exercise");
            this.task_btn = base.FindInChild<Button>("navigationBar/task");
            this.copy_btn = base.FindInChild<Button>("navigationBar/copy");
            this.shop_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.luck_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.exercise_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.task_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
            this.copy_btn.onClick = new UIWidgetContainer.VoidDelegate(this.onClickHandler);
        }

        private void onClickHandler(GameObject go)
        {
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map27 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(5);
                    dictionary.Add("shop", 0);
                    dictionary.Add("copy", 1);
                    dictionary.Add("luck", 2);
                    dictionary.Add("task", 3);
                    dictionary.Add("exercise", 4);
                    <>f__switch$map27 = dictionary;
                }
                if (<>f__switch$map27.TryGetValue(name, out num))
                {
                    BaseSceneNPC npcByModuleId;
                    switch (num)
                    {
                        case 0:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(15);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache6 == null)
                                {
                                    <>f__am$cache6 = () => Singleton<ShopView>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache6);
                            }
                            else
                            {
                                Singleton<ShopView>.Instance.OpenView();
                            }
                            break;

                        case 1:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(0x17);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache7 == null)
                                {
                                    <>f__am$cache7 = () => Singleton<DungeonChapterView>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache7);
                            }
                            else
                            {
                                Singleton<DungeonChapterView>.Instance.OpenView();
                            }
                            break;

                        case 2:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(0x25);
                            Singleton<BaoxiangSystem>.Instance.currentPanel = 0;
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache8 == null)
                                {
                                    <>f__am$cache8 = () => Singleton<BaoxiangSystem>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache8);
                            }
                            else
                            {
                                Singleton<BaoxiangSystem>.Instance.OpenView();
                            }
                            break;

                        case 3:
                            npcByModuleId = AppMap.Instance.GetNpcByModuleId(0x29);
                            if (npcByModuleId != null)
                            {
                                if (<>f__am$cache9 == null)
                                {
                                    <>f__am$cache9 = () => Singleton<DailyTaskView>.Instance.OpenView();
                                }
                                CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, npcByModuleId.Container, <>f__am$cache9);
                            }
                            else
                            {
                                Singleton<DailyTaskView>.Instance.OpenView();
                            }
                            break;

                        case 4:
                            Singleton<ActivityMode>.Instance.GetActivityInfos();
                            break;
                    }
                }
            }
        }

        public override void RegisterUpdateHandler()
        {
        }

        public Button DungeonButton
        {
            get
            {
                return this.copy_btn;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        public Button LuckBtn
        {
            get
            {
                return this.luck_btn;
            }
        }
    }
}

