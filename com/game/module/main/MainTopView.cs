﻿namespace com.game.module.main
{
    using com.game.module.core;
    using System;

    internal class MainTopView : BaseView<MainTopView>
    {
        private MainToolView _toolViwView;

        protected override void HandleAfterOpenView()
        {
            this._toolViwView.isActive = true;
        }

        protected override void HandleBeforeCloseView()
        {
            this._toolViwView.isActive = false;
        }

        protected override void Init()
        {
            this._toolViwView = new MainToolView(base.FindChild("info"));
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

