﻿namespace com.game.module.main
{
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class MainView : BaseView<MainView>
    {
        private UILabel labNotice;
        private GameObject noticeObj;
        private GameObject topright;

        private void AcceptInvite()
        {
        }

        public override void CancelUpdateHandler()
        {
            Singleton<ChatMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateNoticeHandle);
            Singleton<ChatMode>.Instance.dataUpdated -= new DataUpateHandler(this.UpdateRumorHandle);
        }

        public void CloseViewByCopy()
        {
        }

        private void DenyInvite()
        {
        }

        private void FinishNoticeMove()
        {
            this.noticeObj.SetActive(false);
        }

        protected override void HandleAfterOpenView()
        {
            base.HandleAfterOpenView();
            Singleton<MainTopView>.Instance.OpenView();
            Singleton<MainTopRightView>.Instance.OpenView();
            Singleton<MainBottomLeftView>.Instance.OpenView();
            Singleton<MainBottomRightView>.Instance.OpenView();
            Singleton<MainBottomView>.Instance.OpenView();
            Singleton<MainTopLeftView>.Instance.OpenView();
        }

        protected override void HandleBeforeCloseView()
        {
            Singleton<MainTopView>.Instance.CloseView();
            Singleton<MainBottomLeftView>.Instance.CloseView();
            Singleton<MainBottomRightView>.Instance.CloseView();
            Singleton<MainBottomView>.Instance.CloseView();
            Singleton<MainTopLeftView>.Instance.CloseView();
            Singleton<MainTopRightView>.Instance.CloseView();
        }

        protected override void Init()
        {
            this.topright = base.FindChild("topright");
            Singleton<MainTopView>.Instance.gameObject = base.FindChild("top");
            Singleton<MainTopRightView>.Instance.gameObject = base.FindChild("topright");
            Singleton<MainTopLeftView>.Instance.gameObject = base.FindChild("topleft");
            Singleton<MainBottomLeftView>.Instance.gameObject = base.FindChild("bottomleft");
            Singleton<MainBottomRightView>.Instance.gameObject = base.FindChild("bottomright");
            Singleton<MainBottomView>.Instance.gameObject = base.FindChild("bottom");
        }

        public void OpenViewBy()
        {
            this.OpenView();
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<ChatMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateNoticeHandle);
            Singleton<ChatMode>.Instance.dataUpdated += new DataUpateHandler(this.UpdateRumorHandle);
        }

        private void ShowNoticeMoveEffect(UILabel labNotice, Vector3 from, Vector3 to, float moveTime)
        {
            TweenPosition component = labNotice.GetComponent<TweenPosition>();
            if (null != component)
            {
                UnityEngine.Object.Destroy(component);
            }
            component = labNotice.gameObject.AddComponent<TweenPosition>();
            component.from = from;
            component.to = to;
            component.style = UITweener.Style.Once;
            component.method = UITweener.Method.None;
            component.duration = moveTime;
        }

        public override void Update()
        {
            Singleton<MainBottomLeftView>.Instance.Update();
        }

        private void UpdateFriendReply()
        {
        }

        private void UpdateFriendReplyHandle(object sender, int type)
        {
        }

        private void UpdateInviteRequest()
        {
        }

        private void UpdateInviteRequestHandle(object sender, int type)
        {
        }

        private void UpdateNoticeHandle(object sender, int type)
        {
            if (Singleton<ChatMode>.Instance.UPDATE_NOTICE == type)
            {
                this.UpdateNoticeRequest(Singleton<ChatMode>.Instance.Notice);
            }
        }

        private void UpdateNoticeRequest(string notice)
        {
            float num = 512f;
            float num2 = 100f;
            Vector3 localPosition = this.labNotice.transform.localPosition;
            localPosition.x = num;
            Vector3 to = localPosition;
            to.x = -NGUIText.CalculatePrintedSize(this.labNotice.text).x - num;
            this.noticeObj.SetActive(true);
            float moveTime = (localPosition.x - to.x) / num2;
            this.ShowNoticeMoveEffect(this.labNotice, localPosition, to, moveTime);
            vp_Timer.In(moveTime, new vp_Timer.Callback(this.FinishNoticeMove), 1, 0f, null);
        }

        private void UpdateRumorHandle(object sender, int type)
        {
            if (Singleton<ChatMode>.Instance.UPDATE_RUMOR == type)
            {
                this.UpdateNoticeRequest(Singleton<ChatMode>.Instance.Rumor);
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.LowLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Main/MainView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

