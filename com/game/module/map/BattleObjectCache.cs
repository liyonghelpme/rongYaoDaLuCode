﻿namespace com.game.module.map
{
    using com.game.data;
    using com.game.manager;
    using com.u3d.bases.display.vo;
    using PCustomDataType;
    using SimpleJSON;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityLog;

    public class BattleObjectCache
    {
        public JSONArray buffList;
        public int BuffSpawnPointId = MaxSpawnId++;
        public IList<int> delayList;
        private static int MaxSpawnId;
        private float NextGenTime;
        private int nowIndex;
        private bool PickedYet = true;
        public int rotateY;
        public int x;
        public int y;
        public int z;

        public bool CheckTime()
        {
            Log.AI(null, string.Concat(new object[] { "PickYet ", this.BuffSpawnPointId, " pickyet ", this.PickedYet }));
            if (this.PickedYet)
            {
                Log.AI(null, string.Concat(new object[] { "CheckTime currentTime ", Time.time, " nexttime ", this.NextGenTime, " spawnPoint ", this.BuffSpawnPointId }));
                return (Time.time > this.NextGenTime);
            }
            return false;
        }

        public void GenerateSuc()
        {
            Log.AI(null, "GenerateSuc " + this.BuffSpawnPointId);
            this.PickedYet = false;
            BattleObjectMgr.Instance.InAddBuffLock = false;
        }

        public int GetRandomTemplateId()
        {
            float num = 0f;
            float num2 = UnityEngine.Random.Range((float) 0f, (float) 1f) * 1000f;
            Log.AI(null, "Possibility " + num2);
            Log.AI(null, string.Concat(new object[] { "buffList ", this.buffList.ToString(), " count ", this.buffList.Count }));
            for (int i = 0; i < this.buffList.Count; i++)
            {
                int asInt = this.buffList[i][0].AsInt;
                int num5 = this.buffList[i][1].AsInt;
                Log.AI(null, string.Concat(new object[] { "possibility ", num2, " rate ", num5, " lastRate ", num, " id ", asInt, " i ", i }));
                if ((num2 > num) && (num2 < (num + num5)))
                {
                    return asInt;
                }
                num += num5;
            }
            return -1;
        }

        public static List<int> HandleDelayString(int id)
        {
            SysDelayControlVo dataById = BaseDataMgr.instance.GetDataById<SysDelayControlVo>((uint) id);
            JSONArray asArray = JSON.Parse(dataById.delay_list).AsArray;
            Log.AI(null, string.Concat(new object[] { " ReadDelayList ", id, " delayList ", dataById.delay_list, " array ", asArray.Count }));
            List<int> list = new List<int>();
            IEnumerator enumerator = asArray.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    JSONNode current = (JSONNode) enumerator.Current;
                    list.Add(current.AsInt);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            return list;
        }

        public void InitState()
        {
            this.PickedYet = true;
            this.SetNextGenTime();
        }

        public void PickMyBuf(BattleObjectVo vo)
        {
            if ((vo == null) || (vo.SpawnPoolId == this.BuffSpawnPointId))
            {
                Log.AI(null, "PickMyBuf " + this.BuffSpawnPointId);
                this.PickedYet = true;
                this.SetNextGenTime();
            }
        }

        public bool RequestGenerate()
        {
            PBattleObject obj2 = BattleObjectMgr.Instance.AddBattleObject(this);
            if (obj2 != null)
            {
                Log.AI(null, " RequestGenerate " + this.BuffSpawnPointId);
                List<PBattleObject> cacheReqList = new List<PBattleObject> {
                    obj2
                };
                BattleObjectMgr.Instance.InAddBuffLock = true;
                BattleObjectMgr.Instance.DoRequestServerAddBattleObject(cacheReqList);
                return true;
            }
            return false;
        }

        private void SetNextGenTime()
        {
            int num = (this.nowIndex < (this.delayList.Count - 1)) ? this.nowIndex++ : (this.delayList.Count - 1);
            this.NextGenTime = Time.time + (((float) this.delayList[num]) * 0.001f);
            Log.AI(null, string.Concat(new object[] { "NextGenerateTime ", this.NextGenTime, " Now ", Time.time }));
        }
    }
}

