﻿namespace com.game.module.map
{
    using com.u3d.bases.display.vo;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class BattleObjectManagerHelper
    {
        public static void Add(this IDictionary<Vector3, BattleObjectVo> dic, BattleObjectVo vo)
        {
            Vector3 key = new Vector3(vo.X, vo.Y, vo.Z);
            dic.Add(key, vo);
        }

        public static bool Contains(this IDictionary<Vector3, BattleObjectVo> dic, BattleObjectVo vo)
        {
            Vector3 key = new Vector3(vo.X, vo.Y, vo.Z);
            return dic.ContainsKey(key);
        }

        public static bool Remove(this IDictionary<Vector3, BattleObjectVo> dic, BattleObjectVo vo)
        {
            Vector3 key = new Vector3(vo.X, vo.Y, vo.Z);
            return dic.Remove(key);
        }
    }
}

