﻿namespace com.game.module.map
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.vo;
    using PCustomDataType;
    using SimpleJSON;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class BattleObjectMgr
    {
        private readonly IDictionary<ulong, BattleObjectVo> _allList = new Dictionary<ulong, BattleObjectVo>();
        private readonly List<PBattleObject> _cacheReqList = new List<PBattleObject>();
        private readonly ICollection<BattleObjectCache> _delList = new List<BattleObjectCache>();
        private bool _isInitialized;
        private bool _isMaster;
        private float _nowTime;
        private readonly ICollection<BattleObjectCache> _restList = new List<BattleObjectCache>();
        private static BattleObjectViewMgr _viewMgr;
        private readonly IList<BattleObjectVo> _waitList = new List<BattleObjectVo>();
        private static ulong BattleObjectMaxId;
        private BattleObjectCache currentGenCache;
        public bool InAddBuffLock;
        public static readonly BattleObjectMgr Instance;

        static BattleObjectMgr()
        {
            if (Instance == null)
            {
            }
            Instance = new BattleObjectMgr();
            if (_viewMgr == null)
            {
            }
            _viewMgr = new BattleObjectViewMgr();
            BattleObjectMaxId = 0L;
        }

        private BattleObjectMgr()
        {
        }

        public void Add(BattleObjectVo vo)
        {
            Log.AI(null, "Add BattleVO " + vo);
            if (vo != null)
            {
                this._allList.Add(vo.Id, vo);
                this._waitList.Add(vo);
                if (this._isMaster && (this.currentGenCache != null))
                {
                    vo.SpawnPoolId = this.currentGenCache.BuffSpawnPointId;
                    Log.AI(null, "Set Buff SpawnPoint " + vo.SpawnPoolId);
                    this.currentGenCache.GenerateSuc();
                }
                Log.AI(null, "Server Generate Suc");
            }
        }

        public PBattleObject AddBattleObject(BattleObjectCache cache)
        {
            this.currentGenCache = cache;
            int randomTemplateId = cache.GetRandomTemplateId();
            Log.AI(null, "PVP AddBattleObject " + randomTemplateId);
            if (randomTemplateId == -1)
            {
                Log.AI(null, "This Time Generate Fail " + this.currentGenCache.BuffSpawnPointId);
                this.currentGenCache.GenerateSuc();
                this.currentGenCache.PickMyBuf(null);
                this.currentGenCache = null;
                return null;
            }
            return new PBattleObject { id = BattleObjectMaxId += (ulong) 1L, dir = Convert.ToInt32(cache.rotateY), x = Convert.ToInt32(cache.x), y = Convert.ToInt32(cache.y), z = Convert.ToInt32(cache.z), templateId = (uint) randomTemplateId };
        }

        public void Clear()
        {
            Log.AI(null, " Why Clear BattleObjMgr");
            bool flag = false;
            this.IsInitialized = flag;
            this._isMaster = flag;
            this._allList.Clear();
            this._waitList.Clear();
            this._delList.Clear();
            this._cacheReqList.Clear();
            this._restList.Clear();
            _viewMgr.ClearAllBattleObject();
        }

        public void DoRequestServerAddBattleObject(List<PBattleObject> cacheReqList)
        {
            Singleton<MapMode>.Instance.AddBattleObject(cacheReqList);
        }

        private void Execute()
        {
            if (Singleton<MapMode>.Instance.isSceneComplete && (this._waitList.Count > 0))
            {
                BattleObjectVo vo = this._waitList[0];
                this._waitList.RemoveAt(0);
                _viewMgr.CreateBattleObject(vo);
            }
        }

        public IDictionary<ulong, BattleObjectDisplay> GetAllBattleObject()
        {
            return _viewMgr.GetBattleObjectLists();
        }

        public BattleObjectVo GetBattleObject(BattleObjectVo vo)
        {
            return this.GetBattleObject(vo.Id);
        }

        public BattleObjectVo GetBattleObject(ulong id)
        {
            BattleObjectVo vo = null;
            this._allList.TryGetValue(id, out vo);
            return vo;
        }

        private static void HandleBattleObjectPicked(BattleObjectVo vo, ActionDisplay picker)
        {
            if (picker.Controller.buffController != null)
            {
                if (picker is MeDisplay)
                {
                    picker.Controller.buffController.AddBuffList(vo.buff_list, null);
                }
                else if (picker is PlayerDisplay)
                {
                    picker.Controller.buffController.WorkBuffEffectSync(vo.buff_list);
                }
                _viewMgr.DestoryBattleObject(vo.Id);
            }
        }

        private void InformPicked(ActionDisplay picker, BattleObjectVo vo)
        {
            if (picker is MeDisplay)
            {
                Debug.Log("我方英雄捡到BUFF符!");
                Singleton<WifiPvpMode>.Instance.TellOthersBattleObjectPicked(picker, vo);
            }
        }

        public void InitAsMaster(string battleObjectString)
        {
            this.Clear();
            this._restList.Clear();
            JSONArray asArray = JSON.Parse(battleObjectString).AsArray;
            if (asArray.Count == 0)
            {
                Debug.LogError(" BattleObjectList Empty " + battleObjectString);
            }
            IEnumerator enumerator = asArray.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    JSONNode current = (JSONNode) enumerator.Current;
                    Log.AI(null, "Init Battle GameObject " + current.ToString());
                    BattleObjectCache item = new BattleObjectCache {
                        buffList = current[0].AsArray,
                        delayList = BattleObjectCache.HandleDelayString(current[1].AsInt),
                        x = current[3].AsInt,
                        y = current[4].AsInt,
                        z = current[5].AsInt,
                        rotateY = current[6].AsInt
                    };
                    item.InitState();
                    this._restList.Add(item);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            this.IsInitialized = true;
            this._isMaster = true;
        }

        public void InitAsSlave()
        {
            this.Clear();
            this.IsInitialized = true;
            this._isMaster = false;
        }

        private void NotifySpawnPointPicked(BattleObjectVo vo)
        {
            Log.AI(null, "NotifySpawnPointPicked " + vo.SpawnPoolId);
            IEnumerator<BattleObjectCache> enumerator = this._restList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.PickMyBuf(vo);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void Pick(BattleObjectVo vo, ActionDisplay picker)
        {
            if ((vo == null) || !this._allList.ContainsKey(vo.Id))
            {
                Debug.LogError("拾取到的vo为null或不存在");
            }
            else
            {
                this.NotifySpawnPointPicked(vo);
                this.InformPicked(picker, vo);
                HandleBattleObjectPicked(vo, picker);
            }
        }

        public void PickedByOthers(ulong objId, ulong playerId)
        {
            <PickedByOthers>c__AnonStoreyF7 yf = new <PickedByOthers>c__AnonStoreyF7 {
                playerId = playerId
            };
            BattleObjectVo battleObject = this.GetBattleObject(objId);
            PlayerDisplay picker = AppMap.Instance.playerList.FirstOrDefault<PlayerDisplay>(new Func<PlayerDisplay, bool>(yf.<>m__F4));
            if ((battleObject == null) || (picker == null))
            {
                Debug.LogError("同步他人拾取时取不到玩家或者场景物件");
            }
            else
            {
                this.NotifySpawnPointPicked(battleObject);
                HandleBattleObjectPicked(battleObject, picker);
            }
        }

        public void Remove(BattleObjectVo vo)
        {
            if (vo != null)
            {
                this._allList.Remove(vo.Id);
                this._waitList.Remove(vo);
                _viewMgr.DestoryBattleObject(vo.Id);
            }
        }

        public void Update()
        {
            Log.AI(null, string.Concat(new object[] { " update BattleObj ", this.IsInitialized, " isMaster ", this._isMaster }));
            if (this.IsInitialized)
            {
                if (this._isMaster)
                {
                    this.UpdateCache();
                }
                this.Execute();
            }
        }

        private void UpdateCache()
        {
            Log.AI(null, " UpdateCache " + this.InAddBuffLock);
            if (!this.InAddBuffLock)
            {
                IEnumerator<BattleObjectCache> enumerator = this._restList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        BattleObjectCache current = enumerator.Current;
                        if (current.CheckTime() && current.RequestGenerate())
                        {
                            return;
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        private bool IsInitialized
        {
            get
            {
                return this._isInitialized;
            }
            set
            {
                this._isInitialized = value;
            }
        }

        [CompilerGenerated]
        private sealed class <PickedByOthers>c__AnonStoreyF7
        {
            internal ulong playerId;

            internal bool <>m__F4(PlayerDisplay x)
            {
                return (x.GetVo().Id == this.playerId);
            }
        }
    }
}

