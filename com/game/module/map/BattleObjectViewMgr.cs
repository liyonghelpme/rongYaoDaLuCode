﻿namespace com.game.module.map
{
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.vo;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class BattleObjectViewMgr
    {
        private readonly IDictionary<ulong, BattleObjectDisplay> _list = new Dictionary<ulong, BattleObjectDisplay>();
        private Dictionary<uint, BattleObjectVo> cacheBuff = new Dictionary<uint, BattleObjectVo>();

        public void ClearAllBattleObject()
        {
            if (this._list.Count > 0)
            {
                IEnumerator<BattleObjectDisplay> enumerator = this._list.Values.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        enumerator.Current.Dispose();
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                this._list.Clear();
            }
        }

        public void CreateBattleObject(BattleObjectVo vo)
        {
            vo.Type = 800;
            BattleObjectDisplay display = new BattleObjectDisplay();
            vo.ClothUrl = "Model/Monster/" + vo.SysBattleObjectVo.model_id.ToString() + "/Model/BIP.assetbundle";
            SysModelVo modelVo = BaseDataMgr.instance.GetModelVo((uint) vo.SysBattleObjectVo.model_id);
            if (modelVo != null)
            {
                vo.scale = MathUtils.GetVector3(modelVo.scale);
                vo.isChangeModleTransform = true;
            }
            display.SetVo(vo);
            this._list.Add(vo.Id, display);
        }

        public void DestoryBattleObject(ulong id)
        {
            if (!this._list.ContainsKey(id))
            {
                Debug.LogError("dispose场景物件有误");
            }
            else
            {
                BattleObjectDisplay display = this._list[id];
                this._list.Remove(id);
                display.Dispose();
            }
        }

        public IDictionary<ulong, BattleObjectDisplay> GetBattleObjectLists()
        {
            return this._list;
        }
    }
}

