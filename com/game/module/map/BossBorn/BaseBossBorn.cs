﻿namespace com.game.module.map.BossBorn
{
    using com.game;
    using com.liyong;
    using com.u3d.bases.display.character;
    using System;
    using System.Runtime.CompilerServices;

    public class BaseBossBorn : IBossBorn
    {
        public virtual void Exit(BossDeployItemInfo info)
        {
            if (this.boss == null)
            {
            }
            this.boss = AppMap.Instance.GetMonster(info.bossId.ToString());
            this.boss.Pos(info.endPoint.x, info.endPoint.y, info.endPoint.z);
            CommandHandler.AddCommandStatic(this.boss.Controller.gameObject, "idle", 1f);
        }

        public virtual int GetType()
        {
            return 0;
        }

        public virtual void Handler(BossDeployItemInfo info)
        {
        }

        public MonsterDisplay boss { get; set; }
    }
}

