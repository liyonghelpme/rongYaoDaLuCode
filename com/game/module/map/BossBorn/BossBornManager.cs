﻿namespace com.game.module.map.BossBorn
{
    using System;
    using System.Collections.Generic;

    public class BossBornManager
    {
        public Dictionary<int, IBossBorn> bossBornList = new Dictionary<int, IBossBorn>();
        private BossDeployItemInfo curDeployItemInfo;

        private void addBossBornInfo(IBossBorn bossBorn)
        {
            this.bossBornList.Add(bossBorn.GetType(), bossBorn);
        }

        public void Exit()
        {
            if (this.CurrentDeployItemInfo != null)
            {
                this.bossBornList[this.CurrentDeployItemInfo.type].Exit(this.CurrentDeployItemInfo);
            }
        }

        public void Handler(int type, BossDeployItemInfo info)
        {
            this.curDeployItemInfo = info;
            this.bossBornList[type].Handler(info);
        }

        public void SetUp()
        {
            this.addBossBornInfo(new PlayerAnimationBossBorn());
            this.addBossBornInfo(new RiseUpGroundBossBorn());
            this.addBossBornInfo(new ThroughoutDoorBossBorn());
            this.addBossBornInfo(new RunToPointBossBorn());
        }

        public BossDeployItemInfo CurrentDeployItemInfo
        {
            get
            {
                return this.curDeployItemInfo;
            }
            set
            {
                this.curDeployItemInfo = value;
            }
        }
    }
}

