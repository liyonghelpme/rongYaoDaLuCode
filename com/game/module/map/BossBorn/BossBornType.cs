﻿namespace com.game.module.map.BossBorn
{
    using System;

    public class BossBornType
    {
        public const int PlayerAnimation = 1;
        public const int RiseUpGround = 2;
        public const int RunToPoint = 4;
        public const int ThroughoutDoor = 3;
    }
}

