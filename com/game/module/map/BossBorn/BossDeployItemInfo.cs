﻿namespace com.game.module.map.BossBorn
{
    using System;
    using UnityEngine;

    public class BossDeployItemInfo
    {
        public int animationId;
        public ulong bossId;
        public Vector3 endPoint;
        public object param;
        public float param1;
        public float param2;
        public float param3;
        public Vector3 startPoint;
        public int type;
    }
}

