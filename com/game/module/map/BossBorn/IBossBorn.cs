﻿namespace com.game.module.map.BossBorn
{
    using com.u3d.bases.display.character;
    using System;

    public interface IBossBorn
    {
        void Exit(BossDeployItemInfo info);
        int GetType();
        void Handler(BossDeployItemInfo info);

        MonsterDisplay boss { get; set; }
    }
}

