﻿namespace com.game.module.map.BossBorn
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.display.controler;
    using System;

    public class PlayerAnimationBossBorn : BaseBossBorn
    {
        public override void Exit(BossDeployItemInfo info)
        {
            this.boss.Controller.StatuController.SetStatu(0);
        }

        public override int GetType()
        {
            return 1;
        }

        public override void Handler(BossDeployItemInfo info)
        {
            this.boss = AppMap.Instance.GetMonster(info.bossId.ToString());
            Singleton<BattleMode>.Instance.SetAllSelfAndMonstersAI(false, false);
            this.boss.Controller.StatuController.SetStatu(info.animationId);
        }

        private void MoveComplete(BaseControler controller)
        {
        }
    }
}

