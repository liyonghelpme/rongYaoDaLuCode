﻿namespace com.game.module.map.BossBorn
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.display.controler;
    using System;

    public class RiseUpGroundBossBorn : BaseBossBorn
    {
        public override void Exit(BossDeployItemInfo info)
        {
            base.Exit(info);
        }

        public override int GetType()
        {
            return 2;
        }

        public override void Handler(BossDeployItemInfo info)
        {
            this.boss = AppMap.Instance.GetMonster(info.bossId.ToString());
            Singleton<BattleMode>.Instance.SetAllSelfAndMonstersAI(false, false);
            this.boss.Pos(info.startPoint.x, info.startPoint.y, info.startPoint.z);
            (this.boss.Controller as ActionControler).WalkTo(info.endPoint, new MoveEndCallback(this.WalkEnd), false, false);
        }

        private void WalkEnd(BaseControler controller)
        {
            controller.AiController.SetAi(true);
        }
    }
}

