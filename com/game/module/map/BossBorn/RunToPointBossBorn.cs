﻿namespace com.game.module.map.BossBorn
{
    using com.game;
    using com.game.module.core;
    using com.liyong;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class RunToPointBossBorn : BaseBossBorn
    {
        public override void Exit(BossDeployItemInfo info)
        {
            if (this.boss == null)
            {
            }
            this.boss = AppMap.Instance.GetMonster(info.bossId.ToString());
            this.boss.Pos(info.endPoint.x, info.endPoint.y, info.endPoint.z);
            this.boss.Controller.StatuController.SetStatu(0);
        }

        public override int GetType()
        {
            return 4;
        }

        public override void Handler(BossDeployItemInfo info)
        {
            this.boss = AppMap.Instance.GetMonster(info.bossId.ToString());
            Singleton<BattleMode>.Instance.SetAllSelfAndMonstersAI(false, false);
            this.boss.Pos(info.startPoint.x, info.startPoint.y, info.startPoint.z);
            CommandHandler.AddCommandStatic(this.boss.Controller.gameObject, string.Format("move_attack {0}", -1), 1f);
            Vector3 endPoint = info.endPoint;
            int num = this.boss.Controller.monsterMoveAiPath.AddCallbackMap(delegate {
                this.MoveComplete(this.boss.Controller);
                CommandHandler.AddCommandStatic(this.boss.GoBase, "idle", 1f);
            });
            object[] args = new object[] { endPoint.x, endPoint.y, endPoint.z, num };
            CommandHandler.AddCommandStatic(this.boss.GoBase, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
        }

        private void MoveComplete(BaseControler controller)
        {
            controller.AiController.SetAi(true);
        }
    }
}

