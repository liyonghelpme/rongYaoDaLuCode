﻿namespace com.game.module.map
{
    using System;

    public class ChestEvent
    {
        public const int ADD = 1;
        public const int CLEAR = 4;
        public const int PICK = 3;
        public const int REMOVE = 2;
    }
}

