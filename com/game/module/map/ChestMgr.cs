﻿namespace com.game.module.map
{
    using com.game.basic;
    using com.game.module.core;
    using com.u3d.bases.display.vo;
    using System;
    using System.Collections.Generic;

    public class ChestMgr : BaseMode<ChestMgr>
    {
        private Dictionary<uint, ChestVo> _allList = new Dictionary<uint, ChestVo>();
        private Dictionary<uint, ChestVo> _pickedList = new Dictionary<uint, ChestVo>();
        private ChestViewMgr _viewMgr = new ChestViewMgr();
        private List<ChestVo> _waitList = new List<ChestVo>();

        public void Add(ChestVo vo)
        {
            if ((vo != null) && !this._allList.ContainsKey(vo.sysId))
            {
                this._allList.Add(vo.sysId, vo);
                this._waitList.Add(vo);
                base.DataUpdateWithParam(1, vo);
            }
        }

        public void Clear()
        {
            if (this._allList.Count > 0)
            {
                this._allList.Clear();
                this._waitList.Clear();
                this._pickedList.Clear();
                this._viewMgr.Clear();
                base.DataUpdateWithParam(4, null);
            }
        }

        public void Execute()
        {
            if (Singleton<MapMode>.Instance.isSceneComplete && (this._waitList.Count > 0))
            {
                ChestVo vo = this._waitList[this._waitList.Count - 1];
                this._waitList.RemoveAt(this._waitList.Count - 1);
                this._viewMgr.CreateChest(vo);
            }
        }

        public void Pick(ChestVo vo)
        {
            if ((vo != null) && (this._allList.ContainsKey(vo.sysId) && !this._pickedList.ContainsKey(vo.sysId)))
            {
                this._pickedList.Add(vo.sysId, vo);
                base.DataUpdateWithParam(3, vo);
                GlobalAPI.facade.Notify(3, (int) vo.Id, 1, null);
            }
        }

        public void Remove(ChestVo vo)
        {
            if ((vo != null) && this._allList.ContainsKey(vo.sysId))
            {
                this._allList.Remove(vo.sysId);
                this._pickedList.Remove(vo.sysId);
                this._waitList.Remove(vo);
                this._viewMgr.DestroyChest(vo.sysId);
                base.DataUpdateWithParam(2, vo);
            }
        }

        public int numChestTotal
        {
            get
            {
                return this._allList.Count;
            }
        }

        public int numNotPicked
        {
            get
            {
                return (this.numChestTotal - this.numPicked);
            }
        }

        public int numPicked
        {
            get
            {
                return this._pickedList.Count;
            }
        }

        public List<ChestVo> pickList
        {
            get
            {
                List<ChestVo> list = new List<ChestVo>();
                foreach (ChestVo vo in this._pickedList.Values)
                {
                    list.Add(vo);
                }
                return list;
            }
        }
    }
}

