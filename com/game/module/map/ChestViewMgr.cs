﻿namespace com.game.module.map
{
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.vo;
    using System;
    using System.Collections.Generic;

    internal class ChestViewMgr
    {
        private Dictionary<uint, ChestDisplay> _list = new Dictionary<uint, ChestDisplay>();

        public void Clear()
        {
            if (this._list.Count > 0)
            {
                foreach (ChestDisplay display in this._list.Values)
                {
                    display.Dispose();
                }
                this._list.Clear();
            }
        }

        public void CreateChest(ChestVo vo)
        {
            vo.Type = 700;
            ChestDisplay display = new ChestDisplay();
            vo.ClothUrl = "Model/Monster/300002/Model/BIP.assetbundle";
            SysModelVo modelVo = BaseDataMgr.instance.GetModelVo(0x493e2);
            if (modelVo != null)
            {
                vo.scale = MathUtils.GetVector3(modelVo.scale);
                vo.isChangeModleTransform = true;
            }
            display.SetVo(vo);
            this._list.Add(vo.sysId, display);
        }

        public void DestroyChest(uint uid)
        {
            if (this._list.ContainsKey(uid))
            {
                ChestDisplay display = this._list[uid];
                this._list.Remove(uid);
                display.Dispose();
            }
        }
    }
}

