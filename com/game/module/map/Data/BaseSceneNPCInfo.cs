﻿namespace com.game.module.map.Data
{
    using com.game.data;
    using com.game.manager;
    using System;

    public class BaseSceneNPCInfo : BaseSceneObjInfo
    {
        protected uint id;
        public SysNpcVo npcTemplate;
        private uint templateId;

        public override string GetName()
        {
            return this.npcTemplate.name;
        }

        public override uint GetObjId()
        {
            return this.id;
        }

        public override int GetObjType()
        {
            return 5;
        }

        public void SetObjId(uint id)
        {
            this.id = id;
        }

        public void SetTemplateId(uint templateId)
        {
            if (templateId != this.id)
            {
                this.templateId = templateId;
                this.npcTemplate = BaseDataMgr.instance.GetNpcVoById(templateId);
            }
        }

        public override int OpenModuleId
        {
            get
            {
                if (this.npcTemplate != null)
                {
                    char[] separator = new char[] { ',' };
                    string s = this.npcTemplate.moduleId.Split(separator)[1];
                    return int.Parse(s);
                }
                return 0;
            }
        }
    }
}

