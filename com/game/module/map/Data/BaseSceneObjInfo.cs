﻿namespace com.game.module.map.Data
{
    using com.game.basic.events;
    using System;
    using UnityEngine;

    public class BaseSceneObjInfo : EventDispatcher
    {
        public int modelId;
        public Vector3 rotation;
        public Vector3 scenePos;
        public float sceneX;
        public float sceneY;
        public float sceneZ;
        public int type;

        public virtual void Dispose()
        {
        }

        public virtual uint GetLevel()
        {
            return 0;
        }

        public virtual string GetName()
        {
            return string.Empty;
        }

        public virtual uint GetObjId()
        {
            return 0;
        }

        public virtual int GetObjType()
        {
            return 0;
        }

        public void SceneRemove()
        {
        }

        public virtual void SetName()
        {
        }

        public virtual int OpenModuleId
        {
            get
            {
                return 0;
            }
        }
    }
}

