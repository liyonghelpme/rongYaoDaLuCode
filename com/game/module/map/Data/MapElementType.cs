﻿namespace com.game.module.map.Data
{
    using System;

    public class MapElementType
    {
        public const int BOSS = 4;
        public const int DOOR = 6;
        public const int MONSTER = 3;
        public const int NPC = 5;
        public const int PLAYER = 1;
        public const int SELF_PLAYER = 2;
    }
}

