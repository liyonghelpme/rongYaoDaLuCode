﻿namespace com.game.module.map
{
    using com.game.data;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.utils;
    using com.game.vo;
    using System;
    using System.Collections.Generic;

    public class DungeonPlayerDriver : Singleton<DungeonPlayerDriver>
    {
        public void InitPos(List<MeVo> list)
        {
            this.ReposByRescue(list);
        }

        private bool ReposByRescue(List<MeVo> list)
        {
            int num = 0;
            float[] arrayStringToFloat = null;
            SysDungeonMissionVo curMissionSysVo = DungeonMgr.Instance.curMissionSysVo;
            if (curMissionSysVo != null)
            {
                arrayStringToFloat = StringUtils.GetArrayStringToFloat(curMissionSysVo.frozen_points);
                num = arrayStringToFloat.Length / 4;
            }
            if (num <= 0)
            {
                return false;
            }
            int num2 = 0;
            for (int i = 0; (i < list.Count) && (num2 < num); i++)
            {
                MeVo vo2 = list[i];
                if (vo2.Id != SelfPlayerManager.instance.onFightGeneralId)
                {
                    vo2.X = arrayStringToFloat[num2 * 4] * 0.001f;
                    vo2.Y = arrayStringToFloat[(num2 * 4) + 1] * 0.001f;
                    vo2.Z = arrayStringToFloat[(num2 * 4) + 2] * 0.001f;
                    vo2.isImprisoned = true;
                    num2++;
                }
            }
            return true;
        }
    }
}

