﻿namespace com.game.module.map
{
    using com.game.module.fight.arpg;
    using com.u3d.bases.controller;
    using com.u3d.bases.display.controler;
    using Pathfinding;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MapAiPath : AIPath
    {
        private BaseControler baseControler;
        private Vector3 lastPos;
        private float lastTime;
        private StatuControllerBase meStatuController;
        private MoveComplete moveComplete;
        private MoveEndCallback moveEndCallback;
        private float notMoveTime;
        private float totalMoveDistance;

        protected override void Awake()
        {
            base.seeker = base.GetComponent<Seeker>();
            base.tr = base.transform;
            base.rigid = base.rigidbody;
            this.meStatuController = base.GetComponent<StatuControllerBase>();
            base.controller = base.GetComponent<CharacterController>();
            base.canSearch = false;
            base.canMove = false;
        }

        private bool CheckMoveDistance()
        {
            this.totalMoveDistance += DamageCheck.GetDistance(base.transform.position, this.lastPos);
            this.notMoveTime += Time.deltaTime;
            if (this.notMoveTime > 0.9f)
            {
                if (this.totalMoveDistance < 0.3f)
                {
                    this.StopWalk();
                    this.ClearPathNotClearChaseTarget();
                    this.notMoveTime = 0f;
                    this.totalMoveDistance = 0f;
                    return true;
                }
                this.notMoveTime = 0f;
                this.totalMoveDistance = 0f;
            }
            return false;
        }

        public void ClearAiPath()
        {
            base.canMove = false;
            base.canSearch = false;
            this.moveEndCallback = null;
            MeControler baseControler = this.baseControler as MeControler;
            if (baseControler == null)
            {
            }
        }

        public void ClearPathNotClearChaseTarget()
        {
            base.canMove = false;
            base.canSearch = false;
            this.moveEndCallback = null;
        }

        public override Vector3 GetFeetPosition()
        {
            return (base.tr.position - ((Vector3) ((Vector3.up * base.controller.height) * 0.5f)));
        }

        protected override void OnPathComplete(Path _p)
        {
            base.OnPathComplete(_p);
            base.canSearch = false;
            base.canMove = true;
            this.meStatuController.SetStatu(1);
        }

        public void SearchPath(Vector3 toPosition, MoveEndCallback callback = null)
        {
            base.canSearch = true;
            base.seeker.StartPath(this.GetFeetPosition(), toPosition);
            this.moveEndCallback = callback;
        }

        public void SetController(ActionControler con)
        {
            this.baseControler = con;
        }

        public void StopWalk()
        {
            if ((this.meStatuController != null) && (this.meStatuController.CurrentStatu == 1))
            {
                this.meStatuController.SetStatu(0);
            }
            if (this.moveEndCallback != null)
            {
                this.moveEndCallback(this.baseControler);
                this.moveEndCallback = null;
            }
        }

        public void StopWalkAndAbortCallback()
        {
            if ((this.meStatuController != null) && (this.meStatuController.CurrentStatu == 1))
            {
                this.meStatuController.SetStatu(0);
            }
            this.moveEndCallback = null;
        }

        public override void Update()
        {
            if ((base.path != null) && base.canMove)
            {
                this.lastPos = base.transform.position;
                Vector3 speed = base.CalculateVelocity(this.GetFeetPosition());
                this.RotateTowards(base.targetDirection);
                base.controller.SimpleMove(speed);
                if ((Time.time - this.lastTime) > 0.5f)
                {
                    this.lastTime = Time.time;
                    MeControler.SendPos();
                }
                if (((speed.x == 0f) && (speed.y == 0f)) && (speed.z == 0f))
                {
                    base.canMove = false;
                    this.notMoveTime = 0f;
                    this.totalMoveDistance = 0f;
                    if (this.meStatuController != null)
                    {
                        this.meStatuController.SetStatu(0);
                    }
                    if (this.moveComplete != null)
                    {
                        this.moveComplete();
                    }
                    if (this.moveEndCallback != null)
                    {
                        this.moveEndCallback(this.baseControler);
                        this.moveEndCallback = null;
                    }
                }
                this.CheckMoveDistance();
            }
        }

        public void UpdateSearchPath(Vector3 toPosition, MoveEndCallback callback = null)
        {
            base.canSearch = true;
            base.canMove = true;
            this.moveEndCallback = callback;
            base.seeker.StartPath(this.GetFeetPosition(), toPosition);
        }
    }
}

