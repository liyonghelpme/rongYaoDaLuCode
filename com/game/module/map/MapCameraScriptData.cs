﻿namespace com.game.module.map
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MapCameraScriptData
    {
        public Vector3 actionPos;
        public Vector3 cameraDir;
        public cameraType cType;
        public float distance;
        public float fieldOfView;
        public bool isMonBorn;
        public float offetX;
        public float offetY;
        public float offetZ;
        public float time;

        public MapCameraScriptData()
        {
            this.offetY = 5f;
            this.offetZ = -4f;
            this.cameraDir = new Vector3(50f, 0f, 0f);
            this.time = 5f;
            this.fieldOfView = 60f;
        }

        public MapCameraScriptData(int cameraDataId)
        {
            this.offetY = 5f;
            this.offetZ = -4f;
            this.cameraDir = new Vector3(50f, 0f, 0f);
            this.time = 5f;
            this.fieldOfView = 60f;
            SysCameraInfoVo cameraInfoVo = BaseDataMgr.instance.GetCameraInfoVo((uint) cameraDataId);
            this.actionPos = StringUtils.StringToVector3(cameraInfoVo.check_pos);
            this.distance = cameraInfoVo.check_dist * 0.001f;
            this.cameraDir = StringUtils.StringToVector3(cameraInfoVo.cameraDir);
            Vector3 vector = StringUtils.StringToVector3(cameraInfoVo.offset_pos);
            this.offetX = vector.x;
            this.offetY = vector.y;
            this.offetZ = vector.z;
            this.cType = (cameraType) cameraInfoVo.type;
            this.time = cameraInfoVo.time * 0.001f;
            this.isMonBorn = true;
            this.fieldOfView = cameraInfoVo.view;
        }

        public static void setCameraByScriptId(int cameraDataId = 0x15f90)
        {
            MapCameraScriptData newCameraData = new MapCameraScriptData(cameraDataId);
            Camera.main.gameObject.AddMissingComponent<MouseOrbit>().SetTraget(AppMap.Instance.me.GoBase, newCameraData);
        }
    }
}

