﻿namespace com.game.module.map
{
    using com.game;
    using com.game.basic;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.effect;
    using com.game.module.fight.arpg;
    using com.game.module.Role;
    using com.game.Public.Message;
    using com.game.sound;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using com.net;
    using com.net.interfaces;
    using com.net.wifi_pvp;
    using com.u3d.bases.ai;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.map;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using UnityEngine;

    public class MapControlII : BaseControl<MapControlII>
    {
        private uint curMapId;
        public bool isTestFirst = true;
        private GameObject mainLight;

        public MapControlII()
        {
            AppMap.Instance.mapParserII.AddMapCallback(new com.u3d.bases.map.MapCallBack(this.MapCallBack));
        }

        private void AddBattleObject(INetData receiveData)
        {
            MapBattleObjectEnterMsg_4_21 g__ = new MapBattleObjectEnterMsg_4_21();
            g__.read(receiveData.GetMemoryStream());
            Singleton<MapMode>.Instance.AddBattleObject(g__.battleObjectList);
        }

        private void AddMonList(List<PMapMon> monList)
        {
            if (MapMode.WaitRefreshMonster)
            {
                foreach (PMapMon mon in monList)
                {
                    Singleton<MapMode>.Instance.waitMonList.Add(mon);
                }
            }
        }

        private void ChangeSceneComplete(uint mapId)
        {
            SysSceneVo currentSceneVo = Singleton<MapMode>.Instance.CurrentSceneVo;
            SysSceneResourceVo vo2 = BaseDataMgr.instance.getSceneResourceVo((uint) currentSceneVo.resource_id);
            if (this.mainLight == null)
            {
            }
            this.mainLight = GameObject.Find("main_light").gameObject;
            this.mainLight.SetActive(!vo2.scene_light);
            Singleton<UIController>.Instance.OnChangeScene();
            if (GlobalData.isInCopy)
            {
                DungeonMgr.Instance.Start();
            }
            PlayerMgr.instance.start();
            Singleton<MonsterMgr>.Instance.start();
        }

        private static void ClearLowHpWarning()
        {
            if (!AppMap.Instance.IsInMainCity && (AppMap.Instance.me != null))
            {
                Singleton<BattleCenterView>.Instance.updateHp(AppMap.Instance.me.GetMeVoByType<PlayerVo>());
            }
        }

        public void ClearMap()
        {
            this.ClearPrevMapObjects();
            GlobalAPI.GC();
        }

        private void ClearPrevMapObjects()
        {
            Singleton<MonsterActivator>.Instance.Clear();
            Singleton<ChestMgr>.Instance.Clear();
            this.removeMapPoint();
            AppMap.Instance.dispose();
            EffectMgr.Instance.ResetEffectWhileChangeScene();
        }

        private void ClientChangeMap(INetData data)
        {
            MapSwitchMsg_4_1 g__ = new MapSwitchMsg_4_1();
            g__.read(data.GetMemoryStream());
            if (g__.code == 0)
            {
                Singleton<MapMode>.Instance.changeScene_4_2();
            }
            else
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }

        private void CopyFinish(INetData data)
        {
            new MapFinishMsg_4_15().read(data.GetMemoryStream());
        }

        private void creatAstar()
        {
            if (Singleton<MapMode>.Instance.IsFirstInToScene)
            {
                new GameObject("aStarPath").AddComponent<AstarPath>();
            }
        }

        public void CreateMapPoint()
        {
        }

        private void CreateMe(bool isFirstIntoScene)
        {
            AppMap.Instance.ClearSelfDisplays();
            SelfPlayerManager.instance.clear();
            MeDisplay me = AppMap.Instance.me;
            SelfPlayerManager.instance.callback = new deCallBack(this.DoChangeSceneComplete);
            MeVo.instance.IsUnbeatable = false;
            MeVo.instance.X = MeVo.instance.toX;
            MeVo.instance.Y = MeVo.instance.toY;
            MeVo.instance.Z = MeVo.instance.toZ;
            if (!GlobalData.isInCopy)
            {
                SelfPlayerManager.instance.AddSelfPlayer(MeVo.instance);
            }
            else
            {
                SelfPlayerManager.instance.createVoByGeneralInfo(true);
            }
            SelfPlayerManager.instance.isInCorp = GlobalData.isInCopy;
            SelfPlayerManager.instance.start();
        }

        private void CreateMonster(INetData data)
        {
            MapMonEnterMsg_4_7 g__ = new MapMonEnterMsg_4_7();
            g__.read(data.GetMemoryStream());
            Singleton<MapMode>.Instance.AddMonInCurrentMap(g__.mons);
            this.AddMonList(g__.mons);
            if (AppMap.Instance.IsInWifiPVP)
            {
                Singleton<WifiPvpMode>.Instance.MyPrivate = g__.mons;
            }
        }

        private void DoChangeSceneComplete(BaseDisplay display)
        {
            Singleton<FreshmanGuideController>.Instance.InitFreshmanGuide();
            this.LoadMeCallBack(display);
            this.ChangeSceneComplete(MeVo.instance.mapId);
            GlobalAPI.facade.Notify(0x10, 0, 0, null);
            if (!AppMap.Instance.IsInArena)
            {
                PlayerAiController.InitPlayerAi();
            }
            AiTargetManager.instance.ClearAllDic();
            if (!GlobalData.isInCopy && !WifiLAN.Instance.IsInitialized)
            {
                WifiLAN.Instance.Start();
            }
            if (AppMap.Instance.IsInWifiPVP && WifiLAN.Instance.IsInitialized)
            {
            }
        }

        private void EnemySummonMonsterDie(INetData receiveData)
        {
            MapSummonMonsterDeadMsg_4_24 g__ = new MapSummonMonsterDeadMsg_4_24();
            g__.read(receiveData.GetMemoryStream());
            ActionDisplay actionDisplayByDisplayTypeAndId = SkillControl.GetActionDisplayByDisplayTypeAndId(400, g__.summorId);
            if (actionDisplayByDisplayTypeAndId != null)
            {
                MonsterVo meVoByType = actionDisplayByDisplayTypeAndId.GetMeVoByType<MonsterVo>();
                if (meVoByType.Camp != 0)
                {
                    meVoByType.ReduceHpToPercent((uint) 0);
                    CommandHandler.AddCommandStatic(actionDisplayByDisplayTypeAndId.GoBase, "death", 1f);
                }
            }
        }

        private void Fun_4_13(INetData data)
        {
            MapRoleDeadMsg_4_13 g__ = new MapRoleDeadMsg_4_13();
            g__.read(data.GetMemoryStream());
            PlayerDisplay player = AppMap.Instance.GetPlayer(g__.id.ToString(CultureInfo.InvariantCulture));
            if ((player != null) && (g__.id != MeVo.instance.Id))
            {
                player.Death();
            }
        }

        public string GetMapBackMusicName(uint mapId)
        {
            return StringUtils.GetNoSuffixString(BaseDataMgr.instance.GetMapVo(mapId).bgMusic);
        }

        private void InitCamera()
        {
            SysSceneVo sceneVo = BaseDataMgr.instance.GetSceneVo(MeVo.instance.mapId);
            Camera.main.gameObject.AddMissingComponent<MouseOrbit>().setTarget(AppMap.Instance.me.GoBase, true);
            Camera.main.gameObject.AddMissingComponent<CameraShake>();
            MapCameraScriptData.setCameraByScriptId(0x15f90);
        }

        private void initMePos()
        {
            MeDisplay me = AppMap.Instance.me;
            if (AppMap.Instance.mapParserII.MapVo.type != 1)
            {
                if (me.Controller.GoName != null)
                {
                    me.Controller.GoName.SetActive(false);
                }
            }
            else if (me.Controller.GoName != null)
            {
                me.Controller.GoName.SetActive(true);
            }
        }

        private void LoadMeCallBack(BaseDisplay display)
        {
            this.InitCamera();
        }

        private void MapCallBack(uint mapId)
        {
            MeVo.instance.mapId = mapId;
            this.curMapId = mapId;
            SoundMgr.Instance.StopAll();
            Singleton<MapMode>.Instance.isSceneComplete = true;
            this.CreateMe(Singleton<MapMode>.Instance.IsFirstInToScene);
        }

        private void MapInfo(INetData data)
        {
            Singleton<MapMode>.Instance.isSceneComplete = false;
            MapSightMsg_4_2 g__ = new MapSightMsg_4_2();
            g__.read(data.GetMemoryStream());
            this.ClearMap();
            Singleton<MapMode>.Instance.pathType = 5;
            Singleton<DungeonChapterView>.Instance.CloseView();
            if (Singleton<MapMode>.Instance.IsFirstInToScene)
            {
                AppFacde.instance.InitAfterIntoScene();
                EffectMgr.Instance.LoadShadowEffect();
            }
            else
            {
                Singleton<ComboHitView>.Instance.CloseView();
            }
            uint mapId = g__.mapId;
            SysSceneVo sceneVo = BaseDataMgr.instance.GetSceneVo(mapId);
            Singleton<MapMode>.Instance.CurrentSceneVo = sceneVo;
            MapSightMsg_4_2 g__2 = g__;
            GlobalAPI.facade.Notify(15, 0, 0, g__2);
            this.creatAstar();
            this.startChangeMap(mapId, (float) g__.x, (float) g__.y, (float) g__.z, 0f);
            Singleton<MapMode>.Instance.AddMonInMap(g__.monsEnter, mapId);
            Singleton<MapMode>.Instance.AddPlayerInMap(g__.rolesEnter);
            DungeonMgr.Instance.Config();
            ClearLowHpWarning();
        }

        private void MonsterDead(INetData data)
        {
            new MapMonDeadMsg_4_12().read(data.GetMemoryStream());
        }

        private void moveComplete()
        {
            switch (Singleton<MapMode>.Instance.pathType)
            {
                case 1:
                    Singleton<DungeonChapterView>.Instance.OpenView();
                    break;
            }
            Singleton<MapMode>.Instance.pathType = 5;
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("1025", new NetMsgCallback(this.ClientChangeMap));
            AppNet.main.addCMD("1026", new NetMsgCallback(this.MapInfo));
            AppNet.main.addCMD("1028", new NetMsgCallback(this.PlayerInSight));
            AppNet.main.addCMD("1029", new NetMsgCallback(this.PlayerOutSight));
            AppNet.main.addCMD("1039", new NetMsgCallback(this.CopyFinish));
            AppNet.main.addCMD("1041", new NetMsgCallback(this.ServerChangeMap));
            AppNet.main.addCMD("1031", new NetMsgCallback(this.CreateMonster));
            AppNet.main.addCMD("1036", new NetMsgCallback(this.MonsterDead));
            AppNet.main.addCMD("1043", new NetMsgCallback(this.UpdateAttackEnergy));
            AppNet.main.addCMD("1037", new NetMsgCallback(this.Fun_4_13));
            AppNet.main.addCMD("1045", new NetMsgCallback(this.AddBattleObject));
            AppNet.main.addCMD("1046", new NetMsgCallback(this.OnPlayerFightStateChange));
            AppNet.main.addCMD("1047", new NetMsgCallback(this.Summon));
            AppNet.main.addCMD("1048", new NetMsgCallback(this.EnemySummonMonsterDie));
        }

        private void OnPlayerFightStateChange(INetData data)
        {
            MapFightStateMsg_4_22 g__ = new MapFightStateMsg_4_22();
            g__.read(data.GetMemoryStream());
            PlayerVo vo = PlayerMgr.instance.getPlayer((uint) g__.roleId);
            if (vo != null)
            {
                vo.isInWar = g__.state > 0;
                GlobalAPI.facade.Notify(0x15, (int) g__.roleId, 0, null);
            }
        }

        private void PlayerInSight(INetData data)
        {
            if (Singleton<MapMode>.Instance.isSceneComplete)
            {
                MapRoleEnterMsg_4_4 g__ = new MapRoleEnterMsg_4_4();
                g__.read(data.GetMemoryStream());
                Singleton<MapMode>.Instance.AddPlayerInMap(g__.roles);
            }
        }

        private void PlayerOutSight(INetData data)
        {
            MapRoleLeaveMsg_4_5 g__ = new MapRoleLeaveMsg_4_5();
            g__.read(data.GetMemoryStream());
            foreach (ulong num in g__.roles)
            {
                PlayerMgr.instance.removePlayer(num);
            }
        }

        public void removeMapPoint()
        {
            if (EffectMgr.Instance.GetMainEffectGameObject("20001", string.Empty) != null)
            {
                EffectMgr.Instance.GetMainEffectGameObject("20001", string.Empty).SetActive(false);
            }
            Singleton<DungeonMode>.Instance.isUpdate = false;
        }

        private void ServerChangeMap(INetData data)
        {
            new MapEnterReqMsg_4_17().read(data.GetMemoryStream());
        }

        public void startChangeMap(uint mapId, float x, float y, float z, float rotation)
        {
            MeVo.instance.toX = x * 0.001f;
            MeVo.instance.toY = y * 0.001f;
            MeVo.instance.toZ = z * 0.001f;
            MeVo.instance.rotateY = rotation;
            MeVo.instance.preMapId = AppMap.Instance.mapParserII.MapId;
            MeVo.instance.mapId = mapId;
            AppMap.Instance.mapParserII.StartLoadMap(mapId);
            Singleton<MapMode>.Instance.ReSetCameraType();
        }

        private void Summon(INetData receiveData)
        {
            MapMonBornSightMsg_4_23 g__ = new MapMonBornSightMsg_4_23();
            g__.read(receiveData.GetMemoryStream());
            ActionDisplay actionDisplayByDisplayTypeAndId = SkillControl.GetActionDisplayByDisplayTypeAndId(DamageCheck.GetDisplayType(g__.syncType), g__.summorId);
            if (actionDisplayByDisplayTypeAndId != null)
            {
                Singleton<MapMode>.Instance.AddSummonMonInMap(g__.monList, actionDisplayByDisplayTypeAndId.GetMeVoByType<BaseRoleVo>().Camp);
                MapMonBornSightMsg_4_23 data = g__;
                GlobalAPI.facade.Notify(0x16, 0, 0, data);
            }
        }

        public void updateArea(Vector3 point)
        {
            if (Math.Abs(Vector3.Distance(point, AppMap.Instance.me.GoBase.transform.position)) < 3f)
            {
                this.moveComplete();
            }
        }

        private void UpdateAttackEnergy(INetData receiveData)
        {
        }
    }
}

