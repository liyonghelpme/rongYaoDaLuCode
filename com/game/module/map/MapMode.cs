﻿namespace com.game.module.map
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.Role;
    using com.game.module.SystemData;
    using com.game.module.WiFiPvP;
    using com.game.vo;
    using com.liyong;
    using com.net.wifi_pvp;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.vo;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class MapMode : BaseMode<MapMode>
    {
        private uint _cameraTypeCity;
        private uint _cameraTypeDungeon;
        private NGUIJoystick _joystick;
        [CompilerGenerated]
        private static Func<PGeneralFightAttr, PNewAttr> <>f__am$cache15;
        public static ushort CUR_MAP_PHASE = 1;
        private Dictionary<cameraUserType, int> dicCamerType = new Dictionary<cameraUserType, int>();
        public static int EndTimestamp;
        public const ushort EVENT_CODE_STOP_LEFTTIME = 3;
        public const ushort EVENT_CODE_UPDATE_LEFTTIME = 2;
        public static uint expire;
        public const ushort INIT_POSX_IN_MAIN_CITY = 5;
        public const float INIT_POSY_IN_MAIN_CITY = 1.8f;
        public bool IsFirstInToScene = true;
        public bool isSceneComplete;
        public const ushort MAX_MONSTER_POSY = 0x15;
        public const ushort MIN_MONSTER_POSY = 0x12;
        public bool NeedGuideMainTask;
        public static ushort NEXT_MAP_PHASE = 1;
        public const int PATH_AI = 5;
        public const int PATH_CLUB = 3;
        public const int PATH_COPY = 1;
        public const int PATH_FIGHT = 2;
        public const int PATH_TRADE = 4;
        public int pathType;
        public const float POS_OFFSET = 2f;
        public List<PMapMon> waitMonList = new List<PMapMon>();

        static MapMode()
        {
            AutoChangeMap = false;
            CanGoToNextPhase = false;
            WaitRefreshMonster = false;
            StartAutoMove = false;
            DisableInput = false;
        }

        public void AddBattleObject(List<PBattleObject> list)
        {
            <AddBattleObject>c__AnonStoreyFE yfe = new <AddBattleObject>c__AnonStoreyFE();
            UnityLog.Log.AI(null, "AddBattleObject From Server " + list.Count<PBattleObject>());
            yfe.mapId = MeVo.instance.mapId;
            IEnumerator<BattleObjectVo> enumerator = list.Select<PBattleObject, BattleObjectVo>(new Func<PBattleObject, BattleObjectVo>(yfe.<>m__FB)).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    BattleObjectVo current = enumerator.Current;
                    UnityLog.Log.AI(null, "SysBattleObjectVo " + current.SysBattleObjectVo);
                    if (current.SysBattleObjectVo == null)
                    {
                        Debug.LogError("模板表为空啦大哥");
                    }
                    else
                    {
                        bool flag = BattleObjectMgr.Instance.GetBattleObject(current) == null;
                        UnityLog.Log.Net(string.Concat(new object[] { "isNew BattleObject ", flag, " inWifi ", AppMap.Instance.IsInWifiPVP }));
                        if (AppMap.Instance.IsInWifiPVP)
                        {
                            BattleObjectMgr.Instance.Add(current);
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            WifiPvpManager.Instance.SyncBattleObjectToOthers(list);
        }

        public void AddBossInWaitList(MonsterVo monVo)
        {
        }

        public void AddCameraType()
        {
            if (GlobalData.isInCopy)
            {
                this._cameraTypeDungeon++;
            }
            else
            {
                this._cameraTypeCity++;
            }
        }

        public void AddMonInCurrentMap(List<PMapMon> monList)
        {
            uint mapId = MeVo.instance.mapId;
            foreach (PMapMon mon in monList)
            {
                bool flag = false;
                SysMonsterVo monsterVO = null;
                MonsterVo monster = null;
                monster = Singleton<MonsterMgr>.Instance.GetMonster(mapId.ToString(), mon.id.ToString());
                if (monster == null)
                {
                    flag = true;
                    monster = new MonsterVo();
                }
                monster.mapId = mapId;
                monster.Id = mon.id;
                monster.monsterId = mon.monid;
                monster.bornType = mon.born;
                monster.Hp = mon.hpFull;
                monster.SetHp(mon.hp);
                monster.groupIndex = mon.groupId;
                monster.rotateY = mon.rotateY;
                monster.X = mon.x * 0.001f;
                monster.Y = mon.y * 0.001f;
                monster.Z = mon.z * 0.001f;
                monster.parent_id = mon.parentMonId;
                monster.parent_template_id = mon.parentMonTemplateId;
                monster.reborn_count = mon.rebornCount;
                monster.max_alive_time = mon.maxAliveTime;
                if (AppMap.Instance.IsInWifiPVP)
                {
                    monster.Camp = 0;
                }
                else
                {
                    monster.Camp = 100;
                }
                monsterVO = monster.MonsterVO;
                if (monsterVO == null)
                {
                    Debug.LogError("monsterVo为空啦大哥");
                }
                else
                {
                    MonsterMgr.CalcMonsterAttr(monster, monsterVO.mon_props);
                    monster.Name = monsterVO.name;
                    monster.HurtResist = (uint) monsterVO.hurt_resist;
                    monster.HurtDownResist = (uint) monsterVO.hurt_down_resist;
                    monster.MoveRatio = monsterVO.move_ratio * 0.001f;
                    monster.Level = monsterVO.lvl;
                    if (flag)
                    {
                        Singleton<MonsterMgr>.Instance.addMonster(mapId.ToString(), monster);
                    }
                }
            }
        }

        public void AddMonInCurrentPVPMap(List<PMapMon> monList)
        {
            uint mapId = MeVo.instance.mapId;
            foreach (PMapMon mon in monList)
            {
                <AddMonInCurrentPVPMap>c__AnonStoreyFD yfd = new <AddMonInCurrentPVPMap>c__AnonStoreyFD();
                bool flag = false;
                yfd.enemyVo = Singleton<MonsterMgr>.Instance.GetMonster(mapId.ToString(), mon.id.ToString());
                if (yfd.enemyVo == null)
                {
                    flag = true;
                    yfd.enemyVo = new MonsterVo();
                }
                yfd.enemyVo.mapId = mapId;
                yfd.enemyVo.Id = mon.id;
                yfd.enemyVo.monsterId = mon.monid;
                yfd.enemyVo.bornType = mon.born;
                yfd.enemyVo.Hp = mon.hpFull;
                yfd.enemyVo.SetHp(mon.hp);
                yfd.enemyVo.Camp = 1;
                yfd.enemyVo.groupIndex = mon.groupId;
                yfd.enemyVo.rotateY = mon.rotateY;
                yfd.enemyVo.X = mon.x * 0.001f;
                yfd.enemyVo.Y = mon.y * 0.001f;
                yfd.enemyVo.Z = mon.z * 0.001f;
                yfd.enemyVo.parent_id = mon.parentMonId;
                yfd.enemyVo.parent_template_id = mon.parentMonTemplateId;
                yfd.enemyVo.reborn_count = mon.rebornCount;
                yfd.enemyVo.max_alive_time = mon.maxAliveTime;
                SysMonsterVo monsterVO = yfd.enemyVo.MonsterVO;
                if (monsterVO == null)
                {
                    Debug.LogError("monsterVo为空啦大哥");
                }
                else
                {
                    MonsterMgr.CalcMonsterAttr(yfd.enemyVo, monsterVO.mon_props);
                    yfd.enemyVo.Name = monsterVO.name;
                    yfd.enemyVo.HurtResist = (uint) monsterVO.hurt_resist;
                    yfd.enemyVo.HurtDownResist = (uint) monsterVO.hurt_down_resist;
                    yfd.enemyVo.MoveRatio = monsterVO.move_ratio * 0.001f;
                    yfd.enemyVo.Level = monsterVO.lvl;
                    if (AppMap.Instance.IsInWifiPVP)
                    {
                        yfd.enemyVo.ModelLoadCallBack = new Action<BaseDisplay>(yfd.<>m__FA);
                    }
                    if (flag)
                    {
                        Singleton<MonsterMgr>.Instance.addMonster(mapId.ToString(), yfd.enemyVo);
                    }
                }
            }
        }

        public void AddMonInMap(List<PMapMon> monList, uint mapId)
        {
            foreach (PMapMon mon in monList)
            {
                <AddMonInMap>c__AnonStoreyFB yfb = new <AddMonInMap>c__AnonStoreyFB();
                bool flag = false;
                yfb.enemyVo = Singleton<MonsterMgr>.Instance.GetMonster(mapId.ToString(), mon.id.ToString());
                if (yfb.enemyVo == null)
                {
                    flag = true;
                    yfb.enemyVo = new MonsterVo();
                }
                yfb.enemyVo.mapId = mapId;
                yfb.enemyVo.Id = mon.id;
                yfb.enemyVo.monsterId = mon.monid;
                yfb.enemyVo.bornType = mon.born;
                yfb.enemyVo.Level = mon.lvl;
                yfb.enemyVo.Hp = mon.hpFull;
                yfb.enemyVo.SetHp(mon.hp);
                yfb.enemyVo.X = mon.x * 0.001f;
                yfb.enemyVo.Y = mon.y * 0.001f;
                yfb.enemyVo.Z = mon.z * 0.001f;
                yfb.enemyVo.rotateY = mon.rotateY;
                yfb.enemyVo.parent_id = mon.parentMonId;
                yfb.enemyVo.parent_template_id = mon.parentMonTemplateId;
                yfb.enemyVo.reborn_count = mon.rebornCount;
                yfb.enemyVo.max_alive_time = mon.maxAliveTime;
                yfb.enemyVo.groupIndex = mon.groupId;
                SysMonsterVo monsterVO = yfb.enemyVo.MonsterVO;
                if (monsterVO == null)
                {
                    com.u3d.bases.debug.Log.info(this, "数据库没这个怪物的信息");
                }
                else
                {
                    yfb.enemyVo.Name = monsterVO.name;
                    MonsterMgr.CalcMonsterAttr(yfb.enemyVo, monsterVO.mon_props);
                    yfb.enemyVo.HurtResist = (uint) monsterVO.hurt_resist;
                    yfb.enemyVo.HurtDownResist = (uint) monsterVO.hurt_down_resist;
                    yfb.enemyVo.MoveRatio = monsterVO.move_ratio * 0.001f;
                    yfb.enemyVo.Level = monsterVO.lvl;
                    if ((monsterVO.type == 6) || (monsterVO.type == 2))
                    {
                        Debug.Log(string.Concat(new object[] { "怪物模板ID:", monsterVO.id, ",类型：", monsterVO.type, ",坐标:[", mon.x, ",", mon.y, ",", mon.z, "]" }));
                    }
                    yfb.enemyVo.ModelLoadCallBack = (Action<BaseDisplay>) Delegate.Combine(yfb.enemyVo.ModelLoadCallBack, new Action<BaseDisplay>(yfb.<>m__F9));
                    if (AppMap.Instance.IsInWifiPVP)
                    {
                        yfb.enemyVo.Camp = 0;
                    }
                    else
                    {
                        yfb.enemyVo.Camp = 100;
                    }
                    if (flag)
                    {
                        Singleton<MonsterActivator>.Instance.AddMonster(yfb.enemyVo);
                    }
                }
            }
        }

        public void AddPlayerInCurrentArenaMap(IList<PGeneralFightAttr> attrList, List<PCoordinator> coorList, bool isMySide)
        {
            if (<>f__am$cache15 == null)
            {
                <>f__am$cache15 = x => x.attr;
            }
            List<PNewAttr> list = attrList.Select<PGeneralFightAttr, PNewAttr>(<>f__am$cache15).ToList<PNewAttr>();
            uint mapId = MeVo.instance.mapId;
            uint num2 = !isMySide ? 1 : 0;
            foreach (PNewAttr attr in list)
            {
                <AddPlayerInCurrentArenaMap>c__AnonStoreyF9 yf = new <AddPlayerInCurrentArenaMap>c__AnonStoreyF9();
                PlayerVo vo = new PlayerVo {
                    mapId = mapId,
                    Id = attr.id
                };
                yf.@in = attr;
                IEnumerator<PCoordinator> enumerator = coorList.Where<PCoordinator>(new Func<PCoordinator, bool>(yf.<>m__F8)).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        PCoordinator current = enumerator.Current;
                        vo.Level = current.lv;
                        vo.X = current.x * 0.001f;
                        vo.Y = current.y * 0.001f;
                        vo.Z = current.z * 0.001f;
                        vo.Camp = num2;
                        vo.rotateY = current.rotateY;
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                vo.Name = "player";
                PStylebin stylebin = new PStylebin {
                    id = (int) attr.generalId,
                    quality = attr.quality
                };
                vo.stylebin = stylebin;
                VoUtils.SetVoAllAttribute(vo, attr, null);
                PlayerMgr.instance.addPlayer(vo);
            }
        }

        public void AddPlayerInCurrentPVPMap(List<PNewAttr> generalList, List<PCoordinator> coorList, List<PNewAttr> equipAttrs)
        {
            uint mapId = MeVo.instance.mapId;
            foreach (PNewAttr attr in generalList)
            {
                <AddPlayerInCurrentPVPMap>c__AnonStoreyF8 yf = new <AddPlayerInCurrentPVPMap>c__AnonStoreyF8();
                PNewAttr equipAttr = ObjectManager.GetEquipAttr(attr.id, equipAttrs);
                PlayerVo vo = new PlayerVo {
                    mapId = mapId,
                    Id = attr.id
                };
                yf.@in = attr;
                IEnumerator<PCoordinator> enumerator = coorList.Where<PCoordinator>(new Func<PCoordinator, bool>(yf.<>m__F6)).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        PCoordinator current = enumerator.Current;
                        vo.Level = current.lv;
                        vo.X = current.x * 0.001f;
                        vo.Y = current.y * 0.001f;
                        vo.Z = current.z * 0.001f;
                        vo.Camp = 1;
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                vo.Name = "player";
                PStylebin stylebin = new PStylebin {
                    id = (int) attr.generalId,
                    quality = attr.quality
                };
                vo.stylebin = stylebin;
                VoUtils.SetVoAllAttribute(vo, attr, equipAttr);
                PlayerMgr.instance.addPlayer(vo);
            }
        }

        public void AddPlayerInMap(List<PMapRole> rolesList)
        {
            uint mapId = MeVo.instance.mapId;
            foreach (PMapRole role in rolesList)
            {
                PlayerVo playerVo = new PlayerVo {
                    mapId = mapId,
                    Id = role.id,
                    Name = role.name,
                    Level = role.level,
                    Hp = 1,
                    job = 1,
                    stylebin = role.styleBin,
                    X = role.x * 0.001f,
                    Y = role.y * 0.001f,
                    Z = role.z * 0.001f,
                    sex = role.sex,
                    PetId = role.petId,
                    isInWar = role.fightState > 0
                };
                playerVo.SetHp(1);
                if (playerVo.sex < 1)
                {
                    playerVo.sex = 1;
                }
                PlayerMgr.instance.addPlayer(playerVo);
            }
        }

        public void AddSummonMonInMap(IEnumerable<PMapMon> monList, uint camp)
        {
            uint mapId = MeVo.instance.mapId;
            IEnumerator<PMapMon> enumerator = monList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PMapMon current = enumerator.Current;
                    bool flag = false;
                    MonsterVo monster = Singleton<MonsterMgr>.Instance.GetMonster(mapId.ToString(), current.id.ToString());
                    if (monster == null)
                    {
                        flag = true;
                        monster = new MonsterVo();
                    }
                    monster.mapId = mapId;
                    monster.Id = current.id;
                    monster.monsterId = current.monid;
                    monster.bornType = current.born;
                    monster.Level = current.lvl;
                    monster.Hp = current.hpFull;
                    monster.SetHp(current.hp);
                    monster.X = current.x * 0.001f;
                    monster.Y = current.y * 0.001f;
                    monster.Z = current.z * 0.001f;
                    monster.rotateY = current.rotateY;
                    monster.parent_id = current.parentMonId;
                    monster.parent_template_id = current.parentMonTemplateId;
                    monster.reborn_count = current.rebornCount;
                    monster.max_alive_time = current.maxAliveTime;
                    monster.groupIndex = current.groupId;
                    SysMonsterVo monsterVO = monster.MonsterVO;
                    if (monsterVO == null)
                    {
                        com.u3d.bases.debug.Log.info(this, "数据库没这个怪物的信息");
                    }
                    else
                    {
                        monster.Name = monsterVO.name;
                        MonsterMgr.CalcMonsterAttr(monster, monsterVO.mon_props);
                        monster.HurtResist = (uint) monsterVO.hurt_resist;
                        monster.HurtDownResist = (uint) monsterVO.hurt_down_resist;
                        monster.MoveRatio = monsterVO.move_ratio * 0.001f;
                        monster.Level = monsterVO.lvl;
                        monster.Camp = camp;
                        monster.MonsterVO.type = 6;
                        if (flag)
                        {
                            Singleton<MonsterMgr>.Instance.addMonster(mapId.ToString(), monster);
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void changeScene(uint mapId)
        {
            MemoryStream msdata = new MemoryStream();
            Module_4.write_4_1(msdata, mapId, 0, 0);
            AppNet.gameNet.send(msdata, 4, 1);
        }

        public void changeScene_4_2()
        {
            Singleton<StartLoadingView>.Instance.OpenView();
            AppNet.gameNet.send(new MemoryStream(), 4, 2);
        }

        public uint GetCameraTypeCount()
        {
            if (GlobalData.isInCopy)
            {
                return this.CameraTypeDungeon;
            }
            return this.CameraTypeCity;
        }

        public void InitMapCameraData()
        {
            this.dicCamerType.Clear();
            this.dicCamerType.Add(cameraUserType.normal, 0x15f90);
            this.dicCamerType.Add(cameraUserType.focus, 0x15f91);
            this.dicCamerType.Add(cameraUserType.focus_near, 0x15f92);
        }

        public void MeDeath(uint id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_4.write_4_13(msdata, (ulong) id);
            AppNet.gameNet.send(msdata, 4, 13);
        }

        private MonsterVo MonsterAdapt(MonsterVo enemyVo)
        {
            if (BaseDataMgr.instance.GetMapVo(MeVo.instance.mapId).adapt)
            {
                enemyVo.Level = MeVo.instance.Level;
            }
            return enemyVo;
        }

        public void MonsterDeath(ulong id)
        {
            MemoryStream msdata = new MemoryStream();
            PlayerVo meVoByType = AppMap.Instance.me.GetMeVoByType<PlayerVo>();
            Module_4.write_4_12(msdata, id, 0, meVoByType.CurHp, 0);
            AppNet.gameNet.send(msdata, 4, 12);
        }

        public void MySideSummonMonsterDie(ulong id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_4.write_4_24(msdata, id);
            if (AppMap.Instance.IsInWifiPVP)
            {
                WifiLAN.Instance.Send(msdata, 4, 0x18);
            }
            else
            {
                AppNet.gameNet.send(msdata, 4, 0x18);
            }
        }

        public void RequestMonReborn(uint rebornCount, ulong parent_id, int parent_tid, int mon_tid, int x, int y, int z)
        {
            MemoryStream msdata = new MemoryStream();
            Module_4.write_4_20(msdata, (byte) rebornCount, parent_id, parent_tid, mon_tid, x, y, z);
            AppNet.gameNet.send(msdata, 4, 20);
        }

        public void ReSetCameraType()
        {
            this._cameraTypeCity = 0;
            this._cameraTypeDungeon = 0;
        }

        public void RoleDeath(uint id)
        {
            MemoryStream msdata = new MemoryStream();
            Module_4.write_4_13(msdata, (ulong) id);
            AppNet.gameNet.send(msdata, 4, 13);
        }

        public void RoleMove(ushort x, ushort y, ushort moveStatus)
        {
            MemoryStream msdata = new MemoryStream();
            Module_4.write_4_6(msdata, (short) x, (short) y, moveStatus);
            AppNet.gameNet.send(msdata, 4, 6);
        }

        public void ShowMapCameraEffect()
        {
            cameraUserType cameraTypeCount = (cameraUserType) this.GetCameraTypeCount();
            if (this.dicCamerType.ContainsKey(cameraTypeCount))
            {
                MapCameraScriptData.setCameraByScriptId(this.dicCamerType[cameraTypeCount]);
            }
        }

        public void StopLeftTime()
        {
            base.DataUpdate(3);
        }

        public void Summon(ulong id, SkillSynType type, List<PMapMon> list)
        {
            MemoryStream msdata = new MemoryStream();
            Module_4.write_4_23(msdata, id, (byte) type, list);
            AppNet.gameNet.send(msdata, 4, 0x17);
        }

        public void SyncAttackEnergy()
        {
            MemoryStream msdata = new MemoryStream();
            uint attackEnergy = (uint) AppMap.Instance.me.Controller.ComboHitMgr.AttackEnergy;
            uint fixTime = (uint) AppMap.Instance.me.Controller.ComboHitMgr.FixTime;
            Module_4.write_4_19(msdata, attackEnergy, fixTime);
            AppNet.gameNet.send(msdata, 4, 0x13);
        }

        public void UpdateExpire(uint leftTime)
        {
            expire = leftTime;
            EndTimestamp = ((int) expire) + ServerTime.Instance.Timestamp;
            base.DataUpdate(2);
        }

        public static bool AutoChangeMap
        {
            [CompilerGenerated]
            get
            {
                return <AutoChangeMap>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <AutoChangeMap>k__BackingField = value;
            }
        }

        private uint CameraTypeCity
        {
            get
            {
                return ((this._cameraTypeCity % 3) + 1);
            }
        }

        private uint CameraTypeDungeon
        {
            get
            {
                return ((this._cameraTypeDungeon % 3) + 1);
            }
        }

        public static bool CanGoToNextPhase
        {
            [CompilerGenerated]
            get
            {
                return <CanGoToNextPhase>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <CanGoToNextPhase>k__BackingField = value;
            }
        }

        public SysSceneVo CurrentSceneVo { get; set; }

        public static bool DisableInput
        {
            [CompilerGenerated]
            get
            {
                return <DisableInput>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <DisableInput>k__BackingField = value;
            }
        }

        public static bool InStageEndStory
        {
            [CompilerGenerated]
            get
            {
                return <InStageEndStory>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <InStageEndStory>k__BackingField = value;
            }
        }

        public static bool InStory
        {
            [CompilerGenerated]
            get
            {
                return <InStory>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <InStory>k__BackingField = value;
            }
        }

        public NGUIJoystick joystick
        {
            get
            {
                return this._joystick;
            }
            set
            {
                this._joystick = value;
            }
        }

        public static bool StartAutoMove
        {
            [CompilerGenerated]
            get
            {
                return <StartAutoMove>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <StartAutoMove>k__BackingField = value;
            }
        }

        public static bool WaitRefreshMonster
        {
            [CompilerGenerated]
            get
            {
                return <WaitRefreshMonster>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <WaitRefreshMonster>k__BackingField = value;
            }
        }

        [CompilerGenerated]
        private sealed class <AddBattleObject>c__AnonStoreyFE
        {
            internal uint mapId;

            internal BattleObjectVo <>m__FB(PBattleObject obj)
            {
                return new BattleObjectVo { mapId = this.mapId, Id = obj.id, template_id = obj.templateId, X = obj.x * 0.001f, Y = obj.y * 0.001f, Z = obj.z * 0.001f, rotateY = obj.dir, SysBattleObjectVo = BaseDataMgr.instance.GetDataByTypeAndId<SysBattleObjectVo>("SysBattleObjectVo", obj.templateId) };
            }
        }

        [CompilerGenerated]
        private sealed class <AddMonInCurrentPVPMap>c__AnonStoreyFD
        {
            internal MonsterVo enemyVo;

            internal void <>m__FA(BaseDisplay bd)
            {
                <AddMonInCurrentPVPMap>c__AnonStoreyFC yfc = new <AddMonInCurrentPVPMap>c__AnonStoreyFC {
                    <>f__ref$253 = this,
                    bd = bd
                };
                vp_Timer.In(0.5f, new vp_Timer.Callback(yfc.<>m__FD), null);
            }

            private sealed class <AddMonInCurrentPVPMap>c__AnonStoreyFC
            {
                internal MapMode.<AddMonInCurrentPVPMap>c__AnonStoreyFD <>f__ref$253;
                internal BaseDisplay bd;

                internal void <>m__FD()
                {
                    DamageCheck.DoAddEffectInPvp(this.bd as ActionDisplay, DamageCheck.AddEffectInPvp(this.<>f__ref$253.enemyVo));
                    switch (this.bd.GetMeVoByType<MonsterVo>().MonsterVO.type)
                    {
                        case 3:
                            WifiPvpManager.Instance.AddEnemyTower(this.bd as MonsterDisplay);
                            break;

                        case 5:
                            WifiPvpManager.Instance.AddEnemyBase(this.bd as MonsterDisplay);
                            break;
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <AddMonInMap>c__AnonStoreyFB
        {
            internal MonsterVo enemyVo;

            internal void <>m__F9(BaseDisplay bd)
            {
                <AddMonInMap>c__AnonStoreyFA yfa = new <AddMonInMap>c__AnonStoreyFA {
                    <>f__ref$251 = this,
                    bd = bd
                };
                vp_Timer.In(0.5f, new vp_Timer.Callback(yfa.<>m__FC), null);
            }

            private sealed class <AddMonInMap>c__AnonStoreyFA
            {
                internal MapMode.<AddMonInMap>c__AnonStoreyFB <>f__ref$251;
                internal BaseDisplay bd;

                internal void <>m__FC()
                {
                    DamageCheck.DoAddEffectInPvp(this.bd as ActionDisplay, DamageCheck.AddEffectInPvp(this.<>f__ref$251.enemyVo));
                }
            }
        }

        [CompilerGenerated]
        private sealed class <AddPlayerInCurrentArenaMap>c__AnonStoreyF9
        {
            internal PNewAttr @in;

            internal bool <>m__F8(PCoordinator item)
            {
                return (item.id == this.@in.id);
            }
        }

        [CompilerGenerated]
        private sealed class <AddPlayerInCurrentPVPMap>c__AnonStoreyF8
        {
            internal PNewAttr @in;

            internal bool <>m__F6(PCoordinator item)
            {
                return (item.id == this.@in.id);
            }
        }

        private enum cameraUserType
        {
            focus = 2,
            focus_near = 3,
            normal = 1
        }
    }
}

