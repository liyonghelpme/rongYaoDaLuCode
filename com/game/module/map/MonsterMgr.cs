﻿namespace com.game.module.map
{
    using com.game;
    using com.game.basic;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.effect;
    using com.game.module.hud;
    using com.game.module.map.BossBorn;
    using com.game.module.WiFiPvP;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class MonsterMgr : BaseMode<MonsterMgr>
    {
        private float _beginTime;
        private MonsterVo _bossVo;
        private IDictionary<string, IList<MonsterVo>> _bossWaitList = new Dictionary<string, IList<MonsterVo>>();
        private string _curMonsterUrl;
        private bool _isRuning;
        private IDictionary<string, GameObject> _monsterPreloadList = new Dictionary<string, GameObject>();
        private IDictionary<string, IList<MonsterVo>> _monsterVoList = new Dictionary<string, IList<MonsterVo>>();
        private int _preloadIndex;
        [CompilerGenerated]
        private static Action<BaseDisplay> <>f__am$cache13;
        private const float AiStartDelayTime = 2.5f;
        public BossBornManager bossBornManager = new BossBornManager();
        public static bool CanSetAi = true;
        private IDictionary<ulong, Vector3> deadMonsterPoints = new Dictionary<ulong, Vector3>();
        private float delay = 1f;
        private string effectUrl;
        public const int EventBossUpdate = 1;
        public GameObject FootEffect;
        public int LiveMonsterNumber;
        public readonly float MONSTER_POSY = 1.5f;
        public MoveToCallBack moveMoveToCallBack;
        private Dictionary<uint, RebornCheck> rebornCheckList = new Dictionary<uint, RebornCheck>();

        public MonsterMgr()
        {
            this.bossBornManager.SetUp();
        }

        public void AddBossInWaitList(string mapId, MonsterVo vo)
        {
            if (((vo != null) && !StringUtils.isEmpty(mapId)) && (vo.MonsterVO.quality == 3))
            {
                IList<MonsterVo> list = null;
                if (this._bossWaitList.ContainsKey(mapId))
                {
                    list = this._bossWaitList[mapId];
                }
                else
                {
                    list = new List<MonsterVo>();
                    this._bossWaitList.Add(mapId, list);
                }
                list.Add(vo);
                if (vo.MonsterVO.quality == 3)
                {
                    this.BossVo = vo;
                }
            }
        }

        public void addMonster(string mapId, MonsterVo vo)
        {
            if ((vo != null) && !StringUtils.isEmpty(mapId))
            {
                IList<MonsterVo> list = null;
                if (this._monsterVoList.ContainsKey(mapId))
                {
                    list = this._monsterVoList[mapId];
                }
                else
                {
                    list = new List<MonsterVo>();
                    this._monsterVoList.Add(mapId, list);
                }
                list.Add(vo);
                if (vo.MonsterVO.quality == 3)
                {
                    this.BossVo = vo;
                }
                this.LiveMonsterNumber++;
            }
        }

        [DebuggerHidden]
        private IEnumerator AddMonsterAdditionalView(BaseDisplay display)
        {
            return new <AddMonsterAdditionalView>c__Iterator3C { display = display, <$>display = display, <>f__this = this };
        }

        public void addMonsterPoint(ulong id)
        {
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    if (!this.deadMonsterPoints.ContainsKey(id))
                    {
                        this.deadMonsterPoints.Add(id, new Vector3(current.Monster.X, current.Monster.Y, current.Monster.Z));
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void CalcBornMonsterAttr(BaseRoleVo parVo, MonsterVo vo, string str)
        {
            AttributeInfo attribute = new AttributeInfo();
            attribute = StringUtils.GetMonBornAttributeInfo(parVo, str, attribute);
            VoUtils.SetVoAllAttributeByInfo(vo, attribute);
        }

        public static void CalcMonsterAttr(MonsterVo vo, string str)
        {
            AttributeInfo attribute = new AttributeInfo();
            attribute = StringUtils.GetAttributeByStr(str, attribute);
            VoUtils.SetVoAllAttributeByInfo(vo, attribute);
        }

        public void ClearMonsterPreload()
        {
            if (this._monsterPreloadList != null)
            {
                this._monsterPreloadList.Clear();
            }
            Resources.UnloadUnusedAssets();
        }

        public void ClearRebornCheckWhenSwitchMap()
        {
            this.rebornCheckList.Clear();
        }

        public void ClearWaitBossList(string mapId)
        {
            if (this._bossWaitList.ContainsKey(mapId))
            {
                this._bossWaitList[mapId].Clear();
            }
        }

        public bool ContainsRebornCheck(uint meMonId)
        {
            return this.rebornCheckList.ContainsKey(meMonId);
        }

        public void CreateBoss(string mapId)
        {
            if (this._bossWaitList.ContainsKey(mapId))
            {
                IEnumerator<MonsterVo> enumerator = this._bossWaitList[mapId].GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        MonsterVo current = enumerator.Current;
                        this.addMonster(mapId, current);
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                this._bossWaitList[mapId].Clear();
            }
        }

        public void DeleteRebornCheck(uint meMonId)
        {
            if (this.rebornCheckList.ContainsKey(meMonId))
            {
                this.rebornCheckList.Remove(meMonId);
            }
        }

        private void DoRemoverMonster(object ids)
        {
            string id = (ids as objes).id;
            if (StringUtils.isEmpty(id))
            {
                return;
            }
            string key = AppMap.Instance.mapParserII.MapId.ToString();
            MonsterDisplay monster = AppMap.Instance.GetMonster(id);
            if ((monster != null) && (monster.MonsterVo.quality == 3))
            {
                this.RemoveFootEffect();
            }
            AppMap.Instance.remove(monster);
            IList<MonsterVo> list = !this._monsterVoList.ContainsKey(key) ? null : this._monsterVoList[key];
            if (list == null)
            {
                return;
            }
            IEnumerator<MonsterVo> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterVo current = enumerator.Current;
                    if (current.Id.Equals(id))
                    {
                        if (current.MonsterVO.quality == 3)
                        {
                            this.BossVo = null;
                        }
                        list.Remove(current);
                        goto Label_0102;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        Label_0102:
            Singleton<BattleMode>.Instance.DoMonsterDeath();
        }

        public void execute()
        {
            if (this._isRuning && Singleton<MapMode>.Instance.isSceneComplete)
            {
                float time = Time.time;
                if (((AppMap.Instance.me.GoBase != null) && (AppMap.Instance.mapParserII != null)) && (((Time.frameCount % 10) == 0) && (this.moveMoveToCallBack != null)))
                {
                    this.moveMoveToCallBack(AppMap.Instance.me.GoBase.transform.position);
                }
                try
                {
                    this._beginTime = time;
                    if (AppMap.Instance.mapParserII != null)
                    {
                        string key = AppMap.Instance.mapParserII.MapId.ToString();
                        IList<MonsterVo> list = !this._monsterVoList.ContainsKey(key) ? null : this._monsterVoList[key];
                        if ((list != null) && (list.Count >= 1))
                        {
                            MonsterVo item = list[0];
                            if (AppMap.Instance.GetMonster(item.Id.ToString()) == null)
                            {
                                list.Remove(item);
                                if (item.IsEmptyHp)
                                {
                                    com.u3d.bases.debug.Log.info(this, "这个怪物已经死了，不用创建！");
                                }
                                else
                                {
                                    com.u3d.bases.debug.Log.info(this, "创建怪物 ，怪物类型ID：" + item.monsterId);
                                    item.ModelLoadCallBack = (Action<BaseDisplay>) Delegate.Combine(item.ModelLoadCallBack, new Action<BaseDisplay>(this.LoadMonsterCallBack));
                                    if ((AppMap.Instance.IsInWifiPVP && (item.MonsterVO.type == 5)) && (item.Camp == 0))
                                    {
                                        if (<>f__am$cache13 == null)
                                        {
                                            <>f__am$cache13 = bd => WifiPvpManager.LoadingFinished();
                                        }
                                        item.ModelLoadCallBack = (Action<BaseDisplay>) Delegate.Combine(item.ModelLoadCallBack, <>f__am$cache13);
                                    }
                                    AppMap.Instance.CreateMonster(item);
                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    com.u3d.bases.debug.Log.error(this, "-execute() 添加怪物到场景出错,reason:" + exception.StackTrace);
                }
            }
        }

        public IList<MonsterVo> getCurMapMonsters()
        {
            string key = AppMap.Instance.mapParserII.MapId.ToString();
            return (!this._monsterVoList.ContainsKey(key) ? null : this._monsterVoList[key]);
        }

        public MonsterVo GetCurrentMonster(string mapId)
        {
            IList<MonsterVo> list = !this._monsterVoList.ContainsKey(mapId) ? null : this._monsterVoList[mapId];
            if ((list != null) && (list.Count >= 1))
            {
                return list[0];
            }
            return null;
        }

        public MonsterVo GetMonster(string id)
        {
            <GetMonster>c__AnonStorey100 storey = new <GetMonster>c__AnonStorey100 {
                id = id
            };
            if (!StringUtils.isEmpty(storey.id))
            {
                string key = AppMap.Instance.mapParserII.MapId.ToString();
                IList<MonsterVo> source = !this._monsterVoList.ContainsKey(key) ? null : this._monsterVoList[key];
                if ((source != null) && (source.Count >= 1))
                {
                    return source.FirstOrDefault<MonsterVo>(new Func<MonsterVo, bool>(storey.<>m__FF));
                }
            }
            return null;
        }

        public MonsterVo GetMonster(string mapId, string monsterId)
        {
            <GetMonster>c__AnonStoreyFF yff = new <GetMonster>c__AnonStoreyFF {
                monsterId = monsterId
            };
            IList<MonsterVo> source = !this._monsterVoList.ContainsKey(mapId) ? null : this._monsterVoList[mapId];
            if ((source != null) && (source.Count >= 1))
            {
                return source.FirstOrDefault<MonsterVo>(new Func<MonsterVo, bool>(yff.<>m__FE));
            }
            return null;
        }

        public Vector3 getMonsterPoint(uint id)
        {
            if (this.deadMonsterPoints.ContainsKey((ulong) id))
            {
                return this.deadMonsterPoints[(ulong) id];
            }
            return new Vector3(0f, 0f, 0f);
        }

        public GameObject GetMonsterPreload(string url)
        {
            if (!this._monsterPreloadList.ContainsKey(url))
            {
                return null;
            }
            return this._monsterPreloadList[url];
        }

        public MonsterVo GetMonsterWithConfigId(string id)
        {
            if (!StringUtils.isEmpty(id))
            {
                foreach (MonsterDisplay display in AppMap.Instance.GetMonster())
                {
                    if (display.MonsterVo.id.ToString().Equals(id))
                    {
                        return display.Monster;
                    }
                }
            }
            return null;
        }

        public int GetNumOfMonster(uint monsterId, bool includeDead = false, bool includeStaticBox = false, bool includeMySide = false)
        {
            string key = AppMap.Instance.mapParserII.MapId.ToString();
            if ((this._monsterVoList == null) || !this._monsterVoList.ContainsKey(key))
            {
                return 0;
            }
            IList<MonsterVo> list = this._monsterVoList[key];
            int num = 0;
            IEnumerator<MonsterVo> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterVo current = enumerator.Current;
                    if ((((current != null) && (includeStaticBox || (current.MonsterVO.type != 2))) && (includeDead || (current.CurHp > 0))) && (((monsterId == 0) || (current.MonsterVO.id == monsterId)) && (includeMySide || !current.isMySide)))
                    {
                        num++;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return num;
        }

        private float GetRandomValueX()
        {
            return 0f;
        }

        public RebornCheck GetRebornCheck(uint meMonId)
        {
            return this.rebornCheckList[meMonId];
        }

        public void HideAllMonster()
        {
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    current.GoBase.SetActive(false);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void LoadMonsterCallBack(BaseDisplay display)
        {
            MonsterVo meVoByType = display.GetMeVoByType<MonsterVo>();
            display.ChangeDire(meVoByType.rotateY);
            com.u3d.bases.debug.Log.info(this, string.Format("Monster Id : {0} Model Id: {1} Quality: {2}", meVoByType.MonsterVO.id, meVoByType.MonsterVO.res, meVoByType.MonsterVO.quality));
            UnityLog.Log.AI(null, "Initial MonsterVO " + meVoByType);
            if (meVoByType.MonsterVO.quality == 3)
            {
                EffectMgr.Instance.CreateUIEffect("30029", Vector3.zero, null, null);
            }
            this.StartShowMonsterAdditionalView(display);
            ShaderUtils.ResetAllShader();
            if (GlobalData.isInCopy || AppMap.Instance.IsInWifiPVP)
            {
                HudView.Instance.AddHealthBarAfterLoadingFinish(display);
            }
        }

        private void MonsterPreLoaded(GameObject monster)
        {
            try
            {
                if ((null != monster) && !this._monsterPreloadList.ContainsKey(this._curMonsterUrl))
                {
                    this._monsterPreloadList.Add(this._curMonsterUrl, monster);
                }
            }
            finally
            {
                StartLoadingView instance = Singleton<StartLoadingView>.Instance;
                instance.PreLoadedNum++;
                this._preloadIndex++;
                this.PreloadResource();
            }
        }

        [DebuggerHidden]
        public IEnumerator PreloadMonsterList()
        {
            return new <PreloadMonsterList>c__Iterator3D { <>f__this = this };
        }

        private void PreloadResource()
        {
            try
            {
                while (this._preloadIndex < this.PreMonloadList.Count)
                {
                    SysMonsterVo vo = BaseDataMgr.instance.getSysMonsterVo(uint.Parse(this.PreMonloadList[this._preloadIndex]));
                    if (vo != null)
                    {
                        this._curMonsterUrl = "Model/Monster/" + vo.res + "/Model/BIP.assetbundle";
                        if (!this._monsterPreloadList.ContainsKey(this._curMonsterUrl))
                        {
                            com.u3d.bases.debug.Log.info(this, "preload effect asset " + this._curMonsterUrl);
                            AssetManager.Instance.LoadAsset<GameObject>(this._curMonsterUrl, new LoadAssetFinish<GameObject>(this.MonsterPreLoaded), null, false, true);
                            return;
                        }
                    }
                    StartLoadingView instance = Singleton<StartLoadingView>.Instance;
                    instance.PreLoadedNum++;
                    this._preloadIndex++;
                }
            }
            catch (Exception exception)
            {
                com.u3d.bases.debug.Log.warin(this, "preloadResource error, exception is: " + exception.Message);
            }
        }

        private void RemoveFootEffect()
        {
            if (this.FootEffect != null)
            {
                this.FootEffect.transform.parent = EffectMgr.Instance.MainEffectRoot.transform;
                this.FootEffect.transform.name = "Effect/Main/20017.assetbundle";
                this.FootEffect.gameObject.SetActive(false);
            }
        }

        public void RemoveMonster(string id)
        {
            MonsterDisplay monster = AppMap.Instance.GetMonster(id);
            objes arguments = new objes {
                id = id
            };
            if (((monster.MonsterVo.type == 2) || (monster.MonsterVo.type == 3)) || (monster.MonsterVo.type == 5))
            {
                vp_Timer.In((float) monster.MonsterVo.dead_model_keep_time, new vp_Timer.ArgCallback(this.DoRemoverMonster), arguments, null);
            }
            else
            {
                this.DoRemoverMonster(arguments);
            }
        }

        public void ResetSpriteRender(GameObject effectObj)
        {
            foreach (SpriteRenderer renderer in effectObj.GetComponentsInChildren<SpriteRenderer>())
            {
                renderer.sortingLayerName = "BackEffect";
            }
        }

        public void ShowAllMonster()
        {
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    current.GoBase.SetActive(true);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void ShowBossFootEffect(BaseDisplay boss)
        {
            if (MeVo.instance.mapId == 0x9c41)
            {
                this.FootEffect = EffectMgr.Instance.GetMainEffectGameObject("20011", string.Empty);
            }
            else
            {
                this.FootEffect = EffectMgr.Instance.GetMainEffectGameObject("20017", string.Empty);
                com.u3d.bases.debug.Log.info(this, "FootEffect is Null " + (this.FootEffect == null));
            }
            float x = boss.GetMeByType<ActionDisplay>().BoxCollider2D.size.x / 5f;
            x += 0.2f;
            if (this.FootEffect != null)
            {
                this.FootEffect.name = "FootEffect";
                this.FootEffect.transform.transform.localScale = new Vector3(x, x, x);
                this.FootEffect.transform.position = boss.GetMeByType<ActionDisplay>().GoBase.transform.position + new Vector3(0f, 0f, -10f);
                this.FootEffect.transform.parent = boss.GetMeByType<ActionDisplay>().GoBase.transform;
                this.FootEffect.SetActive(true);
                this.ResetSpriteRender(this.FootEffect);
            }
        }

        public void ShowMonsterByGroupId(uint groupId)
        {
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    MonsterVo vo = current.GetVo() as MonsterVo;
                    if (vo.groupIndex == groupId)
                    {
                        current.GoBase.SetActive(true);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void ShowMonsterSpawnEffect(Vector3 pos)
        {
            EffectMgr.Instance.CreateMainEffect("20003", pos, false, null, 0f);
        }

        public void start()
        {
            if (!this._isRuning)
            {
                this._isRuning = true;
                this._beginTime = Time.time;
            }
        }

        private void StartShowMonsterAdditionalView(BaseDisplay display)
        {
            CoroutineManager.StartCoroutine(this.AddMonsterAdditionalView(display), true);
        }

        public void stop()
        {
            this._isRuning = false;
            this.RemoveFootEffect();
            this._monsterVoList.Clear();
            this.deadMonsterPoints.Clear();
            this.LiveMonsterNumber = 0;
        }

        public void UpdateRebornCheck(uint meMonId, RebornCheck rc)
        {
            if (this.rebornCheckList.ContainsKey(meMonId))
            {
                this.rebornCheckList[meMonId] = rc;
            }
            else
            {
                this.rebornCheckList.Add(meMonId, rc);
            }
        }

        public MonsterVo BossVo
        {
            get
            {
                return this._bossVo;
            }
            set
            {
                this._bossVo = value;
                base.DataUpdate(1);
            }
        }

        public IDictionary<ulong, Vector3> getDropPointList
        {
            get
            {
                return this.deadMonsterPoints;
            }
        }

        public IList<string> PreMonloadList { get; set; }

        [CompilerGenerated]
        private sealed class <AddMonsterAdditionalView>c__Iterator3C : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BaseDisplay <$>display;
            internal MonsterMgr <>f__this;
            internal ActionControler <actionControler>__5;
            internal float <add_y>__3;
            internal MapCameraScriptData <bossBornData>__8;
            internal GameObject <child>__1;
            internal MonsterVo <item>__4;
            internal BossDeployItemInfo <itemInfo>__10;
            internal SysMonBornActionVo <monsterBornInfo>__9;
            internal MonsterDisplay <monsterDisplay>__0;
            internal GameObject <monsterMsg>__6;
            internal MouseOrbit <mouseOrbit>__7;
            internal float <y>__2;
            internal BaseDisplay display;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<monsterDisplay>__0 = this.display as MonsterDisplay;
                        this.display.GoCloth.transform.localPosition = new Vector3(0f, 0f, 0f);
                        if (this.display.Controller != null)
                        {
                            this.<child>__1 = NGUITools.AddChild(this.display.Controller.gameObject);
                            if (this.<monsterDisplay>__0 != null)
                            {
                                this.<y>__2 = (this.<monsterDisplay>__0.Controller.characterController.center.y + (this.<monsterDisplay>__0.Controller.characterController.height / 2f)) * this.display.GoBase.transform.localScale.y;
                                this.<add_y>__3 = this.display.GetMeVoByType<MonsterVo>().MonsterVO.pivot_localposition_y * 0.001f;
                                this.<y>__2 += this.<add_y>__3;
                                this.<child>__1.transform.localPosition = new Vector3(0f, this.<y>__2, 0f);
                                this.<child>__1.name = "pivot";
                                this.<item>__4 = this.display.GetVo() as MonsterVo;
                                if (this.<item>__4 != null)
                                {
                                    this.<actionControler>__5 = this.display.Controller as ActionControler;
                                    if (this.<actionControler>__5 != null)
                                    {
                                        this.<monsterMsg>__6 = HudView.Instance.AddItem("msg", this.<child>__1.transform, HudView.Type.Monster);
                                        this.<actionControler>__5.GoBossMsg = this.<monsterMsg>__6;
                                        this.<monsterDisplay>__0.InitSpeakView(this.<monsterMsg>__6);
                                    }
                                    AppMap.Instance.AddMonster(this.<monsterDisplay>__0);
                                }
                                if (this.<item>__4.MonsterVO.born_camera != 0)
                                {
                                    Singleton<BattleMode>.Instance.CloseBattleView();
                                    this.<mouseOrbit>__7 = Camera.main.gameObject.AddMissingComponent<MouseOrbit>();
                                    this.<item>__4.IsUnbeatable = true;
                                    this.<bossBornData>__8 = new MapCameraScriptData(this.<item>__4.MonsterVO.born_camera);
                                    this.<mouseOrbit>__7.SetTargetInTime(this.display.GoBase, this.<bossBornData>__8, this.<monsterDisplay>__0);
                                }
                                if (this.<item>__4.MonsterVO.born_action != 0)
                                {
                                    this.<monsterBornInfo>__9 = BaseDataMgr.instance.GetMonBornAtionVo((uint) this.<item>__4.MonsterVO.born_action);
                                    this.<itemInfo>__10 = new BossDeployItemInfo();
                                    this.<itemInfo>__10.type = this.<monsterBornInfo>__9.type;
                                    this.<itemInfo>__10.bossId = this.<item>__4.Id;
                                    this.<itemInfo>__10.animationId = this.<monsterBornInfo>__9.animation;
                                    this.<itemInfo>__10.startPoint = (Vector3) (MathUtils.GetVector3Cost(this.<monsterBornInfo>__9.start_pos.Replace("[", string.Empty).Replace("]", string.Empty)) / 1000f);
                                    this.<itemInfo>__10.endPoint = (Vector3) (MathUtils.GetVector3Cost(this.<monsterBornInfo>__9.end_pos.Replace("[", string.Empty).Replace("]", string.Empty)) / 1000f);
                                    this.<>f__this.bossBornManager.Handler(this.<itemInfo>__10.type, this.<itemInfo>__10);
                                }
                                if (DungeonMgr.Instance.curDungeonId == Singleton<ConfigConst>.Instance.GetConfigData("NEW_GUIDE_DUPLICATE"))
                                {
                                    this.<monsterDisplay>__0.StopAi();
                                }
                                else
                                {
                                    this.<monsterDisplay>__0.ReadyToUseAi(2.5f);
                                }
                            }
                        }
                        this.$current = 0;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetMonster>c__AnonStorey100
        {
            internal string id;

            internal bool <>m__FF(MonsterVo item)
            {
                return item.Id.Equals(this.id);
            }
        }

        [CompilerGenerated]
        private sealed class <GetMonster>c__AnonStoreyFF
        {
            internal string monsterId;

            internal bool <>m__FE(MonsterVo item)
            {
                return item.Id.Equals(this.monsterId);
            }
        }

        [CompilerGenerated]
        private sealed class <PreloadMonsterList>c__Iterator3D : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MonsterMgr <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.<>f__this.PreMonloadList == null)
                        {
                            goto Label_0091;
                        }
                        this.<>f__this._preloadIndex = 0;
                        this.<>f__this.PreloadResource();
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_0098;
                }
                if (this.<>f__this._preloadIndex < this.<>f__this.PreMonloadList.Count)
                {
                    this.$current = 0;
                    this.$PC = 1;
                    return true;
                }
                this.<>f__this.PreMonloadList = null;
            Label_0091:
                this.$PC = -1;
            Label_0098:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        public delegate void MonsterGroupAllDead(int groupId);

        public delegate void MoveToCallBack(Vector3 pos);

        public class objes
        {
            public string id;
        }
    }
}

