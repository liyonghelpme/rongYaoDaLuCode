﻿namespace com.game.module.map.NPC
{
    using com.game.basic.events;
    using com.game.Interface.Character;
    using com.game.module.map.Data;
    using System;
    using UnityEngine;
    using UnityEngine.Events;

    public class BaseCharacter : ICharacter, IEventDispatcher
    {
        protected Animator animator;
        protected GameObject characterModel;
        protected int characterModelId;
        protected IEventDispatcher eventDispatcher = new EventDispatcher();
        protected bool loadComplete;
        protected CharacterLoader loader;

        public void AddEventListener(string type, UnityAction<EventObject> listener)
        {
            this.eventDispatcher.AddEventListener(type, listener);
        }

        public virtual void ChangeCharacterModel(BaseSceneObjInfo roleInfo)
        {
        }

        public void Dispatcher(EventObject evt)
        {
            this.eventDispatcher.Dispatcher(evt);
        }

        public bool HasEventListener(string type)
        {
            return this.eventDispatcher.HasEventListener(type);
        }

        public void RemoveEventListener(string type, UnityAction<EventObject> listener)
        {
            this.eventDispatcher.RemoveEventListener(type, listener);
        }

        public virtual void SetAction(int actionId)
        {
        }

        public GameObject CharacterModel
        {
            get
            {
                return this.characterModel;
            }
            set
            {
                this.characterModel = value;
            }
        }

        public int CharacterModelId
        {
            get
            {
                return this.characterModelId;
            }
        }
    }
}

