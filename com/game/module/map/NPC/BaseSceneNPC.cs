﻿namespace com.game.module.map.NPC
{
    using com.game;
    using com.game.cmd;
    using com.game.module.map.Data;
    using com.liyong;
    using System;
    using UnityEngine;

    public class BaseSceneNPC : BaseSceneObject
    {
        private GameObject _speakGameObject;
        private UISprite msgBg;
        private NPCController npcController;
        public BaseSceneNPCInfo sceneNPCInfo;
        private UILabel speakLabel;

        public BaseSceneNPC(BaseSceneNPCInfo sceneNPCInfo) : base(sceneNPCInfo)
        {
            this.sceneNPCInfo = sceneNPCInfo;
            this.npcController = new NPCController();
            this.npcController.SetResource(this);
            base.Container.tag = "NPC";
        }

        protected override void AddScript(GameObject gameObject)
        {
            ClickTrigger local1 = gameObject.AddComponent<ClickTrigger>();
            local1.onTrigger = (System.Action) Delegate.Combine(local1.onTrigger, new System.Action(this.DoMouseClick));
            base.character.CharacterModel.GetComponent<NPCAnimationEventDispathcer>().SetResource(this);
        }

        public override void Dispose()
        {
            base.Dispose();
            if (this._speakGameObject != null)
            {
                UnityEngine.Object.Destroy(this._speakGameObject);
                this._speakGameObject = null;
            }
            if (this.speakLabel != null)
            {
                UnityEngine.Object.Destroy(this.speakLabel);
                this.speakLabel = null;
            }
            if (this.msgBg != null)
            {
                UnityEngine.Object.Destroy(this.msgBg);
                this.msgBg = null;
            }
            if (this.npcController != null)
            {
                this.npcController.Dispose();
            }
        }

        public override void DoMouseClick()
        {
            CombatUtil.MoveToNpc(AppMap.Instance.me.GoBase, base.Container, new Util.VoidDelegate(this.WalkToNpc));
        }

        public BaseSceneNPCInfo GetSceneNPCInfo()
        {
            return (base.roleInfo as BaseSceneNPCInfo);
        }

        protected override void InitEvent()
        {
            base.InitEvent();
        }

        public void InitSpeakView(GameObject speakView)
        {
            this._speakGameObject = speakView;
            this.speakLabel = NGUITools.FindInChild<UILabel>(speakView, "msg");
            this.msgBg = NGUITools.FindInChild<UISprite>(speakView, "msgBg");
            speakView.SetActive(false);
        }

        protected override void RemoveEvent()
        {
            base.RemoveEvent();
        }

        private void RemoveWord()
        {
            if (this._speakGameObject != null)
            {
                this._speakGameObject.SetActive(false);
            }
        }

        public void SpeakWord(string descript)
        {
            char[] separator = new char[] { '#' };
            string[] strArray = descript.Split(separator);
            int index = UnityEngine.Random.Range(0, strArray.Length);
            string str = strArray[index];
            this._speakGameObject.SetActive(true);
            this.speakLabel.text = str;
            this.msgBg.width = ((int) this.speakLabel.printedSize.x) + 20;
            this.msgBg.height = ((int) this.speakLabel.printedSize.y) + 40;
            vp_Timer.In(2f, new vp_Timer.Callback(this.RemoveWord), null);
        }

        private void WalkToNpc()
        {
            if (this.sceneNPCInfo.npcTemplate != null)
            {
                com.game.cmd.Command.Parse(this.sceneNPCInfo.npcTemplate.moduleId).Exe();
            }
        }
    }
}

