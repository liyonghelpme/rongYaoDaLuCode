﻿namespace com.game.module.map.NPC
{
    using com.game.basic.components;
    using com.game.basic.events;
    using com.game.Interface.Character;
    using com.game.module.map.Data;
    using System;
    using UnityEngine;
    using UnityEngine.Events;

    public class BaseSceneObject : Sprite3D, ISceneObject
    {
        public UnityEngine.Animator Animator;
        public ICharacter character;
        public const string CHARACTER_CREATE_COMPLETE = "characterCreateComplete";
        public CharacterController characterController;
        protected bool mouseAvoid;
        protected BaseSceneObjInfo roleInfo;

        public BaseSceneObject(BaseSceneObjInfo roleInfo)
        {
            this.roleInfo = roleInfo;
            this.character = new SceneCharacter(roleInfo);
            this.InitEvent();
        }

        protected virtual void AddCharacterController()
        {
            Component component = base.Container.GetComponent<CharacterController>();
            if (component != null)
            {
                this.characterController = component as CharacterController;
            }
            else
            {
                this.characterController = base.Container.AddComponent<CharacterController>();
            }
            CharacterController controller = this.character.CharacterModel.GetComponent<CharacterController>();
            this.characterController.center = controller.center;
            this.characterController.radius = controller.radius;
            this.characterController.height = controller.height;
            UnityEngine.Object.Destroy(controller);
        }

        protected virtual void AddScript(GameObject gameObject)
        {
        }

        public void ChangeCharacterStyle(BaseSceneObjInfo roleInfo)
        {
            this.character.ChangeCharacterModel(roleInfo);
        }

        protected Vector3 Clone(Vector3 v3)
        {
            return new Vector3 { x = v3.x, y = v3.y, z = v3.z };
        }

        private void CreateCharacterCompleteHandler(EventObject evt)
        {
            this.Animator = (this.character as SceneCharacter).GetAnimator;
            this.Relation(this.character.CharacterModel, base.Container.transform);
            this.SetPosition(this.roleInfo.scenePos);
            this.SetRotation(this.roleInfo.rotation);
            this.AddCharacterController();
            this.AddScript(base.Container);
            this.Dispatcher(new EventObject("characterCreateComplete", null));
        }

        public virtual void Dispose()
        {
            this.RemoveEvent();
            this.Animator = null;
            if ((this.character != null) && (this.character.CharacterModel != null))
            {
                UnityEngine.Object.Destroy(this.character.CharacterModel);
            }
            if (base.Container != null)
            {
                UnityEngine.Object.Destroy(base.Container);
            }
        }

        public virtual void DoMouseClick()
        {
        }

        public virtual BaseSceneObjInfo GetBaseSceneObjInfo()
        {
            return this.roleInfo;
        }

        public Sprite3D GetFigure()
        {
            return this;
        }

        protected virtual void InitEvent()
        {
            this.character.AddEventListener("createCharacterCompleted", new UnityAction<EventObject>(this.CreateCharacterCompleteHandler));
        }

        protected void Relation(GameObject child, Transform parent)
        {
            if (parent != null)
            {
                this.RemoveAllChild(parent);
                Vector3 vector = this.Clone(child.transform.position);
                Vector3 vector2 = this.Clone(child.transform.eulerAngles);
                Vector3 vector3 = this.Clone(child.transform.lossyScale);
                child.transform.parent = parent;
                child.transform.localPosition = vector;
                child.transform.localEulerAngles = vector2;
                child.transform.localScale = vector3;
            }
        }

        protected void RemoveAllChild(Transform parent)
        {
            if (parent != null)
            {
                parent.DetachChildren();
            }
        }

        protected virtual void RemoveEvent()
        {
            this.character.RemoveEventListener("createCharacterCompleted", new UnityAction<EventObject>(this.CreateCharacterCompleteHandler));
        }

        public void SetMouseAvoid(bool mouseAvoid)
        {
            this.mouseAvoid = mouseAvoid;
        }

        public void SetPosition(Vector3 position)
        {
            base.Position = position;
        }

        public void SetPosition(float x, float y, float z)
        {
            Vector3 vector = new Vector3(x, y, z);
            base.Container.transform.position = vector;
        }

        public virtual void SetRoleInfo(BaseSceneObjInfo roleInfo)
        {
            this.roleInfo = roleInfo;
        }

        public void SetRotation(Vector3 rotation)
        {
            base.Container.transform.rotation = Quaternion.Euler(rotation);
        }
    }
}

