﻿namespace com.game.module.map.NPC
{
    using com.game.manager;
    using com.game.module.map.Data;
    using System;
    using UnityEngine;

    public class CharacterLoader
    {
        private Action<GameObject, int> loadModelCallback;

        private int GetModelId(string name)
        {
            name = name.Replace("(Clone)", string.Empty);
            return Convert.ToInt32(name);
        }

        private string GetModelPath(int modelType, int modelId)
        {
            string str = string.Empty;
            if (modelType != 5)
            {
                return str;
            }
            object[] objArray1 = new object[] { "Model/Npc/", modelId, "/Model/", modelId, ".assetbundle" };
            return string.Concat(objArray1);
        }

        public void LoadCharacterModel(BaseSceneObjInfo info, Action<GameObject, int> callback)
        {
            string modelPath = this.GetModelPath(info.GetObjType(), info.modelId);
            this.loadModelCallback = callback;
            AssetManager.Instance.LoadAsset<GameObject>(modelPath, new LoadAssetFinish<GameObject>(this.LoadComplete), null, false, true);
        }

        private void LoadComplete(GameObject gameObject)
        {
            this.loadModelCallback(gameObject, this.GetModelId(gameObject.name));
        }
    }
}

