﻿namespace com.game.module.map.NPC
{
    using System;
    using UnityEngine;

    public class ClickTrigger : MonoBehaviour
    {
        public System.Action onTrigger;

        private void Start()
        {
        }

        private void Update()
        {
            RaycastHit hit;
            if ((Input.GetMouseButtonDown(0) && (UICamera.lastHit.collider == null)) && ((Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit) && (hit.collider != null)) && ((hit.collider.tag == base.transform.tag) && (this.onTrigger != null))))
            {
                this.onTrigger();
            }
        }
    }
}

