﻿namespace com.game.module.map.NPC
{
    using com.game.basic.events;
    using System;
    using UnityEngine;

    public class NPCAnimationEventDispathcer : MonoBehaviour
    {
        private BaseSceneObject sceneObject;
        public const string STAND1_ANIMATION_OVER = "stand1AnimationOver";
        public const string STAND2_ANIMATION_OVER = "stand2AnimationOver";
        public const string STAND3_ANIMATION_OVER = "stand3AnimationOver";

        public void SetResource(BaseSceneObject sceneObject)
        {
            this.sceneObject = sceneObject;
        }

        public void Stand1AnimationOver()
        {
            this.sceneObject.Dispatcher(new EventObject("stand1AnimationOver", null));
        }

        public void Stand2AnimationOver()
        {
            this.sceneObject.Dispatcher(new EventObject("stand2AnimationOver", null));
        }

        public void Stand3AnimationOver()
        {
            this.sceneObject.Dispatcher(new EventObject("stand3AnimationOver", null));
        }
    }
}

