﻿namespace com.game.module.map.NPC
{
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.Interface.Character;
    using com.game.Interface.Tick;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Events;

    public class NPCController : ITick
    {
        private float _time;
        private int lastActionId;
        private BaseSceneNPC npc;
        private SysNpcVo npcTemplate;
        private int tick;

        private void AnimationEndHandler(EventObject evt)
        {
            this.npc.character.SetAction(0);
        }

        private void CreateNPCComplete(EventObject evt)
        {
            GlobalAPI.tickManager.addTick(this);
        }

        public void Dispose()
        {
            this.RemoveEvent();
            GlobalAPI.tickManager.removeTick(this);
            this.npc = null;
            this.npcTemplate = null;
        }

        private void InitEvent()
        {
            this.npc.AddEventListener("characterCreateComplete", new UnityAction<EventObject>(this.CreateNPCComplete));
            this.npc.AddEventListener("stand1AnimationOver", new UnityAction<EventObject>(this.AnimationEndHandler));
            this.npc.AddEventListener("stand2AnimationOver", new UnityAction<EventObject>(this.AnimationEndHandler));
            this.npc.AddEventListener("stand3AnimationOver", new UnityAction<EventObject>(this.AnimationEndHandler));
        }

        private void RemoveEvent()
        {
            this.npc.RemoveEventListener("characterCreateComplete", new UnityAction<EventObject>(this.CreateNPCComplete));
            this.npc.RemoveEventListener("stand1AnimationOver", new UnityAction<EventObject>(this.AnimationEndHandler));
            this.npc.RemoveEventListener("stand2AnimationOver", new UnityAction<EventObject>(this.AnimationEndHandler));
            this.npc.RemoveEventListener("stand3AnimationOver", new UnityAction<EventObject>(this.AnimationEndHandler));
        }

        public void SetResource(ISceneObject npc)
        {
            this.npc = npc as BaseSceneNPC;
            this.npcTemplate = this.npc.GetSceneNPCInfo().npcTemplate;
            this.InitEvent();
        }

        public void Update(int times, float dt = 0.04f)
        {
            this.tick++;
            this._time += RealTime.deltaTime;
            if (this._time >= this.npcTemplate.speak_cd)
            {
                if (UnityEngine.Random.Range((float) 0f, (float) 1f) >= (this.npcTemplate.speak_rate / 100))
                {
                    this.npc.SpeakWord(this.npcTemplate.descript);
                    this.tick = 0;
                    this._time = 0f;
                }
                int actionId = UnityEngine.Random.Range(1, 4);
                if ((this.lastActionId != 0) && (this.lastActionId == actionId))
                {
                    if (actionId == 1)
                    {
                        actionId++;
                    }
                    else
                    {
                        actionId--;
                    }
                }
                this.lastActionId = actionId;
                this.npc.character.SetAction(actionId);
            }
        }
    }
}

