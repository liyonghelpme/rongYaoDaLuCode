﻿namespace com.game.module.map.NPC
{
    using com.game.basic.events;
    using com.game.module.map.Data;
    using System;
    using UnityEngine;

    public class SceneCharacter : BaseCharacter
    {
        public const string CREATE_CHARACTER_COMPLETED = "createCharacterCompleted";

        public SceneCharacter(BaseSceneObjInfo roleInfo)
        {
            base.loader = new CharacterLoader();
            base.loader.LoadCharacterModel(roleInfo, new Action<GameObject, int>(this.LoadCharacterModelCallback));
        }

        public override void ChangeCharacterModel(BaseSceneObjInfo roleInfo)
        {
            base.loader = (base.loader != null) ? base.loader : new CharacterLoader();
            base.loader.LoadCharacterModel(roleInfo, new Action<GameObject, int>(this.LoadCharacterModelCallback));
        }

        private void LoadCharacterModelCallback(GameObject model, int modelId)
        {
            base.loadComplete = true;
            base.characterModelId = modelId;
            if (base.characterModel != null)
            {
                UnityEngine.Object.Destroy(base.characterModel);
            }
            base.characterModel = (GameObject) UnityEngine.Object.Instantiate(model);
            base.animator = base.characterModel.GetComponent<Animator>();
            this.Dispatcher(new EventObject("createCharacterCompleted", null));
        }

        public override void SetAction(int actionId)
        {
            base.animator.SetInteger("State", actionId);
        }

        public Animator GetAnimator
        {
            get
            {
                return base.animator;
            }
        }
    }
}

