﻿namespace com.game.module.map
{
    using com.game;
    using com.game.module.hud;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class PlayerMgr
    {
        private float beginTime;
        private float delay = 0.05f;
        public static PlayerMgr instance = new PlayerMgr();
        private bool isRuning;
        private readonly List<PlayerVo> playerList = new List<PlayerVo>();
        private readonly List<PlayerVo> waitCreatePlayerList = new List<PlayerVo>();

        public void addPlayer(PlayerVo playerVo)
        {
            if (playerVo != null)
            {
                foreach (PlayerVo vo in this.playerList)
                {
                    if (vo.Id.Equals(playerVo.Id))
                    {
                        this.removePlayer(vo.Id);
                        break;
                    }
                }
                this.playerList.Add(playerVo);
                this.waitCreatePlayerList.Add(playerVo);
            }
        }

        public void Clear()
        {
            this.playerList.Clear();
            this.waitCreatePlayerList.Clear();
        }

        public void execute()
        {
            if (this.isRuning && ((Time.time - this.beginTime) >= this.delay))
            {
                this.beginTime = Time.time;
                if (this.waitCreatePlayerList.Count > 0)
                {
                    <execute>c__AnonStorey101 storey = new <execute>c__AnonStorey101 {
                        item = this.waitCreatePlayerList[0]
                    };
                    this.waitCreatePlayerList.Remove(storey.item);
                    storey.item.ModelLoadCallBack = new Action<BaseDisplay>(this.LoadPlayerModelBack);
                    storey.item.ModelLoadCallBack = (Action<BaseDisplay>) Delegate.Combine(storey.item.ModelLoadCallBack, new Action<BaseDisplay>(storey.<>m__101));
                    AppMap.Instance.CreatePlayer(storey.item);
                }
            }
        }

        internal PlayerVo getPlayer(uint playerId)
        {
            foreach (PlayerVo vo in this.playerList)
            {
                if (vo.Id.Equals((ulong) playerId))
                {
                    return vo;
                }
            }
            return null;
        }

        private void LoadPlayerModelBack(BaseDisplay display)
        {
            PlayerDisplay display2 = display as PlayerDisplay;
            if (display.GetMeVoByType<BaseRoleVo>().IsEmptyHp)
            {
                display.Controller.StatuController.SetStatu(11);
                CommandHandler.AddCommandStatic(display.GoBase, "death", 1f);
            }
            if (!AppMap.Instance.IsInMainCity)
            {
                HudView.Instance.AddHealthBarAfterLoadingFinish(display);
            }
        }

        internal void removePlayer(ulong playerId)
        {
            AppMap.Instance.remove(AppMap.Instance.GetPlayer(playerId.ToString()));
            foreach (PlayerVo vo in this.playerList)
            {
                if (vo.Id.Equals(playerId))
                {
                    this.playerList.Remove(vo);
                    break;
                }
            }
            foreach (PlayerVo vo2 in this.waitCreatePlayerList)
            {
                if (vo2.Id.Equals(playerId))
                {
                    this.waitCreatePlayerList.Remove(vo2);
                    break;
                }
            }
        }

        public void start()
        {
            if (!this.isRuning)
            {
                this.isRuning = true;
                this.beginTime = Time.time;
            }
        }

        public void stop()
        {
            this.isRuning = false;
        }

        [CompilerGenerated]
        private sealed class <execute>c__AnonStorey101
        {
            internal PlayerVo item;

            internal void <>m__101(BaseDisplay bd)
            {
                if (this.item.rotateY > 0f)
                {
                    bd.ChangeDire(this.item.rotateY);
                }
            }
        }
    }
}

