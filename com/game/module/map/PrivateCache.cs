﻿namespace com.game.module.map
{
    using com.game.manager;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PrivateCache
    {
        public int count;
        public IList<int> delayList;
        public int groupId;
        public float lastUpdateTime;
        public int monId;
        public float nextUpdateTime;
        public int nowIndex;
        public int nowWave;
        public float rotateY;
        public int totalWave;
        public float x;
        public float y;
        public float z;

        public PrivateCache(int monId, int totalWave, int count, int delayId, int groupId, int x, int y, int z, int rotateY)
        {
            this.monId = monId;
            this.nowWave = 0;
            this.totalWave = totalWave;
            this.groupId = groupId;
            this.delayList = HandleDelayString(delayId);
            this.x = x * 0.001f;
            this.y = y * 0.001f;
            this.z = z * 0.001f;
            this.rotateY = rotateY;
        }

        private static List<int> HandleDelayString(int id)
        {
            return StringUtils.GetStringToInt(BaseDataMgr.instance.GetDataById<SysDelayControlVo>((uint) id).delay_list).ToList<int>();
        }

        public void Update(float nowtime)
        {
            int num = (this.nowIndex < (this.delayList.Count - 1)) ? this.nowIndex++ : (this.delayList.Count - 1);
            this.nextUpdateTime = nowtime + (((float) this.delayList[num]) * 0.001f);
            this.lastUpdateTime = nowtime;
            this.nowWave++;
        }
    }
}

