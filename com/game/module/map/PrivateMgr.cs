﻿namespace com.game.module.map
{
    using com.game;
    using com.game.utils;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using UnityEngine;

    public class PrivateMgr
    {
        private readonly List<PPvpPrivate> _cacheReqList = new List<PPvpPrivate>();
        private readonly ICollection<PrivateCache> _delPrivateList = new List<PrivateCache>();
        private bool _isEnable;
        private bool _isInitialized;
        private float _nowTime;
        private readonly ICollection<PrivateCache> _privateList = new List<PrivateCache>();
        public static readonly PrivateMgr Instance;
        private static int testYet;

        static PrivateMgr()
        {
            if (Instance == null)
            {
            }
            Instance = new PrivateMgr();
            testYet = 8;
        }

        private PPvpPrivate AddPrivate(PrivateCache pc)
        {
            return new PPvpPrivate { monId = Convert.ToUInt64(pc.monId), x = Convert.ToInt32((float) (pc.x * 1000f)), y = Convert.ToInt32((float) (pc.y * 1000f)), z = Convert.ToInt32((float) (pc.z * 1000f)), dir = Convert.ToInt32(pc.rotateY), groupId = pc.groupId };
        }

        public void Clear()
        {
            this._isEnable = false;
            this._isInitialized = false;
            this._privateList.Clear();
            this._cacheReqList.Clear();
            this._delPrivateList.Clear();
        }

        private void DoRequestServerAddPrivate(List<PPvpPrivate> cacheReqList)
        {
            MemoryStream msdata = new MemoryStream();
            Module_14.write_14_15(msdata, cacheReqList);
            AppNet.gameNet.send(msdata, 14, 15);
        }

        public void Init(string privateString)
        {
            this._privateList.Clear();
            if (privateString != "[]")
            {
                List<int> list = StringUtils.GetArrayStringToInt(privateString).ToList<int>();
                for (int i = 0; i < list.Count; i += 9)
                {
                    PrivateCache item = new PrivateCache(list[i], list[i + 1], list[i + 2], list[i + 3], list[i + 4], list[i + 5], list[i + 6], list[i + 7], list[i + 8]);
                    this._privateList.Add(item);
                }
            }
            this._isInitialized = true;
            this._isEnable = true;
        }

        public void Update()
        {
            if (this._isInitialized && this._isEnable)
            {
                this._nowTime = Time.time;
                IEnumerator<PrivateCache> enumerator = this._privateList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        PrivateCache current = enumerator.Current;
                        if (current.nextUpdateTime < this._nowTime)
                        {
                            if ((current.lastUpdateTime != 0f) && !Main.mainObj.HidePrivate)
                            {
                                this._cacheReqList.Add(this.AddPrivate(current));
                                testYet--;
                            }
                            current.Update(this._nowTime);
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                if ((this._cacheReqList.Count > 0) && AppMap.Instance.IsInWifiPVP)
                {
                    this.DoRequestServerAddPrivate(this._cacheReqList);
                }
                IEnumerator<PrivateCache> enumerator2 = this._delPrivateList.GetEnumerator();
                try
                {
                    while (enumerator2.MoveNext())
                    {
                        PrivateCache item = enumerator2.Current;
                        this._privateList.Remove(item);
                    }
                }
                finally
                {
                    if (enumerator2 == null)
                    {
                    }
                    enumerator2.Dispose();
                }
                this._cacheReqList.Clear();
                this._delPrivateList.Clear();
            }
        }
    }
}

