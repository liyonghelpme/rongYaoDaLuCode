﻿namespace com.game.module.map
{
    using com.game;
    using com.game.module.Arena;
    using com.game.module.core;
    using com.game.module.General;
    using com.game.module.hud;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SelfPlayerManager
    {
        public deCallBack callback;
        public static SelfPlayerManager instance = new SelfPlayerManager();
        public bool isAllReloaded;
        public bool isInCorp;
        public bool isRuning;
        private uint loadRoleCount;
        public ulong onFightGeneralId;
        private List<MeVo> selfPlayerVoList = new List<MeVo>();
        private List<MeVo> waitPlayerVoList = new List<MeVo>();

        public void AddSelfPlayer(MeVo playerVo)
        {
            if (playerVo != null)
            {
                this.selfPlayerVoList.Add(playerVo);
                this.waitPlayerVoList.Add(playerVo);
                this.loadRoleCount++;
                this.isAllReloaded = false;
            }
        }

        public void changeOnFightVo(ulong id)
        {
            bool isAiEnable = AppMap.Instance.me.Controller.AiController.IsAiEnable;
            this.onFightGeneralId = id;
            IEnumerator<MeDisplay> enumerator = AppMap.Instance.SelfplayerList.Cast<MeDisplay>().GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MeDisplay current = enumerator.Current;
                    if (current.GetVo().Id == this.onFightGeneralId)
                    {
                        Camera.main.gameObject.AddMissingComponent<MouseOrbit>().setTarget(current.GoBase, false);
                        current.IsUsing = true;
                        AppMap.Instance.me = current;
                        Singleton<BattleCenterView>.Instance.updateDeathEffect();
                        Singleton<BattleBottomRightView>.Instance.SetSkillIcon();
                    }
                    else
                    {
                        current.IsUsing = false;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            BaseDisplay.HandleShadowColor(AppMap.Instance.SelfplayerList);
            PlayerAiController.InitPlayerAi(isAiEnable);
        }

        public void clear()
        {
            this.selfPlayerVoList.Clear();
            this.waitPlayerVoList.Clear();
        }

        public void createVoByGeneralInfo(bool bNeedClear = false)
        {
            if (bNeedClear)
            {
                this.selfPlayerVoList.Clear();
                this.waitPlayerVoList.Clear();
            }
            uint num2 = 0;
            foreach (GeneralFightInfo info in Singleton<GeneralMode>.Instance.generalsFightAttrList.Values)
            {
                MeVo playerVo = new MeVo {
                    generalTemplateInfo = info.genralTemplateInfo,
                    Id = info.attr.id
                };
                playerVo = this.setVoByGeneralInfo(info, playerVo);
                playerVo.job = Convert.ToByte(info.genralTemplateInfo.career);
                playerVo.Level = MeVo.instance.Level;
                float num3 = 0f;
                float num4 = 0f;
                if (Singleton<GeneralMode>.Instance.curSelectedPlace == info.place)
                {
                    playerVo.X = MeVo.instance.toX;
                    playerVo.Y = MeVo.instance.toY;
                    playerVo.Z = MeVo.instance.toZ;
                    playerVo.rotateY = MeVo.instance.rotateY;
                    this.onFightGeneralId = playerVo.Id;
                }
                else
                {
                    num3 = ((num2++ % 2) != 0) ? ((float) (-1)) : ((float) 1);
                    num4 = -1f;
                    playerVo.X = MeVo.instance.toX + num3;
                    playerVo.Y = MeVo.instance.toY;
                    playerVo.Z = MeVo.instance.toZ + num4;
                }
                this.AddSelfPlayer(playerVo);
            }
            if (AppMap.Instance.IsInArena)
            {
                ArenaManager.Instance.FixCoordinator(this.selfPlayerVoList);
            }
            else
            {
                Singleton<DungeonPlayerDriver>.Instance.InitPos(this.selfPlayerVoList);
            }
        }

        private void LoadPlayerModelBack(BaseDisplay display)
        {
            if ((display.GetVo().Id == this.onFightGeneralId) || !this.isInCorp)
            {
                display.IsUsing = true;
            }
            else
            {
                display.IsUsing = false;
            }
            this.loadRoleCount--;
            if (this.loadRoleCount == 0)
            {
                this.isAllReloaded = true;
                this.callback(display);
                PlayerAiManager.instance.Clear();
                PlayerAiManager.instance.Add(AppMap.Instance.SelfplayerList);
            }
            if (this.isInCorp)
            {
                GameObject obj2 = NGUITools.AddChild(display.Controller.gameObject);
                obj2.name = "blood";
                Transform target = obj2.transform;
                float magnitude = display.GoCloth.GetComponentInChildren<SkinnedMeshRenderer>().bounds.extents.magnitude;
                target.localPosition = new Vector3(0f, magnitude, 0f);
                GameObject obj3 = null;
                obj3 = HudView.Instance.AddPlayerBloodBar(target, display);
                display.Controller.GoHp = obj3;
            }
        }

        private MeVo setVoByGeneralInfo(GeneralFightInfo generalInfo, MeVo playerVo)
        {
            VoUtils.SetVoAllAttribute(playerVo, generalInfo.attr, generalInfo.EquipAttr);
            foreach (PGeneralSkillInfo info in generalInfo.skillInfoList)
            {
                SkillInfoVo item = new SkillInfoVo {
                    groupId = info.groupId,
                    skillLvl = info.skillLvl
                };
                playerVo.SkillInfoList.Add(item);
            }
            return playerVo;
        }

        public void start()
        {
            if (!this.isRuning)
            {
                this.isRuning = true;
            }
        }

        public void Update()
        {
            if (this.isRuning && (this.waitPlayerVoList.Count > 0))
            {
                MeVo item = this.waitPlayerVoList[0];
                this.waitPlayerVoList.Remove(item);
                item.ModelLoadCallBack = new Action<BaseDisplay>(this.LoadPlayerModelBack);
                if (!this.isInCorp)
                {
                    AppMap.Instance.CreateMe(item);
                }
                else if (item.Id == this.onFightGeneralId)
                {
                    AppMap.Instance.CreateMeInCorp(item, true);
                }
                else
                {
                    AppMap.Instance.CreateMeInCorp(item, false);
                }
            }
        }

        public bool hasGeneralEverDead
        {
            get
            {
                foreach (MeVo vo in this.selfPlayerVoList)
                {
                    if (vo.numDead > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool isAllGeneralsDead
        {
            get
            {
                foreach (MeVo vo in this.selfPlayerVoList)
                {
                    if (vo.CurHp > 0)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public bool isAllRescued
        {
            get
            {
                foreach (MeVo vo in this.selfPlayerVoList)
                {
                    if (vo.isImprisoned || (vo.numDead > 0))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public MeVo mainMeVo
        {
            get
            {
                foreach (MeVo vo in this.selfPlayerVoList)
                {
                    if (vo.Id == this.onFightGeneralId)
                    {
                        return vo;
                    }
                }
                return null;
            }
        }

        public List<MeVo> MePlayerVoList
        {
            get
            {
                return this.selfPlayerVoList;
            }
        }
    }
}

