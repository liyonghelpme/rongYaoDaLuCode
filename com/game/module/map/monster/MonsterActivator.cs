﻿namespace com.game.module.map.monster
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class MonsterActivator : BaseMode<MonsterActivator>
    {
        private bool _enabled;
        private Dictionary<int, Type> _type2ClsDic = new Dictionary<int, Type>();
        private Dictionary<int, MonsterCache> _type2InstDic = new Dictionary<int, MonsterCache>();

        public MonsterActivator()
        {
            this._type2ClsDic.Add(0x3e7, typeof(MonsterMobCache));
            this._type2ClsDic.Add(1, typeof(MonsterAreaCache));
        }

        public void AddMonster(MonsterVo monster)
        {
            switch (DungeonMgr.Instance.curFightType)
            {
                case 2:
                case 3:
                    this.GetCache(0x3e7).Add(monster);
                    return;
            }
            int num2 = monster.MonsterVO.born_type;
            if (num2 <= 0)
            {
                Singleton<MonsterMgr>.Instance.addMonster(monster.mapId.ToString(), monster);
            }
            else
            {
                SysMonsterBornTypeVo monBornTypeVo = BaseDataMgr.instance.GetMonBornTypeVo((uint) num2);
                MonsterCache cache = this.GetCache(monBornTypeVo.type);
                if (cache != null)
                {
                    cache.Add(monster);
                }
            }
        }

        public void Clear()
        {
            foreach (MonsterCache cache in this._type2InstDic.Values)
            {
                cache.Clear();
            }
        }

        private MonsterCache GetCache(int type)
        {
            if (this._type2InstDic.ContainsKey(type))
            {
                return this._type2InstDic[type];
            }
            if (this._type2ClsDic.ContainsKey(type))
            {
                Type type2 = this._type2ClsDic[type];
                MonsterCache cache = (MonsterCache) Activator.CreateInstance(type2);
                this._type2InstDic.Add(type, cache);
                return cache;
            }
            return null;
        }

        public int GetNumOfMonster(uint monsterId, bool includeMySide = false)
        {
            int num = 0;
            foreach (MonsterCache cache in this._type2InstDic.Values)
            {
                num += cache.GetNumById(monsterId, includeMySide);
            }
            return num;
        }

        public void Start()
        {
            this.Enabled = true;
            if (DungeonMgr.Instance.curMissionSysVo.fight_type == 2)
            {
                this.mobCache.NextStage();
            }
        }

        public void Update()
        {
            if (this._enabled && ((this._type2InstDic != null) && (this._type2InstDic.Count > 0)))
            {
                foreach (MonsterCache cache in this._type2InstDic.Values)
                {
                    cache.Update();
                }
            }
        }

        public bool Enabled
        {
            get
            {
                return this._enabled;
            }
            set
            {
                this._enabled = value;
            }
        }

        public MonsterMobCache mobCache
        {
            get
            {
                return (MonsterMobCache) this.GetCache(0x3e7);
            }
        }

        public int numMonsters
        {
            get
            {
                int num = 0;
                foreach (MonsterCache cache in this._type2InstDic.Values)
                {
                    num += cache.numMonster;
                }
                return num;
            }
        }
    }
}

