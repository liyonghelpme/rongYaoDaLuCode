﻿namespace com.game.module.map.monster
{
    using com.game.data;
    using com.game.manager;
    using com.game.utils;
    using com.game.vo;
    using System;
    using UnityEngine;

    internal class MonsterArea
    {
        private MonsterVo _mon;
        private float range;
        private float x;
        private float y;
        private float z;

        public MonsterArea(MonsterVo mon)
        {
            this._mon = mon;
            int num = mon.MonsterVO.born_type;
            SysMonsterBornTypeVo monBornTypeVo = BaseDataMgr.instance.GetMonBornTypeVo((uint) num);
            char[] separator = new char[] { ',' };
            string[] strArray = StringUtils.GetValueString(monBornTypeVo.param1).Split(separator);
            this.x = float.Parse(strArray[0]);
            this.y = float.Parse(strArray[1]);
            this.z = float.Parse(strArray[2]);
            this.range = 0.001f * float.Parse(monBornTypeVo.param2);
        }

        public bool HasHit(Vector3 pos)
        {
            pos.x -= this.x;
            pos.z -= this.z;
            float num = (pos.x * pos.x) + (pos.z * pos.z);
            return (num <= (this.range * this.range));
        }

        public MonsterVo mon
        {
            get
            {
                return this._mon;
            }
        }
    }
}

