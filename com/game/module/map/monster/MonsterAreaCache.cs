﻿namespace com.game.module.map.monster
{
    using com.game;
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class MonsterAreaCache : MonsterCache
    {
        private Dictionary<ulong, MonsterArea> _monDic = new Dictionary<ulong, MonsterArea>();

        public override void Add(MonsterVo mon)
        {
            if (!this._monDic.ContainsKey(mon.Id))
            {
                MonsterArea area = new MonsterArea(mon);
                this._monDic.Add(mon.Id, area);
            }
        }

        public override void Clear()
        {
            this._monDic.Clear();
        }

        public override int GetNumById(uint id, bool includeMySide)
        {
            int num = 0;
            foreach (MonsterArea area in this._monDic.Values)
            {
                if (((id == 0) || (area.mon.MonsterVO.id == id)) && (includeMySide || !area.mon.isMySide))
                {
                    num++;
                }
            }
            return num;
        }

        public override void Update()
        {
            if (((Singleton<MapMode>.Instance.isSceneComplete && (this._monDic.Count > 0)) && ((AppMap.Instance.me.GoBase != null) || (AppMap.Instance.mapParserII != null))) && ((Time.frameCount % 8) == 0))
            {
                foreach (MonsterArea area in this._monDic.Values)
                {
                    if (area.HasHit(AppMap.Instance.me.GoBase.transform.position))
                    {
                        this._monDic.Remove(area.mon.Id);
                        Singleton<MonsterMgr>.Instance.addMonster(area.mon.mapId.ToString(), area.mon);
                        break;
                    }
                }
            }
        }

        public override int numMonster
        {
            get
            {
                return this._monDic.Count;
            }
        }
    }
}

