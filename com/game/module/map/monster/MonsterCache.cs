﻿namespace com.game.module.map.monster
{
    using com.game.vo;
    using System;

    public abstract class MonsterCache
    {
        protected MonsterCache()
        {
        }

        public abstract void Add(MonsterVo mon);
        public abstract void Clear();
        public abstract int GetNumById(uint id, bool includeMySide);
        public abstract void Update();

        public abstract int numMonster { get; }
    }
}

