﻿namespace com.game.module.map.monster
{
    using com.game.vo;
    using System;
    using System.Collections.Generic;

    public class MonsterMobCache : MonsterCache
    {
        private int _curStage;
        private int _maxStage;
        private Dictionary<int, MonsterStage> _stageList = new Dictionary<int, MonsterStage>();

        public override void Add(MonsterVo mon)
        {
            MonsterStage stage;
            int groupIndex = mon.groupIndex;
            if (this._stageList.ContainsKey(groupIndex))
            {
                stage = this._stageList[groupIndex];
            }
            else
            {
                stage = new MonsterStage(groupIndex);
                this._stageList.Add(groupIndex, stage);
            }
            stage.Add(mon);
            if (this._maxStage < groupIndex)
            {
                this._maxStage = groupIndex;
            }
        }

        private bool CheckNextStage()
        {
            return false;
        }

        public override void Clear()
        {
            this._curStage = 0;
            this._maxStage = 0;
            this._stageList.Clear();
        }

        public void CreateStageMonsters()
        {
            if ((this._curStage <= this._maxStage) && this._stageList.ContainsKey(this._curStage))
            {
                MonsterStage stage = this._stageList[this._curStage];
                this._stageList.Remove(this._curStage);
                stage.Exe();
            }
        }

        public override int GetNumById(uint id, bool includeMySide)
        {
            int num = 0;
            foreach (MonsterStage stage in this._stageList.Values)
            {
                num += stage.NumOf(id, includeMySide);
            }
            return num;
        }

        public void NextStage()
        {
            do
            {
                if (this.curStage < this.maxStage)
                {
                    this.curStage++;
                }
                else
                {
                    return;
                }
            }
            while (!this._stageList.ContainsKey(this.curStage));
            this.CreateStageMonsters();
        }

        public override void Update()
        {
            if (this.CheckNextStage())
            {
                this.NextStage();
            }
        }

        public int curStage
        {
            get
            {
                return this._curStage;
            }
            set
            {
                this._curStage = value;
            }
        }

        public int maxStage
        {
            get
            {
                return this._maxStage;
            }
        }

        public override int numMonster
        {
            get
            {
                int num = 0;
                foreach (MonsterStage stage in this._stageList.Values)
                {
                    num += stage.count;
                }
                return num;
            }
        }
    }
}

