﻿namespace com.game.module.map.monster
{
    using com.game.module.core;
    using com.game.vo;
    using System;
    using System.Collections.Generic;

    internal class MonsterStage
    {
        private List<MonsterVo> _list = new List<MonsterVo>();
        private int _stage;

        public MonsterStage(int stage)
        {
            this._stage = stage;
        }

        public void Add(MonsterVo mon)
        {
            if ((mon != null) && (this._list.IndexOf(mon) < 0))
            {
                this._list.Add(mon);
            }
        }

        public void Exe()
        {
            foreach (MonsterVo vo in this._list)
            {
                Singleton<MonsterMgr>.Instance.addMonster(vo.mapId.ToString(), vo);
            }
            this._list.Clear();
        }

        public int NumOf(uint id, bool includeMySide)
        {
            int num = 0;
            foreach (MonsterVo vo in this._list)
            {
                if (((id == 0) || (vo.MonsterVO.id == id)) && (includeMySide || !vo.isMySide))
                {
                    num++;
                }
            }
            return num;
        }

        public int count
        {
            get
            {
                return this._list.Count;
            }
        }
    }
}

