﻿namespace com.game.module.network
{
    using com.game.module.core;
    using System;

    public class NetworkInfoView : BaseView<NetworkInfoView>
    {
        private float delayTime;
        private UISprite info;

        protected override void Init()
        {
            this.info = base.FindInChild<UISprite>("icn");
            this.delayTime = 0f;
            this.info.spriteName = "wifi1";
        }

        public void SetDisConnect()
        {
            if (this.info != null)
            {
                this.info.spriteName = "wifi1";
                this.delayTime = 0f;
            }
        }

        public void UpdateDelayInfo(float delayTime)
        {
            if (!object.ReferenceEquals(this.gameObject, null) && (this.delayTime != delayTime))
            {
                this.delayTime = delayTime;
                if (this.delayTime < 0.1)
                {
                    this.info.spriteName = "wifi4";
                }
                else if (this.delayTime < 0.3)
                {
                    this.info.spriteName = "wifi3";
                }
                else
                {
                    this.info.spriteName = "wifi2";
                }
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public override string url
        {
            get
            {
                return "UI/NetWork/NetworkView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

