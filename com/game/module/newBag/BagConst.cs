﻿namespace com.game.module.newBag
{
    using System;

    internal class BagConst
    {
        public const int COLOR_BLUE = 3;
        public const int COLOR_GREEN = 2;
        public const int COLOR_ORANGE = 5;
        public const int COLOR_PURPLE = 4;
        public const int COLOR_WRITE = 1;
        public const int COLOR_YELLOW = 6;
        public const byte ITEM_MAIN_TYPE_EQUIP = 1;
        public const byte ITEM_MAIN_TYPE_FRAG = 3;
        public const byte ITEM_MAIN_TYPE_HERO = 5;
        public const byte ITEM_MAIN_TYPE_HERO_EXP = 7;
        public const byte ITEM_MAIN_TYPE_MATERIAL = 2;
        public const byte ITEM_MAIN_TYPE_TREASURE = 4;
        public const byte ITEM_TYPE_BAG = 1;
        public const byte ITEM_TYPE_EQUIP = 2;
        public const byte ITME_MAIN_TYPE_COST = 6;
    }
}

