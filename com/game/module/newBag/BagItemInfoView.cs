﻿namespace com.game.module.newBag
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.module.Equipment.datas;
    using com.game.module.General;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class BagItemInfoView : BaseView<BagItemInfoView>
    {
        private UISprite _copperIcon;
        private UILabel _copperLabel;
        private UILabel _descLabel;
        private UISprite _icon;
        private UILabel _itemNameLabel;
        private UILabel _itemTipsLabel;
        private UISprite _line;
        private UILabel _ownNumLabel;
        private UISprite _quality;
        private Button _sellBtn;
        private Button _useBtn;
        private UILabel _useBtnLabel;
        private GameObject sellObject;
        private GameObject useObject;

        private string GetItemQualityName(SysItemsVo vo)
        {
            string format = string.Empty;
            switch (vo.color)
            {
                case 1:
                    format = "[FFFFFF]{0}[-]";
                    break;

                case 2:
                    format = "[4AD83B]{0}[-]";
                    break;

                case 3:
                    format = "[3B8FED]{0}[-]";
                    break;

                case 4:
                    format = "[B258F7]{0}[-]";
                    break;

                case 5:
                    format = "[FF7800]{0}[-]";
                    break;

                case 6:
                    format = "[FFE400]{0}[-]";
                    break;
            }
            if (!format.Equals(string.Empty))
            {
                return string.Format(format, vo.name);
            }
            return string.Empty;
        }

        protected override void HandleAfterOpenView()
        {
            if (Singleton<BagMode>.Instance.CurrentCell != null)
            {
                BagCell currentCell = Singleton<BagMode>.Instance.CurrentCell;
                PItems itemInfo = Singleton<BagMode>.Instance.CurrentCell.itemInfo;
                if (itemInfo != null)
                {
                    SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(itemInfo.templateId);
                    this.RepositionButton(dataById);
                    this._icon.atlas = currentCell.icon.atlas;
                    this._icon.spriteName = currentCell.icon.spriteName;
                    this._quality.SetActive(currentCell.quality.gameObject.activeSelf);
                    this._quality.atlas = currentCell.quality.atlas;
                    this._quality.spriteName = currentCell.quality.spriteName;
                    this._line.atlas = currentCell.background.atlas;
                    this._line.spriteName = currentCell.background.spriteName;
                    this._itemTipsLabel.text = "[fffed8]" + currentCell.GetTips() + "[-]";
                    this._descLabel.text = "[fffed8]" + dataById.desc + "[-]";
                    this._itemNameLabel.text = this.GetItemQualityName(dataById);
                    this._copperLabel.text = "[efe981]" + dataById.sell_price.ToString() + "[-]";
                    this._ownNumLabel.text = "拥有[ede981]" + itemInfo.count + "[-]件";
                }
                else
                {
                    Singleton<BagItemInfoView>.Instance.CloseView();
                }
            }
        }

        protected override void Init()
        {
            this._icon = base.FindInChild<UISprite>("itemCell/itemPic");
            this._line = base.FindInChild<UISprite>("itemCell/itemLine");
            this._quality = base.FindInChild<UISprite>("itemCell/qualityFrame");
            this._itemNameLabel = base.FindInChild<UILabel>("itemCell/itemName");
            this._ownNumLabel = base.FindInChild<UILabel>("itemCell/itemNum");
            this._itemTipsLabel = base.FindInChild<UILabel>("descs/itemTips");
            this._descLabel = base.FindInChild<UILabel>("descs/desc");
            this._copperIcon = base.FindInChild<UISprite>("descs/copperIcon");
            this._copperLabel = base.FindInChild<UILabel>("descs/copper");
            this._sellBtn = base.FindInChild<Button>("sell/sellBtn");
            this._sellBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnSellBtnClick);
            this._useBtn = base.FindInChild<Button>("use/useBtn");
            this.useObject = base.FindChild("use");
            this.sellObject = base.FindChild("sell");
            this._useBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnUseBtnClick);
            this._useBtnLabel = base.FindInChild<UILabel>("use/Label");
        }

        private void OnSellBtnClick(GameObject go)
        {
            Singleton<BagSellView>.Instance.OpenView();
        }

        private void OnUseBtnClick(GameObject go)
        {
            BagCell currentCell = Singleton<BagMode>.Instance.CurrentCell;
            SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(currentCell.itemInfo.templateId);
            ToEquipmentData data = new ToEquipmentData();
            List<GeneralTemplateInfo> templateInfoList = Singleton<GeneralMode>.Instance.templateInfoList;
            GeneralInfo generalInfo = templateInfoList[0].generalInfo;
            if (dataById.type == 4)
            {
                if (templateInfoList.Count != 0)
                {
                    data.generalInfo = generalInfo;
                    data.equipInfo = currentCell.itemInfo;
                    Singleton<EquipmentMainPanel>.Instance.OpenEquipment(data, 2);
                }
            }
            else if (dataById.type == 7)
            {
                Singleton<ExpDrugPanel>.Instance.OpenExpDrugPanel(generalInfo);
            }
            else
            {
                Singleton<BagControl>.Instance.UseItem(currentCell.itemInfo.id);
            }
        }

        private void RepositionButton(SysItemsVo vo)
        {
            Vector3 localPosition = this._sellBtn.transform.localPosition;
            float x = localPosition.x;
            float y = localPosition.y;
            float z = localPosition.z;
            Vector3 vector2 = new Vector3(x, y, z) {
                x = -78f
            };
            Vector3 vector3 = new Vector3(x, y, z) {
                x = 90f
            };
            Vector3 vector4 = new Vector3(x, y, z) {
                x = 0f
            };
            if ((vo.use_type == 0) && vo.can_sell)
            {
                this._sellBtn.transform.localPosition = vector2;
                this._useBtn.transform.localPosition = vector3;
            }
            else if ((vo.use_type == 0) && !vo.can_sell)
            {
                this._useBtn.transform.localPosition = vector4;
            }
            else if ((vo.use_type == 1) && vo.can_sell)
            {
                this._sellBtn.transform.localPosition = vector4;
            }
            this._useBtn.SetActive(vo.use_type == 0);
            this._sellBtn.SetActive(vo.can_sell);
            this.useObject.SetActive(vo.use_type == 0);
            this.sellObject.SetActive(vo.can_sell);
            if (vo.type == 4)
            {
                this._sellBtn.transform.localPosition = vector2;
                this._useBtn.transform.localPosition = vector3;
                this.useObject.SetActive(true);
                this.sellObject.SetActive(true);
                this._useBtn.SetActive(true);
                this._sellBtn.SetActive(true);
                this._useBtnLabel.text = "镶 嵌";
            }
            else
            {
                this._useBtnLabel.text = "使 用";
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }
    }
}

