﻿namespace com.game.module.newBag
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using System;
    using UnityEngine;

    public class BagSellView : BaseView<BagSellView>
    {
        private Button _addBtn;
        private Button _canelBtn;
        private UISprite _copperIcon;
        private UILabel _copperLabel;
        private uint _currentNum;
        private Button _maxBtn;
        private uint _maxNum;
        private Button _minusBtn;
        private Button _sellBtn;
        private UILabel _stripLabel;
        private OnLongClickHandle addBtnHandle;
        private bool firstOpen;
        private OnLongClickHandle subBtnHandle;

        private void AddBtnOnPress(GameObject go, bool state)
        {
            if (state)
            {
                this.addBtnHandle.Down();
            }
            else
            {
                this.addBtnHandle.Up();
            }
        }

        private uint GetStripNum()
        {
            return this._currentNum;
        }

        protected override void HandleAfterOpenView()
        {
            this.SetNumStrip();
            this.SetCopperLabel();
            if (!this.firstOpen)
            {
                this._addBtn.onPress = (UIWidgetContainer.BoolDelegate) Delegate.Combine(this._addBtn.onPress, new UIWidgetContainer.BoolDelegate(this.AddBtnOnPress));
                this._minusBtn.onPress = (UIWidgetContainer.BoolDelegate) Delegate.Combine(this._minusBtn.onPress, new UIWidgetContainer.BoolDelegate(this.SubBtnOnPress));
                this.firstOpen = true;
            }
        }

        protected override void Init()
        {
            this._sellBtn = base.FindInChild<Button>("content/sell/sellBtn");
            this._sellBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnSellBtnClick);
            this._canelBtn = base.FindInChild<Button>("content/canel/canelBtn");
            this._canelBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnCanelBtnClick);
            this._maxBtn = base.FindInChild<Button>("content/max/maxBtn");
            this._maxBtn.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._maxBtn.onClick, new UIWidgetContainer.VoidDelegate(this.OnMaxBtnClick));
            this._copperLabel = base.FindInChild<UILabel>("content/copperNum");
            this._copperIcon = base.FindInChild<UISprite>("content/labels/icon");
            this._addBtn = base.FindInChild<Button>("content/numStrip/addBtn");
            this._addBtn.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this._addBtn.onClick, new UIWidgetContainer.VoidDelegate(this.OnStripAddClick));
            this._minusBtn = base.FindInChild<Button>("content/numStrip/minusBtn");
            this._minusBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnStripMinusClick);
            this._stripLabel = base.FindInChild<UILabel>("content/numStrip/num");
            this.addBtnHandle = new OnLongClickHandle(new UIWidgetContainer.VoidDelegate(this.OnStripAddClick));
            this.subBtnHandle = new OnLongClickHandle(new UIWidgetContainer.VoidDelegate(this.OnStripMinusClick));
        }

        private void InitNumStrip()
        {
        }

        private void OnCanelBtnClick(GameObject go)
        {
            this.CloseView();
        }

        private void OnMaxBtnClick(GameObject go)
        {
            this.SetStripMax();
        }

        private void OnSellBtnClick(GameObject go)
        {
            BagCell currentCell = Singleton<BagMode>.Instance.CurrentCell;
            Singleton<BagMode>.Instance.SellingCell = currentCell;
            Singleton<BagControl>.Instance.SellItem(currentCell.itemInfo.id, this._currentNum);
            if (this._currentNum >= currentCell.realCount)
            {
                Singleton<BagMode>.Instance.SellingCell = null;
            }
            this.CloseView();
        }

        private void OnStripAddClick(GameObject go)
        {
            if (this._currentNum < this._maxNum)
            {
                this._currentNum++;
                this.SetNumStripLabel();
            }
        }

        private void OnStripMinusClick(GameObject go)
        {
            if (this._currentNum > 1)
            {
                this._currentNum--;
                this.SetNumStripLabel();
            }
        }

        private void SetCopperLabel()
        {
            BagCell currentCell = Singleton<BagMode>.Instance.CurrentCell;
            SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(currentCell.itemInfo.templateId);
            this._copperLabel.text = dataById.sell_price.ToString();
        }

        private void SetNumStrip()
        {
            uint count = 1;
            uint num2 = 1;
            count = Singleton<BagMode>.Instance.CurrentCell.itemInfo.count;
            this._maxNum = count;
            if (this._maxNum == 0)
            {
                this._currentNum = 0;
            }
            else if (num2 > count)
            {
                this._currentNum = this._maxNum;
            }
            else
            {
                this._currentNum = num2;
            }
            this.SetNumStripLabel();
        }

        private void SetNumStripLabel()
        {
            this._stripLabel.text = string.Format("{0}/{1}", this._currentNum, this._maxNum);
            BagCell currentCell = Singleton<BagMode>.Instance.CurrentCell;
            SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(currentCell.itemInfo.templateId);
            this._copperLabel.text = (dataById.sell_price * this._currentNum).ToString();
        }

        private void SetStripMax()
        {
            this._currentNum = this._maxNum;
            this.SetNumStripLabel();
        }

        private void SubBtnOnPress(GameObject go, bool state)
        {
            if (state)
            {
                this.subBtnHandle.Down();
            }
            else
            {
                this.subBtnHandle.Up();
            }
        }

        public override void Update()
        {
            base.Update();
            this.addBtnHandle.Update();
            this.subBtnHandle.Update();
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.NoneLayer;
            }
        }

        private class OnLongClickHandle
        {
            private UIWidgetContainer.VoidDelegate clickHandle;
            private float nowRuningTime;
            public int state;
            public const int STATE_DOWN = 0;
            public const int STATE_UP = 1;
            public const float waitingTime = 0.5f;

            public OnLongClickHandle(UIWidgetContainer.VoidDelegate clickHandle)
            {
                this.clickHandle = clickHandle;
                this.state = 1;
            }

            public void Down()
            {
                this.state = 0;
                this.nowRuningTime = 0f;
            }

            public void Up()
            {
                this.state = 1;
                this.nowRuningTime = 0f;
            }

            public void Update()
            {
                if (this.state == 0)
                {
                    this.nowRuningTime += Time.deltaTime;
                    if (this.nowRuningTime >= 0.5f)
                    {
                        this.clickHandle(null);
                    }
                }
            }
        }
    }
}

