﻿namespace com.game.module.newBag
{
    using com.game.consts;
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.module.Role;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BagView : BaseView<BagView>
    {
        private int _beginNum;
        private Button _btnClose;
        private GameObject _cellGo;
        private List<BagCell> _cells = new List<BagCell>();
        private UIPanel _content;
        private UIToggle _curToggle;
        private UIGrid _grid;
        private MainToolView _mainToolBarView;
        private int _showCellNum;
        private SpringPanel _spring;
        private List<UIToggle> _tabBtns = new List<UIToggle>();
        [CompilerGenerated]
        private static Comparison<PItems> <>f__am$cacheB;
        private const int EACH_SCREEN_LINES = 5;
        private const int MAX_CELL_NUM = 100;
        private const int PER_COL_NUM = 5;

        public override void CancelUpdateHandler()
        {
            Singleton<BagMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnBagUpdate);
        }

        private BagCell CloneCell()
        {
            if (null == this._cellGo)
            {
                this._cellGo = base.FindChild("itemPanel/items/Grid/item");
            }
            GameObject go = NGUITools.AddChild(this._grid.gameObject, this._cellGo);
            BagCell item = go.AddMissingComponent<BagCell>();
            item.onClick = new UIWidgetContainer.VoidDelegate(this.OnCellClick);
            this._cells.Add(item);
            go.GetComponent<UIToggle>().optionCanBeNone = true;
            go.name = this.TurnToCellName(this._cells.Count);
            return item;
        }

        private void CloseItemInfoView()
        {
            Singleton<BagItemInfoView>.Instance.CloseView();
        }

        private void DestroyHandleCells(int beginNum)
        {
            List<BagCell> list = new List<BagCell>();
            for (int i = beginNum; i < this._cells.Count; i++)
            {
                list.Add(this._cells[i]);
            }
            foreach (BagCell cell in list)
            {
                this._cells.Remove(cell);
                NGUITools.Destroy(cell.gameObject);
            }
        }

        private void GotoTop(float distance)
        {
            Vector3 localPosition = this._content.transform.localPosition;
            localPosition.y += distance;
            this._content.transform.localPosition = localPosition;
            Vector2 clipOffset = this._content.clipOffset;
            clipOffset.y -= distance;
            this._content.clipOffset = clipOffset;
            Vector4 clipRange = this._content.clipRange;
            clipRange.x = 85f;
            clipRange.y = -20f;
            this._content.clipRange = clipRange;
        }

        protected override void HandleAfterOpenView()
        {
            this._mainToolBarView.isActive = true;
            Singleton<BagSellView>.Instance.CloseView();
            this._tabBtns[0].value = true;
            Singleton<BagMode>.Instance.TagType = 0;
            this.UpdateItemsView();
        }

        protected override void HandleBeforeCloseView()
        {
            this._mainToolBarView.isActive = false;
        }

        protected override void Init()
        {
            this._mainToolBarView = new MainToolView(base.FindChild("itemPanel/toolbar"));
            Singleton<BagSellView>.Instance.gameObject = base.FindChild("sellPanel");
            Singleton<BagItemInfoView>.Instance.gameObject = base.FindChild("itemPanel/itemInfo");
            string[] strArray = new string[] { "itemPanel/TabBar/tab_buttons/Grid/all", "itemPanel/TabBar/tab_buttons/Grid/equip", "itemPanel/TabBar/tab_buttons/Grid/material", "itemPanel/TabBar/tab_buttons/Grid/fragment", "itemPanel/TabBar/tab_buttons/Grid/treasure", "itemPanel/TabBar/tab_buttons/Grid/cost" };
            for (int i = 0; i < strArray.Length; i++)
            {
                this._tabBtns.Add(base.FindInChild<UIToggle>(strArray[i]));
                EventDelegate.Add(this._tabBtns[i].onChange, new EventDelegate.Callback(this.OnTabChange));
            }
            this._tabBtns[0].SetActive(true);
            this._content = base.FindInChild<UIPanel>("itemPanel/items");
            this._spring = base.FindInChild<SpringPanel>("itemPanel/items");
            this._grid = base.FindInChild<UIGrid>("itemPanel/items/Grid");
            this._btnClose = base.FindInChild<Button>("itemPanel/closeBtn");
            this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickClose);
            Singleton<BagSellView>.Instance.CloseView();
            this.UpdateItemsView();
        }

        private void OnBagUpdate(object sender, int code)
        {
            if (code == BagMode.UPDATE_ITEM)
            {
                this.UpdateItemsView();
            }
        }

        private void OnCellClick(GameObject go)
        {
            go.GetComponent<UIToggle>().value = true;
            BagCell component = go.GetComponent<BagCell>();
            if (Singleton<BagMode>.Instance.LastSelectCell != null)
            {
                this.UpdateBagcellState(Singleton<BagMode>.Instance.LastSelectCell, false);
            }
            Singleton<BagMode>.Instance.LastSelectCell = Singleton<BagMode>.Instance.CurrentCell;
            Singleton<BagMode>.Instance.CurrentCell = component;
            this.UpdateBagcellState(component, true);
            this.OpenItemInfoView();
        }

        private void OnClickClose(GameObject go)
        {
            this.CloseItemInfoView();
            this.CloseView();
        }

        private void onClickHandler(GameObject go)
        {
            Singleton<BuyVigourView>.Instance.Show();
        }

        private void OnTabChange()
        {
            Singleton<BagMode>.Instance.SellingCell = null;
            UIToggle current = UIToggle.current;
            if (current.value && (current != this._curToggle))
            {
                if (this._curToggle != null)
                {
                    this.showLineState(this._curToggle.gameObject, false);
                }
                this._curToggle = current;
                this.showLineState(current.gameObject, true);
                Singleton<BagMode>.Instance.TagType = (uint) this._tabBtns.IndexOf(current);
                this.GotoTop(-this._content.transform.localPosition.y);
                this._beginNum = 0;
                this.UpdateItemsView();
            }
        }

        private void OpenItemInfoView()
        {
            Singleton<BagItemInfoView>.Instance.OpenView();
        }

        private void RefreshCacheItems()
        {
            int num = 0;
            num = (100 / this._grid.maxPerLine) - 5;
            if (this._content.transform.localPosition.y <= 20f)
            {
                if (this._beginNum == 0)
                {
                    return;
                }
                this._beginNum -= num;
                this.GotoTop(num * this._grid.cellHeight);
            }
            else
            {
                if (this._showCellNum < 100)
                {
                    return;
                }
                int num2 = 0;
                foreach (PItems items in Singleton<BagMode>.Instance.ItemDict.Values)
                {
                    SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(items.templateId);
                    if ((Singleton<BagMode>.Instance.TagType == 0) || (dataById.tagtype == Singleton<BagMode>.Instance.TagType))
                    {
                        num2++;
                    }
                }
                if ((this._showCellNum + (this._beginNum * this._grid.maxPerLine)) == num2)
                {
                    return;
                }
                this._beginNum += num;
                this.GotoTop(-num * this._grid.cellHeight);
            }
            this.UpdateItemsView();
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<BagMode>.Instance.dataUpdated += new DataUpateHandler(this.OnBagUpdate);
            Singleton<BagControl>.Instance.RequestItemInfo(1);
            this._spring.onFinished = (SpringPanel.OnFinished) Delegate.Combine(this._spring.onFinished, new SpringPanel.OnFinished(this.RefreshCacheItems));
        }

        private void ResetContentPosition(Transform target, int showNum)
        {
            int num = ((showNum - 1) / this._grid.maxPerLine) + 1;
            if ((num * this._grid.cellHeight) < target.localPosition.y)
            {
                Vector3 localPosition = target.localPosition;
                Vector3 vector2 = new Vector3(localPosition.x, 0f, localPosition.z);
                Vector2 vector3 = new Vector2(this._content.clipOffset.x, -10f);
                this._content.clipOffset = vector3;
                target.localPosition = vector2;
                this._beginNum = 0;
                this.UpdateItemsView();
            }
        }

        private void showLineState(GameObject TabBarObject, bool state)
        {
            NGUITools.FindInChild<UISprite>(TabBarObject, "xian").SetActive(state);
            NGUITools.FindInChild<UILabel>(TabBarObject, "Label").color = !state ? ColorConst.FontColor_687687 : ColorConst.FontColor_DFDFC5;
        }

        private List<PItems> SortItems(SortedDictionary<ulong, PItems>.ValueCollection items)
        {
            List<PItems> list = new List<PItems>();
            foreach (PItems items2 in items)
            {
                list.Add(items2);
            }
            if (<>f__am$cacheB == null)
            {
                <>f__am$cacheB = delegate (PItems left, PItems right) {
                    if (left.templateId == right.templateId)
                    {
                        if (left.id < right.id)
                        {
                            return -1;
                        }
                        return 1;
                    }
                    if (left.templateId < right.templateId)
                    {
                        return -1;
                    }
                    return 1;
                };
            }
            list.Sort(<>f__am$cacheB);
            return list;
        }

        private string TurnToCellName(int count)
        {
            int num = count / 0x3e8;
            int num2 = count / 100;
            int num3 = count / 10;
            int num4 = count % 10;
            return (num.ToString() + num2.ToString() + num3.ToString() + num4.ToString());
        }

        public override void Update()
        {
            this._spring.onFinished = new SpringPanel.OnFinished(this.RefreshCacheItems);
        }

        private void UpdateBagcellState(BagCell cell, bool state)
        {
            NGUITools.FindInChild<UISprite>(cell.gameObject, "selectFrame").SetActive(state);
        }

        private void UpdateItemsView()
        {
            SortedDictionary<ulong, PItems> itemDict = Singleton<BagMode>.Instance.ItemDict;
            List<PItems> list = this.SortItems(itemDict.Values);
            int count = itemDict.Count;
            int num2 = 0;
            if (Singleton<BagMode>.Instance.CurrentCell != null)
            {
                Singleton<BagMode>.Instance.CurrentCell.gameObject.GetComponent<UIToggle>().value = false;
            }
            int num3 = 0;
            foreach (PItems items in list)
            {
                if ((num3 >= (this._grid.maxPerLine * this._beginNum)) && (num3 < ((this._grid.maxPerLine * this._beginNum) + 100)))
                {
                    int num4 = (int) items.count;
                    while (num4 > 0)
                    {
                        int num5 = 0;
                        SysItemsVo dataById = BaseDataMgr.instance.GetDataById<SysItemsVo>(items.templateId);
                        if (dataById.can_superpose)
                        {
                            num5 = num4;
                            num4 = 0;
                        }
                        else
                        {
                            num4--;
                            num5 = 1;
                        }
                        if (num2 >= this._cells.Count)
                        {
                            BagCell cell = this.CloneCell();
                        }
                        if ((Singleton<BagMode>.Instance.TagType == 0) || (dataById.tagtype == Singleton<BagMode>.Instance.TagType))
                        {
                            this._cells[num2].SetActive(true);
                            this._cells[num2].itemInfo = items;
                            this._cells[num2].realCount = num5;
                            this._cells[num2].UpdateCell();
                            num2++;
                        }
                    }
                }
                num3++;
            }
            this._showCellNum = num2;
            while (num2 < this._cells.Count)
            {
                this._cells[num2].SetActive(false);
                num2++;
            }
            if (this._showCellNum >= 1)
            {
                Singleton<BagMode>.Instance.CurrentCell = this._cells[0];
                this.UpdateBagcellState(Singleton<BagMode>.Instance.CurrentCell, true);
                Singleton<BagMode>.Instance.CurrentCell.gameObject.GetComponent<UIToggle>().value = true;
                if (Singleton<BagMode>.Instance.SellingCell != null)
                {
                    Singleton<BagMode>.Instance.CurrentCell = Singleton<BagMode>.Instance.SellingCell;
                    Singleton<BagMode>.Instance.SellingCell.gameObject.GetComponent<UIToggle>().value = true;
                }
                else
                {
                    Singleton<BagMode>.Instance.CurrentCell.gameObject.GetComponent<UIToggle>().value = true;
                }
                Singleton<BagItemInfoView>.Instance.CloseView();
                Singleton<BagItemInfoView>.Instance.OpenView();
            }
            else
            {
                Singleton<BagMode>.Instance.CurrentCell = null;
                Singleton<BagItemInfoView>.Instance.CloseView();
            }
            this._grid.Reposition();
            Transform parent = this._grid.transform.parent;
            this.ResetContentPosition(parent, this._showCellNum);
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Bag/BagView.assetbundle";
            }
        }
    }
}

