﻿namespace com.game.module.rolecreate
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.game.utils;
    using com.u3d.bases.debug;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Text;
    using UnityEngine;

    internal class RoleCreateView : BaseView<RoleCreateView>
    {
        private string[] _familyName;
        private string[] _manDoubleName;
        private string[] _manSingleName;
        private string _serverInfo = string.Empty;
        private string[] _specialName;
        private string[] _womanDoubleName;
        private string[] _womanSingleName;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map23;
        private UIWidgetContainer btn_back;
        private Button btn_random;
        private UIWidgetContainer btn_start;
        private string[] doubleFamilys;
        private string[] firstNames;
        private UIInput input;
        private readonly int JOB = 1;
        private string[][] laynameLayout;
        private SpinWithMouse modelSpin;
        private readonly int NAME_LEN_MAX = 14;
        private readonly int NAME_LEN_MIN = 4;
        private const string RANDOM_NAME_URL = "Xml/RandomName.assetbundle";
        private UILabel serverInfo;
        private readonly int SEX = 1;
        private string singleFamilys;

        public RoleCreateView()
        {
            string[][] textArrayArray1 = new string[8][];
            textArrayArray1[0] = new string[] { "s", "s", "d" };
            textArrayArray1[1] = new string[] { "s", "t", "d" };
            textArrayArray1[2] = new string[] { "d", "s", "s" };
            textArrayArray1[3] = new string[] { "t", "s", "s", "d" };
            textArrayArray1[4] = new string[] { "s", "s", "t", "d" };
            textArrayArray1[5] = new string[] { "t", "s", "d", "t" };
            textArrayArray1[6] = new string[] { "d", "t", "s" };
            textArrayArray1[7] = new string[] { "t", "s", "s", "d", "t" };
            this.laynameLayout = textArrayArray1;
        }

        private void BackOnClick(GameObject go)
        {
            base.CloseView();
            Singleton<LoginPanel>.Instance.OpenView();
        }

        public override void CloseView()
        {
            if (!object.ReferenceEquals(this.gameObject, null))
            {
                base.FindChild("btn_random").SetActive(false);
                base.FindChild("btn_start").SetActive(false);
                base.FindChild("inp_name").SetActive(false);
                Singleton<LoginControl>.Instance.ChangeToFirstScene();
                base.CloseView();
            }
        }

        [DebuggerHidden]
        private IEnumerator DelayClose()
        {
            return new <DelayClose>c__Iterator3B { <>f__this = this };
        }

        private int GetByteLength(string roleName)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(roleName);
            int num = 0;
            for (int i = 0; i < bytes.GetLength(0); i++)
            {
                if ((i % 2) == 0)
                {
                    num++;
                }
                else if (bytes[i] > 0)
                {
                    num++;
                }
            }
            return num;
        }

        public string getRandomName()
        {
            string[] strArray;
            string[] strArray2;
            if ((Time.realtimeSinceStartup % 2f) > 0f)
            {
                strArray = this._manSingleName;
                strArray2 = this._manDoubleName;
            }
            else
            {
                strArray = this._womanSingleName;
                strArray2 = this._womanDoubleName;
            }
            if ((strArray == null) || (strArray2 == null))
            {
                return string.Empty;
            }
            System.Random random = new System.Random();
            string[] strArray3 = this.laynameLayout[random.Next(0, this.laynameLayout.Length - 1)];
            string str = string.Empty;
            foreach (string str2 in strArray3)
            {
                string key = str2;
                if (key != null)
                {
                    int num2;
                    if (<>f__switch$map23 == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                        dictionary.Add("s", 0);
                        dictionary.Add("d", 1);
                        dictionary.Add("t", 2);
                        <>f__switch$map23 = dictionary;
                    }
                    if (<>f__switch$map23.TryGetValue(key, out num2))
                    {
                        switch (num2)
                        {
                            case 0:
                                str = str + strArray[random.Next(0, strArray.Length - 1)];
                                break;

                            case 1:
                                str = str + strArray2[random.Next(0, strArray2.Length - 1)];
                                break;

                            case 2:
                                str = str + this._specialName[random.Next(0, this._specialName.Length - 1)];
                                break;
                        }
                    }
                }
            }
            return str;
        }

        protected override void HandleAfterOpenView()
        {
            this.gameObject.SetActive(false);
            this.RandomName(null);
            base.HandleAfterOpenView();
        }

        protected override void Init()
        {
            this.btn_start = Tools.find(this.gameObject, "btn_start").GetComponent<UIWidgetContainer>();
            this.btn_start.FindInChild<UILabel>("label").text = LanguageManager.GetWord("RoleCreate.BtnStart");
            this.btn_random = Tools.find(this.gameObject, "btn_random").GetComponent<Button>();
            this.btn_back = Tools.find(this.gameObject, "btn_back").GetComponent<Button>();
            this.input = Tools.find(this.gameObject, "inp_name").GetComponent<UIInput>();
            this.serverInfo = Tools.find(this.gameObject, "serverInfo").GetComponent<UILabel>();
            this.modelSpin = NGUITools.FindInChild<SpinWithMouse>(this.gameObject, "mode");
            this.btn_random.onClick = (UIWidgetContainer.VoidDelegate) Delegate.Combine(this.btn_random.onClick, new UIWidgetContainer.VoidDelegate(this.RandomName));
            this.btn_start.onClick = new UIWidgetContainer.VoidDelegate(this.StartGame);
            this.btn_back.onClick = new UIWidgetContainer.VoidDelegate(this.BackOnClick);
            this.input.characterLimit = this.NAME_LEN_MAX;
            this.gameObject.SetActive(false);
            if (this._serverInfo != string.Empty)
            {
                this.SetServer(this._serverInfo);
            }
            this.LoadRandomNameData();
        }

        private void LoadRandomNameCallback(TextAsset nameText)
        {
            XMLNode node = XMLParser.Parse(nameText.ToString());
            string str2 = node.GetValue("root>0>special>0>_text");
            char[] separator = new char[] { ',' };
            this._specialName = str2.Split(separator);
            string str3 = node.GetValue("root>0>man>0>single>0>_text");
            char[] chArray2 = new char[] { ',' };
            this._manSingleName = str3.Split(chArray2);
            string str4 = node.GetValue("root>0>man>0>double>0>_text");
            char[] chArray3 = new char[] { ',' };
            this._manDoubleName = str4.Split(chArray3);
            string str5 = node.GetValue("root>0>women>0>single>0>_text");
            char[] chArray4 = new char[] { ',' };
            this._womanSingleName = str5.Split(chArray4);
            string str6 = node.GetValue("root>0>women>0>double>0>_text");
            char[] chArray5 = new char[] { ',' };
            this._womanDoubleName = str6.Split(chArray5);
            this.RandomName(null);
            this.gameObject.SetActive(true);
        }

        private void LoadRandomNameData()
        {
            AssetManager.Instance.LoadAsset<TextAsset>("Xml/RandomName.assetbundle", new LoadAssetFinish<TextAsset>(this.LoadRandomNameCallback), null, false, true);
        }

        private void RandomName(GameObject go)
        {
            this.input.value = this.getRandomName();
        }

        public void SetServer(string stringInfo)
        {
            this._serverInfo = stringInfo;
            if (null != this.serverInfo)
            {
                this.serverInfo.text = this._serverInfo;
            }
        }

        private void StartGame(GameObject go)
        {
            string roleName = this.input.value.Replace(" ", string.Empty);
            this.input.label.text = roleName;
            com.u3d.bases.debug.Log.debug(this, "角色名字： " + roleName);
            int byteLength = this.GetByteLength(roleName);
            UnityEngine.Debug.Log("字节数: " + byteLength);
            if (StringUtils.isEmpty(roleName) || roleName.Equals("请输入角色名称"))
            {
                this.input.label.text = "请输入角色名称";
            }
            else if ((byteLength < this.NAME_LEN_MIN) || (byteLength > this.NAME_LEN_MAX))
            {
                MessageManager.Show("战队名字为4-14个字符（2-7个汉字）哦~~, 当前为" + byteLength.ToString());
            }
            else
            {
                Singleton<RoleMode>.Instance.roleName = roleName;
                Singleton<LoginMode>.Instance.createRole(this.JOB, this.SEX, roleName, 0x3ee);
            }
        }

        private void test(GameObject go)
        {
            UnityEngine.Debug.Log("go.name = " + go.name);
        }

        public override bool isDestroy
        {
            get
            {
                return true;
            }
        }

        public override bool isUnloadDelay
        {
            get
            {
                return true;
            }
        }

        public override bool playClosedSound
        {
            get
            {
                return false;
            }
        }

        public string serverName { get; set; }

        public override string url
        {
            get
            {
                return "UI/RoleCreate/RoleCreateView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }

        [CompilerGenerated]
        private sealed class <DelayClose>c__Iterator3B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal RoleCreateView <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(0f);
                        this.$PC = 1;
                        return true;

                    case 1:
                        Singleton<LoginControl>.Instance.ChangeToFirstScene();
                        CoroutineManager.StopAllCoroutine();
                        this.<>f__this.CloseView();
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

