﻿namespace com.game.module.shop
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using com.game.Public.Message;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShopBuyView : BaseView<ShopBuyView>
    {
        private Button _bgBtn;
        private List<Button> _buttons = new List<Button>();
        private UILabel _buyCount;
        private SysItemVoCell _cell;
        private UILabel _cost;
        private UILabel _desp;
        private UISprite _icon;
        private PShopItem _info;
        private SysItemsVo _itemSysVo;
        private int _lastPressFC;
        private UILabel _name;
        private uint _numBuy = 1;
        private int _numOwn;
        private UILabel _ownCount;
        private int _PressAction;
        private SysShopItemVo _shopItemSysVo;

        public override void CancelUpdateHandler()
        {
            foreach (Button button in this._buttons)
            {
                button.onClick = null;
            }
            this._buttons[0].onPress = (UIWidgetContainer.BoolDelegate) (this._buttons[1].onPress = null);
            this._bgBtn.onClick = null;
        }

        protected override void HandleAfterOpenView()
        {
            this._PressAction = -1;
            this.UpdateView();
        }

        protected override void HandleBeforeCloseView()
        {
            this._numOwn = 0;
            this._numBuy = 1;
        }

        protected override void Init()
        {
            this._cell = base.FindChild("item").AddMissingComponent<SysItemVoCell>();
            this._name = base.FindInChild<UILabel>("name");
            this._ownCount = base.FindInChild<UILabel>("count");
            this._desp = base.FindInChild<UILabel>("desp");
            this._buyCount = base.FindInChild<UILabel>("buyCount/label");
            this._cost = base.FindInChild<UILabel>("cost/costLabel");
            this._icon = base.FindInChild<UISprite>("cost/icon");
            string[] strArray = new string[] { "btnDown", "btnUp", "btnMax", "btnBuy", "btnCancel" };
            foreach (string str in strArray)
            {
                Button item = base.FindInChild<Button>(str);
                this._buttons.Add(item);
            }
            this._bgBtn = base.FindInChild<Button>("bgBtn");
        }

        private void OnClick(GameObject go)
        {
            <OnClick>c__AnonStoreyEB yeb = new <OnClick>c__AnonStoreyEB {
                go = go
            };
            switch (this._buttons.FindIndex(new Predicate<Button>(yeb.<>m__B8)))
            {
                case 0:
                    this.OnPress(false);
                    return;

                case 1:
                    this.OnPress(true);
                    return;

                case 2:
                    if (this._info.amount >= 0)
                    {
                        this._numBuy = (this._info.amount <= 0) ? 1 : ((uint) this._info.amount);
                        break;
                    }
                    this._numBuy = 0x3e7;
                    break;

                case 3:
                    this.OnClickBuy();
                    return;

                case 4:
                    this.CloseView();
                    return;

                default:
                    return;
            }
            this.UpdateView();
        }

        private void OnClickBg(GameObject go)
        {
            this.CloseView();
        }

        private void OnClickBuy()
        {
            uint num = this._numBuy * this._shopItemSysVo.price;
            if (ShopMode.Currency.GetOwn(this._shopItemSysVo.pay_type) < num)
            {
                ShopMode.Currency currency = ShopMode.Currency.GetCurrency(this._shopItemSysVo.pay_type);
                MessageManager.Show(LanguageManager.GetWord("Game.CurrencyNotEnough", currency.name));
            }
            else
            {
                Singleton<ShopMode>.Instance.ReqBuyShopItem((byte) this._shopItemSysVo.shop_type_id, this._info.id, this._numBuy);
            }
        }

        private void OnPress(bool add)
        {
            this._lastPressFC = Time.frameCount;
            if (add)
            {
                if ((this._info.amount > 0) && (this._numBuy >= this._info.amount))
                {
                    return;
                }
                this._numBuy++;
            }
            else
            {
                if (this._numBuy <= 1)
                {
                    return;
                }
                this._numBuy--;
            }
            this.UpdateView();
        }

        private void OnPress(GameObject go, bool state)
        {
            <OnPress>c__AnonStoreyEC yec = new <OnPress>c__AnonStoreyEC {
                go = go
            };
            if (!state)
            {
                this._PressAction = -1;
            }
            else
            {
                this._PressAction = this._buttons.FindIndex(new Predicate<Button>(yec.<>m__B9));
                this._lastPressFC = Time.frameCount;
            }
        }

        public override void RegisterUpdateHandler()
        {
            foreach (Button button in this._buttons)
            {
                button.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
            }
            this._buttons[0].onPress = this._buttons[1].onPress = new UIWidgetContainer.BoolDelegate(this.OnPress);
            this._bgBtn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickBg);
        }

        public void Show(PShopItem info)
        {
            this._info = info;
            this.OpenView();
        }

        public override void Update()
        {
            if ((Time.frameCount - this._lastPressFC) >= 8)
            {
                switch (this._PressAction)
                {
                    case 0:
                        this.OnPress(false);
                        break;

                    case 1:
                        this.OnPress(true);
                        break;
                }
            }
        }

        private void UpdateView()
        {
            this._shopItemSysVo = BaseDataMgr.instance.GetShopItem(this._info.shopId);
            this._itemSysVo = BaseDataMgr.instance.getGoodsVo(this._shopItemSysVo.item_id);
            this._cell.sysVoId = (int) this._shopItemSysVo.item_id;
            this._name.text = this._itemSysVo.name;
            this._desp.text = this._itemSysVo.desc;
            this._ownCount.text = this._numOwn.ToString();
            this._buyCount.text = this._numBuy.ToString();
            this._cost.text = (this._numBuy * this._shopItemSysVo.price).ToString();
            ShopMode.Currency currency = ShopMode.Currency.GetCurrency(this._shopItemSysVo.pay_type);
            this._icon.spriteName = currency.iconName;
            this._cost.color = ((this._numBuy * this._shopItemSysVo.price) <= ShopMode.Currency.GetOwn(currency.type)) ? Color.white : Color.red;
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        [CompilerGenerated]
        private sealed class <OnClick>c__AnonStoreyEB
        {
            internal GameObject go;

            internal bool <>m__B8(Button btn)
            {
                return (btn.gameObject == this.go);
            }
        }

        [CompilerGenerated]
        private sealed class <OnPress>c__AnonStoreyEC
        {
            internal GameObject go;

            internal bool <>m__B9(Button btn)
            {
                return (btn.gameObject == this.go);
            }
        }
    }
}

