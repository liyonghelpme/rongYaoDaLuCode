﻿namespace com.game.module.shop
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class ShopControl : BaseControl<ShopControl>
    {
        protected override void NetListener()
        {
            AppNet.main.addCMD("3841", new NetMsgCallback(this.OnInitShop));
            AppNet.main.addCMD("3842", new NetMsgCallback(this.OnBuy));
            AppNet.main.addCMD("3843", new NetMsgCallback(this.OnRefreshShop));
        }

        private void OnBuy(INetData receiveData)
        {
            ShopBuyMsg_15_2 g__ = new ShopBuyMsg_15_2();
            g__.read(receiveData.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
            else
            {
                MessageManager.Show(LanguageManager.GetWord("Shop.SucToBuyItem"));
                Singleton<ShopMode>.Instance.Update(g__.shopId, g__.amount);
            }
        }

        private void OnInitShop(INetData receiveData)
        {
            ShopInfoMsg_15_1 g__ = new ShopInfoMsg_15_1();
            g__.read(receiveData.GetMemoryStream());
            Singleton<ShopMode>.Instance.Reset(g__.shopList);
        }

        private void OnRefreshShop(INetData receiveData)
        {
            ShopRefreshMsg_15_3 g__ = new ShopRefreshMsg_15_3();
            g__.read(receiveData.GetMemoryStream());
            if (g__.code != 0)
            {
                ErrorCodeManager.ShowError(g__.code);
            }
        }
    }
}

