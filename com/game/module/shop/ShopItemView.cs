﻿namespace com.game.module.shop
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.bag;
    using com.game.module.core;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class ShopItemView
    {
        private Button _btn;
        private GameObject _go;
        private GameObject _goodsGo;
        private UISprite _icon;
        private PShopItem _info;
        private SysItemVoCell _itemCell;
        private UILabel _nameLabel;
        private UILabel _priceLabel;
        private SysShopItemVo _shopSysVo;

        public ShopItemView(GameObject go)
        {
            this._go = go;
            this._goodsGo = NGUITools.FindChild(this._go, "item");
            this._btn = this._go.GetComponent<Button>();
            this._itemCell = this._goodsGo.AddMissingComponent<SysItemVoCell>();
            this._nameLabel = NGUITools.FindInChild<UILabel>(this._go, "name");
            this._priceLabel = NGUITools.FindInChild<UILabel>(this._go, "price/label");
            this._icon = NGUITools.FindInChild<UISprite>(this._go, "price/icon");
        }

        private void OnClick(GameObject go)
        {
            Singleton<ShopBuyView>.Instance.Show(this._info);
        }

        public void Update(PShopItem info)
        {
            this._info = info;
            if (this._info == null)
            {
                this._shopSysVo = null;
                this._go.SetActive(false);
                this._btn.onClick = null;
            }
            else
            {
                this._btn.onClick = new UIWidgetContainer.VoidDelegate(this.OnClick);
                this._go.SetActive(true);
                this._shopSysVo = BaseDataMgr.instance.GetShopItem(info.shopId);
                this._itemCell.sysVoId = (int) this._shopSysVo.item_id;
                this._itemCell.count = info.amount;
                if (this._info.amount == 0)
                {
                    if (!this._itemCell.icon.isShowAsGray)
                    {
                        this._itemCell.icon.ShowAsGray();
                    }
                }
                else if (this._itemCell.icon.isShowAsGray)
                {
                    this._itemCell.icon.ShowAsMyself();
                }
                this._itemCell.alwaysShowNum = true;
                this._nameLabel.text = this._itemCell.itemVo.name;
                this._priceLabel.text = this._shopSysVo.price.ToString();
                ShopMode.Currency currency = ShopMode.Currency.GetCurrency(this._shopSysVo.pay_type);
                this._icon.spriteName = currency.iconName;
            }
        }

        public GameObject go
        {
            get
            {
                return this._go;
            }
        }

        public PShopItem info
        {
            get
            {
                return this._info;
            }
        }

        public bool isActive
        {
            get
            {
                return this._go.activeSelf;
            }
        }
    }
}

