﻿namespace com.game.module.shop
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.vo;
    using PCustomDataType;
    using Proto;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.InteropServices;

    public class ShopMode : BaseMode<ShopMode>
    {
        private Dictionary<int, PShopList> _shopDic;

        private int Compare(PShopItem a, PShopItem b)
        {
            if (a.isFixed == b.isFixed)
            {
                return (int) (a.id - b.id);
            }
            return (int) (b.isFixed - a.isFixed);
        }

        public PShopItem GetItem(ulong id)
        {
            foreach (PShopList list in this._shopDic.Values)
            {
                foreach (PShopItem item in list.itemList)
                {
                    if (item.id == id)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public List<PShopItem> GetItemByPage(byte type, int page, int pageSize, Func<PShopItem, bool> filter = null)
        {
            List<PShopItem> items = this.GetItems(type);
            if (items == null)
            {
                return null;
            }
            if (page <= 0)
            {
                return items;
            }
            List<PShopItem> list2 = new List<PShopItem>();
            int num = page * pageSize;
            int num2 = (page + 1) * pageSize;
            int num3 = Math.Min(num2, items.Count);
            for (int i = num; i < num3; i++)
            {
                if ((filter == null) || filter(items[i]))
                {
                    list2.Add(items[i]);
                }
            }
            return list2;
        }

        public List<PShopItem> GetItems(byte type)
        {
            return (!((this._shopDic != null) && this._shopDic.ContainsKey(type)) ? null : this._shopDic[type].itemList);
        }

        public SysShopRefreshVo GetRefreshVo(byte type)
        {
            SysShopVo shop = BaseDataMgr.instance.GetShop(type);
            if ((shop == null) || (shop.manualUpdate == 0))
            {
                return null;
            }
            int count = Math.Min(this.GetShop(type).refreshCount + 1, shop.payMax);
            return BaseDataMgr.instance.GetShopRefresh(type, count);
        }

        public PShopList GetShop(int id)
        {
            return (((this._shopDic == null) || !this._shopDic.ContainsKey(id)) ? null : this._shopDic[id]);
        }

        public void ReqBuyShopItem(byte type, ulong shopItemId, uint count)
        {
            MemoryStream msdata = new MemoryStream();
            Module_15.write_15_2(msdata, type, shopItemId, count);
            AppNet.gameNet.send(msdata, 15, 2);
        }

        public void ReqRefresh(byte type)
        {
            MemoryStream msdata = new MemoryStream();
            Module_15.write_15_3(msdata, type);
            AppNet.gameNet.send(msdata, 15, 3);
        }

        public void ReqShopInfo(byte type = 0)
        {
            MemoryStream msdata = new MemoryStream();
            Module_15.write_15_1(msdata, type);
            AppNet.gameNet.send(msdata, 15, 1);
        }

        public void Reset(List<PShopList> itemList)
        {
            if (this._shopDic == null)
            {
                this._shopDic = new Dictionary<int, PShopList>();
            }
            foreach (PShopList list in itemList)
            {
                list.itemList.Sort(new Comparison<PShopItem>(this.Compare));
                if (!this._shopDic.ContainsKey(list.type))
                {
                    this._shopDic.Add(list.type, list);
                }
                else
                {
                    this._shopDic[list.type] = list;
                }
            }
            base.DataUpdateWithParam(1, null);
        }

        public void Update(ulong id, int amount)
        {
            foreach (PShopList list in this._shopDic.Values)
            {
                for (int i = list.itemList.Count - 1; i >= 0; i--)
                {
                    if (list.itemList[i].id == id)
                    {
                        list.itemList[i].amount = amount;
                        base.DataUpdateWithParam(2, list.itemList[i]);
                        break;
                    }
                }
            }
        }

        public class Currency
        {
            private static Dictionary<int, ShopMode.Currency> _dic;
            public const int BIND_GOLD = 4;
            public const int BIND_STONE = 2;
            public const int GOLD = 3;
            public string iconName;
            public string name;
            public const int STONE = 1;
            public int type;

            public static ShopMode.Currency GetCurrency(int type)
            {
                if (_dic == null)
                {
                    _dic = new Dictionary<int, ShopMode.Currency>();
                    ShopMode.Currency currency2 = new ShopMode.Currency {
                        type = 1,
                        name = "钻石",
                        iconName = "zuanshi"
                    };
                    ShopMode.Currency currency = currency2;
                    _dic.Add(currency.type, currency);
                    currency2 = new ShopMode.Currency {
                        type = 2,
                        name = "绑定钻石",
                        iconName = string.Empty
                    };
                    currency = currency2;
                    _dic.Add(currency.type, currency);
                    currency2 = new ShopMode.Currency {
                        type = 3,
                        name = "金币",
                        iconName = "jinbi"
                    };
                    currency = currency2;
                    _dic.Add(currency.type, currency);
                    currency2 = new ShopMode.Currency {
                        type = 4,
                        name = "绑定金币",
                        iconName = string.Empty
                    };
                    currency = currency2;
                    _dic.Add(currency.type, currency);
                }
                if (_dic.ContainsKey(type))
                {
                    return _dic[type];
                }
                return null;
            }

            public static uint GetOwn(int type)
            {
                uint num = 0;
                switch (type)
                {
                    case 1:
                        return MeVo.instance.diamond;

                    case 2:
                        return num;

                    case 3:
                        return MeVo.instance.diam;
                }
                return num;
            }

            public static ShopMode.Currency gold
            {
                get
                {
                    return GetCurrency(3);
                }
            }

            public static ShopMode.Currency stone
            {
                get
                {
                    return GetCurrency(1);
                }
            }
        }

        public class ShopEvent
        {
            public const int BUY = 2;
            public const int RESET = 1;
        }
    }
}

