﻿namespace com.game.module.shop
{
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class ShopTabView
    {
        private GameObject _go;
        private UIGrid _grid;
        private GameObject _itemGo;
        private List<ShopItemView> _itemList;

        public ShopTabView(GameObject go)
        {
            this._go = go;
            this._itemGo = NGUITools.FindChild(this._go, "shopItem");
            this._grid = this._go.GetComponent<UIGrid>();
            ShopItemView item = new ShopItemView(this._itemGo);
            item.Update(null);
            this._itemList = new List<ShopItemView>();
            this._itemList.Add(item);
        }

        public void Update(PShopItem item)
        {
            ulong id = item.id;
            foreach (ShopItemView view in this._itemList)
            {
                if (view.info.id == item.id)
                {
                    if (view.isActive)
                    {
                        view.Update(item);
                    }
                    break;
                }
            }
        }

        public void Update(List<PShopItem> items)
        {
            if ((items == null) || (items.Count <= 0))
            {
                this._go.SetActive(false);
            }
            else
            {
                this._go.SetActive(true);
                while (this._itemList.Count < items.Count)
                {
                    ShopItemView view = new ShopItemView(NGUITools.AddChild(this._go, this._itemGo));
                    this._itemList.Add(view);
                }
                int num = 0;
                foreach (PShopItem item in items)
                {
                    this._itemList[num].Update(item);
                    num++;
                }
                while (num < this._itemList.Count)
                {
                    this._itemList[num].Update(null);
                    num++;
                }
                this._grid.Reposition();
            }
        }
    }
}

