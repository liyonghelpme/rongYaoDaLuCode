﻿namespace com.game.module.shop
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.SystemData;
    using com.game.Public.Confirm;
    using com.game.Public.Message;
    using com.game.vo;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShopView : BaseView<ShopView>
    {
        private UISprite _bg2;
        private Button _btnClose;
        private Transform _btnContainer;
        private Button _btnNext;
        private Button _btnPrev;
        private Button _btnTabNext;
        private Button _btnTabPrev;
        private Button _btnUpdate;
        private GameObject _contentGo;
        private PShopList _curShop;
        private SysShopVo _curShopSysVo;
        private int _curTab = -1;
        private ShopTabView _curTabView;
        private byte _curType;
        private int _endIdx;
        private int _lastUpdateFC;
        private GameObject _listGo;
        private MainToolView _mainTollBarView;
        private GameObject _moneyGo;
        private UISprite _moneyIcon;
        private UILabel _moneyLabel;
        private UIScrollView _scrl;
        private List<byte> _shopTypes = new List<byte>();
        private int _startIdx;
        private List<UIToggle> _tabToggles;
        private GameObject _tickGo;
        private UILabel _tickLabel;
        private int BTN_PAGE_SIZE;
        public const int PAGE_SIZE = 8;

        public override void CancelUpdateHandler()
        {
            Singleton<ShopMode>.Instance.dataUpdatedWithParam -= new com.game.module.core.DataUpateHandlerWithParam(this.OnShopEvent);
            Singleton<RoleMode>.Instance.dataUpdated -= new DataUpateHandler(this.OnRoleUpdate);
            this._btnPrev.onClick = null;
            this._btnNext.onClick = null;
            this._btnUpdate.onClick = null;
            this._btnClose.onClick = null;
        }

        protected override void HandleAfterOpenView()
        {
            this._mainTollBarView.isActive = true;
            this._scrl.ResetPosition();
            this.UpdateView();
            this._startIdx = 0;
            this.UpdateLRBtns(true);
        }

        protected override void HandleBeforeCloseView()
        {
            this._mainTollBarView.isActive = false;
            foreach (UIToggle toggle in this._tabToggles)
            {
                toggle.value = false;
            }
            if (this._curTab >= 0)
            {
                this._tabToggles[this._curTab].value = false;
                this._curTab = -1;
            }
            this._curShopSysVo = null;
            this._curShop = null;
            this._lastUpdateFC = 0;
        }

        protected override void Init()
        {
            foreach (object obj2 in BaseDataMgr.instance.GetDicByType<SysShopVo>().Values)
            {
                SysShopVo vo = (SysShopVo) obj2;
                this._shopTypes.Add((byte) vo.unikey);
            }
            this._shopTypes.Sort();
            Singleton<ShopMode>.Instance.ReqShopInfo(0);
            Singleton<ShopBuyView>.Instance.gameObject = base.FindChild("ShopBuyPanel");
            Singleton<ShopBuyView>.Instance.gameObject.SetActive(false);
            this._mainTollBarView = new MainToolView(base.FindChild("content/info"));
            this._contentGo = base.FindChild("content");
            this._contentGo.SetActive(true);
            this._listGo = base.FindChild("content/list");
            this._moneyGo = base.FindChild("content/money");
            this._tickGo = base.FindChild("content/tick");
            this._tabToggles = new List<UIToggle>();
            this._btnContainer = base.FindChild("content/tabs/tabBtns").transform;
            UIToggle item = base.FindInChild<UIToggle>("content/tabs/tabBtns/btn0");
            BoxCollider component = item.GetComponent<BoxCollider>();
            Vector3 localPosition = item.transform.localPosition;
            for (int i = 0; i < this._shopTypes.Count; i++)
            {
                if (i > 0)
                {
                    item = NGUITools.AddChild(this._tabToggles[0].transform.parent.gameObject, this._tabToggles[0].gameObject).GetComponent<UIToggle>();
                    localPosition.x = i * component.size.x;
                    item.transform.localPosition = this._tabToggles[0].transform.localPosition + localPosition;
                }
                SysShopVo shop = BaseDataMgr.instance.GetShop(this._shopTypes[i]);
                NGUITools.FindInChild<UILabel>(item.gameObject, "label").text = shop.name;
                EventDelegate.Add(item.onChange, new EventDelegate.Callback(this.OnChangeTab));
                this._tabToggles.Add(item);
            }
            this._scrl = this._listGo.GetComponent<UIScrollView>();
            this._bg2 = base.FindInChild<UISprite>("content/bg2");
            this.BTN_PAGE_SIZE = (int) Math.Floor((double) (((double) this._bg2.width) / ((double) component.size.x)));
            this._tickLabel = base.FindInChild<UILabel>("content/tick/label");
            this._btnUpdate = base.FindInChild<Button>("content/tick/btnUpdate");
            this._btnPrev = base.FindInChild<Button>("content/btnPrev");
            this._btnNext = base.FindInChild<Button>("content/btnNext");
            this._btnTabPrev = base.FindInChild<Button>("content/btnTabPrev");
            this._btnTabNext = base.FindInChild<Button>("content/btnTabNext");
            this._btnClose = base.FindInChild<Button>("content/btnClose");
            this._moneyLabel = base.FindInChild<UILabel>("content/money/label");
            this._moneyIcon = base.FindInChild<UISprite>("content/money/icon");
            this._btnTabNext.onClick = this._btnTabPrev.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickTabBtn);
        }

        private void OnChangeTab()
        {
            <OnChangeTab>c__AnonStoreyED yed = new <OnChangeTab>c__AnonStoreyED {
                toggle = UIToggle.current
            };
            if (yed.toggle.value)
            {
                int index = this._tabToggles.FindIndex(new Predicate<UIToggle>(yed.<>m__BA));
                if ((index >= 0) && (index != this._curTab))
                {
                    this.SetTab(index);
                    this.UpdateLRBtns(false);
                }
            }
        }

        private void OnClickClose(GameObject go)
        {
            this.CloseView();
        }

        private void OnClickNext(GameObject go)
        {
            if (this._curTab < (this._shopTypes.Count - 1))
            {
                int index = this._curTab + 1;
                this.SetTab(index);
                if ((index < this._startIdx) || (index > this._endIdx))
                {
                    this._startIdx = Math.Max(0, (index - this.BTN_PAGE_SIZE) + 1);
                }
                this.UpdateLRBtns(true);
            }
        }

        private void OnClickPrev(GameObject go)
        {
            if (this._curTab > 0)
            {
                int index = this._curTab - 1;
                this.SetTab(index);
                if ((index < this._startIdx) || (index > this._endIdx))
                {
                    this._startIdx = index;
                }
                this.UpdateLRBtns(true);
            }
        }

        private void OnClickTabBtn(GameObject go)
        {
            bool flag = false;
            if (this._btnTabPrev.gameObject == go)
            {
                if (this._startIdx <= 0)
                {
                    return;
                }
                flag = true;
                this._startIdx--;
            }
            else if (this._btnTabNext.gameObject == go)
            {
                if (this._endIdx >= (this._shopTypes.Count - 1))
                {
                    return;
                }
                this._startIdx++;
            }
            this.UpdateIndex();
            if ((this._curTab < this._startIdx) || (this._curTab > this._endIdx))
            {
                if (flag)
                {
                    this.SetTab(this._endIdx);
                }
                else
                {
                    this.SetTab(this._startIdx);
                }
            }
            this.UpdateLRBtns(false);
        }

        private void OnClickUpdate(GameObject go)
        {
            PShopList shop = Singleton<ShopMode>.Instance.GetShop(this._curType);
            if ((this._curShopSysVo.manualUpdate >= 0) && (shop.refreshCount >= this._curShopSysVo.manualUpdate))
            {
                MessageManager.Show(LanguageManager.GetWord("Shop.RefreshCountOut"));
            }
            else
            {
                SysShopRefreshVo refreshVo = Singleton<ShopMode>.Instance.GetRefreshVo(this._curType);
                if (refreshVo == null)
                {
                    MessageManager.Show(LanguageManager.GetWord("Shop.RefreshCountOut"));
                }
                else
                {
                    uint own = ShopMode.Currency.GetOwn(refreshVo.pay_type);
                    ShopMode.Currency currency = ShopMode.Currency.GetCurrency(refreshVo.pay_type);
                    if (own < refreshVo.pay_count)
                    {
                        MessageManager.Show(LanguageManager.GetWord("Game.CurrencyNotEnough", currency.name));
                    }
                    else
                    {
                        string[] param = new string[] { refreshVo.pay_count.ToString(), currency.name };
                        ConfirmMgr.Instance.ShowCommonAlert(LanguageManager.GetWord("Shop.SureToRefresh", param), "OK_CANCEL", () => Singleton<ShopMode>.Instance.ReqRefresh(this._curType), LanguageManager.GetWord("ConfirmView.Ok"), null, LanguageManager.GetWord("ConfirmView.Cancel"));
                    }
                }
            }
        }

        private void OnRoleUpdate(object o, int code)
        {
            this.UpdateCurrency();
        }

        private void OnShopEvent(object sender, int code, object param)
        {
            switch (code)
            {
                case 1:
                    this.UpdateView();
                    break;

                case 2:
                    this._curTabView.Update((PShopItem) param);
                    break;
            }
        }

        public override void RegisterUpdateHandler()
        {
            Singleton<ShopMode>.Instance.dataUpdatedWithParam += new com.game.module.core.DataUpateHandlerWithParam(this.OnShopEvent);
            Singleton<RoleMode>.Instance.dataUpdated += new DataUpateHandler(this.OnRoleUpdate);
            this._btnPrev.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickPrev);
            this._btnNext.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickNext);
            this._btnUpdate.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickUpdate);
            this._btnClose.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickClose);
        }

        private void SetTab(int index)
        {
            if (this._curTab != index)
            {
                this._curTab = index;
                this._lastUpdateFC = 0;
                this._scrl.ResetPosition();
                this.UpdateView();
            }
        }

        private void SetTime(int leftSecond)
        {
            if (leftSecond < 0)
            {
                leftSecond = 0;
            }
            int num = leftSecond / 60;
            int num2 = leftSecond % 60;
            int num3 = 0;
            if (leftSecond >= 0xe10)
            {
                num3 = num / 60;
                num = num % 60;
            }
            string str = (num >= 10) ? (num + string.Empty) : ("0" + num);
            string str2 = (num2 >= 10) ? (num2 + string.Empty) : ("0" + num2);
            string str3 = (num3 >= 10) ? (num3 + string.Empty) : ("0" + num3);
            this._tickLabel.text = LanguageManager.GetWord("Shop.LeftUpdateTime", str3 + ":" + str + ":" + str2);
        }

        public void Show(int tabIdx)
        {
            this._curTab = tabIdx;
            this.OpenView();
        }

        public override void Update()
        {
            int leftSecond = this.UpdateLeftTime();
            if (this._tickGo.activeInHierarchy)
            {
                if (leftSecond <= 0)
                {
                    this.SetTime(0);
                }
                else
                {
                    this.SetTime(leftSecond);
                }
            }
        }

        private void UpdateCurrency()
        {
            if ((this._curShopSysVo != null) && (this._curShopSysVo.showMoney > 0))
            {
                switch (this._curShopSysVo.showMoney)
                {
                    case 1:
                        this._moneyLabel.text = MeVo.instance.diamond.ToString();
                        break;

                    case 3:
                        this._moneyLabel.text = MeVo.instance.diam.ToString();
                        break;
                }
            }
        }

        private void UpdateIndex()
        {
            this._endIdx = Math.Min(this._startIdx + this.BTN_PAGE_SIZE, this._shopTypes.Count) - 1;
            this._startIdx = Math.Max(0, (this._endIdx - this.BTN_PAGE_SIZE) + 1);
        }

        private int UpdateLeftTime()
        {
            if ((this._curShopSysVo == null) || (this._curShop == null))
            {
                return 0;
            }
            int num = ((int) this._curShop.endTime) - ServerTime.Instance.Timestamp;
            if ((num <= 0) && ((Time.frameCount - this._lastUpdateFC) >= 0x10))
            {
                this._lastUpdateFC = Time.frameCount;
                Singleton<ShopMode>.Instance.ReqShopInfo(this._curType);
            }
            return num;
        }

        private void UpdateLRBtns(bool updateIndex)
        {
            if (updateIndex)
            {
                this.UpdateIndex();
            }
            BoxCollider component = this._tabToggles[0].GetComponent<BoxCollider>();
            Vector3 localPosition = this._btnContainer.localPosition;
            localPosition.x = -this._startIdx * component.size.x;
            this._btnContainer.localPosition = localPosition;
            for (int i = 0; i < this._tabToggles.Count; i++)
            {
                this._tabToggles[i].SetActive((i >= this._startIdx) && (i <= this._endIdx));
            }
            this._btnPrev.SetActive(this._curTab > 0);
            this._btnNext.SetActive(this._curTab < (this._shopTypes.Count - 1));
            this._btnTabPrev.SetActive(this._startIdx > 0);
            this._btnTabNext.SetActive(this._endIdx < (this._shopTypes.Count - 1));
        }

        private void UpdateView()
        {
            if (this._curTabView == null)
            {
                this._curTabView = new ShopTabView(this._listGo);
            }
            if (this._curTab < 0)
            {
                this._curTab = 0;
            }
            for (int i = 0; i < this._tabToggles.Count; i++)
            {
                this._tabToggles[i].value = this._curTab == i;
            }
            this._curType = this._shopTypes[this._curTab];
            this._curShopSysVo = BaseDataMgr.instance.GetShop(this._curType);
            this._curShop = Singleton<ShopMode>.Instance.GetShop(this._curType);
            List<PShopItem> items = Singleton<ShopMode>.Instance.GetItems(this._curType);
            this._curTabView.Update(items);
            bool flag = this._curShopSysVo.showMoney > 0;
            this._moneyGo.SetActive(flag);
            if (flag)
            {
                ShopMode.Currency currency = ShopMode.Currency.GetCurrency(this._curShopSysVo.showMoney);
                this._moneyIcon.spriteName = (currency == null) ? string.Empty : currency.iconName;
            }
            this.UpdateCurrency();
            this._tickGo.SetActive(0 != this._curShopSysVo.manualUpdate);
            this.UpdateLeftTime();
        }

        public byte curType
        {
            get
            {
                return this._curType;
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Shop/ShopView.assetbundle";
            }
        }

        [CompilerGenerated]
        private sealed class <OnChangeTab>c__AnonStoreyED
        {
            internal UIToggle toggle;

            internal bool <>m__BA(UIToggle t)
            {
                return (this.toggle == t);
            }
        }
    }
}

