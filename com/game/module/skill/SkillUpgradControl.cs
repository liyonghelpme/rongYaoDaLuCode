﻿namespace com.game.module.skill
{
    using com.game;
    using com.game.module.core;
    using com.game.Public.Message;
    using com.net;
    using com.net.interfaces;
    using Proto;
    using System;

    public class SkillUpgradControl : BaseControl<SkillUpgradControl>
    {
        private void Fun_19_2(INetData data)
        {
            SkillLearningSkillLvlupMsg_19_2 skillLvlup = new SkillLearningSkillLvlupMsg_19_2();
            skillLvlup.read(data.GetMemoryStream());
            if (skillLvlup.code != 0)
            {
                ErrorCodeManager.ShowError(skillLvlup.code);
            }
            else
            {
                Singleton<GeneralMode>.Instance.UpdateGeneralSkillInfo(skillLvlup);
            }
        }

        private void Fun_19_4(INetData data)
        {
            SkillLearningSkillPointMsg_19_4 skillPoint = new SkillLearningSkillPointMsg_19_4();
            skillPoint.read(data.GetMemoryStream());
            if (skillPoint.code != 0)
            {
                ErrorCodeManager.ShowError(skillPoint.code);
            }
            else
            {
                Singleton<GeneralMode>.Instance.UpdateGeneralSkillTime(skillPoint);
            }
        }

        protected override void NetListener()
        {
            AppNet.main.addCMD("4866", new NetMsgCallback(this.Fun_19_2));
            AppNet.main.addCMD("4868", new NetMsgCallback(this.Fun_19_4));
        }
    }
}

