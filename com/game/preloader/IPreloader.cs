﻿namespace com.game.preloader
{
    using System.Collections;
    using System.Collections.Generic;

    public interface IPreloader
    {
        IEnumerator PreloadResourceList(IList<SysReadyLoadVo> preLoadList);
    }
}

