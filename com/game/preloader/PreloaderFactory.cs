﻿namespace com.game.preloader
{
    using com.game.data;
    using com.game.module.effect;
    using com.game.sound;
    using System;

    public class PreloaderFactory
    {
        public static PreloaderFactory Instance = ((Instance != null) ? Instance : new PreloaderFactory());

        private PreloaderFactory()
        {
        }

        public IPreloader GetPreLoader(SysReadyLoadVo preLoadVo)
        {
            switch (preLoadVo.subtype)
            {
                case 1:
                    return EffectMgr.Instance;

                case 5:
                    return SoundMgr.Instance;
            }
            return null;
        }
    }
}

