﻿namespace com.game.sound
{
    using System;

    public class AudioSetting
    {
        public float Delay;
        public bool IsLoading;
        public bool Loop;
        public float Volumn;
    }
}

