﻿namespace com.game.sound
{
    using System;

    public class SoundId
    {
        public const string Music_EnterGame = "1000";
        public const string Music_PrepareBattle = "1006";
        public const string Music_StartMovie = "1017";
        public const string Sound_BattleWin = "3014";
        public const string Sound_ButtonFold = "3010";
        public const string Sound_ConfirmClose = "3005";
        public const string Sound_ConfirmOk = "3004";
        public const string Sound_ConfirmSel = "3006";
        public const string Sound_CopyChallenge = "3019";
        public const string Sound_CopySelect = "3018";
        public const string Sound_EatDiam1 = "3007";
        public const string Sound_EatDiam2 = "3008";
        public const string Sound_FinishTask = "3017";
        public const string Sound_Login = "3015";
        public const string Sound_MagicWin = "77001";
        public const string Sound_OpenGoldBox = "3009";
        public const string Sound_ReceieTask = "3016";
        public const string Sound_StoneEmbed = "3002";
        public const string Sound_StoreSumption = "3003";
        public const string Sound_StreassEquip = "3013";
        public const string Sound_SwordSkill3Bullet = "60631";
        public const string Sound_SwordSkill4 = "60043";
        public const string Sound_Teleport = "3001";
        public const string Sound_WearArmor = "3011";
        public const string Sound_WearWeapon = "3012";
    }
}

