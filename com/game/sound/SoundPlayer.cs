﻿namespace com.game.sound
{
    using System;
    using UnityEngine;

    public class SoundPlayer
    {
        private static AudioListener audioListener;
        private AudioSource audioSource;
        private float delay;
        private bool isDelayPlaying;

        public SoundPlayer(AudioClip clip)
        {
            if (null == audioListener)
            {
                audioListener = UnityEngine.Object.FindObjectOfType(typeof(AudioListener)) as AudioListener;
                if (null == audioListener)
                {
                    Camera main = Camera.main;
                    if (null == main)
                    {
                        main = UnityEngine.Object.FindObjectOfType(typeof(Camera)) as Camera;
                    }
                    if (null != main)
                    {
                        audioListener = main.gameObject.AddComponent<AudioListener>();
                    }
                }
            }
            if (null != audioListener)
            {
                this.audioSource = audioListener.gameObject.AddComponent<AudioSource>();
                this.audioSource.playOnAwake = false;
            }
            this.audioSource.clip = clip;
        }

        private void DelayPlay()
        {
            this.isDelayPlaying = false;
            this.audioSource.Play();
        }

        public void Pause()
        {
            this.audioSource.Pause();
        }

        public void Play()
        {
            if (this.delay > 0f)
            {
                this.isDelayPlaying = true;
                vp_Timer.In(this.delay, new vp_Timer.Callback(this.DelayPlay), 1, 0f, null);
            }
            else
            {
                this.audioSource.Play();
            }
        }

        public void Stop()
        {
            this.audioSource.Stop();
        }

        public AudioClip Clip
        {
            get
            {
                return this.audioSource.clip;
            }
            set
            {
                this.audioSource.clip = value;
            }
        }

        public float Delay
        {
            get
            {
                return this.delay;
            }
            set
            {
                this.delay = value;
            }
        }

        public bool IsPlaying
        {
            get
            {
                return (this.audioSource.isPlaying || this.isDelayPlaying);
            }
        }

        public bool Loop
        {
            get
            {
                return this.audioSource.loop;
            }
            set
            {
                this.audioSource.loop = value;
            }
        }

        public bool Mute
        {
            get
            {
                return this.audioSource.mute;
            }
            set
            {
                this.audioSource.mute = value;
            }
        }

        public float Volumn
        {
            get
            {
                return this.audioSource.volume;
            }
            set
            {
                this.audioSource.volume = value;
            }
        }
    }
}

