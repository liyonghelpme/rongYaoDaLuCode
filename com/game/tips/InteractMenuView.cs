﻿namespace com.game.tips
{
    using com.game.module.core;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class InteractMenuView : BaseView<InteractMenuView>
    {
        [CompilerGenerated]
        private static Func<Button, bool> <>f__am$cacheA;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map28;
        private Button append_friend;
        private UISprite background;
        private List<Button> buttonList;
        private int CurrentHeight;
        private Button inter_change;
        private Button invite_team;
        private bool isFriend;
        private Button look_info;
        private const int MaxHeight = 0xf7;
        private const int MinHeight = 0x99;
        private List<float> pointList = new List<float> { 0f, -47f, -94f, -141f, -188f };
        private Button send_messager;

        public override void CancelUpdateHandler()
        {
        }

        protected override void HandleAfterOpenView()
        {
            Vector3 vector3;
            this.isFriend = true;
            this.CurrentHeight = !this.isFriend ? 0xf7 : 0x99;
            this.UpdateItemList();
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.x += this.background.width / 2;
            mousePosition.y -= this.background.height / 2;
            mousePosition.z = 0f;
            Vector3 tgPosition = UICamera.currentCamera.ScreenToWorldPoint(mousePosition);
            PointUtils.ScreenByPoint(this.background.transform, tgPosition, 1, out vector3);
            this.transform.position = vector3;
        }

        protected override void Init()
        {
            this.background = base.FindInChild<UISprite>("background");
            this.look_info = base.FindInChild<Button>("list/look_info");
            this.send_messager = base.FindInChild<Button>("list/send_messager");
            this.append_friend = base.FindInChild<Button>("list/append_friend");
            this.invite_team = base.FindInChild<Button>("list/invite_team");
            this.inter_change = base.FindInChild<Button>("list/inter_change");
            this.buttonList = new List<Button> { this.look_info, this.send_messager, this.append_friend, this.invite_team, this.inter_change };
            this.RegisterEventHandler();
        }

        private void OnClickHandler(GameObject go)
        {
            this.CloseView();
            string name = go.name;
            if (name != null)
            {
                int num;
                if (<>f__switch$map28 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(5);
                    dictionary.Add("look_info", 0);
                    dictionary.Add("send_messager", 1);
                    dictionary.Add("append_friend", 2);
                    dictionary.Add("invite_team", 3);
                    dictionary.Add("inter_change", 4);
                    <>f__switch$map28 = dictionary;
                }
                if (<>f__switch$map28.TryGetValue(name, out num))
                {
                    switch (num)
                    {
                        case 0:
                            Singleton<CorpsPanel>.Instance.OpenView(PanelType.NONE);
                            Singleton<RoleMode>.Instance.RequestOtherRoleInfo(Singleton<RoleMode>.Instance.CurrentRoleId);
                            break;
                    }
                }
            }
        }

        private void RegisterEventHandler()
        {
            this.look_info.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.send_messager.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.append_friend.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.invite_team.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
            this.inter_change.onClick = new UIWidgetContainer.VoidDelegate(this.OnClickHandler);
        }

        public override void RegisterUpdateHandler()
        {
        }

        private void UpdateItemList()
        {
            this.invite_team.SetActive(false);
            this.append_friend.SetActive(!this.isFriend);
            int num = -1;
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = button => button.gameObject.activeSelf;
            }
            IEnumerator<Button> enumerator = this.buttonList.Where<Button>(<>f__am$cacheA).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Button current = enumerator.Current;
                    num++;
                    current.transform.localPosition = new Vector3(0f, this.pointList[num], 0f);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.background.height = this.CurrentHeight;
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Tips/InteractMenuView.assetbundle";
            }
        }
    }
}

