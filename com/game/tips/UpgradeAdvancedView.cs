﻿namespace com.game.tips
{
    using com.game.module.core;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class UpgradeAdvancedView : BaseView<UpgradeAdvancedView>
    {
        private GameObject Advanced;
        private List<UILabel> advancedProperty = new List<UILabel>();
        private UISprite afterHead;
        private UILabel afterLv;
        private List<UISprite> afterStar = new List<UISprite>();
        private UISprite beforeHead;
        private UILabel beforeLv;
        private List<UISprite> beforeStar = new List<UISprite>();
        private const string change_m = "魔攻成长";
        private const string change_w = "物攻成长";
        private UILabel changetTitle;
        private Button closeBtn;
        private StateView CurrentView;
        private GameObject panelTitle;
        private GameObject Upgrade;
        private List<UILabel> upgradeProperty = new List<UILabel>();

        protected override void HandleAfterOpenView()
        {
            this.CurrentView = (StateView) Singleton<RoleMode>.Instance.CurrentTipsType;
            Vector3 localPosition = this.panelTitle.transform.localPosition;
            Vector3 localScale = this.panelTitle.transform.localScale;
            switch (this.CurrentView)
            {
                case StateView.Upgrade:
                    this.panelTitle.transform.localPosition = new Vector3(localPosition.x, 140f, localPosition.z);
                    this.panelTitle.transform.localScale = new Vector3(0.82f, localScale.y, localScale.z);
                    this.closeBtn.gameObject.transform.localPosition = new Vector3(195f, 140f, 1f);
                    break;

                case StateView.Advanced:
                    this.panelTitle.transform.localPosition = new Vector3(localPosition.x, 195f, localPosition.z);
                    this.panelTitle.transform.localScale = new Vector3(1f, localScale.y, localScale.z);
                    this.closeBtn.gameObject.transform.localPosition = new Vector3(240f, 195f, 1f);
                    break;
            }
        }

        protected override void HandleBeforeCloseView()
        {
        }

        protected override void Init()
        {
            this.Upgrade = base.FindChild("advanced");
            this.Advanced = base.FindChild("upgrade");
            this.panelTitle = base.FindChild("title");
            this.closeBtn = base.FindInChild<Button>("close");
            this.beforeHead = base.FindInChild<UISprite>("advanced/before/general/headIcon");
            this.beforeLv = base.FindInChild<UILabel>("advanced/before/general/levelLabel");
            this.afterHead = base.FindInChild<UISprite>("advanced/after/general/headIcon");
            this.afterLv = base.FindInChild<UILabel>("advanced/after/general/levelLabel");
            this.beforeStar.Add(base.FindInChild<UISprite>("advanced/before/general/starContainer/star1"));
            this.beforeStar.Add(base.FindInChild<UISprite>("advanced/before/general/starContainer/star2"));
            this.beforeStar.Add(base.FindInChild<UISprite>("advanced/before/general/starContainer/star3"));
            this.beforeStar.Add(base.FindInChild<UISprite>("advanced/before/general/starContainer/star4"));
            this.beforeStar.Add(base.FindInChild<UISprite>("advanced/before/general/starContainer/star5"));
            this.afterStar.Add(base.FindInChild<UISprite>("advanced/after/general/starContainer/star1"));
            this.afterStar.Add(base.FindInChild<UISprite>("advanced/after/general/starContainer/star2"));
            this.afterStar.Add(base.FindInChild<UISprite>("advanced/after/general/starContainer/star3"));
            this.afterStar.Add(base.FindInChild<UISprite>("advanced/after/general/starContainer/star4"));
            this.afterStar.Add(base.FindInChild<UISprite>("advanced/after/general/starContainer/star5"));
            this.advancedProperty.Add(base.FindInChild<UILabel>("advanced/property/property_1/num"));
            this.advancedProperty.Add(base.FindInChild<UILabel>("advanced/property/property_2/num"));
            this.advancedProperty.Add(base.FindInChild<UILabel>("advanced/property/property_3/num"));
            this.advancedProperty.Add(base.FindInChild<UILabel>("advanced/property/property_4/num"));
            this.upgradeProperty.Add(base.FindInChild<UILabel>("upgrade/property/property_1/num"));
            this.upgradeProperty.Add(base.FindInChild<UILabel>("upgrade/property/property_2/num"));
            this.upgradeProperty.Add(base.FindInChild<UILabel>("upgrade/property/property_3/num"));
            this.upgradeProperty.Add(base.FindInChild<UILabel>("upgrade/property/property_4/num"));
            this.upgradeProperty.Add(base.FindInChild<UILabel>("upgrade/property/property_5/num"));
            this.changetTitle = base.FindInChild<UILabel>("advanced/property/property_2/title");
        }

        private void UpdateAdvancedInfo()
        {
        }

        private void UpdateUpgradeInfo()
        {
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.MiddleLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Tips/UpgradView.assetbundle";
            }
        }

        public enum StateView
        {
            None,
            Upgrade,
            Advanced
        }
    }
}

