﻿namespace com.game.tips
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.SystemData;
    using com.game.utils;
    using com.game.vo;
    using System;
    using UnityEngine;

    public class VigourTipView : BaseView<VigourTipView>
    {
        private UIWidgetContainer _bgHot;
        private UILabel _countLabel;
        private UILabel _fullLabel;
        private UILabel _gapLabel;
        private UILabel _nextLabel;
        private int _prevUpdateFC;
        private UILabel _timeLabel;

        protected override void HandleAfterOpenView()
        {
            this._bgHot.onHover = new UIWidgetContainer.BoolDelegate(this.OnHover);
            this.Update();
            this._gapLabel.text = LanguageManager.GetWord("Game.VigourTimeInterval", 6f.ToString());
            string[] param = new string[] { MeVo.instance.numBuyVigour.ToString(), VarMgr.NumBuyVigourMax.ToString() };
            this._countLabel.text = LanguageManager.GetWord("Game.CountHasBuy", param);
        }

        protected override void Init()
        {
            this._timeLabel = base.FindInChild<UILabel>("labels/timeLabel");
            this._countLabel = base.FindInChild<UILabel>("labels/countLabel");
            this._nextLabel = base.FindInChild<UILabel>("labels/nextLabel");
            this._fullLabel = base.FindInChild<UILabel>("labels/fullLabel");
            this._gapLabel = base.FindInChild<UILabel>("labels/gapLabel");
            this._bgHot = base.FindInChild<UIWidgetContainer>("bg");
        }

        private void OnHover(GameObject go, bool b)
        {
            if (!b)
            {
                this._bgHot.onHover = null;
                this.CloseView();
            }
        }

        public override void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.CloseView();
            }
            else if ((Time.frameCount - this._prevUpdateFC) >= 0x10)
            {
                this._prevUpdateFC = Time.frameCount;
                this._timeLabel.text = LanguageManager.GetWord("Game.CurTime", TimeUtil.GetTimeStrOfToday());
                int num = MeVo.instance.vigourFull - MeVo.instance.vigour;
                if (num <= 0)
                {
                    this._nextLabel.text = LanguageManager.GetWord("Game.NextVigourTime", LanguageManager.GetWord("Game.VigourFulled"));
                    this._fullLabel.text = LanguageManager.GetWord("Game.FullVigourTime", LanguageManager.GetWord("Game.VigourFulled"));
                }
                else
                {
                    int num2 = 360;
                    int leftTime = 0;
                    int num4 = (MeVo.instance.lastVigourRecoverTime <= 0) ? GlobalData.loginTime : MeVo.instance.lastVigourRecoverTime;
                    leftTime = (num4 + num2) - ServerTime.Instance.Timestamp;
                    this._nextLabel.text = LanguageManager.GetWord("Game.NextVigourTime", TimeUtil.GetTimeHhmmss(leftTime));
                    leftTime = (num4 + (num * num2)) - ServerTime.Instance.Timestamp;
                    this._fullLabel.text = LanguageManager.GetWord("Game.FullVigourTime", TimeUtil.GetTimeHhmmss(leftTime));
                }
            }
        }

        public override ViewLayer layerType
        {
            get
            {
                return ViewLayer.TopLayer;
            }
        }

        public override string url
        {
            get
            {
                return "UI/Tips/VigourTipView.assetbundle";
            }
        }

        public override bool waiting
        {
            get
            {
                return false;
            }
        }
    }
}

