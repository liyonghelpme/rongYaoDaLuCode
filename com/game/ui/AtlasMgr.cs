﻿namespace com.game.ui
{
    using com.u3d.bases.loader;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AtlasMgr
    {
        protected int count;
        protected string dir;
        protected int increment;

        public AtlasMgr(string dir, int count, int increment = 0)
        {
            this.dir = dir;
            this.count = count;
            this.increment = increment;
        }

        public UIAtlas getAtlas(int id)
        {
            int num = (id - this.increment) / this.count;
            UnityEngine.Object obj2 = ResMgr.instance.load(string.Format("{0}/{1}", this.dir, num), typeof(UIAtlas));
            return ((obj2 == null) ? null : (obj2 as UIAtlas));
        }
    }
}

