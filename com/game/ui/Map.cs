﻿namespace com.game.ui
{
    using System;
    using UnityEngine;

    [Serializable]
    public class Map
    {
        public GameObject gameObject;
        public string id;
    }
}

