﻿namespace com.game.ui
{
    using System;
    using UnityEngine;

    public class Table : UITable
    {
        public static int SortByName(Transform a, Transform b)
        {
            int num = Convert.ToInt32(a.name);
            int num2 = Convert.ToInt32(b.name);
            return Convert.ToInt32(num > num2);
        }
    }
}

