﻿namespace com.game.ui
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    [AddComponentMenu("NGUI/Tween/Slider")]
    public class TweenScrollbar : UITweener
    {
        public float from;
        public float to;

        public static TweenScrollbar Begin(GameObject go, float duration, float value, UITweener.Style style = 0)
        {
            TweenScrollbar scrollbar = UITweener.Begin<TweenScrollbar>(go, duration);
            float scrollValue = scrollbar.scrollValue;
            switch (style)
            {
                case UITweener.Style.Loop:
                    if (value < scrollValue)
                    {
                        scrollValue = 0f;
                    }
                    break;

                case UITweener.Style.PingPong:
                    if (value > scrollValue)
                    {
                        scrollValue = 1f;
                    }
                    break;
            }
            scrollbar.from = scrollValue;
            scrollbar.to = value;
            if (duration <= 0f)
            {
                scrollbar.Sample(1f, true);
                scrollbar.enabled = false;
            }
            return scrollbar;
        }

        protected override void OnUpdate(float factor, bool isFinished)
        {
            base.gameObject.GetComponent<UIScrollBar>().scrollValue = (this.from * (1f - factor)) + (this.to * factor);
        }

        public float scrollValue
        {
            get
            {
                UIScrollBar component = base.gameObject.GetComponent<UIScrollBar>();
                if (component == null)
                {
                    UnityEngine.Object.Destroy(this);
                }
                return component.scrollValue;
            }
        }
    }
}

