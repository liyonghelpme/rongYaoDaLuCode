﻿namespace com.game.ui
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    [AddComponentMenu("NGUI/Tween/Slider")]
    public class TweenSlider : UITweener
    {
        public float from;
        public float to;

        public static TweenSlider Begin(GameObject go, float duration, float value, UITweener.Style style = 0)
        {
            TweenSlider slider = UITweener.Begin<TweenSlider>(go, duration);
            float sliderValue = slider.sliderValue;
            switch (style)
            {
                case UITweener.Style.Loop:
                    if (value < sliderValue)
                    {
                        sliderValue = 0f;
                    }
                    break;

                case UITweener.Style.PingPong:
                    if (value > sliderValue)
                    {
                        sliderValue = 1f;
                    }
                    break;
            }
            slider.from = sliderValue;
            slider.to = value;
            if (duration <= 0f)
            {
                slider.Sample(1f, true);
                slider.enabled = false;
            }
            return slider;
        }

        protected override void OnUpdate(float factor, bool isFinished)
        {
            base.gameObject.GetComponent<UISlider>().sliderValue = (this.from * (1f - factor)) + (this.to * factor);
        }

        public float sliderValue
        {
            get
            {
                UISlider component = base.gameObject.GetComponent<UISlider>();
                if (component == null)
                {
                    UnityEngine.Object.Destroy(this);
                }
                return component.sliderValue;
            }
        }
    }
}

