﻿namespace com.game.ui
{
    using System;
    using UnityEngine;

    public class UIColor : MonoBehaviour
    {
        protected UISprite sprite;

        private void Awake()
        {
            this.sprite = base.GetComponent<UISprite>();
        }

        public void setColor(object color)
        {
            System.Type type = color.GetType();
            if (type == typeof(string))
            {
                this.sprite.color = NGUITools.ParseColor((string) color, 0);
            }
            else if (type == typeof(Color))
            {
                this.sprite.color = (Color) color;
            }
        }
    }
}

