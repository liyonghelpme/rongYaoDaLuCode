﻿namespace com.game.ui
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("NGUI/Interaction/DragPageController")]
    public class UIDragPageController : MonoBehaviour
    {
        public bool disableColliderInChildren;
        public int firstPage;
        public List<GameObject> pages = new List<GameObject>();
        public Transform target;
        public float width;

        public void addPage(GameObject page)
        {
            if ((page != null) && !this.pages.Contains(page))
            {
                this.pages.Add(page);
            }
        }

        private void Awake()
        {
            this.immediatelyTurnToPage(this.firstPage);
        }

        public void disableColliderInChildrenExcept(int id)
        {
            for (int i = 0; i < this.pageCount; i++)
            {
                GameObject obj2 = this.getPage(i);
                if (obj2 != null)
                {
                    foreach (BoxCollider collider in obj2.GetComponentsInChildren<BoxCollider>())
                    {
                        collider.enabled = id == i;
                    }
                }
            }
        }

        public GameObject getPage(int id)
        {
            if ((id >= 0) && (id < this.pageCount))
            {
                return this.pages[id];
            }
            return null;
        }

        public float getX(int page)
        {
            return (-this.width * page);
        }

        public void immediatelyTurnToPage(int page)
        {
            if ((page >= 0) && (page < this.pageCount))
            {
                this.stop();
                Vector3 localPosition = this.target.transform.localPosition;
                localPosition.x = this.getX(page);
                this.target.transform.localPosition = localPosition;
            }
        }

        public void moveToTarget(Vector3 dest)
        {
            SpringPosition position = SpringPosition.Begin(this.target.gameObject, dest, 5f);
            position.ignoreTimeScale = true;
            position.worldSpace = false;
        }

        public void setLastPage(int lastPage)
        {
            this.lastPage = lastPage;
        }

        public void stop()
        {
            SpringPosition component = this.target.GetComponent<SpringPosition>();
            if (component != null)
            {
                component.enabled = false;
            }
        }

        public void turnToPage(int page)
        {
            if ((page >= 0) && (page < this.pageCount))
            {
                this.stop();
                if (this.disableColliderInChildren)
                {
                    this.disableColliderInChildrenExcept(page);
                }
                Vector3 localPosition = this.target.transform.localPosition;
                localPosition.x = -this.width * page;
                this.moveToTarget(localPosition);
            }
        }

        public int lastPage { get; protected set; }

        public int pageCount
        {
            get
            {
                return this.pages.Count;
            }
        }
    }
}

