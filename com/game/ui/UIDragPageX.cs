﻿namespace com.game.ui
{
    using System;
    using UnityEngine;

    [AddComponentMenu("NGUI/Interaction/DragPageX")]
    public class UIDragPageX : MonoBehaviour
    {
        public UIDragPageController controller;
        private Vector3 lastHit;
        private float lastTime;
        private float lastX;
        private float leftEdge;
        private Plane plane;
        private float rightEdge;

        private void Awake()
        {
            if (this.controller == null)
            {
                this.controller = NGUITools.FindInParents<UIDragPageController>(base.gameObject);
            }
        }

        private void OnDrag(Vector2 delta)
        {
            if (((base.enabled && NGUITools.GetActive(base.gameObject)) && ((this.controller.target != null) && (this.controller.width > 0f))) && (this.controller.pageCount >= 1))
            {
                Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.lastTouchPosition);
                float enter = 0f;
                if (this.plane.Raycast(ray, out enter))
                {
                    Vector3 point = ray.GetPoint(enter);
                    Vector3 vector2 = point - this.lastHit;
                    this.lastHit = point;
                    this.controller.target.position += new Vector3(vector2.x, 0f, 0f);
                }
            }
        }

        private void OnPress(bool pressed)
        {
            this.leftEdge = -this.controller.width * (this.controller.pageCount - 1);
            this.rightEdge = 0f;
            if (((this.controller.target != null) && (this.controller.width > 0f)) && (this.controller.pageCount >= 1))
            {
                if (pressed)
                {
                    this.controller.stop();
                    this.lastHit = UICamera.lastHit.point;
                    this.lastTime = Time.timeSinceLevelLoad;
                    this.lastX = this.controller.target.transform.localPosition.x;
                    Transform transform = UICamera.currentCamera.transform;
                    this.plane = new Plane((Vector3) (transform.rotation * Vector3.back), this.lastHit);
                }
                else
                {
                    Vector3 localPosition = this.controller.target.transform.localPosition;
                    int page = 0;
                    if (localPosition.x <= this.leftEdge)
                    {
                        localPosition.x = this.leftEdge;
                        page = this.controller.pageCount - 1;
                    }
                    else if (localPosition.x >= this.rightEdge)
                    {
                        localPosition.x = this.rightEdge;
                        page = 0;
                    }
                    else
                    {
                        float num2 = localPosition.x - this.lastX;
                        float num3 = Time.timeSinceLevelLoad - this.lastTime;
                        float f = num2 / num3;
                        float num5 = localPosition.x / this.controller.width;
                        if (Mathf.Abs(f) > 200f)
                        {
                            num5 = (f <= 0f) ? Mathf.Floor(num5) : Mathf.Ceil(num5);
                        }
                        page = -Mathf.RoundToInt(num5);
                        localPosition.x = this.controller.getX(page);
                    }
                    this.controller.setLastPage(page);
                    if (this.controller.disableColliderInChildren)
                    {
                        this.controller.disableColliderInChildrenExcept(page);
                    }
                    this.controller.moveToTarget(localPosition);
                }
            }
        }
    }
}

