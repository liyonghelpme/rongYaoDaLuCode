﻿namespace com.game.ui
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class UIDragXPage : MonoBehaviour
    {
        private float lastLocalPostionX;
        private float leftEdge;
        private Vector3 mLastPos;
        [SerializeField]
        private bool mLoop;
        private Plane mPlane;
        public string open_all_page = "OpenAllPage";
        public string page_moved_name = "OnTurnPage";
        private UIPageController pageController;
        public int pageSize;
        private float pressLastTime;
        private float rightEdge;
        public Vector3 scale = Vector3.one;
        public float startPageX;
        public Transform target;
        public string x_postion_notify_name = "OnDragLastMoveX";

        private int getCurrentPage()
        {
            return (int) ((this.target.transform.localPosition.x - this.startPageX) / ((float) this.pageSize));
        }

        private float getDestX(int page)
        {
            return (this.startPageX - (this.pageSize * page));
        }

        public int GetPageCount()
        {
            return this.pageController.pageCount;
        }

        public int GetPageSize()
        {
            return this.pageSize;
        }

        private void moveTarget(Vector3 dest)
        {
            SpringPosition position = SpringPosition.Begin(this.target.gameObject, dest, 5f);
            position.ignoreTimeScale = true;
            position.worldSpace = false;
            DragXPostion postion = new DragXPostion {
                moved = true,
                x = dest.x
            };
            this.target.SendMessage(this.x_postion_notify_name, postion, SendMessageOptions.DontRequireReceiver);
        }

        private void OnDrag(Vector2 delta)
        {
            if (((base.enabled && base.gameObject.active) && ((this.target != null) && (this.pageSize > 0))) && (this.pageController.pageCount >= 1))
            {
                Ray ray = UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.lastTouchPosition);
                float enter = 0f;
                if (this.mPlane.Raycast(ray, out enter))
                {
                    Vector3 point = ray.GetPoint(enter);
                    Vector3 direction = point - this.mLastPos;
                    this.mLastPos = point;
                    if ((direction.x != 0f) || (direction.y != 0f))
                    {
                        direction = this.target.InverseTransformDirection(direction);
                        direction.Scale(this.scale);
                        direction = this.target.TransformDirection(direction);
                    }
                    this.target.position += direction;
                }
            }
        }

        private void OnImmediatelyTurnToPage(int page)
        {
            if ((page >= 0) && (page < this.pageController.pageCount))
            {
                this.stopTarget();
                Vector3 localPosition = this.target.transform.localPosition;
                localPosition.x = this.getDestX(page);
                this.target.transform.localPosition = localPosition;
            }
        }

        private void OnPress(bool pressed)
        {
            List<GameObject> allPages = this.target.gameObject.GetComponent<UIPageController>().GetAllPages();
            float y = allPages[0].transform.localPosition.y;
            float z = allPages[0].transform.localPosition.z;
            if (this.mLoop)
            {
                allPages[0].transform.localPosition = new Vector3(0f, y, z);
                allPages[11].transform.localPosition = new Vector3((float) (this.pageSize * (this.pageController.pageCount - 1)), y, z);
            }
            if (((base.enabled && base.gameObject.active) && ((this.target != null) && (this.pageSize > 0))) && (this.pageController.pageCount >= 1))
            {
                if (pressed)
                {
                    this.sendOpenAllPage();
                    this.stopTarget();
                    this.mLastPos = UICamera.lastHit.point;
                    this.pressLastTime = Time.timeSinceLevelLoad;
                    this.lastLocalPostionX = this.target.transform.localPosition.x;
                    Transform transform = UICamera.currentCamera.transform;
                    this.mPlane = new Plane((Vector3) (transform.rotation * Vector3.back), this.mLastPos);
                }
                else
                {
                    Vector3 localPosition = this.target.transform.localPosition;
                    int page = 0;
                    if (localPosition.x <= this.leftEdge)
                    {
                        localPosition.x = this.leftEdge;
                        page = this.pageController.pageCount - 1;
                        if (this.mLoop)
                        {
                            float num4 = this.target.transform.localPosition.x - this.lastLocalPostionX;
                            float num5 = Time.timeSinceLevelLoad - this.pressLastTime;
                            float f = num4 / num5;
                            if (Mathf.Abs(f) > 200f)
                            {
                                float num7 = this.target.transform.localPosition.x + (this.pageSize * (this.pageController.pageCount - 1));
                                page = 0;
                                localPosition.x = this.getDestX(page);
                                float num8 = this.target.transform.localPosition.y;
                                float num9 = this.target.transform.localPosition.z;
                                this.target.transform.localPosition = new Vector3((localPosition.x + this.pageSize) + num7, num8, num9);
                                allPages[this.pageController.pageCount - 1].transform.localPosition = new Vector3((float) -this.pageSize, y, z);
                            }
                        }
                    }
                    else if (localPosition.x >= this.rightEdge)
                    {
                        localPosition.x = this.rightEdge;
                        page = 0;
                        if (this.mLoop)
                        {
                            float num10 = this.target.transform.localPosition.x - this.lastLocalPostionX;
                            float num11 = Time.timeSinceLevelLoad - this.pressLastTime;
                            float num12 = num10 / num11;
                            if (Mathf.Abs(num12) > 200f)
                            {
                                float x = this.target.transform.localPosition.x;
                                page = this.GetPageCount() - 1;
                                localPosition.x = this.getDestX(page);
                                float num14 = this.target.transform.localPosition.y;
                                float num15 = this.target.transform.localPosition.z;
                                this.target.transform.localPosition = new Vector3((localPosition.x - this.pageSize) + x, num14, num15);
                                allPages[0].transform.localPosition = new Vector3((float) (this.pageSize * this.pageController.pageCount), y, z);
                            }
                        }
                    }
                    else
                    {
                        float num16 = localPosition.x - this.lastLocalPostionX;
                        float num17 = Time.timeSinceLevelLoad - this.pressLastTime;
                        float num18 = num16 / num17;
                        float num19 = (localPosition.x - this.startPageX) / ((float) this.pageSize);
                        if (Mathf.Abs(num18) > 200f)
                        {
                            if (num18 > 0f)
                            {
                                num19 = Mathf.Ceil(num19);
                            }
                            else
                            {
                                num19 = Mathf.Floor(num19);
                            }
                        }
                        page = -Mathf.RoundToInt(num19);
                        localPosition.x = this.getDestX(page);
                    }
                    this.sendPage(page);
                    this.moveTarget(localPosition);
                }
            }
        }

        private void OnTurnToPage(int page)
        {
            if ((page >= 0) && (page < this.pageController.pageCount))
            {
                this.stopTarget();
                Vector3 localPosition = this.target.transform.localPosition;
                localPosition.x = this.startPageX - (this.pageSize * page);
                this.moveTarget(localPosition);
            }
        }

        private void sendOpenAllPage()
        {
            this.target.SendMessage(this.open_all_page, SendMessageOptions.DontRequireReceiver);
        }

        private void sendPage(int page)
        {
            this.target.SendMessage(this.page_moved_name, page, SendMessageOptions.DontRequireReceiver);
        }

        private void SetEdge()
        {
            this.leftEdge = this.startPageX - (this.pageSize * (this.pageController.pageCount - 1));
            this.rightEdge = this.startPageX;
        }

        private void Start()
        {
            this.pageController = NGUITools.FindInParents<UIPageController>(base.gameObject);
        }

        private void stopTarget()
        {
            SpringPosition component = this.target.GetComponent<SpringPosition>();
            if (component != null)
            {
                component.enabled = false;
            }
            DragXPostion postion = new DragXPostion {
                moved = false
            };
            this.target.SendMessage(this.x_postion_notify_name, postion, SendMessageOptions.DontRequireReceiver);
        }

        private void Update()
        {
            this.SetEdge();
            if ((this.mLoop && (this.pageSize > 0)) && (this.pageController.pageCount >= 2))
            {
                List<GameObject> allPages = this.target.gameObject.GetComponent<UIPageController>().GetAllPages();
                float y = allPages[0].transform.localPosition.y;
                float z = allPages[0].transform.localPosition.z;
                allPages[0].transform.localPosition = new Vector3(0f, y, z);
                allPages[this.pageController.pageCount - 1].transform.localPosition = new Vector3((float) (this.pageSize * (this.pageController.pageCount - 1)), y, z);
                Vector3 localPosition = this.target.transform.localPosition;
                if ((localPosition.x < this.pageSize) && (localPosition.x > 0f))
                {
                    allPages[this.pageController.pageCount - 1].transform.localPosition = new Vector3((float) -this.pageSize, y, z);
                }
                float num3 = -this.pageSize * (this.pageController.pageCount - 1);
                if (localPosition.x <= num3)
                {
                    allPages[0].transform.localPosition = new Vector3((float) (this.pageSize * this.pageController.pageCount), y, z);
                }
            }
        }

        public bool Loop
        {
            get
            {
                return this.mLoop;
            }
            set
            {
                this.mLoop = value;
            }
        }
    }
}

