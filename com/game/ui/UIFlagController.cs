﻿namespace com.game.ui
{
    using com.game.utils;
    using com.u3d.bases.loader;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("NGUI/UI/Flag Controller")]
    public class UIFlagController : MonoBehaviour
    {
        protected UIAtlas atlas;
        public UIDragPageController controller;
        protected UIGrid grid;
        protected GameObject groupGo;
        public const float height = 20f;
        public string selectedSprite = "selected";
        public const float space = 5f;
        protected List<UISprite> sprites = new List<UISprite>();
        public string unselectedSprite = "unselected";
        public const string url = "texture/ui/common/common";
        public const float width = 20f;

        protected void addSprite(string id)
        {
            GameObject obj2 = NGUITools.AddChild(this.groupGo);
            obj2.transform.localScale = new Vector3(20f, 20f, 1f);
            obj2.name = id;
            UISprite item = obj2.AddComponent<UISprite>();
            item.atlas = this.atlas;
            item.depth = 100;
            this.sprites.Add(item);
        }

        private void Awake()
        {
            this.atlas = ResMgr.instance.load("texture/ui/common/common", typeof(UIAtlas)) as UIAtlas;
            this.groupGo = NGUITools.AddChild(base.gameObject);
            this.groupGo.name = "group";
            this.grid = this.groupGo.AddComponent<UIGrid>();
            this.grid.cellWidth = 25f;
            this.grid.cellHeight = 25f;
        }

        public void switchFlag(int index)
        {
            foreach (UISprite sprite in this.sprites)
            {
                sprite.spriteName = (index != Convert.ToInt32(sprite.name)) ? this.unselectedSprite : this.selectedSprite;
            }
        }

        private void Update()
        {
            if ((this.controller != null) && (this.controller.target != null))
            {
                if (this.controller.pageCount != this.count)
                {
                    Tools.clearChildren(this.groupGo);
                    this.sprites.Clear();
                    for (int i = 0; i < this.controller.pageCount; i++)
                    {
                        this.addSprite(i.ToString());
                    }
                    this.count = this.controller.pageCount;
                    this.grid.repositionNow = true;
                }
                this.switchFlag(this.controller.lastPage);
            }
        }

        public int count { get; protected set; }
    }
}

