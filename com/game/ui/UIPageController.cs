﻿namespace com.game.ui
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class UIPageController : MonoBehaviour
    {
        public GameObject firstPage;
        public int lastPage;
        private float lastPostionX;
        private bool moveStart;
        public List<GameObject> pageBtns;
        public List<GameObject> pages = new List<GameObject>();

        public void addPage(GameObject page)
        {
            this.pages.Add(page);
        }

        private void clear()
        {
            this.lastPostionX = 0f;
            this.moveStart = false;
            this.lastPage = 0;
        }

        public List<GameObject> GetAllPages()
        {
            return this.pages;
        }

        public int GetLastPage()
        {
            return this.lastPage;
        }

        private void LateUpdate()
        {
            if (this.moveStart)
            {
                SpringPosition component = base.GetComponent<SpringPosition>();
                if ((component != null) && !component.enabled)
                {
                    Vector3 localPosition = base.transform.localPosition;
                    localPosition.x = this.lastPostionX;
                    base.transform.localPosition = localPosition;
                    this.moveStart = false;
                    this.LeavePage();
                    base.gameObject.SendMessage("OnTurnStop", this.lastPage, SendMessageOptions.DontRequireReceiver);
                }
            }
        }

        public void LeavePage()
        {
            if (this.pages != null)
            {
                for (int i = 0; i < this.pages.Count; i++)
                {
                    if (this.pages[i].active && (i != this.lastPage))
                    {
                        this.pages[i].SetActiveRecursively(false);
                    }
                }
                if (((this.lastPage >= 0) && (this.lastPage < this.pages.Count)) && !this.pages[this.lastPage].active)
                {
                    this.pages[this.lastPage].SetActiveRecursively(true);
                }
            }
        }

        private void OnDragLastMoveX(DragXPostion xData)
        {
            this.moveStart = xData.moved;
            this.lastPostionX = xData.x;
        }

        private void OnEnable()
        {
            if (this.firstPage != null)
            {
                this.firstPage.SendMessage("OnImmediatelyTurnToPage", this.lastPage, SendMessageOptions.DontRequireReceiver);
            }
            if (((this.pageBtns != null) && (this.lastPage >= 0)) && (this.lastPage < this.pageBtns.Count))
            {
                this.pageBtns[this.lastPage].SetActiveRecursively(true);
                this.pageBtns[this.lastPage].SendMessage("OnChooseButton", SendMessageOptions.DontRequireReceiver);
            }
            this.LeavePage();
        }

        public void OnReset()
        {
            this.pageBtns = null;
            this.pages = null;
            this.firstPage = null;
            this.clear();
        }

        private void OnTurnPage(int page)
        {
            if (((this.pageBtns != null) && (page >= 0)) && (page < this.pageBtns.Count))
            {
                this.pageBtns[page].SendMessage("OnChooseButton", SendMessageOptions.DontRequireReceiver);
                this.lastPage = page;
            }
            else if ((page >= 0) && (page < this.pages.Count))
            {
                this.lastPage = page;
            }
        }

        private void OnTurnToPage(int page)
        {
            if (((this.pages != null) && (page >= 0)) && (page < this.pages.Count))
            {
                this.OpenAllPage();
                if (this.firstPage != null)
                {
                    this.firstPage.SendMessage("OnTurnToPage", page, SendMessageOptions.DontRequireReceiver);
                }
                this.lastPage = page;
            }
        }

        public void OpenAllPage()
        {
            if (this.pages != null)
            {
                for (int i = 0; i < this.pages.Count; i++)
                {
                    if (!this.pages[i].active)
                    {
                        this.pages[i].SetActiveRecursively(true);
                    }
                }
            }
        }

        public void SimulatorOnClickTurnPage(int page)
        {
            if (((this.pages != null) && (this.pageBtns != null)) && ((page >= 0) && (page < this.pages.Count)))
            {
                this.pageBtns[page].SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
            }
        }

        public void SimulatorOnClickTurnPageImmediately(int page)
        {
            if (((this.pages != null) && (this.pageBtns != null)) && ((page >= 0) && (page < this.pages.Count)))
            {
                this.OpenAllPage();
                if (this.firstPage != null)
                {
                    this.firstPage.SendMessage("OnImmediatelyTurnToPage", page, SendMessageOptions.DontRequireReceiver);
                }
                if (page < this.pageBtns.Count)
                {
                    this.pageBtns[page].SendMessage("OnChooseButton", SendMessageOptions.DontRequireReceiver);
                }
                this.lastPage = page;
                this.LeavePage();
                base.gameObject.SendMessage("OnTurnStop", this.lastPage, SendMessageOptions.DontRequireReceiver);
            }
        }

        public void SimulatorOnClickTurnPageWithoutButton(int page)
        {
            this.OnTurnToPage(page);
        }

        public int pageCount
        {
            get
            {
                return this.pages.Count;
            }
        }
    }
}

