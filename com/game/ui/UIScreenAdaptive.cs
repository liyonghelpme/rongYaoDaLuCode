﻿namespace com.game.ui
{
    using System;
    using UnityEngine;

    public class UIScreenAdaptive : MonoBehaviour
    {
        public float correction = 1f;
        private Animation mAnim;
        private Rect mRect;
        private UIRoot mRoot;
        private Transform mTrans;
        public UIPanel panelContainer;
        public Vector2 relativeSize = Vector2.one;
        private float scr = 1f;
        public Vector2 screenSize = new Vector2(960f, 640f);
        public Style style;
        public Camera uiCamera;
        public UIWidget widgetContainer;

        private void adaptive()
        {
            if (((this.mAnim == null) || !this.mAnim.isPlaying) && (this.style != Style.None))
            {
                float pixelSizeAdjustment = 1f;
                if (this.panelContainer != null)
                {
                    if (this.panelContainer.clipping == UIDrawCall.Clipping.None)
                    {
                        this.mRect.xMin = -Screen.width * 0.5f;
                        this.mRect.yMin = -Screen.height * 0.5f;
                        this.mRect.xMax = -this.mRect.xMin;
                        this.mRect.yMax = -this.mRect.yMin;
                    }
                    else
                    {
                        Vector4 clipRange = this.panelContainer.clipRange;
                        this.mRect.x = clipRange.x - (clipRange.z * 0.5f);
                        this.mRect.y = clipRange.y - (clipRange.w * 0.5f);
                        this.mRect.width = clipRange.z;
                        this.mRect.height = clipRange.w;
                    }
                }
                else if (this.widgetContainer != null)
                {
                    Transform cachedTransform = this.widgetContainer.cachedTransform;
                    Vector3 vector2 = cachedTransform.localScale;
                    Vector3 localPosition = cachedTransform.localPosition;
                    Vector3 relativeSize = (Vector3) this.widgetContainer.relativeSize;
                    Vector3 pivotOffset = (Vector3) this.widgetContainer.pivotOffset;
                    pivotOffset.y--;
                    pivotOffset.x *= this.widgetContainer.relativeSize.x * vector2.x;
                    pivotOffset.y *= this.widgetContainer.relativeSize.y * vector2.y;
                    this.mRect.x = localPosition.x + pivotOffset.x;
                    this.mRect.y = localPosition.y + pivotOffset.y;
                    this.mRect.width = relativeSize.x * vector2.x;
                    this.mRect.height = relativeSize.y * vector2.y;
                }
                else if (this.uiCamera != null)
                {
                    this.mRect = this.uiCamera.pixelRect;
                    if (this.mRoot != null)
                    {
                        pixelSizeAdjustment = this.mRoot.pixelSizeAdjustment;
                    }
                }
                else
                {
                    return;
                }
                float width = this.mRect.width;
                float height = this.mRect.height;
                if ((pixelSizeAdjustment != 1f) && (height > 1f))
                {
                    float num4 = ((float) this.mRoot.activeHeight) / height;
                    width *= num4;
                    height *= num4;
                }
                Vector3 localScale = this.mTrans.localScale;
                if (this.style == Style.BasedOnHeight)
                {
                    localScale.x = ((this.relativeSize.x * Screen.height) / this.screenSize.y) * this.correction;
                    localScale.y = ((this.relativeSize.y * Screen.height) / this.screenSize.y) * this.correction;
                    localScale.z = localScale.x;
                    if (this.mTrans.localScale != localScale)
                    {
                        this.mTrans.localScale = localScale;
                    }
                }
                else
                {
                    if ((this.style == Style.Both) || (this.style == Style.Horizontal))
                    {
                        localScale.x = this.relativeSize.x * width;
                    }
                    if ((this.style == Style.Both) || (this.style == Style.Vertical))
                    {
                        localScale.y = this.relativeSize.y * height;
                    }
                }
                if (this.style == Style.BasedOnScreen)
                {
                    localScale.x = ((this.relativeSize.x * Screen.width) / this.screenSize.x) * this.correction;
                    localScale.y = ((this.relativeSize.y * Screen.height) / this.screenSize.y) * this.correction;
                    localScale.z = localScale.x;
                    if (this.mTrans.localScale != localScale)
                    {
                        this.mTrans.localScale = localScale;
                    }
                }
                if (this.style == Style.BasedOnWidth)
                {
                    localScale.x = ((this.relativeSize.x * Screen.width) / this.screenSize.x) * this.correction;
                    localScale.y = ((this.relativeSize.y * Screen.width) / this.screenSize.x) * this.correction;
                    localScale.z = localScale.x;
                    if (this.mTrans.localScale != localScale)
                    {
                        this.mTrans.localScale = localScale;
                    }
                }
            }
        }

        private void Awake()
        {
            this.mAnim = base.animation;
            this.mRect = new Rect();
            this.mTrans = base.transform;
            if (Screen.height < 640)
            {
                UnityEngine.Object.Destroy(this);
            }
            if (this.style == Style.None)
            {
                switch (GetRatio())
                {
                    case ScreenSize.SIZE_4TO3:
                    case ScreenSize.SIZE_5TO4:
                        this.style = Style.BasedOnWidth;
                        return;
                }
                this.style = Style.BasedOnHeight;
            }
        }

        public static ScreenSize GetRatio()
        {
            float num = ((float) Screen.width) / ((float) Screen.height);
            ScreenSize size = ScreenSize.SIZE_5TO4;
            if (Mathf.Abs((float) (num - 1.777778f)) < 0.01f)
            {
                return ScreenSize.SIZE_16TO9;
            }
            if (Mathf.Abs((float) (num - 1.6f)) < 0.01f)
            {
                return ScreenSize.SIZE_16TO10;
            }
            if (Mathf.Abs((float) (num - 1.5f)) < 0.01f)
            {
                return ScreenSize.SIZE_3TO2;
            }
            if (Mathf.Abs((float) (num - 1.333333f)) < 0.01f)
            {
                return ScreenSize.SIZE_4TO3;
            }
            if (Mathf.Abs((float) (num - 1.25f)) < 0.01f)
            {
                size = ScreenSize.SIZE_5TO4;
            }
            return size;
        }

        private void Start()
        {
            if (this.uiCamera == null)
            {
                this.uiCamera = NGUITools.FindCameraForLayer(base.gameObject.layer);
            }
            this.mRoot = NGUITools.FindInParents<UIRoot>(base.gameObject);
            this.adaptive();
        }

        private void Update()
        {
            this.adaptive();
        }

        public enum ScreenSize
        {
            SIZE_16TO9,
            SIZE_16TO10,
            SIZE_3TO2,
            SIZE_4TO3,
            SIZE_5TO4
        }

        public enum Style
        {
            None,
            Horizontal,
            Vertical,
            Both,
            BasedOnHeight,
            BasedOnWidth,
            BasedOnScreen
        }
    }
}

