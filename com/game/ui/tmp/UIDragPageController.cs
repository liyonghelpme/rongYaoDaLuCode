﻿namespace com.game.ui.tmp
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class UIDragPageController : MonoBehaviour
    {
        public int firstPage;
        public List<GameObject> pages = new List<GameObject>();
        public Transform target;
        public float width;

        public void addPage(GameObject page)
        {
            this.pages.Add(page);
        }

        private void Awake()
        {
            this.immediatelyTurnToPage(this.firstPage);
        }

        public int getCurrentPage()
        {
            return Mathf.Abs((int) (this.target.transform.localPosition.x / this.width));
        }

        public float getX(int page)
        {
            return (-this.width * page);
        }

        public void immediatelyTurnToPage(int page)
        {
            if ((page >= 0) && (page < this.pageCount))
            {
                this.stop();
                Vector3 localPosition = this.target.transform.localPosition;
                localPosition.x = this.getX(page);
                this.target.transform.localPosition = localPosition;
            }
        }

        public void moveToTarget(Vector3 dest)
        {
            SpringPosition position = SpringPosition.Begin(this.target.gameObject, dest, 5f);
            position.ignoreTimeScale = true;
            position.worldSpace = false;
        }

        public void stop()
        {
            SpringPosition component = this.target.GetComponent<SpringPosition>();
            if (component != null)
            {
                component.enabled = false;
            }
        }

        public void turnToPage(int page)
        {
            if ((page >= 0) && (page < this.pageCount))
            {
                this.stop();
                Vector3 localPosition = this.target.transform.localPosition;
                localPosition.x = -this.width * page;
                this.moveToTarget(localPosition);
            }
        }

        private void Update()
        {
            Debug.Log(this.getCurrentPage());
        }

        public int pageCount
        {
            get
            {
                return this.pages.Count;
            }
        }
    }
}

