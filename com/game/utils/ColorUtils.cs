﻿namespace com.game.utils
{
    using com.u3d.bases.debug;
    using System;
    using UnityEngine;

    public class ColorUtils
    {
        public static Color[] colors = new Color[] { Color.white, NGUITools.ParseColor("06ff00", 0), NGUITools.ParseColor("00c0ff", 0), NGUITools.ParseColor("d200ff", 0), NGUITools.ParseColor("ff9000", 0), Color.red, NGUITools.ParseColor("fff000", 0) };
        private static string[] ColorStrings = new string[] { "[FFFFFF]", "[4AD83B]", "[3B8FED]", "[B258F7]", "[FF7800]", "[FF0000]" };

        public static Color GetColor(int r, int g, int b)
        {
            return new Color(((float) r) / 255f, ((float) g) / 255f, ((float) b) / 255f);
        }

        public static string GetColorText(string color, string text)
        {
            return (color + text + "[-]");
        }

        public static string GetEquipColor(int type, string text)
        {
            return GetColorText(ColorStrings[type - 1], text);
        }

        public static void SetEqNameColor(UILabel eqname, int rank)
        {
            if ((rank < 1) || (rank >= colors.Length))
            {
                Log.info("Color Utils", "品质有问题");
                eqname.color = colors[0];
            }
            else
            {
                eqname.color = colors[rank - 1];
            }
        }
    }
}

