﻿namespace com.game.utils
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MathUtils
    {
        public static uint AttributeCalculate(uint baseNumber, float percent)
        {
            float f = baseNumber;
            f *= percent;
            f = Mathf.RoundToInt(f);
            return Convert.ToUInt32(f);
        }

        public static int AttributeCalculateInt(uint baseNumber, float percent)
        {
            float f = baseNumber;
            f *= percent;
            f = Mathf.RoundToInt(f);
            return Convert.ToInt32(f);
        }

        public static uint CalcAddNum(uint addNum, double baseNum = 200f, uint limitBaseNum = 1)
        {
            return (uint) Math.Round((double) (limitBaseNum + Math.Log((double) addNum, baseNum)));
        }

        public static Vector2 GetVector2Cost(string str)
        {
            char[] separator = new char[] { ',' };
            string[] strArray = str.Split(separator);
            if (strArray.Length == 2)
            {
                return new Vector3(float.Parse(strArray[0]), float.Parse(strArray[1]));
            }
            return new Vector2();
        }

        public static Vector3 GetVector3(string str)
        {
            str = str.Replace("[", string.Empty).Replace("]", string.Empty);
            char[] separator = new char[] { ',' };
            string[] strArray = str.Split(separator);
            if (strArray.Length == 3)
            {
                return new Vector3(float.Parse(strArray[0]), float.Parse(strArray[1]), float.Parse(strArray[2]));
            }
            if (strArray.Length == 2)
            {
                return new Vector3(float.Parse(strArray[0]), float.Parse(strArray[1]), 0f);
            }
            return new Vector3();
        }

        public static Vector3 GetVector3Cost(string str)
        {
            char[] separator = new char[] { ',' };
            string[] strArray = str.Split(separator);
            if (strArray.Length == 4)
            {
                return new Vector3(float.Parse(strArray[1]), float.Parse(strArray[2]), float.Parse(strArray[3]));
            }
            if (strArray.Length == 3)
            {
                return new Vector3(float.Parse(strArray[0]), float.Parse(strArray[1]), float.Parse(strArray[2]));
            }
            if (strArray.Length == 2)
            {
                return new Vector3(float.Parse(strArray[0]), float.Parse(strArray[1]), 0f);
            }
            return new Vector3();
        }

        public static int PercentAdd(int var, int percent)
        {
            float f = (float) (var + (((double) (var * percent)) / 10000.0));
            return (int) Mathf.Round(f);
        }

        public static int PercentMul(int var, int percent)
        {
            float f = (float) (((double) (var * percent)) / 10000.0);
            return (int) Mathf.Round(f);
        }

        public static int PercentSub(int var, int percent)
        {
            float f = (float) (var - (((double) (var * percent)) / 10000.0));
            return (int) Mathf.Max(0f, Mathf.Round(f));
        }

        public static int Random()
        {
            return UnityEngine.Random.Range(0, 0x2710);
        }

        public static int Random(int n)
        {
            return UnityEngine.Random.Range(0, n);
        }

        public static uint Random(uint n)
        {
            return (uint) Random((int) n);
        }

        public static int Random(int min, int max)
        {
            return UnityEngine.Random.Range(min, max);
        }

        public static uint Random(uint min, uint max)
        {
            return (uint) Random((int) min, (int) max);
        }

        public static int[] RandomRsequence(int min, int max, int maxLength)
        {
            int num = Random(0, maxLength);
            int[] numArray = new int[num];
            int index = 0;
            while (index < num)
            {
                int num3 = Random(min, max);
                int num4 = 0;
                while (num4 < index)
                {
                    if (num3 == numArray[num4])
                    {
                        break;
                    }
                    num4++;
                }
                if (num4 == index)
                {
                    numArray[index] = num3;
                    index++;
                }
            }
            return numArray;
        }

        public static int[] RandomSequence(int min, int max, int length)
        {
            int[] numArray = new int[length];
            int index = 0;
            while (index < length)
            {
                int num2 = Random(min, max);
                int num3 = 0;
                while (num3 < index)
                {
                    if (num2 == numArray[num3])
                    {
                        break;
                    }
                    num3++;
                }
                if (num3 == index)
                {
                    numArray[index] = num2;
                    index++;
                }
            }
            return numArray;
        }

        public static float VectorDistance(Vector3 startVector, Vector3 endVector)
        {
            startVector = new Vector3(startVector.x, 0f, startVector.z);
            endVector = new Vector3(endVector.x, 0f, endVector.z);
            return Vector3.Distance(startVector, endVector);
        }
    }
}

