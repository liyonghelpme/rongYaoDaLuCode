﻿namespace com.game.utils
{
    using System;

    public enum PanelType
    {
        NONE,
        SKILL_PANEL,
        GENERAL_INFO_PANEL,
        MAIN_VIEW,
        CORPS_PANEL,
        GENERAL_PANEL
    }
}

