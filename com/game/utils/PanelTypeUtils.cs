﻿namespace com.game.utils
{
    using com.game.module.core;
    using System;

    public class PanelTypeUtils
    {
        public static IView GetPanelByPannelType(PanelType type)
        {
            switch (type)
            {
                case PanelType.NONE:
                    return null;

                case PanelType.GENERAL_INFO_PANEL:
                    return Singleton<GeneralInfoPanel>.Instance;

                case PanelType.MAIN_VIEW:
                    return Singleton<MainView>.Instance;

                case PanelType.CORPS_PANEL:
                    return Singleton<CorpsPanel>.Instance;

                case PanelType.GENERAL_PANEL:
                    return Singleton<GeneralPanel>.Instance;
            }
            return null;
        }
    }
}

