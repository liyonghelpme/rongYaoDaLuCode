﻿namespace com.game.utils
{
    using System;
    using UnityEngine;

    public class PhoneUtil
    {
        public static void DonotSleep()
        {
            Screen.sleepTimeout = -1;
        }

        public static void Vibrate()
        {
            Handheld.Vibrate();
        }
    }
}

