﻿namespace com.game.utils
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class PointUtils
    {
        public static void ScreenByPoint(Transform tgtrans, Vector3 tgPosition, int maxScale, out Vector3 point)
        {
            Camera currentCamera = UICamera.currentCamera;
            Bounds bounds = NGUIMath.CalculateAbsoluteWidgetBounds(tgtrans);
            Vector3 position = new Vector3(tgtrans.position.x - (bounds.size.x / 2f), tgtrans.position.y - (bounds.size.y / 2f), 0f);
            position = currentCamera.WorldToScreenPoint(position);
            Vector3 vector2 = new Vector3(tgtrans.position.x + (bounds.size.x / 2f), tgtrans.position.y + (bounds.size.y / 2f), 0f);
            vector2 = currentCamera.WorldToScreenPoint(vector2);
            float num = (vector2.x - position.x) * maxScale;
            float num2 = (vector2.y - position.y) * maxScale;
            position = new Vector3(num / 2f, num2 / 2f, 0f);
            vector2 = new Vector3(Screen.width - (num / 2f), Screen.height - (num2 / 2f), 0f);
            Vector3 vector3 = currentCamera.WorldToScreenPoint(tgPosition);
            vector3.x = Mathf.Clamp(vector3.x, position.x, vector2.x);
            vector3.y = Mathf.Clamp(vector3.y, position.y, vector2.y);
            point = currentCamera.ScreenToWorldPoint(vector3);
        }
    }
}

