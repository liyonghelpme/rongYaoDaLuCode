﻿namespace com.game.utils
{
    using System;
    using System.IO;

    public class PrintPos
    {
        public static void PrintRolePos(int x, int y, int z, uint rotateY)
        {
            if (!File.Exists(posfiles))
            {
                FileStream stream = new FileStream(posfiles, FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(stream);
                writer.WriteLine(string.Concat(new object[] { x, ",", y, ",", z, ",", rotateY, "\t\n" }));
                writer.Close();
                stream.Close();
            }
            else
            {
                FileStream stream2 = new FileStream(posfiles, FileMode.Append, FileAccess.Write);
                StreamWriter writer2 = new StreamWriter(stream2);
                writer2.WriteLine(string.Concat(new object[] { x, ",", y, ",", z, ",", rotateY, "\t\n" }));
                writer2.Close();
                stream2.Close();
            }
        }

        public static string posfiles
        {
            get
            {
                return @"E:\Pos.txt";
            }
        }
    }
}

