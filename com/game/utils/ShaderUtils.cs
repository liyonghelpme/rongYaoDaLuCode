﻿namespace com.game.utils
{
    using System;
    using UnityEngine;

    public class ShaderUtils
    {
        public static void RefreshShader(Material material, string shaderName)
        {
            if (material.shader != null)
            {
                Shader shader2 = Shader.Find(shaderName);
                if (shader2 != null)
                {
                    material.shader = shader2;
                }
            }
        }

        public static void ResetAllShader()
        {
        }

        public static void ResetObjectShader(UnityEngine.Object obj)
        {
        }
    }
}

