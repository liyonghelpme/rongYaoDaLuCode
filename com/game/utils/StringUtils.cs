﻿namespace com.game.utils
{
    using com.game.basic;
    using com.game.data;
    using com.game.module.fight.vo;
    using com.game.vo;
    using PCustomDataType;
    using SimpleJSON;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityLog;

    public class StringUtils
    {
        public static bool TRUE = true;

        private static com.game.basic.AttributeInfo AttributeAddPercent(com.game.basic.AttributeInfo att, int type, float addPercent)
        {
            com.game.basic.AttributeInfo info = att;
            switch (type)
            {
                case 10:
                    info.hpPercent += addPercent;
                    return info;

                case 11:
                    info.hpFullPercent += addPercent;
                    return info;

                case 12:
                    info.attPPercent += addPercent;
                    return info;

                case 13:
                    info.attMPercent += addPercent;
                    return info;

                case 14:
                    info.defPPercent += addPercent;
                    return info;

                case 15:
                    info.defMPercent += addPercent;
                    return info;

                case 0x10:
                    info.hitPercent += addPercent;
                    return info;

                case 0x11:
                    info.dodgePercent += addPercent;
                    return info;

                case 0x12:
                    info.critPercent += addPercent;
                    return info;

                case 0x13:
                    info.flexPercent += addPercent;
                    return info;

                case 20:
                    info.abHurtPercent += addPercent;
                    return info;

                case 0x15:
                    info.abDefPercent += addPercent;
                    return info;

                case 0x25:
                    info.antiKnockDownPercent += addPercent;
                    return info;
            }
            return info;
        }

        private static com.game.basic.AttributeInfo AttributeAddValue(com.game.basic.AttributeInfo att, int type, uint value)
        {
            com.game.basic.AttributeInfo info = att;
            switch (type)
            {
                case 10:
                    info.hp += value;
                    return info;

                case 11:
                    info.hpFull += value;
                    return info;

                case 12:
                    info.attP += value;
                    return info;

                case 13:
                    info.attM += value;
                    return info;

                case 14:
                    info.defP += value;
                    return info;

                case 15:
                    info.defM += value;
                    return info;

                case 0x10:
                    info.hit += value;
                    return info;

                case 0x11:
                    info.dodge += value;
                    return info;

                case 0x12:
                    info.crit += value;
                    return info;

                case 0x13:
                    info.flex += value;
                    return info;

                case 20:
                    info.abHurt += value;
                    return info;

                case 0x15:
                    info.abDef += value;
                    return info;

                case 0x16:
                case 0x1f:
                    return info;

                case 0x17:
                    info.moveSpeed += value;
                    return info;

                case 0x18:
                    info.moveSpeedAdd += value;
                    return info;

                case 0x19:
                    info.attPPen += value;
                    return info;

                case 0x1a:
                    info.attMPen += value;
                    return info;

                case 0x1b:
                    info.attPVam += value;
                    return info;

                case 0x1c:
                    info.attMVam += value;
                    return info;

                case 0x1d:
                    info.critAdd += value;
                    return info;

                case 30:
                    info.atkSpeedAdd += value;
                    return info;

                case 0x20:
                    info.atkNormalAdd += value;
                    return info;

                case 0x21:
                    info.defPPen += value;
                    return info;

                case 0x22:
                    info.defMPen += value;
                    return info;

                case 0x23:
                    info.dmgBackP += value;
                    return info;

                case 0x24:
                    info.dmgBackM += value;
                    return info;

                case 0x25:
                    info.antiKnockDown += value;
                    return info;
            }
            return info;
        }

        public static List<SimpleBuffVo> BuffConfigToList(string str)
        {
            if (string.IsNullOrEmpty(str) || (str == "[]"))
            {
                return new List<SimpleBuffVo>();
            }
            try
            {
                string str2 = str;
                char[] separator = new char[] { '|' };
                string[] strArray = str2.Replace("[[", string.Empty).Replace("]]", string.Empty).Replace("],[", "|").Split(separator);
                List<SimpleBuffVo> list = new List<SimpleBuffVo>();
                for (int i = 0; i < strArray.Length; i++)
                {
                    char[] chArray2 = new char[] { ',' };
                    string[] strArray2 = strArray[i].Split(chArray2);
                    int num2 = 0;
                    int num3 = 0;
                    switch (strArray2.Length)
                    {
                        case 2:
                            num2 = int.Parse(strArray2[0].ToString());
                            num3 = int.Parse(strArray2[1].ToString());
                            list.Add(new SimpleBuffVo((uint) num2, (uint) num3, null));
                            break;

                        case 3:
                            num2 = int.Parse(strArray2[0].ToString());
                            num3 = int.Parse(strArray2[1].ToString());
                            list.Add(new SimpleBuffVo((uint) num2, (uint) num3, null));
                            break;

                        default:
                            throw new Exception("参数错误");
                    }
                }
                return list;
            }
            catch (Exception exception)
            {
                Debug.LogError("buff策划配表有错: id:" + str + " " + exception.ToString());
                return new List<SimpleBuffVo>();
            }
        }

        private static int CalcSpecBuffAttr(BaseRoleVo vo, uint type, int per, int basePer, int value)
        {
            float percent = per / basePer;
            int num2 = 0;
            switch (type)
            {
                case 10:
                    return (MathUtils.AttributeCalculateInt(vo.CurHp, percent) + value);

                case 11:
                    return (MathUtils.AttributeCalculateInt(vo.Hp, percent) + value);

                case 12:
                    return (MathUtils.AttributeCalculateInt(vo.AttrPhy, percent) + value);

                case 13:
                    return (MathUtils.AttributeCalculateInt(vo.AttrMag, percent) + value);

                case 14:
                    return (MathUtils.AttributeCalculateInt(vo.DefP, percent) + value);

                case 15:
                    return (MathUtils.AttributeCalculateInt(vo.DefM, percent) + value);

                case 0x10:
                case 0x11:
                case 0x12:
                case 0x13:
                case 20:
                case 0x15:
                case 0x16:
                    return num2;

                case 0x17:
                    return (MathUtils.AttributeCalculateInt(vo.MoveSpeed, percent) + value);
            }
            return num2;
        }

        public static com.game.basic.AttributeInfo CalculateAttribute(com.game.basic.AttributeInfo att)
        {
            com.game.basic.AttributeInfo info = att;
            info.hp = MathUtils.AttributeCalculate(info.hp, info.hpPercent);
            info.hpFull = MathUtils.AttributeCalculate(info.hpFull, info.hpFullPercent);
            info.attP = MathUtils.AttributeCalculate(info.attP, info.attPPercent);
            info.attM = MathUtils.AttributeCalculate(info.attM, info.attMPercent);
            info.defP = MathUtils.AttributeCalculate(info.defP, info.defPPercent);
            info.defM = MathUtils.AttributeCalculate(info.defM, info.defMPercent);
            info.hit = MathUtils.AttributeCalculate(info.hit, info.hitPercent);
            info.dodge = MathUtils.AttributeCalculate(info.dodge, info.dodgePercent);
            info.crit = MathUtils.AttributeCalculate(info.crit, info.critPercent);
            info.flex = MathUtils.AttributeCalculate(info.flex, info.flexPercent);
            info.abDef = MathUtils.AttributeCalculate(info.abDef, info.abDefPercent);
            info.abHurt = MathUtils.AttributeCalculate(info.abHurt, info.abHurtPercent);
            info.hpRe = MathUtils.AttributeCalculate(info.hpRe, info.hpRePercent);
            info.antiKnockDown = MathUtils.AttributeCalculate(info.antiKnockDown, info.antiKnockDownPercent);
            return info;
        }

        public static Vector3 DivideWith1000(Vector3 vec)
        {
            return (Vector3) (vec * 0.001f);
        }

        public static string formatCurrency(int total)
        {
            string str = total.ToString();
            if (total >= 0x5f5e100)
            {
                int num = total / 0x2710;
                return string.Format("{0}亿", num);
            }
            if (total >= 0x2710)
            {
                int num2 = total / 0x2710;
                str = string.Format("{0}万", num2);
            }
            return str;
        }

        public static float[] GetArrayStringToFloat(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return new float[0];
            }
            string str2 = str.Substring(1, str.Length - 2).Replace("[", string.Empty).Replace("]", string.Empty);
            if (string.IsNullOrEmpty(str2))
            {
                return new float[0];
            }
            char[] separator = new char[] { ',' };
            string[] source = str2.Split(separator);
            if (source.Count<string>() <= 0)
            {
                return new float[0];
            }
            float[] numArray = new float[source.Count<string>()];
            for (int i = 0; i < source.Count<string>(); i++)
            {
                if (!string.IsNullOrEmpty(source[i]))
                {
                    numArray[i] = float.Parse(source[i]);
                }
            }
            return numArray;
        }

        public static int[] GetArrayStringToInt(string str)
        {
            char[] separator = new char[] { ',' };
            string[] source = str.Substring(1, str.Length - 2).Replace("[", string.Empty).Replace("]", string.Empty).Split(separator);
            if (source.Count<string>() <= 0)
            {
                return new int[0];
            }
            int[] numArray = new int[source.Count<string>()];
            for (int i = 0; i < source.Count<string>(); i++)
            {
                if (!string.IsNullOrEmpty(source[i]))
                {
                    numArray[i] = int.Parse(source[i]);
                }
            }
            return numArray;
        }

        public static ListDictionary GetArrayStringToIntFaster(string str)
        {
            ListDictionary dictionary = new ListDictionary();
            char[] separator = new char[] { ',' };
            string[] source = str.Substring(1, str.Length - 2).Replace("[", string.Empty).Replace("]", string.Empty).Split(separator);
            if (source.Any<string>())
            {
                foreach (string str3 in source)
                {
                    if (!string.IsNullOrEmpty(str3))
                    {
                        dictionary.Add(int.Parse(str3), TRUE);
                    }
                }
            }
            return dictionary;
        }

        public static com.game.basic.AttributeInfo GetAttributeByStr(string str, com.game.basic.AttributeInfo attribute)
        {
            if ((str != "[]") && (str != null))
            {
                GetAttributeInfo(str, attribute);
                CalculateAttribute(attribute);
            }
            return attribute;
        }

        public static com.game.basic.AttributeInfo GetAttributeInfo(string str, com.game.basic.AttributeInfo attribute)
        {
            com.game.basic.AttributeInfo att = attribute;
            char[] separator = new char[] { '|' };
            string[] strArray = str.Replace("[[", string.Empty).Replace("]]", string.Empty).Replace("],[", "|").Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] chArray2 = new char[] { ',' };
                string[] strArray2 = strArray[i].Split(chArray2);
                float addPercent = 1f;
                uint num3 = 0;
                if (strArray2.Length == 3)
                {
                    addPercent += float.Parse(strArray2[1]) / float.Parse(strArray2[2]);
                    att = AttributeAddPercent(att, int.Parse(strArray2[0].ToString()), addPercent);
                }
                else
                {
                    num3 = uint.Parse(strArray2[1].ToString());
                    att = AttributeAddValue(att, int.Parse(strArray2[0].ToString()), num3);
                }
            }
            return att;
        }

        public static void GetAttrRange(string str, out int attrMin, out int attrMax)
        {
            str = GetValueString(str);
            string[] strArray = SplitVoString(str, "],[");
            attrMin = 0;
            attrMax = 0;
            char[] separator = new char[] { ',' };
            string[] strArray2 = strArray[0].Split(separator);
            char[] trimChars = new char[] { '[' };
            int num = int.Parse(strArray2[0].TrimStart(trimChars));
            char[] chArray3 = new char[] { ']' };
            int num2 = int.Parse(strArray2[1].TrimEnd(chArray3));
            attrMin = num;
            attrMax = num2;
            for (int i = 1; i < strArray.Length; i++)
            {
                char[] chArray4 = new char[] { ',' };
                strArray2 = strArray[i].Split(chArray4);
                char[] chArray5 = new char[] { '[' };
                num = int.Parse(strArray2[0].TrimStart(chArray5));
                char[] chArray6 = new char[] { ']' };
                num2 = int.Parse(strArray2[1].TrimEnd(chArray6));
                attrMin = (attrMin >= num) ? num : attrMin;
                attrMax = (attrMax <= num2) ? num2 : attrMax;
            }
        }

        public static List<SimpleBuffVo> GetBuffFromString(string str)
        {
            List<SimpleBuffVo> list = new List<SimpleBuffVo>();
            JSONArray asArray = JSON.Parse(str).AsArray;
            Log.AI(null, "Buff Count " + asArray.Count);
            IEnumerator enumerator = asArray.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    JSONNode current = (JSONNode) enumerator.Current;
                    int asInt = current[0].AsInt;
                    int num2 = current[1].AsInt;
                    SimpleBuffVo item = new SimpleBuffVo((uint) asInt, (uint) num2, null);
                    list.Add(item);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            return list;
        }

        public List<PDamageBuff> GetBuffListDuringSkill(SysSkillBaseVo skillvo)
        {
            return GetDamageBuffListFromString(skillvo.buff_list);
        }

        public static int GetCharLength(string str)
        {
            int num = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if ((char.ConvertToUtf32(str, i) >= Convert.ToInt32("4e00", 0x10)) && (char.ConvertToUtf32(str, i) <= Convert.ToInt32("9fff", 0x10)))
                {
                    num += 2;
                }
                else
                {
                    num++;
                }
            }
            return num;
        }

        public static List<PDamageBuff> GetDamageBuffListFromString(string str)
        {
            List<PDamageBuff> list = new List<PDamageBuff>();
            if ((str != "[]") && (str != string.Empty))
            {
                int[] arrayStringToInt = GetArrayStringToInt(str);
                int num = 0;
                int num2 = arrayStringToInt.Length / 3;
                while (num < num2)
                {
                    int num3 = UnityEngine.Random.Range(1, 0x2710);
                    int num4 = arrayStringToInt[num * 3];
                    int num5 = arrayStringToInt[(num * 3) + 1];
                    int num6 = arrayStringToInt[(num * 3) + 2];
                    if (num3 <= num6)
                    {
                        PDamageBuff item = new PDamageBuff {
                            buffId = (uint) num4,
                            lvl = (byte) num5
                        };
                        list.Add(item);
                    }
                    num++;
                }
            }
            return list;
        }

        public static int[][] GetDoubleDimensionalArrayStringToInt(string str)
        {
            string[] strArray = SplitVoString(str.Substring(1, str.Length - 2), "],[");
            int[][] numArray = new int[strArray.Length][];
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] separator = new char[] { ',' };
                string[] strArray2 = GetValueString(strArray[i]).Split(separator);
                numArray[i] = new int[] { int.Parse(strArray2[0]), int.Parse(strArray2[1].ToString()) };
            }
            return numArray;
        }

        public static Dictionary<int, int> GetDoubleDimensionalDicStringToInt(string str)
        {
            string[] strArray = SplitVoString(str.Substring(1, str.Length - 2), "],[");
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] separator = new char[] { ',' };
                string[] strArray2 = GetValueString(strArray[i]).Split(separator);
                dictionary.Add(int.Parse(strArray2[0]), int.Parse(strArray2[1].ToString()));
            }
            return dictionary;
        }

        public static com.game.basic.AttributeInfo GetMonBornAttributeInfo(BaseRoleVo vo, string str, com.game.basic.AttributeInfo attribute)
        {
            if ((str == "[]") || (str == null))
            {
                return attribute;
            }
            com.game.basic.AttributeInfo att = attribute;
            char[] separator = new char[] { '|' };
            string[] strArray = str.Replace("[[", string.Empty).Replace("]]", string.Empty).Replace("],[", "|").Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] chArray2 = new char[] { ',' };
                string[] strArray2 = strArray[i].Split(chArray2);
                if (strArray2.Length == 4)
                {
                    uint type = uint.Parse(strArray2[0].ToString());
                    int per = int.Parse(strArray2[1].ToString());
                    int basePer = int.Parse(strArray2[2].ToString());
                    uint num5 = uint.Parse(strArray2[3].ToString());
                    att = MonBornAttributeAddValue(vo, att, type, per, basePer, num5);
                }
            }
            return att;
        }

        public static List<MonRandomSkill> GetMonRandSkill(SysMonsterVo vo)
        {
            string str = vo.mon_rand_skills;
            List<MonRandomSkill> list = new List<MonRandomSkill>();
            switch (str)
            {
                case string.Empty:
                case "[]":
                    return list;
            }
            int[] arrayStringToInt = GetArrayStringToInt(str);
            int length = arrayStringToInt.Length;
            int num2 = 0;
            for (int i = 0; i < length; i += 2)
            {
                num2 += arrayStringToInt[i + 1];
                MonRandomSkill item = new MonRandomSkill {
                    skill_id = arrayStringToInt[i],
                    possiblity = num2
                };
                list.Add(item);
            }
            return list;
        }

        public static string[] GetMonsterList(string str)
        {
            str = str.Replace("[", string.Empty);
            str = str.Replace("]", string.Empty);
            str = Regex.Replace(str, @"\{[0-9]+,", string.Empty);
            str = Regex.Replace(str, @"[0-9]+\},", string.Empty);
            str = Regex.Replace(str, @"[0-9]+\}", string.Empty);
            str = str.Substring(0, str.Length - 2);
            str = str.Replace(" ", string.Empty);
            char[] separator = new char[] { ',' };
            return str.Split(separator);
        }

        public static List<KeyValueInfo> GetMonsterPaoPaoDic(string condition)
        {
            List<KeyValueInfo> list = new List<KeyValueInfo>();
            condition = GetValueString(condition);
            condition = condition.Replace("][", "#");
            char[] separator = new char[] { '#' };
            string[] strArray = condition.Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                int index = strArray[i].IndexOf(",");
                KeyValueInfo item = new KeyValueInfo {
                    key = strArray[i].Substring(0, index)
                };
                strArray[i] = strArray[i].Replace(item.key + ",", string.Empty);
                index = strArray[i].IndexOf(",");
                item.descriptId = strArray[i].Substring(0, index);
                item.value = strArray[i].Substring(index + 1);
                list.Add(item);
            }
            return list;
        }

        public static string GetNoSuffixString(string str)
        {
            return str.Remove(str.LastIndexOf("."));
        }

        public static List<PDamageBuff> GetPDamgeDamageBuffsFromBuffListWhenUseSkill(string str)
        {
            List<PDamageBuff> list = new List<PDamageBuff>();
            if (str.Length >= "[[0,1,2]]".Length)
            {
                int[] arrayStringToInt = GetArrayStringToInt(str);
                for (int i = 0; i < arrayStringToInt.Length; i += 3)
                {
                    int num2 = arrayStringToInt[i + 1];
                    int num3 = arrayStringToInt[i + 2];
                    PDamageBuff item = new PDamageBuff {
                        buffId = Convert.ToUInt32(num2),
                        lvl = Convert.ToByte(num3)
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public static List<SimpleBuffVo> GetSimpleBuffVoFromBuffListWhenUseSkill(string str)
        {
            List<SimpleBuffVo> list = new List<SimpleBuffVo>();
            if (str.Length >= "[[0,1,2]]".Length)
            {
                int[] arrayStringToInt = GetArrayStringToInt(str);
                for (int i = 0; i < arrayStringToInt.Length; i += 3)
                {
                    int num2 = arrayStringToInt[i + 1];
                    int num3 = arrayStringToInt[i + 2];
                    list.Add(new SimpleBuffVo(Convert.ToUInt32(num2), Convert.ToUInt32(num3), null));
                }
            }
            return list;
        }

        public static List<SimpleBuffVo> GetSimpleBuffVoListFromString(string str)
        {
            List<SimpleBuffVo> list = new List<SimpleBuffVo>();
            if ((str != "[]") && (str != string.Empty))
            {
                int[] arrayStringToInt = GetArrayStringToInt(str);
                int num = 0;
                int num2 = arrayStringToInt.Length / 3;
                while (num < num2)
                {
                    int num3 = UnityEngine.Random.Range(1, 0x2710);
                    int num4 = arrayStringToInt[num * 3];
                    int num5 = arrayStringToInt[(num * 3) + 1];
                    int num6 = arrayStringToInt[(num * 3) + 2];
                    if (num3 <= num6)
                    {
                        SimpleBuffVo item = new SimpleBuffVo((uint) num4, (uint) num5, null);
                        list.Add(item);
                    }
                    num++;
                }
            }
            return list;
        }

        public static int GetSpecBuffAttr(BaseRoleVo vo, string str)
        {
            if ((str != "[]") && (str != null))
            {
                char[] separator = new char[] { ',' };
                string[] strArray = str.Replace("[", string.Empty).Replace("]", string.Empty).Split(separator);
                if (strArray.Length == 5)
                {
                    uint type = uint.Parse(strArray[0].ToString());
                    int per = int.Parse(strArray[2].ToString());
                    int basePer = int.Parse(strArray[3].ToString());
                    int num4 = int.Parse(strArray[4].ToString());
                    return CalcSpecBuffAttr(vo, type, per, basePer, num4);
                }
            }
            return 0;
        }

        public static float[] GetStringToFloat(string str)
        {
            if ((str == null) || (str.Length <= 2))
            {
                return new float[0];
            }
            char[] separator = new char[] { ',' };
            string[] strArray = str.Substring(1, str.Length - 2).Split(separator);
            float[] numArray = new float[strArray.Length];
            for (int i = 0; i < strArray.Length; i++)
            {
                numArray[i] = float.Parse(strArray[i]);
            }
            return numArray;
        }

        public static int[] GetStringToInt(string str)
        {
            if ((str == null) || (str.Length <= 2))
            {
                return new int[0];
            }
            char[] separator = new char[] { ',' };
            string[] strArray = str.Substring(1, str.Length - 2).Split(separator);
            int[] numArray = new int[strArray.Length];
            for (int i = 0; i < strArray.Length; i++)
            {
                numArray[i] = int.Parse(strArray[i]);
            }
            return numArray;
        }

        public static DateTime GetTimeFromUnixTimestamp(long timestamp)
        {
            DateTime time = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(0x7b2, 1, 1));
            TimeSpan span = new TimeSpan(timestamp * 0x989680L);
            return time.Add(span);
        }

        public static DateTime GetTimeFromUnixTimestamp(string timestamp)
        {
            DateTime time = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(0x7b2, 1, 1));
            long ticks = long.Parse(timestamp + "0000000");
            TimeSpan span = new TimeSpan(ticks);
            return time.Add(span);
        }

        public static string[] GetValueCost(string str)
        {
            string[] strArray = SplitVoString(str, "],");
            for (int i = 0; i < strArray.Length; i++)
            {
                char[] trimChars = new char[] { '[' };
                strArray[i] = strArray[i].TrimStart(trimChars);
                char[] chArray2 = new char[] { ']' };
                strArray[i] = strArray[i].TrimEnd(chArray2);
            }
            return strArray;
        }

        public static string[] GetValueListFromString(string str, char separator = ',')
        {
            char[] trimChars = new char[] { '[' };
            str = str.TrimStart(trimChars);
            char[] chArray2 = new char[] { ']' };
            str = str.TrimEnd(chArray2);
            str = str.Replace(" ", string.Empty);
            char[] chArray3 = new char[] { separator };
            return str.Split(chArray3);
        }

        public static string GetValueString(string str)
        {
            str = str.Replace(" ", string.Empty);
            char[] trimChars = new char[] { '[' };
            str = str.TrimStart(trimChars);
            char[] chArray2 = new char[] { ']' };
            str = str.TrimEnd(chArray2);
            return str;
        }

        public static string GetValueStringWithBlank(string str)
        {
            char[] trimChars = new char[] { '[' };
            str = str.TrimStart(trimChars);
            char[] chArray2 = new char[] { ']' };
            str = str.TrimEnd(chArray2);
            return str;
        }

        private static List<BloodCheck> HandleBloodEventString(string hpEventsStr)
        {
            if (hpEventsStr == null)
            {
                return null;
            }
            char[] separator = new char[] { '&' };
            string[] strArray = hpEventsStr.Remove(0, 2).Split(separator);
            List<BloodCheck> list = new List<BloodCheck>();
            foreach (string str in strArray)
            {
                char[] chArray2 = new char[] { '#' };
                string[] strArray3 = str.Split(chArray2);
                BloodCheck item = new BloodCheck {
                    percent = int.Parse(strArray3[0]),
                    skill_id = int.Parse(strArray3[1]),
                    type = int.Parse(strArray3[2])
                };
                list.Add(item);
            }
            return list;
        }

        private static List<DeadCheck> HandleDeadSkillEventString(string deadSkillEventStr)
        {
            if (deadSkillEventStr == null)
            {
                return null;
            }
            char[] separator = new char[] { '#' };
            string[] strArray = deadSkillEventStr.Remove(0, 2).Split(separator);
            List<DeadCheck> list = new List<DeadCheck>();
            DeadCheck item = new DeadCheck {
                skill_id = int.Parse(strArray[0]),
                type = int.Parse(strArray[1])
            };
            list.Add(item);
            return list;
        }

        private static List<RebornCheck> HandleRebornEventString(string rebornEventsStr)
        {
            if (rebornEventsStr == null)
            {
                return null;
            }
            char[] separator = new char[] { ',' };
            string[] strArray = rebornEventsStr.Remove(0, 2).Split(separator);
            List<RebornCheck> list = new List<RebornCheck>();
            RebornCheck item = new RebornCheck {
                reborn_template_id = int.Parse(strArray[0]),
                max_reborn_count = uint.Parse(strArray[1])
            };
            list.Add(item);
            return list;
        }

        private static List<CycleCheck> HandleTimeEventString(string timeEventsStr)
        {
            if (timeEventsStr == null)
            {
                return null;
            }
            char[] separator = new char[] { ',' };
            string[] strArray = timeEventsStr.Remove(0, 2).Split(separator);
            List<CycleCheck> list = new List<CycleCheck>();
            foreach (string str in strArray)
            {
                char[] chArray2 = new char[] { '#' };
                string[] strArray3 = str.Split(chArray2);
                CycleCheck item = new CycleCheck {
                    time = int.Parse(strArray3[0]),
                    skill_id = int.Parse(strArray3[1]),
                    type = int.Parse(strArray3[2])
                };
                list.Add(item);
            }
            return list;
        }

        public static bool isEmpty(string param)
        {
            return ((param == null) || (param.Trim().Length <= 0));
        }

        public static bool isEquals(string param1, string param2)
        {
            return (((param1 != null) && (param2 != null)) && param1.Equals(param2));
        }

        public static bool IsNumber(string s, int precision, int scale)
        {
            if ((precision == 0) && (scale == 0))
            {
                return false;
            }
            string pattern = @"(^\d{1," + precision + "}";
            if (scale > 0)
            {
                string str2 = pattern;
                object[] objArray1 = new object[] { str2, @"\.\d{0,", scale, "}$)|", pattern };
                pattern = string.Concat(objArray1);
            }
            pattern = pattern + "$)";
            return Regex.IsMatch(s, pattern);
        }

        public static bool IsValidConfigParam(string param)
        {
            return (((param != null) && (param.Trim().Length > 0)) && ("0" != param));
        }

        private static com.game.basic.AttributeInfo MonBornAttributeAddValue(BaseRoleVo vo, com.game.basic.AttributeInfo att, uint type, int per, int basePer, uint value)
        {
            com.game.basic.AttributeInfo info = att;
            float percent = per / basePer;
            switch (type)
            {
                case 10:
                    info.hp += MathUtils.AttributeCalculate(vo.CurHp, percent) + value;
                    return info;

                case 11:
                    info.hpFull += MathUtils.AttributeCalculate(vo.Hp, percent) + value;
                    return info;

                case 12:
                    info.attP += MathUtils.AttributeCalculate(vo.AttrPhy, percent) + value;
                    return info;

                case 13:
                    info.attM += MathUtils.AttributeCalculate(vo.AttrMag, percent) + value;
                    return info;

                case 14:
                    info.defP += MathUtils.AttributeCalculate(vo.DefP, percent) + value;
                    return info;

                case 15:
                    info.defM += MathUtils.AttributeCalculate(vo.DefM, percent) + value;
                    return info;

                case 0x10:
                case 0x11:
                case 0x12:
                case 0x13:
                case 20:
                case 0x15:
                case 0x16:
                    return info;

                case 0x17:
                    info.moveSpeed += MathUtils.AttributeCalculate(vo.MoveSpeed, percent) + value;
                    return info;
            }
            return info;
        }

        public static ActionCheck SplitStringToActionScript(string str)
        {
            ActionCheck check = new ActionCheck();
            if ((str != "none") && (str != null))
            {
                char[] separator = new char[] { '|' };
                string[] strArray = str.Split(separator);
                string timeEventsStr = null;
                string hpEventsStr = null;
                string rebornEventsStr = null;
                string deadSkillEventStr = null;
                foreach (string str6 in strArray)
                {
                    if (str6.StartsWith("9"))
                    {
                        timeEventsStr = str6;
                    }
                    else if (str6.StartsWith("3"))
                    {
                        hpEventsStr = str6;
                    }
                    else if (str6.StartsWith("5"))
                    {
                        rebornEventsStr = str6;
                    }
                    else if (str6.StartsWith("4"))
                    {
                        deadSkillEventStr = str6;
                    }
                }
                check.cycle_check = HandleTimeEventString(timeEventsStr);
                check.blood_check = HandleBloodEventString(hpEventsStr);
                check.reborn_check = HandleRebornEventString(rebornEventsStr);
                check.dead_check = HandleDeadSkillEventString(deadSkillEventStr);
            }
            return check;
        }

        public static Dictionary<int, List<Vector3>> SplitStringToPathPosList(string str)
        {
            Dictionary<int, List<Vector3>> dictionary = new Dictionary<int, List<Vector3>>();
            if ((str != "[]") && !string.IsNullOrEmpty(str))
            {
                char[] separator = new char[] { '{' };
                string[] strArray = str.Split(separator);
                int length = strArray.Length;
                for (int i = 1; i < length; i++)
                {
                    char[] chArray2 = new char[] { ',' };
                    string[] strArray2 = strArray[i].Replace("[[", string.Empty).Replace("]", string.Empty).Replace("}", string.Empty).Replace("[", string.Empty).Split(chArray2);
                    int key = int.Parse(strArray2[0]);
                    List<Vector3> list = new List<Vector3>();
                    int num4 = (strArray2.Length - 1) / 3;
                    for (int j = 0; j < num4; j++)
                    {
                        list.Add(new Vector3(int.Parse(strArray2[1 + (j * 3)]) * 0.001f, int.Parse(strArray2[2 + (j * 3)]) * 0.001f, int.Parse(strArray2[3 + (j * 3)]) * 0.001f));
                    }
                    dictionary.Add(key, list);
                }
            }
            return dictionary;
        }

        public static string[] SplitVoString(string str, string delimiter = ",")
        {
            str = str.Replace(" ", string.Empty);
            char[] trimChars = new char[] { '[' };
            str = str.TrimStart(trimChars);
            char[] chArray2 = new char[] { ']' };
            str = str.TrimEnd(chArray2);
            str = str.Replace(delimiter, ":");
            char[] separator = new char[] { ':' };
            return str.Split(separator);
        }

        public static Vector2 StringToVector2(string str)
        {
            if (str != "[]")
            {
                return MathUtils.GetVector2Cost(GetValueString(str));
            }
            return new Vector2(0f, 0f);
        }

        public static Vector3 StringToVector3(string str)
        {
            if (str != "[]")
            {
                return MathUtils.GetVector3Cost(GetValueString(str));
            }
            return new Vector3(0f, 0f, 0f);
        }
    }
}

