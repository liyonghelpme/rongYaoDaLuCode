﻿namespace com.game.utils
{
    using System;

    public class TimeProp
    {
        public int Days;
        public int Hours;
        public int Minutes;
        public int Seconds;
    }
}

