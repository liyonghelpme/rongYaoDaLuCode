﻿namespace com.game.utils
{
    using com.game.module.SystemData;
    using System;

    public class TimeUtil
    {
        public static TimeProp GetElapsedTime(uint timeStamp)
        {
            TimeProp prop = new TimeProp();
            if (timeStamp > 0)
            {
                DateTime now = DateTime.Now;
                DateTime time = GetTime(timeStamp.ToString());
                TimeSpan span = (TimeSpan) (now - time);
                prop.Days = span.Days;
                prop.Hours = span.Hours;
                prop.Minutes = span.Minutes;
                prop.Seconds = span.Seconds;
                return prop;
            }
            prop.Days = 0;
            prop.Hours = 0;
            prop.Minutes = 0;
            prop.Seconds = 0;
            return prop;
        }

        public static DateTime GetTime(string timeStamp)
        {
            DateTime time = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(0x7b2, 1, 1));
            long ticks = long.Parse(timeStamp + "0000000");
            TimeSpan span = new TimeSpan(ticks);
            return time.Add(span);
        }

        public static string GetTimeHhmmss(int leftTime)
        {
            int num = leftTime / 0xe10;
            string str = (num >= 10) ? (num + string.Empty) : ("0" + num);
            int num2 = (leftTime - (num * 0xe10)) / 60;
            string str2 = (num2 >= 10) ? (num2 + string.Empty) : ("0" + num2);
            int num3 = leftTime % 60;
            string str3 = (num3 >= 10) ? (num3 + string.Empty) : ("0" + num3);
            string[] textArray1 = new string[] { str, ":", str2, ":", str3 };
            return string.Concat(textArray1);
        }

        public static string GetTimeHm(uint timeStamp)
        {
            uint num = timeStamp / 0xe10;
            string str = (num >= 10) ? (num + string.Empty) : ("0" + num);
            uint num2 = (timeStamp % 0xe10) / 60;
            string str2 = (num2 >= 10) ? (num2 + string.Empty) : ("0" + num2);
            return (str + ":" + str2);
        }

        public static TimeSpan GetTimeOfToday()
        {
            DateTime time = GetTime(ServerTime.Instance.Timestamp.ToString());
            DateTime time2 = new DateTime(time.Year, time.Month, time.Day);
            return (TimeSpan) (time - time2);
        }

        public static string GetTimeStrOfToday()
        {
            TimeSpan timeOfToday = GetTimeOfToday();
            string str = string.Empty;
            if (timeOfToday.Hours < 10)
            {
                str = str + "0";
            }
            str = str + timeOfToday.Hours + ":";
            if (timeOfToday.Minutes < 10)
            {
                str = str + "0";
            }
            str = str + timeOfToday.Minutes + ":";
            if (timeOfToday.Seconds < 10)
            {
                str = str + "0";
            }
            return (str + timeOfToday.Seconds);
        }

        public static string GetTimeYyyymmdd(uint timeStamp)
        {
            return string.Format("{0:yyyy-MM-dd}", GetTime(timeStamp.ToString()));
        }

        public static string GetTimeYyyymmddHhmmss(uint timeStamp)
        {
            return string.Format("{0:yyyy/MM/dd HH:mm:ss}", GetTime(timeStamp.ToString()));
        }
    }
}

