﻿namespace com.game.utils
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public static class Tools
    {
        public static GameObject addChild(GameObject parent, float layer = 0f)
        {
            GameObject obj2 = new GameObject();
            if (parent != null)
            {
                Transform transform = obj2.transform;
                transform.parent = parent.transform;
                transform.localPosition = new Vector3(0f, 0f, layer);
                transform.localRotation = Quaternion.identity;
                transform.localScale = Vector3.one;
                obj2.layer = parent.layer;
            }
            return obj2;
        }

        public static GameObject addChild(GameObject parent, GameObject prefab, float layer = 0f)
        {
            GameObject obj2 = UnityEngine.Object.Instantiate(prefab) as GameObject;
            if ((obj2 != null) && (parent != null))
            {
                Transform transform = obj2.transform;
                transform.parent = parent.transform;
                if (obj2.name == "uiroot(Clone)")
                {
                    transform.localPosition = new Vector3(5000f, 5000f, layer);
                }
                else
                {
                    transform.localPosition = new Vector3(0f, 0f, layer);
                }
                transform.localRotation = Quaternion.identity;
                transform.localScale = Vector3.one;
                obj2.layer = parent.layer;
            }
            return obj2;
        }

        public static void clearChildren(GameObject gameObject)
        {
            foreach (GameObject obj2 in getChildren(gameObject))
            {
                NGUITools.Destroy(obj2);
            }
        }

        public static GameObject find(GameObject gameObject, string url)
        {
            Transform transform = gameObject.transform.Find(url);
            if (null != transform)
            {
                return transform.gameObject;
            }
            return null;
        }

        public static int getActiveChildCount(GameObject gameObject)
        {
            int num = 0;
            IEnumerator enumerator = gameObject.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    if (NGUITools.GetActive(current.gameObject))
                    {
                        num++;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            return num;
        }

        public static List<GameObject> getChildren(GameObject gameObject)
        {
            List<GameObject> list = new List<GameObject>();
            IEnumerator enumerator = gameObject.transform.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    GameObject item = current.gameObject;
                    list.Add(item);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            return list;
        }

        public static GameObject getParent(GameObject gameObject)
        {
            return gameObject.transform.parent.gameObject;
        }

        public static void setLayerRecursively(GameObject parent, int layer)
        {
            if (null != parent)
            {
                parent.layer = layer;
                IEnumerator enumerator = parent.transform.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        Transform current = (Transform) enumerator.Current;
                        if (null != current)
                        {
                            setLayerRecursively(current.gameObject, layer);
                        }
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable == null)
                    {
                    }
                    disposable.Dispose();
                }
            }
        }
    }
}

