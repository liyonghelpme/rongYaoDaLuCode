﻿namespace com.game.utils
{
    using com.game.module.effect;
    using System;

    public class UrlUtils
    {
        public static string effectSkillUrl(string id)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            return ("effect/skill/" + id + "/" + id);
        }

        public static string effectSkillUrlByIdAndType(string id, int type)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            object[] objArray1 = new object[] { "effect/skill/", id, "/model/", type };
            return string.Concat(objArray1);
        }

        private static string GetEffectPrePath(EffectType type)
        {
            switch (type)
            {
                case EffectType.Normal:
                case EffectType.Bullet:
                    return "Skill/";

                case EffectType.Scene:
                    return "Scene/";

                case EffectType.Buff:
                    return "Buff/";

                case EffectType.State:
                    return "State/";

                case EffectType.UI:
                    return "UI/";
            }
            return "Skill/";
        }

        public static string GetEffectUrl(string effectId, EffectType type)
        {
            return ("Effect/" + GetEffectPrePath(type) + effectId + ".assetbundle");
        }

        public static string GetMainEffectUrl(string effectId)
        {
            if (!isIdValid(effectId))
            {
                return string.Empty;
            }
            return ("Effect/Main/" + effectId + ".assetbundle");
        }

        public static string GetSkillEffectUrl(string effectId)
        {
            if (!isIdValid(effectId))
            {
                return string.Empty;
            }
            return ("Effect/Skill/" + effectId + ".assetbundle");
        }

        public static string GetStoryMovieEffectUrl(string effectId)
        {
            if (!isIdValid(effectId))
            {
                return string.Empty;
            }
            return ("Effect/Story/Movie/" + effectId + ".assetbundle");
        }

        public static string GetStorySceneEffectUrl(string effectId)
        {
            if (!isIdValid(effectId))
            {
                return string.Empty;
            }
            return ("Effect/Story/SceneEffect/" + effectId + ".assetbundle");
        }

        public static string GetStoryScriptUrl(string name)
        {
            return ("Xml/Story/" + name + ".assetbundle");
        }

        public static string GetUIEffectUrl(string effectId)
        {
            if (!isIdValid(effectId))
            {
                return string.Empty;
            }
            return ("Effect/UI/" + effectId + ".assetbundle");
        }

        public static string horseModeUrl(string id)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            return ("mode/horse/" + id + "/" + id);
        }

        private static bool isIdValid(string id)
        {
            return (!StringUtils.isEmpty(id) && !("0" == id));
        }

        public static string monsterBustUrl(string id)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            return ("Model/Monster/" + id + "_bust/Model/BIP.assetbundle");
        }

        public static string monsterModeUrl(string id)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            return ("Model/Monster/" + id + "/Model/BIP.assetbundle");
        }

        public static string npcBustUrl(string id)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            return ("Model/Npc/" + id + "_bust/Model/BIP.assetbundle");
        }

        public static string npcModeUrl(string id)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            return ("Model/Npc/" + id + "/Model/BIP.assetbundle");
        }

        public static string roleModeUrl(int carser, int sex)
        {
            if ((carser < 1) || (sex < 0))
            {
                return string.Empty;
            }
            object[] objArray1 = new object[] { "mode/role/", carser, "_", sex, "/", carser, "_", sex };
            return string.Concat(objArray1);
        }

        public static string weaponModeUrl(string id)
        {
            if (!isIdValid(id))
            {
                return string.Empty;
            }
            return ("mode/weapon/" + id + "/" + id);
        }
    }
}

