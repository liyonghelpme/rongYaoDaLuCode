﻿namespace com.game.utils
{
    using com.game.manager;
    using System;
    using System.Collections.Generic;

    public static class VarMgr
    {
        private static Dictionary<int, int> _values = new Dictionary<int, int>();
        private static int _vipMax = -1;
        public static int NumBuyArenalMax = 0;
        public static int numBuyEnterEliteDugeon = 0;
        public static int numBuyGoldMax = 5;
        public static int NumBuySkillPointMax = 0;
        public static int NumBuyVigourMax = 0;
        public const int SecondsPerVigour = 360;
        public static bool VipSweep = false;
        public static bool VipSweep10 = false;

        public static int GetInt(int type)
        {
            if (_values.ContainsKey(type))
            {
                return _values[type];
            }
            return 0;
        }

        public static void SetInt(int type, int value)
        {
            if (_values.ContainsKey(type))
            {
                _values[type] = value;
            }
            else
            {
                _values.Add(type, value);
            }
        }

        public static int VipMax
        {
            get
            {
                if (_vipMax < 0)
                {
                    _vipMax = -1;
                    do
                    {
                        _vipMax++;
                    }
                    while (BaseDataMgr.instance.GetVIPInfo((uint) _vipMax) != null);
                    _vipMax--;
                }
                return _vipMax;
            }
        }
    }
}

