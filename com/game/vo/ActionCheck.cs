﻿namespace com.game.vo
{
    using System;
    using System.Collections.Generic;

    public class ActionCheck
    {
        public List<AttackedCheck> attacked_check = new List<AttackedCheck>();
        public List<BloodBroad> blood_broad = new List<BloodBroad>();
        public List<BloodCheck> blood_check = new List<BloodCheck>();
        public List<CycleCheck> cycle_check = new List<CycleCheck>();
        public List<DeadCheck> dead_check = new List<DeadCheck>();
        public List<FightCycleCheck> fight_cycle_check = new List<FightCycleCheck>();
        public bool is_move;
        public List<LimitTar> limit_tar = new List<LimitTar>();
        public List<MonRandomSkill> mon_rand_skill = new List<MonRandomSkill>();
        public bool no_hart;
        public List<RebornCheck> reborn_check = new List<RebornCheck>();
        public List<RelyCheck> rely_check = new List<RelyCheck>();
    }
}

