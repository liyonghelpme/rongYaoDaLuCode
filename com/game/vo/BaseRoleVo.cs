﻿namespace com.game.vo
{
    using com.game;
    using com.game.module.fight.vo;
    using com.game.module.WiFiPvP;
    using com.game.Public.Message;
    using com.liyong;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.display.vo;
    using PCustomDataType;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class BaseRoleVo : DisplayVo
    {
        private uint _abDef;
        private uint _abDefEncode1 = 1;
        private uint _abDefEncode2 = 2;
        private uint _abHurt;
        private uint _abHurtEncode1 = 1;
        private uint _abHurtEncode2 = 2;
        private uint _antiKnockBack;
        private uint _antiKnockBackEncode1 = 1;
        private uint _antiKnockBackEncode2 = 2;
        private uint _antiKnockDown;
        private uint _antiKnockDownEncode1 = 1;
        private uint _antiKnockDownEncode2 = 2;
        private uint _atkCdDec;
        private uint _atkCdDecEncode1 = 1;
        private uint _atkCdDecEncode2 = 2;
        private uint _atkNormalAdd;
        private uint _atkNormalAddEncode1 = 1;
        private uint _atkNormalAddEncode2 = 2;
        private uint _atkSpeedAdd;
        private uint _atkSpeedAddEncode1 = 1;
        private uint _atkSpeedAddEncode2 = 2;
        private uint _attMPen;
        private uint _attMPenEncode1 = 1;
        private uint _attMPenEncode2 = 2;
        private uint _attMVam;
        private uint _attMVamEncode1 = 1;
        private uint _attMVamEncode2 = 2;
        private uint _attPPen;
        private uint _attPPenEncode1 = 1;
        private uint _attPPenEncode2 = 2;
        private uint _attPVam;
        private uint _attPVamEncode1 = 1;
        private uint _attPVamEncode2 = 2;
        private uint _attrMag;
        private uint _attrMagEncode1 = 1;
        private uint _attrMagEncode2 = 2;
        private uint _attrPhy;
        private uint _attrPhyEncode1 = 1;
        private uint _attrPhyEncode2 = 2;
        private ActionControler _controler;
        private uint _crit;
        private uint _critAdd;
        private uint _critAddEncode1 = 1;
        private uint _critAddEncode2 = 2;
        private uint _critEncode1 = 1;
        private uint _critEncode2 = 2;
        protected uint _curHp;
        protected uint _curHpEncode1 = 1;
        protected uint _curHpEncode2 = 2;
        private uint _curMp;
        private uint _curMpEncode1 = 1;
        private uint _curMpEncode2 = 2;
        private uint _defM;
        private uint _defMEncode1 = 1;
        private uint _defMEncode2 = 2;
        private uint _defMPen;
        private uint _defMPenEncode1 = 1;
        private uint _defMPenEncode2 = 2;
        private uint _defP;
        private uint _defPEncode1 = 1;
        private uint _defPEncode2 = 2;
        private uint _defPPen;
        private uint _defPPenEncode1 = 1;
        private uint _defPPenEncode2 = 2;
        private uint _dmgBackM;
        private uint _dmgBackMEncode1 = 1;
        private uint _dmgBackMEncode2 = 2;
        private uint _dmgBackP;
        private uint _dmgBackPEncode1 = 1;
        private uint _dmgBackPEncode2 = 2;
        private int _dodge;
        private int _dodgeEncode1 = 1;
        private int _dodgeEncode2 = 2;
        private uint _fight;
        private uint _fightEncode1 = 1;
        private uint _fightEncode2 = 2;
        private uint _fightGeneral;
        private uint _fightGeneralEncode1 = 1;
        private uint _fightGeneralEncode2 = 2;
        private uint _flex;
        private uint _flexEncode1 = 1;
        private uint _flexEncode2 = 2;
        private uint _hit;
        private uint _hitEncode1 = 1;
        private uint _hitEncode2 = 2;
        private uint _hp;
        private uint _hpEncode1 = 1;
        private uint _hpEncode2 = 2;
        private uint _hpRe;
        private uint _hpReEncode1 = 1;
        private uint _hpReEncode2 = 2;
        private bool _isImprisoned;
        private bool _isUnbeatable;
        private int _level;
        private int _levelEncode1 = 1;
        private int _levelEncode2 = 2;
        private uint _moveSpeed;
        private uint _moveSpeedAdd;
        private uint _moveSpeedAddEncode1 = 1;
        private uint _moveSpeedAddEncode2 = 2;
        private uint _moveSpeedEncode1 = 1;
        private uint _moveSpeedEncode2 = 2;
        private uint _mp;
        private uint _mpEncode1 = 1;
        private uint _mpEncode2 = 2;
        private readonly StateVo _stateInfo = new StateVo();
        public FightAttributeVo buffAttVo = new FightAttributeVo();
        private uint camp;
        private PNewAttr equipAttr;
        public FightAttributeVo fightAttVo = new FightAttributeVo();
        public HpProtectedElem HpProtected;
        public uint HurtDownResist;
        public byte job;
        public uint lastHp;
        public uint mapId;
        public float MoveRatio = 1f;
        public bool NeedKeep;
        private uint previousCamp;
        public static uint RandomNumber1 = ((uint) UnityEngine.Random.Range(1, 0x186a0));
        public static uint RandomNumber2 = ((uint) UnityEngine.Random.Range(0x65, 0x3e7));
        public float toX;
        public float toY;
        public float toZ;

        public void AddHp(float delta)
        {
            this.AddHp(Convert.ToUInt32(delta));
        }

        public virtual void AddHp(uint delta)
        {
            if (delta != 0)
            {
                if ((this.CurHp + delta) > this.Hp)
                {
                    this.CurHp = this.Hp;
                }
                else
                {
                    this.CurHp += delta;
                }
                CommandHandler.AddCommandStatic(this.Controller.Me.GoBase, string.Concat(new object[] { "updatehp ", this.CurHp, " ", this.Hp }), 1f);
            }
        }

        public void AddHpByPercent(float percent)
        {
            if ((percent > 0f) && (this.CurHp != this.Hp))
            {
                percent = (percent <= 1f) ? percent : 1f;
                float delta = this.Hp * percent;
                this.AddHp(delta);
            }
        }

        public void AddHpByPercent(uint percent)
        {
            if ((percent > 0) && (this.CurHp != this.Hp))
            {
                percent = (percent <= 100) ? percent : 100;
                float num = percent * 0.01f;
                this.AddHp((float) (this.Hp * num));
            }
        }

        public void AddHpToPercent(float percent)
        {
            if ((percent > 0f) && (this.CurHp != this.Hp))
            {
                percent = (percent <= 1f) ? percent : 1f;
                uint num = Convert.ToUInt32((float) (this.Hp * percent));
                uint delta = (this.CurHp <= num) ? (num - this.CurHp) : 0;
                this.AddHp(delta);
            }
        }

        public void AddHpToPercent(uint percent)
        {
            if ((percent > 0) && (this.CurHp != this.Hp))
            {
                percent = (percent <= 100) ? percent : 100;
                float num = percent * 0.01f;
                uint num2 = Convert.ToUInt32((float) (this.Hp * num));
                uint delta = (this.CurHp <= num2) ? (num2 - this.CurHp) : 0;
                this.AddHp(delta);
            }
        }

        private uint CheckHpProtected(uint delta)
        {
            if (this.HpProtected == null)
            {
                return delta;
            }
            uint num2 = 0;
            switch (this.HpProtected.type)
            {
                case HpProtectedType.by_value:
                    num2 = Convert.ToUInt32(this.HpProtected.value);
                    break;

                case HpProtectedType.by_ratio:
                    num2 = Convert.ToUInt32((float) ((this.HpProtected.value * 0.001f) * this.Hp));
                    break;
            }
            if (this.CurHp <= num2)
            {
                return 0;
            }
            if (this.CurHp > (num2 + delta))
            {
                return delta;
            }
            return (this.CurHp - num2);
        }

        private bool CheckInt(int encodeValue1, int encodeValue2, int originalValue)
        {
            return (((encodeValue1 == (((originalValue + RandomNumber2) << 4) + RandomNumber1)) && (encodeValue2 == (((originalValue + RandomNumber1) << 2) + RandomNumber2))) || (((encodeValue1 == 1) && (encodeValue2 == 2)) && (originalValue == 0)));
        }

        private bool CheckUint(uint encodeValue1, uint encodeValue2, ulong originalValue)
        {
            return (((encodeValue1 == (((originalValue + RandomNumber2) << 4) + RandomNumber1)) && (encodeValue2 == (((originalValue + RandomNumber1) << 2) + RandomNumber2))) || (((encodeValue1 == 1) && (encodeValue2 == 2)) && (originalValue == 0L)));
        }

        private uint CombineAttr(uint basic, uint equip)
        {
            if (this.camp == 0)
            {
                Log.AI(this.Controller, string.Concat(new object[] { " myside attribute ", basic, " equip ", equip }));
                return (basic + ((uint) (equip * MeVo.instance.GetSoulPercent())));
            }
            Log.AI(this.Controller, string.Concat(new object[] { "enemySide attribute", basic, " equip ", equip }));
            return (basic + ((uint) (equip * Mathf.Clamp01(((float) WifiPvpManager.Instance.EnemySoul) / 100f))));
        }

        private void Encodeint(out int encodeValue1, out int encodeValue2, int originalValue)
        {
            encodeValue1 = (int) (((originalValue + RandomNumber2) << 4) + RandomNumber1);
            encodeValue2 = (int) (((originalValue + RandomNumber1) << 2) + RandomNumber2);
        }

        private void EncodeUint(out uint encodeValue1, out uint encodeValue2, uint originalValue)
        {
            encodeValue1 = ((originalValue + RandomNumber2) << 4) + RandomNumber1;
            encodeValue2 = ((originalValue + RandomNumber1) << 2) + RandomNumber2;
        }

        public float GetMonMoveSpeed()
        {
            if (AppMap.Instance.IsInMainCity)
            {
                return 2f;
            }
            Log.AI(this.Controller.gameObject, string.Concat(new object[] { "speed is ", this.MoveSpeed, " MoveAdd ", this.MoveSpeedAdd, " rate ", this.fightAttVo.MoveSpeedRate }));
            return Mathf.Max((float) (((this.MoveSpeed * 0.01) * (1f + this.MoveSpeedAdd)) * this.fightAttVo.MoveSpeedRate), 0f);
        }

        private float GetPvpBuff()
        {
            if (WifiPvpManager.Instance.GetTimecost() > 300)
            {
                return 2f;
            }
            return 1f;
        }

        public float GetRoleMoveSpeed()
        {
            if (AppMap.Instance.IsInMainCity)
            {
                return 3f;
            }
            return Mathf.Max((float) (((this.MoveSpeed * 0.01) * (1f + this.MoveSpeedAdd)) * this.fightAttVo.MoveSpeedRate), 0f);
        }

        private void HandleCheat()
        {
            MessageManager.Show("系统监测到您正在作弊，请立即终止当前行为，否则将做封号处理！");
        }

        private float Int2Float(uint val)
        {
            return (((float) val) / 10000f);
        }

        public void ReduceHp(float delta)
        {
            this.ReduceHp(Convert.ToUInt32(delta));
        }

        public virtual void ReduceHp(uint delta)
        {
            if (delta != 0)
            {
                delta = this.CheckHpProtected(delta);
                if (this.CurHp > delta)
                {
                    this.CurHp -= delta;
                }
                else
                {
                    this.SetToZero();
                }
                CommandHandler.AddCommandStatic(this.Controller.Me.GoBase, string.Concat(new object[] { "updatehp ", this.CurHp, " ", this.Hp }), 1f);
            }
        }

        public void ReduceHpByPercent(float percent)
        {
            if ((percent > 0f) && (this.CurHp != 0))
            {
                percent = (percent <= 1f) ? percent : 1f;
                uint delta = Convert.ToUInt32((float) (this.Hp * percent));
                if (this.CurHp < delta)
                {
                    this.SetToZero();
                }
                else
                {
                    this.ReduceHp(delta);
                }
            }
        }

        public void ReduceHpByPercent(uint percent)
        {
            if ((percent > 0) && (this.CurHp != 0))
            {
                percent = (percent <= 100) ? percent : 100;
                float num = percent * 0.01f;
                uint delta = Convert.ToUInt32((float) (this.Hp * num));
                if (this.CurHp < delta)
                {
                    this.SetToZero();
                }
                else
                {
                    this.ReduceHp(delta);
                }
            }
        }

        public void ReduceHpToPercent(float percent)
        {
            if (this.CurHp != 0)
            {
                percent = (percent >= 0f) ? percent : 0f;
                percent = (percent <= 1f) ? percent : (percent * 0.01f);
                uint num = Convert.ToUInt32((float) (this.Hp * percent));
                uint delta = (this.CurHp >= num) ? (num - this.CurHp) : 0;
                this.ReduceHp(delta);
            }
        }

        public void ReduceHpToPercent(uint percent)
        {
            if (this.CurHp != 0)
            {
                percent = (percent <= 100) ? percent : 100;
                float num = (percent <= 1) ? ((float) percent) : (percent * 0.01f);
                uint num2 = Convert.ToUInt32((float) (this.Hp * num));
                uint delta = (this.CurHp >= num2) ? (this.CurHp - num2) : 0;
                this.ReduceHp(delta);
            }
        }

        public void RevertToLastCamp()
        {
            this.camp = this.previousCamp;
        }

        public void SetEquipAttr(PNewAttr attr)
        {
            this.equipAttr = attr;
        }

        public virtual void SetHp(uint newHp)
        {
            this.CurHp = (newHp <= this.Hp) ? newHp : this.Hp;
            if ((this.Controller != null) && (this.Controller.Me != null))
            {
                CommandHandler.AddCommandStatic(this.Controller.Me.GoBase, string.Concat(new object[] { "updatehp ", this.CurHp, " ", this.Hp }), 1f);
            }
        }

        private void SetRoleAttrToAction(FightAttributeVo preFightVo)
        {
            if (this.fightAttVo.MoveSpeedRate != preFightVo.MoveSpeedRate)
            {
                this.Controller.ChangeSpeedTo(0f);
            }
        }

        protected virtual void SetToZero()
        {
            this.CurHp = 0;
        }

        public void UpdateAttr()
        {
            FightAttributeVo preFightVo = this.fightAttVo.ShadowCopy();
            this.fightAttVo.Clear();
            string str = this.Controller.buffController.GetAttrStr().Replace("]][[", "],[").Replace("]][", "],[").Replace("][[", "],[");
            this.fightAttVo = FightAttributeVo.GetAttributeInfo(str, this);
            this.SetRoleAttrToAction(preFightVo);
        }

        public uint AbDef
        {
            get
            {
                if (this.CheckUint(this._abDefEncode1, this._abDefEncode2, (ulong) this._abDef))
                {
                    if (this.equipAttr == null)
                    {
                        return this._abDef;
                    }
                    return this.CombineAttr(this._abDef, this.equipAttr.abDef);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._abDef = value;
                this.EncodeUint(out this._abDefEncode1, out this._abDefEncode2, this._abDef);
            }
        }

        public uint AbHurt
        {
            get
            {
                if (this.CheckUint(this._abHurtEncode1, this._abHurtEncode2, (ulong) this._abHurt))
                {
                    if (this.equipAttr == null)
                    {
                        return this._abHurt;
                    }
                    return this.CombineAttr(this._abHurt, this.equipAttr.abHurt);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._abHurt = value;
                this.EncodeUint(out this._abHurtEncode1, out this._abHurtEncode2, this._abHurt);
            }
        }

        public uint AntiKnockBack
        {
            get
            {
                if (this.CheckUint(this._antiKnockBackEncode1, this._antiKnockBackEncode2, (ulong) this._antiKnockBack))
                {
                    return this._antiKnockBack;
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._antiKnockBack = value;
                this.EncodeUint(out this._antiKnockBackEncode1, out this._antiKnockBackEncode2, this._antiKnockBack);
            }
        }

        public uint AntiKnockDown
        {
            get
            {
                if (this.CheckUint(this._antiKnockDownEncode1, this._antiKnockDownEncode2, (ulong) this._antiKnockDown))
                {
                    return this._antiKnockDown;
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._antiKnockDown = value;
                this.EncodeUint(out this._antiKnockDownEncode1, out this._antiKnockDownEncode2, this._antiKnockDown);
            }
        }

        public float AtkCdDec
        {
            get
            {
                if (this.CheckUint(this._atkCdDecEncode1, this._atkCdDecEncode2, (ulong) this._atkCdDec))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._atkCdDec, this.equipAttr.atkCdDec)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._atkCdDec), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._atkCdDec = (uint) value;
                this.EncodeUint(out this._atkCdDecEncode1, out this._atkCdDecEncode2, this._atkCdDec);
            }
        }

        public float AtkNormalAdd
        {
            get
            {
                if (this.CheckUint(this._atkNormalAddEncode1, this._atkNormalAddEncode2, (ulong) this._atkNormalAdd))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._atkNormalAdd, this.equipAttr.atkNormalAdd)), 0.3f);
                    }
                    return Mathf.Min(this.Int2Float(this._atkNormalAdd), 0.3f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._atkNormalAdd = (uint) value;
                this.EncodeUint(out this._atkNormalAddEncode1, out this._atkNormalAddEncode2, this._atkNormalAdd);
            }
        }

        public float AtkSpeedAdd
        {
            get
            {
                if (this.CheckUint(this._atkSpeedAddEncode1, this._atkSpeedAddEncode2, (ulong) this._atkSpeedAdd))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._atkSpeedAdd, this.equipAttr.atkSpeedAdd)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._atkSpeedAdd), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._atkSpeedAdd = (uint) value;
                this.EncodeUint(out this._atkSpeedAddEncode1, out this._atkSpeedAddEncode2, this._atkSpeedAdd);
            }
        }

        public float AttMPen
        {
            get
            {
                if (this.CheckUint(this._attMPenEncode1, this._attMPenEncode2, (ulong) this._attMPen))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._attMPen, this.equipAttr.attMPen)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._attMPen), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._attMPen = (uint) value;
                this.EncodeUint(out this._attMPenEncode1, out this._attMPenEncode2, this._attMPen);
            }
        }

        public float AttMVam
        {
            get
            {
                if (this.CheckUint(this._attMVamEncode1, this._attMVamEncode2, (ulong) this._attMVam))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._attMVam, this.equipAttr.attMVam)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._attMVam), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._attMVam = (uint) value;
                this.EncodeUint(out this._attMVamEncode1, out this._attMVamEncode2, this._attMVam);
            }
        }

        public float AttPPen
        {
            get
            {
                if (this.CheckUint(this._attPPenEncode1, this._attPPenEncode2, (ulong) this._attPPen))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._attPPen, this.equipAttr.attPPen)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._attPPen), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._attPPen = (uint) value;
                this.EncodeUint(out this._attPPenEncode1, out this._attPPenEncode2, this._attPPen);
            }
        }

        public float AttPVam
        {
            get
            {
                if (this.CheckUint(this._attPVamEncode1, this._attPVamEncode2, (ulong) this._attPVam))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._attPVam, this.equipAttr.attPVam)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._attPVam), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._attPVam = (uint) value;
                this.EncodeUint(out this._attPVamEncode1, out this._attPVamEncode2, this._attPVam);
            }
        }

        public uint AttrMag
        {
            get
            {
                if (this.CheckUint(this._attrMagEncode1, this._attrMagEncode2, (ulong) this._attrMag))
                {
                    if (this.equipAttr == null)
                    {
                        return this._attrMag;
                    }
                    float pvpBuff = this.GetPvpBuff();
                    return (uint) (this.CombineAttr(this._attrMag, this.equipAttr.attM) * pvpBuff);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._attrMag = value;
                this.EncodeUint(out this._attrMagEncode1, out this._attrMagEncode2, this._attrMag);
            }
        }

        public uint AttrPhy
        {
            get
            {
                if (this.CheckUint(this._attrPhyEncode1, this._attrPhyEncode2, (ulong) this._attrPhy))
                {
                    if (this.equipAttr == null)
                    {
                        return this._attrPhy;
                    }
                    float pvpBuff = this.GetPvpBuff();
                    return (uint) (this.CombineAttr(this._attrPhy, this.equipAttr.attP) * pvpBuff);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._attrPhy = value;
                this.EncodeUint(out this._attrPhyEncode1, out this._attrPhyEncode2, this._attrPhy);
            }
        }

        public uint Camp
        {
            get
            {
                return this.camp;
            }
            set
            {
                this.previousCamp = this.camp;
                this.camp = value;
            }
        }

        public ActionControler Controller
        {
            get
            {
                return this._controler;
            }
            set
            {
                this._controler = value;
                this._stateInfo.MeControler = value;
            }
        }

        public float Crit
        {
            get
            {
                if (this.CheckUint(this._critEncode1, this._critEncode2, (ulong) this._crit))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._crit, this.equipAttr.crit)), 1f);
                    }
                    return Mathf.Min(this.Int2Float(this._crit), 1f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._crit = (uint) value;
                this.EncodeUint(out this._critEncode1, out this._critEncode2, this._crit);
            }
        }

        public float CritAdd
        {
            get
            {
                if (this.CheckUint(this._critAddEncode1, this._critAddEncode2, (ulong) this._critAdd))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._critAdd, this.equipAttr.critAdd)), 0.5f);
                    }
                    return Mathf.Min(this.Int2Float(this._critAdd), 0.5f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._critAdd = (uint) value;
                this.EncodeUint(out this._critAddEncode1, out this._critAddEncode2, this._critAdd);
            }
        }

        public virtual uint CurHp
        {
            get
            {
                if (this.CheckUint(this._curHpEncode1, this._curHpEncode2, (ulong) this._curHp))
                {
                    if (this.equipAttr == null)
                    {
                        return this._curHp;
                    }
                    return this._curHp;
                }
                this.HandleCheat();
                return 1;
            }
            protected set
            {
                this._curHp = value;
                this.EncodeUint(out this._curHpEncode1, out this._curHpEncode2, this._curHp);
            }
        }

        public float CurHpPercent
        {
            get
            {
                float num = ((float) this.CurHp) / ((float) this.Hp);
                return ((num <= 1f) ? num : 1f);
            }
        }

        public virtual uint CurMp
        {
            get
            {
                if (this.CheckUint(this._curMpEncode1, this._curMpEncode2, (ulong) this._curMp))
                {
                    return this._curMp;
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._curMp = value;
                this.EncodeUint(out this._curMpEncode1, out this._curMpEncode2, this._curMp);
            }
        }

        public uint DefM
        {
            get
            {
                if (this.CheckUint(this._defMEncode1, this._defMEncode2, (ulong) this._defM))
                {
                    if (this.equipAttr == null)
                    {
                        return this._defM;
                    }
                    return this.CombineAttr(this._defM, this.equipAttr.defM);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._defM = value;
                this.EncodeUint(out this._defMEncode1, out this._defMEncode2, this._defM);
            }
        }

        public float DefMPen
        {
            get
            {
                if (this.CheckUint(this._defMPenEncode1, this._defMPenEncode2, (ulong) this._defMPen))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._defMPen, this.equipAttr.defMPen)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._defMPen), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._defMPen = (uint) value;
                this.EncodeUint(out this._defMPenEncode1, out this._defMPenEncode2, this._defMPen);
            }
        }

        public uint DefP
        {
            get
            {
                if (this.CheckUint(this._defPEncode1, this._defPEncode2, (ulong) this._defP))
                {
                    if (this.equipAttr == null)
                    {
                        return this._defP;
                    }
                    return this.CombineAttr(this._defP, this.equipAttr.defP);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._defP = value;
                this.EncodeUint(out this._defPEncode1, out this._defPEncode2, this._defP);
            }
        }

        public float DefPPen
        {
            get
            {
                if (this.CheckUint(this._defPPenEncode1, this._defPPenEncode2, (ulong) this._defPPen))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._defPPen, this.equipAttr.defPPen)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._defPPen), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._defPPen = (uint) value;
                this.EncodeUint(out this._defPPenEncode1, out this._defPPenEncode2, this._defPPen);
            }
        }

        public uint DeltaToEmptyHp
        {
            get
            {
                return this.CurHp;
            }
        }

        public uint DeltaToFullHp
        {
            get
            {
                return (this.Hp - this.CurHp);
            }
        }

        public float DmgBackM
        {
            get
            {
                if (this.CheckUint(this._dmgBackMEncode1, this._dmgBackMEncode2, (ulong) this._dmgBackM))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._dmgBackM, this.equipAttr.dmgBackM)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._dmgBackM), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._dmgBackM = (uint) value;
                this.EncodeUint(out this._dmgBackMEncode1, out this._dmgBackMEncode2, this._dmgBackM);
            }
        }

        public float DmgBackP
        {
            get
            {
                if (this.CheckUint(this._dmgBackPEncode1, this._dmgBackPEncode2, (ulong) this._dmgBackP))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._dmgBackP, this.equipAttr.dmgBackP)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._dmgBackP), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._dmgBackP = (uint) value;
                this.EncodeUint(out this._dmgBackPEncode1, out this._dmgBackPEncode2, this._dmgBackP);
            }
        }

        public float Dodge
        {
            get
            {
                if (this.CheckInt(this._dodgeEncode1, this._dodgeEncode2, this._dodge))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr((uint) this._dodge, this.equipAttr.dodge)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float((uint) this._dodge), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._dodge = (int) value;
                this.Encodeint(out this._dodgeEncode1, out this._dodgeEncode2, this._dodge);
            }
        }

        public uint Fight
        {
            get
            {
                if (this.CheckUint(this._fightEncode1, this._fightEncode2, (ulong) this._fight))
                {
                    return this._fight;
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._fight = value;
                this.EncodeUint(out this._fightEncode1, out this._fightEncode2, this._fight);
            }
        }

        public uint FightGeneral
        {
            get
            {
                if (this.CheckUint(this._fightGeneralEncode1, this._fightGeneralEncode2, (ulong) this._fightGeneral))
                {
                    return this._fightGeneral;
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._fightGeneral = value;
                this.EncodeUint(out this._fightGeneralEncode1, out this._fightGeneralEncode2, this._fightGeneral);
            }
        }

        public float Flex
        {
            get
            {
                if (this.CheckUint(this._flexEncode1, this._flexEncode2, (ulong) this._flex))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._flex, this.equipAttr.flex)), 1f);
                    }
                    return Mathf.Min(this.Int2Float(this._flex), 1f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._flex = (uint) value;
                this.EncodeUint(out this._flexEncode1, out this._flexEncode2, this._flex);
            }
        }

        public float Hit
        {
            get
            {
                if (this.CheckUint(this._hitEncode1, this._hitEncode2, (ulong) this._hit))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._hit, this.equipAttr.hit)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._hit), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._hit = (uint) value;
                this.EncodeUint(out this._hitEncode1, out this._hitEncode2, this._hit);
            }
        }

        public uint Hp
        {
            get
            {
                if (this.CheckUint(this._hpEncode1, this._hpEncode2, (ulong) this._hp))
                {
                    if (this.equipAttr == null)
                    {
                        return this._hp;
                    }
                    return this.CombineAttr(this._hp, this.equipAttr.hpFull);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._hp = value;
                this.EncodeUint(out this._hpEncode1, out this._hpEncode2, this._hp);
            }
        }

        public float HpRe
        {
            get
            {
                if (this.CheckUint(this._hpReEncode1, this._hpReEncode2, (ulong) this._hpRe))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._hpRe, this.equipAttr.hpRe)), 0.4f);
                    }
                    return Mathf.Min(this.Int2Float(this._hpRe), 0.4f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._hpRe = (uint) value;
                this.EncodeUint(out this._hpReEncode1, out this._hpReEncode2, this._hpRe);
            }
        }

        public uint HurtResist { get; set; }

        public bool IsEmptyHp
        {
            get
            {
                return (this.CurHp <= 0);
            }
        }

        public bool IsFullHp
        {
            get
            {
                return (this.CurHp >= this.Hp);
            }
        }

        public bool isImprisoned
        {
            get
            {
                return this._isImprisoned;
            }
            set
            {
                this.stateInfo.IsFrozen = this._isImprisoned = value;
            }
        }

        public bool IsUnbeatable
        {
            get
            {
                return this._isUnbeatable;
            }
            set
            {
                if (value)
                {
                    this.stateInfo.AddState(StateVo.CANT_BEAT);
                }
                else
                {
                    this.stateInfo.RemoveState(StateVo.CANT_BEAT);
                }
                this._isUnbeatable = value;
            }
        }

        public int Level
        {
            get
            {
                if (this.CheckInt(this._levelEncode1, this._levelEncode2, this._level))
                {
                    return this._level;
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._level = value;
                this.Encodeint(out this._levelEncode1, out this._levelEncode2, this._level);
            }
        }

        public uint MoveSpeed
        {
            get
            {
                if (this.CheckUint(this._moveSpeedEncode1, this._moveSpeedEncode2, (ulong) this._moveSpeed))
                {
                    if (this.equipAttr == null)
                    {
                        return this._moveSpeed;
                    }
                    return this.CombineAttr(this._moveSpeed, this.equipAttr.moveSpeed);
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._moveSpeed = value;
                this.EncodeUint(out this._moveSpeedEncode1, out this._moveSpeedEncode2, this._moveSpeed);
            }
        }

        public float MoveSpeedAdd
        {
            get
            {
                if (this.CheckUint(this._moveSpeedAddEncode1, this._moveSpeedAddEncode2, (ulong) this._moveSpeedAdd))
                {
                    if (this.equipAttr != null)
                    {
                        return Mathf.Min(this.Int2Float(this.CombineAttr(this._moveSpeedAdd, this.equipAttr.moveSpeedAdd)), 0.2f);
                    }
                    return Mathf.Min(this.Int2Float(this._moveSpeedAdd), 0.2f);
                }
                this.HandleCheat();
                return 1f;
            }
            set
            {
                this._moveSpeedAdd = (uint) value;
                this.EncodeUint(out this._moveSpeedAddEncode1, out this._moveSpeedAddEncode2, this._moveSpeedAdd);
            }
        }

        public uint Mp
        {
            get
            {
                if (this.CheckUint(this._mpEncode1, this._mpEncode2, (ulong) this._mp))
                {
                    if (this.equipAttr == null)
                    {
                        return this._mp;
                    }
                    return this._mp;
                }
                this.HandleCheat();
                return 1;
            }
            set
            {
                this._mp = value;
                this.EncodeUint(out this._mpEncode1, out this._mpEncode2, this._mp);
            }
        }

        public StateVo stateInfo
        {
            get
            {
                return this._stateInfo;
            }
        }

        public class HpProtectedElem
        {
            public BaseRoleVo.HpProtectedType type;
            public int value;

            public HpProtectedElem(BaseRoleVo.HpProtectedType type, int value)
            {
                this.type = type;
                this.value = value;
            }
        }

        public enum HpProtectedType
        {
            by_ratio = 2,
            by_value = 1
        }
    }
}

