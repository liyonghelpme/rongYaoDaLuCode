﻿namespace com.game.vo
{
    using System;

    public class BloodBroad
    {
        public float last_use_time;
        public int msg_id1;
        public int msg_id2;
        public int percent;
    }
}

