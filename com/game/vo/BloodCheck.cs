﻿namespace com.game.vo
{
    using System;

    public class BloodCheck
    {
        public float last_use_time;
        public int percent;
        public int skill_id;
        public int type;
    }
}

