﻿namespace com.game.vo
{
    using System;
    using UnityEngine;

    public class EffectVo
    {
        public int direction;
        public Vector3 eulerAngles = Vector3.zero;
        public string id;
        public bool isChangeDir;
        public string name;
        public Vector3 position;
        public Vector3 posOffset = Vector3.zero;
        public float speed;
        public GameObject target;
        public string url;
        public float waitTime;
    }
}

