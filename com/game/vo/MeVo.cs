﻿namespace com.game.vo
{
    using com.game;
    using com.game.module.core;
    using com.game.utils;
    using com.u3d.bases.display;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MeVo : PlayerVo
    {
        private uint _soul;
        public uint bindingDiamond;
        public static readonly int DataHpMpUpdate = 1;
        public static readonly int DataHpUpdate = 2;
        public static readonly int DataHurtOverPercent10Update = 3;
        public uint diam;
        public uint diamond;
        public ulong generalId;
        public ulong guildId;
        public string guildName = string.Empty;
        private com.game.vo.DataUpateHandlerWithParam handlListWithParam;
        public static MeVo instance = new MeVo();
        public uint lastLoginTime;
        public static readonly int MonsterDataHpUpdateWithParam = 1;
        public bool needEnterDuplicate;
        public uint preMapId;
        public uint repu;
        public uint serverId;

        public event MeDataUpateHandler DataUpdated;

        public event com.game.vo.DataUpateHandlerWithParam DataUpdatedWithParam
        {
            add
            {
                this.handlListWithParam = (com.game.vo.DataUpateHandlerWithParam) Delegate.Remove(this.handlListWithParam, value);
                this.handlListWithParam = (com.game.vo.DataUpateHandlerWithParam) Delegate.Combine(this.handlListWithParam, value);
            }
            remove
            {
                this.handlListWithParam = (com.game.vo.DataUpateHandlerWithParam) Delegate.Remove(this.handlListWithParam, value);
            }
        }

        public void DataUpdate(int code = 0)
        {
            MeDataUpateHandler dataUpdated = this.DataUpdated;
            if (dataUpdated != null)
            {
                foreach (MeDataUpateHandler handler2 in dataUpdated.GetInvocationList())
                {
                    handler2(this, code);
                }
            }
        }

        public void DataUpdateWithParam(int code = 0, object param = null)
        {
            com.game.vo.DataUpateHandlerWithParam handlListWithParam = this.handlListWithParam;
            if (handlListWithParam != null)
            {
                foreach (com.game.vo.DataUpateHandlerWithParam param3 in handlListWithParam.GetInvocationList())
                {
                    param3(this, code, param);
                }
            }
        }

        public float GetSoulPercent()
        {
            return Mathf.Clamp01(((float) this._soul) / 100f);
        }

        public void InitRoleBustPosition(GameObject roleBust)
        {
        }

        protected override void LoadPetCallback(BaseDisplay petDisplay)
        {
            if (AppMap.Instance.mapParserII.MapVo.type == 2)
            {
                petDisplay.Controller.AiController.SetAi(true);
            }
            else
            {
                petDisplay.Controller.AiController.SetAi(false);
            }
        }

        public string BindingDiamondStr
        {
            get
            {
                return StringUtils.formatCurrency((int) this.bindingDiamond);
            }
        }

        public string BustUrl
        {
            get
            {
                switch (base.job)
                {
                    case 1:
                        return UrlUtils.npcBustUrl("100001");

                    case 2:
                        return UrlUtils.npcBustUrl("100002");

                    case 3:
                        return UrlUtils.npcBustUrl("100003");
                }
                return UrlUtils.npcBustUrl("100003");
            }
        }

        public override uint CurHp
        {
            get
            {
                return base.CurHp;
            }
            protected set
            {
                if (value != base._curHp)
                {
                    if ((value < base._curHp) && ((((float) (base._curHp - value)) / ((float) base.Hp)) > 0.1))
                    {
                        this.DataUpdate(DataHurtOverPercent10Update);
                    }
                    base.CurHp = value;
                    this.DataUpdate(DataHpUpdate);
                }
            }
        }

        public string DiamondStr
        {
            get
            {
                return StringUtils.formatCurrency((int) this.diamond);
            }
        }

        public string DiamStr
        {
            get
            {
                return StringUtils.formatCurrency((int) this.diam);
            }
        }

        public uint soul
        {
            get
            {
                return this._soul;
            }
            set
            {
                this._soul = value;
                if (AppMap.Instance.IsInWifiPVP)
                {
                    Singleton<WifiPvpMode>.Instance.SyncSoul(this._soul);
                }
            }
        }
    }
}

