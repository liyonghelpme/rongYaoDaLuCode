﻿namespace com.game.vo
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using System;
    using System.Collections.Generic;
    using UnityLog;

    public class MonsterVo : BaseRoleVo
    {
        private SysmonsterPaoPaoVo _monsterPaoPaoTemplate;
        private SysMonsterVo _monsterVo;
        private MonsterParamStruct _paramStruct;
        public byte bornType;
        public int endure;
        public int groupIndex;
        public int max_alive_time;
        public uint monsterId;
        private List<KeyValueInfo> monsterPaoPaoList;
        public ulong parent_id;
        public int parent_template_id;
        public uint reborn_count;

        public override uint CurHp
        {
            get
            {
                return base.CurHp;
            }
            protected set
            {
                if (value != base._curHp)
                {
                    if (this._paramStruct == null)
                    {
                        this._paramStruct = new MonsterParamStruct();
                    }
                    this._paramStruct.MonsterId = (uint) this.MonsterVO.id;
                    MeVo.instance.DataUpdateWithParam(MeVo.MonsterDataHpUpdateWithParam, this._paramStruct);
                    base.CurHp = value;
                }
            }
        }

        public bool isMySide
        {
            get
            {
                return ((this.MonsterVO.type == 6) || (0 == base.Camp));
            }
        }

        public SysmonsterPaoPaoVo MonsterPaoPao
        {
            get
            {
                if (this._monsterPaoPaoTemplate == null)
                {
                    this._monsterPaoPaoTemplate = BaseDataMgr.instance.GetMonsterPaoPaoTemplateByMonsterId(this.MonsterVO.id);
                }
                return this._monsterPaoPaoTemplate;
            }
        }

        public List<KeyValueInfo> MonsterPaoPaoList
        {
            get
            {
                if ((this.monsterPaoPaoList == null) && (this.MonsterPaoPao != null))
                {
                    this.monsterPaoPaoList = StringUtils.GetMonsterPaoPaoDic(this.MonsterPaoPao.condition);
                }
                return this.monsterPaoPaoList;
            }
        }

        public SysMonsterVo MonsterVO
        {
            get
            {
                if (this._monsterVo == null)
                {
                    this._monsterVo = BaseDataMgr.instance.getSysMonsterVo(this.monsterId);
                    if (AppMap.Instance.IsInWifiPVP)
                    {
                        SysMonsterMobaVo wifiPvpMonsterVO = null;
                        if (base.Camp == 0)
                        {
                            wifiPvpMonsterVO = BaseDataMgr.instance.GetWifiPvpMonsterVO(this.monsterId, Singleton<WifiPvpMode>.Instance.GetMyTeamLevel());
                        }
                        else
                        {
                            wifiPvpMonsterVO = BaseDataMgr.instance.GetWifiPvpMonsterVO(this.monsterId, Singleton<WifiPvpMode>.Instance.GetEnemyTeamLevel());
                        }
                        if (wifiPvpMonsterVO != null)
                        {
                            Log.AI(null, " Set Monster WifiVO props ");
                            this._monsterVo.mon_props = wifiPvpMonsterVO.mon_props;
                        }
                    }
                    this.endure = this._monsterVo.endurance_initval;
                }
                return this._monsterVo;
            }
        }
    }
}

