﻿namespace com.game.vo
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.hud;
    using com.u3d.bases.display;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class PlayerVo : BaseRoleVo
    {
        private float _leftUnbeatableTime;
        private uint _petId;
        private SysRoleBaseInfoVo _SysRoleBaseInfoVo;
        [CompilerGenerated]
        private static Func<SkillInfoVo, uint> <>f__am$cache16;
        public string customFace = string.Empty;
        public uint DiamNeedForVigour;
        public ulong exp;
        public ulong expFull;
        public uint fightPoint;
        public Dictionary<uint, int> generalList = new Dictionary<uint, int>();
        public SysGeneralVo generalTemplateInfo;
        public byte hasCombine;
        public bool isInWar;
        public int lastVigourRecoverTime;
        public byte nation;
        public ushort numBuyVigour;
        public uint numDead;
        public byte sex;
        public List<SkillInfoVo> SkillInfoList = new List<SkillInfoVo>();
        public List<uint> titleList = new List<uint>();
        public ushort vigour;
        public ushort vigourFull;

        public override void AddHp(uint delta)
        {
            uint curHp = this.CurHp;
            base.AddHp(delta);
            uint num2 = this.CurHp;
            if ((base.Controller.Me.GetVo().SubType == 1) && (num2 != curHp))
            {
                uint finalAddHp = num2 - curHp;
                this.HandleHpUpdate(finalAddHp);
            }
        }

        private void DoLeftUnbeatableTimeChanged()
        {
            if (base.Controller != null)
            {
                if (base.IsUnbeatable)
                {
                    base.Controller.GetMeByType<PlayerDisplay>().ShowInvincibleEffect();
                }
                else
                {
                    base.Controller.GetMeByType<PlayerDisplay>().RemoveInvincibleEffect();
                }
            }
        }

        public List<uint> GetSkillList()
        {
            if (<>f__am$cache16 == null)
            {
                <>f__am$cache16 = item => item.skillId;
            }
            return this.SkillInfoList.Select<SkillInfoVo, uint>(<>f__am$cache16).ToList<uint>();
        }

        private void HandleHpUpdate(uint finalAddHp)
        {
            HudView.Instance.recoverBlood(base.Controller.GoHp, (float) base.Hp, (float) this.CurHp, (float) finalAddHp, true);
            HudView.AddHpText((float) finalAddHp, base.Controller.Me.GetMeByType<ActionDisplay>());
            Singleton<BattleRightView>.Instance.UpdateHeadBloodBar(this);
            if (this.CurHp == base.Hp)
            {
                HudView.Instance.stopRecoverBlood(base.Controller.GoHp);
            }
            if (AppMap.Instance.me == base.Controller.Me)
            {
                Singleton<BattleCenterView>.Instance.updateHp(this);
            }
        }

        protected virtual void LoadPetCallback(BaseDisplay petDisplay)
        {
        }

        public override void ReduceHp(uint delta)
        {
            base.ReduceHp(delta);
            if (base.Controller.Me.GetVo().SubType == 1)
            {
                Singleton<BattleRightView>.Instance.UpdateHeadBloodBar(this);
                if (AppMap.Instance.me == base.Controller.Me)
                {
                    Singleton<BattleCenterView>.Instance.updateHp(this);
                }
            }
        }

        public override void SetHp(uint newHp)
        {
            uint curHp = this.CurHp;
            base.SetHp(newHp);
            uint num2 = this.CurHp;
            if (((base.Controller != null) && (base.Controller.Me.GetVo().SubType == 1)) && (num2 != curHp))
            {
                uint finalAddHp = num2 - curHp;
                this.HandleHpUpdate(finalAddHp);
            }
        }

        protected override void SetToZero()
        {
            base.SetToZero();
            Singleton<BattleRightView>.Instance.UpdateHeadBloodBar(this);
            if (AppMap.Instance.me == base.Controller.Me)
            {
                Singleton<BattleCenterView>.Instance.updateHp(this);
            }
        }

        [DebuggerHidden]
        private IEnumerator UpdateLeftUnbeatableTime()
        {
            return new <UpdateLeftUnbeatableTime>c__Iterator43 { <>f__this = this };
        }

        public float LeftUnbeatableTime
        {
            get
            {
                return this._leftUnbeatableTime;
            }
            set
            {
                this._leftUnbeatableTime = value;
                if (this._leftUnbeatableTime > 0f)
                {
                    base.IsUnbeatable = true;
                    CoroutineManager.StartCoroutine(this.UpdateLeftUnbeatableTime(), true);
                    this.DoLeftUnbeatableTimeChanged();
                }
            }
        }

        public uint PetId
        {
            get
            {
                return this._petId;
            }
            set
            {
                this._petId = value;
            }
        }

        public uint SeqAttack { get; set; }

        public SysRoleBaseInfoVo SysRoleBaseInfo
        {
            get
            {
                if (this._SysRoleBaseInfoVo == null)
                {
                    this._SysRoleBaseInfoVo = BaseDataMgr.instance.GetSysRoleBaseInfo(base.job);
                }
                return this._SysRoleBaseInfoVo;
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateLeftUnbeatableTime>c__Iterator43 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PlayerVo <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (this.<>f__this._leftUnbeatableTime > 0f)
                        {
                            this.<>f__this._leftUnbeatableTime -= Time.deltaTime;
                            this.$current = 0;
                            this.$PC = 1;
                        }
                        else
                        {
                            this.<>f__this._leftUnbeatableTime = 0f;
                            this.<>f__this.IsUnbeatable = false;
                            this.<>f__this.DoLeftUnbeatableTimeChanged();
                            this.$current = 0;
                            this.$PC = 2;
                        }
                        return true;

                    case 2:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

