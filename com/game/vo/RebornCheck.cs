﻿namespace com.game.vo
{
    using System;

    public class RebornCheck
    {
        public float last_reborn_time;
        public uint max_reborn_count;
        public uint reborn_count;
        public int reborn_template_id;
    }
}

