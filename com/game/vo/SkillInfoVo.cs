﻿namespace com.game.vo
{
    using com.game.data;
    using com.game.manager;
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class SkillInfoVo
    {
        [CompilerGenerated]
        private static Func<SysSkillBaseVo, bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<SysSkillBaseVo, uint> <>f__am$cache3;
        public uint groupId;
        public ushort skillLvl;

        private uint GetSkillIdByGroupAndLevel(uint groupId, int level)
        {
            <GetSkillIdByGroupAndLevel>c__AnonStorey102 storey = new <GetSkillIdByGroupAndLevel>c__AnonStorey102 {
                groupId = groupId,
                level = level
            };
            if (<>f__am$cache2 == null)
            {
                <>f__am$cache2 = skillvo => skillvo.skill_lvl > 0;
            }
            if (<>f__am$cache3 == null)
            {
                <>f__am$cache3 = skillvo => (uint) skillvo.id;
            }
            return BaseDataMgr.instance.GetDicByType<SysSkillBaseVo>().Values.Cast<SysSkillBaseVo>().Where<SysSkillBaseVo>(<>f__am$cache2).Where<SysSkillBaseVo>(new Func<SysSkillBaseVo, bool>(storey.<>m__106)).Select<SysSkillBaseVo, uint>(<>f__am$cache3).FirstOrDefault<uint>();
        }

        public uint skillId
        {
            get
            {
                return this.GetSkillIdByGroupAndLevel(this.groupId, this.skillLvl);
            }
        }

        [CompilerGenerated]
        private sealed class <GetSkillIdByGroupAndLevel>c__AnonStorey102
        {
            internal uint groupId;
            internal int level;

            internal bool <>m__106(SysSkillBaseVo skillvo)
            {
                return ((skillvo.skill_group == this.groupId) && (skillvo.skill_lvl == this.level));
            }
        }
    }
}

