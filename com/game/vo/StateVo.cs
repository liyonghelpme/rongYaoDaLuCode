﻿namespace com.game.vo
{
    using com.u3d.bases.display.controler;
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class StateVo
    {
        private int _state;
        [CompilerGenerated]
        private static Func<FieldInfo, bool> <>f__am$cache13;
        public static int BLINK = 15;
        public static int CANT_ADD_DEBUFF = 9;
        public static int CANT_ATTACK = 2;
        public static int CANT_BEAT = 10;
        public static int CANT_HEAL = 7;
        public static int CANT_MOVE = 1;
        public static int CANT_SEE = 8;
        public static int CANT_SKILL1 = 3;
        public static int CANT_SKILL2 = 4;
        public static int CANT_SKILL3 = 5;
        public static int CANT_SKILL4 = 6;
        public static int ENDURED = 14;
        public static int FLOAT = 11;
        public static int KNOCK_BACK = 0x10;
        private int MAX_INDEX;
        public ActionControler MeControler;
        public static int TAUNTED = 12;
        public static int TEMPATED = 13;

        public StateVo()
        {
            this.InitMaxIndex();
        }

        public bool AddState(int stateIdx)
        {
            if ((stateIdx <= 0) || (stateIdx > this.MAX_INDEX))
            {
                return false;
            }
            int bitMask = GetBitMask(stateIdx);
            this.SetState(this._state | bitMask);
            return true;
        }

        private static int GetBitMask(int stateIdx)
        {
            return (((int) 1) << stateIdx);
        }

        private static int GetNegativeMask(int stateIdx)
        {
            return (0x7fffffff - GetBitMask(stateIdx));
        }

        private void InitMaxIndex()
        {
            if (<>f__am$cache13 == null)
            {
                <>f__am$cache13 = m => m.FieldType == typeof(int);
            }
            int count = typeof(StateVo).GetFields(BindingFlags.Public | BindingFlags.Static).Where<FieldInfo>(<>f__am$cache13).ToList<FieldInfo>().Count;
            this.MAX_INDEX = count;
        }

        public bool IsInState(int stateIdx)
        {
            int bitMask = GetBitMask(stateIdx);
            return ((this._state & bitMask) > 0);
        }

        public bool IsNoneState()
        {
            return (this._state == 0);
        }

        public bool RemoveState(int stateIdx)
        {
            if ((stateIdx <= 0) || (stateIdx > this.MAX_INDEX))
            {
                return false;
            }
            int negativeMask = GetNegativeMask(stateIdx);
            this.SetState(this._state & negativeMask);
            return true;
        }

        private void SetState(int state)
        {
            this._state = state;
        }

        public bool BeTaunted
        {
            get
            {
                return this.IsInState(TAUNTED);
            }
            set
            {
                if (value)
                {
                    this.AddState(TAUNTED);
                    if (this.MeControler != null)
                    {
                        this.MeControler.ClearPath();
                    }
                }
                else
                {
                    this.RemoveState(TAUNTED);
                }
            }
        }

        public bool BeTemptated
        {
            get
            {
                return this.IsInState(TEMPATED);
            }
            set
            {
                if (value)
                {
                    this.AddState(TEMPATED);
                    if (this.MeControler != null)
                    {
                        this.MeControler.ClearPath();
                    }
                }
                else
                {
                    this.RemoveState(TEMPATED);
                }
            }
        }

        public bool CanAttack
        {
            get
            {
                return !this.IsInState(CANT_ATTACK);
            }
        }

        public bool CanBeSeen
        {
            get
            {
                return !this.IsInState(CANT_SEE);
            }
        }

        public bool CanDebuff
        {
            get
            {
                return !this.IsInState(CANT_ADD_DEBUFF);
            }
        }

        public bool CanMove
        {
            get
            {
                return !this.IsInState(CANT_MOVE);
            }
        }

        public bool CanUseSkill
        {
            get
            {
                return (((!this.IsInState(CANT_SKILL1) && !this.IsInState(CANT_SKILL2)) && !this.IsInState(CANT_SKILL3)) && !this.IsInState(CANT_SKILL4));
            }
        }

        public bool IsDrizzle
        {
            get
            {
                return ((!this.CanMove && !this.CanUseSkill) && !this.CanAttack);
            }
            set
            {
                if (value)
                {
                    this.AddState(CANT_MOVE);
                    this.AddState(CANT_ATTACK);
                    this.AddState(CANT_SKILL1);
                    this.AddState(CANT_SKILL2);
                    this.AddState(CANT_SKILL3);
                    this.AddState(CANT_SKILL4);
                    if (this.MeControler != null)
                    {
                        this.MeControler.ClearPath();
                    }
                }
                else
                {
                    this.RemoveState(CANT_MOVE);
                    this.RemoveState(CANT_ATTACK);
                    this.RemoveState(CANT_SKILL1);
                    this.RemoveState(CANT_SKILL2);
                    this.RemoveState(CANT_SKILL3);
                    this.RemoveState(CANT_SKILL4);
                }
            }
        }

        public bool IsFloat
        {
            get
            {
                return this.IsInState(FLOAT);
            }
            set
            {
                if (value)
                {
                    this.AddState(FLOAT);
                    if (this.MeControler != null)
                    {
                        this.MeControler.ClearPath();
                    }
                }
                else
                {
                    this.RemoveState(FLOAT);
                }
            }
        }

        public bool IsFrozen
        {
            get
            {
                return (this.IsInState(CANT_MOVE) && this.IsInState(CANT_BEAT));
            }
            set
            {
                if (value)
                {
                    this.AddState(CANT_BEAT);
                    this.AddState(CANT_MOVE);
                    if (this.MeControler != null)
                    {
                        this.MeControler.ClearPath();
                    }
                }
                else
                {
                    this.RemoveState(CANT_BEAT);
                    this.RemoveState(CANT_MOVE);
                }
            }
        }

        public bool IsUnbeatable
        {
            get
            {
                return this.IsInState(CANT_BEAT);
            }
        }

        public bool ShouldPauseAI
        {
            get
            {
                return (this.IsInState(TAUNTED) || this.IsInState(FLOAT));
            }
        }

        public bool ShouldPauseJoystickAndAtkBtn
        {
            get
            {
                return ((this.IsInState(TEMPATED) || this.IsInState(TAUNTED)) || this.IsInState(BLINK));
            }
        }
    }
}

