﻿namespace com.game.vo
{
    using System;

    public class StateVoOld
    {
        private int _state = 1;
        public const int DEAD = 3;
        public const int FIGHT = 2;
        public const int FROZEN = 4;
        public const int NORMAL = 1;

        public bool IsInState(int stateIndex)
        {
            int num = (stateIndex - 1) << 1;
            return ((this._state & num) > 0);
        }

        public void SetState(int state)
        {
            this._state = state;
        }

        public void SetState(int stateIndex, bool add)
        {
            int num = (stateIndex - 1) << 1;
            if (add)
            {
                this._state |= num;
            }
            else
            {
                this._state &= 0x7fffffff - num;
            }
        }

        public bool IsFrozen
        {
            get
            {
                return this.IsInState(4);
            }
        }
    }
}

