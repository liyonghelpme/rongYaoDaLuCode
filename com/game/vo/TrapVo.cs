﻿namespace com.game.vo
{
    using com.game.data;
    using com.game.manager;
    using System;

    public class TrapVo : BaseRoleVo
    {
        private SysTrap _sysTrapVo;
        public uint TrapId;

        public SysTrap SysTrapVo
        {
            get
            {
                if (this._sysTrapVo == null)
                {
                }
                return (this._sysTrapVo = BaseDataMgr.instance.GetTrapVoById(this.TrapId));
            }
        }
    }
}

