﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class AIState
    {
        protected bool quit;
        protected StateMachine stateMachine;
        public AIStateEnum type = AIStateEnum.INVALID;

        public virtual bool CheckCommand(CommandHandler.Command cmd)
        {
            return false;
        }

        public virtual bool CheckNextState(AIStateEnum s)
        {
            return true;
        }

        public virtual bool CheckSubState()
        {
            return true;
        }

        public virtual void EnterState()
        {
            this.quit = false;
        }

        public virtual void ExitState()
        {
            this.quit = true;
            Util.VoidDelegate callbackAndClean = this.stateMachine.GetCallbackAndClean(this.type);
            if (callbackAndClean != null)
            {
                callbackAndClean();
            }
        }

        public virtual string GetSubName()
        {
            return string.Empty;
        }

        [DebuggerHidden]
        public virtual IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator76();
        }

        public void SetStateMachine(StateMachine m)
        {
            this.stateMachine = m;
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator76 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        public enum AIStateEnum
        {
            DEATH = 2,
            DEATH_FLY = 6,
            HURT = 5,
            IDLE = 0,
            INVALID = -1,
            MOVE_ATTACK = 3,
            MOVE_KEYBOARD = 4,
            SKILL = 1
        }
    }
}

