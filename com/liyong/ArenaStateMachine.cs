﻿namespace com.liyong
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public sealed class ArenaStateMachine : StateMachine
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map3E;
        private CommandHandler cmdHandler;
        private CommandHandler handler;

        private void Awake()
        {
            base.AddState(new ArenaIdle());
            base.AddState(new ArenaMoveAttackEnemy());
            base.AddState(new ArenaSkill());
            base.AddState(new ArenaDeath());
        }

        public override bool CheckCommand()
        {
            if (base.cmds.Count <= 0)
            {
                return false;
            }
            base.lastCommand = base.cmds[0];
            base.cmds.RemoveAt(0);
            Log.AI(base.gameObject.name, " lastCommand " + base.lastCommand.stringCmd);
            string key = base.lastCommand.cmd[0];
            if (key != null)
            {
                int num;
                if (<>f__switch$map3E == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(5);
                    dictionary.Add("move_attack", 0);
                    dictionary.Add("idle", 1);
                    dictionary.Add("skill", 2);
                    dictionary.Add("hurt", 3);
                    dictionary.Add("death", 4);
                    <>f__switch$map3E = dictionary;
                }
                if (<>f__switch$map3E.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_ATTACK);

                        case 1:
                            return base.ChangeState(AIState.AIStateEnum.IDLE);

                        case 2:
                            return base.ChangeState(AIState.AIStateEnum.SKILL);

                        case 3:
                            return base.ChangeState(AIState.AIStateEnum.HURT);

                        case 4:
                            return base.ChangeState(AIState.AIStateEnum.DEATH);
                    }
                }
            }
            return base.GetCurState().CheckCommand(base.lastCommand);
        }

        private void Start()
        {
            this.handler = base.GetComponent<CommandHandler>();
            this.handler.AddHandler("move_attack", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("idle", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("skill", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("TryToMove", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("death", new CommandHandler.CmdHandler(this.OnCmd));
            base.ChangeState(AIState.AIStateEnum.IDLE);
        }

        private void Update()
        {
            this.handler.Execute();
        }
    }
}

