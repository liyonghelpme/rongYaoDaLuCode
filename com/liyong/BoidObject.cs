﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class BoidObject : MonoBehaviour
    {
        private static Color[] allCols = new Color[] { Color.red, Color.yellow, Color.blue, Color.cyan, Color.green };
        private BoxObject boxObject;
        private static int colId = 0;
        private Vector3 modifySpeed = Vector3.zero;
        private Color myColor;
        private int Size;
        private const int WorldScale = 100;

        [DebuggerHidden]
        private IEnumerator AdjustBoxObject()
        {
            return new <AdjustBoxObject>c__Iterator6C { <>f__this = this };
        }

        private void CalculateModify(BoxObject ignoreWho)
        {
            IEnumerable<BoxObject> neibors = this.boxObject.GetNeibors();
            Vector3 vector = new Vector3(this.boxObject.X, 0f, this.boxObject.Y);
            float num = 0f;
            float num2 = 0f;
            int boxId = this.boxObject.GetBoxId();
            IEnumerator<BoxObject> enumerator = neibors.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    BoxObject current = enumerator.Current;
                    if ((current != ignoreWho) && (current.GetBoxId() < boxId))
                    {
                        float f = this.boxObject.X - current.X;
                        float num5 = Mathf.Max((float) 0f, (float) (200f - Mathf.Abs(f)));
                        float num6 = Mathf.Sign(f);
                        num += num6 * num5;
                        f = this.boxObject.Y - current.Y;
                        num5 = Mathf.Max((float) 0f, (float) (200f - Mathf.Abs(f)));
                        num6 = Mathf.Sign(f);
                        num2 += num6 * num5;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            this.modifySpeed.x = num / 100f;
            this.modifySpeed.z = num2 / 100f;
        }

        public BoxObject GetBoxObject()
        {
            return this.boxObject;
        }

        public Vector3 GetModifySpeed(BoxObject ignoreWho)
        {
            this.CalculateModify(ignoreWho);
            Log.AI(base.gameObject, " Modify Speed is " + this.modifySpeed);
            return this.modifySpeed;
        }

        private void OnDestroy()
        {
            this.boxObject.RemoveBox();
        }

        private void OnDrawGizmos()
        {
            GridManager gridManager = this.boxObject.gridManager;
            Gizmos.color = this.myColor;
            float y = base.transform.position.y;
            foreach (int num2 in this.boxObject.gridIndex)
            {
                Vector2 vector = gridManager.GridIdToPos(num2);
                Vector3 center = new Vector3(vector.x / 100f, y, vector.y / 100f);
                Gizmos.DrawSphere(center, 0.3f);
            }
        }

        private void Start()
        {
            this.myColor = allCols[colId++ % allCols.Length];
            this.boxObject = new BoxObject();
            Vector3 position = base.transform.position;
            this.boxObject.X = position.x * 100f;
            this.boxObject.Y = (position.z * 100f) - (this.Size / 2);
            this.boxObject.Width = this.Size;
            this.boxObject.Height = this.Size;
            this.boxObject.ignoreMax = true;
            this.boxObject.gridManager = MapGridManager.Instance.GetGridManager();
            this.boxObject.AddToGridManager();
            base.StartCoroutine(this.AdjustBoxObject());
        }

        [CompilerGenerated]
        private sealed class <AdjustBoxObject>c__Iterator6C : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BoidObject <>f__this;
            internal Transform <mTransform>__0;
            internal Vector3 <pos>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<mTransform>__0 = this.<>f__this.transform;
                        break;

                    case 1:
                        break;
                        this.$PC = -1;
                        goto Label_00C2;

                    default:
                        goto Label_00C2;
                }
                this.<pos>__1 = this.<mTransform>__0.position;
                this.<>f__this.boxObject.UpdateAttribute(this.<>f__this.Size, this.<>f__this.Size, this.<pos>__1.x * 100f, (this.<pos>__1.z * 100f) - (this.<>f__this.Size / 2));
                this.$current = new WaitForSeconds(0.1f);
                this.$PC = 1;
                return true;
            Label_00C2:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

