﻿namespace com.liyong
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class BoxObject
    {
        private int boxId = -1;
        public List<int> gridIndex = new List<int>();
        public GridManager gridManager;
        public float Height;
        public bool ignoreMax;
        private static int maxId;
        public float Width;
        public float X;
        public float Y;

        public BoxObject()
        {
            this.boxId = maxId++;
        }

        public void AddToGridManager()
        {
            this.gridManager.AddBox(this);
        }

        public int GetBoxId()
        {
            return this.boxId;
        }

        public IEnumerable<BoxObject> GetNeibors()
        {
            return this.gridManager.GetNeibors(this);
        }

        public Vector2 GidToVector2(int gid)
        {
            return this.gridManager.GridIdToPos(gid);
        }

        public void RemoveBox()
        {
            this.gridManager.RemoveBox(this);
        }

        public void UpdateAttribute(int w, int h, float x, float y)
        {
            this.gridManager.RemoveBox(this);
            this.Width = w;
            this.Height = h;
            this.X = x;
            this.Y = y;
            this.gridManager.AddBox(this);
        }

        public void UpdateBox()
        {
            this.gridManager.UpdateBox(this);
        }
    }
}

