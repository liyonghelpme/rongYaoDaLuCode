﻿namespace com.liyong
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.fight.arpg;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.consts;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public static class CombatUtil
    {
        public const float CheckLastTime = 1f;
        public static readonly CheckResult FalseResult = new CheckResult();

        public static void AttackHim(GameObject attacker, GameObject enemy)
        {
            ActionDisplay meByType = enemy.GetComponent<BaseControler>().GetMeByType<ActionDisplay>();
            attacker.GetComponent<SkillController>().RequestUseSkill(0, true, false, null, null, meByType);
        }

        public static bool CanMove(GameObject g)
        {
            StateVo stateInfo = g.GetComponent<BaseControler>().Me.GetMeVoByType<BaseRoleVo>().stateInfo;
            if ((!stateInfo.IsFrozen && !stateInfo.IsDrizzle) && (!stateInfo.IsFloat && stateInfo.CanMove))
            {
                return true;
            }
            Log.AI(g, " Can't Move");
            return false;
        }

        public static void CheckAttackBeforeMove(GameObject attacker, GameObject target)
        {
            int useNormalSkillId = -1;
            if (CheckCanNormalSkill(attacker, target, out useNormalSkillId))
            {
                SkillHim(attacker, target, useNormalSkillId);
            }
            else if (CheckCanAttack(attacker, target))
            {
                AttackHim(attacker, target);
            }
            else
            {
                MoveToHim(attacker, target);
            }
        }

        public static CheckResult CheckAttackSelf(GameObject g)
        {
            UpdateAttribute.LastAttack lastAttackThis = ObjectManager.GetLastAttackThis(g);
            if (((lastAttackThis != null) && (lastAttackThis.lastAttackMe != null)) && ((Time.time - lastAttackThis.lastAttackTime) < 1f))
            {
                return new CheckResult { ok = true, enemy = lastAttackThis.lastAttackMe };
            }
            return new CheckResult();
        }

        public static CheckResult CheckAttackTarget(GameObject g)
        {
            GameObject attackTarget = ObjectManager.GetAttackTarget(g);
            if (attackTarget != null)
            {
                return new CheckResult { ok = true, enemy = attackTarget };
            }
            return FalseResult;
        }

        public static CheckResult CheckAttackTeam(GameObject g)
        {
            GameObject anotherTeamMember = ObjectManager.GetAnotherTeamMember(g);
            if (anotherTeamMember != null)
            {
                UpdateAttribute.LastAttack lastAttackThis = ObjectManager.GetLastAttackThis(anotherTeamMember);
                if (((lastAttackThis != null) && (lastAttackThis.lastAttackMe != null)) && ((Time.time - lastAttackThis.lastAttackTime) < 1f))
                {
                    return new CheckResult { ok = true, enemy = lastAttackThis.lastAttackMe };
                }
            }
            return new CheckResult();
        }

        public static bool CheckCanAttack(GameObject attacker, GameObject target)
        {
            Log.AI(attacker, " CheckInAttackRange " + target);
            if (target == null)
            {
                return false;
            }
            float num = Util.XZSqrMagnitude(attacker.transform.position, target.transform.position);
            float boundDistance = ObjectManager.GetVo(target).boundDistance;
            float num3 = !(ObjectManager.GetVo(attacker) is PlayerVo) ? GetMonsterAttackRange(attacker) : GetPlayerAttackRange(attacker);
            return (num < ((float) (num3 + boundDistance)).Square());
        }

        private static int CheckCanNormalSkill(GameObject attacker, GameObject target)
        {
            if (!CheckTargetDead(attacker, target))
            {
                Vector3 position = target.transform.position;
                float boundDistance = target.GetComponent<BaseControler>().Me.BoundDistance;
                int count = attacker.GetComponent<SkillController>().LearnedSkillList.Count;
                SkillController component = attacker.GetComponent<SkillController>();
                ActionControler controler = attacker.GetComponent<ActionControler>();
                float num3 = Util.XZSqrMagnitude(attacker.transform.position, target.transform.position);
                Log.AI(attacker, " Skill List Length " + count);
                for (int i = 4; i < count; i++)
                {
                    bool flag;
                    switch (i)
                    {
                        case 7:
                        case 3:
                        case 8:
                        {
                            continue;
                        }
                        default:
                        {
                            uint id = component.LearnedSkillList[i];
                            Log.AI(attacker, " learnSkill " + id);
                            if (id == 0)
                            {
                                continue;
                            }
                            if (!component.IsSkillCdReady(i))
                            {
                                Log.AI(attacker, " SkillCD " + i);
                                continue;
                            }
                            SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(id);
                            SysSkillActionVo sysSkillActionVo = BaseDataMgr.instance.GetSysSkillActionVo(sysSkillBaseVo.skill_group);
                            float num6 = sysSkillBaseVo.available_atk_range * 0.001f;
                            float num7 = controler.ExtendedBulletFlyDistance * 0.001f;
                            flag = false;
                            if ((sysSkillBaseVo.target_type == 2) || (sysSkillBaseVo.target_type == 1))
                            {
                                if ((attacker.GetComponent<BaseControler>().Me is MeDisplay) && (sysSkillBaseVo.subtype == 1))
                                {
                                    if (sysSkillBaseVo.param_list.Length < "[3,2]".Length)
                                    {
                                        continue;
                                    }
                                    int[] arrayStringToInt = StringUtils.GetArrayStringToInt(sysSkillBaseVo.param_list);
                                    switch (((SkillConst.HealSkillControlType) arrayStringToInt[0]))
                                    {
                                        case SkillConst.HealSkillControlType.damn_just_use_it:
                                            flag = true;
                                            goto Label_02F8;

                                        case SkillConst.HealSkillControlType.one_member_below_value:
                                            flag = SkillController.IsAtLeastOneMemberHpBelowValue(arrayStringToInt[1]);
                                            goto Label_02F8;

                                        case SkillConst.HealSkillControlType.one_member_below_ratio:
                                            flag = SkillController.IsAtLeastOneMemberHpBelowRatio(arrayStringToInt[1]);
                                            goto Label_02F8;

                                        case SkillConst.HealSkillControlType.all_member_below_value:
                                            flag = SkillController.IsAllMemberHpBelowValue(arrayStringToInt[1]);
                                            goto Label_02F8;

                                        case SkillConst.HealSkillControlType.all_member_below_ratio:
                                            flag = SkillController.IsAllMemberHpBelowRatio(arrayStringToInt[1]);
                                            goto Label_02F8;
                                    }
                                }
                                else
                                {
                                    flag = true;
                                }
                            }
                            else
                            {
                                Log.AI(attacker, string.Concat(new object[] { " beforeCheckSkill ", num3, " requireDistance ", num6, " extendDis ", num7 }));
                                if (sysSkillActionVo.IsBullet)
                                {
                                    float num8 = num6 + num7;
                                    if (num3 > (num8 * num8))
                                    {
                                        continue;
                                    }
                                    flag = true;
                                }
                                else
                                {
                                    float num9 = num6 * num6;
                                    if (num3 > (num9 * num9))
                                    {
                                        continue;
                                    }
                                    flag = DamageCheck.IsSkillCovered3D(sysSkillBaseVo, attacker.transform, position, boundDistance);
                                }
                                Log.AI(attacker, string.Concat(new object[] { " UseSkill Ok Skill ", i, " target is ", target }));
                            }
                            break;
                        }
                    }
                Label_02F8:
                    Log.AI(attacker, " CheckSkillDistance " + flag);
                    if (flag)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static bool CheckCanNormalSkill(GameObject g, GameObject target, out int useNormalSkillId)
        {
            int num = CheckCanNormalSkill(g, target);
            if (num != -1)
            {
                useNormalSkillId = num;
                return true;
            }
            useNormalSkillId = -1;
            return false;
        }

        private static bool CheckCanSkillRange(GameObject attacker, GameObject target, int skillType)
        {
            float num = Util.XZSqrMagnitude(attacker.transform.position, target.transform.position);
            float boundDistance = ObjectManager.GetVo(target).boundDistance;
            float skillAttackRange = GetSkillAttackRange(attacker, skillType);
            Log.AI(attacker, string.Concat(new object[] { " SKillRange ", skillType, " skillRange ", target, " boundDis ", boundDistance, " target ", target, " SkillRange ", skillAttackRange }));
            return (num < ((float) (skillAttackRange + boundDistance)).Square());
        }

        public static bool CheckFarAwayFromSummoner(GameObject slave)
        {
            SummonMonsterAiController component = slave.GetComponent<SummonMonsterAiController>();
            GameObject master = component.GetMaster();
            if (master == null)
            {
                return false;
            }
            float num = Util.XZSqrMagnitude(master.transform.position, slave.transform.position);
            float maxDistanceFromMaster = component.GetMaxDistanceFromMaster();
            Log.AI(slave, string.Concat(new object[] { " dist summoner ", num, " maxDist ", maxDistanceFromMaster }));
            return (num > maxDistanceFromMaster.Square());
        }

        public static bool CheckFarAwayMaster(GameObject slave)
        {
            GameObject master = ObjectManager.GetMaster();
            if (master != null)
            {
                float num = Util.XZSqrMagnitude(master.transform.position, slave.transform.position);
                float num2 = AiConfig.NEAR_THE_MASTER_GAP * 0.001f;
                Log.AI(slave, string.Concat(new object[] { " dist master ", num, " maxDist ", num2 }));
                return (num > (num2 * num2));
            }
            return false;
        }

        public static bool CheckInBattle(GameObject g)
        {
            return g.GetComponent<AiControllerBase>().IsInBattle;
        }

        public static bool CheckMonsterCanSee(GameObject attacker, GameObject target)
        {
            if (target == null)
            {
                return false;
            }
            float num = Util.XZSqrMagnitude(attacker, target);
            float monsterSightRange = GetMonsterSightRange(attacker);
            Log.AI(attacker, string.Concat(new object[] { " See Target ", target, " dist ", num, " sightRange ", monsterSightRange }));
            return (num < (monsterSightRange * monsterSightRange));
        }

        public static CheckResult CheckNewEnemy(GameObject g, GameObject oldEnemy)
        {
            ActionDisplay display = ObjectManager.FindNearestEnemy(g, null);
            if ((display != null) && (oldEnemy != null))
            {
                Log.AI(display.GoBase.name, " newEnemy " + oldEnemy.name);
            }
            if ((display != null) && (display.GoBase != oldEnemy))
            {
                Log.AI(g, " new Enemy GoBase is " + display.GoBase);
                return new CheckResult { ok = true, enemy = display.GoBase };
            }
            return FalseResult;
        }

        public static bool CheckPhysicAttack(int stateId)
        {
            return (((stateId == 3) || (stateId == 4)) || (stateId == 5));
        }

        public static bool CheckPlayerCanSee(GameObject attacker, GameObject target)
        {
            if (target == null)
            {
                return false;
            }
            float num = Util.XZSqrMagnitude(attacker, target);
            float playerSightRange = GetPlayerSightRange(attacker);
            Log.AI(attacker, string.Concat(new object[] { " See Target ", target, " dist ", num, " Player sightRange ", playerSightRange }));
            return (num < (playerSightRange * playerSightRange));
        }

        public static CheckResult CheckPrivateEnemy(GameObject g, GameObject oldEnemy)
        {
            Log.AI(g, " Private Find Nearest Tower Private Hero ");
            ActionDisplay privateEnemy = g.GetComponent<PrivateAiController>().GetPrivateEnemy();
            if (((privateEnemy != null) && (privateEnemy.GoBase != null)) && (privateEnemy.GoBase != oldEnemy))
            {
                return new CheckResult { ok = true, enemy = privateEnemy.GoBase };
            }
            return FalseResult;
        }

        public static void CheckSkillBeforeMove(GameObject attacker, GameObject target, int skillType)
        {
            if (CheckSkillIsOk(attacker, skillType))
            {
                if (CheckTargetDead(attacker, target))
                {
                    SkillEmptyTarget(attacker, skillType);
                }
                else if (CheckCanSkillRange(attacker, target, skillType))
                {
                    SkillHim(attacker, target, skillType);
                }
                else
                {
                    MoveToHimWithSkill(attacker, target, skillType);
                }
            }
        }

        public static bool CheckSkillIsOk(GameObject attacker, int skillType)
        {
            SkillController component = attacker.GetComponent<SkillController>();
            if (component.LearnedSkillList[skillType] == 0)
            {
                Log.AI(null, " Skill Not Learn");
                return false;
            }
            if (!component.IsSkillCdReady(skillType))
            {
                Log.AI(attacker, " SkillCD Not Ok " + skillType);
                return false;
            }
            return true;
        }

        public static bool CheckTargetDead(GameObject attacker, GameObject target)
        {
            if (target != null)
            {
                uint camp = ObjectManager.GetVo(attacker).Camp;
                BaseRoleVo vo = ObjectManager.GetVo(target);
                if (!vo.IsEmptyHp && (vo.Camp != camp))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool CheckTargetFarAwayFromSummoner(GameObject master, GameObject target, float maxDist)
        {
            if ((master == null) || (target == null))
            {
                return false;
            }
            float num = Util.XZSqrMagnitude(master.transform.position, target.transform.position);
            Log.AI(null, "summon monster checking target far away from master: dis maxDist:" + maxDist.Square());
            return (num > maxDist.Square());
        }

        public static CheckResult CheckTeamAttack(GameObject g)
        {
            GameObject anotherTeamMember = ObjectManager.GetAnotherTeamMember(g);
            if (anotherTeamMember != null)
            {
                GameObject attackTarget = ObjectManager.GetAttackTarget(anotherTeamMember);
                if (attackTarget != null)
                {
                    return new CheckResult { ok = true, enemy = attackTarget };
                }
            }
            return new CheckResult();
        }

        public static CheckResult CheckUseHeal(GameObject g)
        {
            PlayerPVPAiController component = g.GetComponent<PlayerPVPAiController>();
            if (component.IsLowHp() && component.IsHealSkillAvailable())
            {
                Log.AI(component.gameObject, " CheckUseHeal OK ");
                component.UseHealSkill();
                return new CheckResult { ok = true };
            }
            return FalseResult;
        }

        public static float GetMonsterAttackRange(GameObject g)
        {
            return (g.GetComponent<SkillController>().MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO.attack_range * 0.001f);
        }

        public static float GetMonsterSightRange(GameObject g)
        {
            return (g.GetComponent<BaseControler>().Me.GetMeVoByType<MonsterVo>().MonsterVO.sight_range * 0.001f);
        }

        public static Vector3 GetMoveMasterPos(GameObject slave, GameObject master)
        {
            byte job = ObjectManager.GetJob(slave);
            float num2 = 0f;
            float num3 = 0f;
            if (job != 2)
            {
                num2 = UnityEngine.Random.Range((float) 135f, (float) 225f);
                num3 = AiConfig.RANGED_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE * 0.001f;
            }
            else
            {
                num2 = UnityEngine.Random.Range((float) -45f, (float) 45f);
                num3 = AiConfig.MELEE_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE * 0.001f;
            }
            Vector3 eulerAngles = master.transform.rotation.eulerAngles;
            Vector3 vector2 = (Vector3) ((Quaternion.Euler(eulerAngles.x, eulerAngles.y + num2, eulerAngles.z) * Vector3.forward) * num3);
            Vector3 point = master.transform.position + vector2;
            Util.GetPointMovable(ref point);
            return point;
        }

        public static Vector3 GetMoveToSummonerPos(GameObject slave, GameObject master)
        {
            float num = MathUtils.Random(0x87, 0xe1);
            float num2 = UnityEngine.Random.Range((float) 0.35f, (float) 0.85f);
            float num3 = AiConfig.MELEE_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE * 0.001f;
            num3 *= num2;
            Vector3 eulerAngles = master.transform.rotation.eulerAngles;
            Vector3 vector2 = (Vector3) ((Quaternion.Euler(eulerAngles.x, eulerAngles.y + num, eulerAngles.z) * Vector3.forward) * num3);
            Vector3 point = master.transform.position + vector2;
            Util.GetPointMovable(ref point);
            return point;
        }

        public static float GetPlayerAttackRange(GameObject g)
        {
            return (g.GetComponent<SkillController>().GetAttack1AvailableAtkRange() * 0.001f);
        }

        public static float GetPlayerSightRange(GameObject g)
        {
            return (g.GetComponent<BaseControler>().Me.GetMeVoByType<PlayerVo>().generalTemplateInfo.ai_sight_range * 0.001f);
        }

        private static float GetSkillAttackRange(GameObject attacker, int skillType)
        {
            SkillController component = attacker.GetComponent<SkillController>();
            ActionControler controler = attacker.GetComponent<ActionControler>();
            uint id = component.LearnedSkillList[skillType];
            Log.AI(attacker, " learnSkill " + id);
            if (id == 0)
            {
                return 0f;
            }
            if (!component.IsSkillCdReady(skillType))
            {
                Log.AI(attacker, " SkillCD " + skillType);
                return 0f;
            }
            SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(id);
            SysSkillActionVo sysSkillActionVo = BaseDataMgr.instance.GetSysSkillActionVo(sysSkillBaseVo.skill_group);
            if ((sysSkillBaseVo.target_type == 1) || (sysSkillBaseVo.target_type == 2))
            {
                return 999f;
            }
            float num2 = sysSkillBaseVo.available_atk_range * 0.001f;
            float num3 = controler.ExtendedBulletFlyDistance * 0.001f;
            Log.AI(attacker, string.Concat(new object[] { " beforeCheckSkill  requireDistance ", num2, " extendDis ", num3 }));
            if (sysSkillActionVo.IsBullet)
            {
                return (num2 + num3);
            }
            return num2;
        }

        public static void MoveToBuff(GameObject attacker, GameObject target)
        {
            <MoveToBuff>c__AnonStorey127 storey = new <MoveToBuff>c__AnonStorey127 {
                attacker = attacker
            };
            if (((storey.attacker != null) && (target != null)) && (target.GetComponent<BaseControler>() != null))
            {
                Vector3 position = target.transform.position;
                CommandHandler.AddCommandStatic(storey.attacker, string.Format("move_attack {0} -1 buff", target.GetComponent<BaseControler>().GetControllerId()), 1f);
                int num = storey.attacker.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storey.<>m__1F0));
                object[] args = new object[] { position.x, position.y, position.z, num };
                CommandHandler.AddCommandStatic(storey.attacker, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
            }
        }

        public static void MoveToHim(GameObject attacker, GameObject target)
        {
            <MoveToHim>c__AnonStorey129 storey = new <MoveToHim>c__AnonStorey129 {
                attacker = attacker
            };
            if ((storey.attacker != null) && (target != null))
            {
                Vector3 position = target.transform.position;
                CommandHandler.AddCommandStatic(storey.attacker, string.Format("move_attack {0}", target.GetComponent<BaseControler>().GetControllerId()), 1f);
                int num = storey.attacker.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storey.<>m__1F2));
                object[] args = new object[] { position.x, position.y, position.z, num };
                CommandHandler.AddCommandStatic(storey.attacker, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
            }
        }

        private static void MoveToHimWithSkill(GameObject attacker, GameObject target, int skillType)
        {
            <MoveToHimWithSkill>c__AnonStorey128 storey = new <MoveToHimWithSkill>c__AnonStorey128 {
                attacker = attacker
            };
            int controllerId = target.GetComponent<BaseControler>().GetControllerId();
            Vector3 position = target.transform.position;
            object[] args = new object[] { controllerId, -1, "attackByHand", skillType };
            CommandHandler.AddCommandStatic(storey.attacker, string.Format("move_attack {0} {1} {2} {3}", args), 1f);
            int num2 = storey.attacker.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storey.<>m__1F1));
            object[] objArray2 = new object[] { position.x, position.y, position.z, num2 };
            CommandHandler.AddCommandStatic(storey.attacker, string.Format("TryToMove {0} {1} {2} {3}", objArray2), 1f);
        }

        public static void MoveToMaster(GameObject slave, GameObject master)
        {
            <MoveToMaster>c__AnonStorey12A storeya = new <MoveToMaster>c__AnonStorey12A {
                slave = slave
            };
            Vector3 moveMasterPos = GetMoveMasterPos(storeya.slave, master);
            AiControllerBase component = storeya.slave.GetComponent<AiControllerBase>();
            if (component != null)
            {
                component.CleanTargetDisplay();
            }
            CommandHandler.AddCommandStatic(storeya.slave, string.Format("move_attack {0} {1}", -1, master.GetComponent<BaseControler>().GetControllerId()), 1f);
            int num = storeya.slave.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storeya.<>m__1F3));
            object[] args = new object[] { moveMasterPos.x, moveMasterPos.y, moveMasterPos.z, num };
            CommandHandler.AddCommandStatic(storeya.slave, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
        }

        public static void MoveToNpc(GameObject attacker, GameObject target, Util.VoidDelegate callback)
        {
            attacker.GetComponent<StateMachine>().StartCoroutine(MoveToNpcCor(attacker, target, callback));
        }

        [DebuggerHidden]
        private static IEnumerator MoveToNpcCor(GameObject attacker, GameObject target, Util.VoidDelegate callback)
        {
            return new <MoveToNpcCor>c__Iterator6A { target = target, attacker = attacker, callback = callback, <$>target = target, <$>attacker = attacker, <$>callback = callback };
        }

        public static void MoveToPos(GameObject g, Vector3 md)
        {
            <MoveToPos>c__AnonStorey126 storey = new <MoveToPos>c__AnonStorey126 {
                g = g
            };
            CommandHandler.AddCommandStatic(storey.g, string.Format("move_attack {0}", -1), 1f);
            int num = storey.g.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storey.<>m__1EF));
            Log.AI(storey.g, string.Concat(new object[] { " MoveToPos ", num, " targetPos ", md }));
            object[] args = new object[] { md.x, md.y, md.z, num };
            CommandHandler.AddCommandStatic(storey.g, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
        }

        public static void MoveToSpring(GameObject g, Vector3 md)
        {
            <MoveToSpring>c__AnonStorey125 storey = new <MoveToSpring>c__AnonStorey125 {
                g = g
            };
            CommandHandler.AddCommandStatic(storey.g, "move_attack -1 -1 spring", 1f);
            int num = storey.g.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storey.<>m__1EE));
            Log.AI(storey.g, string.Concat(new object[] { " MoveToPos ", num, " targetPos ", md }));
            object[] args = new object[] { md.x, md.y, md.z, num };
            CommandHandler.AddCommandStatic(storey.g, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
        }

        public static void MoveToSummoner(GameObject slave, GameObject master)
        {
            <MoveToSummoner>c__AnonStorey12B storeyb = new <MoveToSummoner>c__AnonStorey12B {
                slave = slave
            };
            Vector3 moveToSummonerPos = GetMoveToSummonerPos(storeyb.slave, master);
            AiControllerBase component = storeyb.slave.GetComponent<AiControllerBase>();
            if (component != null)
            {
                component.CleanTargetDisplay();
            }
            CommandHandler.AddCommandStatic(storeyb.slave, string.Format("move_attack {0} {1}", -1, master.GetComponent<BaseControler>().GetControllerId()), 1f);
            int num = storeyb.slave.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storeyb.<>m__1F4));
            object[] args = new object[] { moveToSummonerPos.x, moveToSummonerPos.y, moveToSummonerPos.z, num };
            CommandHandler.AddCommandStatic(storeyb.slave, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
        }

        public static void NetMoveToPos(GameObject attacker, Vector3 md, float rotY, bool ignoreCollision, CommandHandler.Command lastCommand)
        {
            <NetMoveToPos>c__AnonStorey124 storey = new <NetMoveToPos>c__AnonStorey124 {
                attacker = attacker,
                lastCommand = lastCommand,
                rotY = rotY
            };
            CommandHandler.AddCommandStatic(storey.attacker, string.Format("move_attack -1", new object[0]), 1f);
            storey.lastCommand.isRunning = true;
            int num = storey.attacker.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(storey.<>m__1ED));
            object[] args = new object[] { md.x, md.y, md.z, num, ignoreCollision };
            CommandHandler.AddCommandStatic(storey.attacker, string.Format("TryToMove {0} {1} {2} {3} {4}", args), storey.lastCommand.rate);
        }

        public static void RotateDir(GameObject attacker, float rotateY)
        {
            attacker.transform.rotation = Quaternion.Euler(new Vector3(0f, rotateY, 0f));
        }

        private static void SkillEmptyTarget(GameObject attacker, int skillType)
        {
            CommandHandler.AddCommandStatic(attacker, string.Format("skill {0} {1}", skillType, -1), 1f);
        }

        public static void SkillHim(GameObject attacker, GameObject enemy, int skillId)
        {
            Log.AI(attacker, string.Concat(new object[] { " SkillHim ", enemy, " skillId ", skillId }));
            ActionDisplay meByType = enemy.GetComponent<BaseControler>().GetMeByType<ActionDisplay>();
            attacker.GetComponent<SkillController>().RequestUseSkill(skillId, true, false, null, null, meByType);
        }

        public static void WifiSkillHim(GameObject attacker, GameObject enemy, uint skillId, float rotateY, CommandHandler.Command cmd)
        {
            ActionDisplay display;
            <WifiSkillHim>c__AnonStorey123 storey = new <WifiSkillHim>c__AnonStorey123 {
                attacker = attacker,
                skillId = skillId,
                cmd = cmd
            };
            storey.cmd.isRunning = true;
            Util.VoidDelegate cb = new Util.VoidDelegate(storey.<>m__1EC);
            storey.attacker.GetComponent<StateMachine>().SetStateExitCallback(AIState.AIStateEnum.SKILL, cb);
            Log.Net(string.Concat(new object[] { storey.attacker, " UseSkillById ", enemy, " skillId ", storey.skillId, " rotateY ", rotateY }));
            RotateDir(storey.attacker, rotateY);
            CommandHandler.Command command = storey.cmd;
            storey.attacker.GetComponent<SkillController>().UseSkillById(storey.skillId, out display, false, false, true, false, null, null, null, -1, command);
        }

        [CompilerGenerated]
        private sealed class <MoveToBuff>c__AnonStorey127
        {
            internal GameObject attacker;

            internal void <>m__1F0()
            {
                CommandHandler.AddCommandStatic(this.attacker, "idle", 1f);
            }
        }

        [CompilerGenerated]
        private sealed class <MoveToHim>c__AnonStorey129
        {
            internal GameObject attacker;

            internal void <>m__1F2()
            {
                CommandHandler.AddCommandStatic(this.attacker, "idle", 1f);
            }
        }

        [CompilerGenerated]
        private sealed class <MoveToHimWithSkill>c__AnonStorey128
        {
            internal GameObject attacker;

            internal void <>m__1F1()
            {
                CommandHandler.AddCommandStatic(this.attacker, "idle", 1f);
            }
        }

        [CompilerGenerated]
        private sealed class <MoveToMaster>c__AnonStorey12A
        {
            internal GameObject slave;

            internal void <>m__1F3()
            {
                CommandHandler.AddCommandStatic(this.slave, "idle", 1f);
            }
        }

        [CompilerGenerated]
        private sealed class <MoveToNpcCor>c__Iterator6A : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal GameObject <$>attacker;
            internal Util.VoidDelegate <$>callback;
            internal GameObject <$>target;
            internal int <cbId>__1;
            internal Vector3 <md>__0;
            internal GameObject attacker;
            internal Util.VoidDelegate callback;
            internal GameObject target;

            internal void <>m__1F5()
            {
                CommandHandler.AddCommandStatic(this.attacker, "idle", 1f);
                this.callback();
            }

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    {
                        this.<md>__0 = this.target.transform.position;
                        CommandHandler.AddCommandStatic(this.attacker, string.Format("move_attack {0}", -1), 1f);
                        this.<cbId>__1 = this.attacker.GetComponent<MonsterMoveAI>().AddCallbackMap(new Util.VoidDelegate(this.<>m__1F5));
                        object[] args = new object[] { this.<md>__0.x, this.<md>__0.y, this.<md>__0.z, this.<cbId>__1 };
                        CommandHandler.AddCommandStatic(this.attacker, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
                        break;
                    }
                    case 1:
                        break;

                    default:
                        goto Label_0153;
                }
                if (MathUtils.VectorDistance(this.attacker.transform.position, this.target.transform.position) <= 2f)
                {
                    CommandHandler.AddCommandStatic(this.attacker, "idle", 1f);
                    this.callback();
                }
                else
                {
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_0153:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <MoveToPos>c__AnonStorey126
        {
            internal GameObject g;

            internal void <>m__1EF()
            {
                CommandHandler.AddCommandStatic(this.g, "idle", 1f);
            }
        }

        [CompilerGenerated]
        private sealed class <MoveToSpring>c__AnonStorey125
        {
            internal GameObject g;

            internal void <>m__1EE()
            {
                CommandHandler.AddCommandStatic(this.g, "idle", 1f);
            }
        }

        [CompilerGenerated]
        private sealed class <MoveToSummoner>c__AnonStorey12B
        {
            internal GameObject slave;

            internal void <>m__1F4()
            {
                CommandHandler.AddCommandStatic(this.slave, "idle", 1f);
            }
        }

        [CompilerGenerated]
        private sealed class <NetMoveToPos>c__AnonStorey124
        {
            internal GameObject attacker;
            internal CommandHandler.Command lastCommand;
            internal float rotY;

            internal void <>m__1ED()
            {
                Log.Net(this.attacker + " Move Finish");
                CommandHandler.AddCommandStatic(this.attacker, "idle", 1f);
                this.lastCommand.isRunning = false;
                CombatUtil.RotateDir(this.attacker, this.rotY);
            }
        }

        [CompilerGenerated]
        private sealed class <WifiSkillHim>c__AnonStorey123
        {
            internal GameObject attacker;
            internal CommandHandler.Command cmd;
            internal uint skillId;

            internal void <>m__1EC()
            {
                Log.Net(this.attacker + " UseSkill Over " + this.skillId);
                this.cmd.isRunning = false;
            }
        }

        public class CheckResult
        {
            public GameObject enemy;
            public bool ok;
        }
    }
}

