﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class CommandRunner
    {
        protected NetworkCommandHandler handler;
        protected CommandHandler.Command myCmd;
        protected GameObject owner;

        public CommandRunner(CommandHandler.Command cmd, GameObject g)
        {
            this.myCmd = cmd;
            this.owner = g;
            this.handler = this.owner.GetComponent<NetworkCommandHandler>();
        }

        public virtual bool CheckNextCommand(CommandHandler.Command cmd)
        {
            return true;
        }

        public bool IsRunning()
        {
            return this.myCmd.isRunning;
        }

        public virtual void RunCommand()
        {
        }

        [DebuggerHidden]
        public virtual IEnumerator WaitCommandFinish()
        {
            return new <WaitCommandFinish>c__Iterator6D { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <WaitCommandFinish>c__Iterator6D : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal CommandRunner <>f__this;
            internal float <waitTime>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Log.Net(this.<>f__this.owner + " WaitCommandFinish " + this.<>f__this.myCmd.stringCmd);
                        this.<waitTime>__0 = 0f;
                        break;

                    case 1:
                        if (this.<waitTime>__0 <= 1f)
                        {
                            break;
                        }
                        Log.AI(this.<>f__this.owner, " wait this State TooLong " + this.<>f__this.myCmd.cmd[1]);
                        goto Label_00D6;

                    default:
                        goto Label_00DD;
                }
                if (this.<>f__this.myCmd.isRunning)
                {
                    this.<waitTime>__0 += Time.deltaTime;
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
            Label_00D6:
                this.$PC = -1;
            Label_00DD:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

