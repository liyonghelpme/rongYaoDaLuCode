﻿namespace com.liyong
{
    using System;

    public class DeathState : AIState
    {
        public DeathState()
        {
            base.type = AIState.AIStateEnum.DEATH;
        }
    }
}

