﻿namespace com.liyong
{
    using com.game;
    using com.game.module.fight.arpg;
    using com.game.module.WiFiPvP;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class DefaultDeath : DeathState
    {
        private bool deathYet;

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            if (((s == AIState.AIStateEnum.IDLE) && (lastCommand.cmd.Length >= 2)) && (lastCommand.cmd[1] == "relive"))
            {
                this.deathYet = false;
                Log.AI(base.stateMachine.gameObject, " Relive From Death");
                return true;
            }
            Log.AI(base.stateMachine.gameObject, " Death Can't Change To OtherState\n" + lastCommand.stringCmd + " \n" + lastCommand.source);
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(11);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            if (AppMap.Instance.IsInWifiPVP)
            {
                GameObject gameObject = base.stateMachine.gameObject;
                if (!this.deathYet && (ObjectManager.GetVo(base.stateMachine.gameObject).Camp == 1))
                {
                    Log.AI(gameObject, "Killed Add Soul");
                    Soul.Instance.AddSoul(ObjectManager.GetDisplay(base.stateMachine.gameObject));
                }
                if (this.deathYet)
                {
                    Log.AI(gameObject, " death again");
                }
                this.deathYet = true;
                WifiPvpManager.Instance.EnemyIncreaseDeadCount(ObjectManager.GetDisplay(gameObject));
            }
        }

        public override string GetSubName()
        {
            return "_default";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator7B { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator7B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal DefaultDeath <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

