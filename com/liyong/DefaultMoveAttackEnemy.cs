﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class DefaultMoveAttackEnemy : MoveAttackEnemy
    {
        private GameObject oldEnemy;

        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if (cmd.cmd[0] == "TryToMove")
            {
                Log.AI(base.stateMachine.name, " TryToMove AI player ");
                float x = Convert.ToSingle(cmd.cmd[1]);
                float y = Convert.ToSingle(cmd.cmd[2]);
                float z = Convert.ToSingle(cmd.cmd[3]);
                int cid = Convert.ToInt32(cmd.cmd[4]);
                bool ignoreCollision = false;
                if (cmd.cmd.Count<string>() >= 6)
                {
                    ignoreCollision = Convert.ToBoolean(cmd.cmd[5]);
                }
                MonsterMoveAI component = base.stateMachine.GetComponent<MonsterMoveAI>();
                component.StartCoroutine(component.TryToMove(new Vector3(x, y, z), component.GetCallback(cid), null, null, ignoreCollision, cmd));
            }
            return false;
        }

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            if (s == AIState.AIStateEnum.HURT)
            {
                return false;
            }
            return base.CheckNextState(s);
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(1);
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            this.oldEnemy = ObjectManager.GetController(Convert.ToInt32(lastCommand.cmd[1]));
            if (this.oldEnemy != null)
            {
                Log.AI(base.stateMachine.gameObject.name, " monster oldEnemy " + this.oldEnemy.name);
            }
        }

        public override string GetSubName()
        {
            return "_default";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator7E { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator7E : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal DefaultMoveAttackEnemy <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

