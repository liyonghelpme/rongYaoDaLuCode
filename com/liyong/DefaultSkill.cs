﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class DefaultSkill : SkillState
    {
        private Animator animator;
        private float rate = 1f;

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            if (s == AIState.AIStateEnum.HURT)
            {
                return false;
            }
            return base.CheckNextState(s);
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            int nextStatu = Convert.ToInt32(base.stateMachine.GetLastCommand().cmd[1]);
            Log.AI(base.stateMachine.gameObject, string.Concat(new object[] { " Skill Default ", nextStatu, " stateName ", 3 }));
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(nextStatu);
            Log.AI(base.stateMachine.gameObject, " EnterSkillState");
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            this.rate = lastCommand.rate;
            this.animator = ObjectManager.GetPlayerAnimator(base.stateMachine.gameObject);
            this.animator.speed = this.rate;
        }

        public override void ExitState()
        {
            this.animator.speed = 1f;
            base.ExitState();
        }

        public override string GetSubName()
        {
            return "_default";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator7F { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator7F : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal DefaultSkill <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

