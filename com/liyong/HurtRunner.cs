﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class HurtRunner : CommandRunner
    {
        public HurtRunner(CommandHandler.Command cmd, GameObject g) : base(cmd, g)
        {
        }

        public override bool CheckNextCommand(CommandHandler.Command cmd)
        {
            return false;
        }

        public override void RunCommand()
        {
            base.myCmd.isRunning = true;
            CommandHandler.AddCommandStatic(base.owner, "hurt", 1f);
        }

        [DebuggerHidden]
        public override IEnumerator WaitCommandFinish()
        {
            return new <WaitCommandFinish>c__Iterator6E { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <WaitCommandFinish>c__Iterator6E : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal HurtRunner <>f__this;
            internal StateMachine <stateMachine>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Log.Net(this.<>f__this.owner + " Wait Skill Finish");
                        this.<stateMachine>__0 = this.<>f__this.owner.GetComponent<StateMachine>();
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_0097;
                }
                if (this.<stateMachine>__0.GetCurState().type == AIState.AIStateEnum.HURT)
                {
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.<>f__this.myCmd.isRunning = false;
                this.$PC = -1;
            Label_0097:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

