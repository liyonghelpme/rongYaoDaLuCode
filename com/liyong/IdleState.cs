﻿namespace com.liyong
{
    using System;

    public class IdleState : AIState
    {
        public IdleState()
        {
            base.type = AIState.AIStateEnum.IDLE;
        }
    }
}

