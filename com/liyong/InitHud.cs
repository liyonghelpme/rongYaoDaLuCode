﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class InitHud : MonoBehaviour
    {
        private void Start()
        {
            MyHudText.hudview = GameObject.Find("UI Root");
            MyHudText.uiCamera = Util.FindChildRecursive(MyHudText.hudview.transform, "Camera").camera;
            MyHudText.gameCamera = GameObject.Find("Main Camera").camera;
            base.StartCoroutine(this.TestNum());
        }

        [DebuggerHidden]
        private IEnumerator TestNum()
        {
            return new <TestNum>c__Iterator6B();
        }

        private void Update()
        {
        }

        [CompilerGenerated]
        private sealed class <TestNum>c__Iterator6B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal GameObject <cube>__0;
            internal MyHudText <myhud>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<cube>__0 = GameObject.Find("HealtHudText");
                        this.<myhud>__1 = this.<cube>__0.GetComponent<MyHudText>();
                        break;

                    case 1:
                        this.<myhud>__1.AddText("+220", Vector3.zero, null);
                        this.<myhud>__1.AddText("+420", Vector3.zero, null);
                        this.<myhud>__1.AddText("+1220", Vector3.zero, null);
                        this.$current = null;
                        this.$PC = 2;
                        goto Label_00D2;

                    case 2:
                        break;
                        this.$PC = -1;
                        goto Label_00D0;

                    default:
                        goto Label_00D0;
                }
                this.<myhud>__1.AddText("+120", Vector3.zero, null);
                this.$current = null;
                this.$PC = 1;
                goto Label_00D2;
            Label_00D0:
                return false;
            Label_00D2:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

