﻿namespace com.liyong
{
    using System;
    using UnityEngine;

    public class MapGridManager
    {
        private GridManager gridManager = new GridManager(100f, 100f);
        public static MapGridManager Instance = new MapGridManager();

        public GridManager GetGridManager()
        {
            return this.gridManager;
        }

        public Vector3 GetMinForcePos(Vector3 attackerPos, Vector3 targetPos)
        {
            Vector3 vector = attackerPos - targetPos;
            vector.y = 0f;
            vector.Normalize();
            float x = vector.x;
            float z = vector.z;
            Vector3 pos = new Vector3(targetPos.x * 10f, 0f, targetPos.z * 10f);
            Util.Int2 gid = this.gridManager.PosToGridId(pos);
            int[] neiborsCrowdSituation = this.gridManager.GetNeiborsCrowdSituation(gid);
            for (int i = 0; i < 9; i++)
            {
            }
            return Vector3.zero;
        }
    }
}

