﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public sealed class MasterAiEnableIdle : IdleState
    {
        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if ((cmd.cmd[0] == "insert_command") && (cmd.cmd[1] == "move_keyboard"))
            {
                base.stateMachine.ChangeState(AIState.AIStateEnum.MOVE_KEYBOARD);
                return true;
            }
            return false;
        }

        public override bool CheckSubState()
        {
            Log.AI(base.stateMachine.gameObject, " Frozen 困住 " + base.stateMachine.GetComponent<BaseControler>().GetMeByType<PlayerDisplay>().GetVo().stateInfo.IsFrozen);
            if (CombatUtil.CanMove(base.stateMachine.gameObject))
            {
                PlayerAiController component = base.stateMachine.GetComponent<PlayerAiController>();
                Log.AI(base.stateMachine.gameObject, " Master AI Enable what " + component);
                if ((component != null) && component.IsAiEnable)
                {
                    Log.AI(base.stateMachine.gameObject, " isMaster " + component.IsMaster());
                    return component.IsMaster();
                }
            }
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase aiController = base.stateMachine.GetComponent<SkillController>().MeController.AiController;
            if (aiController != null)
            {
                aiController.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }
        }

        public override string GetSubName()
        {
            return "_master_Auto";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator81 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator81 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MasterAiEnableIdle <>f__this;
            internal PlayerAiController <ai>__1;
            internal GameObject <g>__0;
            internal CombatUtil.CheckResult <ret1>__2;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        this.<ai>__1 = this.<g>__0.GetComponent<PlayerAiController>();
                        break;

                    case 1:
                    case 2:
                        break;

                    default:
                        goto Label_0245;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    if (this.<ai>__1.IsInRecovery())
                    {
                        this.$current = null;
                        this.$PC = 1;
                    }
                    else
                    {
                        this.<ret1>__2 = CombatUtil.CheckAttackTarget(this.<g>__0);
                        if (this.<ret1>__2.ok)
                        {
                            Log.AI(this.<>f__this.stateMachine.gameObject, " MoveToSelfEnemy");
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__2.enemy);
                        }
                        if (!this.<ret1>__2.ok)
                        {
                            this.<ret1>__2 = CombatUtil.CheckAttackSelf(this.<g>__0);
                            if (this.<ret1>__2.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " Move Who Attack Me");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__2.enemy);
                            }
                        }
                        if (!this.<ret1>__2.ok)
                        {
                            this.<ret1>__2 = CombatUtil.CheckAttackTeam(this.<>f__this.stateMachine.gameObject);
                            if (this.<ret1>__2.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " MoveToTeam");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__2.enemy);
                            }
                        }
                        if (!this.<ret1>__2.ok)
                        {
                            this.<ret1>__2 = CombatUtil.CheckTeamAttack(this.<>f__this.stateMachine.gameObject);
                            if (this.<ret1>__2.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " Attack Who Attack Team Member ");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__2.enemy);
                            }
                        }
                        this.$current = null;
                        this.$PC = 2;
                    }
                    return true;
                }
                this.$PC = -1;
            Label_0245:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

