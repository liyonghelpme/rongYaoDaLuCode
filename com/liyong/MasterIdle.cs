﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class MasterIdle : IdleState
    {
        public override bool CheckSubState()
        {
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<MeStatuController>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase aiController = base.stateMachine.GetComponent<SkillController>().MeController.AiController;
            if (aiController != null)
            {
                aiController.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }
        }

        public override string GetSubName()
        {
            return "_master_InCity";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator82 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator82 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MasterIdle <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

