﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class MasterMoveAttackByHand : MoveAttackEnemy
    {
        private GameObject oldEnemy;
        private int skillId = -1;

        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if (cmd.cmd[0] == "TryToMove")
            {
                Log.AI(base.stateMachine.name, " TryToMove AI player ");
                float x = Convert.ToSingle(cmd.cmd[1]);
                float y = Convert.ToSingle(cmd.cmd[2]);
                float z = Convert.ToSingle(cmd.cmd[3]);
                int cid = Convert.ToInt32(cmd.cmd[4]);
                MonsterMoveAI component = base.stateMachine.GetComponent<MonsterMoveAI>();
                component.StartCoroutine(component.TryToMove(new Vector3(x, y, z), component.GetCallback(cid), null, null, false, null));
            }
            return false;
        }

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            if (s == AIState.AIStateEnum.HURT)
            {
                return false;
            }
            return base.CheckNextState(s);
        }

        public override bool CheckSubState()
        {
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            return ((lastCommand.cmd.Count<string>() >= 5) && (lastCommand.cmd[3] == "attackByHand"));
        }

        public override void EnterState()
        {
            Log.AI(null, " AttackByHand ");
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(1);
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            this.oldEnemy = ObjectManager.GetController(Convert.ToInt32(lastCommand.cmd[1]));
            Log.AI(base.stateMachine.gameObject, " MoveAttack No Enemy " + this.oldEnemy);
            this.skillId = Convert.ToInt32(lastCommand.cmd[4]);
        }

        public override string GetSubName()
        {
            return "_Hand";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator83 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator83 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MasterMoveAttackByHand <>f__this;
            internal GameObject <g>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00C6;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    if (CombatUtil.CheckCanAttack(this.<g>__0, this.<>f__this.oldEnemy))
                    {
                        CombatUtil.SkillHim(this.<>f__this.stateMachine.gameObject, this.<>f__this.oldEnemy, this.<>f__this.skillId);
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_00C6:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

