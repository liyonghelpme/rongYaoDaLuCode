﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public class MasterSkill : SkillState
    {
        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            return (((s != AIState.AIStateEnum.HURT) && (s != AIState.AIStateEnum.MOVE_KEYBOARD)) && base.CheckNextState(s));
        }

        public override bool CheckSubState()
        {
            PlayerAiController component = base.stateMachine.GetComponent<PlayerAiController>();
            Log.AI(base.stateMachine.gameObject, " ai is what " + component);
            if (component != null)
            {
                Log.AI(base.stateMachine.gameObject, " isMaster " + component.IsMaster());
                return (component.IsMaster() && !component.IsAiEnable);
            }
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            int nextStatu = Convert.ToInt32(base.stateMachine.GetLastCommand().cmd[1]);
            base.stateMachine.GetComponent<MeStatuController>().SetStatu(nextStatu);
        }

        public override string GetSubName()
        {
            return "_Master";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator84 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator84 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MasterSkill <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

