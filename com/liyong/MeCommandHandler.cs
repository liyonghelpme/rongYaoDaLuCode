﻿namespace com.liyong
{
    using System;
    using UnityEngine;

    [RequireComponent(typeof(CommandHandler)), RequireComponent(typeof(KeyboardHandler))]
    public class MeCommandHandler : MonoBehaviour
    {
        private CommandHandler cmd;
        public float inputX;
        public float inputY;

        private void OnMove(CommandHandler.Command command)
        {
            this.inputX = Convert.ToSingle(command.cmd[1]);
            this.inputY = Convert.ToSingle(command.cmd[2]);
        }

        private void Start()
        {
            this.cmd = base.GetComponent<CommandHandler>();
            this.cmd.AddHandler("move", new CommandHandler.CmdHandler(this.OnMove));
        }
    }
}

