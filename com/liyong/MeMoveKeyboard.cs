﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class MeMoveKeyboard : AIState
    {
        public MeMoveKeyboard()
        {
            base.type = AIState.AIStateEnum.MOVE_KEYBOARD;
        }

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            if (s == AIState.AIStateEnum.HURT)
            {
                return false;
            }
            return base.CheckNextState(s);
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<MeStatuController>().SetStatu(1);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase component = base.stateMachine.gameObject.GetComponent<AiControllerBase>();
            if (component != null)
            {
                component.CleanTargetDisplay();
            }
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator89 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator89 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MeMoveKeyboard <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

