﻿namespace com.liyong
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public class MeStateMachine : StateMachine
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map40;
        private CommandHandler cmdHandler;

        private void Awake()
        {
            base.AddState(new MasterIdle());
            base.AddState(new MasterAiEnableIdle());
            base.AddState(new SlaveIdle());
            base.AddState(new StopAiIdle());
            base.AddState(new MasterSkill());
            base.AddState(new SlaveSkill());
            base.AddState(new SlaveNormalSkill());
            base.AddState(new MeDeath());
            base.AddState(new MeDeathFly());
            base.AddState(new MeMoveAttackEnemy());
            base.AddState(new MeMoveKeyboard());
            base.AddState(new MoveToMaster());
            base.AddState(new AdjustMove());
            base.AddState(new MasterMoveAttackByHand());
            base.AddState(new MeHurt());
        }

        public override bool CheckCommand()
        {
            if (base.cmds.Count <= 0)
            {
                return false;
            }
            base.lastCommand = base.cmds[0];
            base.cmds.RemoveAt(0);
            Log.AI(base.gameObject.name, " lastCommand " + base.lastCommand.stringCmd);
            string key = base.lastCommand.cmd[0];
            if (key != null)
            {
                int num;
                if (<>f__switch$map40 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(7);
                    dictionary.Add("move_attack", 0);
                    dictionary.Add("move_keyboard", 1);
                    dictionary.Add("idle", 2);
                    dictionary.Add("skill", 3);
                    dictionary.Add("death", 4);
                    dictionary.Add("hurt", 5);
                    dictionary.Add("death_fly", 6);
                    <>f__switch$map40 = dictionary;
                }
                if (<>f__switch$map40.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_ATTACK);

                        case 1:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_KEYBOARD);

                        case 2:
                            return base.ChangeState(AIState.AIStateEnum.IDLE);

                        case 3:
                            return base.ChangeState(AIState.AIStateEnum.SKILL);

                        case 4:
                            return base.ChangeState(AIState.AIStateEnum.DEATH);

                        case 5:
                            return base.ChangeState(AIState.AIStateEnum.HURT);

                        case 6:
                            return base.ChangeState(AIState.AIStateEnum.DEATH_FLY);
                    }
                }
            }
            return base.GetCurState().CheckCommand(base.lastCommand);
        }

        private void Start()
        {
            CommandHandler component = base.GetComponent<CommandHandler>();
            this.cmdHandler = component;
            component.AddHandler("move_attack", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("move_keyboard", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("idle", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("skill", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("death", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("TryToMove", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("hurt", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("death_fly", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("insert_command", new CommandHandler.CmdHandler(this.OnCmd));
            base.ChangeState(AIState.AIStateEnum.IDLE);
        }

        private void Update()
        {
            this.cmdHandler.Execute();
        }
    }
}

