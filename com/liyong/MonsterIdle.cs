﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class MonsterIdle : IdleState
    {
        private CombatUtil.CheckResult ChaseEnemy()
        {
            return new CombatUtil.CheckResult();
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<MonsterStatuController>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase aiController = base.stateMachine.GetComponent<SkillController>().MeController.AiController;
            if (aiController != null)
            {
                aiController.SetAiStatusButNotClearTarget(AiEnum.MonsterAiStatus.THINK);
            }
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator92 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator92 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MonsterIdle <>f__this;
            internal CombatUtil.CheckResult <ret1>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.<ret1>__0 = new CombatUtil.CheckResult();
                            if (!this.<ret1>__0.ok)
                            {
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_0085;
                }
                this.$PC = -1;
            Label_0085:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

