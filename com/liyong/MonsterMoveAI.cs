﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    [RequireComponent(typeof(PhysicComponent)), RequireComponent(typeof(Seeker)), RequireComponent(typeof(PathComponent)), RequireComponent(typeof(MoveComponent)), RequireComponent(typeof(CharacterController))]
    public class MonsterMoveAI : MonoBehaviour
    {
        private Dictionary<int, Util.VoidDelegate> callbackMap = new Dictionary<int, Util.VoidDelegate>();
        private int directId;
        private bool inMove;
        private MoveComponent move;
        private int moveId;
        private float mvSpeed = 3f;
        private PathComponent path;

        public int AddCallbackMap(Util.VoidDelegate cb)
        {
            Log.AI(base.gameObject, " Add Another Callback " + cb);
            this.callbackMap.Clear();
            this.callbackMap[this.moveId + 1] = cb;
            return (this.moveId + 1);
        }

        private void Awake()
        {
            this.path = base.GetComponent<PathComponent>();
            this.move = base.GetComponent<MoveComponent>();
        }

        public Util.VoidDelegate GetCallback(int cid)
        {
            Log.AI(base.gameObject, string.Concat(new object[] { " GetCallback is ", cid, " totalCount ", this.callbackMap.Count }));
            if (this.callbackMap.ContainsKey(cid))
            {
                return this.callbackMap[cid];
            }
            return null;
        }

        [DebuggerHidden]
        private IEnumerator MoveDirectly(Vector3 destination, int curId)
        {
            return new <MoveDirectly>c__Iterator45 { curId = curId, destination = destination, <$>curId = curId, <$>destination = destination, <>f__this = this };
        }

        [DebuggerHidden]
        private IEnumerator MoveTo(Vector3 destination, Util.VoidDelegate startRun, MoveResult result, BoidObject boidObject, bool ignoreCollision, CommandHandler.Command cmd)
        {
            return new <MoveTo>c__Iterator46 { destination = destination, result = result, startRun = startRun, boidObject = boidObject, ignoreCollision = ignoreCollision, cmd = cmd, <$>destination = destination, <$>result = result, <$>startRun = startRun, <$>boidObject = boidObject, <$>ignoreCollision = ignoreCollision, <$>cmd = cmd, <>f__this = this };
        }

        public void SetSpeed(float speed)
        {
            this.mvSpeed = speed;
        }

        public void StopByStateMachine()
        {
            base.StartCoroutine(this.StopMove(null));
        }

        [DebuggerHidden]
        private IEnumerator StopMove(Util.VoidDelegate stopRun)
        {
            return new <StopMove>c__Iterator47 { stopRun = stopRun, <$>stopRun = stopRun, <>f__this = this };
        }

        [DebuggerHidden]
        public IEnumerator TryToMove(Vector3 destination, Util.VoidDelegate callback, Util.VoidDelegate startRun, BoidObject boidObject = null, bool ignoreCollision = false, CommandHandler.Command cmd = null)
        {
            return new <TryToMove>c__Iterator48 { destination = destination, callback = callback, startRun = startRun, boidObject = boidObject, ignoreCollision = ignoreCollision, cmd = cmd, <$>destination = destination, <$>callback = callback, <$>startRun = startRun, <$>boidObject = boidObject, <$>ignoreCollision = ignoreCollision, <$>cmd = cmd, <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <MoveDirectly>c__Iterator45 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal int <$>curId;
            internal Vector3 <$>destination;
            internal MonsterMoveAI <>f__this;
            internal Vector3 <dir>__0;
            internal float <dist>__1;
            internal Vector3 <moveSpeed>__3;
            internal Vector3 <normalDir>__2;
            internal float <passTime>__5;
            internal PhysicComponent <physic>__4;
            internal int curId;
            internal Vector3 destination;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " Go Go Go ", this.curId, " speed ", this.<>f__this.mvSpeed }));
                        this.<dir>__0 = this.destination - this.<>f__this.transform.position;
                        this.<dir>__0.y = 0f;
                        this.<dist>__1 = this.<dir>__0.sqrMagnitude;
                        this.<normalDir>__2 = this.<dir>__0.normalized;
                        this.<moveSpeed>__3 = (Vector3) (this.<>f__this.mvSpeed * this.<normalDir>__2);
                        this.<physic>__4 = this.<>f__this.GetComponent<PhysicComponent>();
                        this.<physic>__4.inMove = true;
                        this.<passTime>__5 = 0f;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_01CE;
                }
                if ((this.<passTime>__5 < 1f) && (this.curId == this.<>f__this.directId))
                {
                    this.<physic>__4.MoveSpeed(this.<moveSpeed>__3);
                    this.<passTime>__5 += Time.deltaTime;
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " Stop Go Go Go ", this.<moveSpeed>__3, " curId ", this.curId, " directId ", this.<>f__this.directId }));
                this.$PC = -1;
            Label_01CE:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <MoveTo>c__Iterator46 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BoidObject <$>boidObject;
            internal CommandHandler.Command <$>cmd;
            internal Vector3 <$>destination;
            internal bool <$>ignoreCollision;
            internal MonsterMoveAI.MoveResult <$>result;
            internal Util.VoidDelegate <$>startRun;
            internal MonsterMoveAI <>f__this;
            internal int <curId>__0;
            internal float <dist>__1;
            internal PhysicComponent <physic>__3;
            internal StateMachine <stateMachine>__2;
            internal BoidObject boidObject;
            internal CommandHandler.Command cmd;
            internal Vector3 destination;
            internal bool ignoreCollision;
            internal MonsterMoveAI.MoveResult result;
            internal Util.VoidDelegate startRun;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    {
                        this.<curId>__0 = ++this.<>f__this.moveId;
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " MoveCommand ", this.<>f__this.transform.position, " destination ", this.destination }));
                        this.result.ok = false;
                        this.<>f__this.inMove = true;
                        Vector3 vector = this.<>f__this.transform.position - this.destination;
                        this.<dist>__1 = vector.sqrMagnitude;
                        this.<>f__this.directId++;
                        Log.AI(this.<>f__this.gameObject, " MoveDirectly Network ");
                        this.<stateMachine>__2 = this.<>f__this.GetComponent<StateMachine>();
                        this.<stateMachine>__2.otherState = "_Search";
                        this.$current = this.<>f__this.StartCoroutine(this.<>f__this.path.SearchPath(this.destination, this.<curId>__0));
                        this.$PC = 1;
                        goto Label_02B8;
                    }
                    case 1:
                        this.<>f__this.directId++;
                        if (this.<>f__this.path.GetPath() == null)
                        {
                            this.<physic>__3 = this.<>f__this.GetComponent<PhysicComponent>();
                            if (this.<curId>__0 == this.<>f__this.moveId)
                            {
                                this.<physic>__3.inMove = false;
                            }
                            break;
                        }
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " 停止 旧的移动 接着 生成新的移动 ", this.destination, " ", Time.frameCount }));
                        if (this.startRun != null)
                        {
                            this.startRun();
                        }
                        this.<stateMachine>__2.otherState = "_Move";
                        this.$current = this.<>f__this.StartCoroutine(this.<>f__this.move.MoveByPath(this.<>f__this.path.GetPath(), this.destination, this.<>f__this.mvSpeed, this.result, this.<curId>__0, this.boidObject, this.ignoreCollision, this.cmd));
                        this.$PC = 2;
                        goto Label_02B8;

                    case 2:
                        break;

                    default:
                        goto Label_02B6;
                }
                if (this.<curId>__0 == this.<>f__this.moveId)
                {
                    this.<>f__this.inMove = false;
                }
                this.$PC = -1;
            Label_02B6:
                return false;
            Label_02B8:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StopMove>c__Iterator47 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal Util.VoidDelegate <$>stopRun;
            internal MonsterMoveAI <>f__this;
            internal Util.VoidDelegate stopRun;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " Stop Move Here ", Time.frameCount, " inMove ", this.<>f__this.inMove }));
                        if (this.<>f__this.inMove)
                        {
                            this.<>f__this.moveId++;
                            this.<>f__this.path.StopSearch(this.<>f__this.moveId);
                            this.<>f__this.move.StopMove(this.<>f__this.moveId);
                            this.<>f__this.inMove = false;
                        }
                        if (this.stopRun != null)
                        {
                            this.stopRun();
                        }
                        this.$current = null;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TryToMove>c__Iterator48 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BoidObject <$>boidObject;
            internal Util.VoidDelegate <$>callback;
            internal CommandHandler.Command <$>cmd;
            internal Vector3 <$>destination;
            internal bool <$>ignoreCollision;
            internal Util.VoidDelegate <$>startRun;
            internal MonsterMoveAI <>f__this;
            internal MonsterMoveAI.MoveResult <result>__0;
            internal BoidObject boidObject;
            internal Util.VoidDelegate callback;
            internal CommandHandler.Command cmd;
            internal Vector3 destination;
            internal bool ignoreCollision;
            internal Util.VoidDelegate startRun;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " TryToMove ", Time.frameCount, " 目标点 ", this.destination, " callback ", this.callback }));
                        if (this.callback == null)
                        {
                            Log.AI(this.<>f__this.gameObject, " Callback Lost why " + this.destination);
                        }
                        this.<result>__0 = new MonsterMoveAI.MoveResult();
                        this.$current = this.<>f__this.StartCoroutine(this.<>f__this.MoveTo(this.destination, this.startRun, this.<result>__0, this.boidObject, this.ignoreCollision, this.cmd));
                        this.$PC = 1;
                        goto Label_018C;

                    case 1:
                        this.$current = null;
                        this.$PC = 2;
                        goto Label_018C;

                    case 2:
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " MoveFinish Stop Or Normal ", this.<result>__0.ok, " callback ", this.callback }));
                        if (this.<result>__0.ok && (this.callback != null))
                        {
                            this.callback();
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            Label_018C:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        public class MoveResult
        {
            public bool ok;
        }
    }
}

