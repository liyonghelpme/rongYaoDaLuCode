﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public sealed class MonsterMoveAttackEnemy : AIState
    {
        private GameObject oldEnemy;

        public MonsterMoveAttackEnemy()
        {
            base.type = AIState.AIStateEnum.MOVE_ATTACK;
        }

        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if (cmd.cmd[0] == "TryToMove")
            {
                Log.AI(base.stateMachine.name, " TryToMove AI player ");
                float x = Convert.ToSingle(cmd.cmd[1]);
                float y = Convert.ToSingle(cmd.cmd[2]);
                float z = Convert.ToSingle(cmd.cmd[3]);
                int cid = Convert.ToInt32(cmd.cmd[4]);
                MonsterMoveAI component = base.stateMachine.GetComponent<MonsterMoveAI>();
                BoidObject boidObject = null;
                if (this.oldEnemy != null)
                {
                    boidObject = this.oldEnemy.GetComponent<BoidObject>();
                }
                component.StartCoroutine(component.TryToMove(new Vector3(x, y, z), component.GetCallback(cid), null, boidObject, false, null));
            }
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(1);
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            this.oldEnemy = ObjectManager.GetController(Convert.ToInt32(lastCommand.cmd[1]));
            if (this.oldEnemy != null)
            {
                Log.AI(base.stateMachine.gameObject.name, " monster oldEnemy " + this.oldEnemy.name);
            }
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator93 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator93 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MonsterMoveAttackEnemy <>f__this;
            internal float <atkRange>__0;
            internal float <dist>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<atkRange>__0 = CombatUtil.GetMonsterAttackRange(this.<>f__this.stateMachine.gameObject);
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_0132;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    if (this.<>f__this.oldEnemy != null)
                    {
                        this.<dist>__1 = Util.XZSqrMagnitude(this.<>f__this.oldEnemy.transform.position, this.<>f__this.stateMachine.transform.position);
                        Log.AI(this.<>f__this.stateMachine.gameObject, " attackRange " + this.<dist>__1);
                        if (this.<dist>__1 < (this.<atkRange>__0 * this.<atkRange>__0))
                        {
                            this.<>f__this.stateMachine.GetComponent<MonsterControler>().EnemyInAtkRange(this.<>f__this.oldEnemy);
                        }
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_0132:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

