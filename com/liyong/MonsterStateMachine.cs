﻿namespace com.liyong
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    [RequireComponent(typeof(CommandHandler))]
    public class MonsterStateMachine : StateMachine
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map41;
        private CommandHandler handler;

        private void Awake()
        {
            base.AddState(new MonsterIdle());
            base.AddState(new MonsterMoveAttackEnemy());
            base.AddState(new MonsterDeath());
            base.AddState(new MonsterSkill());
            base.AddState(new MonsterHurt());
        }

        public override bool CheckCommand()
        {
            if (base.cmds.Count <= 0)
            {
                return false;
            }
            base.lastCommand = base.cmds[0];
            base.cmds.RemoveAt(0);
            Log.AI(base.gameObject.name, " lastCommand " + base.lastCommand.stringCmd);
            string key = base.lastCommand.cmd[0];
            if (key != null)
            {
                int num;
                if (<>f__switch$map41 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                    dictionary.Add("move_attack", 0);
                    dictionary.Add("idle", 1);
                    dictionary.Add("skill", 2);
                    dictionary.Add("hurt", 3);
                    <>f__switch$map41 = dictionary;
                }
                if (<>f__switch$map41.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_ATTACK);

                        case 1:
                            return base.ChangeState(AIState.AIStateEnum.IDLE);

                        case 2:
                            return base.ChangeState(AIState.AIStateEnum.SKILL);

                        case 3:
                            return base.ChangeState(AIState.AIStateEnum.HURT);
                    }
                }
            }
            return base.GetCurState().CheckCommand(base.lastCommand);
        }

        private void Start()
        {
            this.handler = base.GetComponent<CommandHandler>();
            this.handler.AddHandler("move_attack", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("idle", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("skill", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("TryToMove", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("hurt", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("death", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("death_fly", new CommandHandler.CmdHandler(this.OnCmd));
            base.ChangeState(AIState.AIStateEnum.IDLE);
        }

        private void Update()
        {
            this.handler.Execute();
        }
    }
}

