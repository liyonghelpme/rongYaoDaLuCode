﻿namespace com.liyong
{
    using Pathfinding;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    [RequireComponent(typeof(BoidObject))]
    public class MoveComponent : MonoBehaviour
    {
        private const float _adjustTime = 0.5f;
        private float _lastMoveByGridTime;
        private int _lastRotateDir;
        private Vector3 debugDir = Vector3.zero;
        public float ForceCoff = 0.1f;
        private int maxMoveId = -1;
        private Vector3 moveDir = Vector3.zero;
        private float pickNextWaypointDist = 0.1f;

        [DebuggerHidden]
        private IEnumerator MoveByGrid(Vector3 realDestination, float mvSpeed, int curId)
        {
            return new <MoveByGrid>c__Iterator4B { realDestination = realDestination, mvSpeed = mvSpeed, curId = curId, <$>realDestination = realDestination, <$>mvSpeed = mvSpeed, <$>curId = curId, <>f__this = this };
        }

        [DebuggerHidden]
        public IEnumerator MoveByPath(Path path, Vector3 realDestination, float mvSpeed, MonsterMoveAI.MoveResult result, int curId, BoidObject boidTarget, bool ignoreCollision, CommandHandler.Command cmd)
        {
            return new <MoveByPath>c__Iterator49 { 
                cmd = cmd, curId = curId, realDestination = realDestination, path = path, boidTarget = boidTarget, ignoreCollision = ignoreCollision, mvSpeed = mvSpeed, result = result, <$>cmd = cmd, <$>curId = curId, <$>realDestination = realDestination, <$>path = path, <$>boidTarget = boidTarget, <$>ignoreCollision = ignoreCollision, <$>mvSpeed = mvSpeed, <$>result = result, 
                <>f__this = this
             };
        }

        [DebuggerHidden]
        private IEnumerator MoveDirectly(Vector3 realDestination, float mvSpeed)
        {
            return new <MoveDirectly>c__Iterator4A();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(base.transform.position, (Vector3) (this.debugDir * 1f));
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(base.transform.position, (Vector3) (this.moveDir * 1f));
        }

        private float RotateToTarget(Vector3 dir)
        {
            Quaternion rotation = base.transform.rotation;
            Quaternion to = Quaternion.LookRotation(dir);
            Vector3 eulerAngles = Quaternion.Slerp(rotation, to, 10f * Time.deltaTime).eulerAngles;
            eulerAngles.z = 0f;
            eulerAngles.x = 0f;
            rotation = Quaternion.Euler(eulerAngles);
            base.transform.rotation = rotation;
            return Mathf.Abs((float) (to.eulerAngles.y - rotation.eulerAngles.y));
        }

        public void StopMove(int maxId)
        {
            this.maxMoveId = maxId;
            base.GetComponent<PhysicComponent>().inMove = false;
        }

        [CompilerGenerated]
        private sealed class <MoveByGrid>c__Iterator4B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal int <$>curId;
            internal float <$>mvSpeed;
            internal Vector3 <$>realDestination;
            internal MoveComponent <>f__this;
            internal Vector3 <diff>__3;
            internal int <dir>__1;
            internal Vector3 <moveSpeed>__7;
            internal Transform <mTransform>__2;
            internal float <passTime>__8;
            internal PhysicComponent <physic>__6;
            internal float <res>__9;
            internal float <rotateDeg>__4;
            internal Vector3 <rotateDir>__5;
            internal StateMachine <state>__0;
            internal int curId;
            internal float mvSpeed;
            internal Vector3 realDestination;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<state>__0 = this.<>f__this.GetComponent<StateMachine>();
                        this.<state>__0.otherState = "_avoid";
                        Log.AI(this.<>f__this.gameObject, " HoldTime too Long Just Move Around ");
                        this.<dir>__1 = UnityEngine.Random.Range(0, 2);
                        if ((Time.time - this.<>f__this._lastMoveByGridTime) >= 4f)
                        {
                            this.<>f__this._lastMoveByGridTime = Time.time;
                            this.<>f__this._lastRotateDir = this.<dir>__1;
                            break;
                        }
                        this.<dir>__1 = this.<>f__this._lastRotateDir;
                        break;

                    case 1:
                        goto Label_024F;

                    default:
                        goto Label_028C;
                }
                this.<mTransform>__2 = this.<>f__this.transform;
                this.<diff>__3 = this.realDestination - this.<mTransform>__2.position;
                this.<rotateDeg>__4 = 90f;
                if (this.<dir>__1 == 0)
                {
                    this.<rotateDeg>__4 = 90f;
                    this.<rotateDir>__5 = (Vector3) (Quaternion.Euler(new Vector3(0f, 90f, 0f)) * this.<diff>__3);
                }
                else
                {
                    this.<rotateDeg>__4 = -90f;
                    this.<rotateDir>__5 = (Vector3) (Quaternion.Euler(new Vector3(0f, -90f, 0f)) * this.<diff>__3);
                }
                this.<rotateDir>__5.Normalize();
                this.<physic>__6 = this.<>f__this.GetComponent<PhysicComponent>();
                this.<moveSpeed>__7 = (Vector3) (this.<rotateDir>__5 * this.mvSpeed);
                this.<passTime>__8 = 0f;
            Label_024F:
                while ((this.<passTime>__8 < 0.5f) && (this.curId == this.<>f__this.maxMoveId))
                {
                    this.<diff>__3 = this.realDestination - this.<mTransform>__2.position;
                    this.<rotateDir>__5 = (Vector3) (Quaternion.Euler(new Vector3(0f, this.<rotateDeg>__4, 0f)) * this.<diff>__3);
                    this.<res>__9 = this.<>f__this.RotateToTarget(this.<rotateDir>__5);
                    if (this.<res>__9 < 30f)
                    {
                        this.<physic>__6.MoveSpeed(this.<moveSpeed>__7);
                    }
                    this.<passTime>__8 += Time.deltaTime;
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.<state>__0.otherState = string.Empty;
                this.$PC = -1;
            Label_028C:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <MoveByPath>c__Iterator49 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BoidObject <$>boidTarget;
            internal CommandHandler.Command <$>cmd;
            internal int <$>curId;
            internal bool <$>ignoreCollision;
            internal float <$>mvSpeed;
            internal Path <$>path;
            internal Vector3 <$>realDestination;
            internal MonsterMoveAI.MoveResult <$>result;
            internal MoveComponent <>f__this;
            internal BoidObject <boidObject>__6;
            internal BoxObject <boxTarget>__9;
            internal float <cmdRate>__0;
            internal float <cosVal>__7;
            internal int <currentWaypointIndex>__3;
            internal float <diffY>__17;
            internal Vector3 <dir>__11;
            internal float <dist>__12;
            internal Vector3 <force>__14;
            internal float <holdTime>__5;
            internal Vector3 <lastPos>__4;
            internal Vector3 <moveSpeed>__16;
            internal Vector3 <normalDir>__13;
            internal PhysicComponent <physic>__2;
            internal float <rate>__15;
            internal bool <slowDown>__8;
            internal StateMachine <state>__10;
            internal List<Vector3> <vPath>__1;
            internal BoidObject boidTarget;
            internal CommandHandler.Command cmd;
            internal int curId;
            internal bool ignoreCollision;
            internal float mvSpeed;
            internal Path path;
            internal Vector3 realDestination;
            internal MonsterMoveAI.MoveResult result;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<cmdRate>__0 = 1f;
                        if (this.cmd != null)
                        {
                            this.<cmdRate>__0 = this.cmd.rate;
                            Log.AI(this.<>f__this.gameObject, " MoveComponentSpeedUp " + this.<cmdRate>__0);
                        }
                        this.<>f__this.maxMoveId = Mathf.Max(this.<>f__this.maxMoveId, this.curId);
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " Start New Move Target 开始新的移动 目标点是什么 ", this.realDestination, " curId ", this.curId, " maxMoveId ", this.<>f__this.maxMoveId }));
                        if (this.<>f__this.maxMoveId != this.curId)
                        {
                            goto Label_070B;
                        }
                        this.<vPath>__1 = this.path.vectorPath;
                        if (this.<vPath>__1.Count == 0)
                        {
                            this.<vPath>__1.Insert(0, this.<>f__this.transform.position);
                        }
                        this.<vPath>__1[this.<vPath>__1.Count - 1] = this.realDestination;
                        this.<physic>__2 = this.<>f__this.GetComponent<PhysicComponent>();
                        this.<physic>__2.inMove = true;
                        Log.AI(null, " inMove True ");
                        this.<currentWaypointIndex>__3 = 1;
                        this.<lastPos>__4 = this.<>f__this.transform.position;
                        this.<holdTime>__5 = 0f;
                        this.<boidObject>__6 = this.<>f__this.GetComponent<BoidObject>();
                        this.<cosVal>__7 = 1f;
                        this.<slowDown>__8 = false;
                        this.<boxTarget>__9 = null;
                        if (this.boidTarget != null)
                        {
                            this.<boxTarget>__9 = this.boidTarget.GetBoxObject();
                        }
                        this.<state>__10 = this.<>f__this.GetComponent<StateMachine>();
                        break;

                    case 1:
                        goto Label_04A4;

                    case 2:
                        break;

                    case 3:
                        goto Label_063C;

                    case 4:
                        if (this.curId == this.<>f__this.maxMoveId)
                        {
                            this.result.ok = true;
                            this.<>f__this.StopMove(this.<>f__this.maxMoveId);
                        }
                        Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { " Finish Move ", this.curId, " MaxMove ", this.<>f__this.maxMoveId }));
                        this.<state>__10.otherState = string.Empty;
                        this.$PC = -1;
                        goto Label_070B;

                    default:
                        goto Label_070B;
                }
                while (this.curId == this.<>f__this.maxMoveId)
                {
                    Vector3 vector;
                    if (this.<currentWaypointIndex>__3 >= this.<vPath>__1.Count)
                    {
                        break;
                    }
                    this.<dir>__11 = this.<vPath>__1[this.<currentWaypointIndex>__3] - this.<>f__this.transform.position;
                    this.<dir>__11.y = 0f;
                    this.<dist>__12 = this.<dir>__11.sqrMagnitude;
                    if (this.<dist>__12 < (this.<>f__this.pickNextWaypointDist * this.<>f__this.pickNextWaypointDist))
                    {
                        this.<currentWaypointIndex>__3++;
                        Log.AI(this.<>f__this.gameObject, " MoveToNextWayPoint " + this.<currentWaypointIndex>__3);
                    }
                    else
                    {
                        this.<normalDir>__13 = this.<dir>__11.normalized;
                        this.<>f__this.moveDir = this.<normalDir>__13;
                        if (!this.ignoreCollision)
                        {
                            this.<force>__14 = this.<boidObject>__6.GetModifySpeed(this.<boxTarget>__9);
                            this.<>f__this.debugDir = this.<force>__14;
                            this.<cosVal>__7 = Vector3.Dot(this.<normalDir>__13, this.<>f__this.debugDir);
                            if (this.<cosVal>__7 < 0f)
                            {
                                if (this.<dir>__11.sqrMagnitude < 16f)
                                {
                                    break;
                                }
                                if (this.<dir>__11.sqrMagnitude > 1f)
                                {
                                }
                            }
                            else
                            {
                                this.<slowDown>__8 = false;
                            }
                        }
                        Log.AI(this.<>f__this.gameObject, " debugDir " + this.<>f__this.debugDir);
                        this.<rate>__15 = 1f;
                        if (this.<slowDown>__8)
                        {
                            this.<rate>__15 = 0.5f;
                        }
                        else
                        {
                            this.<state>__10.otherState = "_" + this.<currentWaypointIndex>__3;
                        }
                        this.<moveSpeed>__16 = (Vector3) (((this.mvSpeed * this.<normalDir>__13) * this.<rate>__15) * this.<cmdRate>__0);
                        this.<diffY>__17 = this.<>f__this.RotateToTarget(this.<normalDir>__13);
                        if (this.<diffY>__17 < 30f)
                        {
                            this.<physic>__2.MoveSpeed(this.<moveSpeed>__16);
                        }
                        else
                        {
                            this.<physic>__2.MoveSpeed(Vector3.zero);
                        }
                        this.$current = null;
                        this.$PC = 1;
                        goto Label_070D;
                    }
                Label_04A4:
                    vector = this.<>f__this.transform.position - this.<lastPos>__4;
                    if (vector.sqrMagnitude < 0.4f)
                    {
                        this.<holdTime>__5 += Time.deltaTime;
                    }
                    else
                    {
                        this.<lastPos>__4 = this.<>f__this.transform.position;
                        this.<holdTime>__5 = 0f;
                    }
                    if (this.<holdTime>__5 >= 0.5f)
                    {
                        break;
                    }
                    this.$current = null;
                    this.$PC = 2;
                    goto Label_070D;
                }
                if (this.curId != this.<>f__this.maxMoveId)
                {
                    Log.AI(this.<>f__this.gameObject, string.Concat(new object[] { "  currentMove 当前行走 进程Id 被终止 ", this.curId, "  maxId ", this.<>f__this.maxMoveId }));
                }
                else if (this.<holdTime>__5 >= 0.5f)
                {
                    Log.AI(this.<>f__this.gameObject, " hold Time too long");
                }
                else if (this.<cosVal>__7 < 0f)
                {
                    Log.AI(this.<>f__this.gameObject, " Collision With Forward");
                    this.$current = this.<>f__this.StartCoroutine(this.<>f__this.MoveByGrid(this.realDestination, this.mvSpeed, this.curId));
                    this.$PC = 3;
                    goto Label_070D;
                }
            Label_063C:
                this.$current = this.<>f__this.StartCoroutine(this.<>f__this.MoveDirectly(this.realDestination, this.mvSpeed));
                this.$PC = 4;
                goto Label_070D;
            Label_070B:
                return false;
            Label_070D:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <MoveDirectly>c__Iterator4A : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

