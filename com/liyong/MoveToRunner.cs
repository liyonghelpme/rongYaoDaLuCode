﻿namespace com.liyong
{
    using System;
    using UnityEngine;
    using UnityLog;

    public class MoveToRunner : CommandRunner
    {
        private const float MinMoveDist = 0.3f;

        public MoveToRunner(CommandHandler.Command cmd, GameObject g) : base(cmd, g)
        {
        }

        public override bool CheckNextCommand(CommandHandler.Command cmd)
        {
            return ((cmd.cmd[1] == "moveTo") || ((cmd.cmd[1] == "skill") && (base.handler.CheckCommandCached() > 0)));
        }

        public override void RunCommand()
        {
            CommandHandler.Command myCmd = base.myCmd;
            float rotY = Convert.ToSingle(myCmd.cmd[2]);
            float x = Convert.ToSingle(myCmd.cmd[3]);
            float y = Convert.ToSingle(myCmd.cmd[4]);
            float z = Convert.ToSingle(myCmd.cmd[5]);
            Vector3 a = new Vector3(x, y, z);
            float num5 = Util.XZSqrMagnitude(a, base.owner.transform.position);
            if ((num5 > 0.09f) && (base.handler.CheckCommandCached() <= 2))
            {
                float num6 = 1f;
                if (base.handler.CheckCommandCached() > 0)
                {
                    Log.AI(base.owner, " MoveTo SpeedUp");
                    num6 = 2f;
                }
                myCmd.rate = num6;
                Log.Net(string.Concat(new object[] { base.owner, " MoveToUseSkill ", num5, " tarPos ", a, " RotateY ", rotY }));
                CommandHandler.AddCommandStatic(base.owner, "move_attack -1", 1f);
                CombatUtil.NetMoveToPos(base.owner, a, rotY, true, myCmd);
            }
            else
            {
                base.myCmd.isRunning = false;
                CombatUtil.RotateDir(base.owner, rotY);
            }
        }
    }
}

