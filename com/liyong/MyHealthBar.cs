﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MyHealthBar : MonoBehaviour
    {
        public bool addYet;
        public bool alwaysShow;
        public GameObject blueHealthBarPrefab;
        public int gridX;
        public int gridY;
        public GameObject healthBarPrefab;
        public static GameObject hudView;
        public Vector3 idealPos;
        private Transform mHealthbarTransform;
        private bool quit;
        public GameObject redHealthBarPrefab;
        private Vector3 targetPos;

        private void AdjustHealthBarPos()
        {
            Vector3 position = base.transform.position;
            Vector3 vector2 = Camera.main.WorldToScreenPoint(position);
            vector2.z = 0f;
            Vector3 vector3 = UICamera.currentCamera.ScreenToWorldPoint(vector2);
            Vector3 vector4 = this.mHealthbarTransform.position;
            this.mHealthbarTransform.position = vector3;
            this.idealPos = this.mHealthbarTransform.localPosition;
            this.mHealthbarTransform.position = vector4;
        }

        private void Awake()
        {
        }

        private bool CheckFarAwayCamera()
        {
            Vector3 vector = Camera.main.transform.position - base.transform.position;
            return (vector.sqrMagnitude >= 100f);
        }

        private bool CheckInCamera()
        {
            if (!this.alwaysShow)
            {
                if (!this.addYet && !HealthBarManager.Instance.CheckActiveBloodNum())
                {
                    return false;
                }
                if (this.CheckFarAwayCamera())
                {
                    return false;
                }
            }
            return this.CheckInCameraRange();
        }

        private bool CheckInCameraRange()
        {
            Vector3 vector = Camera.main.WorldToScreenPoint(base.transform.position);
            return ((((vector.x < (Screen.width + 200)) && (vector.x > -200f)) && ((vector.y > -200f) && (vector.y < (Screen.height + 200)))) && (vector.z > 0f));
        }

        public bool InCamera()
        {
            return this.CheckInCameraRange();
        }

        [DebuggerHidden]
        private IEnumerator InitHealthBar()
        {
            return new <InitHealthBar>c__Iterator59 { <>f__this = this };
        }

        private void OnDestroy()
        {
            if (this.healthBar != null)
            {
                this.healthBar.GetComponent<UpdateHpBar>().RemoveSelf();
            }
            HealthBarManager.Instance.RemoveHealthBar(this);
        }

        private void OnDisable()
        {
        }

        private void OnEnable()
        {
        }

        public void SetHealthBar()
        {
            uint myCamp = ObjectManager.GetMyCamp(base.transform.parent.parent.gameObject);
            if ((base.transform.parent != null) && (base.transform.parent.parent != null))
            {
                switch (myCamp)
                {
                    case 0:
                        this.healthBar = NGUITools.AddChild(hudView, this.blueHealthBarPrefab);
                        goto Label_00C6;

                    case 1:
                        this.healthBar = NGUITools.AddChild(hudView, this.redHealthBarPrefab);
                        goto Label_00C6;

                    case 100:
                        this.healthBar = NGUITools.AddChild(hudView, this.redHealthBarPrefab);
                        goto Label_00C6;
                }
                this.healthBar = null;
            }
        Label_00C6:
            if (this.healthBar != null)
            {
                this.mHealthbarTransform = this.healthBar.transform;
                this.healthBar.transform.position = new Vector3(-1000f, 0f, 0f);
                this.healthBar.AddComponent<UpdateHpBar>();
                this.healthBar.GetComponent<UISlider>().value = 1f;
            }
        }

        private void Start()
        {
            this.healthBar.name = base.transform.parent.parent.name;
            base.StartCoroutine(this.UpdateHealthBarPos());
        }

        [DebuggerHidden]
        private IEnumerator UpdateHealthBarPos()
        {
            return new <UpdateHealthBarPos>c__Iterator5A { <>f__this = this };
        }

        public GameObject healthBar { get; private set; }

        [CompilerGenerated]
        private sealed class <InitHealthBar>c__Iterator59 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MyHealthBar <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 2:
                        if (this.<>f__this.healthBar == null)
                        {
                            this.$current = new WaitForSeconds(0.1f);
                            this.$PC = 1;
                            goto Label_0083;
                        }
                        this.$PC = -1;
                        break;

                    case 1:
                        this.<>f__this.SetHealthBar();
                        this.$current = null;
                        this.$PC = 2;
                        goto Label_0083;
                }
                return false;
            Label_0083:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateHealthBarPos>c__Iterator5A : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MyHealthBar <>f__this;
            internal Vector3 <curPos>__2;
            internal int <idleFrame>__3;
            internal Vector2 <initPos>__0;
            internal float <lastSwitch>__4;
            internal HealthBarManager.UpdateResult <newPos1>__7;
            internal bool <newState>__6;
            internal bool <oldState>__5;
            internal float <rd>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        this.$PC = 1;
                        goto Label_025D;

                    case 1:
                        this.<>f__this.AdjustHealthBarPos();
                        this.<>f__this.mHealthbarTransform.localPosition = this.<>f__this.idealPos;
                        this.<>f__this.targetPos = this.<>f__this.idealPos;
                        if (!this.<>f__this.alwaysShow)
                        {
                            if (!this.<>f__this.CheckInCamera())
                            {
                                this.<>f__this.healthBar.SetActive(false);
                                break;
                            }
                            this.<initPos>__0 = HealthBarManager.Instance.AddHealthBar(this.<>f__this);
                            this.<>f__this.mHealthbarTransform.localPosition = (Vector3) this.<initPos>__0;
                            this.<>f__this.targetPos = (Vector3) this.<initPos>__0;
                        }
                        break;

                    case 2:
                        goto Label_0137;
                        this.$PC = -1;
                        goto Label_025B;

                    default:
                        goto Label_025B;
                }
                this.<rd>__1 = UnityEngine.Random.Range((float) 0.1f, (float) 0.2f);
                this.<curPos>__2 = this.<>f__this.targetPos;
                this.<idleFrame>__3 = 0;
                this.<lastSwitch>__4 = 0f;
                this.<oldState>__5 = false;
                this.<newState>__6 = false;
            Label_0137:
                if (this.<>f__this.CheckInCamera())
                {
                    this.<newState>__6 = true;
                }
                else
                {
                    this.<newState>__6 = false;
                }
                if ((this.<newState>__6 != this.<oldState>__5) && ((Time.time - this.<lastSwitch>__4) > 1f))
                {
                    this.<lastSwitch>__4 = Time.time;
                    this.<oldState>__5 = this.<newState>__6;
                }
                if (this.<oldState>__5)
                {
                    this.<>f__this.AdjustHealthBarPos();
                    this.<>f__this.mHealthbarTransform.localPosition = this.<>f__this.idealPos;
                    this.<>f__this.healthBar.SetActive(true);
                    if (!this.<>f__this.alwaysShow)
                    {
                        this.<newPos1>__7 = HealthBarManager.Instance.UpdateHealthBar(this.<>f__this, this.<idleFrame>__3);
                    }
                }
                else
                {
                    this.<>f__this.healthBar.SetActive(false);
                    if (!this.<>f__this.alwaysShow)
                    {
                        HealthBarManager.Instance.RemoveHealthBar(this.<>f__this);
                    }
                }
                this.$current = null;
                this.$PC = 2;
                goto Label_025D;
            Label_025B:
                return false;
            Label_025D:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

