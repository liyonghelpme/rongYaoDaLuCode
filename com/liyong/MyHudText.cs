﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class MyHudText : MonoBehaviour
    {
        public AnimationCurve alphaCurve;
        public float animationTime = 1f;
        public AnimationCurve colorCurve;
        public float deltaInitOffsetY;
        public float deltaOffSetX;
        public Color edge1;
        public Color edge2;
        public static Camera gameCamera;
        private int gId;
        public Color gradient1;
        public Color gradient2;
        public Color gradient3;
        public Color gradient4;
        public static GameObject hudview;
        public bool isChild;
        private float lastShowTime;
        private int MAX_NUM;
        public float minWaitTime;
        private List<NumToShow> numbersStack = new List<NumToShow>();
        public Vector3 offsetToParent;
        public float posMagnitude;
        public AnimationCurve posYCurve;
        private static float publicLastTimeStamp;
        public List<float> rateTableList;
        public AnimationCurve scaleCurve;
        public float scaleMagnitude;
        public UILabel textFont;
        public static Camera uiCamera;
        private List<ShowingLabel> uiStack = new List<ShowingLabel>();
        public bool useColor;

        public MyHudText()
        {
            Keyframe[] keys = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(1f, 1f) };
            this.scaleCurve = new AnimationCurve(keys);
            this.scaleMagnitude = 1f;
            Keyframe[] keyframeArray2 = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(1f, 1f) };
            this.posYCurve = new AnimationCurve(keyframeArray2);
            this.posMagnitude = 1f;
            Keyframe[] keyframeArray3 = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(1f, 1f) };
            this.alphaCurve = new AnimationCurve(keyframeArray3);
            this.gradient1 = Color.white;
            this.gradient2 = Color.white;
            this.edge1 = Color.white;
            this.gradient3 = Color.white;
            this.gradient4 = Color.white;
            this.edge2 = Color.white;
            Keyframe[] keyframeArray4 = new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(1f, 1f) };
            this.colorCurve = new AnimationCurve(keyframeArray4);
            this.rateTableList = new List<float>();
            this.minWaitTime = 0.5f;
            this.deltaInitOffsetY = 30f;
            this.deltaOffSetX = 30f;
            this.MAX_NUM = 10;
            this.offsetToParent = new Vector3();
        }

        public void AddCriticalText(string num, Vector3 point, int type = 0)
        {
            if (this.numbersStack.Count <= this.MAX_NUM)
            {
                NumToShow item = new NumToShow {
                    num = num,
                    point = point,
                    isCritical = true,
                    textType = type
                };
                this.numbersStack.Add(item);
            }
        }

        public void AddText(string num, Vector3 point, Util.Int2 initPos = null)
        {
            if (this.numbersStack.Count <= this.MAX_NUM)
            {
                NumToShow item = new NumToShow {
                    num = num,
                    point = point,
                    initPos = initPos
                };
                this.numbersStack.Add(item);
            }
        }

        private void AdjustGoObj(ShowingLabel showLabel)
        {
            Vector3 position = Camera.main.WorldToScreenPoint(showLabel.worldPosition);
            position.z = 0f;
            Vector3 vector2 = UICamera.currentCamera.ScreenToWorldPoint(position);
            showLabel.go.transform.position = vector2;
        }

        [DebuggerHidden]
        private IEnumerator AnimateText(ShowingLabel showLabel)
        {
            return new <AnimateText>c__Iterator5C { showLabel = showLabel, <$>showLabel = showLabel, <>f__this = this };
        }

        private Util.Int2 GetInitPos(ShowingLabel showLabel)
        {
            List<float> rateTableList = this.rateTableList;
            int num = showLabel.order % 3;
            float num2 = (2 - num) * this.deltaInitOffsetY;
            float num3 = (num - 1) * this.deltaOffSetX;
            return new Util.Int2 { x = (int) num3, y = (int) num2 };
        }

        private void OnDestroy()
        {
            foreach (ShowingLabel label in this.uiStack)
            {
                UnityEngine.Object.Destroy(label.go);
            }
            this.uiStack.Clear();
            this.numbersStack.Clear();
        }

        [DebuggerHidden]
        private IEnumerator ShowNum()
        {
            return new <ShowNum>c__Iterator5B { <>f__this = this };
        }

        private void Start()
        {
            base.StartCoroutine(this.ShowNum());
        }

        [CompilerGenerated]
        private sealed class <AnimateText>c__Iterator5C : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MyHudText.ShowingLabel <$>showLabel;
            internal MyHudText <>f__this;
            internal float <alphaValue>__14;
            internal float <colorValue>__16;
            internal int <curOrder>__7;
            internal Util.Int2 <initPos>__1;
            internal int <initX>__2;
            internal int <initY>__3;
            internal UILabel <label>__4;
            internal float <newY>__15;
            internal float <passTime>__0;
            internal float <posYValue>__13;
            internal float <rate>__11;
            internal int <rateId>__9;
            internal List<float> <rateTable>__5;
            internal float <rateValue>__10;
            internal float <scaleValue>__12;
            internal int <startNum>__6;
            internal int <totalCount>__8;
            internal MyHudText.ShowingLabel showLabel;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        this.$PC = 1;
                        goto Label_045C;

                    case 1:
                        this.<passTime>__0 = 0f;
                        this.<initPos>__1 = this.<>f__this.GetInitPos(this.showLabel);
                        this.<initX>__2 = this.<initPos>__1.x;
                        this.<initY>__3 = this.<initPos>__1.y;
                        this.<label>__4 = this.showLabel.label.GetComponent<UILabel>();
                        this.<rateTable>__5 = this.<>f__this.rateTableList;
                        if (this.showLabel.numShow.isCritical)
                        {
                            this.<>f__this.transform.GetChild(0).GetComponent<MyHudText>().AddText(this.showLabel.numShow.num, this.showLabel.numShow.point, this.<initPos>__1);
                        }
                        if (this.<>f__this.isChild)
                        {
                            this.<initX>__2 = this.showLabel.numShow.initPos.x + ((int) this.<>f__this.offsetToParent.x);
                            this.<initY>__3 = this.showLabel.numShow.initPos.y + ((int) this.<>f__this.offsetToParent.y);
                        }
                        break;

                    case 2:
                        break;

                    default:
                        goto Label_045A;
                }
                while (this.<passTime>__0 < (this.<>f__this.animationTime * 1.2f))
                {
                    this.<>f__this.AdjustGoObj(this.showLabel);
                    this.<startNum>__6 = this.<>f__this.uiStack[0].order;
                    this.<curOrder>__7 = this.showLabel.order - this.<startNum>__6;
                    this.<totalCount>__8 = Mathf.Min(this.<rateTable>__5.Count, this.<>f__this.uiStack.Count);
                    this.<rateId>__9 = Mathf.Min(Mathf.Max(0, (this.<totalCount>__8 - 1) - this.<curOrder>__7), 2);
                    this.<rateValue>__10 = this.<rateTable>__5[this.<rateId>__9];
                    this.<rate>__11 = Mathf.Min((float) 1f, (float) (this.<passTime>__0 / this.<>f__this.animationTime));
                    this.<scaleValue>__12 = this.<>f__this.scaleCurve.Evaluate(this.<rate>__11) * this.<>f__this.scaleMagnitude;
                    this.<posYValue>__13 = this.<>f__this.posYCurve.Evaluate(this.<rate>__11);
                    this.<alphaValue>__14 = this.<>f__this.alphaCurve.Evaluate(this.<rate>__11);
                    this.<label>__4.cachedTransform.localScale = new Vector3(this.<scaleValue>__12, this.<scaleValue>__12, this.<scaleValue>__12);
                    this.<label>__4.alpha = this.<alphaValue>__14;
                    this.<newY>__15 = Mathf.Lerp(this.<label>__4.cachedTransform.localPosition.y, this.<initY>__3 + ((this.<posYValue>__13 * this.<>f__this.posMagnitude) * this.<rateValue>__10), Time.deltaTime * 10f);
                    this.<label>__4.cachedTransform.localPosition = new Vector3((float) this.<initX>__2, this.<newY>__15, 0f);
                    if (this.<>f__this.useColor)
                    {
                        this.<colorValue>__16 = this.<>f__this.colorCurve.Evaluate(this.<rate>__11);
                        this.<label>__4.gradientBottom = Color.Lerp(this.<>f__this.gradient1, this.<>f__this.gradient3, this.<colorValue>__16);
                        this.<label>__4.gradientTop = Color.Lerp(this.<>f__this.gradient2, this.<>f__this.gradient4, this.<colorValue>__16);
                        this.<label>__4.effectColor = Color.Lerp(this.<>f__this.edge1, this.<>f__this.edge2, this.<colorValue>__16);
                    }
                    this.<passTime>__0 += Time.deltaTime;
                    this.$current = null;
                    this.$PC = 2;
                    goto Label_045C;
                }
                this.<>f__this.uiStack.Remove(this.showLabel);
                UnityEngine.Object.Destroy(this.showLabel.go);
                this.$PC = -1;
            Label_045A:
                return false;
            Label_045C:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowNum>c__Iterator5B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MyHudText <>f__this;
            internal MyHudText.ShowingLabel <entry>__6;
            internal GameObject <go>__0;
            internal MyHudText.NumToShow <numShow>__5;
            internal Vector3 <pt>__2;
            internal GameObject <text>__4;
            internal Vector3 <uiPoint>__3;
            internal Vector3 <worldPos>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        break;

                    case 2:
                        break;
                        this.$PC = -1;
                        goto Label_02F9;

                    default:
                        goto Label_02F9;
                }
                if (this.<>f__this.numbersStack.Count > 0)
                {
                    if (!ObjectManager.CheckInCameraRange(this.<>f__this.gameObject))
                    {
                        Log.AI(this.<>f__this.gameObject, " Not In Camera Range HudText");
                        this.$current = null;
                        this.$PC = 1;
                        goto Label_02FB;
                    }
                    this.<>f__this.lastShowTime = Time.time;
                    MyHudText.publicLastTimeStamp = Time.time;
                    this.<go>__0 = NGUITools.AddChild(MyHudText.hudview);
                    this.<go>__0.name = string.Empty + this.<>f__this.gId;
                    this.<worldPos>__1 = this.<>f__this.numbersStack[0].point;
                    this.<pt>__2 = Camera.main.WorldToScreenPoint(this.<worldPos>__1);
                    this.<pt>__2.z = 0f;
                    this.<uiPoint>__3 = UICamera.currentCamera.ScreenToWorldPoint(this.<pt>__2);
                    this.<go>__0.transform.position = this.<uiPoint>__3;
                    this.<go>__0.transform.localScale = Vector3.one;
                    this.<go>__0.transform.localRotation = Quaternion.identity;
                    this.<text>__4 = NGUITools.AddChild(this.<go>__0, this.<>f__this.textFont.gameObject);
                    this.<text>__4.transform.localPosition = Vector3.zero;
                    this.<text>__4.transform.localScale = Vector3.one;
                    this.<text>__4.transform.localRotation = Quaternion.identity;
                    this.<numShow>__5 = this.<>f__this.numbersStack[0];
                    if (this.<>f__this.numbersStack[0].isCritical)
                    {
                        this.<>f__this.numbersStack.RemoveAt(0);
                    }
                    else
                    {
                        this.<text>__4.GetComponent<UILabel>().text = this.<>f__this.numbersStack[0].num;
                        this.<>f__this.numbersStack.RemoveAt(0);
                    }
                    MyHudText.ShowingLabel label = new MyHudText.ShowingLabel {
                        go = this.<go>__0,
                        label = this.<text>__4,
                        timestamp = Time.time,
                        worldPosition = this.<worldPos>__1,
                        order = this.<>f__this.gId++,
                        numShow = this.<numShow>__5
                    };
                    this.<entry>__6 = label;
                    this.<>f__this.uiStack.Add(this.<entry>__6);
                    this.<>f__this.StartCoroutine(this.<>f__this.AnimateText(this.<entry>__6));
                }
                this.$current = null;
                this.$PC = 2;
                goto Label_02FB;
            Label_02F9:
                return false;
            Label_02FB:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        private class NumToShow
        {
            public Util.Int2 initPos;
            public bool isCritical;
            public string num;
            public Vector3 point;
            public int textType;
        }

        private class ShowingLabel
        {
            public GameObject go;
            public GameObject label;
            public MyHudText.NumToShow numShow;
            public int order;
            public float timestamp;
            public Vector3 worldPosition;
        }
    }
}

