﻿namespace com.liyong
{
    using com.game;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class NetDebug : MonoBehaviour
    {
        private List<string> allMsgs = new List<string>();
        private Dictionary<int, string> codeToName = new Dictionary<int, string>();
        private List<string> consoleDebug = new List<string>();
        private Dictionary<string, int> nameToCode = new Dictionary<string, int>();
        private static NetDebug netDebug;

        public static void AddConsole(string msg)
        {
            if (netDebug != null)
            {
                int num = Convert.ToInt32(msg);
                string message = netDebug.codeToName[num];
                Log.Net(message);
                netDebug.consoleDebug.Add(message);
                if (netDebug.consoleDebug.Count > 10)
                {
                    netDebug.consoleDebug.RemoveAt(0);
                }
            }
        }

        private void Awake()
        {
            <Awake>c__AnonStorey12C storeyc = new <Awake>c__AnonStorey12C {
                <>f__this = this
            };
            netDebug = this;
            storeyc.@namespace = "Proto";
            Assembly.GetExecutingAssembly().GetTypes().Where<System.Type>(new Func<System.Type, bool>(storeyc.<>m__1F6)).ToList<System.Type>().ForEach(new Action<System.Type>(storeyc.<>m__1F7));
            foreach (string str in this.allMsgs)
            {
                System.Type type = System.Type.GetType("Proto." + str);
                MethodInfo method = type.GetMethod("getCode", BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Static);
                if (method != null)
                {
                    int num = (int) method.Invoke(null, null);
                    this.codeToName[num] = type.Name;
                    this.nameToCode[type.Name] = num;
                }
            }
        }

        private void OnGUI()
        {
            if (Main.mainObj.TestNet)
            {
                GUIStyle style = GUI.skin.GetStyle("TextField");
                style.fontSize = 15;
                GUILayout.BeginVertical(new GUILayoutOption[0]);
                GUILayout.TextField(string.Join("\n", this.consoleDebug.ToArray()), style, new GUILayoutOption[0]);
                GUILayout.EndVertical();
            }
        }

        [CompilerGenerated]
        private sealed class <Awake>c__AnonStorey12C
        {
            internal NetDebug <>f__this;
            internal string @namespace;

            internal bool <>m__1F6(System.Type t)
            {
                return (t.IsClass && (t.Namespace == this.@namespace));
            }

            internal void <>m__1F7(System.Type t)
            {
                this.<>f__this.allMsgs.Add(t.Name);
            }
        }
    }
}

