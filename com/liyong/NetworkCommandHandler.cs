﻿namespace com.liyong
{
    using com.game;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    [RequireComponent(typeof(CommandHandler))]
    public class NetworkCommandHandler : MonoBehaviour
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map3D;
        private string cachePrintCmd = string.Empty;
        private CommandRunner cmdRunner;
        private List<CommandHandler.Command> cmds = new List<CommandHandler.Command>();
        private CommandHandler handler;
        private CommandHandler.Command lastCommand;
        public bool ShowCommand;
        private StateMachine stateMachine;

        public int CheckCommandCached()
        {
            return this.cmds.Count;
        }

        private void OnGUI()
        {
            if (Main.mainObj.TestNetCommand)
            {
                Vector3 vector = Camera.main.WorldToScreenPoint(base.transform.position + new Vector3(0f, 0f, 0f));
                if (this.lastCommand != null)
                {
                    List<string> list = new List<string>();
                    for (int i = Mathf.Max(this.cmds.Count - 4, 0); i < this.cmds.Count; i++)
                    {
                        list.Add(this.cmds[i].cmd[1]);
                    }
                    string text = string.Join("\n", list.ToArray());
                    GUI.skin = Main.mainObj.DebugSkin;
                    GUI.TextField(new Rect(vector.x - 50f, Screen.height - vector.y, 100f, 100f), text);
                    this.cachePrintCmd = base.gameObject + "allCmd\n" + text;
                    if (this.cmds.Count == 0)
                    {
                        GUI.Label(new Rect(vector.x - 50f, (Screen.height - vector.y) + 40f, 100f, 100f), "NoCMD");
                    }
                }
            }
        }

        private void OnNetCMD(CommandHandler.Command cmd)
        {
            cmd.time = Time.time;
            this.cmds.Add(cmd);
        }

        [DebuggerHidden]
        private IEnumerator RunCMD()
        {
            return new <RunCMD>c__Iterator6F { <>f__this = this };
        }

        private void Start()
        {
            this.handler = base.GetComponent<CommandHandler>();
            this.handler.AddHandler("networkCommand", new CommandHandler.CmdHandler(this.OnNetCMD));
            base.StartCoroutine(this.RunCMD());
            this.stateMachine = base.GetComponent<StateMachine>();
        }

        [CompilerGenerated]
        private sealed class <RunCMD>c__Iterator6F : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal NetworkCommandHandler <>f__this;
            internal CommandHandler.Command <cmd>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        break;

                    case 1:
                        Log.AI(this.<>f__this.gameObject, " WaitForLastCommand Finish " + this.<>f__this.lastCommand.stringCmd);
                        goto Label_012F;

                    case 2:
                        break;
                        this.$PC = -1;
                        goto Label_02FC;

                    default:
                        goto Label_02FC;
                }
                if (this.<>f__this.cmds.Count <= 0)
                {
                    goto Label_02DD;
                }
                this.<cmd>__0 = this.<>f__this.cmds[0];
                Log.Net(this.<>f__this.gameObject + " NetworkCommandHandler beforewait " + this.<cmd>__0.stringCmd);
                this.<>f__this.cmds.RemoveAt(0);
                Log.Net(this.<>f__this.cachePrintCmd);
                if (((this.<>f__this.cmdRunner != null) && this.<>f__this.cmdRunner.IsRunning()) && !this.<>f__this.cmdRunner.CheckNextCommand(this.<cmd>__0))
                {
                    this.$current = this.<>f__this.StartCoroutine(this.<>f__this.cmdRunner.WaitCommandFinish());
                    this.$PC = 1;
                    goto Label_02FE;
                }
            Label_012F:
                Log.Net(this.<>f__this.gameObject + " WaitNetCommand " + this.<cmd>__0.stringCmd);
                string key = this.<cmd>__0.cmd[1];
                if (key != null)
                {
                    int num2;
                    if (NetworkCommandHandler.<>f__switch$map3D == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(3);
                        dictionary.Add("moveTo", 0);
                        dictionary.Add("skill", 1);
                        dictionary.Add("hurt", 2);
                        NetworkCommandHandler.<>f__switch$map3D = dictionary;
                    }
                    if (NetworkCommandHandler.<>f__switch$map3D.TryGetValue(key, out num2))
                    {
                        switch (num2)
                        {
                            case 0:
                                Log.Net(this.<>f__this.gameObject + "MoveToCommand Run ");
                                this.<>f__this.cmdRunner = new MoveToRunner(this.<cmd>__0, this.<>f__this.gameObject);
                                this.<>f__this.cmdRunner.RunCommand();
                                goto Label_02CC;

                            case 1:
                                Log.Net(this.<>f__this.gameObject + " SkillCommand ");
                                this.<>f__this.cmdRunner = new SkillRunner(this.<cmd>__0, this.<>f__this.gameObject);
                                this.<>f__this.cmdRunner.RunCommand();
                                goto Label_02CC;

                            case 2:
                                this.<>f__this.cmdRunner = new HurtRunner(this.<cmd>__0, this.<>f__this.gameObject);
                                this.<>f__this.cmdRunner.RunCommand();
                                goto Label_02CC;
                        }
                    }
                }
                UnityEngine.Debug.LogError(this.<>f__this.gameObject + " Unknow Command " + this.<cmd>__0.stringCmd);
            Label_02CC:
                this.<>f__this.lastCommand = this.<cmd>__0;
            Label_02DD:
                this.$current = null;
                this.$PC = 2;
                goto Label_02FE;
            Label_02FC:
                return false;
            Label_02FE:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

