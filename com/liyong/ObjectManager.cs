﻿namespace com.liyong
{
    using com.game;
    using com.game.module.fight.vo;
    using com.game.module.hud;
    using com.game.module.map;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.fsmUtil;
    using PCustomDataType;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class ObjectManager : MonoBehaviour
    {
        [CompilerGenerated]
        private static Func<MonsterDisplay, <>__AnonType27<MonsterDisplay, int>> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<<>__AnonType27<MonsterDisplay, int>, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<<>__AnonType27<MonsterDisplay, int>, MonsterDisplay> <>f__am$cache5;
        private static int displayId = 0;
        private static Dictionary<int, BaseControler> displayMap = new Dictionary<int, BaseControler>();
        public static ObjectManager objectManager = null;

        public static int AddController(BaseControler controller)
        {
            displayMap[displayId] = controller;
            return displayId++;
        }

        public static void AddHealthBar(BaseDisplay display)
        {
            Log.AI(display.GoBase, " AddHealthBar For Tower ");
            HudView.Instance.AddHealthBar(display);
        }

        private void Awake()
        {
            objectManager = this;
        }

        public static bool CheckInCameraRange(GameObject g)
        {
            Vector3 vector = Camera.main.WorldToScreenPoint(g.transform.position);
            return ((((vector.x < (Screen.width + 200)) && (vector.x > -200f)) && ((vector.y > -200f) && (vector.y < (Screen.height + 200)))) && (vector.z > 0f));
        }

        private static void FilterEnemyWhenWifiPVP(ref List<ActionDisplay> attackableList)
        {
            if (<>f__am$cache3 == null)
            {
                <>f__am$cache3 = md => new <>__AnonType27<MonsterDisplay, int>(md, md.GetMeVoByType<MonsterVo>().MonsterVO.type);
            }
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = <>__TranspIdent42 => <>__TranspIdent42.monType == 5;
            }
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = <>__TranspIdent42 => <>__TranspIdent42.md;
            }
            IEnumerator<ActionDisplay> enumerator = attackableList.OfType<MonsterDisplay>().Select<MonsterDisplay, <>__AnonType27<MonsterDisplay, int>>(<>f__am$cache3).Where<<>__AnonType27<MonsterDisplay, int>>(<>f__am$cache4).Select<<>__AnonType27<MonsterDisplay, int>, MonsterDisplay>(<>f__am$cache5).Cast<ActionDisplay>().ToList<ActionDisplay>().GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    ActionDisplay current = enumerator.Current;
                    attackableList.Remove(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static GameObject FindNearestBuff(GameObject go)
        {
            IDictionary<ulong, BattleObjectDisplay> allBattleObject = BattleObjectMgr.Instance.GetAllBattleObject();
            float maxValue = float.MaxValue;
            GameObject goBase = null;
            Vector3 position = go.transform.position;
            Log.AI(go, " Buff Number " + allBattleObject.Count);
            IEnumerator<KeyValuePair<ulong, BattleObjectDisplay>> enumerator = allBattleObject.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    KeyValuePair<ulong, BattleObjectDisplay> current = enumerator.Current;
                    Log.AI(null, "Buff Object Pos " + current.Value.GoBase);
                    if (current.Value.GoBase != null)
                    {
                        float num2 = Util.XZSqrMagnitude(position, current.Value.GoBase.transform.position);
                        if (num2 < maxValue)
                        {
                            maxValue = num2;
                            goBase = current.Value.GoBase;
                        }
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            Log.AI(go, " near Buff Is " + goBase);
            return goBase;
        }

        public static ActionDisplay FindNearestEnemy(GameObject go, List<ActionDisplay> enemyList = null)
        {
            if (enemyList == null)
            {
                enemyList = GetEnemyDisplay(go);
            }
            ActionDisplay display = null;
            float maxValue = float.MaxValue;
            Vector3 position = go.transform.position;
            int num2 = 0;
            int count = enemyList.Count;
            while (num2 < count)
            {
                BaseControler controller = enemyList[num2].Controller;
                if (controller != null)
                {
                    Vector3 b = controller.transform.position;
                    float num4 = Util.XZSqrMagnitude(position, b);
                    if ((enemyList[num2].GetMeVoByType<BaseRoleVo>().CurHp > 0) && (num4 < maxValue))
                    {
                        maxValue = num4;
                        display = enemyList[num2];
                    }
                }
                num2++;
            }
            Log.AI(go.name, " Find nearest Monster Enemy " + display);
            return display;
        }

        public static GameObject GetAnotherTeamMember(GameObject g)
        {
            PlayerAiController anotherAi = g.GetComponent<PlayerAiController>().GetAnotherAi();
            if (anotherAi != null)
            {
                return anotherAi.gameObject;
            }
            return null;
        }

        public static GameObject GetAttackTarget(GameObject g)
        {
            AiControllerBase component = g.GetComponent<AiControllerBase>();
            if ((component != null) && (component.GetTargetDisplay() != null))
            {
                return component.GetTargetDisplay().GoBase;
            }
            return null;
        }

        public static FightAttributeVo GetBasicAttributeVo(GameObject g)
        {
            return null;
        }

        public static GameObject GetController(int id)
        {
            if (displayMap.ContainsKey(id))
            {
                return displayMap[id].gameObject;
            }
            return null;
        }

        public static ActionDisplay GetDisplay(GameObject g)
        {
            if (g == null)
            {
                return null;
            }
            return g.GetComponent<BaseControler>().GetMeByType<ActionDisplay>();
        }

        public static List<ActionDisplay> GetEnemyDisplay(GameObject me)
        {
            Log.AI(me, " GetEnemyDisplay ");
            ActionControler meController = me.GetComponent<SkillController>().MeController;
            uint camp = meController.Me.GetMeVoByType<BaseRoleVo>().Camp;
            bool isInWifiPVP = AppMap.Instance.IsInWifiPVP;
            bool enemyForWhenRole = true;
            List<ActionDisplay> attackableList = new List<ActionDisplay>();
            switch (meController.GetMeVo().Type)
            {
                case 100:
                    enemyForWhenRole = GetEnemyForWhenRole(camp, ref attackableList);
                    break;

                case 400:
                    enemyForWhenRole = GetEnemyWhenMonster(camp, isInWifiPVP, ref attackableList);
                    break;

                default:
                    Log.AI(me, " type is ");
                    break;
            }
            if (isInWifiPVP && !enemyForWhenRole)
            {
                FilterEnemyWhenWifiPVP(ref attackableList);
            }
            return attackableList;
        }

        private static bool GetEnemyForWhenRole(uint myCamp, ref List<ActionDisplay> attackableList)
        {
            Log.AI(null, "my camp " + myCamp);
            bool flag = true;
            attackableList.Clear();
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    MonsterVo meVoByType = current.GetMeVoByType<MonsterVo>();
                    Log.AI(current.GoBase, "ad camp " + meVoByType.Camp);
                    if (((current.Controller != null) && !meVoByType.IsEmptyHp) && (meVoByType.Camp != myCamp))
                    {
                        if (meVoByType.MonsterVO.type == 3)
                        {
                            flag = false;
                        }
                        attackableList.Add(current);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            Log.AI(myCamp, string.Concat(new object[] { " GetEnemyForRole ", attackableList.Count, " ", AppMap.Instance.monsterList.Count }));
            IEnumerator<PlayerDisplay> enumerator2 = AppMap.Instance.playerList.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    PlayerDisplay item = enumerator2.Current;
                    BaseRoleVo vo = item.GetVo();
                    Log.AI(myCamp, string.Concat(new object[] { " player camp ", vo.Camp, " ", item.GoBase, "  ", vo.SubType }));
                    if ((!vo.stateInfo.IsFrozen && !vo.IsEmptyHp) && (vo.Camp != myCamp))
                    {
                        attackableList.Add(item);
                    }
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
            Log.AI(myCamp, string.Concat(new object[] { " GetPlayer List ", attackableList.Count, " player ", AppMap.Instance.playerList.Count }));
            return flag;
        }

        private static bool GetEnemyWhenMonster(uint myCamp, bool isInWifiPvp, ref List<ActionDisplay> attackableList)
        {
            <GetEnemyWhenMonster>c__AnonStorey130 storey = new <GetEnemyWhenMonster>c__AnonStorey130 {
                myCamp = myCamp
            };
            bool flag = true;
            attackableList = lists.Filter<ActionDisplay>(new Predicate<ActionDisplay>(storey.<>m__1FC), AppMap.Instance.playerList.Cast<ActionDisplay>()).ToList<ActionDisplay>();
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    MonsterVo meVoByType = current.GetMeVoByType<MonsterVo>();
                    if (((current.Controller != null) && !meVoByType.IsEmptyHp) && (meVoByType.Camp != storey.myCamp))
                    {
                        if (meVoByType.MonsterVO.type == 3)
                        {
                            Log.AI(null, "Find Tower Enemy");
                            flag = false;
                        }
                        attackableList.Add(current);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return flag;
        }

        public static PNewAttr GetEquipAttr(ulong heroId, List<PNewAttr> attrs)
        {
            foreach (PNewAttr attr in attrs)
            {
                if (attr.id == heroId)
                {
                    return attr;
                }
            }
            return null;
        }

        public static FightAttributeVo GetEquipAttributeVo(GameObject g)
        {
            return null;
        }

        public static byte GetJob(GameObject g)
        {
            return g.GetComponent<BaseControler>().Me.GetMeVoByType<PlayerVo>().job;
        }

        public static UpdateAttribute.LastAttack GetLastAttackThis(GameObject g)
        {
            UpdateAttribute component = g.GetComponent<UpdateAttribute>();
            if (component != null)
            {
                return component.GetLastAttackMe();
            }
            return null;
        }

        public static GameObject GetMaster()
        {
            return AppMap.Instance.me.GoBase;
        }

        public static Vector3 GetMasterPos()
        {
            GameObject master = GetMaster();
            if (master != null)
            {
                return master.transform.position;
            }
            return Vector3.zero;
        }

        public static GameObject GetMasterTarget()
        {
            GameObject master = GetMaster();
            if (master != null)
            {
                AiControllerBase component = master.GetComponent<AiControllerBase>();
                if ((component != null) && (component.GetTargetDisplay() != null))
                {
                    return component.GetTargetDisplay().GoBase;
                }
            }
            return null;
        }

        public static MeDisplay GetMeDisplay(GameObject g)
        {
            if (g == null)
            {
                return null;
            }
            return g.GetComponent<BaseControler>().GetMeByType<MeDisplay>();
        }

        public static MeVo GetMeVo(GameObject g)
        {
            BaseControler component = g.GetComponent<BaseControler>();
            if ((component != null) && (component.Me != null))
            {
                return component.Me.GetMeVoByType<MeVo>();
            }
            return null;
        }

        public static uint GetMyCamp(GameObject g)
        {
            BaseRoleVo vo = GetVo(g);
            if (vo != null)
            {
                return vo.Camp;
            }
            return 0x3e7;
        }

        public static List<ActionDisplay> GetMyTeammatesExceptMe(GameObject me)
        {
            <GetMyTeammatesExceptMe>c__AnonStorey12F storeyf = new <GetMyTeammatesExceptMe>c__AnonStorey12F();
            Log.AI(me, " GetTeammateDisplay ");
            BaseControler component = me.GetComponent<BaseControler>();
            if (!(component is MeControler))
            {
                return null;
            }
            storeyf.meDisplay = component.Me.GetMeByType<MeDisplay>();
            storeyf.myCamp = component.Me.GetMeVoByType<BaseRoleVo>().Camp;
            return AppMap.Instance.SelfplayerList.Cast<ActionDisplay>().Where<ActionDisplay>(new Func<ActionDisplay, bool>(storeyf.<>m__1FB)).ToList<ActionDisplay>();
        }

        public static Animator GetPlayerAnimator(GameObject g)
        {
            return g.GetComponent<BaseControler>().Me.Animator;
        }

        public static GameObject GetSummoner(GameObject g)
        {
            return g.GetComponent<SummonMonsterAiController>().GetMaster();
        }

        public static GameObject GetTargetDisplay(GameObject g)
        {
            ActionDisplay targetDisplay = g.GetComponent<AiControllerBase>().GetTargetDisplay();
            if (targetDisplay != null)
            {
                return targetDisplay.GoBase;
            }
            return null;
        }

        public static BaseRoleVo GetVo(GameObject g)
        {
            BaseControler component = g.GetComponent<BaseControler>();
            if ((component != null) && (component.Me != null))
            {
                return component.Me.GetMeVoByType<BaseRoleVo>();
            }
            return null;
        }

        public static bool IsSummorStillAlive(GameObject g)
        {
            GameObject summoner = GetSummoner(g);
            if (summoner == null)
            {
                return false;
            }
            return !GetVo(summoner).IsEmptyHp;
        }

        public static void RemoveController(int id)
        {
            displayMap.Remove(id);
        }

        public static float RotateToTarget(GameObject attacker, GameObject target)
        {
            if (target == null)
            {
                return 0f;
            }
            Vector3 forward = target.transform.position - attacker.transform.position;
            forward.y = 0f;
            Quaternion rotation = attacker.transform.rotation;
            Quaternion to = Quaternion.LookRotation(forward);
            Vector3 eulerAngles = Quaternion.Slerp(rotation, to, 10f * Time.deltaTime).eulerAngles;
            eulerAngles.z = 0f;
            eulerAngles.x = 0f;
            rotation = Quaternion.Euler(eulerAngles);
            attacker.transform.rotation = rotation;
            return Mathf.Abs((float) (to.eulerAngles.y - rotation.eulerAngles.y));
        }

        [CompilerGenerated]
        private sealed class <GetEnemyWhenMonster>c__AnonStorey130
        {
            internal uint myCamp;

            internal bool <>m__1FC(ActionDisplay ad)
            {
                BaseRoleVo meVoByType = ad.GetMeVoByType<BaseRoleVo>();
                if (meVoByType.stateInfo.IsFrozen)
                {
                    return false;
                }
                return (((ad.Controller != null) && !meVoByType.IsEmptyHp) && (meVoByType.Camp != this.myCamp));
            }
        }

        [CompilerGenerated]
        private sealed class <GetMyTeammatesExceptMe>c__AnonStorey12F
        {
            internal MeDisplay meDisplay;
            internal uint myCamp;

            internal bool <>m__1FB(ActionDisplay x)
            {
                return (((x.GetMeVoByType<BaseRoleVo>().Camp == this.myCamp) && !x.GetMeVoByType<BaseRoleVo>().IsEmptyHp) && (x != this.meDisplay));
            }
        }
    }
}

