﻿namespace com.liyong
{
    using Pathfinding;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class PathComponent : MonoBehaviour
    {
        private int maxSearchId = -1;
        private Path path;
        private Seeker seeker;

        public Path GetPath()
        {
            return this.path;
        }

        [DebuggerHidden]
        public IEnumerator SearchPath(Vector3 destination, int searchId)
        {
            return new <SearchPath>c__Iterator4C { searchId = searchId, destination = destination, <$>searchId = searchId, <$>destination = destination, <>f__this = this };
        }

        private void Start()
        {
            this.seeker = base.GetComponent<Seeker>();
        }

        public void StopSearch(int maxId)
        {
            this.maxSearchId = maxId;
        }

        [CompilerGenerated]
        private sealed class <SearchPath>c__Iterator4C : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal Vector3 <$>destination;
            internal int <$>searchId;
            internal PathComponent <>f__this;
            internal float <dist>__2;
            internal bool <findPath>__0;
            internal Path <tempPath>__1;
            internal Vector3 destination;
            internal int searchId;

            internal void <>m__1B1(Path _p)
            {
                this.<findPath>__0 = true;
                this.<tempPath>__1 = _p;
            }

            internal void <>m__1B2(Path _p)
            {
                this.<findPath>__0 = true;
                this.<tempPath>__1 = _p;
            }

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<>f__this.maxSearchId = Mathf.Max(this.searchId, this.<>f__this.maxSearchId);
                        if (this.<>f__this.maxSearchId == this.searchId)
                        {
                            this.<findPath>__0 = false;
                            this.<tempPath>__1 = null;
                            this.<dist>__2 = Util.XZSqrMagnitude(this.<>f__this.transform.position, this.destination);
                            if ((AstarPath.active.graphs.Count<NavGraph>() >= 2) && (this.<dist>__2 > 16f))
                            {
                                this.<>f__this.GetComponent<FunnelModifier>().enabled = false;
                                this.<>f__this.seeker.StartPath(this.<>f__this.transform.position, this.destination, new OnPathDelegate(this.<>m__1B1), 2);
                            }
                            else
                            {
                                this.<>f__this.GetComponent<FunnelModifier>().enabled = true;
                                this.<>f__this.seeker.StartPath(this.<>f__this.transform.position, this.destination, new OnPathDelegate(this.<>m__1B2));
                            }
                            break;
                        }
                        goto Label_01A6;

                    case 1:
                        break;

                    default:
                        goto Label_01A6;
                }
                while (!this.<findPath>__0 && (this.searchId == this.<>f__this.maxSearchId))
                {
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                if (this.searchId == this.<>f__this.maxSearchId)
                {
                    this.<>f__this.path = this.<tempPath>__1;
                }
                this.$PC = -1;
            Label_01A6:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

