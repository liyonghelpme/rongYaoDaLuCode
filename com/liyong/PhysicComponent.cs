﻿namespace com.liyong
{
    using System;
    using UnityEngine;

    public class PhysicComponent : MonoBehaviour
    {
        private bool _inMove;
        private CharacterController controller;
        private Vector3 gravity = new Vector3(0f, -20f, 0f);
        private Vector3 moveValue = Vector3.zero;

        private void LateUpdate()
        {
            if (this.inMove)
            {
                this.controller.Move((Vector3) (this.moveValue * Time.deltaTime));
            }
        }

        public void MoveSpeed(Vector3 mv)
        {
            this.moveValue = mv;
            this.moveValue.y = -20f;
        }

        private void Start()
        {
            this.inMove = false;
            this.controller = base.GetComponent<CharacterController>();
        }

        private void Update()
        {
            if (!this.inMove)
            {
                this.moveValue = Vector3.Lerp(this.moveValue, Vector3.zero, Time.deltaTime);
                this.moveValue.y = -20f;
            }
        }

        public bool inMove
        {
            get
            {
                return this._inMove;
            }
            set
            {
                this._inMove = value;
            }
        }
    }
}

