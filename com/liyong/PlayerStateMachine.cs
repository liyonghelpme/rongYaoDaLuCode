﻿namespace com.liyong
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public class PlayerStateMachine : StateMachine
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map42;
        private CommandHandler cmdHandler;

        private void Awake()
        {
            base.AddState(new PlayerIdle());
            base.AddState(new PlayerSkill());
            base.AddState(new PlayerDeath());
            base.AddState(new PlayerMoveAttack());
            base.AddState(new PlayerHurt());
        }

        public override bool CheckCommand()
        {
            Log.AI(base.gameObject, " why not check Command " + base.cmds.Count);
            if (base.cmds.Count <= 0)
            {
                return false;
            }
            base.lastCommand = base.cmds[0];
            base.cmds.RemoveAt(0);
            Log.AI(base.gameObject.name, " lastCommand " + base.lastCommand.stringCmd);
            string key = base.lastCommand.cmd[0];
            if (key != null)
            {
                int num;
                if (<>f__switch$map42 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(5);
                    dictionary.Add("move_attack", 0);
                    dictionary.Add("idle", 1);
                    dictionary.Add("skill", 2);
                    dictionary.Add("death", 3);
                    dictionary.Add("hurt", 4);
                    <>f__switch$map42 = dictionary;
                }
                if (<>f__switch$map42.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_ATTACK);

                        case 1:
                            return base.ChangeState(AIState.AIStateEnum.IDLE);

                        case 2:
                            return base.ChangeState(AIState.AIStateEnum.SKILL);

                        case 3:
                            return base.ChangeState(AIState.AIStateEnum.DEATH);

                        case 4:
                            return base.ChangeState(AIState.AIStateEnum.HURT);
                    }
                }
            }
            return base.GetCurState().CheckCommand(base.lastCommand);
        }

        private void Start()
        {
            this.cmdHandler = base.GetComponent<CommandHandler>();
            Log.AI(base.gameObject, " initial PlayerStateMachine ");
            CommandHandler component = base.GetComponent<CommandHandler>();
            component.AddHandler("move_attack", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("idle", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("skill", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("death", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("TryToMove", new CommandHandler.CmdHandler(this.OnCmd));
            component.AddHandler("hurt", new CommandHandler.CmdHandler(this.OnCmd));
            base.ChangeState(AIState.AIStateEnum.IDLE);
        }

        private void Update()
        {
            this.cmdHandler.Execute();
        }
    }
}

