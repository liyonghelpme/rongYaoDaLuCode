﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public sealed class PrivateHurt : AIState
    {
        public PrivateHurt()
        {
            base.type = AIState.AIStateEnum.HURT;
        }

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            switch (s)
            {
                case AIState.AIStateEnum.SKILL:
                case AIState.AIStateEnum.MOVE_ATTACK:
                case AIState.AIStateEnum.MOVE_KEYBOARD:
                    return false;
            }
            return base.CheckNextState(s);
        }

        public override void EnterState()
        {
            base.EnterState();
            Log.AI(base.stateMachine.gameObject, " Private Hurt1 State");
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            base.stateMachine.GetComponent<MonsterStatuController>().SetStatu(10);
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator9B { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator9B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PrivateHurt <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

