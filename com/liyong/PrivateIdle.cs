﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public sealed class PrivateIdle : IdleState
    {
        private float _lastCheckTime;
        private GameObject lastEnemy;

        private CombatUtil.CheckResult CheckLastEnemy()
        {
            if (!CombatUtil.CheckTargetDead(base.stateMachine.gameObject, this.lastEnemy))
            {
                return new CombatUtil.CheckResult { ok = true, enemy = this.lastEnemy };
            }
            this.lastEnemy = null;
            this._lastCheckTime = 0f;
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckNewEnemy()
        {
            if ((Time.time - this._lastCheckTime) >= 1f)
            {
                GameObject gameObject = base.stateMachine.gameObject;
                this._lastCheckTime = Time.time;
                return CombatUtil.CheckPrivateEnemy(gameObject, this.lastEnemy);
            }
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckNextStep()
        {
            GameObject gameObject = base.stateMachine.gameObject;
            PrivateAiController component = gameObject.GetComponent<PrivateAiController>();
            int num = component._currentPathStep;
            int num2 = component._maxPathStep;
            Log.AI(gameObject, string.Concat(new object[] { " curStep maxStep ", num, " maxStep ", num2 }));
            if (num >= num2)
            {
                return CombatUtil.FalseResult;
            }
            Vector3 nextPoint = component.GetNextPoint();
            CombatUtil.MoveToPos(gameObject, nextPoint);
            return new CombatUtil.CheckResult { ok = true };
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase component = base.stateMachine.GetComponent<AiControllerBase>();
            if (component != null)
            {
                component.SetAiStatusButNotClearTarget(AiEnum.PrivateAiStatus.THINK);
            }
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator9C { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator9C : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PrivateIdle <>f__this;
            internal GameObject <g>__0;
            internal CombatUtil.CheckResult <ret>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_01A0;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    this.<ret>__1 = new CombatUtil.CheckResult();
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.CheckNewEnemy();
                        if (this.<ret>__1.ok)
                        {
                            this.<>f__this.lastEnemy = this.<ret>__1.enemy;
                            Log.AI(this.<g>__0, " MoveToAttack Enemy ");
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret>__1.enemy);
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.CheckLastEnemy();
                        if (this.<ret>__1.ok)
                        {
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret>__1.enemy);
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.CheckNextStep();
                        if (this.<ret>__1.ok)
                        {
                            Log.AI(this.<g>__0, " MoveToNextStep ");
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        Log.AI(this.<g>__0, " NotFindTarget ");
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_01A0:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

