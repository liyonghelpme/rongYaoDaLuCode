﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public sealed class PrivateMoveAttackEnemy : MoveAttackEnemy
    {
        private float _lastCheckTime;
        private GameObject oldEnemy;

        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if (cmd.cmd[0] == "TryToMove")
            {
                Log.AI(base.stateMachine.name, " TryToMove AI private ");
                float x = float.Parse(cmd.cmd[1]);
                float y = float.Parse(cmd.cmd[2]);
                float z = float.Parse(cmd.cmd[3]);
                int cid = int.Parse(cmd.cmd[4]);
                MonsterMoveAI component = base.stateMachine.GetComponent<MonsterMoveAI>();
                component.StartCoroutine(component.TryToMove(new Vector3(x, y, z), component.GetCallback(cid), null, null, false, null));
            }
            return false;
        }

        private bool CheckNewEnemy()
        {
            if ((Time.time - this._lastCheckTime) >= 1f)
            {
                GameObject gameObject = base.stateMachine.gameObject;
                this._lastCheckTime = Time.time;
                CombatUtil.CheckResult result = CombatUtil.CheckNewEnemy(gameObject, this.oldEnemy);
                if (result.ok && CombatUtil.CheckMonsterCanSee(gameObject, result.enemy))
                {
                    this.oldEnemy = result.enemy;
                    CombatUtil.CheckAttackBeforeMove(base.stateMachine.gameObject, this.oldEnemy);
                    return true;
                }
            }
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(1);
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            this.oldEnemy = ObjectManager.GetController(int.Parse(lastCommand.cmd[1]));
            if (this.oldEnemy != null)
            {
                Log.AI(base.stateMachine.gameObject.name, " private oldEnemy " + this.oldEnemy.name);
            }
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator9D { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator9D : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PrivateMoveAttackEnemy <>f__this;
            internal GameObject <g>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00D0;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    if (CombatUtil.CheckCanAttack(this.<g>__0, this.<>f__this.oldEnemy))
                    {
                        CombatUtil.AttackHim(this.<>f__this.stateMachine.gameObject, this.<>f__this.oldEnemy);
                    }
                    else if (this.<>f__this.CheckNewEnemy())
                    {
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_00D0:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

