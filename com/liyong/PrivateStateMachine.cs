﻿namespace com.liyong
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    [RequireComponent(typeof(CommandHandler))]
    public class PrivateStateMachine : StateMachine
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map43;
        private CommandHandler handler;

        private void Awake()
        {
            base.AddState(new PrivateIdle());
            base.AddState(new PrivateHurt());
            base.AddState(new PrivateMoveAttackEnemy());
            base.AddState(new PrivateDeath());
            base.AddState(new PrivateSkill());
        }

        public override bool CheckCommand()
        {
            if (base.cmds.Count <= 0)
            {
                return false;
            }
            base.lastCommand = base.cmds[0];
            base.cmds.RemoveAt(0);
            Log.AI(base.gameObject.name, " lastCommand " + base.lastCommand.stringCmd);
            string key = base.lastCommand.cmd[0];
            if (key != null)
            {
                int num;
                if (<>f__switch$map43 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(6);
                    dictionary.Add("death", 0);
                    dictionary.Add("death_fly", 0);
                    dictionary.Add("move_attack", 1);
                    dictionary.Add("idle", 2);
                    dictionary.Add("skill", 3);
                    dictionary.Add("hurt", 4);
                    <>f__switch$map43 = dictionary;
                }
                if (<>f__switch$map43.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return base.ChangeState(AIState.AIStateEnum.DEATH);

                        case 1:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_ATTACK);

                        case 2:
                            return base.ChangeState(AIState.AIStateEnum.IDLE);

                        case 3:
                            return base.ChangeState(AIState.AIStateEnum.SKILL);
                    }
                }
            }
            return base.GetCurState().CheckCommand(base.lastCommand);
        }

        private void Start()
        {
            this.handler = base.GetComponent<CommandHandler>();
            this.handler.AddHandler("move_attack", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("idle", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("skill", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("TryToMove", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("hurt", new CommandHandler.CmdHandler(this.OnCmd));
            base.ChangeState(AIState.AIStateEnum.IDLE);
        }

        private void Update()
        {
            this.handler.Execute();
        }
    }
}

