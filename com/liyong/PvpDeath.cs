﻿namespace com.liyong
{
    using com.game;
    using com.game.module.WiFiPvP;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public class PvpDeath : DeathState
    {
        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            if (((s == AIState.AIStateEnum.IDLE) && (lastCommand.cmd.Length >= 2)) && (lastCommand.cmd[1] == "relive"))
            {
                Log.AI(base.stateMachine.gameObject, " Relive From Death");
                return true;
            }
            Log.AI(base.stateMachine.gameObject, " Death Can't Change To OtherState\n" + lastCommand.stringCmd + " \n" + lastCommand.source);
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(11);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            MeDisplay meDisplay = ObjectManager.GetMeDisplay(base.stateMachine.gameObject);
            if (AppMap.Instance.IsInWifiPVP)
            {
                WifiPvpManager.Instance.IncreaseDeadCount(meDisplay);
            }
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator9F { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator9F : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PvpDeath <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

