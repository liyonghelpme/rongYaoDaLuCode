﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using com.u3d.bases.display;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class PvpIdle : IdleState
    {
        private float _lastCheckTime;
        private float lastEatBuff;
        private GameObject lastEnemy;

        private CombatUtil.CheckResult CheckInHeal()
        {
            GameObject gameObject = base.stateMachine.gameObject;
            PlayerPVPAiController component = base.stateMachine.GetComponent<PlayerPVPAiController>();
            if ((component != null) && component.IsInHeal())
            {
                return new CombatUtil.CheckResult { ok = true };
            }
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckLastEnemy()
        {
            if (!CombatUtil.CheckTargetDead(base.stateMachine.gameObject, this.lastEnemy))
            {
                return new CombatUtil.CheckResult { ok = true, enemy = this.lastEnemy };
            }
            this.lastEnemy = null;
            this._lastCheckTime = 0f;
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckNewEnemy()
        {
            if ((Time.time - this._lastCheckTime) >= 1f)
            {
                GameObject gameObject = base.stateMachine.gameObject;
                this._lastCheckTime = Time.time;
                ActionDisplay playerEnemy = gameObject.GetComponent<PlayerPVPAiController>().GetPlayerEnemy();
                if (playerEnemy != null)
                {
                    return new CombatUtil.CheckResult { ok = true, enemy = playerEnemy.GoBase };
                }
            }
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckRunBackSpring()
        {
            GameObject gameObject = base.stateMachine.gameObject;
            PlayerPVPAiController component = base.stateMachine.GetComponent<PlayerPVPAiController>();
            if (!component.IsHpEnough())
            {
                Vector3 springPos = component.GetSpringPos();
                Log.AI(gameObject, " MoveToSpring " + springPos);
                CombatUtil.MoveToSpring(base.stateMachine.gameObject, springPos);
                return new CombatUtil.CheckResult { ok = true };
            }
            return CombatUtil.FalseResult;
        }

        public override bool CheckSubState()
        {
            if (!CombatUtil.CanMove(base.stateMachine.gameObject))
            {
                Log.AI(base.stateMachine.gameObject, " Frozen 困住 ");
                return false;
            }
            PlayerAiController component = base.stateMachine.GetComponent<PlayerAiController>();
            return ((component != null) && component.IsAiEnable);
        }

        private CombatUtil.CheckResult EatBuff()
        {
            if ((Time.time - this.lastEatBuff) >= 10f)
            {
                GameObject gameObject = base.stateMachine.gameObject;
                GameObject target = ObjectManager.FindNearestBuff(gameObject);
                if ((target != null) && CombatUtil.CheckPlayerCanSee(gameObject, target))
                {
                    this.lastEatBuff = Time.time;
                    CombatUtil.MoveToBuff(gameObject, target);
                    return new CombatUtil.CheckResult { ok = true, enemy = target };
                }
            }
            return CombatUtil.FalseResult;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase component = base.stateMachine.GetComponent<AiControllerBase>();
            if (component != null)
            {
                component.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }
        }

        private CombatUtil.CheckResult FarawayTower()
        {
            return CombatUtil.FalseResult;
        }

        public override string GetSubName()
        {
            return "_PvpIdle";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__IteratorA0 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__IteratorA0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PvpIdle <>f__this;
            internal GameObject <g>__0;
            internal CombatUtil.CheckResult <ret>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_0234;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    this.<ret>__1 = CombatUtil.FalseResult;
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.CheckInHeal();
                        if (!this.<ret>__1.ok)
                        {
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.CheckRunBackSpring();
                        if (!this.<ret>__1.ok)
                        {
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = CombatUtil.CheckUseHeal(this.<g>__0);
                        if (!this.<ret>__1.ok)
                        {
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.EatBuff();
                        if (!this.<ret>__1.ok)
                        {
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.FarawayTower();
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.CheckNewEnemy();
                        if (this.<ret>__1.ok)
                        {
                            Log.AI(this.<g>__0, " goto New ENemy " + this.<ret>__1.enemy);
                            this.<>f__this.lastEnemy = this.<ret>__1.enemy;
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret>__1.enemy);
                        }
                    }
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = this.<>f__this.CheckLastEnemy();
                        if (this.<ret>__1.ok)
                        {
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret>__1.enemy);
                        }
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_0234:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

