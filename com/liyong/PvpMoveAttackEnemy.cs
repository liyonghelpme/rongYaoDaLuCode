﻿namespace com.liyong
{
    using com.u3d.bases.display;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class PvpMoveAttackEnemy : MoveAttackEnemy
    {
        private float _lastCheckTime;
        private GameObject oldEnemy;

        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if (cmd.cmd[0] == "TryToMove")
            {
                Log.AI(base.stateMachine.name, " TryToMove AI player ");
                float x = Convert.ToSingle(cmd.cmd[1]);
                float y = Convert.ToSingle(cmd.cmd[2]);
                float z = Convert.ToSingle(cmd.cmd[3]);
                int cid = Convert.ToInt32(cmd.cmd[4]);
                MonsterMoveAI component = base.stateMachine.GetComponent<MonsterMoveAI>();
                component.StartCoroutine(component.TryToMove(new Vector3(x, y, z), component.GetCallback(cid), null, null, false, null));
            }
            return false;
        }

        private bool CheckNewEnemy()
        {
            if ((Time.time - this._lastCheckTime) >= 1f)
            {
                this._lastCheckTime = Time.time;
                ActionDisplay display = ObjectManager.FindNearestEnemy(base.stateMachine.gameObject, null);
                if ((display != null) && (this.oldEnemy != null))
                {
                    Log.AI(display.GoBase.name, " newEnemy " + this.oldEnemy.name);
                }
                if ((display != null) && (display.GoBase != this.oldEnemy))
                {
                    this.oldEnemy = display.GoBase;
                    CombatUtil.CheckAttackBeforeMove(base.stateMachine.gameObject, this.oldEnemy);
                    return true;
                }
            }
            return false;
        }

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            if (s == AIState.AIStateEnum.HURT)
            {
                return false;
            }
            return base.CheckNextState(s);
        }

        public override bool CheckSubState()
        {
            if (base.stateMachine.GetLastCommand().cmd.Length > 2)
            {
                return false;
            }
            return true;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(1);
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            this.oldEnemy = ObjectManager.GetController(Convert.ToInt32(lastCommand.cmd[1]));
            if (this.oldEnemy != null)
            {
                Log.AI(base.stateMachine.gameObject.name, " monster oldEnemy " + this.oldEnemy.name);
            }
        }

        public override string GetSubName()
        {
            return "_pvp";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__IteratorA1 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__IteratorA1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PvpMoveAttackEnemy <>f__this;
            internal GameObject <g>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00D0;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    if (CombatUtil.CheckCanAttack(this.<g>__0, this.<>f__this.oldEnemy))
                    {
                        CombatUtil.AttackHim(this.<>f__this.stateMachine.gameObject, this.<>f__this.oldEnemy);
                    }
                    else if (this.<>f__this.CheckNewEnemy())
                    {
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_00D0:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

