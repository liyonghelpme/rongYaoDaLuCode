﻿namespace com.liyong
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class PvpMoveToSpring : MoveAttackEnemy
    {
        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if (cmd.cmd[0] == "TryToMove")
            {
                Log.AI(base.stateMachine.name, " TryToMove AI player ");
                float x = Convert.ToSingle(cmd.cmd[1]);
                float y = Convert.ToSingle(cmd.cmd[2]);
                float z = Convert.ToSingle(cmd.cmd[3]);
                int cid = Convert.ToInt32(cmd.cmd[4]);
                MonsterMoveAI component = base.stateMachine.GetComponent<MonsterMoveAI>();
                component.StartCoroutine(component.TryToMove(new Vector3(x, y, z), component.GetCallback(cid), null, null, false, null));
            }
            return false;
        }

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            if (s == AIState.AIStateEnum.HURT)
            {
                return false;
            }
            return base.CheckNextState(s);
        }

        public override bool CheckSubState()
        {
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            if (lastCommand.cmd.Count<string>() >= 4)
            {
                string str = lastCommand.cmd[3];
                if (str == "spring")
                {
                    return true;
                }
            }
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(1);
        }

        public override string GetSubName()
        {
            return "_ToSpring";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__IteratorA3 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__IteratorA3 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal PvpMoveToSpring <>f__this;
            internal GameObject <g>__0;
            internal CombatUtil.CheckResult <ret>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00AC;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    this.<ret>__1 = CombatUtil.FalseResult;
                    if (!this.<ret>__1.ok)
                    {
                        this.<ret>__1 = CombatUtil.CheckUseHeal(this.<g>__0);
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_00AC:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

