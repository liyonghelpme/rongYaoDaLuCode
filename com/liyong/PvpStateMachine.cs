﻿namespace com.liyong
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public class PvpStateMachine : StateMachine
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map44;
        private CommandHandler handler;

        private void Awake()
        {
            base.AddState(new PvpIdle());
            base.AddState(new StopAiIdle());
            base.AddState(new MeMoveKeyboard());
            base.AddState(new PvpMoveAttackEnemy());
            base.AddState(new PvpMoveToSpring());
            base.AddState(new PvpMoveToBuff());
            base.AddState(new MasterMoveAttackByHand());
            base.AddState(new MasterSkill());
            base.AddState(new PvpSkill());
            base.AddState(new SlaveNormalSkill());
            base.AddState(new PvpDeath());
            base.AddState(new DefaultHurt());
        }

        public override bool CheckCommand()
        {
            if (base.cmds.Count <= 0)
            {
                return false;
            }
            base.lastCommand = base.cmds[0];
            base.cmds.RemoveAt(0);
            Log.AI(base.gameObject.name, " lastCommand " + base.lastCommand.stringCmd);
            string key = base.lastCommand.cmd[0];
            if (key != null)
            {
                int num;
                if (<>f__switch$map44 == null)
                {
                    Dictionary<string, int> dictionary = new Dictionary<string, int>(6);
                    dictionary.Add("move_attack", 0);
                    dictionary.Add("move_keyboard", 1);
                    dictionary.Add("idle", 2);
                    dictionary.Add("skill", 3);
                    dictionary.Add("hurt", 4);
                    dictionary.Add("death", 5);
                    <>f__switch$map44 = dictionary;
                }
                if (<>f__switch$map44.TryGetValue(key, out num))
                {
                    switch (num)
                    {
                        case 0:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_ATTACK);

                        case 1:
                            return base.ChangeState(AIState.AIStateEnum.MOVE_KEYBOARD);

                        case 2:
                            return base.ChangeState(AIState.AIStateEnum.IDLE);

                        case 3:
                            return base.ChangeState(AIState.AIStateEnum.SKILL);

                        case 4:
                            return base.ChangeState(AIState.AIStateEnum.HURT);

                        case 5:
                            return base.ChangeState(AIState.AIStateEnum.DEATH);
                    }
                }
            }
            return base.GetCurState().CheckCommand(base.lastCommand);
        }

        private void Start()
        {
            this.handler = base.GetComponent<CommandHandler>();
            this.handler.AddHandler("move_attack", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("idle", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("skill", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("TryToMove", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("move_keyboard", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("insert_command", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("death", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("death_fly", new CommandHandler.CmdHandler(this.OnCmd));
            this.handler.AddHandler("hurt", new CommandHandler.CmdHandler(this.OnCmd));
            base.ChangeState(AIState.AIStateEnum.IDLE);
        }

        private void Update()
        {
            this.handler.Execute();
        }
    }
}

