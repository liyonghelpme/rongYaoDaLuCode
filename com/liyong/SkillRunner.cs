﻿namespace com.liyong
{
    using System;
    using UnityEngine;
    using UnityLog;

    public class SkillRunner : CommandRunner
    {
        public SkillRunner(CommandHandler.Command cmd, GameObject g) : base(cmd, g)
        {
        }

        public override bool CheckNextCommand(CommandHandler.Command cmd)
        {
            return false;
        }

        public override void RunCommand()
        {
            CommandHandler.Command myCmd = base.myCmd;
            Log.Net(base.owner + " WifiUseSkill");
            uint skillId = Convert.ToUInt32(myCmd.cmd[2]);
            int id = Convert.ToInt32(myCmd.cmd[3]);
            float rotateY = Convert.ToSingle(myCmd.cmd[4]);
            base.myCmd.isRunning = true;
            if (base.handler.CheckCommandCached() > 0)
            {
                myCmd.rate = 2f;
            }
            CombatUtil.WifiSkillHim(base.owner, ObjectManager.GetController(id), skillId, rotateY, myCmd);
        }
    }
}

