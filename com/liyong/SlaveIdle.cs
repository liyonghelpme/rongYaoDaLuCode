﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public sealed class SlaveIdle : IdleState
    {
        private float lastAdjustTime;

        private void AdjustSlavePos(Vector3 md)
        {
            CommandHandler.AddCommandStatic(base.stateMachine.gameObject, string.Format("move_attack {0} {1} adjustPos", -1, -1), 1f);
            int num = base.stateMachine.GetComponent<MonsterMoveAI>().AddCallbackMap(() => CommandHandler.AddCommandStatic(base.stateMachine.gameObject, "idle", 1f));
            Log.AI(base.stateMachine.gameObject, " AdjustSlavePos " + num);
            object[] args = new object[] { md.x, md.y, md.z, num };
            CommandHandler.AddCommandStatic(base.stateMachine.gameObject, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
        }

        private CombatUtil.CheckResult CheckAttackMaster()
        {
            GameObject master = ObjectManager.GetMaster();
            if (master != null)
            {
                UpdateAttribute.LastAttack lastAttackThis = ObjectManager.GetLastAttackThis(master);
                if (((lastAttackThis != null) && (lastAttackThis.lastAttackMe != null)) && ((Time.time - lastAttackThis.lastAttackTime) < 1f))
                {
                    return new CombatUtil.CheckResult { ok = true, enemy = lastAttackThis.lastAttackMe };
                }
            }
            return new CombatUtil.CheckResult();
        }

        private bool CheckInRightMasterZone(GameObject master)
        {
            Vector3 lhs = base.stateMachine.transform.position - master.transform.position;
            lhs.y = 0f;
            float num = (AiConfig.RANGED_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE * 0.001f) * 0.8f;
            Log.AI(base.stateMachine.gameObject, " CheckInRightMasterZone minSpace " + num);
            if ((lhs.sqrMagnitude - (num * num)) < 0f)
            {
                return false;
            }
            Vector3 forward = master.transform.forward;
            forward.y = 0f;
            float num2 = Vector3.Dot(lhs, forward);
            if (ObjectManager.GetJob(base.stateMachine.gameObject) != 2)
            {
                return (num2 < 0f);
            }
            return (num2 > 0f);
        }

        private CombatUtil.CheckResult CheckMasterAttack()
        {
            GameObject master = ObjectManager.GetMaster();
            if ((master != null) && CombatUtil.CheckInBattle(master))
            {
                GameObject masterTarget = ObjectManager.GetMasterTarget();
                if (masterTarget != null)
                {
                    return new CombatUtil.CheckResult { ok = true, enemy = masterTarget };
                }
            }
            return new CombatUtil.CheckResult();
        }

        private CombatUtil.CheckResult CheckNeedAdjustPos(out Vector3 pos)
        {
            if ((Time.time - this.lastAdjustTime) < 1f)
            {
                pos = Vector3.zero;
                return new CombatUtil.CheckResult();
            }
            this.lastAdjustTime = Time.time;
            GameObject gameObject = base.stateMachine.gameObject;
            GameObject master = ObjectManager.GetMaster();
            if (master != null)
            {
                AIState.AIStateEnum type = master.GetComponent<StateMachine>().GetCurState().type;
                if (((type == AIState.AIStateEnum.MOVE_ATTACK) || (type == AIState.AIStateEnum.MOVE_KEYBOARD)) && !this.CheckInRightMasterZone(master))
                {
                    Vector3 moveMasterPos = CombatUtil.GetMoveMasterPos(gameObject, master);
                    if (Util.XZSqrMagnitude(moveMasterPos, base.stateMachine.transform.position) > 2f)
                    {
                        pos = moveMasterPos;
                        base.stateMachine.targetPos = moveMasterPos;
                        return new CombatUtil.CheckResult { ok = true };
                    }
                }
            }
            pos = Vector3.zero;
            return new CombatUtil.CheckResult();
        }

        private CombatUtil.CheckResult CheckSelfAttack()
        {
            GameObject master = ObjectManager.GetMaster();
            if ((master != null) && CombatUtil.CheckInBattle(master))
            {
                return CombatUtil.CheckAttackTarget(base.stateMachine.gameObject);
            }
            return new CombatUtil.CheckResult();
        }

        public override bool CheckSubState()
        {
            Log.AI(base.stateMachine.gameObject, " Frozen 困住 " + base.stateMachine.GetComponent<BaseControler>().GetMeByType<PlayerDisplay>().GetVo().stateInfo.IsFrozen);
            if (!CombatUtil.CanMove(base.stateMachine.gameObject))
            {
                return false;
            }
            PlayerAiController component = base.stateMachine.GetComponent<PlayerAiController>();
            if (component == null)
            {
                return false;
            }
            if (!component.IsAiEnable)
            {
                return false;
            }
            Log.AI(base.stateMachine.gameObject, " isMaster " + component.IsMaster());
            return !component.IsMaster();
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase component = base.stateMachine.GetComponent<AiControllerBase>();
            if (component != null)
            {
                component.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }
        }

        public override string GetSubName()
        {
            return "_slave";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator8B { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator8B : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal SlaveIdle <>f__this;
            internal GameObject <g>__0;
            internal CombatUtil.CheckResult <ret1>__1;
            internal Vector3 <tar>__2;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                    case 2:
                        break;

                    default:
                        goto Label_0378;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    if (!CombatUtil.CanMove(this.<g>__0))
                    {
                        this.$current = null;
                        this.$PC = 1;
                    }
                    else
                    {
                        if (CombatUtil.CheckFarAwayMaster(this.<g>__0))
                        {
                            Log.AI(this.<>f__this.stateMachine.gameObject, " faraway from master");
                            CombatUtil.MoveToMaster(this.<g>__0, ObjectManager.GetMaster());
                        }
                        this.<ret1>__1 = this.<>f__this.CheckSelfAttack();
                        if (this.<ret1>__1.ok)
                        {
                            Log.AI(this.<g>__0, " Move To Self Target");
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                        }
                        if (!this.<ret1>__1.ok)
                        {
                            this.<ret1>__1 = this.<>f__this.CheckNeedAdjustPos(out this.<tar>__2);
                            if (this.<ret1>__1.ok)
                            {
                                this.<>f__this.AdjustSlavePos(this.<tar>__2);
                            }
                        }
                        if (!this.<ret1>__1.ok)
                        {
                            this.<ret1>__1 = this.<>f__this.CheckMasterAttack();
                            if (this.<ret1>__1.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " moveToMasterTarget ");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                            }
                        }
                        if (!this.<ret1>__1.ok)
                        {
                            this.<ret1>__1 = this.<>f__this.CheckAttackMaster();
                            if (this.<ret1>__1.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " MoveToMaster");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                            }
                        }
                        if (!this.<ret1>__1.ok)
                        {
                            this.<ret1>__1 = CombatUtil.CheckAttackSelf(this.<>f__this.stateMachine.gameObject);
                            if (this.<ret1>__1.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " MoveToSelfEnemy");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                            }
                        }
                        if (!this.<ret1>__1.ok)
                        {
                            this.<ret1>__1 = CombatUtil.CheckAttackTeam(this.<>f__this.stateMachine.gameObject);
                            if (this.<ret1>__1.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " MoveToTeam");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                            }
                        }
                        if (!this.<ret1>__1.ok)
                        {
                            this.<ret1>__1 = CombatUtil.CheckTeamAttack(this.<>f__this.stateMachine.gameObject);
                            if (this.<ret1>__1.ok)
                            {
                                Log.AI(this.<>f__this.stateMachine.gameObject, " Attack Who Attack Team Member ");
                                CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                            }
                        }
                        this.$current = null;
                        this.$PC = 2;
                    }
                    return true;
                }
                this.$PC = -1;
            Label_0378:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

