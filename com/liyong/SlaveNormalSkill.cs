﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class SlaveNormalSkill : SkillState
    {
        private GameObject attackTarget;

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            return (((s != AIState.AIStateEnum.HURT) && (s != AIState.AIStateEnum.MOVE_KEYBOARD)) && base.CheckNextState(s));
        }

        public override bool CheckSubState()
        {
            PlayerAiController component = base.stateMachine.GetComponent<PlayerAiController>();
            Log.AI(base.stateMachine.gameObject, " ai is what " + component);
            if (component != null)
            {
                int stateId = Convert.ToInt32(base.stateMachine.GetLastCommand().cmd[1]);
                Log.AI(base.stateMachine.gameObject, string.Concat(new object[] { " Slave NormalSkill ", component.IsMaster(), " stateId ", stateId }));
                if (!CombatUtil.CheckPhysicAttack(stateId))
                {
                    return (!component.IsMaster() || (component.IsMaster() && component.IsAiEnable));
                }
            }
            return false;
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            int nextStatu = Convert.ToInt32(lastCommand.cmd[1]);
            base.stateMachine.GetComponent<MeStatuController>().SetStatu(nextStatu);
            int id = Convert.ToInt32(lastCommand.cmd[2]);
            this.attackTarget = ObjectManager.GetController(id);
        }

        public override string GetSubName()
        {
            return "_Slave_Normal";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator8C { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator8C : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal SlaveNormalSkill <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

