﻿namespace com.liyong
{
    using com.game.module.fight.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.consts;
    using com.u3d.bases.display;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class SlaveSkill : SkillState
    {
        private int _curAniNameHash = Status.NAME_HASH_ATTACK1;
        private float _keyboardPressTime;
        private GameObject attackTarget;

        private bool ChangeAttackTarget()
        {
            GameObject gameObject = base.stateMachine.gameObject;
            ActionDisplay display = ObjectManager.FindNearestEnemy(gameObject, null);
            if (((display != null) && (display.GoBase != null)) && CombatUtil.CheckCanAttack(gameObject, display.GoBase))
            {
                this.attackTarget = display.GoBase;
                return true;
            }
            return false;
        }

        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if ((cmd.cmd[0] == "insert_command") && (cmd.cmd[1] == "move_keyboard"))
            {
                this._keyboardPressTime = Time.time;
            }
            return false;
        }

        private bool CheckMoveStopSkill()
        {
            return ((Time.time - this._keyboardPressTime) < 0.1f);
        }

        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            return (((s != AIState.AIStateEnum.HURT) && (s != AIState.AIStateEnum.MOVE_KEYBOARD)) && base.CheckNextState(s));
        }

        private bool CheckPredictDead()
        {
            return false;
        }

        public override bool CheckSubState()
        {
            PlayerAiController component = base.stateMachine.GetComponent<PlayerAiController>();
            Log.AI(base.stateMachine.gameObject, " ai is what " + component);
            if (component != null)
            {
                int stateId = Convert.ToInt32(base.stateMachine.GetLastCommand().cmd[1]);
                Log.AI(base.stateMachine.gameObject, string.Concat(new object[] { " isMaster ", component.IsMaster(), " attackStateId ", stateId }));
                if (CombatUtil.CheckPhysicAttack(stateId))
                {
                    return (!component.IsMaster() || (component.IsMaster() && component.IsAiEnable));
                }
            }
            return false;
        }

        private bool CheckTargetDead()
        {
            if ((this.attackTarget != null) && !ObjectManager.GetVo(this.attackTarget).IsEmptyHp)
            {
                return false;
            }
            return true;
        }

        private bool CheckTargetFaraway()
        {
            return !CombatUtil.CheckCanAttack(base.stateMachine.gameObject, this.attackTarget);
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            int nextStatu = Convert.ToInt32(lastCommand.cmd[1]);
            base.stateMachine.GetComponent<MeStatuController>().SetStatu(nextStatu);
            int id = Convert.ToInt32(lastCommand.cmd[2]);
            this.attackTarget = ObjectManager.GetController(id);
            this._curAniNameHash = Status.NAME_HASH_ATTACK1;
            this._keyboardPressTime = 0f;
        }

        public override void ExitState()
        {
            base.ExitState();
            base.stateMachine.GetComponent<PlayerAttackController>().ClearAttackList();
        }

        public override string GetSubName()
        {
            return ("_Slave_" + Status.GetName(this._curAniNameHash));
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator8E { <>f__this = this };
        }

        [DebuggerHidden]
        private IEnumerator WaitAttack(Animator animator, int nameHash)
        {
            return new <WaitAttack>c__Iterator8D { nameHash = nameHash, animator = animator, <$>nameHash = nameHash, <$>animator = animator, <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator8E : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal SlaveSkill <>f__this;
            internal Animator <animator>__0;
            internal GameObject <g>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<animator>__0 = ObjectManager.GetPlayerAnimator(this.<>f__this.stateMachine.gameObject);
                        this.<g>__1 = this.<>f__this.stateMachine.gameObject;
                        this.$current = this.<>f__this.stateMachine.StartCoroutine(this.<>f__this.WaitAttack(this.<animator>__0, Status.NAME_HASH_ATTACK1));
                        this.$PC = 1;
                        goto Label_0151;

                    case 1:
                        this.$current = this.<>f__this.stateMachine.StartCoroutine(this.<>f__this.WaitAttack(this.<animator>__0, Status.NAME_HASH_ATTACK2));
                        this.$PC = 2;
                        goto Label_0151;

                    case 2:
                        this.$current = this.<>f__this.stateMachine.StartCoroutine(this.<>f__this.WaitAttack(this.<animator>__0, Status.NAME_HASH_ATTACK3));
                        this.$PC = 3;
                        goto Label_0151;

                    case 3:
                    case 4:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 4;
                            goto Label_0151;
                        }
                        break;

                    default:
                        goto Label_014F;
                }
                this.$PC = -1;
            Label_014F:
                return false;
            Label_0151:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <WaitAttack>c__Iterator8D : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal Animator <$>animator;
            internal int <$>nameHash;
            internal SlaveSkill <>f__this;
            internal ActionVo <attackVo>__2;
            internal GameObject <g>__0;
            internal AnimatorStateInfo <stateInfo>__1;
            internal Animator animator;
            internal int nameHash;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        this.<>f__this._curAniNameHash = this.nameHash;
                        Log.AI(this.<>f__this.stateMachine.gameObject, " WaitAttack animator is " + Status.GetName(this.nameHash));
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_02E4;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    this.<stateInfo>__1 = this.animator.GetCurrentAnimatorStateInfo(0);
                    Log.AI(this.<>f__this.stateMachine.gameObject, string.Concat(new object[] { " stateInfo  ", Status.GetName(this.<stateInfo>__1.nameHash), " time ", this.<stateInfo>__1.normalizedTime, " statusNameHash ", Status.GetName(this.nameHash) }));
                    if (!CombatUtil.CheckFarAwayMaster(this.<g>__0))
                    {
                        if ((this.<stateInfo>__1.normalizedTime <= 0.8f) || (this.<stateInfo>__1.nameHash != this.nameHash))
                        {
                            ObjectManager.RotateToTarget(this.<g>__0, this.<>f__this.attackTarget);
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        if (this.<>f__this.CheckTargetDead())
                        {
                            if (this.<>f__this.ChangeAttackTarget())
                            {
                                Log.AI(this.<g>__0, " AttackTarget Dead Change To Another " + this.<>f__this.attackTarget);
                            }
                            else
                            {
                                Log.AI(this.<g>__0, " TargetDead Not Find New Target");
                                goto Label_02DD;
                            }
                        }
                        if (!this.<>f__this.CheckTargetFaraway())
                        {
                            Log.AI(this.<>f__this.stateMachine.gameObject, " Skill Add Attack current Skill " + Status.GetName(this.<stateInfo>__1.nameHash));
                            if (!this.<>f__this.CheckMoveStopSkill() && !this.<>f__this.CheckPredictDead())
                            {
                                ActionVo vo = new ActionVo {
                                    ActionType = "attack",
                                    IsIgnoreDistance = true,
                                    Target = ObjectManager.GetDisplay(this.<>f__this.attackTarget)
                                };
                                this.<attackVo>__2 = vo;
                                Log.AI(this.<>f__this.stateMachine.gameObject, " AttackContinue " + Status.GetName(this.nameHash));
                                this.<>f__this.stateMachine.GetComponent<AttackControllerBase>().AddAttackList(this.<attackVo>__2, false, true);
                            }
                        }
                    }
                    else
                    {
                        CombatUtil.MoveToMaster(this.<g>__0, ObjectManager.GetMaster());
                    }
                }
            Label_02DD:
                this.$PC = -1;
            Label_02E4:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

