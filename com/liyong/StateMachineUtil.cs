﻿namespace com.liyong
{
    using com.game;
    using com.game.module.WiFiPvP;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;
    using UnityEngine;

    public class StateMachineUtil
    {
        public static bool AddStateMachine(GameObject go, ActionDisplay display)
        {
            if (AppMap.Instance.IsInMainCity)
            {
                if (display is MeDisplay)
                {
                    return go.AddComponent<MeStateMachine>();
                }
                return DoAddDefaultStateMachine(go);
            }
            if (AppMap.Instance.IsInWifiPVP)
            {
                return AddStateMachineInWiFiPVP(go, display);
            }
            if (AppMap.Instance.IsInArena)
            {
                return AddStateMachineInArena(go, display);
            }
            return AddStateMachineInPVE(go, display);
        }

        private static bool AddStateMachineInArena(GameObject go, ActionDisplay display)
        {
            return ((display is PlayerDisplay) && DoAddStateMachine(go, "Arena"));
        }

        private static bool AddStateMachineInPVE(GameObject go, ActionDisplay display)
        {
            string sm = string.Empty;
            if (display is MeDisplay)
            {
                sm = "Me";
            }
            else if (display is MonsterDisplay)
            {
                switch (display.GetMeVoByType<MonsterVo>().MonsterVO.type)
                {
                    case 1:
                        sm = "Monster";
                        break;

                    case 2:
                    case 5:
                        sm = "StaticItem";
                        break;

                    case 3:
                        sm = "Tower";
                        break;

                    case 4:
                        sm = "Private";
                        break;

                    case 6:
                        sm = "SummonMonster";
                        break;
                }
            }
            return DoAddStateMachine(go, sm);
        }

        private static bool AddStateMachineInWiFiPVP(GameObject go, ActionDisplay display)
        {
            if (display.GetMeVoByType<BaseRoleVo>().Camp != 0)
            {
                go.AddMissingComponent<NetworkCommandHandler>();
                go.AddComponent<DefaultStateMachine>();
                return false;
            }
            if (WifiPvpManager.Instance.PvpTemplate == null)
            {
                return false;
            }
            string sm = string.Empty;
            if (display is MeDisplay)
            {
                sm = "Pvp";
            }
            else if (display is MonsterDisplay)
            {
                switch (display.GetMeVoByType<MonsterVo>().MonsterVO.type)
                {
                    case 1:
                        sm = "Monster";
                        break;

                    case 2:
                    case 5:
                        sm = "StaticItem";
                        break;

                    case 3:
                        sm = "Tower";
                        break;

                    case 4:
                        sm = "Private";
                        break;

                    case 6:
                        sm = "SummonMonster";
                        break;
                }
            }
            return DoAddStateMachine(go, sm);
        }

        public static bool DoAddDefaultStateMachine(GameObject go)
        {
            go.AddMissingComponent<DefaultStateMachine>();
            return true;
        }

        private static bool DoAddStateMachine(GameObject go, string sm)
        {
            Component component = null;
            if (!string.IsNullOrEmpty(sm))
            {
                sm = sm + "StateMachine";
                if (go.GetComponent(sm) != null)
                {
                    return false;
                }
                component = go.AddComponent(sm);
            }
            if (component == null)
            {
                DoAddDefaultStateMachine(go);
            }
            return true;
        }
    }
}

