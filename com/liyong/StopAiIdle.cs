﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public class StopAiIdle : IdleState
    {
        public override bool CheckNextState(AIState.AIStateEnum s)
        {
            if ((s != AIState.AIStateEnum.DEATH) && !CombatUtil.CanMove(base.stateMachine.gameObject))
            {
                return false;
            }
            return true;
        }

        public override bool CheckSubState()
        {
            Log.AI(base.stateMachine.gameObject, " Frozen 困住 " + base.stateMachine.GetComponent<BaseControler>().GetMeByType<PlayerDisplay>().GetVo().stateInfo.IsFrozen);
            if (!CombatUtil.CanMove(base.stateMachine.gameObject))
            {
                return true;
            }
            PlayerAiController component = base.stateMachine.GetComponent<PlayerAiController>();
            Log.AI(base.stateMachine.gameObject, " ai is what " + component);
            return ((component != null) && !component.IsAiEnable);
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase aiController = base.stateMachine.GetComponent<SkillController>().MeController.AiController;
            if (aiController != null)
            {
                aiController.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }
        }

        public override string GetSubName()
        {
            return "_stopAI";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__Iterator8F { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__Iterator8F : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal StopAiIdle <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (!this.<>f__this.quit)
                        {
                            if (this.<>f__this.stateMachine.CheckCommand())
                            {
                                break;
                            }
                            this.$current = null;
                            this.$PC = 1;
                            return true;
                        }
                        break;

                    default:
                        goto Label_006A;
                }
                this.$PC = -1;
            Label_006A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

