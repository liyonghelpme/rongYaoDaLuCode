﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class SummonMonsterIdle : IdleState
    {
        private float _lastCheckTime;
        private bool isFirst = true;
        private GameObject lastEnemy;

        private CombatUtil.CheckResult CheckLastEnemy()
        {
            if (!CombatUtil.CheckTargetDead(base.stateMachine.gameObject, this.lastEnemy))
            {
                GameObject gameObject = base.stateMachine.gameObject;
                GameObject summoner = ObjectManager.GetSummoner(gameObject);
                float maxDistanceFromMaster = gameObject.GetComponent<SummonMonsterAiController>().GetMaxDistanceFromMaster();
                if (CombatUtil.CheckTargetFarAwayFromSummoner(summoner, this.lastEnemy, maxDistanceFromMaster))
                {
                    return CombatUtil.FalseResult;
                }
                return new CombatUtil.CheckResult { ok = true, enemy = this.lastEnemy };
            }
            this.lastEnemy = null;
            this._lastCheckTime = 0f;
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckNewEnemy()
        {
            if ((Time.time - this._lastCheckTime) >= 1f)
            {
                GameObject gameObject = base.stateMachine.gameObject;
                this._lastCheckTime = Time.time;
                CombatUtil.CheckResult result = CombatUtil.CheckNewEnemy(gameObject, this.lastEnemy);
                if (result.ok)
                {
                    GameObject summoner = ObjectManager.GetSummoner(gameObject);
                    float maxDistanceFromMaster = gameObject.GetComponent<SummonMonsterAiController>().GetMaxDistanceFromMaster();
                    if (CombatUtil.CheckTargetFarAwayFromSummoner(summoner, result.enemy, maxDistanceFromMaster))
                    {
                        return CombatUtil.FalseResult;
                    }
                    if (CombatUtil.CheckMonsterCanSee(gameObject, result.enemy))
                    {
                        Log.AI(gameObject, " CanSee Mosnter " + result.enemy);
                        return result;
                    }
                    Log.AI(gameObject, " Can'tSee Monster " + result.enemy);
                }
            }
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckSummonerAlive(GameObject g)
        {
            if (!ObjectManager.IsSummorStillAlive(g))
            {
                g.GetComponent<BaseControler>().Me.GetMeVoByType<BaseRoleVo>().ReduceHpToPercent((uint) 0);
                CommandHandler.AddCommandStatic(base.stateMachine.gameObject, "death", 1f);
                return new CombatUtil.CheckResult { ok = true };
            }
            return CombatUtil.FalseResult;
        }

        private CombatUtil.CheckResult CheckSummonerTargetOrAttacker(GameObject g)
        {
            GameObject summoner = ObjectManager.GetSummoner(g);
            if (summoner != null)
            {
                AiControllerBase component = summoner.GetComponent<AiControllerBase>();
                ActionDisplay targetDisplay = component.GetTargetDisplay();
                if (component.IsAiEnable && (targetDisplay != null))
                {
                    float maxDistanceFromMaster = g.GetComponent<SummonMonsterAiController>().GetMaxDistanceFromMaster();
                    if (CombatUtil.CheckTargetFarAwayFromSummoner(summoner, targetDisplay.GoBase, maxDistanceFromMaster))
                    {
                        return CombatUtil.FalseResult;
                    }
                    return new CombatUtil.CheckResult { ok = true, enemy = targetDisplay.GoBase };
                }
                ActionDisplay lastAttackerOrEnemy = component.GetLastAttackerOrEnemy();
                if (lastAttackerOrEnemy != null)
                {
                    return new CombatUtil.CheckResult { ok = true, enemy = lastAttackerOrEnemy.GoBase };
                }
            }
            return CombatUtil.FalseResult;
        }

        [DebuggerHidden]
        private IEnumerator DoBirth()
        {
            return new <DoBirth>c__IteratorA9 { <>f__this = this };
        }

        private void DoCheckAttackOrMoveTo(GameObject g, GameObject enemy)
        {
            if (CombatUtil.CheckCanAttack(g, enemy))
            {
                CombatUtil.AttackHim(g, enemy);
            }
            else
            {
                CombatUtil.MoveToHim(g, enemy);
            }
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(0);
            base.stateMachine.GetComponent<MonsterMoveAI>().StopByStateMachine();
            AiControllerBase component = base.stateMachine.GetComponent<AiControllerBase>();
            if (component != null)
            {
                component.SetAiStatusButNotClearTarget(AiEnum.SummonAiStatus.THINK);
            }
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__IteratorA8 { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <DoBirth>c__IteratorA9 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal SummonMonsterIdle <>f__this;
            internal StatuControllerBase <stc>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<stc>__0 = this.<>f__this.stateMachine.GetComponent<StatuControllerBase>();
                        if (this.<stc>__0 == null)
                        {
                            break;
                        }
                        this.<stc>__0.SetStatu(2);
                        UnityEngine.Debug.Log("--------------- 切动作! 清清的操作请改这里");
                        this.$current = new WaitForSeconds(3f);
                        this.$PC = 1;
                        goto Label_009A;

                    case 1:
                        break;

                    case 2:
                        this.$PC = -1;
                        goto Label_0098;

                    default:
                        goto Label_0098;
                }
                this.$current = null;
                this.$PC = 2;
                goto Label_009A;
            Label_0098:
                return false;
            Label_009A:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__IteratorA8 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal SummonMonsterIdle <>f__this;
            internal GameObject <g>__0;
            internal CombatUtil.CheckResult <ret1>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        goto Label_02BA;

                    case 1:
                        this.<>f__this.isFirst = false;
                        UnityEngine.Debug.Log("已结束出生动作");
                        this.$current = null;
                        this.$PC = 2;
                        goto Label_02D3;

                    case 2:
                        break;

                    case 3:
                    case 4:
                        goto Label_02BA;

                    default:
                        goto Label_02D1;
                }
            Label_00AE:
                if (this.<>f__this.stateMachine.CheckCommand())
                {
                    goto Label_02CA;
                }
                if (!CombatUtil.CanMove(this.<g>__0))
                {
                    this.$current = null;
                    this.$PC = 3;
                }
                else
                {
                    this.<ret1>__1 = CombatUtil.FalseResult;
                    if (!this.<ret1>__1.ok)
                    {
                        this.<ret1>__1 = this.<>f__this.CheckSummonerAlive(this.<g>__0);
                    }
                    if (!this.<ret1>__1.ok && CombatUtil.CheckFarAwayFromSummoner(this.<g>__0))
                    {
                        Log.AI(this.<>f__this.stateMachine.gameObject, " faraway from summoner");
                        CombatUtil.MoveToSummoner(this.<g>__0, ObjectManager.GetSummoner(this.<g>__0));
                        CombatUtil.CheckResult result = new CombatUtil.CheckResult {
                            ok = true
                        };
                        this.<ret1>__1 = result;
                    }
                    if (!this.<ret1>__1.ok)
                    {
                        this.<ret1>__1 = this.<>f__this.CheckNewEnemy();
                        if (this.<ret1>__1.ok)
                        {
                            this.<>f__this.lastEnemy = this.<ret1>__1.enemy;
                            Log.AI(this.<g>__0, " MoveToAttack Enemy ");
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                        }
                    }
                    if (!this.<ret1>__1.ok)
                    {
                        this.<ret1>__1 = this.<>f__this.CheckLastEnemy();
                        if (this.<ret1>__1.ok)
                        {
                            CombatUtil.CheckAttackBeforeMove(this.<g>__0, this.<ret1>__1.enemy);
                        }
                    }
                    if (!this.<ret1>__1.ok)
                    {
                        this.<ret1>__1 = this.<>f__this.CheckSummonerTargetOrAttacker(this.<g>__0);
                        if (this.<ret1>__1.ok)
                        {
                            Log.AI(this.<>f__this.stateMachine.gameObject, " MoveToMaster");
                            this.<>f__this.DoCheckAttackOrMoveTo(this.<g>__0, this.<ret1>__1.enemy);
                        }
                    }
                    this.$current = null;
                    this.$PC = 4;
                }
                goto Label_02D3;
            Label_02BA:
                if (!this.<>f__this.quit)
                {
                    if (this.<>f__this.isFirst)
                    {
                        this.$current = this.<>f__this.stateMachine.StartCoroutine(this.<>f__this.DoBirth());
                        this.$PC = 1;
                        goto Label_02D3;
                    }
                    goto Label_00AE;
                }
            Label_02CA:
                this.$PC = -1;
            Label_02D1:
                return false;
            Label_02D3:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

