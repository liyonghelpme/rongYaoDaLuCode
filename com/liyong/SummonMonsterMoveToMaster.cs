﻿namespace com.liyong
{
    using com.u3d.bases.ai;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class SummonMonsterMoveToMaster : MoveAttackEnemy
    {
        private GameObject master;

        public override bool CheckCommand(CommandHandler.Command cmd)
        {
            if (cmd.cmd[0] == "TryToMove")
            {
                Log.AI(base.stateMachine.name, " TryToMove AI player ");
                float x = Convert.ToSingle(cmd.cmd[1]);
                float y = Convert.ToSingle(cmd.cmd[2]);
                float z = Convert.ToSingle(cmd.cmd[3]);
                int cid = Convert.ToInt32(cmd.cmd[4]);
                MonsterMoveAI component = base.stateMachine.GetComponent<MonsterMoveAI>();
                component.StartCoroutine(component.TryToMove(new Vector3(x, y, z), component.GetCallback(cid), null, null, false, null));
            }
            return false;
        }

        private bool CheckNearMaster()
        {
            if (this.master != null)
            {
                float num = Util.XZSqrMagnitude(base.stateMachine.transform.position, this.master.transform.position);
                float val = AiConfig.NEAR_THE_MASTER_GAP * 0.001f;
                return (num < val.Square());
            }
            return true;
        }

        public override bool CheckSubState()
        {
            CommandHandler.Command lastCommand = base.stateMachine.GetLastCommand();
            if (lastCommand.cmd.Count<string>() > 3)
            {
                return false;
            }
            return ((lastCommand.cmd.Count<string>() > 2) && (Convert.ToInt32(lastCommand.cmd[2]) != -1));
        }

        public override void EnterState()
        {
            base.EnterState();
            base.stateMachine.GetComponent<StatuControllerBase>().SetStatu(1);
            this.master = ObjectManager.GetSummoner(base.stateMachine.gameObject);
        }

        public override string GetSubName()
        {
            return "_ToMaster";
        }

        [DebuggerHidden]
        public override IEnumerator RunLogic()
        {
            return new <RunLogic>c__IteratorAB { <>f__this = this };
        }

        [CompilerGenerated]
        private sealed class <RunLogic>c__IteratorAB : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal SummonMonsterMoveToMaster <>f__this;
            internal GameObject <g>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<g>__0 = this.<>f__this.stateMachine.gameObject;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00B5;
                }
                if (!this.<>f__this.quit && !this.<>f__this.stateMachine.CheckCommand())
                {
                    if (this.<>f__this.CheckNearMaster())
                    {
                        Log.AI(this.<g>__0, " NearMaster Now Stop");
                        CommandHandler.AddCommandStatic(this.<g>__0, "idle", 1f);
                    }
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_00B5:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

