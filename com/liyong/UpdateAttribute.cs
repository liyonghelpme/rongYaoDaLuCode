﻿namespace com.liyong
{
    using com.game;
    using com.game.module.WiFiPvP;
    using com.game.vo;
    using com.u3d.bases.ai;
    using System;
    using UnityEngine;
    using UnityLog;

    public class UpdateAttribute : MonoBehaviour
    {
        private BaseRoleVo _vo;
        private GameObject lastAttackMe;
        private float lastAttackTime;

        public LastAttack GetLastAttackMe()
        {
            Log.AI(base.gameObject, " GetLastAttackMe " + this.lastAttackMe);
            return new LastAttack { lastAttackMe = this.lastAttackMe, lastAttackTime = this.lastAttackTime };
        }

        private BaseRoleVo GetMyVo()
        {
            if (this._vo == null)
            {
                this._vo = ObjectManager.GetDisplay(base.gameObject).GetMeVoByType<BaseRoleVo>();
            }
            return this._vo;
        }

        private void OnGUI()
        {
            if (Main.mainObj.TestSoul)
            {
                PlayerAiController component = base.gameObject.GetComponent<PlayerAiController>();
                if ((component != null) && component.IsMaster())
                {
                    Vector3 vector = Camera.main.WorldToScreenPoint(base.transform.position + new Vector3(0f, 0f, 0f));
                    GUI.Label(new Rect(vector.x - 50f, (Screen.height - vector.y) - 80f, 100f, 100f), string.Concat(new object[] { "MySoul ", MeVo.instance.soul, "\nenemy Soul:", WifiPvpManager.Instance.EnemySoul }));
                }
            }
            if (Main.mainObj.TestAttribute)
            {
                GUI.skin = Main.mainObj.DebugSkin;
                Vector3 vector2 = Camera.main.WorldToScreenPoint(base.transform.position + new Vector3(0f, 0f, 0f));
                string str2 = string.Empty;
                object[] objArray2 = new object[] { str2, "HP:", this.GetMyVo().Hp, "\n" };
                str2 = string.Concat(objArray2);
                object[] objArray3 = new object[] { str2, "Phy:", this.GetMyVo().AttrPhy, "\n" };
                str2 = string.Concat(objArray3);
                object[] objArray4 = new object[] { str2, "Mag:", this.GetMyVo().AttrMag, "\n" };
                string text = string.Concat(objArray4);
                GUI.TextField(new Rect(vector2.x - 50f, (Screen.height - vector2.y) - 80f, 100f, 100f), text);
            }
        }

        private void OnUpdateHP(CommandHandler.Command cmd)
        {
            Log.AI(base.gameObject, " sethp " + cmd.stringCmd);
            CommandHandler.AddCommandStaticImmediately(base.GetComponent<SkillController>().MeController.GoHp, cmd.stringCmd);
        }

        public void SetLastAttackMe(GameObject g)
        {
            Log.AI(base.gameObject, " SetLastAttackMe " + g);
            this.lastAttackMe = g;
            this.lastAttackTime = Time.time;
        }

        private void Start()
        {
            CommandHandler component = base.GetComponent<CommandHandler>();
            Log.AI(base.gameObject, " UpdateAttribute CommandHandler " + component);
            component.AddHandler("updatehp", new CommandHandler.CmdHandler(this.OnUpdateHP));
        }

        public class LastAttack
        {
            public GameObject lastAttackMe;
            public float lastAttackTime;
        }
    }
}

