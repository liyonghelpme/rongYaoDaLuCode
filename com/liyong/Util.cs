﻿namespace com.liyong
{
    using Pathfinding;
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class Util
    {
        public static bool CheckMovable(Vector3 point)
        {
            if ((AstarPath.active != null) && (AstarPath.active.graphs.Length > 0))
            {
                RecastGraph graph = AstarPath.active.graphs[0] as RecastGraph;
                NNInfo nearest = graph.GetNearest(point);
                TriangleMeshNode node = nearest.node as TriangleMeshNode;
                return (node.Walkable && graph.ContainsPoint(nearest.node as TriangleMeshNode, point));
            }
            Debug.Log("not find node to check");
            return false;
        }

        public static Transform FindChildRecursive(Transform t, string name)
        {
            if (t.name == name)
            {
                return t;
            }
            Transform transform = null;
            IEnumerator enumerator = t.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    Transform current = (Transform) enumerator.Current;
                    if (current.name == name)
                    {
                        return current;
                    }
                    transform = FindChildRecursive(current, name);
                    if (transform != null)
                    {
                        return transform;
                    }
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable == null)
                {
                }
                disposable.Dispose();
            }
            return null;
        }

        public static float GetMapHeight(Vector3 point)
        {
            if ((AstarPath.active != null) && (AstarPath.active.graphs.Length > 0))
            {
                RecastGraph graph = AstarPath.active.graphs[0] as RecastGraph;
                point = (graph.GetNearest(point).node as TriangleMeshNode).ClosestPointOnNode(point);
                return point.y;
            }
            return 5f;
        }

        public static bool GetPointMovable(ref Vector3 point)
        {
            if ((AstarPath.active != null) && (AstarPath.active.graphs.Length > 0))
            {
                point.y = 0f;
                RecastGraph graph = AstarPath.active.graphs[0] as RecastGraph;
                point = (graph.GetNearest(point).node as TriangleMeshNode).ClosestPointOnNodeXZ(point);
                return true;
            }
            Debug.LogError("Not Find NavMesh Graph To Check Movable");
            return false;
        }

        public static void InitGameObject(GameObject g)
        {
            g.transform.localPosition = Vector3.zero;
            g.transform.localScale = Vector3.one;
            g.transform.localRotation = Quaternion.identity;
        }

        public void Log(string msg)
        {
        }

        public static float XZSqrMagnitude(GameObject dis, StateMachine sm)
        {
            return XZSqrMagnitude(dis.transform.position, sm.transform.position);
        }

        public static float XZSqrMagnitude(GameObject go1, GameObject go2)
        {
            return XZSqrMagnitude(go1.transform.position, go2.transform.position);
        }

        public static float XZSqrMagnitude(Transform t, StateMachine sm)
        {
            return XZSqrMagnitude(t.position, sm.transform.position);
        }

        public static float XZSqrMagnitude(Transform a, Transform b)
        {
            return XZSqrMagnitude(a.position, b.position);
        }

        public static float XZSqrMagnitude(Transform a, Vector3 b)
        {
            return XZSqrMagnitude(a.position, b);
        }

        public static float XZSqrMagnitude(Vector3 a, Transform b)
        {
            return XZSqrMagnitude(a, b.position);
        }

        public static float XZSqrMagnitude(Vector3 a, Vector3 b)
        {
            float num = b.x - a.x;
            float num2 = b.z - a.z;
            return ((num * num) + (num2 * num2));
        }

        public class Int2
        {
            public int x;
            public int y;
        }

        public delegate void VoidDelegate();
    }
}

