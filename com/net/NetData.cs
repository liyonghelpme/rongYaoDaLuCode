﻿namespace com.net
{
    using com.game;
    using com.net.interfaces;
    using Proto;
    using System;
    using System.IO;

    public class NetData : INetData
    {
        private byte[] bodyBuffer;

        public NetData(byte[] data)
        {
            this.bodyBuffer = data;
        }

        public static byte[] Encode(MemoryStream msdata, byte framNum, byte wayNum)
        {
            byte[] buffer = new byte[2];
            byte[] buffer2 = new byte[1];
            byte[] buffer3 = new byte[2];
            buffer2[0] = framNum;
            byte[] buffer4 = new byte[] { wayNum };
            byte[] buffer5 = msdata.ToArray();
            buffer = ByteUtil.Number2Bytes(4 + buffer5.Length, 2);
            int src = 0;
            if (AppNet.uniq > 0xffff)
            {
                AppNet.uniq = 0x63;
            }
            src = AppNet.uniq + 1;
            AppNet.uniq = src;
            buffer3 = ByteUtil.Number2Bytes(src, 2);
            byte[] array = new byte[(((buffer.Length + buffer2.Length) + buffer4.Length) + buffer5.Length) + buffer3.Length];
            buffer.CopyTo(array, 0);
            buffer3.CopyTo(array, buffer.Length);
            buffer2.CopyTo(array, (int) (buffer.Length + buffer3.Length));
            buffer4.CopyTo(array, (int) ((buffer.Length + buffer3.Length) + buffer2.Length));
            buffer5.CopyTo(array, (int) (((buffer.Length + buffer3.Length) + buffer2.Length) + buffer4.Length));
            return array;
        }

        public string GetCMD()
        {
            byte[] destinationArray = new byte[1];
            Array.Copy(this.bodyBuffer, 0, destinationArray, 0, destinationArray.Length);
            byte[] buffer2 = new byte[1];
            Array.Copy(this.bodyBuffer, destinationArray.Length, buffer2, 0, buffer2.Length);
            int num = 0;
            num += (destinationArray[0] * 0x100) + buffer2[0];
            return num.ToString();
        }

        public MemoryStream GetMemoryStream()
        {
            byte[] destinationArray = new byte[this.bodyBuffer.Length - 2];
            Array.Copy(this.bodyBuffer, 2, destinationArray, 0, destinationArray.Length);
            MemoryStream stream = new MemoryStream();
            stream.Write(destinationArray, 0, destinationArray.Length);
            stream.Position = 0L;
            return stream;
        }
    }
}

