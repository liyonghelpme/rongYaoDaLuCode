﻿namespace com.net.interfaces
{
    using System;
    using System.IO;

    public interface INetData
    {
        string GetCMD();
        MemoryStream GetMemoryStream();
    }
}

