﻿namespace com.net.interfaces
{
    using System;
    using System.Text;

    public interface IP8583Msg
    {
        void clear();
        void clear(short index);
        void dispose();
        byte[] getBitmap();
        string getCMD();
        byte[] getField(int index);
        int getFieldInt(int index);
        string getFieldTrim(int index);
        string getHexBitmap();
        string getHexMac();
        int getMacIndex();
        string getRespCode();
        bool isExisit(int index);
        void setBitmap(byte[] bytes);
        void setCMD(string str);
        void setField(int index, int src);
        void setField(int index, string str);
        void setField(int index, StringBuilder builder);
        void setField(int index, byte[] bytes);
        void setMac(string str);
        void setMac(byte[] bytes);
        void setRespCode(string str);
    }
}

