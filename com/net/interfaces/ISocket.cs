﻿namespace com.net.interfaces
{
    using com.net;
    using System;
    using System.IO;

    public interface ISocket
    {
        void close();
        void connect(string ip, int port);
        bool connected();
        int getTryableNum();
        void msgListenner(NetMsgCallback callback);
        void retryConnect();
        void send(MemoryStream msdata, byte framNum, byte wayNum);
        void setTryableNum(int num);
        void statusListener(NetStatusCallback listener);
        void Update();

        string ip { get; set; }

        int port { get; set; }
    }
}

