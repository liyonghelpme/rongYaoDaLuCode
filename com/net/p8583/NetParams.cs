﻿namespace com.net.p8583
{
    using com.net.debug;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class NetParams
    {
        private static int[] bitList = new int[] { 0x20, 0x40, 0x80 };
        internal static short bitmapLen = 0x10;
        public const char BLANK = ' ';
        internal static string encode = "utf-8";
        public const string HEART_MSG = "0000";
        public const string LINK_FAIL = "linkFail";
        public const string LINK_OK = "linkOk";
        public const string LINKING = "linking";
        public const short P8583_4 = 4;
        public const short P8583_8 = 8;
        internal static Dictionary<short, P8583Tep> tepMap = new Dictionary<short, P8583Tep>();
        public const char ZERO = '0';

        public static void addLevel(int level)
        {
            NetLog.addLevel(level);
        }

        public static void addLevel(string format)
        {
            NetLog.addLevel(format);
        }

        public static void initNet(int bitLen = 0x80, string charset = "utf-8")
        {
            bool flag = false;
            foreach (int num2 in bitList)
            {
                if (num2 == bitLen)
                {
                    flag = true;
                    break;
                }
            }
            if (!flag)
            {
                NetLog.info("NetParams-initNet()", "指定的位元：" + bitLen + "值不是32、64、128其中的一个！");
            }
            else
            {
                bitmapLen = (short) (bitLen / 8);
                NetLog.info("NetParams-initNet()", "bitmapLen:" + bitmapLen);
                if ((charset != null) && (charset.Trim().Length > 0))
                {
                    encode = charset;
                }
            }
        }

        public static void printWay(bool isConsle)
        {
            NetLog.printWay = isConsle;
        }

        public static void setDecoderTrace(bool isTrace)
        {
            P8583Parser.instance.isDecoderTrace = isTrace;
        }

        public static void setEncoderTrace(bool isTrace)
        {
            P8583Parser.instance.isEncoderTrace = isTrace;
        }
    }
}

