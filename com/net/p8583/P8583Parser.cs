﻿namespace com.net.p8583
{
    using com.net.debug;
    using com.net.interfaces;
    using com.net.p8583.utils;
    using com.net.p8583.vo;
    using System;
    using System.Runtime.InteropServices;

    internal class P8583Parser
    {
        internal static P8583Parser instance = new P8583Parser();
        internal bool isDecoderTrace = true;
        internal bool isEncoderTrace = true;

        public byte[] createBitmap(IP8583Msg p8583Msg)
        {
            int num = p8583Msg.getMacIndex();
            byte[] bitmap = new byte[NetParams.bitmapLen];
            for (short i = 1; i < num; i = (short) (i + 1))
            {
                if ((p8583Msg.getField(i) != null) && (p8583Msg.getField(i).Length > 0))
                {
                    BitUtils.generateBitmap(bitmap, i);
                }
            }
            return bitmap;
        }

        public IP8583Msg decoder(byte[] bytes)
        {
            int sourceIndex = 0;
            byte[] destinationArray = new byte[NetParams.bitmapLen];
            P8583Msg msg = new P8583Msg();
            Array.Copy(bytes, 6, destinationArray, 0, destinationArray.Length);
            msg.setBitmap(destinationArray);
            if (this.isDecoderTrace)
            {
                BitUtils.println(destinationArray);
            }
            for (short i = 0; i < NetParams.bitmapLen; i = (short) (i + 1))
            {
                for (short j = 0; j < 8; j = (short) (j + 1))
                {
                    if (((destinationArray[i] >> (7 - j)) & 1) == 1)
                    {
                        byte[] buffer2;
                        short index = (short) (((i * 8) + j) + 1);
                        P8583Tep tep = NetParams.tepMap[index];
                        if (tep.maxLen > 0)
                        {
                            buffer2 = new byte[tep.maxLen];
                            Array.Copy(bytes, sourceIndex, buffer2, 0, tep.maxLen);
                            sourceIndex += tep.maxLen;
                            if (index == 1)
                            {
                                sourceIndex += NetParams.bitmapLen;
                            }
                            if (this.isDecoderTrace)
                            {
                                NetLog.info(this, string.Concat(new object[] { "-decoder() no=", index, ",maxLen=", tep.maxLen, ",msg=", MiscUtils.bytes2String(buffer2) }));
                            }
                        }
                        else
                        {
                            byte[] buffer3 = new byte[tep.lenFlag];
                            Array.Copy(bytes, sourceIndex, buffer3, 0, tep.lenFlag);
                            sourceIndex += tep.lenFlag;
                            buffer2 = new byte[MiscUtils.bytes2Int(buffer3)];
                            Array.Copy(bytes, sourceIndex, buffer2, 0, buffer2.Length);
                            sourceIndex += buffer2.Length;
                            if (this.isDecoderTrace)
                            {
                                NetLog.info(this, string.Concat(new object[] { "-decoder() no=", index, ",lenFlag=", tep.lenFlag, ",msg=", MiscUtils.bytes2String(buffer2) }));
                            }
                        }
                        msg.setField(index, buffer2);
                    }
                }
            }
            return msg;
        }

        public byte[] encoder(IP8583Msg p8583Msg, bool isMac = false)
        {
            int num = p8583Msg.getMacIndex();
            p8583Msg.setBitmap(this.createBitmap(p8583Msg));
            if (isMac)
            {
                BitUtils.generateBitmap(p8583Msg.getBitmap(), (short) num);
            }
            byte[] sourceArray = new byte[0];
            byte[] bytes = p8583Msg.getBitmap();
            if (this.isEncoderTrace)
            {
                BitUtils.println(bytes);
            }
            for (short i = 0; i < NetParams.bitmapLen; i = (short) (i + 1))
            {
                for (short j = 0; j < 8; j = (short) (j + 1))
                {
                    if (((bytes[i] >> (7 - j)) & 1) == 1)
                    {
                        short index = (short) (((i * 8) + j) + 1);
                        if (index < num)
                        {
                            byte[] buffer3;
                            P8583Tep tep = NetParams.tepMap[index];
                            if (tep.maxLen > 0)
                            {
                                byte[] buffer4 = MiscUtils.fill(p8583Msg.getField(index), tep.maxLen, ' ');
                                buffer3 = new byte[(sourceArray.Length + tep.maxLen) + ((index != 1) ? 0 : NetParams.bitmapLen)];
                                Array.Copy(sourceArray, 0, buffer3, 0, sourceArray.Length);
                                Array.Copy(buffer4, 0, buffer3, sourceArray.Length, tep.maxLen);
                                if (index == 1)
                                {
                                    Array.Copy(bytes, 0, buffer3, sourceArray.Length + tep.maxLen, bytes.Length);
                                }
                                if (this.isEncoderTrace)
                                {
                                    NetLog.info(this, string.Concat(new object[] { "-encoder() no=", index, ",maxLen=", tep.maxLen, ",msg=", MiscUtils.bytes2String(buffer4) }));
                                }
                            }
                            else
                            {
                                int length = p8583Msg.getField(index).Length;
                                byte[] buffer5 = MiscUtils.number2Bytes(length, tep.lenFlag);
                                buffer3 = new byte[(sourceArray.Length + buffer5.Length) + length];
                                Array.Copy(sourceArray, 0, buffer3, 0, sourceArray.Length);
                                Array.Copy(buffer5, 0, buffer3, sourceArray.Length, buffer5.Length);
                                Array.Copy(p8583Msg.getField(index), 0, buffer3, sourceArray.Length + buffer5.Length, length);
                                if (this.isEncoderTrace)
                                {
                                    NetLog.info(this, string.Concat(new object[] { "-encoder() no=", index, ",lenFlag=", tep.lenFlag, ",msg len=", MiscUtils.bytes2Int(buffer5), ",msg=", MiscUtils.bytes2String(p8583Msg.getField(index)) }));
                                }
                            }
                            sourceArray = buffer3;
                        }
                    }
                }
            }
            if (isMac)
            {
                byte[] buffer7 = MiscUtils.string2Bytes("CMAC_000");
                byte[] destinationArray = new byte[sourceArray.Length + buffer7.Length];
                Array.Copy(sourceArray, 0, destinationArray, 0, sourceArray.Length);
                Array.Copy(buffer7, 0, destinationArray, sourceArray.Length, buffer7.Length);
                return destinationArray;
            }
            return sourceArray;
        }
    }
}

