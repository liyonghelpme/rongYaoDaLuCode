﻿namespace com.net.p8583.utils
{
    using com.net.p8583;
    using System;
    using System.Text;

    public class BitUtils
    {
        public static byte[] convert(short[] indexs)
        {
            byte[] buffer = new byte[NetParams.bitmapLen];
            for (short i = 0; i < indexs.Length; i = (short) (i + 1))
            {
                short num2 = indexs[i];
                short index = (short) ((num2 - 1) / 8);
                short num4 = (short) (7 - ((num2 - 1) % 8));
                buffer[index] = (byte) (buffer[index] | (((int) 1) << num4));
            }
            return buffer;
        }

        public static void generateBitmap(byte[] bitmap, short index)
        {
            short num = (short) ((index - 1) / 8);
            short num2 = (short) (7 - ((index - 1) % 8));
            bitmap[num] = (byte) (bitmap[num] | (((int) 1) << num2));
        }

        public static bool isExisit(byte[] b, short from, short index)
        {
            short num = (short) (((index - 1) / 8) + from);
            short num2 = (short) (7 - ((index - 1) % 8));
            return (((b[num] >> num2) & 1) == 1);
        }

        public static void println(byte[] bytes)
        {
            if ((bytes != null) && (bytes.Length == NetParams.bitmapLen))
            {
                StringBuilder builder = new StringBuilder();
                for (short i = 0; i < bytes.Length; i = (short) (i + 1))
                {
                    for (short j = 0; j < 8; j = (short) (j + 1))
                    {
                        if (((bytes[i] >> (7 - j)) & 1) == 1)
                        {
                            int num3 = ((i * 8) + j) + 1;
                            builder.Append((builder.Length <= 0) ? (num3) : ("," + num3));
                        }
                    }
                }
                if (builder.Length > 0)
                {
                    Console.WriteLine(builder);
                }
            }
        }
    }
}

