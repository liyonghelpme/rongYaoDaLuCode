﻿namespace com.net.p8583.utils
{
    using com.net.p8583;
    using System;
    using System.Text;

    public class MiscUtils
    {
        private const int P256 = 0x100;

        public static int bytes2Int(byte[] bytes)
        {
            int num = 0;
            for (short i = 0; i < bytes.Length; i = (short) (i + 1))
            {
                int num3 = (int) Math.Pow(256.0, (double) i);
                num += (bytes[i] & 0xff) * num3;
            }
            return num;
        }

        public static string bytes2String(byte[] bytes)
        {
            try
            {
                string str2 = Encoding.GetEncoding(NetParams.encode).GetString(bytes);
                char[] trimChars = new char[1];
                return str2.TrimEnd(trimChars);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                return null;
            }
        }

        public static byte[] fill(byte[] src, int len, char fillChar)
        {
            if (src.Length == len)
            {
                return src;
            }
            if (src.Length > len)
            {
                byte[] buffer2 = new byte[len];
                Array.Copy(src, 0, buffer2, 0, len);
                return buffer2;
            }
            int num = len - src.Length;
            StringBuilder builder = new StringBuilder();
            for (short i = 0; i < num; i = (short) (i + 1))
            {
                builder.Append(fillChar);
            }
            byte[] destinationArray = new byte[len];
            byte[] sourceArray = string2Bytes(builder.ToString());
            Array.Copy(src, 0, destinationArray, 0, src.Length);
            Array.Copy(sourceArray, 0, destinationArray, src.Length, sourceArray.Length);
            return destinationArray;
        }

        public static byte[] number2Bytes(int src, int len)
        {
            byte[] buffer = new byte[len];
            for (short i = 0; i < len; i = (short) (i + 1))
            {
                int num2 = src % 0x100;
                buffer[i] = (byte) (num2 & 0xff);
                src /= 0x100;
            }
            return buffer;
        }

        public static byte[] string2Bytes(string str)
        {
            return Encoding.GetEncoding(NetParams.encode).GetBytes(str);
        }
    }
}

