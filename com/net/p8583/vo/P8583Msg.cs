﻿namespace com.net.p8583.vo
{
    using com.net.interfaces;
    using com.net.p8583.utils;
    using System;
    using System.Text;

    internal class P8583Msg : IP8583Msg
    {
        private byte[][] field;
        private int len = ((NetParams.bitmapLen * 8) + 1);

        public P8583Msg()
        {
            this.field = new byte[this.len][];
            for (short i = 0; i < this.field.Length; i = (short) (i + 1))
            {
                this.field[i] = new byte[0];
            }
        }

        public void clear()
        {
            for (short i = 0; i < this.field.Length; i = (short) (i + 1))
            {
                this.clear(i);
            }
        }

        public void clear(short index)
        {
            if (this.isExisit(index))
            {
                this.field[index] = new byte[0];
            }
        }

        public void dispose()
        {
            this.len = 0;
            for (short i = 0; i < this.field.Length; i = (short) (i + 1))
            {
                this.field[i] = null;
            }
            this.field = null;
        }

        public byte[] getBitmap()
        {
            return this.field[0];
        }

        public string getCMD()
        {
            return this.getFieldTrim(1);
        }

        public byte[] getField(int index)
        {
            if (this.isExisit(index))
            {
                return this.field[index];
            }
            return new byte[0];
        }

        public int getFieldInt(int index)
        {
            return int.Parse(this.getFieldTrim(index));
        }

        public string getFieldTrim(int index)
        {
            if (this.isExisit(index))
            {
                string str2 = MiscUtils.bytes2String(this.field[index]);
                return ((str2 == null) ? string.Empty : str2.Trim());
            }
            return string.Empty;
        }

        public string getHexBitmap()
        {
            return HexUtils.byteToHexStr(this.getBitmap());
        }

        public string getHexMac()
        {
            return HexUtils.byteToHexStr(this.getField(this.getMacIndex()));
        }

        public int getMacIndex()
        {
            return (this.len - 1);
        }

        public string getRespCode()
        {
            return this.getFieldTrim(2);
        }

        public bool isExisit(int index)
        {
            return ((index >= 0) && (index < this.field.Length));
        }

        public void setBitmap(byte[] bytes)
        {
            if (bytes != null)
            {
                this.field[0] = bytes;
            }
        }

        public void setCMD(string str)
        {
            this.setField(1, str);
        }

        public void setField(int index, int src)
        {
            this.setField(index, src.ToString());
        }

        public void setField(int index, string str)
        {
            if (this.isExisit(index) && (str != null))
            {
                this.field[index] = MiscUtils.string2Bytes(str);
            }
        }

        public void setField(int index, StringBuilder builder)
        {
            if (builder != null)
            {
                this.setField(index, builder.ToString());
            }
        }

        public void setField(int index, byte[] bytes)
        {
            if (this.isExisit(index) && (bytes != null))
            {
                this.field[index] = bytes;
            }
        }

        public void setMac(string str)
        {
            this.setField(this.getMacIndex(), str);
        }

        public void setMac(byte[] bytes)
        {
            if (bytes != null)
            {
                this.field[this.getMacIndex()] = bytes;
            }
        }

        public void setRespCode(string str)
        {
            this.setField(2, str);
        }
    }
}

