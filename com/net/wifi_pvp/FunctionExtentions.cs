﻿namespace com.net.wifi_pvp
{
    using com.game.module.WiFiPvP;
    using com.u3d.bases.display;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.CompilerServices;
    using TNet;

    internal static class FunctionExtentions
    {
        public static void Add(this Dictionary<byte, SGameClient.OnPacket> dic, SGameClient.OnPacket handler)
        {
            dic.Add(0x7f, handler);
        }

        public static void Add(this IDictionary<ActionDisplay, FightCounter> dic, ActionDisplay key)
        {
            if (dic.ContainsKey(key))
            {
                dic.Remove(key);
            }
            dic.Add(key, new FightCounter());
        }

        public static BinaryWriter BeginPacket(this TNet.Buffer buf)
        {
            return buf.BeginPacket((byte) 0x7f);
        }

        public static BinaryWriter BeginSend(this SGameClient gc)
        {
            return gc.BeginSend((byte) 0x7f);
        }

        public static void Update(this IDictionary<ActionDisplay, FightCounter> dic, ActionDisplay key, FightCounter value)
        {
            if (dic.ContainsKey(key))
            {
                dic.Remove(key);
            }
            dic.Add(key, value);
        }
    }
}

