﻿namespace com.net.wifi_pvp
{
    using com.game.consts;
    using System;
    using System.IO;
    using System.Runtime.InteropServices;

    public class RpcData
    {
        public byte[] buffer;
        public string extraMessage;
        public WifiPvpConst.MsgType msg_type;

        public RpcData()
        {
        }

        public RpcData(WifiPvpConst.MsgType type, byte[] buf = null, string extraMessage = null)
        {
            this.extraMessage = extraMessage;
            this.msg_type = type;
            this.buffer = buf;
        }

        public RpcData(int type, byte[] buf = null, string extraMessage = null)
        {
            this.extraMessage = extraMessage;
            this.msg_type = (WifiPvpConst.MsgType) ((byte) type);
            this.buffer = buf;
        }

        public static RpcData GetRpcProtocol(MemoryStream msdata, byte framNum, byte wayNum)
        {
            byte[] buffer = new byte[1];
            byte[] buffer2 = new byte[1];
            buffer[0] = framNum;
            buffer2[0] = wayNum;
            byte[] buffer3 = msdata.ToArray();
            byte[] array = new byte[2 + buffer3.Length];
            buffer.CopyTo(array, 0);
            buffer2.CopyTo(array, 1);
            buffer3.CopyTo(array, 2);
            return new RpcData(WifiPvpConst.MsgType.PROTOCAL, array, null);
        }
    }
}

