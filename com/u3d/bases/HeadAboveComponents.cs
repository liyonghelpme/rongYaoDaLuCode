﻿namespace com.u3d.bases
{
    using com.game.consts;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.hud;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class HeadAboveComponents : MonoBehaviour
    {
        private Font _textFont;
        private Dictionary<HeadComponentsType, List<GameObject>> _totalComponentsDic;
        private const float OFFSET = 3f;
        private const float WARSTATE_HEIGHT = 50f;

        private GameObject AddChildByType(HeadComponentsType type, string content)
        {
            GameObject obj2;
            if (type != HeadComponentsType.WarState)
            {
                obj2 = NGUITools.AddChild(base.gameObject);
                switch (type)
                {
                    case HeadComponentsType.Name:
                    case HeadComponentsType.Club:
                        this.AddTextChild(obj2, content);
                        break;

                    case HeadComponentsType.Title:
                        this.AddSpriteChild(obj2, content);
                        break;
                }
            }
            else
            {
                obj2 = NGUITools.AddChild(base.gameObject, HudView.Instance.GetHudPrefab("Effect/Buff/10091.assetbundle"));
                obj2.transform.localScale = new Vector3(400f, 400f, 0f);
            }
            this._totalComponentsDic[type].Add(obj2);
            obj2.name = type.ToString();
            return obj2;
        }

        public GameObject AddHeadComponents(HeadComponentsType type, string content)
        {
            GameObject obj2 = this.AddChildByType(type, content);
            this.ResetHeadComponentPos();
            return obj2;
        }

        private void AddSpriteChild(GameObject child, string content)
        {
            UISprite sprite = child.AddComponent<UISprite>();
            sprite.atlas = Singleton<AtlasManager>.Instance.GetAtlas("Header");
            sprite.spriteName = content;
        }

        private void AddTextChild(GameObject child, string content)
        {
            UILabel label = child.AddComponent<UILabel>();
            label.text = content;
            label.trueTypeFont = this._textFont;
            label.fontSize = 20;
            label.height = 30;
            label.effectStyle = UILabel.Effect.Outline;
            label.effectColor = ColorConst.Black;
        }

        private void Awake()
        {
            this._totalComponentsDic = new Dictionary<HeadComponentsType, List<GameObject>>();
            this._totalComponentsDic.Add(HeadComponentsType.Name, new List<GameObject>());
            this._totalComponentsDic.Add(HeadComponentsType.Club, new List<GameObject>());
            this._totalComponentsDic.Add(HeadComponentsType.Title, new List<GameObject>());
            this._totalComponentsDic.Add(HeadComponentsType.WarState, new List<GameObject>());
            AssetManager.Instance.LoadAsset<Font>("UI/Font/msyh.assetbundle", new LoadAssetFinish<Font>(this.LoadFontCallBack), "msyh", false, true);
        }

        private void LoadFontCallBack(object go)
        {
            this._textFont = go as Font;
        }

        private void ResetHeadComponentPos()
        {
            float height = 0f;
            foreach (GameObject obj2 in this._totalComponentsDic[HeadComponentsType.Name])
            {
                height = this.SetLabelHeight(obj2, height);
            }
            foreach (GameObject obj3 in this._totalComponentsDic[HeadComponentsType.Club])
            {
                height = this.SetLabelHeight(obj3, height);
            }
            foreach (GameObject obj4 in this._totalComponentsDic[HeadComponentsType.Title])
            {
                height = this.SetSpriteHeight(obj4, height);
            }
            foreach (GameObject obj5 in this._totalComponentsDic[HeadComponentsType.WarState])
            {
                height = this.SetWarStateEffHeight(obj5, height);
            }
        }

        private float SetLabelHeight(GameObject go, float height)
        {
            UILabel component = go.GetComponent<UILabel>();
            float y = (height + (component.printedSize.y / 2f)) + 3f;
            go.transform.localPosition = new Vector3(0f, y, 0f);
            return (y + (component.printedSize.y / 2f));
        }

        private float SetSpriteHeight(GameObject go, float height)
        {
            UISprite component = go.GetComponent<UISprite>();
            float y = (height + (component.height / 2)) + 3f;
            go.transform.localPosition = new Vector3(0f, y, 0f);
            return (y + (component.height / 2));
        }

        private float SetWarStateEffHeight(GameObject go, float height)
        {
            float y = (height + 25f) + 3f;
            go.transform.localPosition = new Vector3(0f, y, 0f);
            return (y + 25f);
        }

        private void Start()
        {
        }

        private void Update()
        {
        }

        public GameObject UpdateTitle(string content, int index)
        {
            GameObject obj2;
            if (this._totalComponentsDic[HeadComponentsType.Title].Count < (index + 1))
            {
                obj2 = this.AddHeadComponents(HeadComponentsType.Title, content);
            }
            else
            {
                obj2 = this._totalComponentsDic[3][index];
                obj2.GetComponent<UISprite>().spriteName = content;
            }
            this.ResetHeadComponentPos();
            return obj2;
        }
    }
}

