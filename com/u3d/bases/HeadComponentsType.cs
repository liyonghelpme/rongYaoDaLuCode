﻿namespace com.u3d.bases
{
    using System;

    public enum HeadComponentsType
    {
        Club = 2,
        Name = 1,
        Title = 3,
        WarState = 4
    }
}

