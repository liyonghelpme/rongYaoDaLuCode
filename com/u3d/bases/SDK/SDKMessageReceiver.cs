﻿namespace com.u3d.bases.SDK
{
    using com.game.basic;
    using System;
    using UnityEngine;

    public class SDKMessageReceiver : MonoBehaviour
    {
        public void EndTalk()
        {
            GlobalAPI.facade.Notify(0x67, 0, 0, null);
        }

        public void MediaPlayedComplete()
        {
            GlobalAPI.facade.Notify(0x68, 0, 0, null);
            Debug.Log("palyed over");
        }

        public void ReceiveMessage(string str)
        {
            Debug.Log("Voice    : message = " + str);
            GlobalAPI.facade.Notify(0x65, 0, 0, str);
        }

        public void ShowText(string str)
        {
            Debug.Log("Voice :  " + str);
        }

        public void StoreMessageComplete()
        {
            GlobalAPI.facade.Notify(0x66, 0, 0, null);
        }
    }
}

