﻿namespace com.u3d.bases.ai
{
    using com.game.manager;
    using com.game.module.core;
    using com.game.utils;
    using System;
    using System.Collections.Generic;

    public static class AiConfig
    {
        private static float _arena_ai_select_target_margin;
        private static float _arena_select_target_interval;
        private static List<SimpleBuffVo> _heal_skill_buff_list;
        private static float _hp_percentage_to_continue_ai;
        private static float _low_hp_percent_in_wifi_pvp;
        private static float _melee_general_stand_dis_from_master_in_pve;
        private static float _mon_pve_ai_use_atk_interval;
        private static float _mon_pve_ai_use_rand_skill_interval;
        private static float _mon_recovery_percent;
        private static float _near_the_master_gap;
        private static float _out_of_battle_time_in_wifi_pvp;
        private static float _poss_to_attack_on_mon_idle;
        private static float _ranged_general_stand_dis_from_master_in_pve;
        private static float _role_ai_start_after_add_hp_near_spring;
        private static float _role_hp_rate_to_escape_from_tower;
        private static float _role_recovery_percent;
        private static float _role_recovery_percent_near_spring_in_wifi_pvp;
        private static float _role_recovery_percent_out_of_battle_in_wifi_pvp;
        private static uint _role_recovery_skill_id_in_pvp;
        private static float _tower_attack_range_in_pvp;
        public const float AI_CLOSE_TO_DEST_GAP = 0.5f;
        public const float AI_DIVIDE_WITH_1000 = 0.001f;
        public const int AI_TIME_DENOMINATOR = 0x3e8;
        public const float AUTO_ATTACK_DELAY = 0.05f;
        public const float FAILED_GETING_DISTANCE = 99999f;
        public const float IN_BATTLE_TIME_INTERVAL = 1f;
        public const int MAX_CACHED_AI_BEHAVIOR = 1;
        private static int max_continuous_use_skill_count;
        public const int MON_ACTION_TYPE_ATTACKED = 13;
        public const int MON_ACTION_TYPE_BLOOD = 3;
        public const int MON_ACTION_TYPE_BLOOD_BROAD = 11;
        public const int MON_ACTION_TYPE_CYCLE = 7;
        public const int MON_ACTION_TYPE_DEAD = 4;
        public const int MON_ACTION_TYPE_FIGHT_CYCLE = 9;
        public const int MON_ACTION_TYPE_MOVE = 8;
        public const int MON_ACTION_TYPE_NO_HART = 14;
        public const int MON_ACTION_TYPE_REBORN = 5;
        public const int MON_ACTION_TYPE_RELY = 10;
        public const int MON_ACTION_TYPE_TAR = 12;
        public const float MONSTER_LOOP_INTERVAL_BATTLE = 0.5f;
        public const float MONSTER_LOOP_INTERVAL_NORMAL = 0.2f;
        public const float MONSTER_LOOP_INTERVAL_RUNNING = 0.2f;
        public const float OUT_OF_BATTLE_TIME_GAP = 3f;
        public const float PRIVATE_ATTACK_TIME_GAP = 0.45f;
        public const float ROLE_ATTACK_TIME_GAP = 0f;
        public const float ROLE_LOOP_INTERVAL_ARENA = 0.15f;
        public const float ROLE_LOOP_INTERVAL_NORMAL = 0.15f;
        public const float ROLE_SKILL_TIME_GAP = 0f;
        public const float STATIC_ITEM_LOOP_INTERVAL = float.MaxValue;
        public const float TOWER_ATTACK_INTERVAL = 1f;
        public const float TOWER_LOOP_INTERVAL = 0.1f;

        private static List<SimpleBuffVo> DoGetHealSkillBuffList()
        {
            uint id = ROLE_RECOVERY_SKILL_ID_IN_PVP;
            _heal_skill_buff_list = StringUtils.GetSimpleBuffVoFromBuffListWhenUseSkill(BaseDataMgr.instance.GetSysSkillBaseVo(id).buff_list_when_use_skill);
            return _heal_skill_buff_list;
        }

        public static List<SimpleBuffVo> GetBuffListOfHealSkill()
        {
            if (_heal_skill_buff_list == null)
            {
            }
            return DoGetHealSkillBuffList();
        }

        public static float ARENA_AI_SELECT_TARGET_MARGIN
        {
            get
            {
                if (_arena_ai_select_target_margin == 0f)
                {
                    _arena_ai_select_target_margin = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("ARENA_SELECT_TARGET_DISTANCE_MARGIN"));
                }
                return _arena_ai_select_target_margin;
            }
        }

        public static float ARENA_SELECT_TARGET_INTERVAL
        {
            get
            {
                if (_arena_select_target_interval == 0f)
                {
                    _arena_select_target_interval = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("ARENA_SELECT_TARGET_INTERVAL"));
                }
                return _arena_select_target_interval;
            }
        }

        public static float HP_PERCENTAGE_TO_CONTINUE_AI
        {
            get
            {
                if (_hp_percentage_to_continue_ai == 0f)
                {
                    _hp_percentage_to_continue_ai = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("HP_PERCENTAGE_TO_CONTINUE_AI"));
                }
                return _hp_percentage_to_continue_ai;
            }
        }

        public static float LOW_HP_PERCENT_IN_WIFI_PVP
        {
            get
            {
                if (_low_hp_percent_in_wifi_pvp == 0f)
                {
                    _low_hp_percent_in_wifi_pvp = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("LOW_HP_PERCENT_IN_WIFI_PVP"));
                }
                return _low_hp_percent_in_wifi_pvp;
            }
        }

        public static int MAX_CONTINUOUS_USE_SKILL_COUNT
        {
            get
            {
                if (max_continuous_use_skill_count == 0)
                {
                    max_continuous_use_skill_count = Singleton<ConfigConst>.Instance.GetConfigData("MAX_CONTINUOUS_USE_SKILL_COUNT");
                }
                return max_continuous_use_skill_count;
            }
        }

        public static float MELEE_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE
        {
            get
            {
                if (_melee_general_stand_dis_from_master_in_pve == 0f)
                {
                    _melee_general_stand_dis_from_master_in_pve = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("MELEE_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE"));
                }
                return _melee_general_stand_dis_from_master_in_pve;
            }
        }

        public static float MON_RECOVERY_PERCENT
        {
            get
            {
                if (_mon_recovery_percent == 0f)
                {
                    _mon_recovery_percent = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("MON_RECOVERY_PERCENT"));
                }
                return _mon_recovery_percent;
            }
        }

        public static float MONSTER_PVE_AI_ATTACK_INTERVAL
        {
            get
            {
                if (_mon_pve_ai_use_atk_interval == 0f)
                {
                    _mon_pve_ai_use_atk_interval = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("MONSTER_PVE_AI_ATTACK_INTERVAL")) * 0.001f;
                }
                return _mon_pve_ai_use_atk_interval;
            }
        }

        public static float MONSTER_PVE_AI_USE_RAND_SKILL_INTERVAL
        {
            get
            {
                if (_mon_pve_ai_use_rand_skill_interval == 0f)
                {
                    _mon_pve_ai_use_rand_skill_interval = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("MONSTER_PVE_AI_USE_RAND_SKILL_INTERVAL")) * 0.001f;
                }
                return _mon_pve_ai_use_rand_skill_interval;
            }
        }

        public static float NEAR_THE_MASTER_GAP
        {
            get
            {
                if (_near_the_master_gap == 0f)
                {
                    _near_the_master_gap = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("NEAR_THE_MASTER_GAP_IN_PVE"));
                }
                return _near_the_master_gap;
            }
        }

        public static float OUT_OF_BATTLE_TIME_IN_WIFI_PVP
        {
            get
            {
                if (_out_of_battle_time_in_wifi_pvp == 0f)
                {
                    _out_of_battle_time_in_wifi_pvp = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("OUT_OF_BATTLE_TIME_IN_WIFI_PVP"));
                }
                return _out_of_battle_time_in_wifi_pvp;
            }
        }

        public static float POSS_TO_ATTCK_ON_MON_IDLE
        {
            get
            {
                if (_poss_to_attack_on_mon_idle == 0f)
                {
                    _poss_to_attack_on_mon_idle = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("POSS_TO_ATTCK_ON_MON_IDLE"));
                }
                return _poss_to_attack_on_mon_idle;
            }
        }

        public static float RANGED_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE
        {
            get
            {
                if (_ranged_general_stand_dis_from_master_in_pve == 0f)
                {
                    _ranged_general_stand_dis_from_master_in_pve = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("RANGED_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE"));
                }
                return _ranged_general_stand_dis_from_master_in_pve;
            }
        }

        public static float ROLE_AI_START_AFTER_ADD_HP_NEAR_SPRING
        {
            get
            {
                if (_role_ai_start_after_add_hp_near_spring == 0f)
                {
                    _role_ai_start_after_add_hp_near_spring = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("ROLE_AI_START_AFTER_ADD_HP_NEAR_SPRING"));
                }
                return _role_ai_start_after_add_hp_near_spring;
            }
        }

        public static float ROLE_HP_RATE_TO_ESCAPE_FROM_TOWER
        {
            get
            {
                if (_role_hp_rate_to_escape_from_tower == 0f)
                {
                    _role_hp_rate_to_escape_from_tower = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("ROLE_HP_RATE_TO_ESCAPE_FROM_TOWER"));
                }
                return _role_hp_rate_to_escape_from_tower;
            }
        }

        public static float ROLE_RECOVERY_PERCENT
        {
            get
            {
                if (_role_recovery_percent == 0f)
                {
                    _role_recovery_percent = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("ROLE_RECOVERY_PERCENT"));
                }
                return _role_recovery_percent;
            }
        }

        public static float ROLE_RECOVERY_PERCENT_NEAR_SPRING_IN_WIFI_PVP
        {
            get
            {
                if (_role_recovery_percent_near_spring_in_wifi_pvp == 0f)
                {
                    _role_recovery_percent_near_spring_in_wifi_pvp = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("ROLE_RECOVERY_PERCENT_NEAR_SPRING_IN_WIFI_PVP"));
                }
                return _role_recovery_percent_near_spring_in_wifi_pvp;
            }
        }

        public static float ROLE_RECOVERY_PERCENT_OUT_OF_BATTLE_IN_WIFI_PVP
        {
            get
            {
                if (_role_recovery_percent_out_of_battle_in_wifi_pvp == 0f)
                {
                    _role_recovery_percent_out_of_battle_in_wifi_pvp = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("ROLE_RECOVERY_PERCENT_OUT_OF_BATTLE_IN_WIFI_PVP"));
                }
                return _role_recovery_percent_out_of_battle_in_wifi_pvp;
            }
        }

        public static uint ROLE_RECOVERY_SKILL_ID_IN_PVP
        {
            get
            {
                if (_role_recovery_skill_id_in_pvp == 0f)
                {
                    _role_recovery_skill_id_in_pvp = Convert.ToUInt32(Singleton<ConfigConst>.Instance.GetConfigData("ROLE_RECOVERY_SKILL_ID_IN_PVP"));
                }
                return _role_recovery_skill_id_in_pvp;
            }
        }

        public static float TOWER_ATTACK_RANGE_IN_PVP
        {
            get
            {
                if (_tower_attack_range_in_pvp == 0f)
                {
                    _tower_attack_range_in_pvp = Convert.ToSingle(Singleton<ConfigConst>.Instance.GetConfigData("TOWER_ATTACK_RANGE_IN_PVP"));
                }
                return _tower_attack_range_in_pvp;
            }
        }
    }
}

