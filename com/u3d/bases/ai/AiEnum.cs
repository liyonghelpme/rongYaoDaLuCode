﻿namespace com.u3d.bases.ai
{
    using System;

    public static class AiEnum
    {
        public enum AiScriptType
        {
            MONSTER_PVE = 10,
            PLAYER_ARENA = 3,
            PLAYER_PVE = 1,
            PLAYER_PVP = 2,
            PRIVATE_PVP = 100,
            STATIC_ITEM = 300,
            SUMMON_MONSTER = 11,
            TOWER = 200
        }

        public enum CacheType
        {
            ATTACK = 1,
            ATTACKED_SKILL = 4,
            BLOOD_SKILL = 2,
            CYCLE_SKILL = 3,
            DEAD_SKILL = 5,
            NORMAL_SKILL = 0x63
        }

        public enum MonsterAiStatus
        {
            CAST_SKILL = 100,
            CRUISE = 200,
            RUN_TO_BORN_POS = 13,
            RUN_TO_MASTER = 11,
            RUN_TO_MAX_RANGE = 12,
            RUN_TO_TARGET = 10,
            THINK = 0
        }

        public enum PlayerAiStatus
        {
            CAST_SKILL = 100,
            ESCAPE_FROM_TOWER = 200,
            NEAR_SPRING = 0xc9,
            RUN_TO_ENEMY_BASE = 12,
            RUN_TO_MASTER = 10,
            RUN_TO_MY_SPRING = 13,
            RUN_TO_TARGET = 11,
            THINK = 0
        }

        public enum PrivateAiStatus
        {
            CAST_SKILL = 100,
            RUN_TO_ENEMYBASE = 11,
            RUN_TO_ROADPATH = 12,
            RUN_TO_TARGET = 10,
            THINK = 0
        }

        public enum SummonAiStatus
        {
            CAST_SKILL = 12,
            RUN_TO_MASTER = 11,
            RUN_TO_TARGET = 10,
            THINK = 0
        }
    }
}

