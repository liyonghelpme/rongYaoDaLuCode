﻿namespace com.u3d.bases.ai.AiManager
{
    using com.game;
    using com.u3d.bases.ai;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class AiTargetManager
    {
        private static readonly Dictionary<ActionControler, uint> _targetedCounter;
        private static readonly Dictionary<ActionControler, ActionControler> _targetOfEnemy;
        private static readonly Dictionary<ActionControler, ActionControler> _targetOfMine;
        [CompilerGenerated]
        private static Func<MonsterDisplay, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<MonsterDisplay, <>__AnonType4<MonsterDisplay, AiControllerBase>> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<<>__AnonType4<MonsterDisplay, AiControllerBase>, bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<<>__AnonType4<MonsterDisplay, AiControllerBase>, <>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, <>__AnonType6<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, MonsterAiController>> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<<>__AnonType6<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, MonsterAiController>, bool> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<<>__AnonType6<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, MonsterAiController>, MonsterAiController> <>f__am$cacheA;
        public static readonly AiTargetManager instance;

        static AiTargetManager()
        {
            if (instance == null)
            {
            }
            instance = new AiTargetManager();
            _targetedCounter = new Dictionary<ActionControler, uint>();
            _targetOfEnemy = new Dictionary<ActionControler, ActionControler>();
            _targetOfMine = new Dictionary<ActionControler, ActionControler>();
        }

        private AiTargetManager()
        {
        }

        private static void AddTargetedCounter(ActionControler target)
        {
            if (_targetedCounter.ContainsKey(target))
            {
                Dictionary<ActionControler, uint> dictionary;
                ActionControler controler;
                uint num = dictionary[controler];
                (dictionary = _targetedCounter)[controler = target] = num + 1;
            }
            else
            {
                _targetedCounter.Add(target, 1);
            }
        }

        public void ClearAllDic()
        {
            _targetOfMine.Clear();
            _targetOfEnemy.Clear();
            _targetedCounter.Clear();
        }

        public void GetStaticOfDics(out int countEnemyDic, out int countMyDic)
        {
            countEnemyDic = _targetOfEnemy.Count;
            countMyDic = _targetOfMine.Count;
        }

        public static void InformIdleMonsterEnterBattle(int groupIndex, ActionDisplay ad)
        {
            <InformIdleMonsterEnterBattle>c__AnonStorey104 storey = new <InformIdleMonsterEnterBattle>c__AnonStorey104 {
                groupIndex = groupIndex
            };
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = md => (bool) md.Controller;
            }
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = md => new <>__AnonType4<MonsterDisplay, AiControllerBase>(md, md.Controller.AiController);
            }
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = <>__TranspIdent5 => (<>__TranspIdent5.ai != null) && (<>__TranspIdent5.ai.GetTargetDisplay() == null);
            }
            if (<>f__am$cache7 == null)
            {
                <>f__am$cache7 = <>__TranspIdent5 => new <>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>(<>__TranspIdent5, <>__TranspIdent5.md.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cache8 == null)
            {
                <>f__am$cache8 = <>__TranspIdent6 => new <>__AnonType6<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, MonsterAiController>(<>__TranspIdent6, <>__TranspIdent6.<>__TranspIdent5.ai as MonsterAiController);
            }
            if (<>f__am$cache9 == null)
            {
                <>f__am$cache9 = <>__TranspIdent7 => <>__TranspIdent7.ai2 != null;
            }
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = <>__TranspIdent7 => <>__TranspIdent7.ai2;
            }
            IEnumerator<MonsterAiController> enumerator = AppMap.Instance.monsterList.Where<MonsterDisplay>(<>f__am$cache4).Select<MonsterDisplay, <>__AnonType4<MonsterDisplay, AiControllerBase>>(<>f__am$cache5).Where<<>__AnonType4<MonsterDisplay, AiControllerBase>>(<>f__am$cache6).Select<<>__AnonType4<MonsterDisplay, AiControllerBase>, <>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>>(<>f__am$cache7).Where<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>>(new Func<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, bool>(storey.<>m__126)).Select<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, <>__AnonType6<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, MonsterAiController>>(<>f__am$cache8).Where<<>__AnonType6<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, MonsterAiController>>(<>f__am$cache9).Select<<>__AnonType6<<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo>, MonsterAiController>, MonsterAiController>(<>f__am$cacheA).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.SetTargetDisplayButSkipInformingGroup(ad);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public bool IsEveryoneHasNoTarget(List<ActionControler> list)
        {
            foreach (ActionControler controler in list)
            {
                if (_targetOfMine.ContainsKey(controler))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsMeBeTargeted(ActionControler me)
        {
            return _targetedCounter.ContainsKey(me);
        }

        public bool IsMeHasTarget(ActionControler me)
        {
            return _targetOfMine.ContainsKey(me);
        }

        public bool IsMonsterHasTarget(ActionControler me)
        {
            return _targetOfEnemy.ContainsKey(me);
        }

        public void MeUpdateTarget(ActionControler me, ActionControler target = null)
        {
            if (_targetOfMine.ContainsKey(me))
            {
                ActionControler key = _targetOfMine[me];
                if (_targetedCounter.ContainsKey(key))
                {
                    ReduceTargetedCounter(key);
                }
                _targetOfMine.Remove(me);
            }
            if (target != null)
            {
                _targetOfMine.Add(me, target);
                AddTargetedCounter(target);
            }
        }

        public void MonsterDie(ActionControler me)
        {
            this.MonsterUpdateTarget(me, null);
        }

        public void MonsterUpdateTarget(ActionControler me, ActionControler target = null)
        {
            if (_targetOfEnemy.ContainsKey(me))
            {
                ActionControler key = _targetOfEnemy[me];
                if (_targetedCounter.ContainsKey(key))
                {
                    ReduceTargetedCounter(key);
                }
                _targetOfEnemy.Remove(me);
            }
            if (target != null)
            {
                _targetOfEnemy.Add(me, target);
                AddTargetedCounter(target);
            }
        }

        private static void ReduceTargetedCounter(ActionControler beTargeted)
        {
            uint num = _targetedCounter[beTargeted];
            if (num <= 1)
            {
                _targetedCounter.Remove(beTargeted);
            }
            else
            {
                Dictionary<ActionControler, uint> dictionary;
                ActionControler controler;
                uint num2 = dictionary[controler];
                (dictionary = _targetedCounter)[controler = beTargeted] = num2 - 1;
            }
        }

        [CompilerGenerated]
        private sealed class <InformIdleMonsterEnterBattle>c__AnonStorey104
        {
            internal int groupIndex;

            internal bool <>m__126(<>__AnonType5<<>__AnonType4<MonsterDisplay, AiControllerBase>, MonsterVo> <>__TranspIdent6)
            {
                return (!<>__TranspIdent6.baserolevo.IsEmptyHp && (<>__TranspIdent6.baserolevo.groupIndex == this.groupIndex));
            }
        }
    }
}

