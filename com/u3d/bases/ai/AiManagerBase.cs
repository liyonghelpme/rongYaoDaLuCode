﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.data;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public abstract class AiManagerBase
    {
        [CompilerGenerated]
        private static Func<MonsterDisplay, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType3<PlayerDisplay, BaseRoleVo>> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<<>__AnonType3<PlayerDisplay, BaseRoleVo>, bool> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<<>__AnonType3<PlayerDisplay, BaseRoleVo>, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<<>__AnonType3<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<PlayerDisplay, bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<PlayerDisplay, bool> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<MonsterDisplay, bool> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<PlayerDisplay, bool> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType3<PlayerDisplay, BaseRoleVo>> <>f__am$cacheA;
        [CompilerGenerated]
        private static Func<<>__AnonType3<PlayerDisplay, BaseRoleVo>, bool> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<<>__AnonType3<PlayerDisplay, BaseRoleVo>, bool> <>f__am$cacheC;
        [CompilerGenerated]
        private static Func<<>__AnonType3<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cacheD;
        [CompilerGenerated]
        private static Func<PlayerDisplay, bool> <>f__am$cacheE;
        protected readonly IList<AiElem> List = new List<AiElem>();

        protected AiManagerBase()
        {
        }

        protected virtual void Add(AiElem elem)
        {
            this.List.Add(elem);
        }

        protected virtual void Add(ActionDisplay bd)
        {
        }

        public virtual void Add(IEnumerable<MonsterDisplay> addList)
        {
            IEnumerator<MonsterDisplay> enumerator = addList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    this.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public virtual void Add(IEnumerable<PlayerDisplay> addList)
        {
            this.List.Clear();
            IEnumerator<PlayerDisplay> enumerator = addList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    this.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public virtual void BeginBattle()
        {
        }

        public virtual void Clear()
        {
            this.List.Clear();
        }

        public static void DisableAIAfterTranscation()
        {
            if (GlobalData.isInCopy)
            {
                PlayerAiController aiController = AppMap.Instance.me.Controller.AiController as PlayerAiController;
                if ((aiController != null) && aiController.IsAiEnable)
                {
                    aiController.StopAiBehavoirRightNow();
                }
            }
            else if (AppMap.Instance.IsInWifiPVP)
            {
                PlayerPVPAiController controller2 = AppMap.Instance.me.Controller.AiController as PlayerPVPAiController;
                if (controller2 != null)
                {
                    controller2.SetAi(false);
                }
            }
        }

        public static void EnableAllMonsterAi()
        {
            if (<>f__am$cache8 == null)
            {
                <>f__am$cache8 = mon => mon.Controller != null;
            }
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.Where<MonsterDisplay>(<>f__am$cache8).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StartAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void EnableAllPlayerAi()
        {
            if (<>f__am$cacheE == null)
            {
                <>f__am$cacheE = player => player.Controller != null;
            }
            IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.playerList.Where<PlayerDisplay>(<>f__am$cacheE).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StopAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void EnableMyAi()
        {
            if (<>f__am$cache9 == null)
            {
                <>f__am$cache9 = player => player.Controller != null;
            }
            IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.SelfplayerList.Where<PlayerDisplay>(<>f__am$cache9).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StartAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void EnableOtherPlayerAi()
        {
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = player => new <>__AnonType3<PlayerDisplay, BaseRoleVo>(player, player.GetVo());
            }
            if (<>f__am$cacheB == null)
            {
                <>f__am$cacheB = <>__TranspIdent4 => <>__TranspIdent4.nowDisplay.Controller != null;
            }
            if (<>f__am$cacheC == null)
            {
                <>f__am$cacheC = <>__TranspIdent4 => <>__TranspIdent4.nowDisplay.SubType != 1;
            }
            if (<>f__am$cacheD == null)
            {
                <>f__am$cacheD = <>__TranspIdent4 => <>__TranspIdent4.player;
            }
            IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType3<PlayerDisplay, BaseRoleVo>>(<>f__am$cacheA).Where<<>__AnonType3<PlayerDisplay, BaseRoleVo>>(<>f__am$cacheB).Where<<>__AnonType3<PlayerDisplay, BaseRoleVo>>(<>f__am$cacheC).Select<<>__AnonType3<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cacheD).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StartAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public virtual void EndBattle()
        {
        }

        public virtual void EndBattle(ActionDisplay display)
        {
        }

        public virtual void Remove(AiElem elem)
        {
            this.List.Remove(elem);
        }

        public static void StopAllAi()
        {
            StopAllMonsterAi();
            StopAllPlayerAi();
        }

        public static void StopAllMonsterAi()
        {
            if (<>f__am$cache1 == null)
            {
                <>f__am$cache1 = mon => mon.Controller != null;
            }
            IEnumerator<MonsterDisplay> enumerator = AppMap.Instance.monsterList.Where<MonsterDisplay>(<>f__am$cache1).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StopAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void StopAllPlayerAi()
        {
            if (<>f__am$cache7 == null)
            {
                <>f__am$cache7 = player => player.Controller != null;
            }
            IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.playerList.Where<PlayerDisplay>(<>f__am$cache7).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StopAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void StopMyAi()
        {
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = player => player.Controller != null;
            }
            IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.SelfplayerList.Where<PlayerDisplay>(<>f__am$cache6).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StopAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void StopOtherPlayerAi()
        {
            if (<>f__am$cache2 == null)
            {
                <>f__am$cache2 = player => new <>__AnonType3<PlayerDisplay, BaseRoleVo>(player, player.GetVo());
            }
            if (<>f__am$cache3 == null)
            {
                <>f__am$cache3 = <>__TranspIdent3 => <>__TranspIdent3.nowDisplay.Controller != null;
            }
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = <>__TranspIdent3 => <>__TranspIdent3.nowDisplay.SubType != 1;
            }
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = <>__TranspIdent3 => <>__TranspIdent3.player;
            }
            IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType3<PlayerDisplay, BaseRoleVo>>(<>f__am$cache2).Where<<>__AnonType3<PlayerDisplay, BaseRoleVo>>(<>f__am$cache3).Where<<>__AnonType3<PlayerDisplay, BaseRoleVo>>(<>f__am$cache4).Select<<>__AnonType3<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cache5).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.StopAi();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public abstract class AiElem
        {
            public AiControllerBase Ai;
            public ActionDisplay Display;
            public ActionControler Me;

            protected AiElem()
            {
            }
        }
    }
}

