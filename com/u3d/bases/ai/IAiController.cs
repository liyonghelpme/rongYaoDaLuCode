﻿namespace com.u3d.bases.ai
{
    using com.u3d.bases.display;
    using System;

    internal interface IAiController
    {
        void CleanTargetDisplay();
        void ForceToStopAIAndAllBehavior();
        int GetAiStatus();
        ActionDisplay GetTargetDisplay();
        void SetAi(bool value);
        void SetAiToPrevious();
        void SetBattleState(bool value);
        void SetTargetDisplay(ActionDisplay ad);

        bool IsAiEnable { get; set; }

        bool IsInBattle { get; set; }

        float LastBeAttackedTime { get; }

        float LastSkilledSuccessfullyTime { get; }

        float LastUseAttackTime { get; }

        float LastUseSkillTime { get; }
    }
}

