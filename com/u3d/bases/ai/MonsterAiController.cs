﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.hud;
    using com.game.module.map;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.fsmUtil;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class MonsterAiController : AiControllerBase
    {
        private float _attack_range;
        private List<AttackedCheck> _attacked_check = new List<AttackedCheck>();
        private List<BloodBroad> _blood_broad = new List<BloodBroad>();
        private List<BloodCheck> _blood_check = new List<BloodCheck>();
        private Vector3 _bornPos;
        private float _cruise_range;
        private List<CycleCheck> _cycle_check = new List<CycleCheck>();
        private List<DeadCheck> _dead_check = new List<DeadCheck>();
        private List<FightCycleCheck> _fight_cycle_check = new List<FightCycleCheck>();
        private uint _hpFull;
        private uint _hpNow;
        private uint _hpPrevious;
        private bool _is_active;
        private bool _is_cruise;
        private bool _is_move;
        private bool _isBackingToBornPos;
        private bool _isBoss;
        private bool _isBossBloodBarAdded;
        private bool _isCurrentTargetPlayerRandom;
        private bool _isRunningToMaxRange;
        public float _keepRunningTime;
        private ActionDisplay _lastTargetDisplay;
        private List<LimitTar> _limit_tar = new List<LimitTar>();
        private float _max_range;
        public Vector3 _meLastPos;
        private MonsterVo _meMonsterVo;
        private List<MonRandomSkill> _mon_rand_skill = new List<MonRandomSkill>();
        private bool _no_hart;
        private List<com.game.vo.RebornCheck> _reborn_check = new List<com.game.vo.RebornCheck>();
        private List<RelyCheck> _rely_check = new List<RelyCheck>();
        private float _search_stop;
        private float _sight_range;
        public int _walkFailCount;
        [CompilerGenerated]
        private static PreMoveCallBack <>f__am$cache23;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType7<PlayerDisplay, BaseRoleVo>> <>f__am$cache24;
        [CompilerGenerated]
        private static Func<<>__AnonType7<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cache25;
        [CompilerGenerated]
        private static Func<ActionDisplay, <>__AnonType8<ActionDisplay, BaseRoleVo>> <>f__am$cache26;
        [CompilerGenerated]
        private static Func<<>__AnonType8<ActionDisplay, BaseRoleVo>, <>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>> <>f__am$cache27;
        [CompilerGenerated]
        private static Func<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, <>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>> <>f__am$cache28;
        [CompilerGenerated]
        private static Func<<>__AnonType14<<>__AnonType13<<>__AnonType12<<>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, float>, bool>, bool>, bool> <>f__am$cache29;
        [CompilerGenerated]
        private static Func<<>__AnonType14<<>__AnonType13<<>__AnonType12<<>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, float>, bool>, bool>, ActionDisplay> <>f__am$cache2A;
        [CompilerGenerated]
        private static Func<ActionDisplay, <>__AnonType8<ActionDisplay, BaseRoleVo>> <>f__am$cache2B;
        [CompilerGenerated]
        private static Func<<>__AnonType8<ActionDisplay, BaseRoleVo>, <>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>> <>f__am$cache2C;
        [CompilerGenerated]
        private static Func<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, <>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>> <>f__am$cache2D;
        [CompilerGenerated]
        private static Func<<>__AnonType18<<>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, bool>, bool> <>f__am$cache2E;
        [CompilerGenerated]
        private static Func<<>__AnonType18<<>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, bool>, ActionDisplay> <>f__am$cache2F;
        private float distanceToTarget;
        private Vector3 RunningToMaxRangePos;

        private void AddHp()
        {
            this.AddHp(false);
        }

        private void AddHp(bool isFullRestore)
        {
            if (this._meMonsterVo.CurHpPercent != 1f)
            {
                float num = !isFullRestore ? AiConfig.MON_RECOVERY_PERCENT : 1000f;
                float delta = this._meMonsterVo.Hp * (num * 0.001f);
                this._meMonsterVo.AddHp(delta);
                if (this._meMonsterVo.MonsterVO.quality == 3)
                {
                    Singleton<BattleTopView>.Instance.recoverBloodBar((float) this._meMonsterVo.Hp, (float) this._meMonsterVo.CurHp);
                }
                else
                {
                    HudView.Instance.recoverBlood(this._meMonsterVo.Controller.GoHp, (float) this._meMonsterVo.Hp, (float) this._meMonsterVo.CurHp, delta, false);
                }
            }
        }

        protected override void AiStateDecision(float timeElapsed)
        {
            if (!this.IsSleep())
            {
                this.DoUpdateTarget();
                this.UpdateDebugFlags();
                this._hpPrevious = this._meMonsterVo.lastHp;
                this._hpNow = this._meMonsterVo.CurHp;
                this._hpFull = this._meMonsterVo.Hp;
                if (this.CheckIsRunningToSomeWhere())
                {
                    this._keepRunningTime += timeElapsed;
                    this.CheckStartChase();
                }
                else if (base._currentTargetDisplay == null)
                {
                    if (((this._is_active && (base._nowStatus == 1)) && (base._canDisrupt && (this._keepRunningTime > this._search_stop))) && (!this._isRunningToMaxRange && !this._isBackingToBornPos))
                    {
                        this.EnterStateRest();
                    }
                    else if (((this._is_active && (base._nowStatus == 0)) && (base._canDisrupt && this._is_cruise)) && (base._stopTime > this._search_stop))
                    {
                        this.EnterStateMoveAround();
                    }
                    else
                    {
                        base._stopTime += timeElapsed;
                    }
                }
                else
                {
                    this.distanceToTarget = this.GetTargetSqrMagnitude(base._currentTargetDisplay);
                    if (this.CheckCacheAiBehavior())
                    {
                        this.EnterStateHandleCachedAiBehavior();
                    }
                    else if (this.CheckIsInFight())
                    {
                        this.EnterStateInFight(this.distanceToTarget, this._hpPrevious, this._hpNow, this._hpFull);
                    }
                    else if (this.CheckIsInRunOrIdleOrHurt())
                    {
                        switch (base._nowStatus)
                        {
                            case 0:
                                base._loop_interval = 0.2f;
                                break;

                            case 1:
                                base._loop_interval = 0.2f;
                                break;
                        }
                        if (this._is_active)
                        {
                            if (this.distanceToTarget <= this._attack_range.Square())
                            {
                                this.EnterStateMonsterAttack(this.distanceToTarget, this._hpPrevious, this._hpNow, this._hpFull);
                            }
                            else
                            {
                                this.EnterStateMonsterChasing(this._hpPrevious, this._hpNow, this._hpFull);
                            }
                        }
                    }
                }
            }
        }

        private bool CheckAddBossBloodBar()
        {
            if ((this._isBoss && !this._isBossBloodBarAdded) && (this.LastBeAttackedTime != 0f))
            {
                this._isBossBloodBarAdded = true;
                return true;
            }
            return false;
        }

        protected override bool CheckCanUseAi()
        {
            if (this._meMonsterVo.stateInfo.IsFrozen)
            {
                return false;
            }
            if (this._meMonsterVo.stateInfo.ShouldPauseAI)
            {
                return false;
            }
            bool flag = MonsterMgr.CanSetAi && this.IsAiEnable;
            bool flag2 = !this.CheckIsDead();
            bool isAllReloaded = SelfPlayerManager.instance.isAllReloaded;
            return ((flag && flag2) && isAllReloaded);
        }

        private bool CheckIsInBornPos()
        {
            return (this.GetSqrDistanceFromBornPos() <= ((float) 0.5f).Square());
        }

        private bool CheckIsInCruiseRange()
        {
            return (DamageCheck.GetDistance(base._selfTransform.position, this.RunningToMaxRangePos) <= 0.5f);
        }

        protected override bool CheckIsInFight()
        {
            bool flag = base.CheckIsInFight();
            bool flag2 = (base._nowTime - this.LastBeAttackedTime) < 1f;
            return (flag && flag2);
        }

        private bool CheckIsRunningToSomeWhere()
        {
            return (base._ai_status == 10);
        }

        public bool CheckRebornOnDie()
        {
            com.game.vo.RebornCheck properRebornCheckWhenDie = this.GetProperRebornCheckWhenDie();
            if (properRebornCheckWhenDie == null)
            {
                return false;
            }
            if ((properRebornCheckWhenDie.max_reborn_count > 0) && (properRebornCheckWhenDie.reborn_count < properRebornCheckWhenDie.max_reborn_count))
            {
                int x = Convert.ToInt32((float) (this._meMonsterVo.X * 1000f));
                int y = Convert.ToInt32((float) (this._meMonsterVo.Y * 1000f));
                int z = Convert.ToInt32((float) (this._meMonsterVo.Z * 1000f));
                int id = this._meMonsterVo.MonsterVO.id;
                int num5 = properRebornCheckWhenDie.reborn_template_id;
                ulong num6 = this._meMonsterVo.Id;
                Singleton<MapMode>.Instance.RequestMonReborn(properRebornCheckWhenDie.reborn_count + 1, num6, id, num5, x, y, z);
                properRebornCheckWhenDie.reborn_count++;
                properRebornCheckWhenDie.last_reborn_time = base._nowTime;
                UnityEngine.Debug.LogWarning("重生get");
            }
            return true;
        }

        protected override bool CheckShouldSkipThink()
        {
            if ((this._isBackingToBornPos || this._isRunningToMaxRange) && ((this._walkFailCount > 20) && (base._stopTime > 3f)))
            {
                UnityEngine.Debug.LogWarning("怪物走向出生点或者最远点时, 走不动. 强行解除傻逼状态");
                this._isRunningToMaxRange = false;
                this._isBackingToBornPos = false;
                this._walkFailCount = 0;
                base._canDisrupt = true;
                base._stopTime = 0f;
                vp_Timer.CancelTimerByHandle(base.timeHandle);
                this.SetAiStatus(AiEnum.MonsterAiStatus.THINK);
                return true;
            }
            if (this._keepRunningTime >= this._search_stop)
            {
                this._lastTargetDisplay = base._currentTargetDisplay;
                if (this.SetTargetDisplay(true))
                {
                    UnityEngine.Debug.LogWarning("怪物追击追不到, 且找得到新目标, 换新目标");
                    this.SetAiStatus(AiEnum.MonsterAiStatus.THINK);
                    this._keepRunningTime = 0f;
                    return true;
                }
                this._keepRunningTime = 0f;
                this._lastTargetDisplay = null;
            }
            return this.CheckIsDoingSkill();
        }

        private bool CheckStartChase()
        {
            if (!this._meMonsterVo.stateInfo.CanMove)
            {
                return false;
            }
            this.SetAiStatusButNotClearTarget(AiEnum.MonsterAiStatus.RUN_TO_TARGET);
            this.DoStartChase();
            base._stopTime = 0f;
            return true;
        }

        private bool CheckUseAttack(float distance)
        {
            if ((UnityEngine.Random.Range((float) 1f, (float) 1000f) >= AiConfig.POSS_TO_ATTCK_ON_MON_IDLE) || (distance > this._attack_range.Square()))
            {
                return false;
            }
            if (((base._nowTime - this.LastUseAttackTime) >= AiConfig.MONSTER_PVE_AI_ATTACK_INTERVAL) && !this.CheckCacheAiBehavior())
            {
                this.DoAttack();
                base.LastUseAttackTime = base._nowTime;
            }
            return true;
        }

        private bool CheckUseBloodChangeSkill(uint nowHp, uint previousHp, uint fullHp)
        {
            float lastHpPercentage = (((float) previousHp) / ((float) fullHp)) * 100f;
            float nowHpPercentage = (((float) nowHp) / ((float) fullHp)) * 100f;
            BloodCheck properBloodCheck = this.GetProperBloodCheck(nowHpPercentage, lastHpPercentage);
            if (properBloodCheck == null)
            {
                return false;
            }
            if ((base._nowTime - this.LastUseSkillTime) >= AiConfig.MONSTER_PVE_AI_USE_RAND_SKILL_INTERVAL)
            {
                this._meMonsterVo.Controller.SkillController.MonsterUseSkillById(Convert.ToUInt32(properBloodCheck.skill_id), false, true, null);
                base.LastUseSkillTime = base._nowTime;
                base._stopTime = 0f;
            }
            else
            {
                AiControllerBase.CacheAiBehavior item = new AiControllerBase.CacheAiBehavior {
                    cacheType = 2,
                    skill_id = Convert.ToUInt32(properBloodCheck.skill_id)
                };
                base._CacheAiBehaviorList.Add(item);
            }
            return true;
        }

        private bool CheckUseNormalSkill()
        {
            if (this._mon_rand_skill != null)
            {
                MonRandomSkill monsterNormalSkillToUse = this.GetMonsterNormalSkillToUse();
                if (monsterNormalSkillToUse == null)
                {
                    return false;
                }
                if (((base._nowTime - this.LastUseSkillTime) >= AiConfig.MONSTER_PVE_AI_USE_RAND_SKILL_INTERVAL) && !this.CheckCacheAiBehavior())
                {
                    ActionDisplay target = base._currentTargetDisplay;
                    this._meMonsterVo.Controller.SkillController.MonsterUseSkillById(Convert.ToUInt32(monsterNormalSkillToUse.skill_id), false, true, target);
                    base.LastUseSkillTime = base._nowTime;
                    base._stopTime = 0f;
                    return true;
                }
            }
            return false;
        }

        private bool CheckUseSkillOnCycle()
        {
            if (this._cycle_check == null)
            {
                return false;
            }
            List<CycleCheck> list = this._cycle_check;
            bool flag = false;
            int count = this._cycle_check.Count;
            for (int i = 0; i < count; i++)
            {
                CycleCheck check = this._cycle_check[i];
                if (check.last_use_time == 0f)
                {
                    list[i].last_use_time = base._nowTime;
                }
                if (base._nowTime > (check.last_use_time + (((float) check.time) / 1000f)))
                {
                    if ((base._nowTime - this.LastUseSkillTime) >= AiConfig.MONSTER_PVE_AI_USE_RAND_SKILL_INTERVAL)
                    {
                        this._meMonsterVo.Controller.SkillController.MonsterUseSkillById(Convert.ToUInt32(check.skill_id), false, true, null);
                        list[i].last_use_time = base._nowTime;
                        base.LastUseSkillTime = base._nowTime;
                        base._stopTime = 0f;
                    }
                    else
                    {
                        AiControllerBase.CacheAiBehavior item = new AiControllerBase.CacheAiBehavior {
                            cacheType = 3,
                            skill_id = Convert.ToUInt32(check.skill_id),
                            idx = i
                        };
                        base._CacheAiBehaviorList.Add(item);
                    }
                    flag = true;
                }
            }
            this._cycle_check = list;
            return flag;
        }

        public bool CheckUseSkillOnDie()
        {
            if ((this._dead_check == null) || (this._dead_check.Count != 1))
            {
                return false;
            }
            this._meMonsterVo.IsUnbeatable = true;
            this._meMonsterVo.SetHp(1);
            DeadCheck check = this._dead_check[0];
            check.last_use_time = base._nowTime;
            base.LastUseSkillTime = base._nowTime;
            base.MeController.SkillController.MonsterUseSkillById((uint) check.skill_id, false, true, null);
            return true;
        }

        public override void CleanTargetDisplay()
        {
            if (base._currentTargetDisplay != null)
            {
                base._targetVo = null;
                base._currentTargetDisplay = null;
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                AiTargetManager.instance.MonsterUpdateTarget(base.MeController, null);
            }
        }

        private void DoAttack()
        {
            this.MonsterAiAttackBehaviour();
            base._stopTime = 0f;
        }

        protected override bool DoCacheAttack(AiControllerBase.CacheAiBehavior Elem)
        {
            if (base._nowTime >= (this.LastUseAttackTime + AiConfig.MONSTER_PVE_AI_ATTACK_INTERVAL))
            {
                this.DoAttack();
                return true;
            }
            return false;
        }

        protected override bool DoCacheSkill(AiControllerBase.CacheAiBehavior Elem)
        {
            if (base._nowTime >= (this.LastUseSkillTime + AiConfig.MONSTER_PVE_AI_USE_RAND_SKILL_INTERVAL))
            {
                this._meMonsterVo.Controller.SkillController.MonsterUseSkillById(Elem.skill_id, false, true, null);
                return true;
            }
            return false;
        }

        protected override bool DoCacheSkillAndUpdateLastUsedTime(AiControllerBase.CacheAiBehavior Elem)
        {
            if (base._nowTime >= (this.LastUseSkillTime + AiConfig.MONSTER_PVE_AI_USE_RAND_SKILL_INTERVAL))
            {
                this._meMonsterVo.Controller.SkillController.MonsterUseSkillById(Elem.skill_id, false, true, null);
                this._cycle_check[Elem.idx].last_use_time = base._nowTime;
                return true;
            }
            return false;
        }

        private void DoStartChase()
        {
            if (((this._meMonsterVo == null) || (this._sight_range == 0f)) || (((base._currentTargetDisplay == null) || (base._targetVo == null)) || (base._currentTargetDisplay.GoBase == null)))
            {
                this.CleanTargetDisplay();
            }
            else
            {
                Vector3 dest = DamageCheck.GetFinalDestinationBeforeChase(base._currentTargetDisplay.GoBase.transform.position, base._selfTransform, this._attack_range);
                this.DoStartChase(dest);
            }
        }

        private void DoStartChase(Vector3 dest)
        {
            ActionVo vo2 = new ActionVo {
                Destination = dest,
                IsAStarMove = true,
                ActionType = "run"
            };
            if (<>f__am$cache23 == null)
            {
                <>f__am$cache23 = delegate (BaseControler ms) {
                };
            }
            vo2.Preparework = <>f__am$cache23;
            vo2.Callback = delegate (BaseControler bc) {
                bc.IsMoveDirectly = false;
                bc.IsAStarMove = false;
                this.SetAiStatusButNotClearTarget(AiEnum.MonsterAiStatus.THINK);
            };
            ActionVo vo = vo2;
            base.MeController.AttackController.AddAttackList(vo, false, false);
            if (this._meLastPos == base.transform.position)
            {
                this._walkFailCount++;
                base._stopTime += Time.deltaTime;
            }
            else
            {
                base._stopTime = 0f;
                this._walkFailCount = 0;
                this._meLastPos = base.transform.position;
            }
        }

        [DebuggerHidden]
        private IEnumerator DoTurnAround()
        {
            return new <DoTurnAround>c__Iterator44 { <>f__this = this };
        }

        private void DoUpdateTarget()
        {
            <DoUpdateTarget>c__AnonStorey106 storey = new <DoUpdateTarget>c__AnonStorey106();
            if ((base._currentTargetDisplay != null) && (base._targetVo != null))
            {
                storey.currentChasePlayerId = base._targetVo.Id;
                if (<>f__am$cache24 == null)
                {
                    <>f__am$cache24 = display => new <>__AnonType7<PlayerDisplay, BaseRoleVo>(display, display.GetVo());
                }
                if (<>f__am$cache25 == null)
                {
                    <>f__am$cache25 = <>__TranspIdent8 => <>__TranspIdent8.display;
                }
                IEnumerator<PlayerDisplay> enumerator = AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType7<PlayerDisplay, BaseRoleVo>>(<>f__am$cache24).Where<<>__AnonType7<PlayerDisplay, BaseRoleVo>>(new Func<<>__AnonType7<PlayerDisplay, BaseRoleVo>, bool>(storey.<>m__136)).Select<<>__AnonType7<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cache25).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        PlayerDisplay current = enumerator.Current;
                        base._currentTargetDisplay = current;
                        return;
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        private void EnterStateBeginComingBackToBornPos()
        {
            this.StartBackToBornPos();
            if (this._meMonsterVo.CurHp < this._meMonsterVo.Hp)
            {
                vp_Timer.In(1f, new vp_Timer.Callback(this.AddHp), 0, 1f, base.timeHandle);
            }
            this.CleanTargetDisplay();
            this._isBackingToBornPos = true;
            base._canDisrupt = false;
        }

        private void EnterStateComingBackToBornPos()
        {
            bool flag = this.CheckIsInBornPos();
            if (flag && this._isBackingToBornPos)
            {
                this.AddHp(true);
                this.RemoveAllBuffs();
                base.MeController.Me.ChangeDire(this._meMonsterVo.rotateY);
            }
            if (flag || base._canDisrupt)
            {
                base._canDisrupt = true;
                this._isBackingToBornPos = false;
                vp_Timer.CancelTimerByHandle(base.timeHandle);
            }
            else
            {
                this.StartBackToBornPos();
            }
        }

        private void EnterStateInFight(float distance, uint hpPrevious, uint hpNow, uint hpFull)
        {
            base._loop_interval = 0.5f;
            if ((!this.CheckUseBloodChangeSkill(hpNow, hpPrevious, hpFull) && !this.CheckUseSkillOnCycle()) && (!this.CheckUseNormalSkill() && this.CheckUseAttack(distance)))
            {
            }
        }

        private void EnterStateMonsterAttack(float distance, uint hpPrevious, uint hpNow, uint hpFull)
        {
            if ((!this.CheckUseBloodChangeSkill(hpNow, hpPrevious, hpFull) && !this.CheckUseSkillOnCycle()) && (!this.CheckUseNormalSkill() && this.CheckUseAttack(distance)))
            {
            }
        }

        private void EnterStateMonsterChasing(uint hpPrevious, uint hpNow, uint hpFull)
        {
            if (!this.CheckUseBloodChangeSkill(hpNow, hpPrevious, hpFull) && !this.CheckUseSkillOnCycle())
            {
                this.CheckStartChase();
            }
        }

        private void EnterStateMoveAround()
        {
            this.SetAiStatusButNotClearTarget(AiEnum.MonsterAiStatus.THINK);
            if ((this._meMonsterVo != null) && (this._sight_range != 0f))
            {
                this._keepRunningTime = 0f;
                Vector3 randomPosition = this.GetRandomPosition();
                this.DoStartChase(randomPosition);
            }
        }

        private void EnterStateRest()
        {
            this.SetAiStatusButNotClearTarget(AiEnum.MonsterAiStatus.THINK);
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            base._stopTime = 0f;
            this._keepRunningTime = 0f;
        }

        public override void ForceToStopAIAndAllBehavior()
        {
            base.ForceToStopAIAndAllBehavior();
            if (base.MeController.AttackController != null)
            {
                base.MeController.AttackController.ClearAttackList();
            }
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        private MonRandomSkill GetMonsterNormalSkillToUse()
        {
            float num = UnityEngine.Random.Range((float) 1f, (float) 10000f);
            int count = this._mon_rand_skill.Count;
            for (int i = 0; i < count; i++)
            {
                if (i == (count - 1))
                {
                    if (num <= this._mon_rand_skill[i].possiblity)
                    {
                        return this._mon_rand_skill[i];
                    }
                    return null;
                }
                if (num < this._mon_rand_skill[i].possiblity)
                {
                    return this._mon_rand_skill[i];
                }
                if ((num >= this._mon_rand_skill[i].possiblity) && (num < this._mon_rand_skill[i + 1].possiblity))
                {
                    return this._mon_rand_skill[i + 1];
                }
            }
            return null;
        }

        private BloodCheck GetProperBloodCheck(float nowHpPercentage, float lastHpPercentage)
        {
            if ((this._blood_check == null) || (this._blood_check.Count == 0))
            {
                return null;
            }
            List<BloodCheck> list = this._blood_check;
            BloodCheck check = null;
            int count = this._blood_check.Count;
            for (int i = 0; i < count; i++)
            {
                BloodCheck check2 = this._blood_check[i];
                if (((lastHpPercentage > check2.percent) && (nowHpPercentage <= check2.percent)) && (check2.last_use_time == 0f))
                {
                    list[i].last_use_time = base._nowTime;
                    check = check2;
                }
            }
            this._blood_check = list;
            return check;
        }

        private com.game.vo.RebornCheck GetProperRebornCheckWhenDie()
        {
            List<com.game.vo.RebornCheck> rebornCheck;
            if ((this._meMonsterVo.parent_template_id == 0) && (this._meMonsterVo.reborn_count == 0))
            {
                rebornCheck = ((MonsterAiController) base.MeController.AiController).RebornCheck;
                if ((rebornCheck != null) && (rebornCheck.Count == 1))
                {
                    return rebornCheck[0];
                }
                return null;
            }
            if ((this._meMonsterVo.parent_template_id == 0) || (this._meMonsterVo.reborn_count == 0))
            {
                return null;
            }
            rebornCheck = StringUtils.SplitStringToActionScript(BaseDataMgr.instance.getSysMonsterVo((uint) this._meMonsterVo.parent_template_id).action_script).reborn_check;
            if ((rebornCheck == null) || (rebornCheck.Count != 1))
            {
                return null;
            }
            com.game.vo.RebornCheck check2 = rebornCheck[0];
            check2.reborn_count = this._meMonsterVo.reborn_count;
            return check2;
        }

        private float GetRandomCruiseRange()
        {
            float num = UnityEngine.Random.Range((float) 0.5f, (float) 0.9f);
            return (this._cruise_range * num);
        }

        private Vector3 GetRandomPosition()
        {
            float num = UnityEngine.Random.Range((float) 0f, (float) 360f);
            float randomCruiseRange = this.GetRandomCruiseRange();
            return new Vector3 { x = (((float) Math.Sin((double) num)) * randomCruiseRange) + this._bornPos.x, y = base._selfTransform.position.y, z = (((float) Math.Cos((double) num)) * randomCruiseRange) + this._bornPos.z };
        }

        private float GetSqrDistanceFromBornPos()
        {
            return Util.XZSqrMagnitude(base._selfTransform, this._bornPos);
        }

        protected override void Init()
        {
            base.BaseRoleVo = this._meMonsterVo = base.MeController.Me.GetMeVoByType<MonsterVo>();
            base._selfTransform = base.transform;
            this._bornPos = base._selfTransform.position;
            this._is_active = this._meMonsterVo.MonsterVO.active;
            this._is_cruise = this._meMonsterVo.MonsterVO.is_cruise_enable;
            this._isBoss = this._meMonsterVo.MonsterVO.quality == 3;
            this._hpPrevious = this._meMonsterVo.lastHp;
            this._hpNow = this._meMonsterVo.CurHp;
            this._hpFull = this._meMonsterVo.Hp;
            this.SetMonsterAiActionScript(this._meMonsterVo);
            base.Init();
        }

        protected override void InitLoopInterval()
        {
            base._loop_interval = 0.2f;
            base._tick = UnityEngine.Random.Range(0f, base._loop_interval);
            base._stopTime = UnityEngine.Random.Range((float) 0f, (float) 1f);
        }

        protected override void InitRanges()
        {
            this._sight_range = this._meMonsterVo.MonsterVO.sight_range * 0.001f;
            this._attack_range = this._meMonsterVo.MonsterVO.attack_range * 0.001f;
            this._cruise_range = this._meMonsterVo.MonsterVO.cruise_range * 0.001f;
            this._max_range = this._meMonsterVo.MonsterVO.max_range * 0.001f;
            this._search_stop = this._meMonsterVo.MonsterVO.searchStop * 0.001f;
        }

        private bool IsNeedToComeBackToBornPos()
        {
            return (this.GetSqrDistanceFromBornPos() >= this._max_range.Square());
        }

        protected override Func<bool> IsNeedToSetNewTargetDisplay()
        {
            return delegate {
                if (this._isBackingToBornPos || this._isRunningToMaxRange)
                {
                    return false;
                }
                return (this._isCurrentTargetPlayerRandom || ((((base._currentTargetDisplay == null) || (base._targetVo == null)) || base._targetVo.IsEmptyHp) || ((base._targetVo.Camp == this._meMonsterVo.Camp) || (((this._isBoss && (base._currentTargetDisplay != null)) && (((base._nowTime - this.LastBeAttackedTime) > 3f) && (base.MeController.LastAttacker == base._currentTargetDisplay))) || ((this._isBoss && (base._currentTargetDisplay != null)) && ((base._nowTime - this.LastSkilledSuccessfullyTime) > 3f))))));
            };
        }

        private bool IsSleep()
        {
            return false;
        }

        public override void LookAt(ActionVo attackVo)
        {
            if ((base._currentTargetDisplay == null) || base._targetVo.IsEmptyHp)
            {
                base._selfTransform.LookAtTarget(attackVo.LookAtDestination.position);
                this.SetTargetDisplay(attackVo.Attacker);
            }
        }

        private void MonsterAiAttackBehaviour()
        {
            if (!this._meMonsterVo.IsEmptyHp && (base._currentTargetDisplay.GoBase != null))
            {
                if (!this._isBoss)
                {
                    base._selfTransform.LookAtTarget(base._currentTargetDisplay.GoBase.transform.position);
                }
                else
                {
                    base.StartCoroutine(this.DoTurnAround());
                }
                ActionVo vo = new ActionVo {
                    ActionType = "attack",
                    IsIgnoreDistance = true,
                    Target = base._currentTargetDisplay
                };
                base.MeController.AttackController.AddAttackList(vo, false, false);
            }
        }

        private void RemoveAllBuffs()
        {
            base.MeController.buffController.RemoveAllBuff();
        }

        private void SelectPlayersOutOfAttackRange(IEnumerable<ActionDisplay> fromList, ref List<ActionDisplay> targetList)
        {
            if (<>f__am$cache26 == null)
            {
                <>f__am$cache26 = display => new <>__AnonType8<ActionDisplay, BaseRoleVo>(display, display.GetMeVoByType<BaseRoleVo>());
            }
            if (<>f__am$cache27 == null)
            {
                <>f__am$cache27 = <>__TranspIdent9 => new <>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>(<>__TranspIdent9, !<>__TranspIdent9.vo.IsEmptyHp);
            }
            if (<>f__am$cache28 == null)
            {
                <>f__am$cache28 = <>__TranspIdent10 => new <>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>(<>__TranspIdent10, !<>__TranspIdent10.<>__TranspIdent9.vo.stateInfo.IsFrozen);
            }
            if (<>f__am$cache29 == null)
            {
                <>f__am$cache29 = <>__TranspIdent15 => ((!<>__TranspIdent15.<>__TranspIdent14.<>__TranspIdent13.<>__TranspIdent12.<>__TranspIdent11.<>__TranspIdent10.cond0 || !<>__TranspIdent15.<>__TranspIdent14.<>__TranspIdent13.<>__TranspIdent12.<>__TranspIdent11.cond1) || (!<>__TranspIdent15.<>__TranspIdent14.<>__TranspIdent13.<>__TranspIdent12.cond2 || !<>__TranspIdent15.<>__TranspIdent14.cond3)) ? false : <>__TranspIdent15.cond4;
            }
            if (<>f__am$cache2A == null)
            {
                <>f__am$cache2A = <>__TranspIdent15 => <>__TranspIdent15.<>__TranspIdent14.<>__TranspIdent13.<>__TranspIdent12.<>__TranspIdent11.<>__TranspIdent10.<>__TranspIdent9.display;
            }
            targetList.AddRange((from <>__TranspIdent13 in from <>__TranspIdent11 in fromList.Select<ActionDisplay, <>__AnonType8<ActionDisplay, BaseRoleVo>>(<>f__am$cache26).Select<<>__AnonType8<ActionDisplay, BaseRoleVo>, <>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>>(<>f__am$cache27).Select<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, <>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>>(<>f__am$cache28)
                select new <>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>(<>__TranspIdent11, <>__TranspIdent11.<>__TranspIdent10.<>__TranspIdent9.display != this._lastTargetDisplay) into <>__TranspIdent12
                select new <>__AnonType12<<>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, float>(<>__TranspIdent12, this.GetTargetSqrMagnitude(<>__TranspIdent12.<>__TranspIdent11.<>__TranspIdent10.<>__TranspIdent9.display))
                select new <>__AnonType13<<>__AnonType12<<>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, float>, bool>(<>__TranspIdent13, <>__TranspIdent13.distance <= this._sight_range.Square()) into <>__TranspIdent14
                select new <>__AnonType14<<>__AnonType13<<>__AnonType12<<>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, float>, bool>, bool>(<>__TranspIdent14, <>__TranspIdent14.<>__TranspIdent13.distance > this._attack_range.Square())).Where<<>__AnonType14<<>__AnonType13<<>__AnonType12<<>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, float>, bool>, bool>>(<>f__am$cache29).Select<<>__AnonType14<<>__AnonType13<<>__AnonType12<<>__AnonType11<<>__AnonType10<<>__AnonType9<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, float>, bool>, bool>, ActionDisplay>(<>f__am$cache2A));
        }

        private void SelectTargetNearAttackRange(IEnumerable<ActionDisplay> fromList, ref List<ActionDisplay> targetList)
        {
            if (<>f__am$cache2B == null)
            {
                <>f__am$cache2B = display => new <>__AnonType8<ActionDisplay, BaseRoleVo>(display, display.GetMeVoByType<BaseRoleVo>());
            }
            if (<>f__am$cache2C == null)
            {
                <>f__am$cache2C = <>__TranspIdent16 => new <>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>(<>__TranspIdent16, !<>__TranspIdent16.vo.IsEmptyHp);
            }
            if (<>f__am$cache2D == null)
            {
                <>f__am$cache2D = <>__TranspIdent18 => new <>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>(<>__TranspIdent18, !<>__TranspIdent18.<>__TranspIdent17.<>__TranspIdent16.vo.stateInfo.IsFrozen);
            }
            if (<>f__am$cache2E == null)
            {
                <>f__am$cache2E = <>__TranspIdent20 => ((!<>__TranspIdent20.<>__TranspIdent19.<>__TranspIdent18.<>__TranspIdent17.cond1 || !<>__TranspIdent20.<>__TranspIdent19.<>__TranspIdent18.cond2) || !<>__TranspIdent20.<>__TranspIdent19.cond3) ? false : <>__TranspIdent20.cond4;
            }
            if (<>f__am$cache2F == null)
            {
                <>f__am$cache2F = <>__TranspIdent20 => <>__TranspIdent20.<>__TranspIdent19.<>__TranspIdent18.<>__TranspIdent17.<>__TranspIdent16.display;
            }
            targetList.AddRange((from <>__TranspIdent19 in (from <>__TranspIdent17 in fromList.Select<ActionDisplay, <>__AnonType8<ActionDisplay, BaseRoleVo>>(<>f__am$cache2B).Select<<>__AnonType8<ActionDisplay, BaseRoleVo>, <>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>>(<>f__am$cache2C) select new <>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>(<>__TranspIdent17, this.GetTargetSqrMagnitude(<>__TranspIdent17.<>__TranspIdent16.display) <= this._attack_range.Square())).Select<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, <>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>>(<>f__am$cache2D) select new <>__AnonType18<<>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, bool>(<>__TranspIdent19, <>__TranspIdent19.<>__TranspIdent18.<>__TranspIdent17.<>__TranspIdent16.display != this._lastTargetDisplay)).Where<<>__AnonType18<<>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, bool>>(<>f__am$cache2E).Select<<>__AnonType18<<>__AnonType17<<>__AnonType16<<>__AnonType15<<>__AnonType8<ActionDisplay, BaseRoleVo>, bool>, bool>, bool>, bool>, ActionDisplay>(<>f__am$cache2F));
        }

        public override void SetAi(bool value)
        {
            base.SetAi(value);
            this.SetAiStatus(AiEnum.MonsterAiStatus.THINK);
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.MONSTER_PVE;
        }

        private void SetMonsterAiActionScript(MonsterVo MonsterVo)
        {
            ActionCheck check = StringUtils.SplitStringToActionScript(MonsterVo.MonsterVO.action_script);
            this._blood_check = check.blood_check;
            this._cycle_check = check.cycle_check;
            this._fight_cycle_check = check.fight_cycle_check;
            this._reborn_check = check.reborn_check;
            this._dead_check = check.dead_check;
            this._attacked_check = check.attacked_check;
            this._rely_check = check.rely_check;
            this._is_move = check.is_move;
            this._no_hart = check.no_hart;
            this._blood_broad = check.blood_broad;
            this._limit_tar = check.limit_tar;
            this._mon_rand_skill = StringUtils.GetMonRandSkill(MonsterVo.MonsterVO);
        }

        public override void SetTargetDisplay(ActionDisplay ad)
        {
            if (ad != null)
            {
                this._lastTargetDisplay = null;
                base._currentTargetDisplay = ad;
                this._isCurrentTargetPlayerRandom = false;
                base._targetVo = base._currentTargetDisplay.GetMeVoByType<BaseRoleVo>();
                base._targetLastCamp = base._targetVo.Camp;
                this.InformTargeted(base._currentTargetDisplay);
                AiTargetManager.instance.MonsterUpdateTarget(base.MeController, base._currentTargetDisplay.Controller as ActionControler);
                AiTargetManager.InformIdleMonsterEnterBattle(this._meMonsterVo.groupIndex, base._currentTargetDisplay);
            }
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            base.attackable_list.Clear();
            List<ActionDisplay> fromList = (from x in AppMap.Instance.playerList
                where x.GetMeVoByType<BaseRoleVo>().Camp != this._meMonsterVo.Camp
                select x).Cast<ActionDisplay>().ToList<ActionDisplay>();
            List<ActionDisplay> collection = (from x in AppMap.Instance.monsterList
                where x.GetMeVoByType<BaseRoleVo>().Camp != this._meMonsterVo.Camp
                select x).Cast<ActionDisplay>().ToList<ActionDisplay>();
            fromList.AddRange(collection);
            Func<ActionDisplay, float> keySelector = new Func<ActionDisplay, float>(this.GetTargetSqrMagnitude);
            this.SelectTargetNearAttackRange(fromList, ref this.attackable_list);
            if ((base.attackable_list != null) && (base.attackable_list.Count >= 1))
            {
                base.attackable_list = base.attackable_list.OrderBy<ActionDisplay, float>(keySelector).ToList<ActionDisplay>();
                if (!isTest)
                {
                    this.SetTargetDisplay(base.attackable_list[0]);
                }
                return true;
            }
            this.SelectPlayersOutOfAttackRange(fromList, ref this.attackable_list);
            if ((base.attackable_list == null) || (base.attackable_list.Count < 1))
            {
                if ((!isTest && (this._lastTargetDisplay != null)) && (!this._lastTargetDisplay.GetMeVoByType<BaseRoleVo>().IsEmptyHp && (this.GetTargetSqrMagnitude(this._lastTargetDisplay) <= this._sight_range.Square())))
                {
                    this.SetTargetDisplay(this._lastTargetDisplay);
                }
                return false;
            }
            if (!isTest)
            {
                base.attackable_list = base.attackable_list.OrderBy<ActionDisplay, float>(keySelector).ToList<ActionDisplay>();
                this.SetTargetDisplay(base.attackable_list[0]);
            }
            return true;
        }

        public void SetTargetDisplayButSkipInformingGroup(ActionDisplay ad)
        {
            if (ad != null)
            {
                this._lastTargetDisplay = null;
                base._currentTargetDisplay = ad;
                this._isCurrentTargetPlayerRandom = false;
                base._targetVo = base._currentTargetDisplay.GetMeVoByType<BaseRoleVo>();
                base._targetLastCamp = base._targetVo.Camp;
                this.InformTargeted(base._currentTargetDisplay);
                AiTargetManager.instance.MonsterUpdateTarget(base.MeController, base._currentTargetDisplay.Controller as ActionControler);
            }
        }

        private void StartBackToBornPos()
        {
            this.DoStartChase(this._bornPos);
            base._canDisrupt = false;
        }

        private void UpdateDebugFlags()
        {
        }

        private void UpdateTurnAroundParams(ref Vector3 enemyPos, ref Quaternion nowRotation, ref Vector3 deltaPos, ref Quaternion targetRotation)
        {
            nowRotation = base._selfTransform.rotation;
            if ((base._currentTargetDisplay == null) || (base._currentTargetDisplay.GoBase == null))
            {
                targetRotation = nowRotation;
            }
            else
            {
                enemyPos = base._currentTargetDisplay.GoBase.transform.position;
                deltaPos = enemyPos - base._selfTransform.position;
                deltaPos.y = 0f;
                targetRotation = Quaternion.LookRotation(deltaPos);
            }
        }

        public List<com.game.vo.RebornCheck> RebornCheck
        {
            get
            {
                return this._reborn_check;
            }
            set
            {
                this._reborn_check = value;
            }
        }

        [CompilerGenerated]
        private sealed class <DoTurnAround>c__Iterator44 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MonsterAiController <>f__this;
            internal Vector3 <deltaPos>__1;
            internal Vector3 <enemyPos>__0;
            internal Quaternion <nowRotation>__2;
            internal Quaternion <targetRotation>__3;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<enemyPos>__0 = Vector3.zero;
                        this.<deltaPos>__1 = Vector3.zero;
                        this.<nowRotation>__2 = new Quaternion();
                        this.<targetRotation>__3 = new Quaternion();
                        this.<>f__this.UpdateTurnAroundParams(ref this.<enemyPos>__0, ref this.<nowRotation>__2, ref this.<deltaPos>__1, ref this.<targetRotation>__3);
                        break;

                    case 1:
                        break;

                    case 2:
                        this.$PC = -1;
                        goto Label_0154;

                    default:
                        goto Label_0154;
                }
                if (Quaternion.Angle(this.<nowRotation>__2, this.<targetRotation>__3) > 3f)
                {
                    if (this.<>f__this.MeController.StatuController.CurrentStatu != 1)
                    {
                        this.<>f__this.MeController.StatuController.SetStatu(1);
                    }
                    this.<>f__this._selfTransform.rotation = Quaternion.Slerp(this.<>f__this._selfTransform.rotation, this.<targetRotation>__3, 5f * Time.deltaTime);
                    this.<>f__this.UpdateTurnAroundParams(ref this.<enemyPos>__0, ref this.<nowRotation>__2, ref this.<deltaPos>__1, ref this.<targetRotation>__3);
                    this.$current = null;
                    this.$PC = 1;
                }
                else
                {
                    this.$current = null;
                    this.$PC = 2;
                }
                return true;
            Label_0154:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <DoUpdateTarget>c__AnonStorey106
        {
            internal ulong currentChasePlayerId;

            internal bool <>m__136(<>__AnonType7<PlayerDisplay, BaseRoleVo> <>__TranspIdent8)
            {
                return (<>__TranspIdent8.baseRoleVo.Id == this.currentChasePlayerId);
            }
        }
    }
}

