﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.hud;
    using com.game.Public.Message;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.display.vo;
    using com.u3d.bases.fsmUtil;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    internal class PlayerAiController : AiControllerBase
    {
        private Transform _anotherTransform;
        private PlayerAiController _antoherAi;
        protected int _continuousSkillCount;
        protected bool _isInRecovery;
        protected bool _isMaster;
        protected bool _isNeedClearHpWarning = true;
        protected bool _isNeedContinueAtk;
        protected bool _isNeedRefreshTarget;
        private bool _isReady;
        private AiEnum.PlayerAiStatus _masteAiStatus;
        private PlayerAiController _masterAi;
        private Vector3 _masterDest;
        private PlayerVo _masterPlayerVo;
        private Vector3 _masterPos;
        private ActionDisplay _masterTargetDisplay;
        private Transform _masterTransform;
        protected Vector3 _meLastPos;
        protected PlayerVo _mePlayerVo;
        protected int _restPriorityAtk;
        protected int _roleJob;
        protected float _sightRange;
        protected byte _walkFailCount;
        [CompilerGenerated]
        private static Func<ActionDisplay, bool> <>f__am$cache1C;
        [CompilerGenerated]
        private static Func<ActionDisplay, <>__AnonType19<ActionDisplay, MonsterVo>> <>f__am$cache1D;
        [CompilerGenerated]
        private static Func<<>__AnonType19<ActionDisplay, MonsterVo>, bool> <>f__am$cache1E;
        [CompilerGenerated]
        private static Func<<>__AnonType19<ActionDisplay, MonsterVo>, bool> <>f__am$cache1F;
        [CompilerGenerated]
        private static Func<<>__AnonType19<ActionDisplay, MonsterVo>, ActionDisplay> <>f__am$cache20;
        [CompilerGenerated]
        private static Func<ActionDisplay, <>__AnonType20<ActionDisplay, BaseRoleVo>> <>f__am$cache21;
        [CompilerGenerated]
        private static Func<<>__AnonType20<ActionDisplay, BaseRoleVo>, bool> <>f__am$cache22;
        [CompilerGenerated]
        private static Func<<>__AnonType20<ActionDisplay, BaseRoleVo>, bool> <>f__am$cache23;
        [CompilerGenerated]
        private static Func<<>__AnonType20<ActionDisplay, BaseRoleVo>, ActionDisplay> <>f__am$cache24;
        [CompilerGenerated]
        private static Func<AiControllerBase.KV, float> <>f__am$cache25;
        [CompilerGenerated]
        private static Func<PlayerDisplay, PlayerAiController> <>f__am$cache26;
        public bool BeTargeted;
        public float disToMaster;
        public float disToTarget;
        protected float nextMoveRadius;
        private SkillController skillController;
        public ActionDisplay TargetDisplay;

        protected override void AiStateDecision(float timeElapsed)
        {
            this.UpdateDebugFlags();
            Log.AI(base.gameObject, " AiStateDecision Player ");
            if ((base._currentTargetDisplay != null) && this._isNeedContinueAtk)
            {
            }
            base._stopTime += timeElapsed;
            Log.AI(base.gameObject, " PlayerAiController Stop ");
        }

        private void AutoAttack()
        {
            Log.Net("Auto Ai Is Master " + this._isMaster);
            Log.Net("Set Auto Attack");
            this.SetAi(!this.IsAiEnable);
        }

        public override void BeAttacked(ActionVo attackVo, float time = 0)
        {
            base.BeAttacked(attackVo, time);
            if (this.IsAiEnable && ((base._ai_status == 11) || (base._ai_status == 10)))
            {
                this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }
        }

        public void CancelAddHp()
        {
            HudView.Instance.stopRecoverBlood(this._mePlayerVo.Controller.GoHp);
            vp_Timer.CancelTimerByHandle(base.timeHandle);
            this.SetInRecovery(false);
        }

        protected override bool CheckCanUseAi()
        {
            if (this._mePlayerVo.stateInfo.ShouldPauseAI)
            {
                return false;
            }
            bool isAiEnable = this.IsAiEnable;
            bool flag2 = !this.CheckIsDead();
            bool flag3 = !this._mePlayerVo.IsEmptyHp;
            return ((isAiEnable && flag2) && flag3);
        }

        protected bool CheckIsDoingAttack()
        {
            return StatuControllerBase.IsStatusHashDoingAttack(base._nowStatusHash);
        }

        protected override bool CheckIsDoingSkill()
        {
            bool flag = StatuControllerBase.IsStatusHashDoingSkill(base._nowStatusHash);
            bool flag2 = StatuControllerBase.IsStatusHashInHurtCannotMove(base._nowStatusHash);
            return (flag || flag2);
        }

        private bool CheckIsRunningToSomewhere()
        {
            return (base._ai_status == 11);
        }

        protected override bool CheckShouldSkipThink()
        {
            if (!this._isMaster && !this.IsInBattle)
            {
                if (!this.IsNearMaster())
                {
                    this.MoveToMaster();
                }
                return true;
            }
            if ((!this._isMaster || this.IsInBattle) || !this._isInRecovery)
            {
                return this.CheckIsDoingSkill();
            }
            if (PlayerAiManager.instance.IsAllMemberHpEnoughToAi())
            {
                this.SetInRecovery(false);
                return false;
            }
            return true;
        }

        protected bool CheckUseAttack()
        {
            if (base._targetVo.IsEmptyHp || (base._currentTargetDisplay.GoBase == null))
            {
                this.ResetContinuousAtk();
                return true;
            }
            float targetDistance = this.GetTargetDistance((ActionDisplay) null);
            bool flag = false;
            if (targetDistance <= this.AtkRange)
            {
                flag = true;
                if ((base._nowTime - this.LastUseAttackTime) >= 0f)
                {
                    this.DoAttack();
                    base._stopTime = 0f;
                    base.LastUseAttackTime = base._nowTime;
                    return flag;
                }
                AiControllerBase.CacheAiBehavior item = new AiControllerBase.CacheAiBehavior {
                    cacheType = 1,
                    lastDisplay = base._currentTargetDisplay
                };
                base._CacheAiBehaviorList.Add(item);
            }
            return flag;
        }

        protected bool CheckUseNormalSkill()
        {
            if ((this._continuousSkillCount > AiConfig.MAX_CONTINUOUS_USE_SKILL_COUNT) || (this._restPriorityAtk > 0))
            {
                return false;
            }
            if ((base._currentTargetDisplay.Controller == null) || base._targetVo.IsEmptyHp)
            {
                this._isNeedRefreshTarget = true;
                return true;
            }
            Vector3 position = base._currentTargetDisplay.Controller.transform.position;
            float boundDistance = base._currentTargetDisplay.BoundDistance;
            bool flag = false;
            int num2 = this.skillController.LearnedSkillList.Count<uint>();
            for (int i = 4; i < num2; i++)
            {
                switch (i)
                {
                    case 7:
                    case 3:
                    case 8:
                        break;

                    default:
                    {
                        uint id = this.skillController.LearnedSkillList[i];
                        if ((id != 0) && this.skillController.IsSkillCdReady(i))
                        {
                            bool flag2;
                            SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(id);
                            SysSkillActionVo sysSkillActionVo = BaseDataMgr.instance.GetSysSkillActionVo(sysSkillBaseVo.skill_group);
                            float targetDistance = this.GetTargetDistance((ActionDisplay) null);
                            float num6 = sysSkillBaseVo.available_atk_range * 0.001f;
                            float num7 = base.MeController.ExtendedBulletFlyDistance * 0.001f;
                            if (sysSkillActionVo.IsBullet)
                            {
                                flag2 = targetDistance <= (num6 + num7);
                            }
                            else
                            {
                                if (targetDistance > num6)
                                {
                                    break;
                                }
                                flag2 = DamageCheck.IsSkillCovered3D(sysSkillBaseVo, base._selfTransform, position, boundDistance);
                            }
                            if (flag2)
                            {
                                flag = true;
                                if (((base._nowTime - this.LastUseSkillTime) >= 0f) && !this.CheckCacheAiBehavior())
                                {
                                    ActionDisplay targetDisplay = base._currentTargetDisplay;
                                    this.skillController.RequestUseSkill(i, true, false, null, null, targetDisplay);
                                    base.LastUseSkillTime = base._nowTime;
                                    base._stopTime = 0f;
                                }
                                else
                                {
                                    AiControllerBase.CacheAiBehavior item = new AiControllerBase.CacheAiBehavior {
                                        lastDisplay = base._currentTargetDisplay,
                                        cacheType = 0x63,
                                        skill_id = id,
                                        idx = i
                                    };
                                    base._CacheAiBehaviorList.Add(item);
                                }
                                goto Label_0222;
                            }
                        }
                        break;
                    }
                }
            }
        Label_0222:
            if (flag)
            {
                this._continuousSkillCount++;
            }
            return flag;
        }

        public override void CleanTargetDisplay()
        {
            Log.AI(base.gameObject, " CleanTargetDisplay " + base._currentTargetDisplay);
            base._currentTargetDisplay = null;
            base._targetVo = null;
            AiTargetManager.instance.MeUpdateTarget(base.MeController, null);
        }

        public static void ClickAutoAttack()
        {
            Log.Net("Click AutoAi Button");
            PlayerAiController aiController = AppMap.Instance.me.Controller.AiController as PlayerAiController;
            if (aiController == null)
            {
                Log.Net("Can't Auto Attack");
                MessageManager.Show("该场景不能自动战斗");
            }
            else
            {
                Log.Net(" Goto Auto Attack");
                aiController.AutoAttack();
            }
        }

        public static void DisableAIAfterTranscation(AiControllerBase ai)
        {
            PlayerAiController controller = ai as PlayerAiController;
            if (controller != null)
            {
                controller.StopAiBehavoirRightNow();
            }
        }

        private void DoAddHp()
        {
            this.DoAddHp(false);
        }

        private void DoAddHp(bool isFullRestore)
        {
            if (DungeonMgr.Instance.curMissionSysVo == null)
            {
                vp_Timer.CancelTimerByHandle(base.timeHandle);
            }
            else
            {
                float num = !isFullRestore ? AiConfig.ROLE_RECOVERY_PERCENT : 1000f;
                float delta = this._mePlayerVo.Hp * (num * 0.001f);
                if (delta > this._mePlayerVo.DeltaToFullHp)
                {
                    delta = this._mePlayerVo.DeltaToFullHp;
                    vp_Timer.CancelTimerByHandle(base.timeHandle);
                }
                this._mePlayerVo.AddHp(delta);
            }
        }

        private void DoAttack()
        {
            this.PlayerAiAttackBehaviour();
            base._stopTime = 0f;
        }

        protected override bool DoCacheAttack(AiControllerBase.CacheAiBehavior Elem)
        {
            if (base._nowTime >= this.LastUseSkillTime)
            {
                this.DoAttack();
                return true;
            }
            return false;
        }

        protected override bool DoCacheSkill(AiControllerBase.CacheAiBehavior Elem)
        {
            if (base._nowTime < this.LastUseSkillTime)
            {
                return false;
            }
            if (((Elem.lastDisplay != null) && (Elem.lastDisplay.Controller != null)) && !Elem.lastDisplay.GetMeVoByType<BaseRoleVo>().IsEmptyHp)
            {
                ActionDisplay lastDisplay = Elem.lastDisplay;
                this._mePlayerVo.Controller.SkillController.RequestUseSkill(Elem.idx, true, false, null, null, lastDisplay);
            }
            return true;
        }

        protected void DoStartChaseASTAR(Vector3 dest, bool isChaseEnemy = true)
        {
            <DoStartChaseASTAR>c__AnonStorey107 storey = new <DoStartChaseASTAR>c__AnonStorey107 {
                isChaseEnemy = isChaseEnemy,
                <>f__this = this
            };
            if (this._mePlayerVo.stateInfo.CanMove)
            {
                ActionVo vo = new ActionVo {
                    Destination = dest,
                    ActionType = "run",
                    IsAStarMove = true,
                    Preparework = new PreMoveCallBack(storey.<>m__14B),
                    Callback = new MoveEndCallback(storey.<>m__14C)
                };
                base.MeController.AttackController.AddAttackList(vo, false, false);
                base._stopTime = 0f;
            }
        }

        public override void ForceToStopAIAndAllBehavior()
        {
            this.SetAi(false);
            if ((base.MeController.AttackController != null) && (base.MeController.AttackController.AttackList != null))
            {
                base.MeController.AttackController.ClearAttackList();
            }
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        public PlayerAiController GetAnotherAi()
        {
            return this._antoherAi;
        }

        private float GetMasterDistance()
        {
            return this.GetTargetDistance(this._masterTransform);
        }

        protected Vector3 GetMoveDestinationToEnemy()
        {
            Log.AI(base.gameObject, " Move Destination " + base._currentTargetDisplay);
            return DamageCheck.GetFinalDestinationBeforeChase(base._currentTargetDisplay.GoBase.transform.position, base._selfTransform, this.AtkRange);
        }

        [Obsolete]
        protected Vector3 GetMoveDestinationToEnemyWhenAway()
        {
            return DamageCheck.GetFinalDestinationBeforeChase(base._currentTargetDisplay.GoBase.transform.position, base._selfTransform.position, this.AtkRange);
        }

        private Vector3 GetMoveDestinationToMaster()
        {
            float num;
            if (this._roleJob != 2)
            {
                num = UnityEngine.Random.Range((float) 135f, (float) 225f);
                this.nextMoveRadius = AiConfig.RANGED_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE * 0.001f;
            }
            else
            {
                num = UnityEngine.Random.Range((float) -45f, (float) 45f);
                this.nextMoveRadius = AiConfig.MELEE_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE * 0.001f;
            }
            this.nextMoveRadius *= 0.8f;
            Vector3 eulerAngles = this._masterTransform.rotation.eulerAngles;
            Vector3 vector2 = (Vector3) ((Quaternion.Euler(eulerAngles.x, eulerAngles.y + num, eulerAngles.z) * Vector3.forward) * this.nextMoveRadius);
            return (this._masterPos + vector2);
        }

        private void GoToDoor()
        {
            if (this._isMaster)
            {
                CheckVo.DoorPosition doorPosition = AppMap.Instance.checkVo.GetDoorPosition();
                if (!doorPosition.isNull)
                {
                    this.DoStartChaseASTAR(doorPosition.position, true);
                }
            }
        }

        private void HandleAtkCounter()
        {
            if (--this._restPriorityAtk <= 0)
            {
                if (this._isNeedContinueAtk)
                {
                    this._isNeedContinueAtk = false;
                    this._restPriorityAtk = 0;
                }
                else if (!this._isNeedContinueAtk)
                {
                    this._isNeedContinueAtk = true;
                    this._continuousSkillCount = 0;
                    this._restPriorityAtk = 2;
                }
            }
        }

        protected override void Init()
        {
            base.BaseRoleVo = this._mePlayerVo = base.MeController.Me.GetMeVoByType<PlayerVo>();
            this.skillController = base.MeController.SkillController;
            this._roleJob = this._mePlayerVo.job;
            base._selfTransform = base.transform;
            base.Init();
            this.SetMaster(this._isMaster);
        }

        private void InitAllTeamAiInfo(IEnumerable<PlayerDisplay> displayList, bool isOnFightAiEnable)
        {
            if (!base._isInitialized)
            {
                this.Init();
                base._isInitialized = true;
            }
            Log.Net("Initial Master And Member Ai");
            PlayerDisplay me = AppMap.Instance.me;
            IEnumerator<PlayerDisplay> enumerator = displayList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    if (current == me)
                    {
                        this.InitMasterInfo(me, isOnFightAiEnable);
                    }
                    else if (current != (base.MeController.Me as PlayerDisplay))
                    {
                        this.InitAnotherMemberInfo(current);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void InitAnotherMemberInfo(PlayerDisplay display)
        {
            this._antoherAi = display.Controller.AiController as PlayerAiController;
            this._anotherTransform = display.Controller.transform;
        }

        protected override void InitLoopInterval()
        {
            base._loop_interval = 0.15f;
            base._stopTime = 0f;
            base._tick = 0f;
        }

        private void InitMasterInfo(PlayerDisplay masterDisplay, bool isOnFightAiEnable)
        {
            Log.Net("Init Master Info AI");
            this._masterAi = masterDisplay.Controller.AiController as PlayerAiController;
            this._masterTargetDisplay = this._masterAi.MasterTargetDisplay;
            this._masterPlayerVo = masterDisplay.GetMeVoByType<PlayerVo>();
            this._masterTransform = masterDisplay.Controller.transform;
            this._masterPos = this._masterTransform.position;
            this._isReady = true;
            if (this._masterPlayerVo.Id == base.MeController.Me.GetVo().Id)
            {
                this.SetMaster(true);
                this.SetAi(isOnFightAiEnable);
            }
            else
            {
                this.SetMaster(false);
                this.SetAi(true);
            }
        }

        public static void InitPlayerAi()
        {
            Log.Net("InitalPlayerAi");
            if (GlobalData.isInCopy)
            {
                AiControllerBase aiController = AppMap.Instance.me.Controller.AiController;
                InitPlayerAi((aiController != null) && aiController.IsAiEnable);
            }
        }

        public static void InitPlayerAi(bool isOnFightAiEnable)
        {
            if (!AppMap.Instance.IsInArena)
            {
                Log.AI(null, "Initial All TeamAi");
                if (<>f__am$cache26 == null)
                {
                    <>f__am$cache26 = player => player.Controller.AiController as PlayerAiController;
                }
                IEnumerator<PlayerAiController> enumerator = AppMap.Instance.SelfplayerList.Select<PlayerDisplay, PlayerAiController>(<>f__am$cache26).GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        PlayerAiController current = enumerator.Current;
                        Log.AI(current, " playerList " + AppMap.Instance + " Player Controller ");
                        if (current != null)
                        {
                            current.InitAllTeamAiInfo(AppMap.Instance.SelfplayerList, isOnFightAiEnable);
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        protected override void InitRanges()
        {
            this._sightRange = this._mePlayerVo.generalTemplateInfo.ai_sight_range * 0.001f;
        }

        public bool IsInRecovery()
        {
            return this._isInRecovery;
        }

        public bool IsMaster()
        {
            return this._isMaster;
        }

        private bool IsNearMaster()
        {
            float num;
            if (this._isMaster)
            {
                return true;
            }
            base.IsNearDestination(this._masterPos, out num);
            return (num < (AiConfig.NEAR_THE_MASTER_GAP * 0.001f));
        }

        protected override Func<bool> IsNeedToSetNewTargetDisplay()
        {
            return delegate {
                if (this._isNeedRefreshTarget)
                {
                    this._isNeedRefreshTarget = false;
                    this.ResetContinuousAtk();
                    return true;
                }
                if (!GlobalData.isInCopy)
                {
                    return false;
                }
                if (((base._currentTargetDisplay == null) || (base._targetVo == null)) || base._targetVo.IsEmptyHp)
                {
                    this.ResetContinuousAtk();
                    return true;
                }
                return (base._targetVo.Camp == this._mePlayerVo.Camp);
            };
        }

        private void MoveToAnotherAiTarget(ActionDisplay display)
        {
        }

        private void MoveToMaster()
        {
        }

        private void MoveToMastersTarget(ActionDisplay display)
        {
        }

        protected virtual void MoveToTarget(bool isAway = true)
        {
            Log.AI(base.gameObject, " MoveToTarget " + base._currentTargetDisplay);
        }

        public void OnlyStopAi()
        {
            Log.AI(null, "OnlyStopAi Here");
            if (this._isMaster)
            {
                Singleton<BattleTopLeftView>.Instance.updateAutoEffect(false);
            }
            this._isNeedRefreshTarget = true;
            base.MeController.IsMoveDirectly = false;
            base.MeController.IsAStarMove = false;
            base._lastIsAiEnable = false;
            base.SetAi(false);
            AiTargetManager.instance.MeUpdateTarget(base.MeController, null);
            this.SetAiStatus(AiEnum.PlayerAiStatus.THINK);
        }

        private void PlayerAiAttackBehaviour()
        {
            if (((this._mePlayerVo == null) || (base._currentTargetDisplay.Controller == null)) || base._targetVo.IsEmptyHp)
            {
                this._isNeedRefreshTarget = true;
            }
            else
            {
                ActionVo vo = new ActionVo {
                    ActionType = "attack",
                    IsIgnoreDistance = true,
                    Target = base._currentTargetDisplay
                };
                base._selfTransform.LookAtTarget(base._currentTargetDisplay);
                base.MeController.AttackController.AddAttackList(vo, false, true);
                base.MeController.AttackController.AddAttackList(vo, false, true);
                base.MeController.AttackController.AddAttackList(vo, false, true);
            }
        }

        protected override void PrepareWork()
        {
            if ((this.IsInBattle && (this._mePlayerVo.CurHp > 0)) && (base._ai_type == AiEnum.AiScriptType.PLAYER_PVE))
            {
                this.PlayerEndBattle();
            }
        }

        private void ProperMove()
        {
            Log.AI(base.gameObject, " 移动到合适的目标 " + base._currentTargetDisplay);
            if ((!this._isMaster && (base._currentTargetDisplay == this._masterTargetDisplay)) && (this._masterTargetDisplay != null))
            {
                this.MoveToTarget(true);
            }
            else if ((!this._isMaster && !this.IsNearMaster()) && (this.GetTargetDistance((ActionDisplay) null) > (this.GetMasterDistance() * 1.5f)))
            {
                this.MoveToMaster();
            }
            else
            {
                this.MoveToTarget(true);
            }
        }

        protected void ResetAiCounters()
        {
            this._isNeedContinueAtk = false;
            this._restPriorityAtk = 0;
            this._walkFailCount = 0;
            base._stopTime = 0f;
        }

        protected void ResetContinuousAtk()
        {
            base.MeController.AttackController.AttackList.Clear();
            this._isNeedContinueAtk = false;
            this._restPriorityAtk = 0;
        }

        public override void SetAi(bool value)
        {
            Log.AI(base.gameObject, "SetAi Here who " + value);
            if (this._isMaster)
            {
                Singleton<BattleTopLeftView>.Instance.updateAutoEffect(value);
            }
            base._lastIsAiEnable = this.IsAiEnable;
            base.SetAi(value);
            this._isNeedRefreshTarget = true;
            base.MeController.IsMoveDirectly = false;
            base.MeController.IsAStarMove = false;
            this.SetAiStatus(AiEnum.PlayerAiStatus.THINK);
            AiTargetManager.instance.MeUpdateTarget(base.MeController, null);
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.PLAYER_PVE;
        }

        protected override void SetAiStatus(AiEnum.PlayerAiStatus ai_status = 0)
        {
            Log.AI(base.gameObject, " SetAiStatus " + ai_status);
            base.SetAiStatus(ai_status);
            this._isNeedContinueAtk = false;
            this._restPriorityAtk = 0;
        }

        private void SetInRecovery(bool r)
        {
            this._isInRecovery = r;
            if (this._isInRecovery)
            {
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
        }

        private void SetMaster(bool m)
        {
            Log.AI(base.gameObject, " SetMaster " + m);
            this._isMaster = m;
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        public override void SetTargetDisplay(ActionDisplay ad)
        {
            if (ad != null)
            {
                base._currentTargetDisplay = ad;
                base._targetVo = ad.GetMeVoByType<BaseRoleVo>();
                base._targetLastCamp = base._targetVo.Camp;
                AiTargetManager.instance.MeUpdateTarget(base.MeController, base._currentTargetDisplay.Controller as ActionControler);
            }
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            base.attackable_list.Clear();
            if (<>f__am$cache1C == null)
            {
                <>f__am$cache1C = display => display.Controller != null;
            }
            if (<>f__am$cache1D == null)
            {
                <>f__am$cache1D = display => new <>__AnonType19<ActionDisplay, MonsterVo>(display, display.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cache1E == null)
            {
                <>f__am$cache1E = <>__TranspIdent21 => !<>__TranspIdent21.monsterVo.IsEmptyHp;
            }
            if (<>f__am$cache1F == null)
            {
                <>f__am$cache1F = <>__TranspIdent21 => <>__TranspIdent21.monsterVo.MonsterVO.type == 1;
            }
            if (<>f__am$cache20 == null)
            {
                <>f__am$cache20 = <>__TranspIdent21 => <>__TranspIdent21.display;
            }
            base.attackable_list.AddRange((from display in AppMap.Instance.monsterList.Cast<ActionDisplay>()
                where display.GetMeVoByType<BaseRoleVo>().Camp != this._mePlayerVo.Camp
                select display).Where<ActionDisplay>(<>f__am$cache1C).Select<ActionDisplay, <>__AnonType19<ActionDisplay, MonsterVo>>(<>f__am$cache1D).Where<<>__AnonType19<ActionDisplay, MonsterVo>>(<>f__am$cache1E).Where<<>__AnonType19<ActionDisplay, MonsterVo>>(<>f__am$cache1F).Select<<>__AnonType19<ActionDisplay, MonsterVo>, ActionDisplay>(<>f__am$cache20));
            if (<>f__am$cache21 == null)
            {
                <>f__am$cache21 = display => new <>__AnonType20<ActionDisplay, BaseRoleVo>(display, display.GetMeVoByType<BaseRoleVo>());
            }
            if (<>f__am$cache22 == null)
            {
                <>f__am$cache22 = <>__TranspIdent22 => !<>__TranspIdent22.nowDisplay.IsEmptyHp && (<>__TranspIdent22.nowDisplay.Controller != null);
            }
            if (<>f__am$cache23 == null)
            {
                <>f__am$cache23 = <>__TranspIdent22 => <>__TranspIdent22.nowDisplay.SubType != 1;
            }
            if (<>f__am$cache24 == null)
            {
                <>f__am$cache24 = <>__TranspIdent22 => <>__TranspIdent22.display;
            }
            base.attackable_list.AddRange((from <>__TranspIdent22 in AppMap.Instance.playerList.Cast<ActionDisplay>().Select<ActionDisplay, <>__AnonType20<ActionDisplay, BaseRoleVo>>(<>f__am$cache21)
                where <>__TranspIdent22.nowDisplay.Camp != this._mePlayerVo.Camp
                select <>__TranspIdent22).Where<<>__AnonType20<ActionDisplay, BaseRoleVo>>(<>f__am$cache22).Where<<>__AnonType20<ActionDisplay, BaseRoleVo>>(<>f__am$cache23).Select<<>__AnonType20<ActionDisplay, BaseRoleVo>, ActionDisplay>(<>f__am$cache24));
            List<AiControllerBase.KV> source = new List<AiControllerBase.KV>();
            foreach (ActionDisplay display in base.attackable_list)
            {
                float targetDistance = this.GetTargetDistance(display);
                if (this._isMaster || (targetDistance <= this._sightRange))
                {
                    source.Add(new AiControllerBase.KV(display, targetDistance));
                }
            }
            if (source.Count < 1)
            {
                this.CleanTargetDisplay();
                return false;
            }
            if (<>f__am$cache25 == null)
            {
                <>f__am$cache25 = x => x.distance;
            }
            source = source.OrderBy<AiControllerBase.KV, float>(<>f__am$cache25).ToList<AiControllerBase.KV>();
            this.SetTargetDisplay(source[0].ad);
            this.ResetAiCounters();
            return true;
        }

        public bool StartAddHp()
        {
            if ((this._mePlayerVo.CurHp < this._mePlayerVo.Hp) && (this._mePlayerVo.CurHp > 0))
            {
                vp_Timer.In(1f, new vp_Timer.Callback(this.DoAddHp), 0, 1f, base.timeHandle);
                this._isNeedClearHpWarning = true;
                this.SetInRecovery(true);
                return true;
            }
            return false;
        }

        private void StartChase(Vector3 dest, bool isChaseEnemy = true)
        {
            Log.AI(base.gameObject, " StartChase " + dest);
            if (this._meLastPos == base.transform.position)
            {
                this._walkFailCount = (byte) (this._walkFailCount + 1);
            }
            else
            {
                this._walkFailCount = 0;
                this._meLastPos = base.transform.position;
            }
            this.DoStartChaseASTAR(dest, isChaseEnemy);
        }

        public void StopAiBehavoirRightNow()
        {
            bool isMoveDirectly = base.MeController.IsMoveDirectly;
            bool isAStarMove = base.MeController.IsAStarMove;
            if ((this.IsAiEnable || isMoveDirectly) || isAStarMove)
            {
                this.OnlyStopAi();
                this._isNeedRefreshTarget = true;
                base.MeController.IsAStarMove = false;
                base.MeController.IsMoveDirectly = false;
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
        }

        protected override void Targeted()
        {
            PlayerAiManager.instance.BeginBattle();
        }

        private void UpdateDebugFlags()
        {
        }

        private void UpdateMasterRelated()
        {
            if (!this._isMaster && (this._masterAi != null))
            {
                this._masterPos = this._masterTransform.position;
                this._masterTargetDisplay = this._masterAi.MasterTargetDisplay;
                this._masteAiStatus = (AiEnum.PlayerAiStatus) this._masterAi.GetAiStatus();
            }
        }

        protected float AtkRange
        {
            get
            {
                return ((this.skillController.GetAttack1AvailableAtkRange() * 0.001f) * 0.95f);
            }
        }

        public PlayerAiController MasterAi
        {
            get
            {
                if (this._isMaster)
                {
                    return this;
                }
                return null;
            }
        }

        public ActionDisplay MasterTargetDisplay
        {
            get
            {
                if (!this._isMaster)
                {
                    return null;
                }
                if (this.IsAiEnable)
                {
                    return base._currentTargetDisplay;
                }
                ActionDisplay lastTarget = this.skillController.LastTarget;
                if (lastTarget != null)
                {
                    return lastTarget;
                }
                return base.MeController.LastAttacker;
            }
        }

        [CompilerGenerated]
        private sealed class <DoStartChaseASTAR>c__AnonStorey107
        {
            internal PlayerAiController <>f__this;
            internal bool isChaseEnemy;

            internal void <>m__14B(BaseControler mc)
            {
                this.<>f__this.SetAiStatus(!this.isChaseEnemy ? AiEnum.PlayerAiStatus.RUN_TO_MASTER : AiEnum.PlayerAiStatus.RUN_TO_TARGET);
                Log.AI(this.<>f__this.gameObject, " set AiStatus");
            }

            internal void <>m__14C(BaseControler bc)
            {
                bc.IsAStarMove = false;
                bc.IsMoveDirectly = false;
                this.<>f__this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }
        }
    }
}

