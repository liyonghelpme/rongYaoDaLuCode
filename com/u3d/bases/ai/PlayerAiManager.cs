﻿namespace com.u3d.bases.ai
{
    using com.game.module.core;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityLog;

    internal class PlayerAiManager : AiManagerBase
    {
        [CompilerGenerated]
        private static Func<PlayerAiElem, bool> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<PlayerAiElem, BaseRoleVo> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<BaseRoleVo, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<PlayerAiElem, bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<SummonMonsterAiElem, bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<PlayerAiElem, bool> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<SummonMonsterAiElem, bool> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<PlayerAiElem, bool> <>f__am$cache9;
        public static PlayerAiManager instance;
        private List<SummonMonsterAiElem> summonList = new List<SummonMonsterAiElem>();

        static PlayerAiManager()
        {
            if (instance == null)
            {
            }
            instance = new PlayerAiManager();
        }

        protected override void Add(ActionDisplay display)
        {
            MeDisplay meDisplay = display as MeDisplay;
            if (meDisplay != null)
            {
                PlayerAiElem elem = new PlayerAiElem(meDisplay);
                this.Add(elem);
            }
        }

        public override void Add(IEnumerable<PlayerDisplay> list)
        {
            this.summonList.Clear();
            base.Add(list);
        }

        public void AddSummonMonster(MonsterDisplay display)
        {
            if (display != null)
            {
                SummonMonsterAiElem item = new SummonMonsterAiElem(display);
                this.summonList.Add(item);
            }
        }

        public override void BeginBattle()
        {
            if (this.CheckIsAllTeamOutOfBattle())
            {
                Singleton<BattleCenterView>.Instance.updateBattleState(true);
            }
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = aiElem => aiElem != null;
            }
            IEnumerator<PlayerAiElem> enumerator = base.List.Cast<PlayerAiElem>().Where<PlayerAiElem>(<>f__am$cache5).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerAiElem current = enumerator.Current;
                    current.ai.SetBattleState(true);
                    current.ai.CancelAddHp();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = e => e != null;
            }
            IEnumerator<SummonMonsterAiElem> enumerator2 = this.summonList.Where<SummonMonsterAiElem>(<>f__am$cache6).GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    SummonMonsterAiElem elem2 = enumerator2.Current;
                    elem2.ai.SetBattleState(true);
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
        }

        private bool CheckIsAllTeamOutOfBattle()
        {
            bool flag = true;
            IEnumerator<PlayerAiElem> enumerator = base.List.Cast<PlayerAiElem>().GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerAiElem current = enumerator.Current;
                    if (current.ai == null)
                    {
                        flag = false;
                    }
                    else if (current.ai.IsInBattle)
                    {
                        flag = false;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            foreach (SummonMonsterAiElem elem2 in this.summonList)
            {
                if (elem2.ai == null)
                {
                    return false;
                }
                if (elem2.ai.IsInBattle)
                {
                    flag = false;
                }
            }
            return flag;
        }

        public override void EndBattle(ActionDisplay requesterDisplay)
        {
            if (<>f__am$cache7 == null)
            {
                <>f__am$cache7 = elem => AiTargetManager.instance.IsMeBeTargeted(elem.me);
            }
            if (!base.List.Cast<PlayerAiElem>().Any<PlayerAiElem>(<>f__am$cache7))
            {
                if (<>f__am$cache8 == null)
                {
                    <>f__am$cache8 = elem => AiTargetManager.instance.IsMeBeTargeted(elem.me);
                }
                if (!this.summonList.Any<SummonMonsterAiElem>(<>f__am$cache8))
                {
                    this.HandleAllTeamEndBattleState();
                }
            }
        }

        public bool GetTeamBattleStateInPVE()
        {
            if (<>f__am$cache9 == null)
            {
                <>f__am$cache9 = item => (item.ai != null) && item.ai.IsInBattle;
            }
            return base.List.Cast<PlayerAiElem>().Any<PlayerAiElem>(<>f__am$cache9);
        }

        private void HandleAllTeamEndBattleState()
        {
            Singleton<BattleCenterView>.Instance.updateBattleState(false);
            bool flag = false;
            IEnumerator<PlayerAiElem> enumerator = base.List.Cast<PlayerAiElem>().GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerAiElem current = enumerator.Current;
                    current.ai.SetBattleState(false);
                    if (!current.me.ReliveIfDie() && current.ai.StartAddHp())
                    {
                        flag = true;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            foreach (SummonMonsterAiElem elem2 in this.summonList)
            {
                elem2.ai.SetBattleState(false);
            }
            if (flag)
            {
                Log.AI(null, "AnyOne Need Add Hp");
                IEnumerator<PlayerAiElem> enumerator3 = base.List.Cast<PlayerAiElem>().GetEnumerator();
                try
                {
                    while (enumerator3.MoveNext())
                    {
                        PlayerAiElem elem3 = enumerator3.Current;
                        CommandHandler.AddCommandStatic(elem3.ai.gameObject, "idle", 1f);
                    }
                }
                finally
                {
                    if (enumerator3 == null)
                    {
                    }
                    enumerator3.Dispose();
                }
            }
        }

        public bool IsAllMemberHpEnoughToAi()
        {
            if (<>f__am$cache2 == null)
            {
                <>f__am$cache2 = aielem => aielem != null;
            }
            if (<>f__am$cache3 == null)
            {
                <>f__am$cache3 = aielem => aielem.display.GetVo();
            }
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = baseRoleVo => baseRoleVo.CurHpPercent < (AiConfig.HP_PERCENTAGE_TO_CONTINUE_AI * 0.001f);
            }
            IEnumerator<BaseRoleVo> enumerator = base.List.Cast<PlayerAiElem>().Where<PlayerAiElem>(<>f__am$cache2).Select<PlayerAiElem, BaseRoleVo>(<>f__am$cache3).Where<BaseRoleVo>(<>f__am$cache4).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    BaseRoleVo current = enumerator.Current;
                    Log.AI(null, "curhp percent " + current.CurHpPercent);
                    return false;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return true;
        }

        public void RemoveSummonMonster(MonsterDisplay display)
        {
            <RemoveSummonMonster>c__AnonStorey105 storey = new <RemoveSummonMonster>c__AnonStorey105 {
                display = display
            };
            this.summonList = this.summonList.Where<SummonMonsterAiElem>(new Func<SummonMonsterAiElem, bool>(storey.<>m__12A)).ToList<SummonMonsterAiElem>();
        }

        [CompilerGenerated]
        private sealed class <RemoveSummonMonster>c__AnonStorey105
        {
            internal MonsterDisplay display;

            internal bool <>m__12A(PlayerAiManager.SummonMonsterAiElem x)
            {
                return (x.display != this.display);
            }
        }

        public class PlayerAiElem : AiManagerBase.AiElem
        {
            public PlayerAiController ai;
            public MeDisplay display;
            public MeControler me;

            public PlayerAiElem(MeDisplay meDisplay)
            {
                this.display = meDisplay;
                this.me = meDisplay.Controller as MeControler;
                this.ai = this.me.AiController as PlayerAiController;
            }
        }

        public class SummonMonsterAiElem : AiManagerBase.AiElem
        {
            public SummonMonsterAiController ai;
            public MonsterDisplay display;
            public MonsterControler me;

            public SummonMonsterAiElem(MonsterDisplay display)
            {
                this.display = display;
                this.me = display.Controller as MonsterControler;
                this.ai = this.me.AiController as SummonMonsterAiController;
            }
        }
    }
}

