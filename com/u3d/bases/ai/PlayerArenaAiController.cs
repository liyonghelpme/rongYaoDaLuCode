﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.liyong;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class PlayerArenaAiController : PlayerAiController
    {
        private float _selectNearerInterval;
        private float _selectNearerMargin;
        private float _selectNearerTick;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<AiControllerBase.KV, float> <>f__am$cache7;

        protected override void AiStateDecision(float timeElapsed)
        {
            if (base._currentTargetDisplay != null)
            {
                if (this.CheckCacheAiBehavior())
                {
                    this.EnterStateHandleCachedAiBehavior();
                }
                else if (base._isNeedContinueAtk)
                {
                    base.CheckUseAttack();
                }
                if (!base.CheckIsDoingAttack())
                {
                    if (base.CheckUseNormalSkill())
                    {
                        base._isNeedContinueAtk = false;
                        base._restPriorityAtk = 0;
                    }
                    else if (!base.CheckUseAttack() && this.CheckIsInRunOrIdleOrHurt())
                    {
                        this.ProperMove();
                    }
                }
            }
        }

        protected override bool CheckCanUseAi()
        {
            if (base._mePlayerVo.stateInfo.ShouldPauseAI)
            {
                return false;
            }
            bool isAiEnable = this.IsAiEnable;
            bool flag2 = !base._mePlayerVo.IsEmptyHp;
            bool flag3 = !this.CheckIsDead();
            return ((isAiEnable && flag2) && flag3);
        }

        protected override bool CheckShouldSkipThink()
        {
            return this.CheckIsDoingSkill();
        }

        protected override void InitLoopInterval()
        {
            base._loop_interval = 0.15f;
            base._stopTime = 0f;
            base._tick = base._loop_interval;
            this._selectNearerTick = 0f;
        }

        protected override void InitRanges()
        {
            base.InitRanges();
            this._selectNearerMargin = AiConfig.ARENA_AI_SELECT_TARGET_MARGIN * 0.001f;
            this._selectNearerInterval = AiConfig.ARENA_SELECT_TARGET_INTERVAL * 0.001f;
        }

        protected override Func<bool> IsNeedToSetNewTargetDisplay()
        {
            return delegate {
                if (base._isNeedRefreshTarget)
                {
                    base._isNeedRefreshTarget = false;
                    base.ResetContinuousAtk();
                    return true;
                }
                if (((base._currentTargetDisplay == null) || (base._targetVo == null)) || base._targetVo.IsEmptyHp)
                {
                    base.ResetContinuousAtk();
                    return true;
                }
                return (((base._currentTargetDisplay != null) && (this._selectNearerTick >= this._selectNearerInterval)) || (base._targetVo.Camp != base._targetLastCamp));
            };
        }

        protected override void MoveToTarget(bool isAway = true)
        {
            if (((base._currentTargetDisplay == null) || (base._currentTargetDisplay.Controller == null)) || ((base._mePlayerVo == null) || base._mePlayerVo.IsEmptyHp))
            {
                base._isNeedContinueAtk = false;
            }
            else
            {
                this.StartChase(base._currentTargetDisplay.GoBase.transform.position);
            }
        }

        protected override void PrepareWork()
        {
            this._selectNearerTick += base._loop_interval;
        }

        private void ProperMove()
        {
            this.MoveToTarget(true);
        }

        public override void SetAi(bool value)
        {
            if (base.MeController == AppMap.Instance.me.Controller)
            {
                Singleton<BattleTopLeftView>.Instance.updateAutoEffect(value);
            }
            base.SetAi(value);
            base._isNeedRefreshTarget = true;
            base.MeController.IsMoveDirectly = false;
            base.MeController.IsAStarMove = false;
            base.MeController.ClearPath();
            this.SetAiStatus(AiEnum.PlayerAiStatus.THINK);
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.PLAYER_ARENA;
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            base.attackable_list.Clear();
            if (<>f__am$cache3 == null)
            {
                <>f__am$cache3 = display => new <>__AnonType20<PlayerDisplay, BaseRoleVo>(display, display.GetVo());
            }
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = <>__TranspIdent23 => !<>__TranspIdent23.nowDisplay.IsEmptyHp && (<>__TranspIdent23.nowDisplay.Controller != null);
            }
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = <>__TranspIdent23 => <>__TranspIdent23.display;
            }
            IEnumerator<PlayerDisplay> enumerator = (from <>__TranspIdent23 in AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cache3).Where<<>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cache4)
                where <>__TranspIdent23.nowDisplay.Camp != base.MyCamp
                select <>__TranspIdent23).Select<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cache5).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    base.attackable_list.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = <>__TranspIdent24 => new AiControllerBase.KV(<>__TranspIdent24.ad, <>__TranspIdent24.distance);
            }
            List<AiControllerBase.KV> source = (from ad in base.attackable_list select new <>__AnonType21<ActionDisplay, float>(ad, this.GetTargetDistance(ad))).Select<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV>(<>f__am$cache6).ToList<AiControllerBase.KV>();
            if (source.Count < 1)
            {
                this.CleanTargetDisplay();
                return false;
            }
            if (<>f__am$cache7 == null)
            {
                <>f__am$cache7 = x => x.distance;
            }
            source = source.OrderBy<AiControllerBase.KV, float>(<>f__am$cache7).ToList<AiControllerBase.KV>();
            if (((base._currentTargetDisplay != null) && !base._targetVo.IsEmptyHp) && ((this._selectNearerTick >= this._selectNearerInterval) && (this.GetTargetDistance((ActionDisplay) null) <= (source[0].distance + this._selectNearerMargin))))
            {
                this._selectNearerTick = 0f;
                return false;
            }
            this.SetTargetDisplay(source[0].ad);
            base.ResetAiCounters();
            this._selectNearerTick = 0f;
            return true;
        }

        private void StartChase(Vector3 dest)
        {
            Vector3 vector = DamageCheck.GetFinalDestinationBeforeChase(dest, base._selfTransform, base.AtkRange);
            ActionVo vo = new ActionVo {
                Destination = vector,
                ActionType = "run",
                IsAStarMove = true,
                Callback = delegate (BaseControler bc) {
                    bc.IsMoveDirectly = false;
                    bc.IsAStarMove = false;
                    this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
                }
            };
            this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.RUN_TO_TARGET);
            base.MeController.AttackController.AddAttackList(vo, false, false);
            if (base._meLastPos == base.transform.position)
            {
                base._walkFailCount = (byte) (base._walkFailCount + 1);
                base._stopTime += Time.deltaTime;
            }
            else
            {
                base._stopTime = 0f;
                base._walkFailCount = 0;
                base._meLastPos = base.transform.position;
            }
        }
    }
}

