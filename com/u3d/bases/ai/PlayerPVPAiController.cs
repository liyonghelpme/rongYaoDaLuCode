﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.WiFiPvP;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    internal class PlayerPVPAiController : PlayerAiController
    {
        private ActionDisplay _attackerTower;
        private Vector3 _escapeDest;
        private bool _isHealSkillAvailable = true;
        private bool _isNeedEscapeFromTower;
        private bool _isTargetingBaseOrTower;
        private float _lowHpRate;
        private Vector3 _mySpringPos;
        private float _outOfBattleTime;
        private SkillController _skillController;
        private float _springAddHpLimit;
        private float _springRange;
        private float _towerAtkRange;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>> <>f__am$cache10;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cache11;
        [CompilerGenerated]
        private static Func<MonsterDisplay, <>__AnonType22<MonsterDisplay, MonsterVo>> <>f__am$cache12;
        [CompilerGenerated]
        private static Func<<>__AnonType22<MonsterDisplay, MonsterVo>, MonsterDisplay> <>f__am$cache13;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>> <>f__am$cache14;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cache15;
        [CompilerGenerated]
        private static Func<MonsterDisplay, <>__AnonType22<MonsterDisplay, MonsterVo>> <>f__am$cache16;
        [CompilerGenerated]
        private static Func<<>__AnonType22<MonsterDisplay, MonsterVo>, MonsterDisplay> <>f__am$cache17;
        private float lastSpringRecoverTime;
        private float lastSyncTime;
        private uint oldHp;
        private bool startRecoverYet;

        protected override void AiStateDecision(float timeElapsed)
        {
        }

        public override void BeAttacked(ActionVo actionVo, float time = 0)
        {
            time = (time == 0f) ? base._nowTime : time;
            base.LastBeAttackedTime = time;
            if (base._ai_status != 200)
            {
                ActionDisplay attacker = actionVo.Attacker;
                if (this.IsAiEnable)
                {
                    if ((base._ai_status == 11) || (base._ai_status == 200))
                    {
                        this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
                    }
                    if ((base._currentTargetDisplay is MonsterDisplay) && (attacker is PlayerDisplay))
                    {
                        int type = base._currentTargetDisplay.GetMeVoByType<MonsterVo>().MonsterVO.type;
                        if (((type == 5) || (type == 3)) && (base._ai_status != 200))
                        {
                            base._selfTransform.LookAt(attacker.GoBase.transform);
                            this.SetTargetDisplay(attacker);
                        }
                    }
                }
                if ((attacker is MonsterDisplay) && (attacker.GetMeVoByType<MonsterVo>().MonsterVO.type == 3))
                {
                    this.StartEscapingFromTower(attacker);
                }
            }
        }

        private bool CheckIsRunningToSomewhere()
        {
            return ((base._ai_status == 11) || (base._ai_status == 13));
        }

        protected override bool CheckShouldSkipThink()
        {
            return this.CheckIsDoingSkill();
        }

        public override void CleanTargetDisplay()
        {
            base._currentTargetDisplay = null;
            base._targetVo = null;
        }

        private void DoEscaping()
        {
            MoveEndCallback callbackFunc = delegate (BaseControler bc) {
                bc.IsMoveDirectly = false;
                bc.IsAStarMove = false;
                this.IsNeedEscapeFromTower = false;
                this._escapeDest = Vector3.zero;
                this.SetAiStatus(AiEnum.PlayerAiStatus.THINK);
            };
            this.DoStartChase(this._escapeDest, false, false, callbackFunc);
        }

        private void DoStartChase(Vector3 dest, bool isChaseEnemy = false, bool isToMySpring = false, MoveEndCallback callbackFunc = null)
        {
            <DoStartChase>c__AnonStorey108 storey = new <DoStartChase>c__AnonStorey108 {
                isChaseEnemy = isChaseEnemy,
                isToMySpring = isToMySpring,
                <>f__this = this
            };
            if (callbackFunc == null)
            {
                callbackFunc = new MoveEndCallback(storey.<>m__165);
            }
            ActionVo vo = new ActionVo {
                Destination = dest,
                ActionType = "run",
                IsAStarMove = true,
                Preparework = new PreMoveCallBack(storey.<>m__166),
                Callback = callbackFunc
            };
            base.MeController.AttackController.AddAttackList(vo, false, false);
            base._stopTime = 0f;
        }

        private void EscapeFromTower()
        {
            if (this._attackerTower == null)
            {
                this.IsNeedEscapeFromTower = false;
            }
            else
            {
                float radius = this._towerAtkRange;
                float num2 = 1.5f;
                this._escapeDest = DamageCheck.GetEscapePosFromNowPos(base._selfTransform.position, this._attackerTower.GoBase.transform.position, radius);
                while (DamageCheck.GetDistance(this._escapeDest, base._selfTransform.position) <= 0.3f)
                {
                    num2 += 0.05f;
                    radius = this._towerAtkRange * num2;
                    this._escapeDest = DamageCheck.GetEscapePosFromNowPos(base._selfTransform.position, this._attackerTower.GoBase.transform.position, radius);
                }
                this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.ESCAPE_FROM_TOWER);
            }
        }

        public ActionDisplay GetPlayerEnemy()
        {
            base.attackable_list.Clear();
            if (<>f__am$cache10 == null)
            {
                <>f__am$cache10 = display => new <>__AnonType20<PlayerDisplay, BaseRoleVo>(display, display.GetVo());
            }
            if (<>f__am$cache11 == null)
            {
                <>f__am$cache11 = <>__TranspIdent25 => <>__TranspIdent25.display;
            }
            IEnumerator<PlayerDisplay> enumerator = (from <>__TranspIdent25 in AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cache10)
                where ((<>__TranspIdent25.nowDisplay.Camp != base._mePlayerVo.Camp) && !<>__TranspIdent25.nowDisplay.IsEmptyHp) && (<>__TranspIdent25.nowDisplay.Controller != null)
                where this.GetTargetDistance(<>__TranspIdent25.display) <= base._sightRange
                select <>__TranspIdent25).Select<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cache11).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    base.attackable_list.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (base.attackable_list.Count > 0)
            {
                base.attackable_list = (from x in base.attackable_list
                    orderby this.GetTargetDistance(x)
                    select x).ToList<ActionDisplay>();
                return base.attackable_list[0];
            }
            if (<>f__am$cache12 == null)
            {
                <>f__am$cache12 = ad => new <>__AnonType22<MonsterDisplay, MonsterVo>(ad, ad.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cache13 == null)
            {
                <>f__am$cache13 = <>__TranspIdent26 => <>__TranspIdent26.ad;
            }
            IEnumerator<MonsterDisplay> enumerator2 = (from <>__TranspIdent26 in AppMap.Instance.monsterList.Select<MonsterDisplay, <>__AnonType22<MonsterDisplay, MonsterVo>>(<>f__am$cache12)
                where ((<>__TranspIdent26.nowDisplay.Camp != base._mePlayerVo.Camp) && !<>__TranspIdent26.nowDisplay.IsEmptyHp) && (<>__TranspIdent26.nowDisplay.Controller != null)
                select <>__TranspIdent26).Select<<>__AnonType22<MonsterDisplay, MonsterVo>, MonsterDisplay>(<>f__am$cache13).GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    MonsterDisplay item = enumerator2.Current;
                    base.attackable_list.Add(item);
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
            if (base.attackable_list.Count > 0)
            {
                base.attackable_list.Sort(delegate (ActionDisplay display1, ActionDisplay display2) {
                    MonsterVo meVoByType = display1.GetMeVoByType<MonsterVo>();
                    float targetDistance = this.GetTargetDistance(display1);
                    MonsterVo vo2 = display2.GetMeVoByType<MonsterVo>();
                    float num2 = this.GetTargetDistance(display2);
                    if ((meVoByType.MonsterVO.type != 5) && (vo2.MonsterVO.type == 5))
                    {
                        return 1;
                    }
                    if ((meVoByType.MonsterVO.type == 3) && (vo2.MonsterVO.type == 4))
                    {
                        return 1;
                    }
                    if ((meVoByType.MonsterVO.type == vo2.MonsterVO.type) && (targetDistance < num2))
                    {
                        return 1;
                    }
                    return 0;
                });
                return base.attackable_list[0];
            }
            return null;
        }

        public Vector3 GetSpringPos()
        {
            Vector3 vector = DamageCheck.GetDestPosFromNowPos(base._selfTransform.position, this._mySpringPos, this._springRange * 0.8f);
            Log.AI(base.gameObject, " MoveToSpring Pos " + vector);
            return vector;
        }

        protected override void Init()
        {
            base._selfTransform = base.transform;
            this._mySpringPos = WifiPvpManager.Instance.MySpringPos;
            this._springRange = WifiPvpManager.Instance.PvpTemplate.spring_radius * 0.001f;
            this._towerAtkRange = AiConfig.TOWER_ATTACK_RANGE_IN_PVP * 0.001f;
            this._lowHpRate = AiConfig.ROLE_HP_RATE_TO_ESCAPE_FROM_TOWER * 0.001f;
            this._springAddHpLimit = AiConfig.ROLE_AI_START_AFTER_ADD_HP_NEAR_SPRING * 0.001f;
            this._outOfBattleTime = AiConfig.OUT_OF_BATTLE_TIME_IN_WIFI_PVP * 0.001f;
            if (this._skillController == null)
            {
            }
            this._skillController = base.MeController.SkillController;
            base.Init();
        }

        public bool IsFarAwayTowerHp()
        {
            return (base._mePlayerVo.CurHpPercent <= 0.5f);
        }

        public bool IsHealSkillAvailable()
        {
            Log.AI(base.gameObject, " IsHealAvailable ");
            return (this.IsHealSkillCDReady() && this._isHealSkillAvailable);
        }

        private bool IsHealSkillCDReady()
        {
            return this._skillController.IsSkillCdReady(7);
        }

        public bool IsHpEnough()
        {
            Log.AI(base.gameObject, string.Concat(new object[] { " hp percent ", base._mePlayerVo.CurHpPercent, " springHpLimit ", this._springAddHpLimit }));
            return (base._mePlayerVo.CurHpPercent >= this._springAddHpLimit);
        }

        public bool IsInHeal()
        {
            return (this.IsNearDestination(this._mySpringPos) && !this.IsHpEnough());
        }

        public bool IsLowHp()
        {
            Log.AI(base.gameObject, string.Concat(new object[] { " hp percent ", base._mePlayerVo.CurHpPercent, " lowHpRate ", this._lowHpRate }));
            return (base._mePlayerVo.CurHpPercent <= this._lowHpRate);
        }

        private bool IsNearDestination(Vector3 dest)
        {
            return (DamageCheck.GetDistance(base._selfTransform.position, dest) < this._springRange);
        }

        protected override Func<bool> IsNeedToSetNewTargetDisplay()
        {
            return () => ((((base._currentTargetDisplay == null) || (base._currentTargetDisplay.Controller == null)) || (base._currentTargetDisplay.GetMeVoByType<BaseRoleVo>().IsEmptyHp || (this.GetTargetDistance((ActionDisplay) null) > base._sightRange))) || (base._targetVo.Camp != base._targetLastCamp));
        }

        private bool IsOtherSideBaseAvailable()
        {
            return true;
        }

        private void MoveToEnemyBase()
        {
            Vector3 enemyBasePos;
            this.SetAiStatus(AiEnum.PlayerAiStatus.RUN_TO_ENEMY_BASE);
            float radius = (DamageCheck.GetDistance(base._selfTransform.position, WifiPvpManager.Instance.EnemyBasePos) <= base.AtkRange) ? 0f : base.AtkRange;
            if (radius == 0f)
            {
                enemyBasePos = WifiPvpManager.Instance.EnemyBasePos;
            }
            else
            {
                enemyBasePos = DamageCheck.GetDestPosFromNowPos(base._selfTransform.position, WifiPvpManager.Instance.EnemyBasePos, radius);
            }
            this.DoStartChase(enemyBasePos, false, false, null);
        }

        private void MoveToSpring()
        {
            this.SetAiStatus(AiEnum.PlayerAiStatus.RUN_TO_MY_SPRING);
            Vector3 dest = DamageCheck.GetDestPosFromNowPos(base._selfTransform.position, this._mySpringPos, this._springRange * 0.8f);
            this.DoStartChase(dest, false, true, delegate (BaseControler bc) {
                bc.IsMoveDirectly = false;
                bc.IsAStarMove = false;
                this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.NEAR_SPRING);
            });
        }

        private void MoveToTarget()
        {
            if (((base._currentTargetDisplay == null) || (base._currentTargetDisplay.GoBase == null)) || ((base._mePlayerVo == null) || base._mePlayerVo.IsEmptyHp))
            {
                base._isNeedRefreshTarget = true;
            }
            else if ((base._currentTargetDisplay is MonsterDisplay) && (base._currentTargetDisplay.GetMeVoByType<MonsterVo>().MonsterVO.type == 4))
            {
                base._isNeedRefreshTarget = true;
                this.MoveToEnemyBase();
            }
            else
            {
                Vector3 moveDestinationToEnemy = base.GetMoveDestinationToEnemy();
                this.StartChase(moveDestinationToEnemy);
            }
        }

        protected override void PrepareWork()
        {
            if (this.IsNearDestination(this._mySpringPos))
            {
                if (!base._mePlayerVo.IsFullHp)
                {
                    if (!this.startRecoverYet)
                    {
                        this.startRecoverYet = true;
                        this.lastSpringRecoverTime = Time.time;
                    }
                    float num = Time.time - this.lastSpringRecoverTime;
                    if (num > 1f)
                    {
                        this.lastSpringRecoverTime = Time.time;
                        float percent = (0.001f * AiConfig.ROLE_RECOVERY_PERCENT_NEAR_SPRING_IN_WIFI_PVP) * num;
                        Log.AI(null, string.Concat(new object[] { " Spring AddHp Percent ", percent, " deltaTime ", num, " role_recovery_percent ", AiConfig.ROLE_RECOVERY_PERCENT_NEAR_SPRING_IN_WIFI_PVP }));
                        base._mePlayerVo.AddHpByPercent(percent);
                        if ((base._mePlayerVo.CurHp != this.oldHp) || ((Time.time - this.lastSyncTime) > 1f))
                        {
                            this.lastSyncTime = Time.time;
                            this.oldHp = base._mePlayerVo.CurHp;
                            Singleton<WifiPvpMode>.Instance.SyncHpVal(base._mePlayerVo.Controller.GetMeByType<ActionDisplay>(), this.oldHp);
                        }
                    }
                }
                else
                {
                    this.startRecoverYet = false;
                }
            }
            else
            {
                this.startRecoverYet = false;
            }
            if (!base._mePlayerVo.IsEmptyHp && this.IsHealSkillCDReady())
            {
                Log.AI(base.gameObject, " HealSkill ReadyToUse ");
                this._isHealSkillAvailable = true;
                this._skillController.ContinueHealSkill();
            }
            else
            {
                this._isHealSkillAvailable = false;
                this._skillController.PauseHealSkill();
            }
        }

        private void ProperMove()
        {
            this.MoveToTarget();
        }

        public override void SetAi(bool value)
        {
            if (base.MeController == AppMap.Instance.me.Controller)
            {
                Singleton<BattleTopLeftView>.Instance.updateAutoEffect(value);
            }
            if (value != this.IsAiEnable)
            {
                base._lastIsAiEnable = this.IsAiEnable;
                this.IsAiEnable = value;
                this.IsNeedEscapeFromTower = false;
                base._isNeedRefreshTarget = true;
            }
            base.MeController.IsMoveDirectly = false;
            base.MeController.IsAStarMove = false;
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.PLAYER_PVP;
        }

        public override void SetTargetDisplay(ActionDisplay ad)
        {
            if (ad != null)
            {
                if ((ad != base._currentTargetDisplay) && (base._currentTargetDisplay != null))
                {
                    Debug.Log("切换锁定的AI目标: " + base._currentTargetDisplay.Key);
                }
                base._currentTargetDisplay = ad;
                base._targetVo = ad.GetMeVoByType<BaseRoleVo>();
                base._targetLastCamp = base._targetVo.Camp;
                if (base._currentTargetDisplay is MonsterDisplay)
                {
                    switch (base._currentTargetDisplay.GetMeVoByType<MonsterVo>().MonsterVO.type)
                    {
                        case 5:
                        case 3:
                            Debug.Log("在打塔或基地时, 被敌方英雄攻击, 切换目标为敌方英雄");
                            this._isTargetingBaseOrTower = true;
                            break;
                    }
                }
                else
                {
                    this._isTargetingBaseOrTower = false;
                }
            }
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            base.attackable_list.Clear();
            if (<>f__am$cache14 == null)
            {
                <>f__am$cache14 = display => new <>__AnonType20<PlayerDisplay, BaseRoleVo>(display, display.GetVo());
            }
            if (<>f__am$cache15 == null)
            {
                <>f__am$cache15 = <>__TranspIdent27 => <>__TranspIdent27.display;
            }
            IEnumerator<PlayerDisplay> enumerator = (from <>__TranspIdent27 in AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cache14)
                where ((<>__TranspIdent27.nowDisplay.Camp != base._mePlayerVo.Camp) && !<>__TranspIdent27.nowDisplay.IsEmptyHp) && (<>__TranspIdent27.nowDisplay.Controller != null)
                where this.GetTargetDistance(<>__TranspIdent27.display) <= base._sightRange
                select <>__TranspIdent27).Select<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cache15).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    base.attackable_list.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (base.attackable_list.Count > 0)
            {
                base.attackable_list = (from x in base.attackable_list
                    orderby this.GetTargetDistance(x)
                    select x).ToList<ActionDisplay>();
                this.SetTargetDisplay(base.attackable_list[0]);
                return true;
            }
            if (<>f__am$cache16 == null)
            {
                <>f__am$cache16 = ad => new <>__AnonType22<MonsterDisplay, MonsterVo>(ad, ad.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cache17 == null)
            {
                <>f__am$cache17 = <>__TranspIdent28 => <>__TranspIdent28.ad;
            }
            IEnumerator<MonsterDisplay> enumerator2 = (from <>__TranspIdent28 in AppMap.Instance.monsterList.Select<MonsterDisplay, <>__AnonType22<MonsterDisplay, MonsterVo>>(<>f__am$cache16)
                where ((<>__TranspIdent28.nowDisplay.Camp != base._mePlayerVo.Camp) && !<>__TranspIdent28.nowDisplay.IsEmptyHp) && (<>__TranspIdent28.nowDisplay.Controller != null)
                where this.GetTargetDistance(<>__TranspIdent28.ad) <= base._sightRange
                select <>__TranspIdent28).Select<<>__AnonType22<MonsterDisplay, MonsterVo>, MonsterDisplay>(<>f__am$cache17).GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    MonsterDisplay item = enumerator2.Current;
                    base.attackable_list.Add(item);
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
            if (base.attackable_list.Count > 0)
            {
                base.attackable_list.Sort(delegate (ActionDisplay display1, ActionDisplay display2) {
                    MonsterVo meVoByType = display1.GetMeVoByType<MonsterVo>();
                    float targetDistance = this.GetTargetDistance(display1);
                    MonsterVo vo2 = display2.GetMeVoByType<MonsterVo>();
                    float num2 = this.GetTargetDistance(display2);
                    if ((meVoByType.MonsterVO.type != 5) && (vo2.MonsterVO.type == 5))
                    {
                        return 1;
                    }
                    if ((meVoByType.MonsterVO.type == 3) && (vo2.MonsterVO.type == 4))
                    {
                        return 1;
                    }
                    if ((meVoByType.MonsterVO.type == vo2.MonsterVO.type) && (targetDistance < num2))
                    {
                        return 1;
                    }
                    return 0;
                });
                this.SetTargetDisplay(base.attackable_list[0]);
                return true;
            }
            this.CleanTargetDisplay();
            return false;
        }

        private void StartChase(Vector3 dest)
        {
            if (base._meLastPos == base.transform.position)
            {
                base._walkFailCount = (byte) (base._walkFailCount + 1);
            }
            else
            {
                base._walkFailCount = 0;
                base._meLastPos = base.transform.position;
            }
            this.DoStartChase(dest, false, false, null);
        }

        private void StartEscapingFromTower(ActionDisplay attacker)
        {
            if (this.IsAiEnable)
            {
                this.IsNeedEscapeFromTower = true;
                this._attackerTower = attacker;
                this.EscapeFromTower();
            }
        }

        public void UseHealSkill()
        {
            Log.AI(base.gameObject, " UseHealSkill ");
            this._skillController.RequestUseSkill(7, true, false, null, null, null);
            this._isHealSkillAvailable = false;
        }

        private bool IsInBattle
        {
            get
            {
                return (((base._nowTime - this.LastUseSkillTime) <= this._outOfBattleTime) || ((base._nowTime - this.LastBeAttackedTime) <= this._outOfBattleTime));
            }
        }

        private bool IsNeedEscapeFromTower
        {
            get
            {
                return this._isNeedEscapeFromTower;
            }
            set
            {
                if (!value)
                {
                    this._attackerTower = null;
                    this._escapeDest = Vector3.zero;
                }
                this._isNeedEscapeFromTower = value;
            }
        }

        [CompilerGenerated]
        private sealed class <DoStartChase>c__AnonStorey108
        {
            internal PlayerPVPAiController <>f__this;
            internal bool isChaseEnemy;
            internal bool isToMySpring;

            internal void <>m__165(BaseControler bc)
            {
                bc.IsMoveDirectly = false;
                bc.IsAStarMove = false;
                this.<>f__this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.THINK);
            }

            internal void <>m__166(BaseControler mc)
            {
                mc.ClearPath();
                if (this.isChaseEnemy)
                {
                    this.<>f__this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.RUN_TO_TARGET);
                }
                else if (this.isToMySpring)
                {
                    this.<>f__this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.RUN_TO_MY_SPRING);
                }
                else
                {
                    this.<>f__this.SetAiStatusButNotClearTarget(AiEnum.PlayerAiStatus.RUN_TO_ENEMY_BASE);
                }
            }
        }
    }
}

