﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.data;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.WiFiPvP;
    using com.game.vo;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class PrivateAiController : AiControllerBase
    {
        private float _atkRange;
        private Vector3 _enemyBasePos;
        private IList<Vector3> _givenPath = new List<Vector3>();
        private bool _isNeedRefreshTarget;
        private MonsterVo _meMonsterVo;
        private float _sightRange;
        private int _walkFailCount;
        [CompilerGenerated]
        private static Func<MonsterDisplay, <>__AnonType23<MonsterDisplay, MonsterVo>> <>f__am$cache10;
        [CompilerGenerated]
        private static Func<<>__AnonType23<MonsterDisplay, MonsterVo>, MonsterDisplay> <>f__am$cache11;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>> <>f__am$cache12;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, bool> <>f__am$cache13;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cache14;
        [CompilerGenerated]
        private static Func<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV> <>f__am$cache15;
        [CompilerGenerated]
        private static Func<AiControllerBase.KV, float> <>f__am$cache16;
        [CompilerGenerated]
        private static Func<MonsterDisplay, <>__AnonType23<MonsterDisplay, MonsterVo>> <>f__am$cacheA;
        [CompilerGenerated]
        private static Func<<>__AnonType23<MonsterDisplay, MonsterVo>, MonsterDisplay> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>> <>f__am$cacheC;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, bool> <>f__am$cacheD;
        [CompilerGenerated]
        private static Func<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay> <>f__am$cacheE;
        [CompilerGenerated]
        private static Func<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV> <>f__am$cacheF;
        private float dis;

        protected override void AiStateDecision(float timeElapsed)
        {
        }

        public override void BeAttacked(ActionVo attackVo, float time = 0)
        {
            base.LastBeAttackedTime = (time == 0f) ? Time.time : time;
            this.SetAiStatus(AiEnum.PrivateAiStatus.THINK);
        }

        private bool CheckUseAttack(float distance)
        {
            if (base._targetVo.IsEmptyHp)
            {
                this._isNeedRefreshTarget = true;
                return true;
            }
            if (distance > this._atkRange)
            {
                return false;
            }
            if ((base._nowTime - this.LastUseAttackTime) > 0.45f)
            {
                this.DoAttack();
                base._stopTime = 0f;
                base.LastUseAttackTime = base._nowTime;
            }
            return true;
        }

        public override void CleanTargetDisplay()
        {
            base._currentTargetDisplay = null;
            base._targetVo = null;
            if (!AppMap.Instance.IsInWifiPVP)
            {
                AiTargetManager.instance.MonsterUpdateTarget(base.MeController, null);
            }
        }

        private void DoAttack()
        {
            if (this._meMonsterVo != null)
            {
                Vector3 position = base._currentTargetDisplay.Controller.transform.position;
                ActionVo vo = new ActionVo {
                    ActionType = "attack",
                    IsIgnoreDistance = true,
                    Target = base._currentTargetDisplay
                };
                base.MeController.AttackController.AddAttackList(vo, false, false);
            }
        }

        private ActionDisplay DoPrioritySelect(IEnumerable<AiControllerBase.KV> targetList, int displayType, int givenType)
        {
            List<AiControllerBase.KV> source = new List<AiControllerBase.KV>();
            IEnumerator<AiControllerBase.KV> enumerator = targetList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    AiControllerBase.KV current = enumerator.Current;
                    bool flag = current.ad.Type == displayType;
                    bool flag2 = false;
                    if (displayType != 100)
                    {
                        MonsterVo meVoByType = current.ad.GetMeVoByType<MonsterVo>();
                        if (current.ad is PlayerDisplay)
                        {
                            continue;
                        }
                        if (current.ad is MonsterDisplay)
                        {
                            flag2 = meVoByType.MonsterVO.type == givenType;
                        }
                    }
                    else
                    {
                        flag2 = true;
                    }
                    if (flag && flag2)
                    {
                        source.Add(current);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (source.Count <= 0)
            {
                return null;
            }
            if (<>f__am$cache16 == null)
            {
                <>f__am$cache16 = x => x.distance;
            }
            source.OrderBy<AiControllerBase.KV, float>(<>f__am$cache16).ToList<AiControllerBase.KV>();
            return source[0].ad;
        }

        private float GetDistanceFromMe()
        {
            if (base._currentTargetDisplay == null)
            {
                return 99999f;
            }
            Vector3 position = base._currentTargetDisplay.GoBase.transform.position;
            return this.GetDistanceFromMe(position);
        }

        private float GetDistanceFromMe(Vector3 pos)
        {
            return DamageCheck.GetDistance(base.transform.position, pos);
        }

        private float GetDistanceToLastPathPoint()
        {
            Vector3 y = this._givenPath[this._currentPathStep];
            return DamageCheck.GetDistance(base._selfTransform.position, y);
        }

        private float GetDistanceToNextPathPoint()
        {
            int num = (this._currentPathStep >= this._maxPathStep) ? this._maxPathStep : (this._currentPathStep + 1);
            Vector3 y = this._givenPath[num];
            return DamageCheck.GetDistance(base._selfTransform.position, y);
        }

        public Vector3 GetNextPoint()
        {
            int num;
            this._currentPathStep = num = this._currentPathStep + 1;
            return this._givenPath[num];
        }

        public ActionDisplay GetPrivateEnemy()
        {
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = monsterDisplay => new <>__AnonType23<MonsterDisplay, MonsterVo>(monsterDisplay, monsterDisplay.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cacheB == null)
            {
                <>f__am$cacheB = <>__TranspIdent29 => <>__TranspIdent29.monsterDisplay;
            }
            List<ActionDisplay> list = (from <>__TranspIdent29 in AppMap.Instance.monsterList.Select<MonsterDisplay, <>__AnonType23<MonsterDisplay, MonsterVo>>(<>f__am$cacheA)
                where ((<>__TranspIdent29.mon.Controller != null) && !<>__TranspIdent29.mon.IsEmptyHp) && (<>__TranspIdent29.mon.Camp != base.MyCamp)
                select <>__TranspIdent29).Select<<>__AnonType23<MonsterDisplay, MonsterVo>, MonsterDisplay>(<>f__am$cacheB).Cast<ActionDisplay>().ToList<ActionDisplay>();
            if (<>f__am$cacheC == null)
            {
                <>f__am$cacheC = display => new <>__AnonType20<PlayerDisplay, BaseRoleVo>(display, display.GetVo());
            }
            if (<>f__am$cacheD == null)
            {
                <>f__am$cacheD = <>__TranspIdent30 => !<>__TranspIdent30.nowDisplay.IsEmptyHp && (<>__TranspIdent30.nowDisplay.Controller != null);
            }
            if (<>f__am$cacheE == null)
            {
                <>f__am$cacheE = <>__TranspIdent30 => <>__TranspIdent30.display;
            }
            list.AddRange((from <>__TranspIdent30 in AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cacheC).Where<<>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cacheD)
                where (<>__TranspIdent30.nowDisplay.SubType != 1) && (<>__TranspIdent30.nowDisplay.Camp != base.MyCamp)
                select <>__TranspIdent30).Select<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cacheE).Cast<ActionDisplay>());
            if (<>f__am$cacheF == null)
            {
                <>f__am$cacheF = <>__TranspIdent31 => new AiControllerBase.KV(<>__TranspIdent31.ad, <>__TranspIdent31.distance);
            }
            List<AiControllerBase.KV> targetList = (from ad in list
                select new <>__AnonType21<ActionDisplay, float>(ad, this.GetDistanceFromMe(ad.GoBase.transform.position)) into <>__TranspIdent31
                where <>__TranspIdent31.distance <= this._sightRange
                select <>__TranspIdent31).Select<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV>(<>f__am$cacheF).ToList<AiControllerBase.KV>();
            if (targetList.Count >= 1)
            {
                return this.PriorityTarget(targetList);
            }
            return null;
        }

        protected override void Init()
        {
            base.BaseRoleVo = this._meMonsterVo = base.MeController.Me.GetMeVoByType<MonsterVo>();
            base._selfTransform = base.transform;
            this.InitPath();
            base.Init();
        }

        protected override void InitLoopInterval()
        {
            base._loop_interval = 0.2f;
            base._tick = UnityEngine.Random.Range(0f, base._loop_interval);
            base._stopTime = UnityEngine.Random.Range((float) 0f, (float) 1f);
        }

        private void InitPath()
        {
            this._givenPath = WifiPvpManager.Instance.MyPath(this._meMonsterVo.groupIndex);
            if (this._givenPath != null)
            {
                this._maxPathStep = this._givenPath.Count - 1;
                this._currentPathStep = -1;
            }
        }

        protected override void InitRanges()
        {
            this._sightRange = this._meMonsterVo.MonsterVO.sight_range * 0.001f;
            this._atkRange = this._meMonsterVo.MonsterVO.attack_range * 0.001f;
            this._enemyBasePos = WifiPvpManager.Instance.EnemyBasePos;
        }

        private bool IsInAttackRange(float disToLast, float disToNext, float actualDis)
        {
            bool flag = actualDis <= disToNext;
            bool flag2 = actualDis >= disToLast;
            return (flag && flag2);
        }

        protected override Func<bool> IsNeedToSetNewTargetDisplay()
        {
            return delegate {
                if (!GlobalData.isInCopy)
                {
                    return false;
                }
                return (((base._currentTargetDisplay == null) || base._currentTargetDisplay.GetMeVoByType<BaseRoleVo>().IsEmptyHp) || ((base._targetVo.Camp != base._targetLastCamp) || this._isNeedRefreshTarget));
            };
        }

        private void MoveToEnemyBase()
        {
            this.SetAiStatusButNotClearTarget(AiEnum.PrivateAiStatus.RUN_TO_ENEMYBASE);
            this.StartChase(this._enemyBasePos);
        }

        private void MoveToNextPathPoint()
        {
            if (this._currentPathStep < this._maxPathStep)
            {
                int num;
                this._currentPathStep = num = this._currentPathStep + 1;
                Vector3 pos = this._givenPath[num];
                this.SetAiStatusButNotClearTarget(AiEnum.PrivateAiStatus.RUN_TO_ROADPATH);
                this.StartChase(pos);
            }
        }

        private bool PrioritySelect(List<AiControllerBase.KV> targetList)
        {
            int[,] numArray = new int[,] { { 400, 3 }, { 400, 4 }, { 100, 0 }, { 400, 5 } };
            for (int i = 0; i < 4; i++)
            {
                ActionDisplay ad = this.DoPrioritySelect(targetList, numArray[i, 0], numArray[i, 1]);
                if (ad != null)
                {
                    this.SetTargetDisplay(ad);
                    return true;
                }
            }
            return false;
        }

        private ActionDisplay PriorityTarget(List<AiControllerBase.KV> targetList)
        {
            int[,] numArray = new int[,] { { 400, 3 }, { 400, 4 }, { 100, 0 }, { 400, 5 } };
            for (int i = 0; i < 4; i++)
            {
                ActionDisplay display = this.DoPrioritySelect(targetList, numArray[i, 0], numArray[i, 1]);
                if (display != null)
                {
                    return display;
                }
            }
            return null;
        }

        private void ProperMove()
        {
            if ((base._currentTargetDisplay.GoBase == null) || (base._currentTargetDisplay.Controller == null))
            {
                this._isNeedRefreshTarget = true;
            }
            else
            {
                this.SetAiStatusButNotClearTarget(AiEnum.PrivateAiStatus.RUN_TO_TARGET);
                this.StartChase(base._currentTargetDisplay.GoBase.transform.position);
            }
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.PRIVATE_PVP;
        }

        public override void SetTargetDisplay(ActionDisplay ad)
        {
            base._currentTargetDisplay = ad;
            base._targetVo = ad.GetMeVoByType<BaseRoleVo>();
            base._targetLastCamp = base._targetVo.Camp;
            if (!AppMap.Instance.IsInWifiPVP)
            {
                this.InformTargeted(base._currentTargetDisplay);
                AiTargetManager.instance.MonsterUpdateTarget(base.MeController, base._currentTargetDisplay.Controller as ActionControler);
                AiTargetManager.InformIdleMonsterEnterBattle(this._meMonsterVo.groupIndex, base._currentTargetDisplay);
            }
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            if (<>f__am$cache10 == null)
            {
                <>f__am$cache10 = monsterDisplay => new <>__AnonType23<MonsterDisplay, MonsterVo>(monsterDisplay, monsterDisplay.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cache11 == null)
            {
                <>f__am$cache11 = <>__TranspIdent32 => <>__TranspIdent32.monsterDisplay;
            }
            List<ActionDisplay> list = (from <>__TranspIdent32 in AppMap.Instance.monsterList.Select<MonsterDisplay, <>__AnonType23<MonsterDisplay, MonsterVo>>(<>f__am$cache10)
                where ((<>__TranspIdent32.mon.Controller != null) && !<>__TranspIdent32.mon.IsEmptyHp) && (<>__TranspIdent32.mon.Camp != base.MyCamp)
                select <>__TranspIdent32).Select<<>__AnonType23<MonsterDisplay, MonsterVo>, MonsterDisplay>(<>f__am$cache11).Cast<ActionDisplay>().ToList<ActionDisplay>();
            if (<>f__am$cache12 == null)
            {
                <>f__am$cache12 = display => new <>__AnonType20<PlayerDisplay, BaseRoleVo>(display, display.GetVo());
            }
            if (<>f__am$cache13 == null)
            {
                <>f__am$cache13 = <>__TranspIdent33 => !<>__TranspIdent33.nowDisplay.IsEmptyHp && (<>__TranspIdent33.nowDisplay.Controller != null);
            }
            if (<>f__am$cache14 == null)
            {
                <>f__am$cache14 = <>__TranspIdent33 => <>__TranspIdent33.display;
            }
            list.AddRange((from <>__TranspIdent33 in AppMap.Instance.playerList.Select<PlayerDisplay, <>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cache12).Where<<>__AnonType20<PlayerDisplay, BaseRoleVo>>(<>f__am$cache13)
                where (<>__TranspIdent33.nowDisplay.SubType != 1) && (<>__TranspIdent33.nowDisplay.Camp != base.MyCamp)
                select <>__TranspIdent33).Select<<>__AnonType20<PlayerDisplay, BaseRoleVo>, PlayerDisplay>(<>f__am$cache14).Cast<ActionDisplay>());
            if (<>f__am$cache15 == null)
            {
                <>f__am$cache15 = <>__TranspIdent34 => new AiControllerBase.KV(<>__TranspIdent34.ad, <>__TranspIdent34.distance);
            }
            List<AiControllerBase.KV> targetList = (from ad in list
                select new <>__AnonType21<ActionDisplay, float>(ad, this.GetDistanceFromMe(ad.GoBase.transform.position)) into <>__TranspIdent34
                where <>__TranspIdent34.distance <= this._sightRange
                select <>__TranspIdent34).Select<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV>(<>f__am$cache15).ToList<AiControllerBase.KV>();
            if (targetList.Count >= 1)
            {
                return this.PrioritySelect(targetList);
            }
            this.CleanTargetDisplay();
            return false;
        }

        private void StartChase(Vector3 pos)
        {
            if (this.GetDistanceFromMe(pos) <= 0.3f)
            {
                this._walkFailCount++;
            }
            else
            {
                Vector3 vector = DamageCheck.GetFinalDestinationBeforeChase(pos, base._selfTransform, this._atkRange);
                ActionVo vo = new ActionVo {
                    Destination = vector,
                    ActionType = "run",
                    IsMoveDirectly = true,
                    IsAStarMove = true,
                    Callback = delegate (BaseControler bc) {
                        bc.IsMoveDirectly = false;
                        bc.IsAStarMove = false;
                        this.SetAiStatus(AiEnum.PrivateAiStatus.THINK);
                    }
                };
                base.MeController.AttackController.AddAttackList(vo, false, false);
                base._stopTime = 0f;
            }
        }

        public int _currentPathStep { get; private set; }

        public int _maxPathStep { get; private set; }
    }
}

