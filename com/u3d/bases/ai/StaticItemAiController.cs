﻿namespace com.u3d.bases.ai
{
    using com.game.module.fight.vo;
    using com.u3d.bases.display;
    using System;
    using System.Runtime.InteropServices;

    internal class StaticItemAiController : AiControllerBase
    {
        protected override void AiStateDecision(float timeElapsed)
        {
        }

        public override void CleanTargetDisplay()
        {
        }

        protected override void InitLoopInterval()
        {
            base._loop_interval = float.MaxValue;
            this.SetAi(false);
        }

        protected override void InitRanges()
        {
        }

        public override void LookAt(ActionVo attackVo)
        {
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.STATIC_ITEM;
        }

        public override void SetTargetDisplay(ActionDisplay ad)
        {
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            return false;
        }
    }
}

