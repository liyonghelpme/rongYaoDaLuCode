﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.data;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.Role;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.fsmUtil;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    internal class SummonMonsterAiController : AiControllerBase
    {
        private float _atkRange;
        private bool _isNeedRefreshTarget;
        private AiControllerBase _masterAi;
        private ActionDisplay _masterDisplay;
        private Transform _masterTrans;
        private BaseRoleVo _masterVo;
        private float _maxLiveTime;
        private float _maxRangeToMaster;
        private MonsterVo _meMonsterVo;
        [CompilerGenerated]
        private static Func<ActionDisplay, <>__AnonType20<ActionDisplay, BaseRoleVo>> <>f__am$cache10;
        [CompilerGenerated]
        private static Func<<>__AnonType20<ActionDisplay, BaseRoleVo>, bool> <>f__am$cache11;
        [CompilerGenerated]
        private static Func<<>__AnonType20<ActionDisplay, BaseRoleVo>, bool> <>f__am$cache12;
        [CompilerGenerated]
        private static Func<<>__AnonType20<ActionDisplay, BaseRoleVo>, ActionDisplay> <>f__am$cache13;
        [CompilerGenerated]
        private static Func<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV> <>f__am$cache14;
        [CompilerGenerated]
        private static Func<AiControllerBase.KV, float> <>f__am$cache15;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<ActionDisplay, bool> <>f__am$cacheC;
        [CompilerGenerated]
        private static Func<ActionDisplay, <>__AnonType19<ActionDisplay, MonsterVo>> <>f__am$cacheD;
        [CompilerGenerated]
        private static Func<<>__AnonType19<ActionDisplay, MonsterVo>, bool> <>f__am$cacheE;
        [CompilerGenerated]
        private static Func<<>__AnonType19<ActionDisplay, MonsterVo>, ActionDisplay> <>f__am$cacheF;
        private bool deadFlag;
        private bool initFlag;

        protected override void AiStateDecision(float timeEplased)
        {
        }

        private bool CheckIsRunningToSomewhere()
        {
            return (base._ai_status == 10);
        }

        protected override bool CheckShouldSkipThink()
        {
            return this.CheckIsDoingSkill();
        }

        private bool CheckUseAttack()
        {
            if (base._targetVo.IsEmptyHp || (base._currentTargetDisplay.GoBase == null))
            {
                this._isNeedRefreshTarget = true;
                return true;
            }
            float num = Util.XZSqrMagnitude(base._currentTargetDisplay.GoBase.transform, base._selfTransform.position);
            bool flag = false;
            if (num <= this._atkRange.Square())
            {
                flag = true;
                if ((base._nowTime - this.LastUseAttackTime) >= 0f)
                {
                    this.DoAttack();
                    base._stopTime = 0f;
                    base.LastUseAttackTime = base._nowTime;
                    return flag;
                }
                AiControllerBase.CacheAiBehavior item = new AiControllerBase.CacheAiBehavior {
                    cacheType = 1,
                    lastDisplay = base._currentTargetDisplay
                };
                base._CacheAiBehaviorList.Add(item);
            }
            return flag;
        }

        public override void CleanTargetDisplay()
        {
            base._currentTargetDisplay = null;
            base._targetVo = null;
        }

        private void Die()
        {
            this._meMonsterVo.ReduceHpToPercent((uint) 0);
            ActionVo vo = new ActionVo {
                ActionType = "death"
            };
            base.MeController.AttackController.AddAttackList(vo, false, false);
        }

        private void DoAttack()
        {
            this.SummonAiAttackBehavior();
            base._stopTime = 0f;
        }

        public GameObject GetMaster()
        {
            if (!this.initFlag)
            {
                this.Init();
            }
            if (this._masterDisplay == null)
            {
                return null;
            }
            return this._masterDisplay.GoBase;
        }

        public float GetMaxDistanceFromMaster()
        {
            return this._maxRangeToMaster;
        }

        protected override void Init()
        {
            if (!this.initFlag)
            {
                base.BaseRoleVo = this._meMonsterVo = base.MeController.Me.GetMeVoByType<MonsterVo>();
                base._selfTransform = base.transform;
                base.Init();
                int displayType = (this._meMonsterVo.parent_template_id != 0) ? 400 : 100;
                ulong displayId = this._meMonsterVo.parent_id;
                ActionDisplay actionDisplayByDisplayTypeAndId = SkillControl.GetActionDisplayByDisplayTypeAndId(displayType, displayId);
                if (actionDisplayByDisplayTypeAndId == null)
                {
                    this.SetAi(false);
                }
                else
                {
                    this._masterDisplay = actionDisplayByDisplayTypeAndId;
                    this._masterTrans = actionDisplayByDisplayTypeAndId.GoBase.transform;
                    this._masterVo = actionDisplayByDisplayTypeAndId.GetMeVoByType<BaseRoleVo>();
                    this._masterAi = actionDisplayByDisplayTypeAndId.Controller.AiController;
                    this._maxLiveTime = this._meMonsterVo.max_alive_time;
                    this.SetAi(true);
                    this.initFlag = true;
                    if (this._masterDisplay is MeDisplay)
                    {
                        PlayerAiManager.instance.AddSummonMonster(base.MeController.Me.GetMeByType<MonsterDisplay>());
                    }
                }
            }
        }

        protected override void InitLoopInterval()
        {
            base._loop_interval = 0.2f;
            base._tick = UnityEngine.Random.Range(0f, base._loop_interval);
            base._stopTime = UnityEngine.Random.Range((float) 0f, (float) 1f);
        }

        protected override void InitRanges()
        {
            this._maxRangeToMaster = this._meMonsterVo.MonsterVO.sight_range * 0.001f;
            this._atkRange = this._meMonsterVo.MonsterVO.attack_range * 0.001f;
        }

        private bool IsNearMaster()
        {
            if (this._masterDisplay == null)
            {
                return false;
            }
            return (Util.XZSqrMagnitude(this._masterTrans, base._selfTransform.position) < this._maxRangeToMaster.Square());
        }

        protected override Func<bool> IsNeedToSetNewTargetDisplay()
        {
            if (<>f__am$cacheB == null)
            {
                <>f__am$cacheB = () => false;
            }
            return <>f__am$cacheB;
        }

        private void MoveToMaster()
        {
            float num = UnityEngine.Random.Range((float) 135f, (float) 225f);
            float num2 = AiConfig.MELEE_GENERAL_STAND_DIS_FROM_MASTER_IN_PVE * 0.001f;
            num2 *= 0.9f;
            Vector3 eulerAngles = this._masterTrans.rotation.eulerAngles;
            Vector3 vector2 = (Vector3) ((Quaternion.Euler(eulerAngles.x, eulerAngles.y + num, eulerAngles.z) * Vector3.forward) * num2);
            Vector3 dest = this._masterTrans.position + vector2;
            this.StartChase(dest, true);
        }

        private void MoveToTarget()
        {
            Log.AI(base.gameObject, " Move Destination " + base._currentTargetDisplay);
            Vector3 dest = DamageCheck.GetFinalDestinationBeforeChase(base._currentTargetDisplay.GoBase.transform.position, base._selfTransform, this._atkRange);
            this.StartChase(dest, false);
        }

        protected override void OnDestroy()
        {
            if (this._masterDisplay is MeDisplay)
            {
                PlayerAiManager.instance.RemoveSummonMonster(base.MeController.GetMeByType<MonsterDisplay>());
            }
            base.OnDestroy();
        }

        protected override void PrepareWork()
        {
            this._maxLiveTime -= base._loop_interval;
            if ((this._maxLiveTime <= 0f) && !this.deadFlag)
            {
                this.Die();
                this.IsAiEnable = false;
                this.deadFlag = true;
            }
            if (this._masterVo.IsEmptyHp)
            {
                this.Die();
                this.IsAiEnable = false;
                this.deadFlag = true;
            }
            if ((this.IsInBattle && !this._meMonsterVo.IsEmptyHp) && ((this._masterDisplay is MeDisplay) && GlobalData.isInCopy))
            {
                this.PlayerEndBattle();
            }
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.SUMMON_MONSTER;
        }

        public override void SetTargetDisplay(ActionDisplay ad)
        {
            if (ad != null)
            {
                base._currentTargetDisplay = ad;
                base._targetVo = ad.GetMeVoByType<BaseRoleVo>();
                base._targetLastCamp = base._targetVo.Camp;
                AiTargetManager.instance.MeUpdateTarget(base.MeController, base._currentTargetDisplay.Controller as ActionControler);
            }
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            ActionDisplay targetDisplay;
            if (this._masterAi.IsAiEnable)
            {
                targetDisplay = this._masterAi.GetTargetDisplay();
            }
            else
            {
                ActionDisplay lastTarget = this._masterVo.Controller.SkillController.LastTarget;
                if (lastTarget != null)
                {
                    targetDisplay = lastTarget;
                }
                else
                {
                    targetDisplay = this._masterVo.Controller.LastAttacker;
                }
            }
            if (targetDisplay != null)
            {
                this.SetTargetDisplay(targetDisplay);
                return true;
            }
            base.attackable_list.Clear();
            if (<>f__am$cacheC == null)
            {
                <>f__am$cacheC = display => display.Controller != null;
            }
            if (<>f__am$cacheD == null)
            {
                <>f__am$cacheD = display => new <>__AnonType19<ActionDisplay, MonsterVo>(display, display.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cacheE == null)
            {
                <>f__am$cacheE = <>__TranspIdent35 => !<>__TranspIdent35.monsterVo.IsEmptyHp;
            }
            if (<>f__am$cacheF == null)
            {
                <>f__am$cacheF = <>__TranspIdent35 => <>__TranspIdent35.display;
            }
            base.attackable_list.AddRange((from display in AppMap.Instance.monsterList.Cast<ActionDisplay>()
                where display.GetMeVoByType<BaseRoleVo>().Camp != this._meMonsterVo.Camp
                select display).Where<ActionDisplay>(<>f__am$cacheC).Select<ActionDisplay, <>__AnonType19<ActionDisplay, MonsterVo>>(<>f__am$cacheD).Where<<>__AnonType19<ActionDisplay, MonsterVo>>(<>f__am$cacheE).Select<<>__AnonType19<ActionDisplay, MonsterVo>, ActionDisplay>(<>f__am$cacheF));
            if (<>f__am$cache10 == null)
            {
                <>f__am$cache10 = display => new <>__AnonType20<ActionDisplay, BaseRoleVo>(display, display.GetMeVoByType<BaseRoleVo>());
            }
            if (<>f__am$cache11 == null)
            {
                <>f__am$cache11 = <>__TranspIdent36 => !<>__TranspIdent36.nowDisplay.IsEmptyHp && (<>__TranspIdent36.nowDisplay.Controller != null);
            }
            if (<>f__am$cache12 == null)
            {
                <>f__am$cache12 = <>__TranspIdent36 => <>__TranspIdent36.nowDisplay.SubType != 1;
            }
            if (<>f__am$cache13 == null)
            {
                <>f__am$cache13 = <>__TranspIdent36 => <>__TranspIdent36.display;
            }
            base.attackable_list.AddRange((from <>__TranspIdent36 in AppMap.Instance.playerList.Cast<ActionDisplay>().Select<ActionDisplay, <>__AnonType20<ActionDisplay, BaseRoleVo>>(<>f__am$cache10)
                where <>__TranspIdent36.nowDisplay.Camp != this._meMonsterVo.Camp
                select <>__TranspIdent36).Where<<>__AnonType20<ActionDisplay, BaseRoleVo>>(<>f__am$cache11).Where<<>__AnonType20<ActionDisplay, BaseRoleVo>>(<>f__am$cache12).Select<<>__AnonType20<ActionDisplay, BaseRoleVo>, ActionDisplay>(<>f__am$cache13));
            if (<>f__am$cache14 == null)
            {
                <>f__am$cache14 = <>__TranspIdent37 => new AiControllerBase.KV(<>__TranspIdent37.ad, <>__TranspIdent37.distance);
            }
            List<AiControllerBase.KV> source = (from ad in base.attackable_list
                select new <>__AnonType21<ActionDisplay, float>(ad, Util.XZSqrMagnitude(ad.GoBase.transform, base._selfTransform.position)) into <>__TranspIdent37
                where <>__TranspIdent37.distance <= this._maxRangeToMaster.Square()
                select <>__TranspIdent37).Select<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV>(<>f__am$cache14).ToList<AiControllerBase.KV>();
            if (source.Count < 1)
            {
                this.CleanTargetDisplay();
                return false;
            }
            if (<>f__am$cache15 == null)
            {
                <>f__am$cache15 = x => x.distance;
            }
            source = source.OrderBy<AiControllerBase.KV, float>(<>f__am$cache15).ToList<AiControllerBase.KV>();
            this.SetTargetDisplay(source[0].ad);
            return true;
        }

        private void StartChase(Vector3 dest, bool isMoveToMaster)
        {
            <StartChase>c__AnonStorey109 storey = new <StartChase>c__AnonStorey109 {
                isMoveToMaster = isMoveToMaster,
                <>f__this = this
            };
            if (this._meMonsterVo.stateInfo.CanMove)
            {
                ActionVo vo = new ActionVo {
                    Destination = dest,
                    ActionType = "run",
                    IsAStarMove = true,
                    Preparework = new PreMoveCallBack(storey.<>m__1A2),
                    Callback = new MoveEndCallback(storey.<>m__1A3)
                };
                base.MeController.AttackController.AddAttackList(vo, false, false);
                base._stopTime = 0f;
            }
        }

        private void SummonAiAttackBehavior()
        {
            if ((base._currentTargetDisplay.GoBase == null) || base._targetVo.IsEmptyHp)
            {
                this._isNeedRefreshTarget = true;
            }
            else
            {
                ActionVo vo = new ActionVo {
                    ActionType = "attack",
                    IsIgnoreDistance = true,
                    Target = base._currentTargetDisplay
                };
                base._selfTransform.LookAtTarget(base._currentTargetDisplay);
                base.MeController.AttackController.AddAttackList(vo, false, true);
                base.MeController.AttackController.AddAttackList(vo, false, true);
                base.MeController.AttackController.AddAttackList(vo, false, true);
            }
        }

        protected override void Targeted()
        {
            if (GlobalData.isInCopy && (this._masterDisplay is MeDisplay))
            {
                PlayerAiManager.instance.BeginBattle();
            }
        }

        [CompilerGenerated]
        private sealed class <StartChase>c__AnonStorey109
        {
            internal SummonMonsterAiController <>f__this;
            internal bool isMoveToMaster;

            internal void <>m__1A2(BaseControler mc)
            {
                this.<>f__this.SetAiStatus(!this.isMoveToMaster ? AiEnum.SummonAiStatus.RUN_TO_MASTER : AiEnum.SummonAiStatus.RUN_TO_TARGET);
                Log.AI(this.<>f__this.gameObject, " set AiStatus");
            }

            internal void <>m__1A3(BaseControler bc)
            {
                bc.IsAStarMove = false;
                bc.IsMoveDirectly = false;
                this.<>f__this.SetAiStatusButNotClearTarget(AiEnum.SummonAiStatus.THINK);
                CommandHandler.AddCommandStatic(this.<>f__this.MeController.Me.GoBase, "idle", 1f);
            }
        }
    }
}

