﻿namespace com.u3d.bases.ai
{
    using com.game;
    using com.game.module.fight.vo;
    using com.game.module.map;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    internal class TowerAiController : AiControllerBase
    {
        private float _attack_range;
        private MonsterVo _meMonsterVo;
        [CompilerGenerated]
        private static Func<MonsterDisplay, bool> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<MonsterDisplay, <>__AnonType24<MonsterDisplay, MonsterVo>> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<<>__AnonType24<MonsterDisplay, MonsterVo>, bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<<>__AnonType24<MonsterDisplay, MonsterVo>, <>__AnonType25<<>__AnonType24<MonsterDisplay, MonsterVo>, int>> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<<>__AnonType25<<>__AnonType24<MonsterDisplay, MonsterVo>, int>, bool> <>f__am$cache8;
        [CompilerGenerated]
        private static Func<<>__AnonType25<<>__AnonType24<MonsterDisplay, MonsterVo>, int>, MonsterDisplay> <>f__am$cache9;
        [CompilerGenerated]
        private static Func<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV> <>f__am$cacheA;
        [CompilerGenerated]
        private static Func<AiControllerBase.KV, float> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<AiControllerBase.KV, float> <>f__am$cacheC;
        public float DistanceToTarget;
        public ActionDisplay TargetDisplay;

        protected override void AiStateDecision(float timeElapsed)
        {
            if ((base._currentTargetDisplay != null) && !this.CheckIsDoingAttack())
            {
                this.CheckUseAttack();
            }
        }

        protected override bool CheckCanUseAi()
        {
            bool flag = !this.CheckIsDead();
            bool isAllReloaded = SelfPlayerManager.instance.isAllReloaded;
            bool flag3 = MonsterMgr.CanSetAi && this.IsAiEnable;
            return ((flag && isAllReloaded) && flag3);
        }

        private bool CheckIsDoingAttack()
        {
            return StatuControllerBase.IsStatusHashDoingAttack(base._nowStatusHash);
        }

        private bool CheckUseAttack()
        {
            if ((base._nowTime - this.LastUseAttackTime) > 1f)
            {
                if (this._meMonsterVo.IsEmptyHp)
                {
                    return true;
                }
                if (this.GetTargetDistance(base._currentTargetDisplay) <= this._attack_range)
                {
                    ActionVo vo = new ActionVo {
                        ActionType = "attack",
                        IsIgnoreDistance = true,
                        Target = base._currentTargetDisplay
                    };
                    base.MeController.AttackController.AddAttackList(vo, false, false);
                    base.LastUseAttackTime = base._nowTime;
                    return true;
                }
                this.CleanTargetDisplay();
            }
            return false;
        }

        public override void CleanTargetDisplay()
        {
            base._targetVo = null;
            base._currentTargetDisplay = null;
            if (!AppMap.Instance.IsInWifiPVP)
            {
                AiTargetManager.instance.MonsterUpdateTarget(base.MeController, null);
            }
        }

        protected override void Init()
        {
            base.BaseRoleVo = this._meMonsterVo = base.MeController.Me.GetMeVoByType<MonsterVo>();
            base._selfTransform = base.transform;
            base.Init();
        }

        protected override void InitLoopInterval()
        {
            base._loop_interval = 0.1f;
        }

        protected override void InitRanges()
        {
            this._attack_range = this._meMonsterVo.MonsterVO.attack_range * 0.001f;
        }

        protected override Func<bool> IsNeedToSetNewTargetDisplay()
        {
            return () => ((((base._targetVo == null) || base._targetVo.IsEmptyHp) || ((base._currentTargetDisplay == null) || (base._currentTargetDisplay.Controller == null))) || ((this.GetTargetDistance((ActionDisplay) null) > this._attack_range) || (base._targetVo.Camp != base._targetLastCamp)));
        }

        public override void LookAt(ActionVo attackVo)
        {
        }

        protected override void SetAiScriptType()
        {
            base._ai_type = AiEnum.AiScriptType.TOWER;
        }

        public override void SetTargetDisplay(ActionDisplay ad)
        {
            if (ad != null)
            {
                base._currentTargetDisplay = ad;
                base._targetVo = base._currentTargetDisplay.GetMeVoByType<BaseRoleVo>();
                base._targetLastCamp = base._targetVo.Camp;
            }
            if (!AppMap.Instance.IsInWifiPVP)
            {
                this.InformTargeted(base._currentTargetDisplay);
                AiTargetManager.instance.MonsterUpdateTarget(base.MeController, base._currentTargetDisplay.Controller as ActionControler);
                AiTargetManager.InformIdleMonsterEnterBattle(this._meMonsterVo.groupIndex, base._currentTargetDisplay);
            }
        }

        protected override bool SetTargetDisplay(bool isTest = false)
        {
            base.attackable_list.Clear();
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = ad => (ad != null) && (ad.Controller != null);
            }
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = ad => new <>__AnonType24<MonsterDisplay, MonsterVo>(ad, ad.GetMeVoByType<MonsterVo>());
            }
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = <>__TranspIdent38 => !<>__TranspIdent38.vo.IsEmptyHp;
            }
            if (<>f__am$cache7 == null)
            {
                <>f__am$cache7 = <>__TranspIdent38 => new <>__AnonType25<<>__AnonType24<MonsterDisplay, MonsterVo>, int>(<>__TranspIdent38, <>__TranspIdent38.vo.MonsterVO.type);
            }
            if (<>f__am$cache8 == null)
            {
                <>f__am$cache8 = <>__TranspIdent39 => ((<>__TranspIdent39.monType == 4) || (<>__TranspIdent39.monType == 1)) || (<>__TranspIdent39.monType == 6);
            }
            if (<>f__am$cache9 == null)
            {
                <>f__am$cache9 = <>__TranspIdent39 => <>__TranspIdent39.<>__TranspIdent38.ad;
            }
            IEnumerator<MonsterDisplay> enumerator = (from <>__TranspIdent38 in AppMap.Instance.monsterList.Where<MonsterDisplay>(<>f__am$cache4).Select<MonsterDisplay, <>__AnonType24<MonsterDisplay, MonsterVo>>(<>f__am$cache5)
                where <>__TranspIdent38.vo.Camp != this._meMonsterVo.Camp
                select <>__TranspIdent38).Where<<>__AnonType24<MonsterDisplay, MonsterVo>>(<>f__am$cache6).Select<<>__AnonType24<MonsterDisplay, MonsterVo>, <>__AnonType25<<>__AnonType24<MonsterDisplay, MonsterVo>, int>>(<>f__am$cache7).Where<<>__AnonType25<<>__AnonType24<MonsterDisplay, MonsterVo>, int>>(<>f__am$cache8).Select<<>__AnonType25<<>__AnonType24<MonsterDisplay, MonsterVo>, int>, MonsterDisplay>(<>f__am$cache9).GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    base.attackable_list.Add(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            if (<>f__am$cacheA == null)
            {
                <>f__am$cacheA = <>__TranspIdent40 => new AiControllerBase.KV(<>__TranspIdent40.ad, <>__TranspIdent40.distance);
            }
            List<AiControllerBase.KV> source = (from ad in base.attackable_list
                select new <>__AnonType21<ActionDisplay, float>(ad, Util.XZSqrMagnitude(ad.GoBase.transform, base._selfTransform)) into <>__TranspIdent40
                where <>__TranspIdent40.distance < this._attack_range.Square()
                select <>__TranspIdent40).Select<<>__AnonType21<ActionDisplay, float>, AiControllerBase.KV>(<>f__am$cacheA).ToList<AiControllerBase.KV>();
            if (source.Count >= 1)
            {
                if (<>f__am$cacheB == null)
                {
                    <>f__am$cacheB = x => x.distance;
                }
                source = source.OrderBy<AiControllerBase.KV, float>(<>f__am$cacheB).ToList<AiControllerBase.KV>();
                this.SetTargetDisplay(source[0].ad);
                return true;
            }
            base.attackable_list.Clear();
            source.Clear();
            IEnumerator<PlayerDisplay> enumerator2 = AppMap.Instance.playerList.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    PlayerDisplay display = enumerator2.Current;
                    BaseRoleVo meVoByType = display.GetMeVoByType<BaseRoleVo>();
                    if (!meVoByType.IsEmptyHp && (meVoByType.Controller != null))
                    {
                        if (!AppMap.Instance.IsInWifiPVP)
                        {
                            float dis = Util.XZSqrMagnitude(display.GoBase.transform, base._selfTransform);
                            if (dis <= this._attack_range.Square())
                            {
                                source.Add(new AiControllerBase.KV(display, dis));
                            }
                        }
                        else if (meVoByType.SubType != 1)
                        {
                            float num2 = Util.XZSqrMagnitude(display.GoBase.transform, base._selfTransform);
                            if (num2 <= this._attack_range.Square())
                            {
                                source.Add(new AiControllerBase.KV(display, num2));
                            }
                        }
                    }
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
            if (source.Count >= 1)
            {
                if (<>f__am$cacheC == null)
                {
                    <>f__am$cacheC = x => x.distance;
                }
                source = source.OrderBy<AiControllerBase.KV, float>(<>f__am$cacheC).ToList<AiControllerBase.KV>();
                this.SetTargetDisplay(source[0].ad);
                return true;
            }
            this.CleanTargetDisplay();
            return false;
        }

        public void TurnToMyHerosEnemy(PlayerDisplay pd)
        {
            if (!pd.GetVo().IsEmptyHp && !this._meMonsterVo.IsEmptyHp)
            {
                Debug.Log("我方英雄攻击地方英雄, 塔立即获得该目标");
                this.SetTargetDisplay(pd);
            }
        }
    }
}

