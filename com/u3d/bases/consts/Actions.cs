﻿namespace com.u3d.bases.consts
{
    using System;

    public class Actions
    {
        public const int Assist4Friend = 0x3ed;
        public const int Assist4Self = 0x3ec;
        public const string ATTACK = "attack";
        public const string ATTACK1 = "attack1";
        public const string ATTACK2 = "attack2";
        public const string ATTACK3 = "attack3";
        public const string ATTACK4 = "attack4";
        public const string BUFF = "buff";
        public const string CONJURE = "conjure";
        public const string DEATH = "death";
        public const string DEATHFLY = "deathfly";
        public const string DEATHWAIT = "deathwait";
        public const string DEFENSE = "defense";
        public const string DOUBLE = "double";
        public const string EFFECT = "effect";
        public const string FIGHT = "fight";
        public const string FLY = "fly";
        public const string HAND = "hand";
        public const string HIT = "hit";
        public const int HurtFly = 0x3eb;
        public const int HurtNormal = 0x3e9;
        public const string INJURED = "injured";
        public const string KNOCK_BACK = "knockback";
        public const string KNOCK_DOWN = "knockdown";
        public const int KnockBack = 0x3ef;
        public const int KnockDown = 0x3ee;
        public const int KnockRise = 0x3f0;
        public const string MAGIC = "magic";
        public const string PARRY = "parry";
        public const string RIDE_RUN = "riderun";
        public const string RIDE_WAIT = "ridewait";
        public const string RUN = "run";
        public const string SIT = "sit";
        public const string SKILL1 = "skill1";
        public const string SKILL2 = "skill2";
        public const string SKILL3 = "skill3";
        public const string SKILL4 = "skill4";
        public const string SPECIAL = "special";
        public const string STAND = "stand";
        public const string STORE = "store";
        public const string SUPERSKILL = "superskill";
        public const string SUPERSKILL1 = "superskill1";
        public const string WAIT = "wait";
        public const string WALK = "walk";
        public const string WINS = "wins";
    }
}

