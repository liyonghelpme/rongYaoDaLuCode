﻿namespace com.u3d.bases.consts
{
    using System;
    using UnityEngine;

    public class Directions
    {
        public const int Down = 3;
        public const int DownLeft = 8;
        public const int DownRight = 7;
        public const int Left = 4;
        public const int Right = 2;
        public const int Top = 1;
        public const int TopLeft = 6;
        public const int TopRight = 5;

        public static int GetDirByVector2(Vector2 pos)
        {
            int num = 0;
            pos = pos.normalized;
            if (pos.x > 0f)
            {
                float num2 = pos.y / pos.x;
                if (num2 > 5f)
                {
                    return 1;
                }
                if (num2 > 0.5f)
                {
                    return 5;
                }
                if (num2 > -0.5f)
                {
                    return 2;
                }
                if (num2 > -5f)
                {
                    return 7;
                }
                return 3;
            }
            if (pos.x < 0f)
            {
                float num3 = pos.y / pos.x;
                if (num3 < -5f)
                {
                    return 1;
                }
                if (num3 < -0.5f)
                {
                    return 6;
                }
                if (num3 < 0.5f)
                {
                    return 4;
                }
                if (num3 < 5f)
                {
                    return 8;
                }
                return 3;
            }
            if (pos.y > 0f)
            {
                return 1;
            }
            if (pos.y < 0f)
            {
                num = 3;
            }
            return num;
        }
    }
}

