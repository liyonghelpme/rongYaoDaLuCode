﻿namespace com.u3d.bases.consts
{
    using System;

    public class DisplayType
    {
        public const int BATTLE_OBJECT = 800;
        public const int CHEST = 700;
        public const int COPY_POINT = 600;
        public const int MAP_POINT = 500;
        public const int MONSTER = 400;
        public const int NPC = 200;
        public const int ROLE = 100;
        public const int ROLE_MODE = 0x65;
        public const int TRAP = 0x1f5;
    }
}

