﻿namespace com.u3d.bases.consts
{
    using System;

    public class MapConst
    {
        public enum MapType
        {
            ACTIVITY = 3,
            ARENA = 6,
            CROSS = 5,
            DUNGEON = 2,
            FIELD = 4,
            TOWN = 1
        }
    }
}

