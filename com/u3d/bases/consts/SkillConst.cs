﻿namespace com.u3d.bases.consts
{
    using System;

    public class SkillConst
    {
        public const uint Assassin_Attack1_ID = 0x11171;
        public const uint Assassin_Attack2_ID = 0x11172;
        public const uint Assassin_Attack3_ID = 0x11173;
        public const uint Assassin_Attack4_ID = 0x11174;
        public const uint Assassin_Skill2_ID_Max = 0x11945;
        public const uint Assassin_Skill2_ID_Min = 0x11940;
        public const uint Assassin_Skill3_ID_Max = 0x11d2d;
        public const uint Assassin_Skill3_ID_Min = 0x11d28;
        public const uint Assassin_Skill4_ID_Max = 0x12115;
        public const uint Assassin_Skill4_ID_Min = 0x12110;
        public const int ATT_TYPE_MAG = 2;
        public const int ATT_TYPE_PHY = 1;
        public const int ATT_TYPE_RE_HP = 3;
        public const uint DAMAGE_TYPE_CRIT = 2;
        public const uint DAMAGE_TYPE_MISS = 0;
        public const uint DAMAGE_TYPE_NORMAL = 3;
        public const int GENERAL_MAX_SKILL_NO = 6;
        public const int GENERAL_MIN_SKILL_NO = 4;
        public const uint Magic_Attack1_ID = 0xea61;
        public const uint Magic_Attack2_ID = 0xea62;
        public const uint Magic_Attack3_ID = 0xea63;
        public const int RESTRICTED_DENOMINATOR = 0x2710;
        public const uint SKILL_DAMAGE_STATE_KNOCK_BACK = 3;
        public const uint SKILL_DAMAGE_STATE_KNOCK_DOWN = 2;
        public const uint SKILL_DAMAGE_STATE_NONE = 0;
        public const float SKILL_RANGE_DENOMINATOR = 0.001f;
        public const int SKILL_SHAPE_CIRCLE = 0;
        public const int SKILL_SHAPE_HALF_CIRCLE = 1;
        public const int SKILL_SHAPE_RECTANGLE = 3;
        public const int SKILL_SHAPE_SECTOR = 2;
        public const int SKILL_TARGET_ENEMY = 3;
        public const int SKILL_TARGET_FRIEND = 2;
        public const int SKILL_TARGET_SELF = 1;
        public const uint Sword_Attack1_ID = 0x2711;
        public const uint Sword_Attack2_ID = 0x2712;
        public const uint Sword_Attack3_ID = 0x2713;
        public const uint Sword_Attack4_ID = 0x2714;

        public enum BlockSpecialSubSkill
        {
            unavailable,
            temptation,
            culling_blade,
            floating
        }

        public enum CullingBladeType
        {
            nope,
            yes_and_value_given,
            yes_and_pecentage_give
        }

        public enum HealSkillControlType
        {
            damn_just_use_it,
            one_member_below_value,
            one_member_below_ratio,
            all_member_below_value,
            all_member_below_ratio
        }

        public enum HeroSkillSubtype
        {
            activation = 0x18,
            area_buff = 15,
            blink_destination = 5,
            blink_distance = 4,
            buff_targeted = 12,
            catapult = 0x11,
            change = 3,
            common = 0,
            culling_blade = 11,
            damage_and_add_hp = 0x12,
            floating = 20,
            heal = 1,
            hp_protected = 0x10,
            invisable = 6,
            knock_back = 0x15,
            rebound_damage = 9,
            rebound_skill = 8,
            run_straight = 14,
            shield = 7,
            ship_skill = 0x13,
            splash = 0x17,
            summon = 2,
            temptation = 13,
            tied = 10,
            turn_attr_to_other = 0x16,
            unavailable = -1
        }

        public enum MonsterSkillSubtype
        {
        }

        public enum SkillOwnerType
        {
            hero,
            common,
            monster
        }
    }
}

