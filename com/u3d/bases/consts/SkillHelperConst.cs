﻿namespace com.u3d.bases.consts
{
    using System;

    public class SkillHelperConst
    {
        public const int BLINK_DISTANCE = 2;
        public const int FLOAT = 1;
        public const int KNOCK_BACK = 3;
        public const int KNOCK_BACK_AND_DAMAGE_AGAIN = 4;
    }
}

