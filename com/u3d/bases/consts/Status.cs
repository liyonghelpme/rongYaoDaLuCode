﻿namespace com.u3d.bases.consts
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class Status
    {
        public const int ATTACK1 = 3;
        public const int ATTACK1_0 = 0x18;
        public const int ATTACK2 = 4;
        public const int ATTACK2_0 = 0x19;
        public const int ATTACK3 = 5;
        public const int ATTACK4 = 6;
        public const float BATTLE_IDLE = 1f;
        public const int COMB_0 = 0;
        public const int COMB_1 = 1;
        public const int COMB_2 = 2;
        public const int COMB_3 = 3;
        public const int COMB_4 = 4;
        public const float CreateRoleIdleOne = 110f;
        public const float CreateRoleIdleThree = 130f;
        public const float CreateRoleIdleTwo = 120f;
        public const int DEATH = 11;
        public const int DEATHFLY = 0x13;
        public const int DIZZINESS = 20;
        public const int EMPTY = -1;
        public const float FrontIdle = 10f;
        private static Dictionary<int, string> HashToName = new Dictionary<int, string>();
        public const int HURT1 = 10;
        public const int HURT2 = 12;
        public const int HURT3 = 13;
        public const int HURT4 = 14;
        public const int HURTDOWN = 15;
        public const int IDLE = 0;
        public const string IDLE_TYPE = "IdleType";
        public const int LOOP1 = 0x16;
        public const int MAGIC1 = 0x17;
        public static readonly int NAME_HASH_ATTACK1 = SetHash("Base Layer.Attack1");
        public static readonly int NAME_HASH_ATTACK1_0 = SetHash("Base Layer.Attack1_0");
        public static readonly int NAME_HASH_ATTACK2 = SetHash("Base Layer.Attack2");
        public static readonly int NAME_HASH_ATTACK2_0 = SetHash("Base Layer.Attack2_0");
        public static readonly int NAME_HASH_ATTACK3 = SetHash("Base Layer.Attack3");
        public static readonly int NAME_HASH_ATTACK4 = SetHash("Base Layer.Attack4");
        public static readonly int NAME_HASH_DEATH = SetHash("Base Layer.Death");
        public static readonly int NAME_HASH_DEATH_END = SetHash("Base Layer.DeathEnd");
        public static readonly int NAME_HASH_DEATHFLY = SetHash("Base Layer.DeathFly");
        public static readonly int NAME_HASH_DIZZINESS = SetHash("Base Layer.Dizziness");
        public static readonly int NAME_HASH_EMPTY = SetHash("Base Layer.Empty");
        public static readonly int NAME_HASH_HURT1 = SetHash("Base Layer.Hurt1");
        public static readonly int NAME_HASH_HURT2 = SetHash("Base Layer.Hurt2");
        public static readonly int NAME_HASH_HURT3 = SetHash("Base Layer.Hurt3");
        public static readonly int NAME_HASH_HURT4 = SetHash("Base Layer.Hurt4");
        public static readonly int NAME_HASH_HURTDOWN = SetHash("Base Layer.HurtDown");
        public static readonly int NAME_HASH_IDLE = SetHash("Base Layer.Idle");
        public static readonly int NAME_HASH_LOOP1 = SetHash("Base Layer.Loop1");
        public static readonly int NAME_HASH_MAGIC1 = SetHash("Base Layer.Magic1");
        public static readonly int NAME_HASH_RISE = SetHash("Base Layer.Rise");
        public static readonly int NAME_HASH_ROLL = SetHash("Base Layer.Roll");
        public static readonly int NAME_HASH_RUN = SetHash("Base Layer.Run");
        public static readonly int NAME_HASH_SKILL1 = SetHash("Base Layer.Skill1");
        public static readonly int NAME_HASH_SKILL2 = SetHash("Base Layer.Skill2");
        public static readonly int NAME_HASH_SKILL3 = SetHash("Base Layer.Skill3");
        public static readonly int NAME_HASH_SKILL4 = SetHash("Base Layer.Skill4");
        public static readonly int NAME_HASH_SPECIAL = SetHash("Base Layer.Special");
        public static readonly int NAME_HASH_STANDUP = SetHash("Base Layer.StandUp");
        public static readonly int NAME_HASH_WIN = SetHash("Base Layer.Win");
        public const float NORMAL_IDLE = 0f;
        public const int RISE = 0x15;
        public const int ROLL = 2;
        public const int RUN = 1;
        public const int SKILL1 = 7;
        public const int SKILL2 = 8;
        public const int SKILL3 = 9;
        public const int SKILL4 = 0x11;
        public const int STANDUP = 0x10;
        public const string STATU = "State";
        public const int WIN = 0x12;

        public static string GetName(int hash)
        {
            if (HashToName.ContainsKey(hash))
            {
                return HashToName[hash];
            }
            return "ERROR";
        }

        private static int SetHash(string name)
        {
            int num = Animator.StringToHash(name);
            HashToName[num] = name;
            return num;
        }
    }
}

