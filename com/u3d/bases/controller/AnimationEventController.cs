﻿namespace com.u3d.bases.controller
{
    using com.game.data;
    using com.game.module.effect;
    using System;
    using UnityEngine;

    public class AnimationEventController : MonoBehaviour
    {
        private SysSkillBaseVo _skillVo;
        public SkillController skillController;

        public void checkDamanage()
        {
            if ((this._skillVo != null) && (this.skillController != null))
            {
                this.skillController.CheckDamage(base.transform.position, this._skillVo, 0, null, null, 0, 0x3e8);
                this._skillVo = null;
            }
        }

        public void DoMove()
        {
            if (this.skillController != null)
            {
                this.skillController.DoEffectDuringSkill();
            }
        }

        public void Move()
        {
        }

        public void RegisterSkillVo(SysSkillBaseVo vo)
        {
            if (vo == null)
            {
                vo = null;
            }
            else
            {
                this._skillVo = vo;
            }
        }

        public void ScaleCamera()
        {
            if (this.skillController != null)
            {
                this.skillController.ScaleCamera();
            }
        }

        public void showEffect()
        {
            EffectMgr.Instance.CreateSceneEffect("20020", Vector3.zero, base.transform.gameObject, true, true, null, null);
            EffectMgr.Instance.CreateSceneEffect("20021", Vector3.zero, base.transform.gameObject, true, true, null, null);
        }
    }
}

