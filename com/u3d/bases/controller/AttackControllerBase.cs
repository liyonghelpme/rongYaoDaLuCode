﻿namespace com.u3d.bases.controller
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.effect;
    using com.game.module.fight.vo;
    using com.game.utils;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public abstract class AttackControllerBase : MonoBehaviour
    {
        public List<ActionVo> AttackList = new List<ActionVo>();
        public BeAttackedControllerBase BeAttackedController;
        protected ActionVo CurVo;
        public ActionControler MeController;
        protected ActionDisplay Target;

        protected AttackControllerBase()
        {
        }

        public abstract void AddAttackList(ActionVo vo, bool isPush = false, bool fromAi = false);
        public virtual void ClearAttackList()
        {
            this.AttackList.Clear();
        }

        private string GetTargetEffectId(uint skillId, int index, bool isBullet)
        {
            SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(skillId);
            if (sysSkillBaseVo == null)
            {
                Log.info(this, "SkillBaseVo表不存在id为" + skillId + "的技能");
                return null;
            }
            SysSkillActionVo sysSkillActionVo = BaseDataMgr.instance.GetSysSkillActionVo(sysSkillBaseVo.skill_group);
            if (sysSkillActionVo == null)
            {
                Log.info(this, "Action表不存在id为" + sysSkillBaseVo.skill_group + "的技能组");
                return null;
            }
            if (sysSkillActionVo.BulletType == 11)
            {
                return string.Empty;
            }
            string[] valueListFromString = StringUtils.GetValueListFromString(sysSkillActionVo.tar_eff, ',');
            if (index >= valueListFromString.Length)
            {
                return string.Empty;
            }
            if (isBullet)
            {
                return valueListFromString[index];
            }
            int num = UnityEngine.Random.Range(0, valueListFromString.Length);
            return valueListFromString[num];
        }

        public void ShowBeAttackedEffect(ActionVo vo, float zoom = 1f, float rotateZ = 0f)
        {
            if ((vo != null) && (vo.SkillId != 0))
            {
                string effectId = this.GetTargetEffectId(vo.SkillId, vo.HurtEffectIndex, false);
                if (effectId != string.Empty)
                {
                    Vector3 position = this.MeController.transform.position;
                    position.y += this.MeController.characterController.height / 2f;
                    Effect effect = EffectFactory.CreateEffect(effectId, this.MeController.gameObject, null, position, EffectType.Normal, null, 0f, 0, null, false, null, true, null, null);
                    EffectMgr.Instance.CreateSkillEffect(effect);
                }
            }
        }
    }
}

