﻿namespace com.u3d.bases.controller
{
    using com.game.module.fight.vo;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class BeAttackedControllerBase : MonoBehaviour
    {
        protected AiControllerBase ai;
        public ActionControler meController;
        protected BaseRoleVo meVo;

        public virtual void BeAttacked(ActionVo actionVo)
        {
            this.meController.LastBeAttackVo = actionVo;
            this.meController.LastBeAttackedTime = Time.time;
            this.meController.LastAttacker = actionVo.Attacker;
            if (!this.meVo.stateInfo.IsInState(StateVo.FLOAT))
            {
                this.meController.Me.GoBase.transform.position = actionVo.Destination;
                this.HandleBeAttackedStatus(actionVo);
            }
            this.meController.AttackController.ShowBeAttackedEffect(actionVo, 1f, 0f);
            this.meController.Me.StartShowModelBeAttackedEffect();
            this.InformAI(actionVo);
        }

        protected virtual void HandleBeAttackedStatus(ActionVo actionVo)
        {
        }

        protected virtual void InformAI(ActionVo actionVo)
        {
            if (this.ai != null)
            {
                this.ai.BeAttacked(actionVo, Time.time);
            }
        }

        protected virtual void Init()
        {
            this.meVo = this.meController.GetMeVoByType<BaseRoleVo>();
            this.ai = this.meController.AiController;
        }

        private void Start()
        {
            this.Init();
        }
    }
}

