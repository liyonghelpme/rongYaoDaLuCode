﻿namespace com.u3d.bases.controller
{
    using System;

    public class CDTimeVo
    {
        public CDEndCallBack callBack;
        public float currentCd;
        public CDPriority priority;
        public float publicCd;
        public bool showMask;
        public int skillType;
        public float totalCd;

        public float GetFillAmount()
        {
            return ((this.publicCd <= 0f) ? (this.currentCd / this.totalCd) : (this.currentCd / this.publicCd));
        }

        public bool IsInCd()
        {
            return ((this.showMask && (this.totalCd > 0f)) && (this.currentCd > 0f));
        }

        public void ResetCurrnetCd()
        {
            this.currentCd = this.totalCd;
        }

        public void SetPublicCd(float cd)
        {
            if (this.currentCd <= 0f)
            {
                this.publicCd = cd;
                this.currentCd = cd;
            }
        }
    }
}

