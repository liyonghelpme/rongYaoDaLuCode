﻿namespace com.u3d.bases.controller
{
    using com.game.vo;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class DeathController : MonoBehaviour
    {
        protected BaseRoleVo MeBaseRoleVo;
        public ActionControler MeController;

        protected virtual void CheckDeath()
        {
        }

        private void Start()
        {
            this.MeBaseRoleVo = this.MeController.Me.GetVo() as BaseRoleVo;
        }

        private void Update()
        {
            this.CheckDeath();
        }
    }
}

