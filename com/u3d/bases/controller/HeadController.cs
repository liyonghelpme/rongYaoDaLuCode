﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.basic;
    using com.game.basic.events;
    using com.game.data;
    using com.game.module.hud;
    using com.game.vo;
    using com.u3d.bases;
    using com.u3d.bases.display.character;
    using System;
    using UnityEngine;

    public class HeadController : MonoBehaviour
    {
        private HeadAboveComponents _headComponents;
        private PlayerDisplay _playerDisplay;
        private PlayerVo _playerVo;
        private GameObject _warEff;

        public void Init(PlayerDisplay player)
        {
            this._playerDisplay = player;
            this.playerVo = (this._playerDisplay != null) ? this._playerDisplay.GetMeVoByType<PlayerVo>() : null;
            this.InitNameGo();
            this.InitEvent();
            this.InitHeadComponentByCopyType();
        }

        private void InitEvent()
        {
            GlobalAPI.facade.Add(0x15, new NoticeListener(this.OnWarStateChange));
        }

        private void InitHeadComponentByCopyType()
        {
            if (AppMap.Instance.IsInMainCity)
            {
                this.InitName();
                this.UpdateWarState();
                this.OnTitleChange();
                this.OnClubChance();
            }
            else if ((!AppMap.Instance.IsInArena && !AppMap.Instance.IsInWifiPVP) && GlobalData.isInCopy)
            {
            }
        }

        private void InitName()
        {
            this._headComponents.AddHeadComponents(HeadComponentsType.Name, this.playerVo.Name);
        }

        private void InitNameGo()
        {
            GameObject obj2 = NGUITools.AddChild(this._playerDisplay.Controller.gameObject);
            obj2.name = "name";
            Transform target = obj2.transform;
            float y = this._playerDisplay.GetMeVoByType<PlayerVo>().generalTemplateInfo.pivot_localposition_y * 0.001f;
            target.localPosition = new Vector3(0f, y, 0f);
            this._headComponents = HudView.Instance.AddHeadComponent(target, this._playerVo);
            this._playerDisplay.Controller.GoName = this._headComponents.gameObject;
        }

        private void OnClubChance()
        {
        }

        private void OnDestroy()
        {
            this.RemoveEvent();
            this._playerDisplay = null;
            this._playerVo = null;
            if (null != this._warEff)
            {
                UnityEngine.Object.Destroy(this._warEff);
                this._warEff = null;
            }
            if (null != this._headComponents)
            {
                UnityEngine.Object.Destroy(this._headComponents.gameObject);
                this._headComponents = null;
            }
        }

        private void OnTitleChange()
        {
        }

        private void OnWarStateChange(int type, int v1, int v2, object data)
        {
            if ((this._playerVo != null) && ((v1 == 0) || (v1 == ((uint) this._playerVo.Id))))
            {
                this.UpdateWarState();
            }
        }

        private void RemoveEvent()
        {
            GlobalAPI.facade.Remove(0x15, new NoticeListener(this.OnWarStateChange));
        }

        private void UpdateWarState()
        {
            if (MeVo.instance.Id != this._playerVo.Id)
            {
                if (this._playerVo.isInWar)
                {
                    if (this._warEff == null)
                    {
                        this._warEff = this._headComponents.AddHeadComponents(HeadComponentsType.WarState, string.Empty);
                    }
                    this._warEff.SetActive(true);
                }
                else if (this._warEff != null)
                {
                    this._warEff.SetActive(false);
                }
            }
        }

        public PlayerVo playerVo
        {
            get
            {
                return this._playerVo;
            }
            private set
            {
                if (this._playerVo != value)
                {
                    this._playerVo = value;
                    if (this._playerVo == null)
                    {
                        Debug.Log("VO is null");
                    }
                }
            }
        }
    }
}

