﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.basic;
    using com.game.module.core;
    using com.game.module.map;
    using com.game.module.Story;
    using com.game.Speech;
    using com.game.vo;
    using com.u3d.bases.consts;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.joystick;
    using System;
    using UnityEngine;

    public class MeStatuController : PlayerStatuController
    {
        private float _autoMoveTime;
        private bool _footSmokeActive;
        private bool _heroArriveStoryPlaying;
        private bool _isBattling;
        private float _lastSyncTime;
        private float _roleHeiht = 1.5f;

        protected override void DoStatuChange()
        {
            base.DoStatuChange();
            switch (base.CurrentStatu)
            {
                case 0x12:
                    break;

                case 0x13:
                    base.DisposePlayerBlood();
                    break;

                case 1:
                    if (!this._footSmokeActive)
                    {
                    }
                    break;

                case 11:
                    this._footSmokeActive = false;
                    base.DisposePlayerBlood();
                    if (base.playerVo.CurHp <= 0)
                    {
                        PlayerVo playerVo = base.playerVo;
                        playerVo.numDead++;
                        GlobalAPI.facade.Notify(2, base.playerVo.generalTemplateInfo.general_id, 1, null);
                    }
                    break;

                default:
                    this._footSmokeActive = false;
                    break;
            }
        }

        protected override void DoStatuTransfered()
        {
            base.DoStatuTransfered();
            if (base.PreStatuNameHash == Status.NAME_HASH_DEATH)
            {
                if (AppMap.Instance.mapParserII.MapId != 0x9c41)
                {
                    if (((AppMap.Instance.mapParserII.MapId != 0xea61) && (AppMap.Instance.mapParserII.MapId != 0x9c42)) && (AppMap.Instance.mapParserII.MapId != 0xea62))
                    {
                        if (SpeechMgr.Instance.IsAssassinSpeech)
                        {
                            SpeechMgr.Instance.PlaySpeech(0x15);
                        }
                    }
                    else if (AppMap.Instance.mapParserII.MapId != 0x9c42)
                    {
                    }
                }
            }
            else if (((base.PreStatuNameHash != Status.NAME_HASH_STANDUP) && (base.PreStatuNameHash != Status.NAME_HASH_RUN)) && (base.PreStatuNameHash == Status.NAME_HASH_WIN))
            {
            }
            this._autoMoveTime = 0f;
        }

        private void HeroArriveCallback()
        {
            if (this._heroArriveStoryPlaying)
            {
                this._heroArriveStoryPlaying = false;
                MapMode.InStory = false;
                AppMap.Instance.StartAllMonstersAi();
                if (this._isBattling)
                {
                    Singleton<BattleView>.Instance.OpenView();
                }
                else
                {
                    Singleton<MainView>.Instance.OpenView();
                }
            }
        }

        private bool NeedSyn(int statu, int lastStatu)
        {
            bool flag = (lastStatu == 11) && (statu == 0);
            bool flag2 = (lastStatu != 11) && (statu == 11);
            bool flag3 = (Time.time - this._lastSyncTime) > 1f;
            bool flag4 = (flag3 && (statu == 1)) && (lastStatu == 0);
            bool flag5 = (flag3 && (statu == 0)) && (lastStatu == 1);
            return (((flag || flag2) || flag4) || flag5);
        }

        private void PlayHeroArriveStory()
        {
            uint mapId = AppMap.Instance.mapParserII.MapId;
            Vector3 localPosition = base.MeControler.transform.localPosition;
            if (Singleton<StoryControl>.Instance.PlayHeroArriveStory(mapId, localPosition, new ClosedCallback(this.HeroArriveCallback)))
            {
                this._heroArriveStoryPlaying = true;
                MapMode.InStory = true;
                AppMap.Instance.StopAllMonstersAi();
                GameObject gameObject = Singleton<BattleView>.Instance.gameObject;
                this._isBattling = (null != gameObject) && gameObject.activeSelf;
                if (this._isBattling)
                {
                    Singleton<BattleView>.Instance.CloseView();
                }
                else
                {
                    Singleton<MainView>.Instance.CloseView();
                }
            }
            else
            {
                this.HeroArriveCallback();
            }
        }

        protected override void ProcessRunState(AnimatorStateInfo stateInfo)
        {
            base.ProcessRunState(stateInfo);
            if (MeControler.IsPressedMoveButton || JoystickController.instance.joystick.onPressed)
            {
                this._autoMoveTime = 0f;
            }
            else
            {
                this._autoMoveTime += Time.deltaTime;
            }
            if (this._autoMoveTime > 1f)
            {
            }
        }

        public override bool SetStatu(int nextStatu)
        {
            bool flag = base.SetStatu(nextStatu);
            if ((flag && AppMap.Instance.mapParserII.NeedSyn) && this.NeedSyn(nextStatu, base.LastStatu))
            {
                Singleton<RoleMode>.Instance.SendStatuChange();
                this._lastSyncTime = Time.time;
            }
            return flag;
        }

        private void Start()
        {
            base.PreStatuNameHash = Status.NAME_HASH_IDLE;
            BoxCollider2D componentInChildren = base.MeControler.gameObject.GetComponentInChildren<BoxCollider2D>();
            if (null != componentInChildren)
            {
                this._roleHeiht = componentInChildren.size.y;
            }
        }
    }
}

