﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.data;
    using com.game.module.effect;
    using com.game.module.fight.vo;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai;
    using com.u3d.bases.consts;
    using com.u3d.bases.display;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityLog;

    public class MonsterAttackController : AttackControllerBase
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map37;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map38;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map39;
        private AiControllerBase ai;
        private bool isPlayDeadEffect;
        private int nowCombo;
        private int nowStatus;
        private int nowStatusHash;

        public override void AddAttackList(ActionVo vo, bool isPush = false, bool fromAi = false)
        {
            Log.AI(base.gameObject, " Monster AddAttackList " + vo.ActionType);
            MonsterVo vo2 = base.MeController.Me.GetVo() as MonsterVo;
            if (((vo2 != null) && vo2.IsEmptyHp) && ((vo.ActionType != "death") && (vo.ActionType != "deathfly")))
            {
                ActionVo item = new ActionVo {
                    ActionType = "death"
                };
                base.AttackList.Add(item);
            }
            else
            {
                if (base.AttackList == null)
                {
                    base.AttackList = new List<ActionVo>();
                }
                if ((vo.ActionType == "death") || (vo.ActionType == "deathfly"))
                {
                    base.AttackList.Clear();
                    base.AttackList.Add(vo);
                }
                else if ((base.MeController.StatuController.CurrentStatu != 2) || (vo.ActionType != "injured"))
                {
                    if (isPush)
                    {
                        base.CurVo = null;
                        base.AttackList.Clear();
                        base.AttackList.Add(vo);
                    }
                    else if ((base.MeController.StatuController.CurrentStatu == 0) || (base.MeController.StatuController.CurrentStatu == 1))
                    {
                        base.AttackList.Add(vo);
                    }
                }
            }
        }

        public bool CheckHaveDeadCameraEffect()
        {
            return (base.MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO.dead_camera != 0);
        }

        private void Death()
        {
            this.SetDeadStatus(11);
        }

        private void FixDeadStatus(int status)
        {
            switch (status)
            {
                case 11:
                    CommandHandler.AddCommandStatic(base.gameObject, "death", 1f);
                    break;

                case 0x13:
                {
                    SysMonsterVo monsterVO = base.MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO;
                    if (((monsterVO.type != 5) && (monsterVO.type != 3)) && monsterVO.enable_hurt_fly)
                    {
                        base.MeController.StatuController.SetStatu(status);
                        base.MeController.StatuController.SetDeathFlyImmediately();
                    }
                    else
                    {
                        base.MeController.StatuController.SetStatu(11);
                        base.MeController.StatuController.SetDeathImmediately();
                    }
                    break;
                }
            }
        }

        private void HandleLookAtAttacker(ActionVo actionVo)
        {
            if ((this.ai != null) && (actionVo.LookAtDestination != null))
            {
                this.ai.LookAt(actionVo);
            }
        }

        private void SetDeadStatus(int status)
        {
            if (this.CheckHaveDeadCameraEffect())
            {
                if (!this.isPlayDeadEffect)
                {
                    MonsterVo meVoByType = base.MeController.Me.GetMeVoByType<MonsterVo>();
                    AppMap.Instance.mapParserII.PlayBossDeathScript(base.MeController.Me.GoBase, meVoByType.MonsterVO.dead_camera);
                }
                this.FixDeadStatus(status);
                this.isPlayDeadEffect = true;
            }
            else
            {
                this.FixDeadStatus(status);
            }
        }

        private void ShowDeathEffect()
        {
            if (base.MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO.type != 2)
            {
                EffectMgr.Instance.CreateStateEffect("10021".ToString(), base.gameObject, base.gameObject.transform.position);
            }
        }

        private void Start()
        {
            if (base.MeController.AiController != null)
            {
                this.ai = base.MeController.AiController;
            }
            base.AttackList = new List<ActionVo>();
        }

        private void Update()
        {
            string actionType;
            Dictionary<string, int> dictionary;
            int num3;
            this.nowStatus = base.MeController.StatuController.CurrentStatu;
            this.nowCombo = base.MeController.StatuController.CurrentCombStatu;
            this.nowStatusHash = base.MeController.StatuController.CurStatuNameHash;
            if ((base.AttackList.Count > 0) && ((((this.nowStatus == 0) && (this.nowStatusHash == Status.NAME_HASH_IDLE)) || ((this.nowStatus == -1) || (this.nowStatusHash == Status.NAME_HASH_EMPTY))) || ((this.nowStatus == 1) || (this.nowStatusHash == Status.NAME_HASH_RUN))))
            {
                base.CurVo = base.AttackList[0];
                base.Target = base.CurVo.Target;
                base.AttackList.RemoveAt(0);
                Log.AI(base.gameObject, " MonsterAttackController " + base.CurVo.ActionType + " ");
                ActionDisplay targetDisplay = null;
                if (base.MeController.AiController != null)
                {
                    targetDisplay = base.MeController.AiController.GetTargetDisplay();
                }
                if (base.CurVo.Preparework != null)
                {
                    base.CurVo.Preparework(base.MeController);
                    base.CurVo.Preparework = null;
                }
                actionType = base.CurVo.ActionType;
                if (actionType != null)
                {
                    if (<>f__switch$map37 == null)
                    {
                        dictionary = new Dictionary<string, int>(4);
                        dictionary.Add("run", 0);
                        dictionary.Add("attack", 1);
                        dictionary.Add("death", 2);
                        dictionary.Add("deathfly", 3);
                        <>f__switch$map37 = dictionary;
                    }
                    if (<>f__switch$map37.TryGetValue(actionType, out num3))
                    {
                        switch (num3)
                        {
                            case 0:
                                Log.AI(base.gameObject, " CanMove " + base.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanMove);
                                if (base.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanMove)
                                {
                                    int controllerId = -1;
                                    if (targetDisplay != null)
                                    {
                                        controllerId = targetDisplay.Controller.GetControllerId();
                                    }
                                    CommandHandler.AddCommandStatic(base.gameObject, string.Format("move_attack {0}", controllerId), 1f);
                                    base.MeController.WalkTo(base.CurVo.Destination, base.CurVo.Callback, base.CurVo.IsMoveDirectly, base.CurVo.IsAStarMove);
                                }
                                break;

                            case 1:
                                ActionDisplay target;
                                switch (this.nowCombo)
                                {
                                    case 0:
                                    case 2:
                                        target = base.Target;
                                        base.MeController.SkillController.RequestUseSkill(0, true, false, null, null, target);
                                        goto Label_03CD;

                                    case 1:
                                    {
                                        int skillType = (base.MeController.SkillController.maxSkillType < 1) ? 0 : 1;
                                        target = base.Target;
                                        base.MeController.SkillController.RequestUseSkill(skillType, true, false, null, null, target);
                                        goto Label_03CD;
                                    }
                                }
                                break;

                            case 2:
                                base.AttackList.Clear();
                                base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                                this.HandleLookAtAttacker(base.CurVo);
                                this.SetDeadStatus(11);
                                CommandHandler.AddCommandStatic(base.gameObject, "death", 1f);
                                break;

                            case 3:
                                base.AttackList.Clear();
                                base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                                this.HandleLookAtAttacker(base.CurVo);
                                this.SetDeadStatus(0x13);
                                CommandHandler.AddCommandStatic(base.gameObject, "death_fly", 1f);
                                break;
                        }
                    }
                }
            }
        Label_03CD:
            if ((base.AttackList.Count > 0) && ((this.nowStatus == 15) || (this.nowStatus == 0x10)))
            {
                base.CurVo = base.AttackList[0];
                Log.AI(base.gameObject, " RunDeath " + base.CurVo.ActionType);
                actionType = base.CurVo.ActionType;
                if (actionType != null)
                {
                    if (<>f__switch$map38 == null)
                    {
                        dictionary = new Dictionary<string, int>(2);
                        dictionary.Add("death", 0);
                        dictionary.Add("deathfly", 1);
                        <>f__switch$map38 = dictionary;
                    }
                    if (<>f__switch$map38.TryGetValue(actionType, out num3))
                    {
                        if (num3 == 0)
                        {
                            base.AttackList.Clear();
                            base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                            this.HandleLookAtAttacker(base.CurVo);
                            base.Invoke("Death", 0.3f);
                            CommandHandler.AddCommandStatic(base.gameObject, "death", 1f);
                        }
                        else if (num3 == 1)
                        {
                            base.AttackList.Clear();
                            base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                            this.HandleLookAtAttacker(base.CurVo);
                            this.SetDeadStatus(0x13);
                            CommandHandler.AddCommandStatic(base.gameObject, "death_fly", 1f);
                        }
                    }
                }
            }
            if (((base.AttackList.Count > 0) && (this.nowStatus != 11)) && (this.nowStatus != 0x13))
            {
                base.CurVo = base.AttackList[0];
                Log.AI(base.gameObject, " RunDeath " + base.CurVo.ActionType);
                actionType = base.CurVo.ActionType;
                if (actionType != null)
                {
                    if (<>f__switch$map39 == null)
                    {
                        dictionary = new Dictionary<string, int>(2);
                        dictionary.Add("deathfly", 0);
                        dictionary.Add("death", 1);
                        <>f__switch$map39 = dictionary;
                    }
                    if (<>f__switch$map39.TryGetValue(actionType, out num3))
                    {
                        if (num3 == 0)
                        {
                            base.AttackList.Clear();
                            base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                            this.HandleLookAtAttacker(base.CurVo);
                            this.SetDeadStatus(0x13);
                            CommandHandler.AddCommandStatic(base.gameObject, "death_fly", 1f);
                        }
                        else if (num3 == 1)
                        {
                            base.AttackList.Clear();
                            base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                            this.HandleLookAtAttacker(base.CurVo);
                            this.SetDeadStatus(11);
                            CommandHandler.AddCommandStatic(base.gameObject, "death", 1f);
                        }
                    }
                }
            }
        }
    }
}

