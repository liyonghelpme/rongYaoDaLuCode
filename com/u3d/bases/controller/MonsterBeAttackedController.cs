﻿namespace com.u3d.bases.controller
{
    using com.game.data;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using System;
    using UnityLog;

    public class MonsterBeAttackedController : BeAttackedControllerBase
    {
        private MonsterVo _monsterVo;

        protected override void HandleBeAttackedStatus(ActionVo actionVo)
        {
            switch (this._monsterVo.MonsterVO.quality)
            {
                case 1:
                    this.HandleNormalMonsterBeAttack(actionVo, this._monsterVo);
                    break;

                case 3:
                    this.HandleBossBeAttack(actionVo, this._monsterVo);
                    break;
            }
        }

        private void HandleBossBeAttack(ActionVo actionVo, MonsterVo monsterVo)
        {
            if (actionVo.HurtType == 0x3e9)
            {
                this.HandleEndurance(monsterVo);
            }
            if ((((monsterVo.Controller.StatuController.CurrentStatu != 7) && (monsterVo.Controller.StatuController.CurrentStatu != 8)) && ((monsterVo.Controller.StatuController.CurrentStatu != 9) && (monsterVo.Controller.StatuController.CurrentStatu != 0x11))) && (((monsterVo.Controller.StatuController.CurrentStatu != 3) && (monsterVo.Controller.StatuController.CurrentStatu != 4)) && (((monsterVo.Controller.StatuController.CurrentStatu != 5) && (monsterVo.Controller.StatuController.CurrentStatu != 6)) && (monsterVo.Controller.StatuController.CurrentStatu != 0x10))))
            {
                switch (actionVo.HurtType)
                {
                    case 0x3e9:
                        if (!monsterVo.stateInfo.IsInState(StateVo.ENDURED))
                        {
                            if (base.meController.StatuController.CurrentStatu == 10)
                            {
                                base.meController.StatuController.SetStatu(0x10);
                                CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                            }
                            else if (((monsterVo.MonsterVO.type != 2) && (monsterVo.MonsterVO.type != 5)) && (monsterVo.MonsterVO.type != 3))
                            {
                                CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                            }
                            break;
                        }
                        return;

                    case 0x3eb:
                        CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                        break;

                    case 0x3ee:
                        if (base.meController.StatuController.CurrentStatu != 15)
                        {
                            CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                        }
                        break;

                    case 0x3ef:
                        if (base.meController.StatuController.CurrentStatu != 15)
                        {
                            if (base.meController.StatuController.CurrentStatu == 10)
                            {
                                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                            }
                            CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                            base.meController.MoveController.IsNeedKnockBack = true;
                        }
                        break;

                    case 0x3f0:
                        if ((base.meController.StatuController.CurrentStatu != 15) || (base.meController.StatuController.CurrentStatu != 0x13))
                        {
                            CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                        }
                        break;
                }
            }
        }

        private void HandleEndurance(MonsterVo monsterVo)
        {
            if (!monsterVo.stateInfo.IsInState(StateVo.ENDURED))
            {
                SysMonsterVo monsterVO = monsterVo.MonsterVO;
                switch (monsterVO.type)
                {
                    case 3:
                    case 5:
                    case 2:
                        return;
                }
                if (monsterVO.endurance_threshold > 0)
                {
                    int configData = Singleton<ConfigConst>.Instance.GetConfigData("ENDURANCE_ADD");
                    monsterVo.endure += Convert.ToInt32(configData);
                    if (monsterVo.endure >= monsterVo.MonsterVO.endurance_threshold)
                    {
                        int[] arrayStringToInt = StringUtils.GetArrayStringToInt(Singleton<ConfigConst>.Instance.GetConfigDataString("ENDURANCE_BUFF"));
                        if (arrayStringToInt.Length >= 2)
                        {
                            BuffController buffController = base.meController.buffController;
                            if (buffController != null)
                            {
                                buffController.AddBuff(arrayStringToInt[0], arrayStringToInt[1], null);
                            }
                        }
                    }
                }
            }
        }

        private void HandleNormalMonsterBeAttack(ActionVo actionVo, MonsterVo monsterVo)
        {
            if (actionVo.HurtType == 0x3e9)
            {
                this.HandleEndurance(monsterVo);
            }
            bool isFromNet = actionVo.IsFromNet;
            Log.Net(base.gameObject + " hurtFromNeetwork " + isFromNet);
            switch (actionVo.HurtType)
            {
                case 0x3e9:
                    if (!monsterVo.stateInfo.IsInState(StateVo.ENDURED))
                    {
                        if (base.meController.StatuController.CurrentStatu == 10)
                        {
                            base.meController.StatuController.SetStatu(0x10);
                            CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                        }
                        else if (((monsterVo.MonsterVO.type != 2) && (monsterVo.MonsterVO.type != 3)) && (monsterVo.MonsterVO.type != 5))
                        {
                            CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                        }
                        break;
                    }
                    return;

                case 0x3eb:
                    base.meController.StatuController.SetStatu(0x13);
                    break;

                case 0x3ee:
                    if ((base.meController.StatuController.CurrentStatu != 0x13) || (base.meController.StatuController.CurrentStatu != 11))
                    {
                        if (base.meController.StatuController.CurrentStatu == 15)
                        {
                            base.meController.StatuController.SetStatu(0x10);
                        }
                        base.meController.StatuController.SetHurtDownImmediately();
                        base.meController.StatuController.SetStatu(15);
                    }
                    break;

                case 0x3ef:
                    if (base.meController.StatuController.CurrentStatu != 15)
                    {
                        if (base.meController.StatuController.CurrentStatu == 10)
                        {
                            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                        }
                        if (isFromNet)
                        {
                            CommandHandler.AddCommandStatic(base.gameObject, "networkCommand hurt", 1f);
                        }
                        else
                        {
                            CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                        }
                        base.meController.MoveController.IsNeedKnockBack = true;
                    }
                    break;

                case 0x3f0:
                    if ((base.meController.StatuController.CurrentStatu != 15) || (base.meController.StatuController.CurrentStatu != 0x13))
                    {
                        base.meController.StatuController.SetStatu(0x13);
                    }
                    break;
            }
        }

        protected override void InformAI(ActionVo actionVo)
        {
            base.InformAI(actionVo);
            if ((base.ai != null) && (actionVo.LookAtDestination != null))
            {
                base.ai.LookAt(actionVo);
            }
        }

        protected override void Init()
        {
            base.Init();
            this._monsterVo = base.meController.GetMeVoByType<MonsterVo>();
        }
    }
}

