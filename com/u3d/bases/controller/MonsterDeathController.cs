﻿namespace com.u3d.bases.controller
{
    using com.game.module.fight.vo;
    using com.game.Speech;
    using com.game.vo;
    using System;
    using UnityLog;

    public class MonsterDeathController : DeathController
    {
        private bool speechPlaying;

        protected override void CheckDeath()
        {
            base.CheckDeath();
            int currentStatu = base.MeController.StatuController.CurrentStatu;
            if (base.MeBaseRoleVo.IsEmptyHp && ((currentStatu != 11) || (currentStatu != 0x13)))
            {
                ActionVo vo3;
                ActionVo vo = (base.MeController.MoveController.DeathFlySpeed == 0f) ? (vo3 = new ActionVo()) : (vo3 = new ActionVo());
                base.MeController.AttackController.AddAttackList(vo, false, false);
                Log.AI(base.gameObject, " CheckEnemy Death ");
                MonsterVo vo2 = base.MeController.Me.GetVo() as MonsterVo;
                if ((vo2.MonsterVO.quality == 3) && !this.speechPlaying)
                {
                    this.speechPlaying = true;
                    SpeechMgr.Instance.PlayKillBossSpeech();
                }
            }
        }
    }
}

