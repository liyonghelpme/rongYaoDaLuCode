﻿namespace com.u3d.bases.controller
{
    using System;
    using UnityEngine;

    public class MonsterMoveController : MoveControllerBase
    {
        protected override void ProcessDeathFlyMove(AnimatorStateInfo stateInfo)
        {
            switch (base.MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO.type)
            {
                case 3:
                case 5:
                    base.DeathFlySpeed = 0f;
                    return;
            }
            if ((stateInfo.normalizedTime > 0f) && (stateInfo.normalizedTime < 0.9))
            {
                float deathFlySpeed = base.DeathFlySpeed;
                Vector3 position = base.ThisTransform.position;
                Quaternion rotation = base.ThisTransform.rotation;
                Quaternion quaternion2 = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y, rotation.eulerAngles.z);
                Vector3 vector2 = position + ((Vector3) (((quaternion2 * Vector3.back) * deathFlySpeed) * Time.deltaTime));
                base.ThisTransform.position = vector2;
            }
        }

        protected override void ProcessHurt1Move(AnimatorStateInfo stateInfo)
        {
            switch (base.MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO.type)
            {
                case 3:
                case 5:
                    return;
            }
            if (base.IsNeedKnockBack && (stateInfo.normalizedTime < 0.75))
            {
                float num2 = 1.5f;
                Vector3 position = base.ThisTransform.position;
                Quaternion rotation = base.ThisTransform.rotation;
                Quaternion quaternion2 = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y, rotation.eulerAngles.z);
                Vector3 vector2 = position + ((Vector3) (((quaternion2 * Vector3.back) * num2) * Time.deltaTime));
                base.ThisTransform.position = vector2;
            }
        }
    }
}

