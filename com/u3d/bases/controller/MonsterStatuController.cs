﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.basic;
    using com.game.module.core;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai;
    using com.u3d.bases.ai.AiManager;
    using com.u3d.bases.consts;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MonsterStatuController : StatuControllerBase
    {
        private const float COMB_INTERVAL = 3f;
        private bool deathFlyStateReject;
        private bool deathStateReject;
        private static readonly int[,] statuChangeMatrix = new int[,] { 
            { 
                0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
                1, 1, 1, 1, 1, 1, 1, 1
             }, { 
                1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 
                0, 1, 1, 1, 1, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 
                0, 0, 1, 1, 1, 1, 1, 1
             }, { 
                1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 
                0, 1, 1, 1, 1, 1, 1, 1
             }, { 
                1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 
                0, 1, 1, 1, 1, 1, 1, 1
             }, { 
                1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 
                0, 1, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 
                0, 1, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 
                0, 0, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 
                0, 0, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 
                0, 0, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 
                0, 0, 1, 1, 1, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 
                0, 0, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 
                0, 0, 1, 1, 1, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 
                0, 0, 1, 1, 1, 1, 1, 1
             }, { 
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 
                0, 0, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 
                1, 0, 1, 1, 0, 1, 1, 1
             }, 
            { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 
                0, 0, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 
                0, 0, 1, 1, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                0, 0, 0, 0, 1, 1, 1, 1
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                0, 0, 0, 0, 1, 1, 1, 1
             }, { 
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
                1, 1, 1, 1, 1, 1, 1, 1
             }, { 
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
                1, 1, 1, 1, 1, 1, 1, 1
             }
         };

        private void DoAttack1Begin()
        {
        }

        private void DoAttack2Begin()
        {
        }

        private void DoDeathBegin()
        {
            base.DisposeShadowEffect();
        }

        private void DoDeathFlyBegin()
        {
            base.DisposeShadowEffect();
        }

        private void DoIdleBegin()
        {
        }

        protected override void DoStatuTransfered()
        {
            if (this.StateInfo.nameHash == Status.NAME_HASH_IDLE)
            {
                this.DoIdleBegin();
            }
            else if (this.StateInfo.nameHash == Status.NAME_HASH_ATTACK1)
            {
                this.DoAttack1Begin();
            }
            else if (this.StateInfo.nameHash == Status.NAME_HASH_ATTACK2)
            {
                this.DoAttack2Begin();
            }
            else if (this.StateInfo.nameHash == Status.NAME_HASH_DEATH)
            {
                this.DoDeathBegin();
            }
            else if (this.StateInfo.nameHash == Status.NAME_HASH_DEATHFLY)
            {
                this.DoDeathFlyBegin();
            }
        }

        protected override int[,] GetStatuMatrix()
        {
            return statuChangeMatrix;
        }

        protected override void ProcessDeathFlyState(AnimatorStateInfo stateInfo)
        {
            base.ProcessDeathFlyState(stateInfo);
            if (((stateInfo.normalizedTime >= 1f) && !this.deathFlyStateReject) && !CommandHandler.CheckHasCommand(base.gameObject))
            {
                this.deathFlyStateReject = true;
                this.RemoveDeadMonster();
            }
        }

        protected override void ProcessDeathState(AnimatorStateInfo stateInfo)
        {
            base.ProcessDeathState(stateInfo);
            if (((stateInfo.normalizedTime >= 1f) && !this.deathStateReject) && !CommandHandler.CheckHasCommand(base.gameObject))
            {
                this.deathStateReject = true;
                this.RemoveDeadMonster();
            }
        }

        protected override void ProcessHurtDownState(AnimatorStateInfo stateInfo)
        {
            if (base.Animator.IsInTransition(0) && (base.CurrentStatu == 15))
            {
                this.SetStatu(0x10);
            }
        }

        protected override void ProcessLoop1State(AnimatorStateInfo stateInfo)
        {
        }

        protected override void ProcessMagic1State(AnimatorStateInfo stateInfo)
        {
        }

        protected override void ProcessRiseState(AnimatorStateInfo stateInfo)
        {
        }

        protected override void ProcessSkill1State(AnimatorStateInfo stateInfo)
        {
        }

        private void RemoveDeadMonster()
        {
            base.StartCoroutine(this.WaitRemoveMonster());
        }

        private void ResetSpeed()
        {
            base.Animator.speed = 1f;
        }

        public override bool SetStatu(int nextStatu)
        {
            if (((nextStatu == 11) || (nextStatu == 0x13)) && (base.MeControler.AiController is MonsterAiController))
            {
                (base.MeControler.AiController as MonsterAiController).CheckUseSkillOnDie();
            }
            return base.SetStatu(nextStatu);
        }

        [DebuggerHidden]
        private IEnumerator WaitRemoveMonster()
        {
            return new <WaitRemoveMonster>c__Iterator4D { <>f__this = this };
        }

        public override int CurrentCombStatu
        {
            get
            {
                return base._currentCombStatu;
            }
            set
            {
                base.CancelInvoke("ResetCombStatu");
                base._currentCombStatu = value;
                base.Invoke("ResetCombStatu", 3f);
            }
        }

        [CompilerGenerated]
        private sealed class <WaitRemoveMonster>c__Iterator4D : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal MonsterStatuController <>f__this;
            internal MonsterAiController <controller>__2;
            internal MonsterVo <meMonsterVo>__0;
            internal MonsterDisplay <monDis>__3;
            internal bool <selfMonster>__1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(0.1f);
                        this.$PC = 1;
                        return true;

                    case 1:
                        this.<meMonsterVo>__0 = this.<>f__this.MeControler.GetMeVoByType<MonsterVo>();
                        Singleton<MonsterMgr>.Instance.addMonsterPoint(this.<meMonsterVo>__0.Id);
                        Singleton<MonsterMgr>.Instance.RemoveMonster(this.<meMonsterVo>__0.Id.ToString(CultureInfo.InvariantCulture));
                        AiTargetManager.instance.MonsterDie(this.<>f__this.MeControler);
                        this.<>f__this.MeControler.buffController.Dead();
                        if (!AppMap.Instance.mapParserII.NeedSyn)
                        {
                            Singleton<MapMode>.Instance.MonsterDeath(this.<meMonsterVo>__0.Id);
                        }
                        this.<selfMonster>__1 = (this.<meMonsterVo>__0.MonsterVO.type == 6) || (this.<meMonsterVo>__0.Camp == 0);
                        if (this.<selfMonster>__1)
                        {
                            Singleton<MapMode>.Instance.MySideSummonMonsterDie(this.<meMonsterVo>__0.Id);
                        }
                        this.<controller>__2 = this.<>f__this.MeControler.AiController as MonsterAiController;
                        if (this.<controller>__2 != null)
                        {
                            this.<controller>__2.CheckRebornOnDie();
                        }
                        if (!this.<selfMonster>__1)
                        {
                            this.<monDis>__3 = this.<>f__this.MeControler.Me.GetMeByType<MonsterDisplay>();
                            GlobalAPI.facade.Notify(1, this.<monDis>__3.MonsterVo.id, 1, this.<monDis>__3.GetMeVoByType<MonsterVo>());
                        }
                        this.$PC = -1;
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

