﻿namespace com.u3d.bases.controller
{
    using com.u3d.bases.consts;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MoveControllerBase : MonoBehaviour
    {
        private Animator _animator;
        private float _deathFlySpeed;
        private EndCallback _endCallback;
        private StartCallback _startCallback;
        public com.u3d.bases.controller.AnimationEventController AnimationEventController;
        public com.u3d.bases.controller.AnimationParameter AnimationParameter;
        protected List<MoveEventCheck> checkList = new List<MoveEventCheck>();
        protected List<MoveEventCheck> delList = new List<MoveEventCheck>();
        protected const float KNOCK_BACK_SPEED = 1.5f;
        public ActionControler MeController;
        protected Transform ThisTransform;

        public virtual void AddMoveEvent(MoveEventCheck mec)
        {
            <AddMoveEvent>c__AnonStorey10A storeya = new <AddMoveEvent>c__AnonStorey10A {
                mec = mec
            };
            List<MoveEventCheck> list = this.checkList.Where<MoveEventCheck>(new Func<MoveEventCheck, bool>(storeya.<>m__1B5)).ToList<MoveEventCheck>();
            MoveEventCheck item = storeya.mec;
            if (storeya.mec.repFun != null)
            {
                item = storeya.mec.repFun(list, storeya.mec);
            }
            foreach (MoveEventCheck check2 in list)
            {
                this.checkList.Remove(check2);
            }
            item.startPos = this.ThisTransform.position;
            this.checkList.Add(item);
        }

        private void CheckMove()
        {
            this.HandleMoveEvent();
            this.CleanObsoleteMoveEvent();
        }

        private void CleanObsoleteMoveEvent()
        {
            if (this.delList.Count != 0)
            {
                foreach (MoveEventCheck check in this.delList)
                {
                    this.checkList.Remove(check);
                }
                this.delList.Clear();
            }
        }

        protected virtual void DoUpdate()
        {
            if (this._animator == null)
            {
                this._animator = this.MeController.Me.Animator;
            }
            this.UpdateProgramMove();
            this.CheckMove();
        }

        public void ForceRemoveMoveEvent(MoveEventCheck mec)
        {
            if (mec.removeFun != null)
            {
                mec.removeFun(this.MeController.Me, mec);
            }
            this.checkList.Remove(mec);
        }

        private void HandleMoveEvent()
        {
            float deltaTime = Time.deltaTime;
            foreach (MoveEventCheck check in this.checkList)
            {
                if (check.endChecker(this.ThisTransform, check))
                {
                    if (check.endFun != null)
                    {
                        check.endFun(this.MeController.Me, check);
                        check.endFun = null;
                    }
                    this.delList.Add(check);
                }
                else
                {
                    if (check.startFun != null)
                    {
                        check.startFun(this.MeController.Me, check);
                        check.startFun = null;
                    }
                    if (check.moveOperation != null)
                    {
                        check.moveOperation(this.MeController.Me, check, deltaTime);
                        check.timecost += Time.deltaTime;
                    }
                }
            }
        }

        public virtual void MoveAtTheStatuBegin(int statuType)
        {
        }

        protected virtual void ProcessAttack1Move(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessAttack2Move(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessAttack3Move(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessAttack4Move(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessDeathFlyMove(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessHurt1Move(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessRollMove(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessRunMove(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessSkill2Move(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessSkill3Move(AnimatorStateInfo stateInfo)
        {
        }

        private void Start()
        {
            if (this.MeController == null)
            {
            }
            this.MeController = base.GetComponent<ActionControler>();
            this.ThisTransform = this.MeController.Me.GoBase.transform;
        }

        private void Update()
        {
            this.DoUpdate();
        }

        private void UpdateProgramMove()
        {
            AnimatorStateInfo currentAnimatorStateInfo = this._animator.GetCurrentAnimatorStateInfo(0);
            if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_RUN)
            {
                this.ProcessRunMove(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ATTACK1)
            {
                this.ProcessAttack1Move(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ATTACK2)
            {
                this.ProcessAttack2Move(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ATTACK3)
            {
                this.ProcessAttack3Move(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ATTACK4)
            {
                this.ProcessAttack4Move(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_ROLL)
            {
                this.ProcessRollMove(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_SKILL2)
            {
                this.ProcessSkill2Move(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_SKILL3)
            {
                this.ProcessSkill3Move(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_HURT1)
            {
                this.ProcessHurt1Move(currentAnimatorStateInfo);
            }
            else if (currentAnimatorStateInfo.nameHash == Status.NAME_HASH_DEATHFLY)
            {
                this.ProcessDeathFlyMove(currentAnimatorStateInfo);
            }
        }

        public float DeathFlySpeed
        {
            get
            {
                return this._deathFlySpeed;
            }
            set
            {
                if (value > 0f)
                {
                    this._deathFlySpeed = value;
                }
            }
        }

        public bool IsNeedKnockBack { get; set; }

        [CompilerGenerated]
        private sealed class <AddMoveEvent>c__AnonStorey10A
        {
            internal MoveEventCheck mec;

            internal bool <>m__1B5(MoveEventCheck item)
            {
                return (item.type == this.mec.type);
            }
        }
    }
}

