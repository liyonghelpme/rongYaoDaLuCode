﻿namespace com.u3d.bases.controller
{
    using System;
    using UnityEngine;

    public class MoveEventCheck
    {
        public float distance;
        public EndChecker endChecker;
        public EndCallback endFun;
        public Vector3 endPos;
        public MoveOperation moveOperation;
        public RemoveOperation removeFun;
        public ReplaceOperation repFun;
        public float speed;
        public StartCallback startFun;
        public Vector3 startPos;
        public float timecost;
        public int type;
    }
}

