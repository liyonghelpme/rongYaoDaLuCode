﻿namespace com.u3d.bases.controller
{
    using com.u3d.bases.display;
    using System;
    using System.Runtime.CompilerServices;

    public delegate void MoveOperation(BaseDisplay display, MoveEventCheck c, float deltaTime);
}

