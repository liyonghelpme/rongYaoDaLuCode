﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.module.fight.vo;
    using com.game.module.map;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.consts;
    using com.u3d.bases.display;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityLog;

    public class PlayerAttackController : AttackControllerBase
    {
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map3A;
        private bool isEmergency;
        private bool isInitialized;
        private int nowStatus;
        private PlayerVo playerVo;
        private SkillController skillController;
        private StatuControllerBase statusController;

        public override void AddAttackList(ActionVo vo, bool isPush = false, bool fromAi = false)
        {
            Log.AI(base.gameObject, string.Concat(new object[] { " AddAttackList ", vo.ActionType, " Destination ", vo.Destination }));
            if ((((this.playerVo != null) && this.playerVo.IsEmptyHp) && ((vo.ActionType != "death") && (vo.ActionType != "deathfly"))) && !AppMap.Instance.IsEditor)
            {
                ActionVo item = new ActionVo {
                    ActionType = "death"
                };
                base.AttackList.Add(item);
                this.isEmergency = true;
            }
            else
            {
                if (base.AttackList == null)
                {
                    base.AttackList = new List<ActionVo>();
                }
                Log.AI(base.gameObject, " ActionList Count " + base.AttackList.Count);
                if ((((base.AttackList.Count < 1) || (vo.ActionType == "death")) || ((vo.ActionType == "deathfly") || isPush)) || fromAi)
                {
                    if (((vo.ActionType == "death") || (vo.ActionType == "deathfly")) || (isPush && !this.IsInBattle()))
                    {
                        base.AttackList.Clear();
                        this.isEmergency = true;
                    }
                    Log.AI(base.gameObject, " AttackList real add  " + vo.ActionType);
                    base.AttackList.Add(vo);
                }
            }
        }

        private void Init()
        {
            if (base.MeController != null)
            {
                this.playerVo = base.MeController.Me.GetMeVoByType<PlayerVo>();
                this.statusController = base.MeController.StatuController;
                this.skillController = base.MeController.SkillController;
                this.isInitialized = true;
            }
        }

        private bool IsInBattle()
        {
            return StatuControllerBase.IsStatusHashInFight(this.statusController.CurStatuNameHash);
        }

        private void Update()
        {
            if (!this.isInitialized)
            {
                this.Init();
            }
            this.nowStatus = this.statusController.CurrentStatu;
            if ((MapMode.StartAutoMove || (this.nowStatus == 11)) || ((this.nowStatus == 0x13) || (this.nowStatus == 0x12)))
            {
                base.AttackList.Clear();
            }
            else if (((base.AttackList.Count > 0) && (this.skillController.SkillList.Count == 0)) && (((this.nowStatus == 0) || (this.nowStatus == 1)) || this.isEmergency))
            {
                this.isEmergency = false;
                base.CurVo = base.AttackList[0];
                ActionDisplay target = (base.MeController.AiController == null) ? null : base.MeController.AiController.GetTargetDisplay();
                if (base.CurVo.Preparework != null)
                {
                    base.CurVo.Preparework(base.MeController);
                    base.CurVo.Preparework = null;
                }
                Log.AI(base.gameObject, " READ　Action " + base.CurVo.ActionType);
                string actionType = base.CurVo.ActionType;
                if (actionType != null)
                {
                    int num2;
                    if (<>f__switch$map3A == null)
                    {
                        Dictionary<string, int> dictionary = new Dictionary<string, int>(4);
                        dictionary.Add("run", 0);
                        dictionary.Add("attack", 1);
                        dictionary.Add("death", 2);
                        dictionary.Add("deathfly", 3);
                        <>f__switch$map3A = dictionary;
                    }
                    if (<>f__switch$map3A.TryGetValue(actionType, out num2))
                    {
                        switch (num2)
                        {
                            case 0:
                            {
                                bool canMove = base.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanMove;
                                Log.AI(base.gameObject, " RUN CanMove " + canMove);
                                base.AttackList.RemoveAt(0);
                                if (canMove)
                                {
                                    if (target == null)
                                    {
                                        target = base.CurVo.Target;
                                    }
                                    int controllerId = -1;
                                    if (target != null)
                                    {
                                        controllerId = target.Controller.GetControllerId();
                                    }
                                    Log.AI(base.gameObject, string.Concat(new object[] { " target is ", target, " target ", controllerId }));
                                    if (base.CurVo.SkillType == -1)
                                    {
                                        CommandHandler.AddCommandStatic(base.gameObject, string.Format("move_attack {0}", controllerId), 1f);
                                    }
                                    else
                                    {
                                        object[] args = new object[] { controllerId, -1, "attackByHand", base.CurVo.SkillType };
                                        CommandHandler.AddCommandStatic(base.gameObject, string.Format("move_attack {0} {1} {2} {3}", args), 1f);
                                    }
                                    this.statusController.ResetCombStatu();
                                    base.MeController.WalkTo(base.CurVo.Destination, base.CurVo.Callback, base.CurVo.IsMoveDirectly, base.CurVo.IsAStarMove);
                                }
                                break;
                            }
                            case 1:
                            {
                                ActionDisplay display2;
                                bool isIgnoreDistance = base.CurVo.IsIgnoreDistance;
                                base.Target = base.CurVo.Target;
                                bool byHand = base.CurVo.ByHand;
                                Log.AI(base.gameObject, " Attack ByHand " + byHand);
                                base.AttackList.RemoveAt(0);
                                if ((this.statusController.CurrentCombStatu == 0) || (this.statusController.CurrentCombStatu == this.statusController.MaxComb))
                                {
                                    if (byHand)
                                    {
                                        this.skillController.UseSkillByHand(0);
                                    }
                                    else
                                    {
                                        display2 = base.Target;
                                        this.skillController.RequestUseSkill(0, isIgnoreDistance, false, null, null, display2);
                                    }
                                }
                                else if ((this.statusController.CurrentCombStatu == 1) && (this.statusController.CurStatuNameHash != Status.NAME_HASH_ATTACK1_0))
                                {
                                    if (byHand)
                                    {
                                        this.skillController.UseSkillByHand(1);
                                    }
                                    else
                                    {
                                        display2 = base.Target;
                                        this.skillController.RequestUseSkill(1, isIgnoreDistance, false, null, null, display2);
                                    }
                                }
                                else if ((this.statusController.CurrentCombStatu == 2) && (this.statusController.CurStatuNameHash != Status.NAME_HASH_ATTACK2_0))
                                {
                                    if (byHand)
                                    {
                                        this.skillController.UseSkillByHand(2);
                                    }
                                    else
                                    {
                                        display2 = base.Target;
                                        this.skillController.RequestUseSkill(2, isIgnoreDistance, false, null, null, display2);
                                    }
                                }
                                break;
                            }
                            case 2:
                                base.MeController.buffController.Dead();
                                base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                                CommandHandler.AddCommandStatic(base.gameObject, "death", 1f);
                                base.AttackList.Clear();
                                break;

                            case 3:
                                base.MeController.buffController.Dead();
                                base.ShowBeAttackedEffect(base.CurVo, 1f, 0f);
                                this.statusController.SetStatu(0x13);
                                CommandHandler.AddCommandStatic(base.gameObject, "death_fly", 1f);
                                base.AttackList.Clear();
                                break;
                        }
                    }
                }
            }
        }
    }
}

