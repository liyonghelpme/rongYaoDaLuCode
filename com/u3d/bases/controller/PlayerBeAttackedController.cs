﻿namespace com.u3d.bases.controller
{
    using com.game.module.fight.vo;
    using com.liyong;
    using com.u3d.bases.ai;
    using com.u3d.bases.display.character;
    using System;

    public class PlayerBeAttackedController : BeAttackedControllerBase
    {
        public override void BeAttacked(ActionVo actionVo)
        {
            base.BeAttacked(actionVo);
            if ((base.meController.buffController != null) && (base.meController.Me is MeDisplay))
            {
                base.meController.buffController.RemoveBuffList(AiConfig.GetBuffListOfHealSkill());
            }
        }

        protected override void HandleBeAttackedStatus(ActionVo actionVo)
        {
            base.meController.Me.GoBase.transform.position = actionVo.Destination;
            switch (base.meController.StatuController.CurrentStatu)
            {
                case 0:
                    CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                    break;

                case 10:
                    base.meController.StatuController.SetStatu(0x10);
                    CommandHandler.AddCommandStatic(base.gameObject, "hurt", 1f);
                    break;
            }
        }
    }
}

