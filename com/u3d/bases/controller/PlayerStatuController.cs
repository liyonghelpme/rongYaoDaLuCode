﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.module.core;
    using com.game.vo;
    using com.u3d.bases.consts;
    using System;
    using UnityEngine;

    public class PlayerStatuController : StatuControllerBase
    {
        private PlayerVo _playerVo;

        protected void Awake()
        {
            base.MaxComb = 3;
        }

        protected override void DoStatuChange()
        {
            base.DoStatuChange();
            if (base.CurrentStatu != 11)
            {
            }
            if ((MeVo.instance.Id != base.MeControler.GetMeVo().Id) && AppMap.Instance.mapParserII.NeedSyn)
            {
                if (base.CurrentStatu == 1)
                {
                    base.MeControler.IsSynMove = true;
                }
                else
                {
                    base.MeControler.IsSynMove = false;
                }
            }
        }

        protected override void DoStatuTransfered()
        {
            if (base.PreStatuNameHash == Status.NAME_HASH_DEATH_END)
            {
                this.playerVo.IsUnbeatable = true;
                base.Invoke("ResetUnbeatableStatu", 3f);
            }
        }

        protected override void ProcessDeathFlyState(AnimatorStateInfo stateInfo)
        {
            base.DisposePlayersFeetShadow();
            if (AppMap.Instance.me.GetVo().Id == this.playerVo.Id)
            {
                Singleton<BattleCenterView>.Instance.updateDeathEffect();
            }
        }

        protected override void ProcessDeathState(AnimatorStateInfo stateInfo)
        {
            base.DisposeShadowEffect();
        }

        protected override void ProcessIdleState(AnimatorStateInfo stateInfo)
        {
            base.ProcessIdleState(stateInfo);
            base.RecoverPlayersFeetShadow();
        }

        private void ResetUnbeatableStatu()
        {
            this.playerVo.IsUnbeatable = false;
        }

        private void Start()
        {
            base.PreStatuNameHash = Status.NAME_HASH_IDLE;
        }

        public PlayerVo playerVo
        {
            get
            {
                if (this._playerVo == null)
                {
                    this._playerVo = base.MeControler.Me.GetVo() as PlayerVo;
                }
                return this._playerVo;
            }
        }
    }
}

