﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.basic;
    using com.game.module.effect;
    using com.game.vo;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class RescueController : MonoBehaviour
    {
        private EffectControlerII _effect;
        private bool _isLoading;
        private ActionControler _meController;
        private BaseDisplay _meDisplay;
        private PlayerVo _meVo;
        private const int BUFF_ID = 0x272f;

        [DebuggerHidden]
        private IEnumerator DelayLoadEffect()
        {
            return new <DelayLoadEffect>c__Iterator4E { <>f__this = this };
        }

        private void OnDestroy()
        {
            if (null != this._effect)
            {
                this._effect.Clear();
                this._effect = null;
            }
            this._meVo = null;
            this._meDisplay = null;
            this._meController = null;
        }

        private void OnEffectCreate(EffectControlerII eff)
        {
            this._isLoading = false;
            this._effect = eff;
            if (this._meVo == null)
            {
                this._effect.Clear();
                this._effect = null;
            }
        }

        public void SetUp(PlayerVo data, BaseDisplay dis, ActionControler ctrl)
        {
            this._meVo = data;
            this._meDisplay = dis;
            this._meController = ctrl;
            this.UpdateView();
        }

        private void Update()
        {
            if (((((this._meDisplay != null) && (null != this._meController)) && (this._meVo != null)) && (((AppMap.Instance.me != null) && (null != AppMap.Instance.me.GoBase)) && (this._meDisplay != AppMap.Instance.me))) && this._meVo.isImprisoned)
            {
                Vector3 position = this._meDisplay.GoBase.transform.position;
                Vector3 b = AppMap.Instance.me.GoBase.transform.position;
                position.y = b.y = 0f;
                if (Vector3.Distance(position, b) < 0.5)
                {
                    this._meVo.isImprisoned = false;
                    this.UpdateView();
                    GlobalAPI.facade.Notify(6, this._meVo.generalTemplateInfo.id, 1, null);
                }
            }
        }

        private void UpdateView()
        {
            ActionControler meController = this.meController;
            if (null != meController.AiController)
            {
                meController.AiController.SetAi(!this._meVo.isImprisoned);
            }
            if (this._meVo.isImprisoned)
            {
                meController.ChangeSpeedByRate(0f, 0f);
                this.meController.StatuController.SlowAnimation(0f);
                if (null == this._effect)
                {
                    this._isLoading = true;
                    base.StartCoroutine(this.DelayLoadEffect());
                }
            }
            else
            {
                meController.ChangeSpeedByRate(1f, 0f);
                this.meController.StatuController.SlowAnimation(1f);
                if (null != this._effect)
                {
                    this._effect.Clear();
                    this._effect = null;
                }
            }
        }

        public ActionControler meController
        {
            get
            {
                return this._meController;
            }
        }

        public BaseDisplay meDisplay
        {
            get
            {
                return this._meDisplay;
            }
        }

        public PlayerVo meVo
        {
            get
            {
                return this._meVo;
            }
        }

        [CompilerGenerated]
        private sealed class <DelayLoadEffect>c__Iterator4E : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal RescueController <>f__this;
            internal int <i>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<i>__0 = 0;
                        break;

                    case 1:
                        this.<i>__0++;
                        break;

                    default:
                        goto Label_00A3;
                }
                if (this.<i>__0 < 0x20)
                {
                    this.$current = 0;
                    this.$PC = 1;
                    return true;
                }
                EffectMgr.Instance.CreateBuffEffect(0x272f.ToString(), this.<>f__this._meDisplay.GoBase, new Effect.EffectCreateCallback(this.<>f__this.OnEffectCreate), null, true, true, null);
                this.$PC = -1;
            Label_00A3:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

