﻿namespace com.u3d.bases.controller
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.Arena;
    using com.game.module.core;
    using com.game.module.Dungeon;
    using com.game.module.effect;
    using com.game.module.fight.arpg;
    using com.game.module.WiFiPvP;
    using com.game.sound;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai;
    using com.u3d.bases.consts;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.fsmUtil;
    using com.u3d.bases.joystick;
    using com.u3d.bases.skills;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class SkillController : MonoBehaviour
    {
        private IList<string> _attackSoundList;
        private int _checkedTime;
        private SysSkillActionVo _currentActionVo;
        private uint _currentSkillId;
        private int _currentSkillType;
        private SysSkillBaseVo _currentSkillVo;
        private int _effectIndex;
        private IList<string> _hitSoundList;
        private float _lastCheckTime;
        private ActionDisplay _lastTargetForAttack;
        private int _nowStatus;
        private Transform _selfTransform;
        private Dictionary<int, SkillCDParams> _skillCdParamsDic = new Dictionary<int, SkillCDParams>();
        private float _skillChaseRange;
        private IDictionary<uint, float> _skillDelayDic;
        private IDictionary<uint, bool> _skillSoundDic;
        [CompilerGenerated]
        private static Func<PlayerDisplay, bool> <>f__am$cache25;
        [CompilerGenerated]
        private static Func<PlayerDisplay, BuffController> <>f__am$cache26;
        [CompilerGenerated]
        private static Func<MonsterDisplay, bool> <>f__am$cache27;
        public List<PDamageBuff> AdditionBuffList = new List<PDamageBuff>();
        public const int Attack1 = 0;
        public const int Attack2 = 1;
        public const int Attack3 = 2;
        public const int Attack4 = 3;
        public AttackControllerBase AttackController;
        public static BeforeUseSkill BUDDY_FORGET_THIS = BeforeUseSkill.New(false, null);
        private const int CANCEL_CD_KEY = -1;
        public CDConroller CdConroller;
        public static BeforeUseSkill DAMN_JUST_USE_IT_NOW = BeforeUseSkill.New(true, null);
        public static List<ActionDisplay> EmptyTargetList = new List<ActionDisplay>();
        public Predicate<SysSkillBaseVo> EvaluateFunc;
        private bool hasInitDungeonCD;
        private CommandHandler.Command lastCmd;
        private SysGeneralVo lastGeneralInfo;
        public List<uint> LearnedSkillList;
        public int maxSkillType;
        public ActionControler MeController;
        public const int Roll = 8;
        public const int Skill1 = 4;
        public const int Skill2 = 5;
        public const int Skill3 = 6;
        public const int Skill4 = 7;
        private uint skillCacheId;
        private string skillCacheSound = string.Empty;
        public List<int> SkillList;
        public List<uint> SuperSkillList;

        public void AddCdParamBySkillType(int skillType, params object[] extendParams)
        {
            if (!this._skillCdParamsDic.ContainsKey(skillType))
            {
                this._skillCdParamsDic[skillType] = new SkillCDParams();
            }
            this._skillCdParamsDic[skillType].paramList.AddRange(extendParams.ToList<object>());
        }

        public void AddCdParams(int skillType, bool inState, bool canClick, CDStateChangeAction action, params object[] para)
        {
            if (!this._skillCdParamsDic.ContainsKey(skillType))
            {
                this._skillCdParamsDic[skillType] = new SkillCDParams();
            }
            this._skillCdParamsDic[skillType].SetInfo(inState, canClick, para.ToList<object>(), action);
        }

        private void AddPriorityCDBySubType(int skillType)
        {
            if (this._currentSkillVo.subtype == 4)
            {
                this.DealBlinkDistanceCd(skillType);
            }
        }

        private void AddSelfBuff(SysSkillBaseVo skillVo)
        {
            if (skillVo.target_type == 1)
            {
                List<PDamageBuff> damageBuffListFromString = StringUtils.GetDamageBuffListFromString(skillVo.buff_list);
                this.MeController.buffController.AddBuff(damageBuffListFromString, null);
            }
        }

        private void Awake()
        {
            this._attackSoundList = new List<string>();
            this._hitSoundList = new List<string>();
            this.CdConroller = new CDConroller();
            this.LearnedSkillList = new List<uint>();
            this.SkillList = new List<int>();
            this.SuperSkillList = new List<uint>();
            this._skillDelayDic = new Dictionary<uint, float>();
            this._skillSoundDic = new Dictionary<uint, bool>();
        }

        private BeforeUseSkill BeforeUseSkillCheck(bool isIgnoreDistance, bool isIgnoreLookAt, ActionDisplay targetDisplay, SysSkillBaseVo skillVo, SysSkillActionVo actionVo, int skillType)
        {
            if (isIgnoreLookAt)
            {
                return DAMN_JUST_USE_IT_NOW;
            }
            if ((this.MeController.Me is MeDisplay) && (targetDisplay != null))
            {
                if ((targetDisplay.Controller == null) || (targetDisplay.GoBase == null))
                {
                    return DAMN_JUST_USE_IT_NOW;
                }
                UnityLog.Log.AI(base.gameObject, " Look At Target ");
                return this.BeforeUsingSkill(skillVo, actionVo, this.MeController.Me.Type, isIgnoreDistance, skillType, targetDisplay);
            }
            if ((this.MeController.Me is MeDisplay) && (skillVo.subtype == 3))
            {
                return DAMN_JUST_USE_IT_NOW;
            }
            if ((this.MeController.Me is MonsterDisplay) && (this.MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO.type == 3))
            {
                return BeforeUseSkill.New(true, targetDisplay);
            }
            return this.BeforeUsingSkill(skillVo, actionVo, this.MeController.Me.Type, isIgnoreDistance, skillType, targetDisplay);
        }

        private BeforeUseSkill BeforeUsingSkill(SysSkillBaseVo skillVo, SysSkillActionVo actionVo, int displayType, bool isIgnore, int skillType, ActionDisplay target = null)
        {
            ActionDisplay display;
            float num;
            UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " Find Skill Target ", target, " skillType ", skillType }));
            switch (skillVo.target_type)
            {
                case 1:
                    return DAMN_JUST_USE_IT_NOW;

                case 2:
                {
                    List<ActionDisplay> myTeammatesExceptMe = ObjectManager.GetMyTeammatesExceptMe(base.gameObject);
                    if ((myTeammatesExceptMe != null) && (myTeammatesExceptMe.Count != 0))
                    {
                        display = ObjectManager.FindNearestEnemy(base.gameObject, myTeammatesExceptMe);
                        if (display != null)
                        {
                            break;
                        }
                    }
                    return DAMN_JUST_USE_IT_NOW;
                }
                default:
                    if (target == null)
                    {
                        List<ActionDisplay> enemyDisplay = ObjectManager.GetEnemyDisplay(base.gameObject);
                        if ((enemyDisplay.Count <= 0) && !(this.MeController is MeControler))
                        {
                            return DAMN_JUST_USE_IT_NOW;
                        }
                        display = ObjectManager.FindNearestEnemy(base.gameObject, enemyDisplay);
                        if (display == null)
                        {
                            return DAMN_JUST_USE_IT_NOW;
                        }
                    }
                    else
                    {
                        if (target.GetMeVoByType<BaseRoleVo>().IsEmptyHp || (target.GoBase == null))
                        {
                            return BUDDY_FORGET_THIS;
                        }
                        display = target;
                    }
                    break;
            }
            if (actionVo.IsBullet)
            {
                num = (skillVo.available_atk_range + this.MeController.ExtendedBulletFlyDistance) * 0.001f;
            }
            else
            {
                num = skillVo.available_atk_range * 0.001f;
            }
            float distance = DamageCheck.GetDistance(display.GoBase.transform, base.transform);
            bool skipSKillCoveredCheck = isIgnore || (displayType != 100);
            return this.CheckRotateToTarget(skillVo, actionVo, display, num, distance, skipSKillCoveredCheck, skillType);
        }

        public void ChangeGeneralSkill()
        {
            this.Init(false);
        }

        public List<PDamageBuff> CheckAddAdditionalBuff(SysSkillBaseVo skillBaseVo)
        {
            if (!this.IsInActivation || (this.AdditionBuffList.Count == 0))
            {
                return null;
            }
            if (this.EvaluateFunc == null)
            {
                return this.AdditionBuffList;
            }
            return (!this.EvaluateFunc(skillBaseVo) ? null : this.AdditionBuffList);
        }

        private void CheckAddBuffWhenUseSkill(SysSkillBaseVo skillVo)
        {
            string str = skillVo.buff_list_when_use_skill;
            if (str.Length >= "[[1,2,3]]".Length)
            {
                BuffController buffController = this.MeController.buffController;
                if (<>f__am$cache25 == null)
                {
                    <>f__am$cache25 = x => x.Controller.buffController != null;
                }
                if (<>f__am$cache26 == null)
                {
                    <>f__am$cache26 = x => x.Controller.buffController;
                }
                List<BuffController> list = AppMap.Instance.SelfplayerList.Where<PlayerDisplay>(<>f__am$cache25).Select<PlayerDisplay, BuffController>(<>f__am$cache26).ToList<BuffController>();
                int[] arrayStringToInt = StringUtils.GetArrayStringToInt(str);
                for (int i = 0; i < arrayStringToInt.Length; i += 3)
                {
                    int buffId = arrayStringToInt[i + 1];
                    int buffLvl = arrayStringToInt[i + 2];
                    switch (arrayStringToInt[i])
                    {
                        case 0:
                            if (buffController != null)
                            {
                                buffController.AddBuff(buffId, buffLvl, null);
                            }
                            break;

                        case 1:
                            foreach (BuffController controller2 in list)
                            {
                                controller2.AddBuff(buffId, buffLvl, null);
                            }
                            break;
                    }
                }
            }
        }

        private bool CheckCDState(int skillType)
        {
            if (!this._skillCdParamsDic.ContainsKey(skillType) || (this._skillCdParamsDic[skillType] == null))
            {
                return false;
            }
            SkillCDParams @params = this._skillCdParamsDic[skillType];
            if (!@params.isInState)
            {
                return false;
            }
            if (@params.canClick)
            {
                @params.DoChange();
            }
            return true;
        }

        public bool CheckDamage(Vector3 effectPosition, SysSkillBaseVo skillVo, int index = 0, Transform effectTransform = null, ActionDisplay ad = null, int count = 0, int ratio = 0x3e8)
        {
            DamageCheckOptions opt = new DamageCheckOptions();
            return this.CheckDamage1(effectPosition, skillVo, ref opt, index, effectTransform, ad, count, ratio);
        }

        public bool CheckDamage1(Vector3 effectPosition, SysSkillBaseVo skillVo, ref DamageCheckOptions opt, int index = 0, Transform effectTransform = null, ActionDisplay ad = null, int count = 0, int ratio = 0x3e8)
        {
            if (base.gameObject == null)
            {
                return false;
            }
            List<ActionDisplay> enemyDisplay = ObjectManager.GetEnemyDisplay(base.gameObject);
            if ((opt.excludeList != null) && (opt.excludeList.Count > 0))
            {
                <CheckDamage1>c__AnonStorey10B storeyb = new <CheckDamage1>c__AnonStorey10B {
                    excludeList = opt.excludeList
                };
                enemyDisplay = enemyDisplay.Filter<ActionDisplay>(new Predicate<ActionDisplay>(storeyb.<>m__1B6)).ToList<ActionDisplay>();
            }
            if ((ad != null) && enemyDisplay.Contains(ad))
            {
                enemyDisplay.Remove(ad);
                enemyDisplay.Insert(0, ad);
            }
            if (skillVo == null)
            {
                return false;
            }
            return this.CheckDamage2(effectPosition, skillVo, ref opt, index, effectTransform, enemyDisplay, count, ratio);
        }

        private bool CheckDamage2(Vector3 effectPosition, SysSkillBaseVo skillVo, ref DamageCheckOptions opt, int index, Transform effectTransform, IList<ActionDisplay> enemyList, int hurtCount, int ratio)
        {
            Transform transform;
            bool needSyn = AppMap.Instance.mapParserII.NeedSyn;
            bool flag2 = false;
            if (effectTransform != null)
            {
                transform = effectTransform;
            }
            else
            {
                transform = this._selfTransform;
            }
            switch (this.MeController.Me.Type)
            {
                case 100:
                    if (this.MeController.GetMeVo().SubType == 1)
                    {
                        flag2 = DamageCheck.Instance.CheckMeInjured(this.MeController, skillVo, effectPosition, transform, enemyList, this._checkedTime, ref opt, index, needSyn, hurtCount, ratio);
                        if (flag2)
                        {
                            this.PlayBeAttackedSound(skillVo.unikey);
                        }
                    }
                    else
                    {
                        flag2 = DamageCheck.CheckPlayerInjured(this.MeController.GetMeVoByType<PlayerVo>(), skillVo, effectPosition, transform, enemyList, this._checkedTime, ref opt, index, needSyn, hurtCount, ratio);
                    }
                    break;

                case 400:
                    flag2 = DamageCheck.Instance.MonsterCheckInjured(this.MeController, skillVo, effectPosition, transform, enemyList, AppMap.Instance.IsInWifiPVP, hurtCount, ratio);
                    if (this.MeController.Me.DefenceEffect != null)
                    {
                        this.MeController.Me.DefenceEffect.transform.position = (Vector3) (Vector3.one * 1000f);
                    }
                    break;
            }
            if (skillVo.shake && (this.MeController.GetMeVo().Id == MeVo.instance.Id))
            {
                CameraEffectManager.ShakeCamera(0f, 0.6f);
            }
            if (flag2)
            {
                this._checkedTime++;
            }
            return flag2;
        }

        private CombatUtil.CheckResult CheckNewEnemy(GameObject lastEnemy)
        {
            if ((Time.time - this._lastCheckTime) >= 1f)
            {
                GameObject gameObject = base.gameObject;
                this._lastCheckTime = Time.time;
                CombatUtil.CheckResult result = CombatUtil.CheckNewEnemy(gameObject, lastEnemy);
                if (result.ok && CombatUtil.CheckPlayerCanSee(gameObject, result.enemy))
                {
                    return result;
                }
            }
            return CombatUtil.FalseResult;
        }

        private BeforeUseSkill CheckRotateToTarget(SysSkillBaseVo skillVo, SysSkillActionVo actionVo, ActionDisplay nearestEnemy, float maxRange, float actualDistance, bool skipSKillCoveredCheck, int skillType)
        {
            float boundDistance = nearestEnemy.BoundDistance;
            actualDistance -= boundDistance;
            bool flag = !skipSKillCoveredCheck || JoystickController.instance.joystick.onPressed;
            if (base.GetComponent<StateMachine>().GetCurState().type == AIState.AIStateEnum.MOVE_KEYBOARD)
            {
                flag = false;
            }
            if (!actionVo.IsBullet)
            {
                if (DamageCheck.IsSkillCovered3D(skillVo, base.transform, nearestEnemy.Controller.transform.position, boundDistance))
                {
                    return this.DoRotateToTarget(nearestEnemy, true);
                }
                if (((actualDistance - maxRange) > 0f) && ((actualDistance - maxRange) <= this._skillChaseRange))
                {
                    if (flag)
                    {
                        UnityLog.Log.AI(base.gameObject, " LookAtTarget " + nearestEnemy.GoBase);
                        return this.RunToTargetThenUseSkill(skillVo, nearestEnemy, maxRange * 0.95f, actualDistance, skillType);
                    }
                    return DAMN_JUST_USE_IT_NOW;
                }
                if (actualDistance < maxRange)
                {
                    return this.DoRotateToTarget(nearestEnemy, true);
                }
                return DAMN_JUST_USE_IT_NOW;
            }
            if (actualDistance < maxRange)
            {
                return this.DoRotateToTarget(nearestEnemy, true);
            }
            if ((((actualDistance - maxRange) > 0f) && ((actualDistance - maxRange) <= this._skillChaseRange)) && flag)
            {
                UnityLog.Log.AI(base.gameObject, " LookAtTarget " + nearestEnemy.GoBase);
                return this.RunToTargetThenUseSkill(skillVo, nearestEnemy, maxRange * 0.95f, actualDistance, skillType);
            }
            return DAMN_JUST_USE_IT_NOW;
        }

        private bool CheckShouldNotUseskill(int reqSkillType)
        {
            if (this.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.IsFrozen)
            {
                return true;
            }
            if ((reqSkillType >= 0) && (reqSkillType <= 3))
            {
                if (!this.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanAttack)
                {
                    return true;
                }
            }
            else if (((reqSkillType >= 4) && (reqSkillType <= 7)) && !this.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanUseSkill)
            {
                return true;
            }
            return false;
        }

        private void CheckSkillSubType(SysSkillBaseVo skillVo)
        {
            switch (((SkillConst.HeroSkillSubtype) skillVo.subtype))
            {
                case SkillConst.HeroSkillSubtype.turn_attr_to_other:
                    SkillUtil.StartAttrTuring(this.MeController, skillVo);
                    return;

                case SkillConst.HeroSkillSubtype.activation:
                    SkillUtil.AddActivation(this.MeController, skillVo);
                    return;

                case SkillConst.HeroSkillSubtype.summon:
                    SkillUtil.Summon(this.MeController.Me.GetMeByType<ActionDisplay>(), skillVo);
                    break;

                case SkillConst.HeroSkillSubtype.run_straight:
                    this.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.AddState(StateVo.BLINK);
                    break;
            }
        }

        public void ClearLastTargetForAttack()
        {
            this._lastTargetForAttack = null;
        }

        public static Func<ActionDisplay, bool> CommonAliveAndAttackableSelector(Vector3 myPos, uint myCamp, float dis)
        {
            <CommonAliveAndAttackableSelector>c__AnonStorey111 storey = new <CommonAliveAndAttackableSelector>c__AnonStorey111 {
                myPos = myPos,
                myCamp = myCamp,
                dis = dis
            };
            return new Func<ActionDisplay, bool>(storey.<>m__1C0);
        }

        public void ContinueHealSkill()
        {
            if ((this.MeController.Me == AppMap.Instance.me) && Singleton<BattleBottomRightView>.Instance.IsPvpInBattle)
            {
                Singleton<BattleBottomRightView>.Instance.ContinueSkill4WhenOutOfBattle();
            }
        }

        private void DealBlinkDistanceCd(int skillType)
        {
            <DealBlinkDistanceCd>c__AnonStorey10C storeyc = new <DealBlinkDistanceCd>c__AnonStorey10C {
                skillType = skillType,
                <>f__this = this
            };
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(this._currentSkillVo.param_list);
            object[] para = new object[] { this.MeController.Me.GoBase.transform.position };
            this.AddCdParams(storeyc.skillType, true, false, new CDStateChangeAction(storeyc.<>m__1B7), para);
            this.CdConroller.AddPrioritySkillCd(storeyc.skillType, arrayStringToInt[2] * 0.001f, false, CDPriority.First, new CDEndCallBack(storeyc.<>m__1B8));
            this.CdConroller.AddPrioritySkillCd(storeyc.skillType, arrayStringToInt[1] * 0.001f, false, CDPriority.Second, new CDEndCallBack(storeyc.<>m__1B9));
            EffectMgr.Instance.CreateSceneEffect(arrayStringToInt[3].ToString(), this.MeController.Me.GoBase.transform.position, null, false, true, new com.game.module.effect.Effect.EffectCreateCallback(storeyc.<>m__1BA), null);
        }

        private void DoBlinkDistanceSkill()
        {
            int num = StringUtils.GetArrayStringToInt(this._currentSkillVo.param_list)[0];
            SkillUtil.StartDistanceBlink(this.MeController, num * 0.001f);
        }

        public void DoEffectDuringSkill()
        {
            switch (((SkillConst.HeroSkillSubtype) this._currentSkillVo.subtype))
            {
                case SkillConst.HeroSkillSubtype.blink_distance:
                    this.DoBlinkDistanceSkill();
                    break;

                case SkillConst.HeroSkillSubtype.run_straight:
                    this.DoMoveStraightSkill();
                    break;
            }
        }

        private void DoMoveStraightSkill()
        {
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(this.CurrentSkillVo.param_list);
            SkillUtil.StartMoveStraight(this.MeController, arrayStringToInt[0] * 0.001f, arrayStringToInt[1] * 0.001f, arrayStringToInt[2] == 1, arrayStringToInt[3] == 1);
        }

        public void DoRoleModelChange(uint modelId)
        {
            if (modelId != 0)
            {
                this.UpdateChangeModelCD(this._currentSkillVo);
            }
            PlayerVo meVoByType = this.MeController.Me.GetMeVoByType<PlayerVo>();
            if (modelId != meVoByType.generalTemplateInfo.model_id)
            {
                SysGeneralVo lastGeneralInfo;
                if (modelId == 0)
                {
                    if (this.lastGeneralInfo == null)
                    {
                        return;
                    }
                    lastGeneralInfo = this.lastGeneralInfo;
                    this.lastGeneralInfo = null;
                }
                else
                {
                    this.lastGeneralInfo = VoUtils.CloneObject<SysGeneralVo>(meVoByType.generalTemplateInfo);
                    lastGeneralInfo = BaseDataMgr.instance.GetGeneralVo(modelId, this.GetGeneralQualityBySkillTalent());
                }
                if (lastGeneralInfo != null)
                {
                    this.MeController.Me.GetMeVoByType<PlayerVo>().generalTemplateInfo = lastGeneralInfo;
                    int currentStatu = this.MeController.StatuController.CurrentStatu;
                    this.MeController.Me.ChangeStyle((uint) lastGeneralInfo.model_id);
                    this.MeController.StatuController.SetStatusWhenChangeModel(currentStatu);
                    if (!AppMap.Instance.IsInWifiPVP || (meVoByType.Camp == 0))
                    {
                        meVoByType.SkillInfoList = GetModelSkillList(meVoByType);
                        Singleton<BattleBottomRightView>.Instance.SetSkillIcon();
                        this.ChangeGeneralSkill();
                    }
                }
            }
        }

        private BeforeUseSkill DoRotateToTarget(ActionDisplay nearestEnemy, bool needChaseOrTurnToTarget)
        {
            if (((nearestEnemy.GetMeVoByType<BaseRoleVo>() == null) || (nearestEnemy.GoBase == null)) || (nearestEnemy.Controller == null))
            {
                return DAMN_JUST_USE_IT_NOW;
            }
            if (needChaseOrTurnToTarget)
            {
                base.transform.LookAtTarget(nearestEnemy.GoBase.transform.position);
            }
            return BeforeUseSkill.New(true, nearestEnemy);
        }

        private void DoUseSkillById(bool isIgnore, bool isFakeUseSkill, ref BeforeCastSkill bc, ref AfterCastSkill ac, SysSkillBaseVo skillVo, SysSkillActionVo actionVo, ref BeforeUseSkill checkResult, ActionDisplay targetDisplay)
        {
            UnityLog.Log.AI(base.gameObject, " who use skill " + targetDisplay);
            if ((targetDisplay == null) || (targetDisplay.GoBase == null))
            {
                targetDisplay = checkResult.display;
            }
            float rate = 1f;
            if (this.lastCmd != null)
            {
                UnityLog.Log.AI(base.gameObject, " skillRate " + this.lastCmd.rate);
                rate = this.lastCmd.rate;
            }
            if (targetDisplay != null)
            {
                CommandHandler.AddCommandStatic(base.gameObject, string.Format("skill {0} {1}", actionVo.action_id, targetDisplay.GoBase.GetComponent<BaseControler>().GetControllerId()), rate);
            }
            else
            {
                CommandHandler.AddCommandStatic(base.gameObject, string.Format("skill {0} {1}", actionVo.action_id, -1), rate);
            }
            if (bc != null)
            {
                bc(this.MeController);
                bc = null;
            }
            checkResult = this.ShowActionAndEffect(isIgnore, isFakeUseSkill, skillVo, actionVo, checkResult);
            if (!isFakeUseSkill)
            {
                this.CheckSkillSubType(skillVo);
                this.CheckAddBuffWhenUseSkill(skillVo);
            }
            if (ac != null)
            {
                ac(this.MeController);
                ac = null;
            }
        }

        public static Func<ActionDisplay, bool> FilterGivenTargetList(ICollection<ActionDisplay> excludeList)
        {
            <FilterGivenTargetList>c__AnonStorey113 storey = new <FilterGivenTargetList>c__AnonStorey113 {
                excludeList = excludeList
            };
            return new Func<ActionDisplay, bool>(storey.<>m__1C2);
        }

        [Obsolete]
        private ActionDisplay FindNearestEnemy(IList<ActionDisplay> enemyList)
        {
            ActionDisplay display = null;
            float maxValue = float.MaxValue;
            Vector3 position = this._selfTransform.position;
            int num2 = 0;
            int count = enemyList.Count;
            while (num2 < count)
            {
                Vector3 y = enemyList[num2].Controller.transform.position;
                float distance = DamageCheck.GetDistance(position, y);
                if ((enemyList[num2].GetMeVoByType<BaseRoleVo>().CurHp > 0) && (distance < maxValue))
                {
                    maxValue = distance;
                    display = enemyList[num2];
                }
                num2++;
            }
            UnityLog.Log.AI(base.gameObject, " Find nearest Monster Enemy " + display);
            return display;
        }

        private ActionDisplay FixAttackTarget(int skillType, ActionDisplay targetDisplay)
        {
            if ((skillType >= 0) && (skillType <= 3))
            {
                if ((targetDisplay == null) && (this._lastTargetForAttack != null))
                {
                    BaseRoleVo meVoByType = this._lastTargetForAttack.GetMeVoByType<BaseRoleVo>();
                    if (!meVoByType.IsEmptyHp && (meVoByType.Camp == 1))
                    {
                        targetDisplay = this._lastTargetForAttack;
                        return targetDisplay;
                    }
                    this._lastTargetForAttack = null;
                }
                return targetDisplay;
            }
            this.MeController.StatuController.ResetCombStatu();
            return targetDisplay;
        }

        public float GetAttack1AvailableAtkRange()
        {
            SysSkillBaseVo vo = this.Attack1Vo;
            return ((vo == null) ? 0f : ((float) vo.available_atk_range));
        }

        public SkillCDParams GetCdParams(int skillType)
        {
            if (this._skillCdParamsDic.ContainsKey(skillType))
            {
                return this._skillCdParamsDic[skillType];
            }
            return null;
        }

        public static List<ActionDisplay> GetEnemyDisplays(Transform trans, uint myCamp, float dis)
        {
            return GetEnemyDisplays(trans.position, myCamp, dis);
        }

        public static List<ActionDisplay> GetEnemyDisplays(Vector3 myPos, uint myCamp, float dis)
        {
            Collection<Func<ActionDisplay, bool>> precond = new Collection<Func<ActionDisplay, bool>> {
                CommonAliveAndAttackableSelector(myPos, myCamp, dis)
            };
            return StandardQueryForEnemy(precond);
        }

        private byte GetGeneralQualityBySkillTalent()
        {
            return 0;
        }

        public Collider[] GetHitColliders()
        {
            return Physics.OverlapSphere(this.MeController.Me.characterController.center + this.MeController.transform.position, 0.5f);
        }

        private static List<SkillInfoVo> GetModelSkillList(PlayerVo info)
        {
            <GetModelSkillList>c__AnonStorey10D storeyd = new <GetModelSkillList>c__AnonStorey10D {
                skillGroupIdArray = StringUtils.GetStringToInt(info.generalTemplateInfo.skill_list).ToList<int>()
            };
            return info.SkillInfoList.Select<SkillInfoVo, SkillInfoVo>(new Func<SkillInfoVo, int, SkillInfoVo>(storeyd.<>m__1BD)).ToList<SkillInfoVo>();
        }

        public static List<ActionDisplay> GetMySideDisplay()
        {
            List<ActionDisplay> teamDisplay = GetTeamDisplay();
            if (<>f__am$cache27 == null)
            {
                <>f__am$cache27 = x => x.GetMeVoByType<BaseRoleVo>().Camp == 0;
            }
            List<ActionDisplay> collection = AppMap.Instance.monsterList.Where<MonsterDisplay>(<>f__am$cache27).Cast<ActionDisplay>().ToList<ActionDisplay>();
            teamDisplay.AddRange(collection);
            return teamDisplay;
        }

        public ActionDisplay GetNearestAttackableTarget(Vector3 myPos, uint myCamp, float maxDistance)
        {
            Collection<Func<ActionDisplay, bool>> precond = new Collection<Func<ActionDisplay, bool>> {
                CommonAliveAndAttackableSelector(myPos, myCamp, maxDistance)
            };
            return GetTheNearestOne(StandardQueryForEnemy(precond), myPos);
        }

        public ActionDisplay GetNearestAttackableTargetForCatapult(Vector3 myPos, uint myCamp, float maxDistance, ICollection<ActionDisplay> exculudeList, ActionDisplay meDisplay, bool canRepeat = true)
        {
            Collection<Func<ActionDisplay, bool>> precond = new Collection<Func<ActionDisplay, bool>> {
                CommonAliveAndAttackableSelector(myPos, myCamp, maxDistance),
                FilterGivenTargetList(exculudeList)
            };
            ActionDisplay theNearestOne = GetTheNearestOne(StandardQueryForEnemy(precond), myPos);
            if (theNearestOne != null)
            {
                return theNearestOne;
            }
            if (!canRepeat)
            {
                return null;
            }
            Collection<Func<ActionDisplay, bool>> collection2 = new Collection<Func<ActionDisplay, bool>> {
                CommonAliveAndAttackableSelector(myPos, myCamp, maxDistance)
            };
            if (meDisplay != null)
            {
                collection2.Add(FilterGivenTargetList(new List<ActionDisplay> { meDisplay }));
            }
            return GetTheNearestOne(SearchFromGivenTargetList(exculudeList, collection2), myPos);
        }

        public ActionDisplay GetNearestTeammateForCatapult(Vector3 myPos, uint myCamp, float maxDistance, ICollection<ActionDisplay> excludeList, ActionDisplay meDisplay, bool canRepeat = true)
        {
            Collection<Func<ActionDisplay, bool>> precond = new Collection<Func<ActionDisplay, bool>> {
                MyAliveTeammateSelector(myPos, myCamp, maxDistance),
                FilterGivenTargetList(excludeList)
            };
            ActionDisplay theNearestOne = GetTheNearestOne(StandardQueryForTeammate(precond), myPos);
            if (theNearestOne != null)
            {
                return theNearestOne;
            }
            if (!canRepeat)
            {
                return null;
            }
            Collection<Func<ActionDisplay, bool>> collection2 = new Collection<Func<ActionDisplay, bool>> {
                MyAliveTeammateSelector(myPos, myCamp, maxDistance)
            };
            if (meDisplay != null)
            {
                collection2.Add(FilterGivenTargetList(new List<ActionDisplay> { meDisplay }));
            }
            return GetTheNearestOne(SearchFromGivenTargetList(excludeList, collection2), myPos);
        }

        private string GetRandAttackSound(string sounds)
        {
            return this.GetRandSound(sounds, this._attackSoundList);
        }

        private string GetRandHitSound(string sounds)
        {
            return this.GetRandSound(sounds, this._hitSoundList);
        }

        private string GetRandSound(string sounds)
        {
            string[] valueListFromString = StringUtils.GetValueListFromString(sounds, ',');
            if (valueListFromString.Length == 0)
            {
                return string.Empty;
            }
            int index = UnityEngine.Random.Range(0, valueListFromString.Length);
            return valueListFromString[index];
        }

        private string GetRandSound(string sounds, IList<string> soundList)
        {
            string[] valueListFromString = StringUtils.GetValueListFromString(sounds, ',');
            if (valueListFromString.Length == 1)
            {
                return valueListFromString[0];
            }
            if (valueListFromString.Length == 0)
            {
                return string.Empty;
            }
            if (soundList.Count == 0)
            {
                foreach (string str in valueListFromString)
                {
                    soundList.Add(str);
                }
            }
            int index = UnityEngine.Random.Range(0, soundList.Count);
            string str2 = soundList[index];
            soundList.RemoveAt(index);
            return str2;
        }

        private float GetSkillDelay(uint skillId)
        {
            if (this._skillDelayDic.ContainsKey(skillId))
            {
                return this._skillDelayDic[skillId];
            }
            return 0f;
        }

        private uint GetSkillIdFromSkillType(int skillType)
        {
            try
            {
                return this.LearnedSkillList[skillType];
            }
            catch
            {
                UnityEngine.Debug.LogWarning(string.Concat(new object[] { "找不到技能!, 技能标号 = ", skillType, " 显示类型:", this.MeController.GetMeVo().Type }));
                return 0;
            }
        }

        private string GetSoundHitPathByAttack(uint skillId)
        {
            SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(skillId);
            if (sysSkillBaseVo == null)
            {
                com.u3d.bases.debug.Log.info(this, "SkillBaseVo表不存在id为" + skillId + "的技能");
                return null;
            }
            return sysSkillBaseVo.sound_hit.ToString(CultureInfo.InvariantCulture);
        }

        public static List<ActionDisplay> GetTeamDisplay()
        {
            return AppMap.Instance.SelfplayerList.Cast<ActionDisplay>().ToList<ActionDisplay>();
        }

        public static ActionDisplay GetTheNearestOne(IList<ActionDisplay> list, Vector3 myPos)
        {
            <GetTheNearestOne>c__AnonStorey114 storey = new <GetTheNearestOne>c__AnonStorey114 {
                myPos = myPos
            };
            switch (list.Count)
            {
                case 0:
                    return null;

                case 1:
                    return list[0];
            }
            list = list.OrderBy<ActionDisplay, float>(new Func<ActionDisplay, float>(storey.<>m__1C3)).ToList<ActionDisplay>();
            return list[0];
        }

        private void HandleAfterUseSkill(int skillType, ActionDisplay target)
        {
            this.HandleCD(skillType);
            this.HandleSkillSound(skillType);
            ulong id = 0L;
            byte num2 = 4;
            if (target != null)
            {
                id = target.GetVo().Id;
                num2 = Convert.ToByte(DamageCheck.GetSynType(target.GetVo().Type));
                this.LastTarget = target;
                this._lastTargetForAttack = target;
            }
            if (this.MeController.GetMeVo().SubType == 1)
            {
                MeVo.instance.DataUpdate(MeVo.DataHpMpUpdate);
                if (AppMap.Instance.mapParserII.NeedSyn)
                {
                    Singleton<SkillMode>.Instance.SendHeroUseSkill(this.MeController.Me.GetVo().Id, 2, this._currentSkillId, this.MeController.Me.GoBase.transform.eulerAngles);
                }
                if (AppMap.Instance.IsInWifiPVP)
                {
                    Singleton<WifiPvpMode>.Instance.UseSkillSync(this.MeController.Me.GetVo().Id, 2, this._currentSkillId, this.MeController.Me.GoBase.transform.eulerAngles, id, num2, this.MeController.Me.GetMeByType<ActionDisplay>());
                }
            }
            else if (((this.MeController.Me.Type == 400) && AppMap.Instance.IsInWifiPVP) && (this.MeController.GetMeVo().Camp == 0))
            {
                Singleton<WifiPvpMode>.Instance.UseSkillSync(this.MeController.Me.GetVo().Id, 1, this._currentSkillId, this.MeController.Me.GoBase.transform.eulerAngles, id, num2, this.MeController.Me.GetMeByType<ActionDisplay>());
            }
        }

        private void HandleCD(int skillType)
        {
            this.AddPriorityCDBySubType(skillType);
            for (int i = 0; i < this.LearnedSkillList.Count; i++)
            {
                if (i == skillType)
                {
                    this.CdConroller.UpdateNormalSkillCd(skillType);
                }
                else if (i != 7)
                {
                    this.CdConroller.UpdatePublicCd(i, this._currentSkillVo.cd_public * 0.001f);
                }
            }
        }

        private void HandleSkillSound(int skillType)
        {
        }

        public bool HasHitEnemy()
        {
            foreach (Collider collider in this.GetHitColliders())
            {
                if (collider.CompareTag("MonsterDisplay"))
                {
                    return true;
                }
            }
            return false;
        }

        private void Init(bool needCover = false)
        {
            this.AttackController = this.MeController.AttackController;
            this._selfTransform = this.MeController.Me.GoBase.transform;
            if (this.MeController.Me.GetVo().SubType == 1)
            {
                PlayerVo meVoByType = this.MeController.Me.GetMeVoByType<PlayerVo>();
                if (!AppMap.Instance.IsEditor)
                {
                    SysSceneVo sceneVo = BaseDataMgr.instance.GetSceneVo(MeVo.instance.mapId);
                    this.LearnedSkillList = (sceneVo.type != 1) ? meVoByType.GetSkillList() : Singleton<GeneralMode>.Instance.GetCurRoleSkillList(meVoByType.Id);
                    if (AppMap.Instance.IsInWifiPVP)
                    {
                        this.LearnedSkillList[7] = AiConfig.ROLE_RECOVERY_SKILL_ID_IN_PVP;
                    }
                }
                else
                {
                    this.LearnedSkillList = meVoByType.GetSkillList();
                }
                this.maxSkillType = this.LearnedSkillList.Count - 1;
                this._skillChaseRange = meVoByType.generalTemplateInfo.skill_chase_range * 0.001f;
            }
            else
            {
                switch (this.MeController.Me.Type)
                {
                    case 100:
                        if (AppMap.Instance.IsInArena)
                        {
                            this.LearnedSkillList = new List<uint>();
                            PlayerVo vo5 = this.MeController.Me.GetMeVoByType<PlayerVo>();
                            if (vo5 != null)
                            {
                                this.LearnedSkillList = ArenaManager.Instance.GetSkillIdListForEnemyInArena(vo5.Id);
                                this.maxSkillType = this.LearnedSkillList.Count - 1;
                            }
                        }
                        break;

                    case 400:
                    {
                        this.LearnedSkillList = new List<uint>();
                        MonsterVo vo3 = this.MeController.Me.GetMeVoByType<MonsterVo>();
                        if (vo3 != null)
                        {
                            foreach (string str in StringUtils.GetValueListFromString(vo3.MonsterVO.skill_ids, ','))
                            {
                                this.LearnedSkillList.Add(uint.Parse(str));
                            }
                            this.maxSkillType = this.LearnedSkillList.Count - 1;
                            this.DirectAtkRange = vo3.MonsterVO.attack_range * 0.001f;
                        }
                        break;
                    }
                }
            }
            this.InitTotalCdTime(needCover);
            this.InitSkillDelayDic();
            this.InitEnterDungeonCD();
        }

        private void InitEnterDungeonCD()
        {
            if ((GlobalData.isInCopy && (this.MeController.Me.GetVo().SubType == 1)) && !this.hasInitDungeonCD)
            {
                int num;
                this.hasInitDungeonCD = true;
                if (AppMap.Instance.IsEditor || AppMap.Instance.IsInArena)
                {
                    num = 0;
                }
                else if (AppMap.Instance.IsInWifiPVP)
                {
                    num = WifiPvpManager.Instance.PvpTemplate.pvp_cd;
                }
                else
                {
                    num = DungeonMgr.Instance.curDungeonSysVo.dungeon_cd;
                }
                if (num != -1)
                {
                    List<uint> skillList = this.MeController.Me.GetMeVoByType<MeVo>().GetSkillList();
                    for (int i = 0; i < skillList.Count; i++)
                    {
                        SysSkillBaseVo dataById = BaseDataMgr.instance.GetDataById<SysSkillBaseVo>(skillList[i]);
                        if ((dataById != null) && (dataById.dungeon_cd != -1))
                        {
                            float totalTime = (num + dataById.dungeon_cd) * 0.001f;
                            this.CdConroller.AddPrioritySkillCd(i, totalTime, true, CDPriority.First, null);
                        }
                    }
                }
            }
        }

        private void InitSkillDelayDic()
        {
            this._skillDelayDic[0x2711] = 0.13f;
            this._skillDelayDic[0x2712] = 0.13f;
            this._skillDelayDic[0x2713] = 0.07f;
            this._skillDelayDic[0x2714] = 0.23f;
            this._skillDelayDic[0xea61] = 0.17f;
            this._skillDelayDic[0xea62] = 0.067f;
            this._skillDelayDic[0xea63] = 0.1f;
            this._skillDelayDic[0x11171] = 0.17f;
            this._skillDelayDic[0x11172] = 0.07f;
            this._skillDelayDic[0x11173] = 0.03f;
            this._skillDelayDic[0x11174] = 0.27f;
            for (uint i = 0x11940; i <= 0x11945; i++)
            {
                this._skillSoundDic[i] = true;
            }
            for (uint j = 0x11d28; j <= 0x11d2d; j++)
            {
                this._skillSoundDic[j] = true;
            }
            for (uint k = 0x12110; k <= 0x12115; k++)
            {
                this._skillSoundDic[k] = true;
            }
        }

        public void InitTotalCdTime(bool needCover)
        {
            for (int i = 0; i < this.LearnedSkillList.Count; i++)
            {
                SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(this.LearnedSkillList[i]);
                if (sysSkillBaseVo != null)
                {
                    this.CdConroller.AddNormalSkillCd(i, sysSkillBaseVo.cd * 0.001f, needCover, null);
                }
            }
        }

        public static bool IsAllMemberHpBelowRatio(int val)
        {
            <IsAllMemberHpBelowRatio>c__AnonStorey115 storey = new <IsAllMemberHpBelowRatio>c__AnonStorey115();
            List<ActionDisplay> teamDisplay = GetTeamDisplay();
            storey.ratio = val * 0.001f;
            return teamDisplay.All<ActionDisplay>(new Func<ActionDisplay, bool>(storey.<>m__1C5));
        }

        public static bool IsAllMemberHpBelowValue(int val)
        {
            <IsAllMemberHpBelowValue>c__AnonStorey117 storey = new <IsAllMemberHpBelowValue>c__AnonStorey117 {
                val = val
            };
            return GetTeamDisplay().All<ActionDisplay>(new Func<ActionDisplay, bool>(storey.<>m__1C7));
        }

        public static bool IsAtLeastOneMemberHpBelowRatio(int val)
        {
            <IsAtLeastOneMemberHpBelowRatio>c__AnonStorey116 storey = new <IsAtLeastOneMemberHpBelowRatio>c__AnonStorey116();
            List<ActionDisplay> teamDisplay = GetTeamDisplay();
            storey.ratio = val * 0.001f;
            return teamDisplay.Any<ActionDisplay>(new Func<ActionDisplay, bool>(storey.<>m__1C6));
        }

        public static bool IsAtLeastOneMemberHpBelowValue(int val)
        {
            <IsAtLeastOneMemberHpBelowValue>c__AnonStorey118 storey = new <IsAtLeastOneMemberHpBelowValue>c__AnonStorey118 {
                val = val
            };
            return GetTeamDisplay().Any<ActionDisplay>(new Func<ActionDisplay, bool>(storey.<>m__1C8));
        }

        public bool IsSkillCdReady(int skillType)
        {
            CDTimeVo currentCdTimeVo = this.CdConroller.GetCurrentCdTimeVo(skillType);
            if (currentCdTimeVo == null)
            {
                return false;
            }
            return !currentCdTimeVo.IsInCd();
        }

        private bool IsWarnAttack(SysSkillBaseVo skillVo)
        {
            return (skillVo.warn_time > 0);
        }

        public void MonsterUseSkillById(uint skillId, bool isIgnore = false, bool isIgnoreDistance = true, ActionDisplay target = null)
        {
            if ((this.MeController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanUseSkill && (this.MeController.Me.Type == 400)) && (BaseDataMgr.instance.GetSysSkillBaseVo(skillId) != null))
            {
                ActionDisplay display;
                ActionDisplay targetDisplay = target;
                this.UseSkillById(skillId, out display, isIgnore, isIgnoreDistance, false, false, null, null, targetDisplay, -1, null);
            }
        }

        public static Func<ActionDisplay, bool> MyAliveTeammateSelector(Vector3 myPos, uint myCamp, float dis)
        {
            <MyAliveTeammateSelector>c__AnonStorey112 storey = new <MyAliveTeammateSelector>c__AnonStorey112 {
                myPos = myPos,
                myCamp = myCamp,
                dis = dis
            };
            return new Func<ActionDisplay, bool>(storey.<>m__1C1);
        }

        public void PauseHealSkill()
        {
            if ((this.MeController.Me == AppMap.Instance.me) && !Singleton<BattleBottomRightView>.Instance.IsPvpInBattle)
            {
                Singleton<BattleBottomRightView>.Instance.PauseSKill4WhenEnterBattle();
            }
        }

        private void PlayBeAttackedSound(uint skillId)
        {
            if (this._skillSoundDic.ContainsKey(skillId) && (this.skillCacheId == skillId))
            {
                SoundMgr.Instance.PlaySkillAudio(this.skillCacheSound, 0f);
            }
            else
            {
                string[] valueListFromString = StringUtils.GetValueListFromString(this.GetSoundHitPathByAttack(skillId), ',');
                if (valueListFromString.Length >= 1)
                {
                    int index = UnityEngine.Random.Range(0, valueListFromString.Length);
                    string param = valueListFromString[index];
                    if (StringUtils.IsValidConfigParam(param))
                    {
                        if (this._skillSoundDic.ContainsKey(skillId))
                        {
                            this.skillCacheId = skillId;
                            this.skillCacheSound = param;
                        }
                        else
                        {
                            this.ResetCacheSound();
                        }
                        SoundMgr.Instance.PlaySkillAudio(param, 0f);
                    }
                }
            }
        }

        private void PlaySkillSound(SysSkillBaseVo skillVo, bool result)
        {
            if (result)
            {
                if (StringUtils.IsValidConfigParam(skillVo.sound_hit))
                {
                    SoundMgr.Instance.PlaySkillAudio(this.GetRandSound(skillVo.sound_hit), 0f);
                }
            }
            else if (StringUtils.IsValidConfigParam(skillVo.sound_id))
            {
                SoundMgr.Instance.PlaySkillAudio(this.GetRandSound(skillVo.sound_id), this.GetSkillDelay((uint) skillVo.id));
                if (skillVo.id == 0x2713L)
                {
                    SoundMgr.Instance.PlaySkillAudio(this.GetRandSound(skillVo.sound_id), 0.17f);
                }
                else if (skillVo.id == 0x11173L)
                {
                    SoundMgr.Instance.PlaySkillAudio(this.GetRandSound(skillVo.sound_id), 0.27f);
                }
            }
        }

        private void RegisterSkill(SysSkillBaseVo vo)
        {
            this.MeController.AnimationEventController.RegisterSkillVo(vo);
        }

        public void RequestUseMonsterStandUpSkill()
        {
            this.UseSkill(this.LearnedSkillList.Count - 1, false, false, null, null, null);
        }

        public void RequestUseSkill(int skillType, bool ignoreDistance = false, bool ignoreStatus = false, BeforeCastSkill bc = null, AfterCastSkill ac = null, ActionDisplay targetDisplay = null)
        {
            UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " RequestUseSkill ", skillType, " targetDisplay ", targetDisplay }));
            if (!this.CheckShouldNotUseskill(skillType) && !this.CheckCDState(skillType))
            {
                targetDisplay = this.FixAttackTarget(skillType, targetDisplay);
                UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " FixAttackTarget ", skillType, " fixTarget ", targetDisplay }));
                CDTimeVo currentCdTimeVo = this.CdConroller.GetCurrentCdTimeVo(skillType);
                if ((currentCdTimeVo != null) && !currentCdTimeVo.IsInCd())
                {
                    this._nowStatus = this.MeController.StatuController.CurrentStatu;
                    if (((this._nowStatus == 0) || (this._nowStatus == 1)) || (this._nowStatus == 10))
                    {
                        this.UseSkill(skillType, ignoreDistance, ignoreStatus, bc, ac, targetDisplay);
                    }
                    else if ((((this._nowStatus == 3) && (skillType == 1)) || ((this._nowStatus == 4) && (skillType == 1))) || (((this._nowStatus == 4) && (skillType == 2)) || ((this._nowStatus == 5) && (skillType == 2))))
                    {
                        this.UseSkill(skillType, ignoreDistance, ignoreStatus, bc, ac, targetDisplay);
                    }
                    if ((((this._nowStatus == 3) || (this._nowStatus == 4)) || (this._nowStatus == 5)) && (skillType >= 4))
                    {
                        this.UseSkill(skillType, ignoreDistance, ignoreStatus, bc, ac, targetDisplay);
                    }
                }
            }
        }

        public void RequestUseSuperSkill(uint skillId)
        {
            if ((this.MeController.StatuController.CurrentStatu == 0) || (this.MeController.StatuController.CurrentStatu == 1))
            {
                this.ResetCacheSound();
                this._checkedTime = 0;
                if (skillId != 0)
                {
                    ActionDisplay display;
                    this._currentSkillId = skillId;
                    this.UseSkillById(this._currentSkillId, out display, true, true, false, false, null, null, null, -1, null);
                    this.SuperSkillList.Clear();
                    AppMap.Instance.me.Controller.ComboHitMgr.ResetAfterUseSuperSkill();
                    if (this.MeController.GetMeVo().Id == AppMap.Instance.me.GetVo().Id)
                    {
                        MeVo.instance.DataUpdate(MeVo.DataHpMpUpdate);
                        if (AppMap.Instance.mapParserII.NeedSyn)
                        {
                            Singleton<SkillMode>.Instance.SendHeroUseSkill(this.MeController.GetMeVo().Id, 2, this._currentSkillId, this.MeController.Me.GoBase.transform.eulerAngles);
                        }
                    }
                }
            }
            else
            {
                this.SuperSkillList.Add(skillId);
            }
        }

        public void ResetCacheSound()
        {
            this.skillCacheId = 0;
            this.skillCacheSound = string.Empty;
        }

        private BeforeUseSkill RunToTargetThenUseSkill(SysSkillBaseVo skillvo, ActionDisplay target, float radius, float nowDistance, int skillType)
        {
            UnityLog.Log.AI(base.gameObject, " PressSkillButton But Can't Use SKill 按下了这个技能按钮 " + skillType);
            ((MeControler) this.MeController).PlayerRunToEnemyAndUseSkill(target, skillvo.id, radius, nowDistance, skillType);
            return BUDDY_FORGET_THIS;
        }

        public void ScaleCamera()
        {
        }

        public static List<ActionDisplay> SearchFromGivenTargetList(IEnumerable<ActionDisplay> givenList, ICollection<Func<ActionDisplay, bool>> precond)
        {
            return StandardQuery(givenList, precond);
        }

        private void ShakeCarema(SysSkillBaseVo skillVo)
        {
            if ((skillVo.shake_id != 0) && (this.MeController.Me.GetVo().Id == AppMap.Instance.me.GetVo().Id))
            {
                CameraShake component = Camera.main.GetComponent<CameraShake>();
                if (component != null)
                {
                    component.play(skillVo.shake_id, 4);
                }
            }
        }

        private BeforeUseSkill ShowActionAndEffect(bool isIgnore, bool isFakeUseSkill, SysSkillBaseVo skillVo, SysSkillActionVo actionVo, BeforeUseSkill checkResult)
        {
            SysSkillActionVo vo = actionVo;
            bool flag = this.MeController.StatuController.SetStatu(actionVo.action_id);
            if (isIgnore || flag)
            {
                this._effectIndex = 0;
                if (skillVo.is_skill)
                {
                    this.AttackController.ClearAttackList();
                }
                this.RegisterSkill(skillVo);
                this.ShakeCarema(skillVo);
                if (this.MeController.ExtendedBulletFlyDistance > 0f)
                {
                    vo = VoUtils.CloneObject<SysSkillActionVo>(actionVo);
                    vo.BulletTravelDistance += Convert.ToInt32(this.MeController.ExtendedBulletFlyDistance);
                }
                this.ShowSkillEffect(vo, skillVo, checkResult.display);
                this._effectIndex++;
                this.MeController.LastUseSkillTime = Time.time;
                if (this.MeController.AiController != null)
                {
                    this.MeController.AiController.CastSkill(skillVo, Time.time);
                }
            }
            return checkResult;
        }

        public void ShowBlinkEffect()
        {
            SysSkillActionVo dataByTypeAndId = BaseDataMgr.instance.GetDataByTypeAndId<SysSkillActionVo>("SysSkillActionVo", this._currentSkillVo.skill_group);
            this.ShowSkillEffect(dataByTypeAndId, this._currentSkillVo, null);
        }

        private void ShowBulletEffect(SysSkillActionVo actionVo, SysSkillBaseVo skillVo, ActionDisplay display)
        {
            if (actionVo.IsBullet)
            {
                string[] strArray = StringUtils.SplitVoString(actionVo.process_eff, "],[");
                for (int i = 0; i < strArray.Length; i++)
                {
                    string[] valueListFromString = StringUtils.GetValueListFromString(strArray[i], ',');
                    if ((valueListFromString.Length < 1) || (valueListFromString[0] == string.Empty))
                    {
                        return;
                    }
                    float delayTime = (valueListFromString.Length >= 2) ? (int.Parse(valueListFromString[1]) * 0.001f) : 0f;
                    ActionDisplay enemyDisplay = display;
                    SysSkillBaseVo vo = skillVo;
                    com.game.module.effect.Effect effect = EffectFactory.CreateEffect(valueListFromString[0], this.MeController.gameObject, this, this.MeController.gameObject.transform.position, EffectType.Bullet, actionVo, delayTime, i, enemyDisplay, false, null, true, null, vo);
                    string valueString = StringUtils.GetValueString(actionVo.environ_eff);
                    if (valueString != string.Empty)
                    {
                        <ShowBulletEffect>c__AnonStorey10E storeye = new <ShowBulletEffect>c__AnonStorey10E();
                        vo = skillVo;
                        storeye.moveEndEffect = EffectFactory.CreateEffect(valueString, this.MeController.gameObject, null, new Vector3(0f, 0f, 0f), EffectType.Normal, null, 0f, 0, null, false, null, true, null, vo);
                        effect.PlayedCallback = new com.game.module.effect.Effect.EffectCallback(storeye.<>m__1BE);
                    }
                    EffectMgr.Instance.CreateSkillEffect(effect);
                }
            }
        }

        private void ShowMountEffects(SysSkillActionVo actionVo, SysSkillBaseVo skillVo, ActionDisplay display)
        {
            foreach (string str in StringUtils.GetValueCost(actionVo.mount_eff))
            {
                if (str != string.Empty)
                {
                    string[] valueListFromString = StringUtils.GetValueListFromString(str, ',');
                    this.ShowNormalEffect(actionVo, skillVo, display, valueListFromString);
                }
            }
        }

        private void ShowNormalEffect(SysSkillActionVo actionVo, SysSkillBaseVo skillVo, ActionDisplay display, string[] effectIds)
        {
            if ((effectIds.Length > 0) && (effectIds[0] != string.Empty))
            {
                string effectId = effectIds[0];
                bool flag = (this.MeController.Me is MonsterDisplay) && (this.MeController.Me.GetMeVoByType<MonsterVo>().MonsterVO.type == 3);
                ActionDisplay enemyDisplay = display;
                SysSkillBaseVo vo = skillVo;
                com.game.module.effect.Effect effect = EffectFactory.CreateEffect(effectId, this.MeController.gameObject, this, this.MeController.gameObject.transform.position, EffectType.Normal, actionVo, 0f, 0, enemyDisplay, false, null, true, null, vo);
                if (effectIds.Length > 1)
                {
                    effect.DelayTime = int.Parse(effectIds[1]) * 0.001f;
                }
                EffectMgr.Instance.CreateSkillEffect(effect);
            }
        }

        public void ShowSkillEffect(SysSkillActionVo actionVo, SysSkillBaseVo skillVo, ActionDisplay display = null)
        {
            this.ShowUseEffect(actionVo, skillVo, display);
            if (this._effectIndex <= 0)
            {
                if (this.IsWarnAttack(skillVo))
                {
                    SkillWarnController component = this.MeController.gameObject.GetComponent<SkillWarnController>();
                    if (null != component)
                    {
                        component.ShowSkillWarn(skillVo);
                    }
                }
                this.ShowMountEffects(actionVo, skillVo, display);
                this.ShowBulletEffect(actionVo, skillVo, display);
            }
        }

        private void ShowUseEffect(SysSkillActionVo actionVo, SysSkillBaseVo skillVo, ActionDisplay display)
        {
            string[] valueCost = StringUtils.GetValueCost(actionVo.use_eff);
            if ((valueCost.Length > this._effectIndex) && !valueCost[this._effectIndex].Equals(string.Empty))
            {
                string[] valueListFromString = StringUtils.GetValueListFromString(valueCost[this._effectIndex], ',');
                this.ShowNormalEffect(actionVo, skillVo, display, valueListFromString);
            }
        }

        [DebuggerHidden]
        private IEnumerator SlowSpeed(int delayFrameNumber)
        {
            return new <SlowSpeed>c__Iterator4F { delayFrameNumber = delayFrameNumber, <$>delayFrameNumber = delayFrameNumber };
        }

        private static List<ActionDisplay> StandardQuery(IEnumerable<ActionDisplay> list, IEnumerable<Func<ActionDisplay, bool>> precond)
        {
            <StandardQuery>c__AnonStorey10F storeyf = new <StandardQuery>c__AnonStorey10F {
                precond = precond
            };
            return list.Where<ActionDisplay>(new Func<ActionDisplay, bool>(storeyf.<>m__1BF)).ToList<ActionDisplay>();
        }

        public static List<ActionDisplay> StandardQueryForEnemy(ICollection<Func<ActionDisplay, bool>> precond)
        {
            List<ActionDisplay> list = StandardQuery(AppMap.Instance.playerList.Cast<ActionDisplay>(), precond);
            List<ActionDisplay> collection = StandardQuery(AppMap.Instance.monsterList.Cast<ActionDisplay>(), precond);
            list.AddRange(collection);
            return list;
        }

        private static List<ActionDisplay> StandardQueryForTeammate(IEnumerable<Func<ActionDisplay, bool>> precond)
        {
            return StandardQuery(AppMap.Instance.SelfplayerList.Cast<ActionDisplay>(), precond);
        }

        private void Start()
        {
            this.Init(false);
        }

        public void StopCoroutine()
        {
            base.StopCoroutine("SlowSpeed");
        }

        private void Update()
        {
            this._nowStatus = this.MeController.StatuController.CurrentStatu;
            this.CdConroller.Update();
            if (this.MeController.GetMeVo().IsEmptyHp)
            {
                this.SkillList.Clear();
            }
            if (this.SuperSkillList.Count > 0)
            {
                this.RequestUseSuperSkill(this.SuperSkillList[0]);
                this.SuperSkillList.Clear();
                this.MeController.ComboHitMgr.ResetAfterUseSuperSkill();
            }
            else if ((this.SkillList.Count > 0) && ((this._nowStatus == 0) || (this._nowStatus == 1)))
            {
                int skillType = this.SkillList[0];
                this.SkillList.RemoveAt(0);
                if (this.MeController.AttackController.AttackList.Count > 0)
                {
                    this.MeController.AttackController.AttackList.Clear();
                }
                this.RequestUseSkill(skillType, false, false, null, null, null);
            }
        }

        public void UpdateCDAfterUseSkillById(uint skillId, ActionDisplay target)
        {
            int skillType = -1;
            for (int i = 0; i < this.LearnedSkillList.Count; i++)
            {
                if (this.LearnedSkillList[i] == skillId)
                {
                    skillType = i;
                }
            }
            if (skillType >= 0)
            {
                this.HandleAfterUseSkill(skillType, target);
            }
        }

        private void UpdateChangeModelCD(SysSkillBaseVo skillVo)
        {
            float totalTime = BuffUtil.GetChangeBuffId(StringUtils.GetPDamgeDamageBuffsFromBuffListWhenUseSkill(skillVo.buff_list_when_use_skill)) * 0.001f;
            this.CdConroller.AddPrioritySkillCd(this._currentSkillType, totalTime, true, CDPriority.Second, null);
        }

        public float UpdateDirectAtkRange()
        {
            this.DirectAtkRange = this.GetAttack1AvailableAtkRange() * 0.001f;
            return this.DirectAtkRange;
        }

        private void UseSkill(int skillType, bool ignoreDistance = false, bool ignoreStatus = false, BeforeCastSkill bc = null, AfterCastSkill ac = null, ActionDisplay target = null)
        {
            this.ResetCacheSound();
            this._checkedTime = 0;
            this._currentSkillType = skillType;
            this._currentSkillId = this.GetSkillIdFromSkillType(skillType);
            if (this._currentSkillId != 0)
            {
                ActionDisplay display;
                UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " UseSkill is ", target, " skillId ", this._currentSkillId }));
                ActionDisplay targetDisplay = target;
                int num = skillType;
                if (this.UseSkillById(this._currentSkillId, out display, ignoreStatus, ignoreDistance, false, false, null, null, targetDisplay, num, null))
                {
                    this.HandleAfterUseSkill(skillType, display);
                }
            }
        }

        public void UseSkillByHand(int skillType)
        {
            UnityLog.Log.AI(base.gameObject, " UseSkillByHand " + skillType);
            GameObject g = null;
            GameObject lastEnemy = null;
            if (((this._lastTargetForAttack != null) && (this._lastTargetForAttack.GoBase != null)) && !CombatUtil.CheckTargetDead(base.gameObject, this._lastTargetForAttack.GoBase))
            {
                lastEnemy = this._lastTargetForAttack.GoBase;
            }
            CombatUtil.CheckResult falseResult = CombatUtil.FalseResult;
            if (!falseResult.ok)
            {
                falseResult = this.CheckNewEnemy(lastEnemy);
                if (falseResult.ok)
                {
                    g = falseResult.enemy;
                }
            }
            if (!falseResult.ok)
            {
                g = lastEnemy;
            }
            UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " UseSkillByHand ", g, " skillType ", skillType }));
            ActionDisplay targetDisplay = ObjectManager.GetDisplay(g);
            this.RequestUseSkill(skillType, false, false, null, null, targetDisplay);
        }

        public bool UseSkillById(uint skillId, out ActionDisplay finalTarget, bool isIgnoreStatus = false, bool isIgnoreDistance = false, bool isIgnoreLookAt = false, bool isFakeUseSkill = false, BeforeCastSkill bc = null, AfterCastSkill ac = null, ActionDisplay targetDisplay = null, int skillType = -1, CommandHandler.Command cmd = null)
        {
            this.lastCmd = cmd;
            UnityLog.Log.AI(base.gameObject, string.Concat(new object[] { " UseSkillById ", targetDisplay, " skillId ", skillId, " skillType ", skillType }));
            SysSkillBaseVo sysSkillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(skillId);
            if (sysSkillBaseVo == null)
            {
                finalTarget = null;
                return false;
            }
            this._currentSkillVo = sysSkillBaseVo;
            if ((sysSkillBaseVo.target_type == 1) || (sysSkillBaseVo.target_type == 2))
            {
                isIgnoreDistance = true;
            }
            SysSkillActionVo dataByTypeAndId = BaseDataMgr.instance.GetDataByTypeAndId<SysSkillActionVo>("SysSkillActionVo", sysSkillBaseVo.skill_group);
            if (dataByTypeAndId == null)
            {
                finalTarget = null;
                return false;
            }
            this._currentActionVo = dataByTypeAndId;
            BeforeUseSkill checkResult = this.BeforeUseSkillCheck(isIgnoreDistance, isIgnoreLookAt, targetDisplay, sysSkillBaseVo, dataByTypeAndId, skillType);
            if (!checkResult.isContinueSkill)
            {
                finalTarget = null;
                return false;
            }
            finalTarget = checkResult.display;
            UnityLog.Log.AI(base.gameObject, " finalTarget is " + finalTarget);
            this.DoUseSkillById(isIgnoreStatus, isFakeUseSkill, ref bc, ref ac, sysSkillBaseVo, dataByTypeAndId, ref checkResult, targetDisplay);
            return true;
        }

        public SysSkillBaseVo Attack1Vo
        {
            get
            {
                return BaseDataMgr.instance.GetSysSkillBaseVo(this.GetSkillIdFromSkillType(0));
            }
        }

        public SysSkillBaseVo CurrentSkillVo
        {
            get
            {
                return this._currentSkillVo;
            }
        }

        public float DirectAtkRange { get; set; }

        public bool IsForcedToAttack { get; set; }

        public bool IsInActivation { get; set; }

        public ActionDisplay LastTarget { get; set; }

        [CompilerGenerated]
        private sealed class <CheckDamage1>c__AnonStorey10B
        {
            internal ICollection<ActionDisplay> excludeList;

            internal bool <>m__1B6(ActionDisplay x)
            {
                return !this.excludeList.Contains(x);
            }
        }

        [CompilerGenerated]
        private sealed class <CommonAliveAndAttackableSelector>c__AnonStorey111
        {
            internal float dis;
            internal uint myCamp;
            internal Vector3 myPos;

            internal bool <>m__1C0(ActionDisplay x)
            {
                if ((x.GoBase == null) || (x.Controller == null))
                {
                    return false;
                }
                BaseRoleVo meVoByType = x.GetMeVoByType<BaseRoleVo>();
                float boundDistance = meVoByType.boundDistance;
                float num2 = Util.XZSqrMagnitude(this.myPos, x.GoBase.transform.position);
                return ((!meVoByType.IsEmptyHp && (meVoByType.Camp != this.myCamp)) && (num2 < ((float) (this.dis + boundDistance)).Square()));
            }
        }

        [CompilerGenerated]
        private sealed class <DealBlinkDistanceCd>c__AnonStorey10C
        {
            internal SkillController <>f__this;
            internal int skillType;

            internal void <>m__1B7(List<object> paramList)
            {
                this.<>f__this.MeController.Me.GoBase.transform.position = (Vector3) paramList[0];
                this.<>f__this._skillCdParamsDic[this.skillType].Reset();
                this.<>f__this.CdConroller.ClearPriorityCdByType(this.skillType, CDPriority.Second);
                if (paramList[1] != null)
                {
                    (paramList[1] as EffectControlerII).Clear();
                    paramList[1] = null;
                }
            }

            internal void <>m__1B8()
            {
                this.<>f__this.GetCdParams(this.skillType).canClick = true;
            }

            internal void <>m__1B9()
            {
                SkillController.SkillCDParams cdParams = this.<>f__this.GetCdParams(this.skillType);
                cdParams.isInState = false;
                if (cdParams.paramList[1] != null)
                {
                    (cdParams.paramList[1] as EffectControlerII).Clear();
                    cdParams.paramList[1] = null;
                }
                this.<>f__this._skillCdParamsDic[this.skillType].Reset();
            }

            internal void <>m__1BA(EffectControlerII controller)
            {
                object[] extendParams = new object[] { controller };
                this.<>f__this.AddCdParamBySkillType(this.skillType, extendParams);
            }
        }

        [CompilerGenerated]
        private sealed class <FilterGivenTargetList>c__AnonStorey113
        {
            internal ICollection<ActionDisplay> excludeList;

            internal bool <>m__1C2(ActionDisplay x)
            {
                return !this.excludeList.Contains(x);
            }
        }

        [CompilerGenerated]
        private sealed class <GetModelSkillList>c__AnonStorey10D
        {
            internal List<int> skillGroupIdArray;

            internal SkillInfoVo <>m__1BD(SkillInfoVo t, int i)
            {
                return new SkillInfoVo { groupId = this.skillGroupIdArray[i], skillLvl = t.skillLvl };
            }
        }

        [CompilerGenerated]
        private sealed class <GetTheNearestOne>c__AnonStorey114
        {
            internal Vector3 myPos;

            internal float <>m__1C3(ActionDisplay x)
            {
                return Util.XZSqrMagnitude(this.myPos, x.GoBase.transform.position);
            }
        }

        [CompilerGenerated]
        private sealed class <IsAllMemberHpBelowRatio>c__AnonStorey115
        {
            internal float ratio;

            internal bool <>m__1C5(ActionDisplay x)
            {
                return (x.GetMeVoByType<BaseRoleVo>().CurHpPercent <= this.ratio);
            }
        }

        [CompilerGenerated]
        private sealed class <IsAllMemberHpBelowValue>c__AnonStorey117
        {
            internal int val;

            internal bool <>m__1C7(ActionDisplay x)
            {
                return (x.GetMeVoByType<BaseRoleVo>().CurHp <= this.val);
            }
        }

        [CompilerGenerated]
        private sealed class <IsAtLeastOneMemberHpBelowRatio>c__AnonStorey116
        {
            internal float ratio;

            internal bool <>m__1C6(ActionDisplay x)
            {
                return (x.GetMeVoByType<BaseRoleVo>().CurHpPercent <= this.ratio);
            }
        }

        [CompilerGenerated]
        private sealed class <IsAtLeastOneMemberHpBelowValue>c__AnonStorey118
        {
            internal int val;

            internal bool <>m__1C8(ActionDisplay x)
            {
                return (x.GetMeVoByType<BaseRoleVo>().CurHp <= this.val);
            }
        }

        [CompilerGenerated]
        private sealed class <MyAliveTeammateSelector>c__AnonStorey112
        {
            internal float dis;
            internal uint myCamp;
            internal Vector3 myPos;

            internal bool <>m__1C1(ActionDisplay x)
            {
                if ((x.GoBase == null) || (x.Controller == null))
                {
                    return false;
                }
                BaseRoleVo meVoByType = x.GetMeVoByType<BaseRoleVo>();
                float boundDistance = meVoByType.boundDistance;
                float num2 = Util.XZSqrMagnitude(this.myPos, x.GoBase.transform.position);
                return ((!meVoByType.IsEmptyHp && (meVoByType.Camp == this.myCamp)) && (num2 < ((float) (this.dis + boundDistance)).Square()));
            }
        }

        [CompilerGenerated]
        private sealed class <ShowBulletEffect>c__AnonStorey10E
        {
            internal com.game.module.effect.Effect moveEndEffect;

            internal void <>m__1BE(GameObject go)
            {
                this.moveEndEffect.BasePosition = go.transform.position;
                EffectMgr.Instance.CreateSkillEffect(this.moveEndEffect);
            }
        }

        [CompilerGenerated]
        private sealed class <SlowSpeed>c__Iterator4F : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal int <$>delayFrameNumber;
            internal int <i>__0;
            internal int <i>__1;
            internal int delayFrameNumber;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<i>__0 = 0;
                        break;

                    case 1:
                        this.<i>__0++;
                        break;

                    case 2:
                        this.$PC = -1;
                        goto Label_00BD;

                    default:
                        goto Label_00BD;
                }
                if (this.<i>__0 < this.delayFrameNumber)
                {
                    this.$current = 0;
                    this.$PC = 1;
                }
                else
                {
                    this.<i>__1 = 0;
                    while (this.<i>__1 < 7)
                    {
                        Time.timeScale -= 0.1f;
                        this.<i>__1++;
                    }
                    this.$current = 0;
                    this.$PC = 2;
                }
                return true;
            Label_00BD:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StandardQuery>c__AnonStorey10F
        {
            internal IEnumerable<Func<ActionDisplay, bool>> precond;

            internal bool <>m__1BF(ActionDisplay display)
            {
                <StandardQuery>c__AnonStorey110 storey = new <StandardQuery>c__AnonStorey110 {
                    <>f__ref$271 = this,
                    display = display
                };
                return this.precond.All<Func<ActionDisplay, bool>>(new Func<Func<ActionDisplay, bool>, bool>(storey.<>m__1C9));
            }

            private sealed class <StandardQuery>c__AnonStorey110
            {
                internal SkillController.<StandardQuery>c__AnonStorey10F <>f__ref$271;
                internal ActionDisplay display;

                internal bool <>m__1C9(Func<ActionDisplay, bool> func)
                {
                    return func(this.display);
                }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct BeforeUseSkill
        {
            public bool isContinueSkill;
            public ActionDisplay display;
            public static SkillController.BeforeUseSkill New(bool flag, ActionDisplay ad)
            {
                return new SkillController.BeforeUseSkill { isContinueSkill = flag, display = ad };
            }

            public void Set(bool flag, ActionDisplay ad)
            {
                this.display = ad;
                this.isContinueSkill = flag;
            }
        }

        public delegate ActionDisplay CatapultCheckNextEnemy(Vector3 myPos, uint myCamp, float maxDistance, ICollection<ActionDisplay> excludeList, ActionDisplay meDisplay, bool canRepeat = true);

        public delegate void CDStateChangeAction(List<object> paramList);

        public class SkillCDParams
        {
            public bool canClick;
            public SkillController.CDStateChangeAction changeAction;
            public bool isInState;
            public List<object> paramList = new List<object>();

            public void DoChange()
            {
                if (this.changeAction != null)
                {
                    this.changeAction(this.paramList);
                }
            }

            public void Reset()
            {
                this.isInState = false;
                this.canClick = false;
                this.paramList = null;
                this.changeAction = null;
            }

            public void SetInfo(bool inState, bool canClick, List<object> para, SkillController.CDStateChangeAction action = null)
            {
                this.isInState = inState;
                this.canClick = canClick;
                this.paramList = para;
                this.changeAction = action;
            }
        }
    }
}

