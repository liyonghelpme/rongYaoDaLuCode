﻿namespace com.u3d.bases.controller
{
    using com.game.data;
    using com.game.module.fight.arpg;
    using System;
    using UnityEngine;

    public class SkillWarnController : MonoBehaviour
    {
        private GameObject _go;
        private bool _isWarning;
        private Shape _shape;
        private SysSkillBaseVo _skillVo;
        private float _startTime;
        private float _warnSec;

        private Shape CreateShape()
        {
            if (this._skillVo != null)
            {
                switch (this._skillVo.effect_shape)
                {
                    case 0:
                    {
                        float radius = this._skillVo.shape_x * 0.001f;
                        return ShapeMgr.main.GetCircle(radius, this._go, 0x20);
                    }
                    case 1:
                    {
                        float num2 = this._skillVo.shape_x * 0.001f;
                        return ShapeMgr.main.GetHalfCircle(num2, this._go, 0x20);
                    }
                    case 2:
                    {
                        float num3 = this._skillVo.shape_x * 0.001f;
                        float degree = this._skillVo.shape_y * 0.001f;
                        return ShapeMgr.main.GetSector(degree, num3, this._go, 0x20);
                    }
                    case 3:
                    {
                        float width = this._skillVo.shape_y * 0.001f;
                        float height = this._skillVo.shape_x * 0.001f;
                        Shape shape = ShapeMgr.main.GetRect(width, height, this._go);
                        shape.Move(0f, shape.transform.localPosition.y, height * 0.5f);
                        return shape;
                    }
                }
            }
            return null;
        }

        public void HideSkillWarn()
        {
            if (null != this._shape)
            {
                ShapeMgr.main.Recycle(this._shape);
                this._shape = null;
            }
            this._isWarning = false;
            this._skillVo = null;
            this._warnSec = 0f;
        }

        public void ShowSkillWarn(SysSkillBaseVo skillVo)
        {
            if (null != this._shape)
            {
                this.HideSkillWarn();
            }
            if (skillVo != null)
            {
                this._skillVo = skillVo;
                this._shape = this.CreateShape();
                if (this._shape != null)
                {
                    Vector3 localPosition = this._shape.transform.localPosition;
                    localPosition.y = 0.2f;
                    this._shape.transform.localPosition = localPosition;
                }
                this._isWarning = true;
                this._startTime = Time.time;
                this._warnSec = (this._skillVo.warn_time <= 0) ? 1f : (this._skillVo.warn_time * 0.001f);
            }
        }

        private void Start()
        {
            this._go = base.gameObject;
        }

        private void Update()
        {
            if (this._isWarning && (this._shape != null))
            {
                if (this._skillVo == null)
                {
                    this.HideSkillWarn();
                }
                else if ((Time.time - this._startTime) >= this._warnSec)
                {
                    this.HideSkillWarn();
                }
            }
        }

        public Shape shape
        {
            get
            {
                return this._shape;
            }
        }
    }
}

