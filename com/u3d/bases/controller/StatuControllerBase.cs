﻿namespace com.u3d.bases.controller
{
    using com.game.module.fight.vo;
    using com.liyong;
    using com.u3d.bases.consts;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public abstract class StatuControllerBase : MonoBehaviour
    {
        protected UnityEngine.Animator _Animator;
        private float _changeTime;
        protected int _currentCombStatu;
        private int _currentStatu;
        private int _lastStatu;
        [CompilerGenerated]
        private static Func<ActionVo, bool> <>f__am$cacheB;
        [CompilerGenerated]
        private static Func<ActionVo, bool> <>f__am$cacheC;
        public int CurStatuNameHash;
        public int MaxComb;
        public ActionControler MeControler;
        protected int PreStatuNameHash;
        protected AnimatorStateInfo StateInfo;
        private static readonly int[,] StatuChangeMatrix = new int[,] { 
            { 
                0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
                1, 1, 1, 1, 1, 1, 0, 0, 0, 0
             }, { 
                1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 
                0, 1, 1, 1, 1, 1, 0, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                0, 0, 1, 1, 1, 1, 0, 0, 0, 0
             }, { 
                1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 
                0, 1, 1, 1, 1, 1, 0, 0, 1, 0
             }, { 
                1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 
                0, 1, 1, 1, 1, 1, 0, 0, 0, 1
             }, { 
                1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 
                0, 1, 1, 1, 0, 1, 0, 0, 0, 0
             }, { 
                1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 
                0, 1, 1, 1, 0, 1, 0, 0, 0, 0
             }, { 
                1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                0, 0, 1, 1, 0, 1, 1, 0, 0, 0
             }, { 
                1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                0, 0, 1, 1, 0, 1, 1, 0, 0, 0
             }, { 
                1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                0, 0, 1, 1, 0, 1, 1, 0, 0, 0
             }, { 
                1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 
                0, 1, 1, 1, 1, 1, 0, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                0, 0, 1, 1, 0, 1, 0, 0, 0, 0
             }, { 
                1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 
                0, 1, 1, 1, 1, 1, 0, 0, 0, 0
             }, { 
                1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 
                0, 1, 1, 1, 1, 1, 0, 0, 0, 0
             }, { 
                0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 
                0, 1, 1, 1, 0, 1, 0, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 
                1, 0, 1, 1, 0, 1, 0, 0, 0, 0
             }, 
            { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 
                0, 0, 1, 1, 0, 1, 0, 0, 0, 0
             }, { 
                1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 
                0, 0, 1, 1, 0, 1, 1, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 0, 1, 0, 0
             }, { 
                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                0, 0, 0, 0, 0, 1, 0, 0, 0, 0
             }, { 
                1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 
                0, 1, 1, 1, 1, 0, 0, 0, 0, 0
             }, { 
                1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 
                0, 1, 1, 1, 1, 0, 0, 0, 0, 0
             }
         };

        protected StatuControllerBase()
        {
        }

        protected void DisposeMonsterShadowEffect()
        {
            UnityEngine.Object.Destroy(this.MeControler.Me.Shadow);
        }

        protected void DisposePlayerBlood()
        {
            Log.AI(base.gameObject, " DisposePlayerBlood here ");
        }

        protected void DisposePlayersFeetShadow()
        {
            if ((this.MeControler.Me.Shadow != null) && this.MeControler.Me.Shadow.activeSelf)
            {
                this.MeControler.Me.Shadow.SetActive(false);
            }
        }

        protected void DisposeShadowEffect()
        {
            if (this is PlayerStatuController)
            {
                if ((this.MeControler.Me.Shadow != null) && this.MeControler.Me.Shadow.activeSelf)
                {
                    this.MeControler.Me.Shadow.SetActive(false);
                }
            }
            else if (this is MonsterStatuController)
            {
                UnityEngine.Object.Destroy(this.MeControler.Me.Shadow);
            }
        }

        protected virtual void DoStatuChange()
        {
            this.MeControler.MoveController.MoveAtTheStatuBegin(this.CurrentStatu);
            if (this.MeControler.AnimationParameter != null)
            {
                this.MeControler.AnimationParameter.AnimationXSpeed = 0f;
            }
        }

        protected abstract void DoStatuTransfered();
        public UnityEngine.Animator GetAnimator()
        {
            return this.Animator;
        }

        protected virtual int[,] GetStatuMatrix()
        {
            return StatuChangeMatrix;
        }

        public static bool IsStatusHashDoingAttack(int nameHash)
        {
            return (((((nameHash == Status.NAME_HASH_ATTACK1) || (nameHash == Status.NAME_HASH_ATTACK1_0)) || ((nameHash == Status.NAME_HASH_ATTACK2) || (nameHash == Status.NAME_HASH_ATTACK2_0))) || (nameHash == Status.NAME_HASH_ATTACK3)) || (nameHash == Status.NAME_HASH_ATTACK4));
        }

        public bool IsStatusHashDoingAttackOrSkill()
        {
            return IsStatusHashDoingAttackOrSkill(this.CurStatuNameHash);
        }

        public static bool IsStatusHashDoingAttackOrSkill(int nameHash)
        {
            return (((((nameHash == Status.NAME_HASH_ATTACK1) || (nameHash == Status.NAME_HASH_ATTACK2)) || ((nameHash == Status.NAME_HASH_ATTACK3) || (nameHash == Status.NAME_HASH_ATTACK1_0))) || (((nameHash == Status.NAME_HASH_ATTACK2_0) || (nameHash == Status.NAME_HASH_ATTACK4)) || ((nameHash == Status.NAME_HASH_SKILL1) || (nameHash == Status.NAME_HASH_SKILL2)))) || (nameHash == Status.NAME_HASH_SKILL3));
        }

        public bool IsStatusHashDoingSkill()
        {
            return IsStatusHashDoingSkill(this.CurStatuNameHash);
        }

        public static bool IsStatusHashDoingSkill(int nameHash)
        {
            return ((((nameHash == Status.NAME_HASH_SKILL1) || (nameHash == Status.NAME_HASH_SKILL2)) || ((nameHash == Status.NAME_HASH_SKILL3) || (nameHash == Status.NAME_HASH_LOOP1))) || (nameHash == Status.NAME_HASH_MAGIC1));
        }

        public static bool IsStatusHashInFight(int nameHash)
        {
            return (((((((nameHash == Status.NAME_HASH_ATTACK1) || (nameHash == Status.NAME_HASH_ATTACK2)) || ((nameHash == Status.NAME_HASH_ATTACK3) || (nameHash == Status.NAME_HASH_ATTACK1_0))) || (((nameHash == Status.NAME_HASH_ATTACK2_0) || (nameHash == Status.NAME_HASH_ATTACK4)) || ((nameHash == Status.NAME_HASH_SKILL1) || (nameHash == Status.NAME_HASH_SKILL2)))) || ((((nameHash == Status.NAME_HASH_SKILL3) || (nameHash == Status.NAME_HASH_SKILL4)) || ((nameHash == Status.NAME_HASH_HURT1) || (nameHash == Status.NAME_HASH_HURT2))) || (((nameHash == Status.NAME_HASH_HURT3) || (nameHash == Status.NAME_HASH_HURT4)) || ((nameHash == Status.NAME_HASH_STANDUP) || (nameHash == Status.NAME_HASH_HURTDOWN))))) || (((nameHash == Status.NAME_HASH_LOOP1) || (nameHash == Status.NAME_HASH_MAGIC1)) || (nameHash == Status.NAME_HASH_RISE))) || (nameHash == Status.NAME_HASH_DIZZINESS));
        }

        public static bool IsStatusHashInHurtCannotMove(int nameHash)
        {
            return ((((nameHash == Status.NAME_HASH_STANDUP) || (nameHash == Status.NAME_HASH_HURTDOWN)) || (nameHash == Status.NAME_HASH_RISE)) || (nameHash == Status.NAME_HASH_DIZZINESS));
        }

        protected virtual void ProcessAttack1_0State(AnimatorStateInfo stateInfo)
        {
            if (stateInfo.normalizedTime > 0.9f)
            {
                this.SetStatu(0);
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                this.ResetCombStatu();
            }
        }

        protected virtual void ProcessAttack1State(AnimatorStateInfo stateInfo)
        {
            if (((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 3)) && (stateInfo.nameHash == Status.NAME_HASH_ATTACK1))
            {
                if ((this.MeControler.AttackController.AttackList.Count > 0) && (this.MeControler.SkillController.SkillList.Count == 0))
                {
                    ActionVo vo = this.MeControler.AttackController.AttackList[0];
                    if (vo.ActionType == "attack")
                    {
                        this.MeControler.AttackController.AttackList.RemoveAt(0);
                        this.CurrentCombStatu = 1;
                        ActionDisplay target = vo.Target;
                        this.MeControler.SkillController.RequestUseSkill(1, false, false, null, null, target);
                    }
                    else
                    {
                        this.CurrentCombStatu = 1;
                        CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                    }
                }
                else
                {
                    this.CurrentCombStatu = 1;
                    CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                }
            }
        }

        protected virtual void ProcessAttack2_0State(AnimatorStateInfo stateInfo)
        {
            if (stateInfo.normalizedTime > 0.9f)
            {
                this.SetStatu(0);
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                this.ResetCombStatu();
            }
        }

        protected void ProcessAttack2State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 4))
            {
                if ((this.MeControler.AttackController.AttackList.Count > 0) && (this.MeControler.SkillController.SkillList.Count == 0))
                {
                    ActionVo vo = this.MeControler.AttackController.AttackList[0];
                    if (vo.ActionType == "attack")
                    {
                        this.MeControler.AttackController.AttackList.RemoveAt(0);
                        this.CurrentCombStatu = 2;
                        ActionDisplay target = vo.Target;
                        this.MeControler.SkillController.RequestUseSkill(2, false, false, null, null, target);
                    }
                    else
                    {
                        CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                        this.CurrentCombStatu = 2;
                        this.SetStatu(0);
                    }
                }
                else
                {
                    CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                    this.CurrentCombStatu = 2;
                    this.SetStatu(0);
                }
            }
        }

        protected void ProcessAttack3State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.95) && (this.CurrentStatu == 5))
            {
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                this.CurrentCombStatu = 3;
                this.SetStatu(0);
                if (<>f__am$cacheB == null)
                {
                    <>f__am$cacheB = x => x.ActionType == "attack";
                }
                if (this.MeControler.AttackController.AttackList.Count<ActionVo>(<>f__am$cacheB) < 3)
                {
                    if (<>f__am$cacheC == null)
                    {
                        <>f__am$cacheC = vo => vo.ActionType == "attack";
                    }
                    IEnumerator<ActionVo> enumerator = this.MeControler.AttackController.AttackList.Where<ActionVo>(<>f__am$cacheC).ToList<ActionVo>().GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            ActionVo current = enumerator.Current;
                            this.MeControler.AttackController.AttackList.Remove(current);
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                }
            }
        }

        protected void ProcessAttack4State(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessDeathFlyState(AnimatorStateInfo stateInfo)
        {
            if (this.Animator.speed == 0f)
            {
                this.ResetAnimation();
            }
        }

        protected virtual void ProcessDeathState(AnimatorStateInfo stateInfo)
        {
            if (this.Animator.speed == 0f)
            {
                this.ResetAnimation();
            }
        }

        protected void ProcessHurt1State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 10))
            {
                this.MeControler.MoveController.IsNeedKnockBack = false;
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
        }

        protected void ProcessHurt2State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 12))
            {
                this.SetStatu(0);
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
        }

        protected void ProcessHurt3State(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessHurt4State(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessHurtDownState(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessIdleState(AnimatorStateInfo stateInfo)
        {
            if (((Time.time - this._changeTime) > 1f) && (this.CurrentStatu != 0))
            {
                this.SetStatu(0);
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
            if (((Time.time - this._changeTime) > 0.1f) && (this.CurrentCombStatu != 0))
            {
                this.ResetCombStatu();
            }
        }

        protected virtual void ProcessLoop1State(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessMagic1State(AnimatorStateInfo stateInfo)
        {
        }

        protected virtual void ProcessRiseState(AnimatorStateInfo stateInfo)
        {
        }

        protected void ProcessRollState(AnimatorStateInfo stateInfo)
        {
            if (this.Animator.IsInTransition(0) && (this.CurrentStatu == 2))
            {
                this.SetStatu(0);
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
        }

        protected virtual void ProcessRunState(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.95f) && (this.CurrentStatu != 1))
            {
            }
        }

        protected virtual void ProcessSkill1State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 7))
            {
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                this.SetStatu(0);
            }
        }

        protected void ProcessSkill2State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 8))
            {
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                this.SetStatu(0);
            }
        }

        protected void ProcessSkill3State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 9))
            {
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                this.SetStatu(0);
            }
        }

        protected void ProcessSkill4State(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 0x11))
            {
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
                this.SetStatu(0);
            }
        }

        protected void ProcessStandUpState(AnimatorStateInfo stateInfo)
        {
        }

        protected void ProcessWin(AnimatorStateInfo stateInfo)
        {
            if ((stateInfo.normalizedTime > 0.9) && (this.CurrentStatu == 0x12))
            {
                this.SetStatu(0);
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
        }

        protected void RecoverPlayersFeetShadow()
        {
            if ((this.MeControler.Me.Shadow != null) && !this.MeControler.Me.Shadow.activeSelf)
            {
                this.MeControler.Me.Shadow.SetActive(true);
            }
        }

        public void ResetAnimation()
        {
            if (((this.CurStatuNameHash != Status.NAME_HASH_DEATH) && (this.CurStatuNameHash != Status.NAME_HASH_DEATH_END)) && (this.CurStatuNameHash != Status.NAME_HASH_DEATHFLY))
            {
                this.Animator.speed = 1f;
            }
        }

        public void ResetCombStatu()
        {
            this._currentCombStatu = 0;
        }

        public void SetDeathFlyImmediately()
        {
            this.Animator.Play(Status.NAME_HASH_DEATHFLY);
            this._currentStatu = 0x13;
        }

        public void SetDeathImmediately()
        {
            this.Animator.Play(Status.NAME_HASH_DEATH);
            this._currentStatu = 11;
        }

        public void SetHurt1Immediately()
        {
            Log.AI(base.gameObject, " Force into hurt1 state");
            this.Animator.Play(Status.NAME_HASH_HURT1);
            this._currentStatu = 10;
        }

        public void SetHurtDownImmediately()
        {
            this.Animator.Play(Status.NAME_HASH_HURTDOWN);
            this._currentStatu = 15;
        }

        public void SetIdleImmediately()
        {
            Log.AI(base.gameObject, " SetIdleImmediately ");
            if (this.Animator != null)
            {
                this.Animator.Play(Status.NAME_HASH_IDLE);
                this._currentStatu = 0;
            }
        }

        public void SetRunImmediately()
        {
            this.Animator.Play(Status.NAME_HASH_RUN);
            this._currentStatu = 1;
        }

        public virtual bool SetStatu(int nextStatu)
        {
            if (((this.GetStatuMatrix()[this.CurrentStatu, nextStatu] == 0) || (this.CurrentStatu == nextStatu)) || (this.Animator == null))
            {
                return false;
            }
            if (this.MeControler.GetMeVo().NeedKeep && (nextStatu != 0))
            {
                return false;
            }
            if (nextStatu == 0)
            {
                this.MeControler.GetMeVo().NeedKeep = false;
            }
            this._changeTime = Time.time;
            this._lastStatu = this._currentStatu;
            this._currentStatu = nextStatu;
            if (((nextStatu < 3) || (nextStatu > 6)) || (nextStatu != 0))
            {
                this.ResetCombStatu();
            }
            this.Animator.SetInteger("State", this._currentStatu);
            this.DoStatuChange();
            return true;
        }

        public void SetStatusWhenChangeModel(int nextStatus)
        {
            this.Animator.SetInteger("State", nextStatus);
            this._lastStatu = this._currentStatu;
            this._currentStatu = nextStatus;
        }

        private void ShowStatusName()
        {
        }

        public void SlowAnimation(float point)
        {
            this.Animator.speed = point;
        }

        private void Start()
        {
            this.PreStatuNameHash = Status.NAME_HASH_IDLE;
            this.SetStatu(0);
        }

        public void StopAnimation()
        {
            if (((this.CurStatuNameHash != Status.NAME_HASH_DEATH) && (this.CurStatuNameHash != Status.NAME_HASH_DEATH_END)) && (this.CurStatuNameHash != Status.NAME_HASH_DEATHFLY))
            {
                this.Animator.speed = 0f;
            }
        }

        private void Update()
        {
            this.Animator = this.MeControler.Me.Animator;
            if (this.Animator == null)
            {
                this.Animator = this.MeControler.Me.Animator;
                if (!this.Animator.enabled)
                {
                    this.Animator.enabled = true;
                }
                this.Animator.applyRootMotion = false;
            }
            else if (this.Animator.speed > 0f)
            {
                this.StateInfo = this.Animator.GetCurrentAnimatorStateInfo(0);
                this.CurStatuNameHash = this.StateInfo.nameHash;
                if (this.CurStatuNameHash == Status.NAME_HASH_ATTACK1)
                {
                    this.ProcessAttack1State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_ATTACK1_0)
                {
                    this.ProcessAttack1_0State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_ATTACK2)
                {
                    this.ProcessAttack2State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_ATTACK2_0)
                {
                    this.ProcessAttack2_0State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_ATTACK3)
                {
                    this.ProcessAttack3State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_ATTACK4)
                {
                    this.ProcessAttack4State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_RUN)
                {
                    this.ProcessRunState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_ROLL)
                {
                    this.ProcessRollState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_IDLE)
                {
                    this.ProcessIdleState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_SKILL1)
                {
                    this.ProcessSkill1State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_SKILL2)
                {
                    this.ProcessSkill2State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_SKILL3)
                {
                    this.ProcessSkill3State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_SKILL4)
                {
                    this.ProcessSkill4State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_HURT1)
                {
                    this.ProcessHurt1State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_HURT2)
                {
                    this.ProcessHurt2State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_HURT3)
                {
                    this.ProcessHurt3State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_HURT4)
                {
                    this.ProcessHurt4State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_HURTDOWN)
                {
                    this.ProcessHurtDownState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_STANDUP)
                {
                    this.ProcessStandUpState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_WIN)
                {
                    this.ProcessWin(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_DEATH)
                {
                    this.ProcessDeathState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_DEATHFLY)
                {
                    this.ProcessDeathFlyState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_RISE)
                {
                    this.ProcessRiseState(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_LOOP1)
                {
                    this.ProcessLoop1State(this.StateInfo);
                }
                else if (this.CurStatuNameHash == Status.NAME_HASH_MAGIC1)
                {
                    this.ProcessMagic1State(this.StateInfo);
                }
                if (this.PreStatuNameHash != this.CurStatuNameHash)
                {
                    this.DoStatuTransfered();
                }
                this.PreStatuNameHash = this.CurStatuNameHash;
            }
        }

        public UnityEngine.Animator Animator
        {
            get
            {
                if (this._Animator == null)
                {
                }
                return (this._Animator = this.MeControler.Me.Animator);
            }
            set
            {
                this._Animator = value;
            }
        }

        public virtual int CurrentCombStatu
        {
            get
            {
                return this._currentCombStatu;
            }
            set
            {
                this._currentCombStatu = value;
            }
        }

        public int CurrentStatu
        {
            get
            {
                return this._currentStatu;
            }
        }

        public bool IsCurStatusHashInFight
        {
            get
            {
                return ((((((this.CurStatuNameHash == Status.NAME_HASH_ATTACK1) || (this.CurStatuNameHash == Status.NAME_HASH_ATTACK2)) || ((this.CurStatuNameHash == Status.NAME_HASH_ATTACK3) || (this.CurStatuNameHash == Status.NAME_HASH_ATTACK4))) || (((this.CurStatuNameHash == Status.NAME_HASH_SKILL1) || (this.CurStatuNameHash == Status.NAME_HASH_SKILL2)) || ((this.CurStatuNameHash == Status.NAME_HASH_SKILL3) || (this.CurStatuNameHash == Status.NAME_HASH_HURT1)))) || ((((this.CurStatuNameHash == Status.NAME_HASH_HURT2) || (this.CurStatuNameHash == Status.NAME_HASH_HURT3)) || ((this.CurStatuNameHash == Status.NAME_HASH_HURT4) || (this.CurStatuNameHash == Status.NAME_HASH_STANDUP))) || (((this.CurStatuNameHash == Status.NAME_HASH_HURTDOWN) || (this.CurStatuNameHash == Status.NAME_HASH_LOOP1)) || ((this.CurStatuNameHash == Status.NAME_HASH_MAGIC1) || (this.CurStatuNameHash == Status.NAME_HASH_RISE))))) || (this.CurStatuNameHash == Status.NAME_HASH_DIZZINESS));
            }
        }

        public bool IsDead
        {
            get
            {
                return (((this.CurStatuNameHash == Status.NAME_HASH_DEATH) || (this.CurStatuNameHash == Status.NAME_HASH_DEATH_END)) || (this.CurStatuNameHash == Status.NAME_HASH_DEATHFLY));
            }
        }

        public bool IsGonnaDie
        {
            get
            {
                return ((this.CurrentStatu == 11) || (this.CurrentStatu == 0x13));
            }
        }

        public bool IsUsingHealSkillInPvp
        {
            get
            {
                return (this.CurrentStatu == 0x11);
            }
        }

        public int LastStatu
        {
            get
            {
                return this._lastStatu;
            }
            set
            {
                this._lastStatu = value;
            }
        }
    }
}

