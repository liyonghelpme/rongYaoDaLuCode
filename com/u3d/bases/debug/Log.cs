﻿namespace com.u3d.bases.debug
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Text;
    using UnityEngine;

    public class Log
    {
        private const int ALL = 5;
        private const int CLOSE = 6;
        private const int DEBUG = 1;
        private const int ERROR = 4;
        private const int INFO = 2;
        private static IList<int> levelList = new List<int>();
        private const int WARIN = 3;

        public static void addLevel(int level)
        {
            if (level > 0)
            {
                levelList.Add(level);
            }
        }

        public static void addLevel(string format)
        {
            if ((format != null) && !format.Equals(string.Empty))
            {
                char[] separator = new char[] { ',' };
                string[] strArray = format.Split(separator);
                if ((strArray != null) && (strArray.Length >= 1))
                {
                    int item = 0;
                    foreach (string str in strArray)
                    {
                        item = int.Parse(str);
                        if (levelList.IndexOf(item) == -1)
                        {
                            levelList.Add(item);
                        }
                    }
                }
            }
        }

        public static void ClearLog()
        {
            levelList.Clear();
        }

        internal static void debug(uint p)
        {
            throw new NotImplementedException();
        }

        public static void debug(object obj, string log)
        {
            debug(obj.GetType().FullName, log);
        }

        public static void debug(string name, string log)
        {
            if ((levelList.IndexOf(1) != -1) || (levelList.IndexOf(5) != -1))
            {
                format(name, "DEBUG", log);
            }
        }

        public static void error(object obj, string log)
        {
            error(obj.GetType().FullName, log);
        }

        public static void error(string name, string log)
        {
            if ((levelList.IndexOf(4) != -1) || (levelList.IndexOf(5) != -1))
            {
                format(name, "ERROR", log);
            }
        }

        private static void format(string name, string level, string log)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("[" + level + "]");
            builder.Append(string.Concat(new object[] { "[", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ":", DateTime.Now.Millisecond, "] " }));
            builder.Append("[" + name + "]");
            builder.Append(" " + log);
            if (printWay)
            {
                Console.WriteLine(builder);
            }
            else if (level.Equals("ERROR"))
            {
                Debug.LogError(builder);
            }
            else if (level.Equals("WARIN"))
            {
                Debug.LogWarning(builder);
            }
            else
            {
                Debug.Log(builder);
            }
        }

        public static void info(object obj, string log)
        {
            info(obj.GetType().FullName, log);
        }

        public static void info(string name, string log)
        {
            if ((levelList.IndexOf(2) != -1) || (levelList.IndexOf(5) != -1))
            {
                format(name, "INFO", log);
            }
        }

        public static void warin(object obj, string log)
        {
            warin(obj.GetType().FullName, log);
        }

        public static void warin(string name, string log)
        {
            if ((levelList.IndexOf(3) != -1) || (levelList.IndexOf(5) != -1))
            {
                format(name, "WARIN", log);
            }
        }

        public static bool printWay
        {
            [CompilerGenerated]
            get
            {
                return <printWay>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <printWay>k__BackingField = value;
            }
        }
    }
}

