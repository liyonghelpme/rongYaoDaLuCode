﻿namespace com.u3d.bases.display
{
    using com.liyong;
    using com.u3d.bases.controller;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    [Serializable]
    public abstract class ActionDisplay : BaseDisplay
    {
        public UnityEngine.BoxCollider2D BoxCollider2D;

        protected ActionDisplay()
        {
        }

        protected void AddAStarPath(GameObject go)
        {
            base.Controller.monsterMoveAiPath = go.AddComponent<MonsterMoveAI>();
            go.AddMissingComponent<FunnelModifier>();
            Seeker component = base.Controller.GetComponent<Seeker>();
        }

        protected virtual void AddHudText()
        {
            GameObject original = Resources.Load<GameObject>("MyHudText");
            if (original != null)
            {
                GameObject obj3 = UnityEngine.Object.Instantiate(original) as GameObject;
                obj3.name = "MyHudText";
                obj3.transform.parent = base.GoBase.transform;
                Util.InitGameObject(obj3);
            }
            GameObject obj4 = Resources.Load<GameObject>("CriticalText");
            if (obj4 != null)
            {
                GameObject obj5 = UnityEngine.Object.Instantiate(obj4) as GameObject;
                obj5.name = "CriticalText";
                obj5.transform.parent = base.GoBase.transform;
                Util.InitGameObject(obj5);
            }
            GameObject g = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("HealtHudText")) as GameObject;
            g.name = "HealtHudText";
            g.transform.parent = base.GoBase.transform;
            Util.InitGameObject(g);
        }

        protected override void AddScript(GameObject go)
        {
            if (go.GetComponent<ActionControler>() == null)
            {
                base.Controller = go.AddComponent<ActionControler>();
                base.Controller.Me = this;
                StatuControllerBase base2 = go.AddComponent<StatuControllerBase>();
                base.Controller.StatuController = base2;
                base.Controller.SkillController = go.AddComponent<SkillController>();
                base.Controller.SkillController.MeController = (ActionControler) base.Controller;
                AttackControllerBase base3 = go.AddComponent<AttackControllerBase>();
                base3.MeController = (ActionControler) base.Controller;
                base.Controller.AttackController = base3;
                BeAttackedControllerBase base4 = go.AddComponent<BeAttackedControllerBase>();
                base4.meController = (ActionControler) base.Controller;
                base.Controller.BeAttackedController = base4;
                base.Controller.AnimationEventController = base.GoCloth.AddMissingComponent<AnimationEventController>();
                base.Controller.AnimationEventController.skillController = base.Controller.SkillController;
            }
        }

        public override void Pos(float x, float y, float z)
        {
            if (base.GoBase != null)
            {
                Vector3 vector;
                vector = new Vector3(x, y, z) {
                    y = Util.GetMapHeight(vector) + 0.05f
                };
                base.GoBase.transform.position = vector;
                base.GoBase.transform.LookAt(vector);
            }
        }

        public virtual void StartAi()
        {
            if (base.Controller.AiController != null)
            {
                base.Controller.AiController.SetAi(true);
            }
        }

        public virtual void StopAi()
        {
            if (base.Controller.AiController != null)
            {
                base.Controller.AiController.SetAi(false);
            }
        }
    }
}

