﻿namespace com.u3d.bases.display
{
    using com.game;
    using com.game.consts;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.effect;
    using com.game.vo;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.display.vo;
    using com.u3d.bases.interfaces;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class BaseDisplay : IPoolable
    {
        private float _boundDistance;
        private Color _endColor;
        private int _keepFrame;
        private float _modelAlpha;
        private SpriteRenderer[] _spriteRenderers;
        private Color _startColor;
        private float _startTime;
        private float _timeLength;
        private Color _warnEndColor;
        private Color _warnStartColor;
        private float _warnStartTime;
        public UnityEngine.Animator Animator;
        public Dictionary<int, string> cachedModelPathList = new Dictionary<int, string>();
        public CharacterController characterController;
        public Dictionary<uint, CharacterControllerInfo> characterControllerList = new Dictionary<uint, CharacterControllerInfo>();
        private uint currentActiveModeId;
        public GameObject DefenceEffect;
        public bool IsUsing;
        public Dictionary<uint, GameObject> majorCityModeList = new Dictionary<uint, GameObject>();
        private int modeCount;
        public Dictionary<uint, GameObject> ModeList = new Dictionary<uint, GameObject>();
        protected DisplayVo Vo;

        public BaseDisplay()
        {
            this.GoBase = new GameObject();
        }

        protected virtual void AddAI(GameObject go)
        {
        }

        protected virtual void AddAnimationControllerScript(GameObject go)
        {
        }

        private void AddBossFootEffect()
        {
            if ((this.Type == 400) || (this.Type == 200))
            {
                GameObject child = UnityEngine.Object.Instantiate(CommonModelAssetManager.Instance.GetFootShadowGameObject()) as GameObject;
                Vector3 one = Vector3.one;
                Vector3 localScale = this.GoCloth.transform.localScale;
                BoxCollider2D colliderd = this.GetMeByType<ActionDisplay>().BoxCollider2D;
                if ((colliderd != null) && (child != null))
                {
                    one.x = (colliderd.size.x * localScale.x) * 1.5f;
                    one.y = (colliderd.size.y * localScale.y) * 1.5f;
                    child.transform.localScale = one;
                }
                this.Relation(child, this.GoCloth.transform);
            }
        }

        protected void AddCharacterController(GameObject mode, uint generalId = 0)
        {
            this.characterController = this.GoBase.AddMissingComponent<CharacterController>();
            CharacterController component = mode.transform.GetComponent<CharacterController>();
            this.characterController.center = (Vector3) (component.center * mode.transform.localScale.y);
            this.characterController.radius = component.radius * mode.transform.localScale.y;
            this.characterController.height = component.height * mode.transform.localScale.y;
            UnityEngine.Object.Destroy(component);
        }

        private void AddFootShadow()
        {
            if (((this.Type == 100) || (this.Type == 400)) || (this.Type == 200))
            {
                GameObject child = UnityEngine.Object.Instantiate(CommonModelAssetManager.Instance.GetFootShadowGameObject()) as GameObject;
                Vector3 one = Vector3.one;
                Vector3 localScale = this.GoCloth.transform.localScale;
                BoxCollider2D colliderd = this.GetMeByType<ActionDisplay>().BoxCollider2D;
                if (((colliderd != null) && (child != null)) && (this.Type != 100))
                {
                    one.x = (colliderd.size.x * localScale.x) * 1.2f;
                    one.y = (colliderd.size.y * localScale.y) * 1.2f;
                    child.transform.localScale = one;
                }
                this.Relation(child, this.GoBase.transform);
            }
        }

        protected virtual void AddScript(GameObject go)
        {
            if (go.GetComponent<BaseControler>() == null)
            {
                this.Controller = go.AddComponent<BaseControler>();
                this.Controller.Me = this;
            }
        }

        public virtual void ChangeDire(float rotateY)
        {
            if (rotateY != 0f)
            {
                Vector3 eulerAngles = this.GoBase.transform.eulerAngles;
                eulerAngles.y = rotateY;
                this.GoBase.transform.eulerAngles = eulerAngles;
            }
        }

        public void ChangeStyle(uint modeId)
        {
            this.currentActiveModeId = modeId;
            if (!this.ModeList.ContainsKey(modeId))
            {
                object[] objArray1 = new object[] { "Model/Role/", modeId, "/Model/", modeId, ".assetbundle" };
                string fileName = string.Concat(objArray1);
                AssetManager.Instance.LoadAsset<GameObject>(fileName, new LoadAssetFinish<GameObject>(this.ChangeStyleLoadCallback), null, true, true);
            }
            else
            {
                this.SetModeActive();
            }
        }

        private void ChangeStyleLoadCallback(GameObject gameObject)
        {
            GameObject obj2 = (GameObject) UnityEngine.Object.Instantiate(gameObject);
            if ((obj2 != null) && (this.GoBase != null))
            {
                if (!this.ModeList.ContainsKey(this.GetModeId(obj2.name)))
                {
                    this.ModeList.Add(this.GetModeId(obj2.name), obj2);
                }
                this.GoCloth = obj2;
                if (this.GoBase.GetComponent<CharacterController>() != null)
                {
                    UnityEngine.Object.Destroy(obj2.GetComponent<CharacterController>());
                }
                this.Relation(obj2, this.GoBase.transform);
                this.SetModeActive();
            }
        }

        protected Vector3 Clone(Vector3 v3)
        {
            return new Vector3 { x = v3.x, y = v3.y, z = v3.z };
        }

        protected virtual void CreateMode(string url)
        {
            Log.AI(url, " CreateMode ");
            if (((this.Type == 100) || (this.Type == 0x65)) || (this.Type == 800))
            {
                AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.LoadModelCallBack), null, true, true);
            }
            else
            {
                AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.LoadModelCallBack), null, false, true);
            }
        }

        private void CreateShadowEffect(DisplayVo vo = null)
        {
            if (EffectMgr.Instance.ShadowEffect != null)
            {
                this.Shadow = UnityEngine.Object.Instantiate(EffectMgr.Instance.ShadowEffect) as GameObject;
                this.Shadow.transform.parent = this.GoBase.transform;
                this.Shadow.transform.position = new Vector3(0f, 0.06f, 0f);
                this.Shadow.transform.name = "shadow";
                this.ShadowZooming(vo);
                this.HandleShadowColor(vo);
            }
        }

        public virtual void Dispose()
        {
            Log.AI(this.GoCloth, " Dispose Buff Cloth ");
            this.IsUsing = false;
            this.Dispose(this.GoCloth);
            this.GoCloth = null;
            this.Animator = null;
            this._spriteRenderers = null;
            if (this.Controller != null)
            {
                this.Controller.Dispose();
            }
        }

        protected void Dispose(GameObject target)
        {
            if (target != null)
            {
                target.transform.parent = null;
                target.SetActive(false);
                Log.AI(target, " Destroy Object " + target);
                UnityEngine.Object.Destroy(target, 3f);
            }
        }

        public void EndModelColorEffect()
        {
            ModelEffectManager.RemoveSpriteColorEffect(this.GoCloth);
        }

        public void getBefore()
        {
        }

        public T GetMeByType<T>() where T: BaseDisplay
        {
            return (this as T);
        }

        public T GetMeVoByType<T>() where T: DisplayVo
        {
            return (this.Vo as T);
        }

        private uint GetModeId(string name)
        {
            name = name.Replace("(Clone)", string.Empty);
            return Convert.ToUInt32(name);
        }

        public virtual DisplayVo GetVo()
        {
            return this.Vo;
        }

        public static void HandleShadowColor(BaseDisplay display)
        {
            display.HandleShadowColor(display.GetVo());
        }

        public void HandleShadowColor(DisplayVo vo)
        {
            GameObject obj2 = NGUITools.FindChild(this.Shadow, "yingziyuanquan2");
            GameObject obj3 = NGUITools.FindChild(this.Shadow, "yingziyuanquan");
            if (((obj2 != null) && (obj3 != null)) && (AppMap.Instance.me != null))
            {
                if (vo == AppMap.Instance.me.GetVo())
                {
                    obj2.SetActive(true);
                    obj3.SetActive(false);
                }
                else
                {
                    obj2.SetActive(false);
                    obj3.SetActive(true);
                }
            }
        }

        public static void HandleShadowColor(IEnumerable<PlayerDisplay> list)
        {
            IEnumerator<PlayerDisplay> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    HandleShadowColor(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        protected void LoadModelCallBack(GameObject gameObject)
        {
            Log.AI(gameObject, "load GameObject ");
            GameObject obj2 = (GameObject) UnityEngine.Object.Instantiate(gameObject);
            Log.AI(gameObject, string.Concat(new object[] { " LoadModel Callback ", obj2.name, " Model ", obj2, " GoBase ", this.GoBase }));
            if (this.GoBase != null)
            {
                if (((this.Type == 100) || (this.Type == 0x65)) && !this.ModeList.ContainsKey(this.GetModeId(obj2.name)))
                {
                    this.ModeList.Add(this.GetModeId(obj2.name), obj2);
                }
                if (this.Vo.isChangeModleTransform)
                {
                    obj2.transform.localScale = this.Vo.scale;
                }
                obj2.transform.localPosition = new Vector3(0f, 0f, 0f);
                obj2.transform.localRotation = Quaternion.identity;
                this.GoCloth = obj2;
                this.Relation(obj2, this.GoBase.transform);
                this.Pos(this.Vo.X, this.Vo.Y, this.Vo.Z);
                this.ChangeDire(this.Vo.rotateY);
                this.AddCharacterController(obj2, 0);
                this.AddScript(this.GoBase);
                this.AddAnimationControllerScript(this.GoCloth);
                this.AddAI(this.GoBase);
                if (this.Vo.ModelLoadCallBack != null)
                {
                    this.Vo.ModelLoadCallBack(this);
                }
                this.PostAddScript(this.GoBase);
                if (this.Type == 100)
                {
                    NGUITools.SetLayer(this.GoBase, LayerMask.NameToLayer("Player"));
                }
                else if (this.Type == 400)
                {
                    NGUITools.SetLayer(this.GoBase, LayerMask.NameToLayer("Monster"));
                }
                this.SetupDisplayName();
            }
        }

        public void Pos(float x, float y)
        {
            if (this.Controller != null)
            {
                this.Pos(x, y, 5f * y);
            }
        }

        public virtual void Pos(float x, float y, float z)
        {
            if (this.GoBase != null)
            {
                Vector3 position = this.GoBase.transform.position;
                position.x = x;
                position.y = y + 0.2f;
                position.z = z;
                this.GoBase.transform.position = position;
                this.GoBase.transform.LookAt(position);
            }
        }

        protected virtual void PostAddScript(GameObject go)
        {
        }

        protected void Relation(GameObject child, Transform parent)
        {
            if (parent != null)
            {
                Vector3 vector = this.Clone(child.transform.position);
                Vector3 vector2 = this.Clone(child.transform.eulerAngles);
                Vector3 vector3 = this.Clone(child.transform.lossyScale);
                child.transform.parent = parent;
                child.transform.localPosition = vector;
                child.transform.localEulerAngles = vector2;
                child.transform.localScale = vector3;
            }
        }

        public void RemoveWhiteColor()
        {
        }

        protected virtual void ResetWhileNew()
        {
            this.Vo = null;
            this.IsUsing = true;
        }

        public virtual void SetAction(string actionType)
        {
        }

        public void SetCurrentFightGeneralModeActive(uint generalId)
        {
            this.Animator = this.ModeList[generalId].GetComponent<UnityEngine.Animator>();
            this.GoCloth = this.ModeList[generalId];
            this.GoCloth.transform.localPosition = new Vector3(0f, 0f, 0f);
            this.Controller.StatuController.Animator = this.Animator;
            foreach (KeyValuePair<uint, GameObject> pair in this.ModeList)
            {
                pair.Value.SetActive(generalId == pair.Key);
            }
        }

        private void SetModeActive()
        {
            this.SetCurrentFightGeneralModeActive(this.currentActiveModeId);
            this.AddAnimationControllerScript(this.GoCloth);
        }

        public void SetSortingOrder(bool isFirst)
        {
        }

        protected void SetStandClothGoPosition()
        {
        }

        public void SetupDisplayName()
        {
            object[] objArray1 = new object[] { base.GetType().Name, "_", this.Type, "_", this.Vo.Id };
            this.GoBase.name = string.Concat(objArray1);
            this.GoBase.transform.tag = base.GetType().Name;
        }

        internal virtual void SetVo(DisplayVo vo)
        {
            this.ResetWhileNew();
            this.Vo = vo;
            this.CreateShadowEffect(this.Vo);
            this.CreateMode(vo.ClothUrl);
        }

        private void ShadowZooming(DisplayVo vo)
        {
            int res = 0;
            MonsterVo vo2 = vo as MonsterVo;
            if (vo2 != null)
            {
                res = vo2.MonsterVO.res;
            }
            else
            {
                PlayerVo vo3 = vo as PlayerVo;
                if (vo3 != null)
                {
                    res = vo3.generalTemplateInfo.model_id;
                }
            }
            if (res != 0)
            {
                SysModelVo modelVo = BaseDataMgr.instance.GetModelVo(Convert.ToUInt32(res));
                if (modelVo != null)
                {
                    float x = modelVo.shadowZoomRate * 0.001f;
                    this.Shadow.transform.localScale = new Vector3(x, 1f, x);
                }
            }
        }

        public void ShowBornModelEffect()
        {
            this._modelAlpha = 0.1f;
            vp_Timer.In(0f, new vp_Timer.Callback(this.UpdateBornEffect), 3, 1f, null);
        }

        [DebuggerHidden]
        private IEnumerator ShowModelBeAttackedEffect()
        {
            return new <ShowModelBeAttackedEffect>c__Iterator30 { <>f__this = this };
        }

        [DebuggerHidden]
        private IEnumerator ShowModelColorEffect()
        {
            return new <ShowModelColorEffect>c__Iterator31 { <>f__this = this };
        }

        [DebuggerHidden]
        public IEnumerator ShowWarningEffect()
        {
            return new <ShowWarningEffect>c__Iterator32 { <>f__this = this };
        }

        public void ShowWhiteColor()
        {
        }

        public void StartShowModelBeAttackedEffect()
        {
            if (this._keepFrame > 0)
            {
                this._keepFrame = 0;
            }
            else
            {
                CoroutineManager.StartCoroutine(this.ShowModelBeAttackedEffect(), true);
            }
        }

        public void StartShowModelColorEffect(Color startColor, Color endColor)
        {
            this._startColor = startColor;
            this._endColor = endColor;
            this._startTime = Time.time;
            CoroutineManager.StartCoroutine(this.ShowModelColorEffect(), true);
        }

        public void StartShowWarningEffect(Color startColor, Color endColor, float timeLength)
        {
            this._timeLength = timeLength;
            this._warnStartTime = Time.time;
            this._warnStartColor = startColor;
            this._warnEndColor = endColor;
            CoroutineManager.StartCoroutine(this.ShowWarningEffect(), true);
        }

        private void UpdateBornEffect()
        {
            this._modelAlpha += 0.3f;
            ModelEffectManager.ChangeAlpha(this.GoCloth, this._modelAlpha);
        }

        public float BoundDistance
        {
            get
            {
                if (this._boundDistance != 0f)
                {
                    return this._boundDistance;
                }
                return (this._boundDistance = this.GetVo().boundDistance);
            }
        }

        public BaseControler Controller { get; set; }

        public float CurDire
        {
            get
            {
                return ((this.Controller != null) ? this.GoBase.transform.eulerAngles.y : 0f);
            }
        }

        public int CurFaceDire
        {
            get
            {
                if (this.Controller == null)
                {
                    return 0;
                }
                return ((this.Controller.transform.localScale.x >= 0f) ? 2 : 4);
            }
        }

        public Vector3 CurForward
        {
            get
            {
                return ((this.GoBase == null) ? new Vector3(0f, 0f, 0f) : this.GoBase.transform.forward);
            }
        }

        public GameObject GoBase { get; private set; }

        public GameObject GoCloth { get; set; }

        public string Key
        {
            get
            {
                return this.Vo.Key;
            }
        }

        public GameObject Shadow { get; set; }

        protected virtual string SortingLayer
        {
            get
            {
                return "Player";
            }
        }

        public int Type
        {
            get
            {
                return this.Vo.Type;
            }
        }

        [CompilerGenerated]
        private sealed class <ShowModelBeAttackedEffect>c__Iterator30 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BaseDisplay <>f__this;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep1Color);
                        this.$current = 0;
                        this.$PC = 1;
                        goto Label_01F0;

                    case 1:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep1Color);
                        this.$current = 0;
                        this.$PC = 2;
                        goto Label_01F0;

                    case 2:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep2Color);
                        this.$current = 0;
                        this.$PC = 3;
                        goto Label_01F0;

                    case 3:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep2Color);
                        this.$current = 0;
                        this.$PC = 4;
                        goto Label_01F0;

                    case 4:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep2Color);
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep2Color);
                        this.$current = 0;
                        this.$PC = 5;
                        goto Label_01F0;

                    case 5:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep2Color);
                        this.$current = 0;
                        this.$PC = 6;
                        goto Label_01F0;

                    case 6:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep1Color);
                        this.$current = 0;
                        this.$PC = 7;
                        goto Label_01F0;

                    case 7:
                        ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, ColorConst.BeAttackStep1Color);
                        this.$current = 0;
                        this.$PC = 8;
                        goto Label_01F0;

                    case 8:
                        ModelEffectManager.RemoveSpriteColorEffect(this.<>f__this.GoCloth);
                        this.$current = 0;
                        this.$PC = 9;
                        goto Label_01F0;

                    case 9:
                        this.$PC = -1;
                        break;
                }
                return false;
            Label_01F0:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowModelColorEffect>c__Iterator31 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BaseDisplay <>f__this;
            internal Color <curColor>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if ((Time.time - this.<>f__this._startTime) < 0.6f)
                        {
                            Color color = new Color {
                                a = UnityEngine.Random.Range(this.<>f__this._startColor.a, this.<>f__this._endColor.a),
                                b = UnityEngine.Random.Range(this.<>f__this._startColor.b, this.<>f__this._endColor.b),
                                r = UnityEngine.Random.Range(this.<>f__this._startColor.r, this.<>f__this._endColor.r),
                                g = UnityEngine.Random.Range(this.<>f__this._startColor.g, this.<>f__this._endColor.g)
                            };
                            this.<curColor>__0 = color;
                            if (this.<>f__this.GoCloth == null)
                            {
                                break;
                            }
                            ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, this.<curColor>__0);
                            this.$current = 0;
                            this.$PC = 1;
                            goto Label_017E;
                        }
                        break;

                    case 2:
                        this.$PC = -1;
                        goto Label_017C;

                    default:
                        goto Label_017C;
                }
                ModelEffectManager.RemoveSpriteColorEffect(this.<>f__this.GoCloth);
                this.$current = 0;
                this.$PC = 2;
                goto Label_017E;
            Label_017C:
                return false;
            Label_017E:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowWarningEffect>c__Iterator32 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BaseDisplay <>f__this;
            internal Color <curColor>__0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if ((Time.time - this.<>f__this._warnStartTime) < this.<>f__this._timeLength)
                        {
                            Color color = new Color {
                                a = UnityEngine.Random.Range(this.<>f__this._warnStartColor.a, this.<>f__this._warnEndColor.a),
                                b = UnityEngine.Random.Range(this.<>f__this._warnStartColor.b, this.<>f__this._warnEndColor.b),
                                r = UnityEngine.Random.Range(this.<>f__this._warnStartColor.r, this.<>f__this._warnEndColor.r),
                                g = UnityEngine.Random.Range(this.<>f__this._warnStartColor.g, this.<>f__this._warnEndColor.g)
                            };
                            this.<curColor>__0 = color;
                            if (this.<>f__this.GoCloth == null)
                            {
                                break;
                            }
                            ModelEffectManager.ShowSpriteColorEffect(this.<>f__this.GoCloth, this.<curColor>__0);
                            this.$current = 0;
                            this.$PC = 1;
                            goto Label_0184;
                        }
                        break;

                    case 2:
                        this.$PC = -1;
                        goto Label_0182;

                    default:
                        goto Label_0182;
                }
                ModelEffectManager.RemoveSpriteColorEffect(this.<>f__this.GoCloth);
                this.$current = 0;
                this.$PC = 2;
                goto Label_0184;
            Label_0182:
                return false;
            Label_0184:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

