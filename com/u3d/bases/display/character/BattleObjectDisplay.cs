﻿namespace com.u3d.bases.display.character
{
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.display.vo;
    using System;
    using UnityEngine;

    public class BattleObjectDisplay : BaseDisplay
    {
        protected override void AddScript(GameObject go)
        {
            BattleObjectVo meVoByType = base.GetMeVoByType<BattleObjectVo>();
            BattleObjectController controller = go.AddMissingComponent<BattleObjectController>();
            base.Controller = controller;
            base.Animator = base.GoCloth.GetComponent<Animator>();
            controller.Setup(meVoByType, this);
        }
    }
}

