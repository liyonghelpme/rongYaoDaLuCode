﻿namespace com.u3d.bases.display.character
{
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.display.vo;
    using System;
    using UnityEngine;

    public class ChestDisplay : BaseDisplay
    {
        protected override void AddScript(GameObject go)
        {
            ChestVo meVoByType = base.GetMeVoByType<ChestVo>();
            ChestController controller = go.AddComponent<ChestController>();
            base.Controller = controller;
            base.Animator = base.GoCloth.GetComponent<Animator>();
            controller.Setup(meVoByType, this);
        }
    }
}

