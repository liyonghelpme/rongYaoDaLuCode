﻿namespace com.u3d.bases.display.character
{
    using com.game.manager;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai;
    using com.u3d.bases.controller;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class MeDisplay : PlayerDisplay
    {
        protected override void AddAI(GameObject go)
        {
            AiControllerBase.AddProperAI(go, this);
        }

        protected override void AddAnimationControllerScript(GameObject go)
        {
            if ((base.GoCloth != null) && (base.Controller != null))
            {
                if (base.GoCloth.GetComponent<AnimationEventController>() != null)
                {
                    base.GoCloth.RemoveComponent<AnimationEventController>();
                }
                base.Controller.AnimationEventController = base.GoCloth.AddComponent<AnimationEventController>();
                base.Controller.AnimationEventController.skillController = base.Controller.SkillController;
                AnimationParameter component = base.GoCloth.GetComponent<AnimationParameter>();
                if (component == null)
                {
                }
                base.Controller.AnimationParameter = base.GoCloth.AddComponent<AnimationParameter>();
            }
        }

        protected override void AddHudText()
        {
            GameObject original = Resources.Load<GameObject>("MeHudText");
            if (original != null)
            {
                GameObject obj3 = UnityEngine.Object.Instantiate(original) as GameObject;
                obj3.name = "MyHudText";
                obj3.transform.parent = base.GoBase.transform;
                Util.InitGameObject(obj3);
            }
            GameObject obj4 = Resources.Load<GameObject>("CriticalText");
            if (obj4 != null)
            {
                GameObject obj5 = UnityEngine.Object.Instantiate(obj4) as GameObject;
                obj5.name = "CriticalText";
                obj5.transform.parent = base.GoBase.transform;
                Util.InitGameObject(obj5);
            }
            GameObject g = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("HealtHudText")) as GameObject;
            g.name = "HealtHudText";
            g.transform.parent = base.GoBase.transform;
            Util.InitGameObject(g);
        }

        private void AddPlayerBox(GameObject go)
        {
            GameObject g = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("PlayerBox")) as GameObject;
            g.transform.parent = go.transform;
            Util.InitGameObject(g);
        }

        protected override void AddScript(GameObject go)
        {
            if (go.GetComponent<MeControler>() == null)
            {
                base.Controller = go.AddComponent<MeControler>();
                base.Controller.Me = this;
                base.Controller.Me.Animator = base.GoCloth.GetComponent<Animator>();
                MeStatuController controller = go.AddComponent<MeStatuController>();
                controller.MeControler = (ActionControler) base.Controller;
                base.Controller.StatuController = controller;
                PlayerMoveController controller2 = go.AddComponent<PlayerMoveController>();
                controller2.AnimationEventController = base.Controller.AnimationEventController;
                base.Controller.MoveController = controller2;
                base.Controller.SkillController = go.AddComponent<SkillController>();
                base.Controller.SkillController.MeController = base.Controller as PlayerControler;
                PlayerAttackController controller3 = go.AddComponent<PlayerAttackController>();
                controller3.MeController = (ActionControler) base.Controller;
                base.Controller.AttackController = controller3;
                PlayerBeAttackedController controller4 = go.AddComponent<PlayerBeAttackedController>();
                controller4.meController = (ActionControler) base.Controller;
                base.Controller.BeAttackedController = controller4;
                go.AddComponent<PlayerDeathController>().MeController = (ActionControler) base.Controller;
                ComboHitMgr mgr = go.AddComponent<ComboHitMgr>();
                base.Controller.ComboHitMgr = mgr;
                SwitchGeneralControllerBase base2 = go.AddComponent<SwitchGeneralControllerBase>();
                base2.Me = base.Controller.Me;
                base.Controller.SwitchGeneralController = base2;
                base.Controller.buffController = go.AddComponent<BuffController>();
                base.Controller.buffController.meController = (ActionControler) base.Controller;
                base.AddAStarPath(go);
                this.AddHudText();
                this.AddPlayerBox(go);
                go.AddMissingComponent<CommandHandler>();
                go.AddMissingComponent<UpdateAttribute>();
                base.SetStandClothGoPosition();
                base.GetMeVoByType<PlayerVo>().Controller = (ActionControler) base.Controller;
                RescueController controller6 = go.AddComponent<RescueController>();
            }
        }

        public BaseRoleVo GetVo()
        {
            return (base.Vo as BaseRoleVo);
        }

        protected override void PostAddScript(GameObject go)
        {
            base.PostAddScript(go);
            if (base.GetMeVoByType<MeVo>().isImprisoned)
            {
                base.Controller.AiController.SetAi(false);
                go.GetComponent<RescueController>().SetUp(base.GetMeVoByType<PlayerVo>(), this, (MeControler) base.Controller);
            }
        }

        protected override string SortingLayer
        {
            get
            {
                return "Player";
            }
        }
    }
}

