﻿namespace com.u3d.bases.display.character
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.controller;
    using com.u3d.bases.debug;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class MonsterDisplay : ActionDisplay
    {
        private bool _hasSpeakAttackWord;
        private bool _hasSpeakDeathWord;
        private bool _hasSpeakIdleWord;
        private GameObject _speakGameObject;
        public UILabel SpeakLabel;

        protected override void AddAI(GameObject go)
        {
            AiControllerBase.AddProperAI(go, this);
        }

        protected override void AddScript(GameObject go)
        {
            if (go.GetComponent<MonsterControler>() == null)
            {
                base.Controller = go.AddComponent<MonsterControler>();
                base.Controller.Me = this;
                base.Controller.Me.Animator = base.Controller.Me.GoCloth.GetComponent<Animator>();
                MonsterStatuController controller = go.AddComponent<MonsterStatuController>();
                controller.MeControler = (ActionControler) base.Controller;
                base.Controller.StatuController = controller;
                SkillController controller2 = base.Controller.SkillController = go.AddComponent<SkillController>();
                controller2.MeController = (ActionControler) base.Controller;
                base.Controller.AnimationEventController = base.GoCloth.AddMissingComponent<AnimationEventController>();
                base.Controller.AnimationEventController.skillController = base.Controller.SkillController;
                base.Controller.AnimationParameter = base.GoCloth.AddMissingComponent<AnimationParameter>();
                MonsterMoveController controller3 = go.AddComponent<MonsterMoveController>();
                controller3.AnimationEventController = base.Controller.AnimationEventController;
                controller3.MeController = (ActionControler) base.Controller;
                base.Controller.MoveController = controller3;
                controller3.AnimationParameter = base.Controller.AnimationParameter;
                MonsterAttackController controller4 = go.AddComponent<MonsterAttackController>();
                controller4.MeController = (ActionControler) base.Controller;
                base.Controller.AttackController = controller4;
                MonsterBeAttackedController controller5 = go.AddComponent<MonsterBeAttackedController>();
                controller5.meController = (ActionControler) base.Controller;
                base.Controller.BeAttackedController = controller5;
                go.AddComponent<MonsterDeathController>().MeController = (ActionControler) base.Controller;
                base.AddAStarPath(go);
                BuffController controller7 = go.AddComponent<BuffController>();
                controller7.meController = (ActionControler) base.Controller;
                base.Controller.buffController = controller7;
                this.AddHudText();
                go.AddMissingComponent<CommandHandler>();
                go.AddMissingComponent<UpdateAttribute>();
                base.SetStandClothGoPosition();
                base.GetMeVoByType<com.game.vo.MonsterVo>().Controller = (ActionControler) base.Controller;
                this.FixColliderRadius();
            }
        }

        protected override void CreateMode(string url)
        {
            try
            {
                GameObject monsterPreload = Singleton<MonsterMgr>.Instance.GetMonsterPreload(url);
                if (monsterPreload != null)
                {
                    base.LoadModelCallBack(monsterPreload);
                }
                else
                {
                    AssetManager.Instance.LoadAsset<GameObject>(url, new LoadAssetFinish<GameObject>(this.LoadModelCallBack), null, false, true);
                }
            }
            catch (Exception exception)
            {
                Log.error(this, "-createMode() url:" + url + " [Error] " + exception.Message);
            }
        }

        private void FixColliderRadius()
        {
            float num = base.GetMeVoByType<com.game.vo.MonsterVo>().MonsterVO.collider_radius;
            num *= 0.001f;
            CharacterController characterController = base.Controller.characterController;
            if (characterController != null)
            {
                characterController.radius = num;
            }
        }

        public void InitSpeakView(GameObject speakView)
        {
            this._speakGameObject = speakView;
            this.SpeakLabel = NGUITools.FindInChild<UILabel>(speakView, "msg");
            speakView.SetActive(false);
        }

        protected override void PostAddScript(GameObject go)
        {
            base.PostAddScript(go);
            com.game.vo.MonsterVo meVoByType = base.GetMeVoByType<com.game.vo.MonsterVo>();
            SysMonsterVo monsterVO = meVoByType.MonsterVO;
            if ((monsterVO.type == 6) || (monsterVO.type == 2))
            {
                Debug.Log(string.Concat(new object[] { "怪物模板ID:", monsterVO.id, ",类型：", monsterVO.type, ",坐标:[", meVoByType.X, ",", meVoByType.Y, ",", meVoByType.Z, "]" }));
            }
            if (GlobalData.IsPlayingCamera)
            {
                this.StopAi();
            }
        }

        public void ReadyToUseAi(float delayTime)
        {
            vp_Timer.In(delayTime, new vp_Timer.Callback(this.UseAi), null);
        }

        private void RemoveWord()
        {
            if (this._speakGameObject != null)
            {
                this._speakGameObject.SetActive(false);
            }
        }

        private void SpeakAttackWords()
        {
            if (!this._hasSpeakAttackWord)
            {
                this._hasSpeakAttackWord = true;
                string str = this.MonsterVo.fight_speak;
                if (str != "0")
                {
                    this._speakGameObject.SetActive(true);
                    this.SpeakLabel.text = str;
                    vp_Timer.In(2f, new vp_Timer.Callback(this.RemoveWord), null);
                }
            }
        }

        private void SpeakDeathWords()
        {
            if (!this._hasSpeakDeathWord)
            {
                this._hasSpeakDeathWord = true;
                string str = this.MonsterVo.death_speak;
                if (str != "0")
                {
                    this._speakGameObject.SetActive(true);
                    this.SpeakLabel.text = str;
                    vp_Timer.In(2f, new vp_Timer.Callback(this.RemoveWord), null);
                }
            }
        }

        private void SpeakIdleWords()
        {
            if (!this._hasSpeakIdleWord)
            {
                string descript;
                this._hasSpeakIdleWord = true;
                if (this.MonsterPaoPaoTemplate != null)
                {
                    descript = this.MonsterPaoPaoTemplate.descript;
                }
                else
                {
                    descript = "0";
                }
                if (descript != "0")
                {
                    this._speakGameObject.SetActive(true);
                    this.SpeakLabel.text = descript;
                    vp_Timer.In(2f, new vp_Timer.Callback(this.RemoveWord), null);
                }
            }
        }

        public bool SpeakWord(string desId)
        {
            string str;
            if (this.MonsterPaoPaoTemplate == null)
            {
                return false;
            }
            char[] separator = new char[] { '#' };
            string[] strArray = DescriptManager.GetText(uint.Parse(desId)).Split(separator);
            int index = UnityEngine.Random.Range(0, strArray.Length);
            if (this.MonsterPaoPaoTemplate != null)
            {
                str = strArray[index];
            }
            else
            {
                str = "0";
            }
            if ((str == "0") || (this._speakGameObject == null))
            {
                return false;
            }
            this._speakGameObject.SetActive(true);
            this.SpeakLabel.text = str;
            UILabel label = NGUITools.FindInChild<UILabel>(this._speakGameObject, "msg");
            UISprite sprite = NGUITools.FindInChild<UISprite>(this._speakGameObject, "msgBg");
            sprite.width = ((int) label.printedSize.x) + 20;
            sprite.height = ((int) label.printedSize.y) + 40;
            vp_Timer.In(2f, new vp_Timer.Callback(this.RemoveWord), null);
            return true;
        }

        public void SpeakWordWithMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                this._speakGameObject.SetActive(false);
            }
            else
            {
                this._speakGameObject.SetActive(true);
                this.SpeakLabel.text = msg;
                vp_Timer.In(1f, new vp_Timer.Callback(this.RemoveWord), null);
            }
        }

        private void UseAi()
        {
            if (base.Controller.AiController != null)
            {
                switch (base.GetMeVoByType<com.game.vo.MonsterVo>().MonsterVO.type)
                {
                    case 1:
                    case 3:
                    case 4:
                        base.Controller.AiController.SetAi(true);
                        break;
                }
            }
        }

        public bool isBoss
        {
            get
            {
                com.game.vo.MonsterVo meVoByType = base.GetMeVoByType<com.game.vo.MonsterVo>();
                if (this.MonsterVo == null)
                {
                    return false;
                }
                return (meVoByType.MonsterVO.quality == 3);
            }
        }

        public com.game.vo.MonsterVo Monster
        {
            get
            {
                return ((ActionControler) base.Controller).GetMeVoByType<com.game.vo.MonsterVo>();
            }
        }

        public SysmonsterPaoPaoVo MonsterPaoPaoTemplate
        {
            get
            {
                return this.Monster.MonsterPaoPao;
            }
        }

        public SysMonsterVo MonsterVo
        {
            get
            {
                return this.Monster.MonsterVO;
            }
        }

        protected override string SortingLayer
        {
            get
            {
                return "Monster";
            }
        }
    }
}

