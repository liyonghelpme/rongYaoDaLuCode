﻿namespace com.u3d.bases.display.character
{
    using com.u3d.bases.display;
    using System;
    using UnityEngine;

    public class NpcDisplay : ActionDisplay
    {
        public GameObject GoTaskState;
        public int TaskState;

        protected override void AddScript(GameObject go)
        {
            if (go.GetComponent<ActionControler>() == null)
            {
                base.Controller = go.AddComponent<ActionControler>();
                base.Controller.Me = this;
                base.Controller.Me.Animator = base.Controller.Me.GoCloth.GetComponent<Animator>();
                base.Controller.Me.Animator.applyRootMotion = false;
                base.BoxCollider2D = base.GoCloth.GetComponent<BoxCollider2D>();
                if (base.BoxCollider2D != null)
                {
                    base.BoxCollider2D.enabled = false;
                }
            }
        }

        public override void Dispose()
        {
            this.TaskState = 0;
            this.RemoveTaskState();
            base.Dispose();
        }

        public void RemoveTaskState()
        {
            if (this.GoTaskState != null)
            {
                if (!this.GoTaskState.activeSelf)
                {
                    this.GoTaskState.SetActive(true);
                }
                base.Dispose(this.GoTaskState);
                this.GoTaskState = null;
            }
        }

        protected override string SortingLayer
        {
            get
            {
                return "Player";
            }
        }
    }
}

