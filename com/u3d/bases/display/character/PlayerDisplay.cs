﻿namespace com.u3d.bases.display.character
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.effect;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class PlayerDisplay : ActionDisplay
    {
        private GameObject _invincibleEffect;
        private uint id;

        protected override void AddAI(GameObject go)
        {
            AiControllerBase.AddProperAI(go, this);
        }

        protected override void AddAnimationControllerScript(GameObject go)
        {
            if ((base.GoCloth != null) && (base.Controller != null))
            {
                AnimationEventController component = base.GoCloth.GetComponent<AnimationEventController>();
                if (component == null)
                {
                }
                base.Controller.AnimationEventController = base.GoCloth.AddComponent<AnimationEventController>();
                base.Controller.AnimationEventController.skillController = base.Controller.SkillController;
                AnimationParameter local2 = base.GoCloth.GetComponent<AnimationParameter>();
                if (local2 == null)
                {
                }
                base.Controller.AnimationParameter = base.GoCloth.AddComponent<AnimationParameter>();
                base.Controller.AnimationEventController.skillController = base.Controller.SkillController;
            }
        }

        protected override void AddScript(GameObject go)
        {
            if (go.GetComponent<PlayerControler>() == null)
            {
                base.Controller = go.AddComponent<PlayerControler>();
                base.Controller.Me = this;
                base.Controller.Me.Animator = base.Controller.Me.GoCloth.GetComponent<Animator>();
                base.Controller.SkillController = go.AddComponent<SkillController>();
                base.Controller.SkillController.MeController = (PlayerControler) base.Controller;
                PlayerMoveController controller = go.AddComponent<PlayerMoveController>();
                controller.AnimationEventController = base.Controller.AnimationEventController;
                base.Controller.MoveController = controller;
                PlayerStatuController controller2 = go.AddComponent<PlayerStatuController>();
                controller2.MeControler = (ActionControler) base.Controller;
                base.Controller.StatuController = controller2;
                PlayerAttackController controller3 = go.AddComponent<PlayerAttackController>();
                controller3.MeController = (ActionControler) base.Controller;
                base.Controller.AttackController = controller3;
                base.Controller.AnimationEventController = base.GoCloth.AddMissingComponent<AnimationEventController>();
                base.Controller.AnimationEventController.skillController = base.Controller.SkillController;
                base.Controller.AnimationParameter = base.GoCloth.AddMissingComponent<AnimationParameter>();
                PlayerBeAttackedController controller4 = go.AddComponent<PlayerBeAttackedController>();
                controller4.meController = (ActionControler) base.Controller;
                base.Controller.BeAttackedController = controller4;
                go.AddComponent<PlayerDeathController>().MeController = (ActionControler) base.Controller;
                base.Controller.buffController = go.AddComponent<BuffController>();
                base.Controller.buffController.meController = (ActionControler) base.Controller;
                base.AddAStarPath(go);
                this.AddHudText();
                go.AddMissingComponent<CommandHandler>();
                go.AddMissingComponent<UpdateAttribute>();
                base.SetStandClothGoPosition();
                base.GetMeVoByType<PlayerVo>().Controller = (ActionControler) base.Controller;
            }
        }

        private void CreateInvincibleEffectBack(EffectControlerII invincibleEffect)
        {
            this._invincibleEffect = invincibleEffect.gameObject;
        }

        public void Death()
        {
            if (base.Controller != null)
            {
                base.Controller.StatuController.SetStatu(11);
            }
        }

        public BaseRoleVo GetVo()
        {
            return (base.Vo as BaseRoleVo);
        }

        private void LoadAllModelCallBack(BaseDisplay display)
        {
            SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo((uint) display.GetVo().stylebin.id, display.GetVo().stylebin.quality);
            display.SetCurrentFightGeneralModeActive((uint) generalVo.model_id);
        }

        protected override void PostAddScript(GameObject go)
        {
            base.PostAddScript(go);
            go.AddComponent<HeadController>().Init(this);
        }

        public void RemoveInvincibleEffect()
        {
            if (this._invincibleEffect != null)
            {
                EffectMgr.Instance.RemoveEffect(this._invincibleEffect.name);
                this._invincibleEffect = null;
            }
        }

        public void ShowInvincibleEffect()
        {
            if (this._invincibleEffect == null)
            {
                EffectMgr.Instance.CreateMainFollowEffect("20007", base.GoBase.gameObject, Vector3.zero, false, null, new com.game.module.effect.Effect.EffectCreateCallback(this.CreateInvincibleEffectBack));
            }
        }

        public void UpdateStyle(PStylebin styleBin)
        {
            this.GetVo().stylebin = styleBin;
            this.id = GlobalData.GetGeneralUniKey(styleBin.id, styleBin.quality);
            SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo((uint) styleBin.id, styleBin.quality);
            base.ChangeStyle((uint) generalVo.model_id);
        }

        protected override string SortingLayer
        {
            get
            {
                return "Player";
            }
        }
    }
}

