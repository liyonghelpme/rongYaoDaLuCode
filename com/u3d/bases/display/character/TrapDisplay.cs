﻿namespace com.u3d.bases.display.character
{
    using com.game.vo;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.controler;
    using System;
    using UnityEngine;

    public class TrapDisplay : ActionDisplay
    {
        protected override void AddScript(GameObject go)
        {
            if (go.GetComponent<ActionControler>() == null)
            {
                base.Controller = go.AddComponent<ActionControler>();
                base.Controller.Me = this;
                base.Controller.Me.Animator = base.Controller.Me.GoCloth.GetComponent<Animator>();
                StatuControllerBase base2 = go.AddComponent<StatuControllerBase>();
                base.Controller.StatuController = base2;
                base2.MeControler = (ActionControler) base.Controller;
                MoveControllerBase base3 = go.AddComponent<MoveControllerBase>();
                base3.AnimationEventController = base.Controller.AnimationEventController;
                base3.MeController = (ActionControler) base.Controller;
                base.Controller.MoveController = base3;
                base3.AnimationParameter = base.Controller.AnimationParameter;
                AttackControllerBase base4 = go.AddComponent<AttackControllerBase>();
                base4.MeController = (ActionControler) base.Controller;
                base.Controller.AttackController = base4;
                SkillController controller = base.Controller.SkillController = go.AddComponent<SkillController>();
                controller.MeController = (ActionControler) base.Controller;
                AnimationEventController component = base.GoCloth.GetComponent<AnimationEventController>();
                if (component == null)
                {
                }
                base.Controller.AnimationEventController = base.GoCloth.AddComponent<AnimationEventController>();
                base.Controller.AnimationEventController.skillController = base.Controller.SkillController;
                switch (this.GetTrapVo().SysTrapVo.Type)
                {
                    case 1:
                        base.GoCloth.AddComponent<TrapAnchorControl>().ActionControler = (ActionControler) base.Controller;
                        break;

                    case 2:
                        base.GoCloth.AddComponent<TrapCollideControl>().ActionControler = (ActionControler) base.Controller;
                        break;
                }
                base.BoxCollider2D = base.GoCloth.GetComponent<BoxCollider2D>();
            }
        }

        public TrapVo GetTrapVo()
        {
            return (base.Vo as TrapVo);
        }

        protected override string SortingLayer
        {
            get
            {
                return "Player";
            }
        }
    }
}

