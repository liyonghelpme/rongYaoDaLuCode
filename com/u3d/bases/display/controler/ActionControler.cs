﻿namespace com.u3d.bases.display.controler
{
    using com.game.data;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.display;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class ActionControler : BaseControler
    {
        private bool _isSynMove;
        private float _lastMoveSpeed;
        private Vector3 _lastPosition;
        protected MoveEndCallback _moveEndCallback;
        private float _notMoveTime;
        private float _speed;
        protected Vector3 destination;
        protected bool isAutoWalkToDestination;
        protected float lastTime;
        private Vector3 moveDir;
        public Action<bool, SkillController> SplashFunc;
        protected Vector2 Target;
        private float totalMoveDistance;

        protected override void ApplyGravity()
        {
            this.moveDir = Vector3.zero;
            this.moveDir.y -= 20f;
            base.characterController.Move((Vector3) ((this.moveDir * Time.deltaTime) * this.MoveSpeed));
            this.ResetPosition();
        }

        protected void ApplyRotation(Vector3 dir)
        {
            Quaternion rotation = base.transform.rotation;
            Quaternion to = Quaternion.LookRotation(dir);
            Vector3 eulerAngles = Quaternion.Slerp(rotation, to, Time.deltaTime * 20f).eulerAngles;
            eulerAngles.x = 0f;
            eulerAngles.z = 0f;
            rotation = Quaternion.Euler(eulerAngles);
            base.transform.rotation = rotation;
        }

        protected virtual void BaseMove(Vector3 lookAtPoint)
        {
            this._lastPosition = base.transform.position;
            Vector3 dir = Vector3.Normalize(lookAtPoint - base.transform.position);
            this.ApplyRotation(dir);
            base.characterController.SimpleMove((Vector3) (dir * this.MoveSpeed));
        }

        public void ChangeSpeedByRate(float ratio, float lastingTime = 0)
        {
            if (ratio != 1f)
            {
                this.MoveSpeed *= ratio;
            }
            else if (this is MonsterControler)
            {
                this.MoveSpeed = base.Me.GetMeVoByType<BaseRoleVo>().GetMonMoveSpeed();
            }
            else if (this is PlayerControler)
            {
                this.MoveSpeed = base.Me.GetMeVoByType<BaseRoleVo>().GetRoleMoveSpeed();
            }
            else
            {
                return;
            }
            if (lastingTime != 0f)
            {
                vp_Timer.In(lastingTime, new vp_Timer.Callback(this.RestoreSpeedToPrevious), null);
            }
        }

        public void ChangeSpeedTo(float lastingTime = 0)
        {
            if (this is MonsterControler)
            {
                this.MoveSpeed = base.Me.GetMeVoByType<BaseRoleVo>().GetMonMoveSpeed();
            }
            else if (this is PlayerControler)
            {
                this.MoveSpeed = base.Me.GetMeVoByType<BaseRoleVo>().GetRoleMoveSpeed();
            }
            if (lastingTime != 0f)
            {
                vp_Timer.In(lastingTime, new vp_Timer.Callback(this.RestoreSpeedToPrevious), null);
            }
        }

        protected virtual bool CheckMove()
        {
            if (base.Me.GetMeVoByType<BaseRoleVo>().IsEmptyHp && GlobalData.isInCopy)
            {
                return false;
            }
            if (base.StatuController == null)
            {
                return false;
            }
            if ((base.StatuController.GetAnimator() != null) && (base.StatuController.GetAnimator().speed <= 0f))
            {
                return false;
            }
            if (base.StatuController.CurrentStatu != 1)
            {
                return false;
            }
            return true;
        }

        protected bool CheckMoveDistance()
        {
            this.totalMoveDistance += DamageCheck.GetDistance(base.transform.position, this._lastPosition);
            this._notMoveTime += Time.deltaTime;
            if (this._notMoveTime > 0.9f)
            {
                if (this.totalMoveDistance < 0.3f)
                {
                    this.StopWalk();
                    this._notMoveTime = 0f;
                    this.totalMoveDistance = 0f;
                    return true;
                }
                this._notMoveTime = 0f;
                this.totalMoveDistance = 0f;
            }
            return false;
        }

        public void CheckStopBeforeDisableAI()
        {
            this.isAutoWalkToDestination = false;
            this.StopWalk();
        }

        public override void Dispose()
        {
            this._moveEndCallback = null;
            base.Dispose();
        }

        public void EnemyInAtkRange(GameObject oldEnemy)
        {
            Log.AI(base.gameObject, " monster enemyInAtkRange ");
            base.GetComponent<CommandHandler>().AddCommand("idle", true, 1f);
            base.IsAStarMove = false;
        }

        public BaseRoleVo GetMeVo()
        {
            return (base.Me.GetVo() as BaseRoleVo);
        }

        public T GetMeVoByType<T>() where T: DisplayVo
        {
            return (base.Me.GetVo() as T);
        }

        public override void MoveTo(float x, float y, MoveEndCallback callback = null)
        {
        }

        protected override void Render()
        {
        }

        private void ResetPosition()
        {
            if (base.transform.position.y < -30f)
            {
                base.transform.position = new Vector3(base.transform.position.x, 30f, base.transform.position.z);
            }
        }

        public void RestoreSpeedToPrevious()
        {
            this.MoveSpeed = this._lastMoveSpeed;
        }

        private void Start()
        {
            this.Target = new Vector3();
            this.MoveSpeed = this.GetMeVo().GetRoleMoveSpeed();
            this._lastMoveSpeed = 3f;
        }

        public void StopAndClearPath()
        {
            base.ClearPath();
        }

        public virtual void StopWalk()
        {
            Log.AI(base.gameObject, " stop Walk Action Here");
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        public void WalkTo(Vector3 destinationPoint, MoveEndCallback moveCallback = null, bool isMoveDirectly = false, bool isAStarMove = false)
        {
            <WalkTo>c__AnonStorey119 storey = new <WalkTo>c__AnonStorey119 {
                moveCallback = moveCallback,
                <>f__this = this
            };
            if (base.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanMove)
            {
                this.destination = destinationPoint;
                if (isAStarMove)
                {
                    base.IsAStarMove = true;
                    base.IsMoveDirectly = false;
                    this.isAutoWalkToDestination = false;
                    int num = base.monsterMoveAiPath.AddCallbackMap(new Util.VoidDelegate(storey.<>m__1CA));
                    object[] args = new object[] { this.destination.x, this.destination.y, this.destination.z, num };
                    CommandHandler.AddCommandStatic(base.gameObject, string.Format("TryToMove {0} {1} {2} {3}", args), 1f);
                }
                else
                {
                    base.IsAStarMove = false;
                    base.IsMoveDirectly = true;
                    CommandHandler.AddCommandStatic(base.gameObject, "move_attack -1", 1f);
                    this.isAutoWalkToDestination = true;
                }
                if ((storey.moveCallback != null) && !isAStarMove)
                {
                    this._moveEndCallback = storey.moveCallback;
                }
            }
        }

        public float ExtendedBulletFlyDistance { get; set; }

        public bool IsInSplash { get; set; }

        public bool IsSynMove
        {
            get
            {
                return this._isSynMove;
            }
            set
            {
                this._isSynMove = value;
                if (!this._isSynMove)
                {
                    this.StopWalk();
                }
            }
        }

        public ActionDisplay LastAttacker { get; set; }

        public float LastBeAttackedTime { get; set; }

        public ActionVo LastBeAttackVo { get; set; }

        public float LastUseSkillTime { get; set; }

        public float MoveSpeed
        {
            protected get
            {
                return this._speed;
            }
            set
            {
                this._lastMoveSpeed = this._speed;
                this._speed = value;
                if (base.monsterMoveAiPath != null)
                {
                    base.monsterMoveAiPath.SetSpeed(this._speed);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <WalkTo>c__AnonStorey119
        {
            internal ActionControler <>f__this;
            internal MoveEndCallback moveCallback;

            internal void <>m__1CA()
            {
                if (this.moveCallback != null)
                {
                    this.moveCallback(this.<>f__this);
                }
            }
        }
    }
}

