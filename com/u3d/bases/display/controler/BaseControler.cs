﻿namespace com.u3d.bases.display.controler
{
    using com.game.manager;
    using com.game.module.fight.arpg;
    using com.liyong;
    using com.u3d.bases.ai;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class BaseControler : MonoBehaviour
    {
        private GameObject _goHp;
        private GameObject _goName;
        private bool _needGravity = true;
        public AiControllerBase AiController;
        public com.u3d.bases.controller.AnimationEventController AnimationEventController;
        public com.u3d.bases.controller.AnimationParameter AnimationParameter;
        public AttackControllerBase AttackController;
        public BeAttackedControllerBase BeAttackedController;
        public BuffController buffController;
        public CharacterController characterController;
        public com.game.manager.ComboHitMgr ComboHitMgr;
        private int ControllerId = -1;
        public GameObject GoBossMsg;
        public GameObject GoHeadInfo;
        public GameObject GoNumber;
        public Transform MeTransform;
        public MonsterMoveAI monsterMoveAiPath;
        public MoveControllerBase MoveController;
        public com.u3d.bases.controller.SkillController SkillController;
        public StatuControllerBase StatuController;
        public SwitchGeneralControllerBase SwitchGeneralController;

        protected virtual void ApplyGravity()
        {
        }

        protected virtual void Awake()
        {
            this.ControllerId = ObjectManager.AddController(this);
            this.characterController = base.transform.gameObject.GetComponent<CharacterController>();
        }

        protected int CalDiretion(float startX, float endX)
        {
            int curFaceDire = this.Me.CurFaceDire;
            if (endX > startX)
            {
                return 2;
            }
            if (endX < startX)
            {
                curFaceDire = 4;
            }
            return curFaceDire;
        }

        public virtual bool CheckShouldApplyGravity()
        {
            return this._needGravity;
        }

        public void ClearPath()
        {
            Log.AI(base.gameObject, " ClearPath For who");
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        public virtual void Dispose()
        {
            if (this._goName != null)
            {
                UnityEngine.Object.Destroy(this._goName);
            }
            if (this.GoNumber != null)
            {
                UnityEngine.Object.Destroy(this.GoNumber);
            }
            if (base.gameObject != null)
            {
                UnityEngine.Object.Destroy(base.gameObject);
            }
            if (this.GoBossMsg != null)
            {
                UnityEngine.Object.Destroy(this.GoBossMsg);
            }
            if (this._goHp != null)
            {
                UnityEngine.Object.Destroy(this._goHp);
            }
        }

        public void DisposeHudView()
        {
            if (this._goName != null)
            {
                this._goName.SetActive(false);
            }
            if (this._goHp != null)
            {
                UpdateHpBar component = this._goHp.GetComponent<UpdateHpBar>();
                if (component != null)
                {
                    component.HideSelf();
                }
            }
        }

        protected virtual void Execute()
        {
            this.Move();
            this.Render();
            this.Logic();
        }

        public int GetControllerId()
        {
            return this.ControllerId;
        }

        public T GetMeByType<T>() where T: BaseDisplay
        {
            return (this.Me as T);
        }

        protected virtual void HandlerMonsterSpeakWords()
        {
        }

        protected virtual void Logic()
        {
        }

        protected virtual void Move()
        {
        }

        public virtual void MoveTo(float x, float y, MoveEndCallback callback = null)
        {
        }

        protected virtual void OnDestroy()
        {
            ObjectManager.RemoveController(this.ControllerId);
        }

        public void PauseGravity()
        {
            this._needGravity = false;
        }

        protected virtual void Render()
        {
            if (this.Me.GoCloth == null)
            {
            }
        }

        public void ResetGravity()
        {
            this._needGravity = true;
        }

        public bool SpeakWord(string desId)
        {
            if (this.Me.GetVo().Type == 400)
            {
                MonsterDisplay me = this.Me as MonsterDisplay;
                if (me != null)
                {
                    return me.SpeakWord(desId);
                }
            }
            return false;
        }

        private void Start()
        {
        }

        private void Update()
        {
            if (this.IsUsing || this.IsMoveDirectly)
            {
                this.Execute();
            }
            if (this.CheckShouldApplyGravity())
            {
                this.ApplyGravity();
            }
            this.HandlerMonsterSpeakWords();
        }

        public GameObject GoHp
        {
            get
            {
                return this._goHp;
            }
            set
            {
                this._goHp = value;
            }
        }

        public GameObject GoName
        {
            get
            {
                return this._goName;
            }
            set
            {
                this._goName = value;
            }
        }

        public bool IsAStarMove { get; set; }

        public bool IsMoveDirectly { get; set; }

        protected bool IsUsing
        {
            get
            {
                return ((this.Me != null) && this.Me.IsUsing);
            }
        }

        public BaseDisplay Me { get; set; }
    }
}

