﻿namespace com.u3d.bases.display.controler
{
    using com.game;
    using com.game.module.map;
    using com.liyong;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.vo;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityLog;

    public class BattleObjectController : BaseControler
    {
        private Animator _anim;
        private CharacterController _chCtrl;
        private readonly IDictionary<ulong, PlayerDisplay> _checkPlayerDic = new Dictionary<ulong, PlayerDisplay>();
        private GameObject _go;
        private bool _isPicked;
        private BattleObjectDisplay _meDisplay;
        private BattleObjectVo _meVo;
        private Transform _selfTransform;
        [CompilerGenerated]
        private static Func<KeyValuePair<ulong, PlayerDisplay>, PlayerDisplay> <>f__am$cache8;

        protected override void ApplyGravity()
        {
            Vector3 zero = Vector3.zero;
            zero.y = -100f;
            Vector3 motion = (Vector3) (Time.deltaTime * zero);
            this._chCtrl.Move(motion);
            Vector3 position = this._selfTransform.position;
            Log.AI(base.gameObject, " ApplyGravity " + motion);
            if (position.y < -50f)
            {
                position.y = 10f;
                this._selfTransform.position = position;
            }
        }

        public override bool CheckShouldApplyGravity()
        {
            return true;
        }

        private PlayerDisplay IsAnyBodyNear()
        {
            <IsAnyBodyNear>c__AnonStorey11A storeya = new <IsAnyBodyNear>c__AnonStorey11A();
            IEnumerable<PlayerDisplay>[] listContainer = new IEnumerable<PlayerDisplay>[] { AppMap.Instance.SelfplayerList };
            this.UpdatePlayerDic(listContainer);
            if (!AppMap.Instance.IsInWifiPVP)
            {
                return null;
            }
            storeya.myPos = this._selfTransform.position;
            storeya.range = 1f;
            Log.AI(base.gameObject, string.Concat(new object[] { " PlayerList ", this._checkPlayerDic.Count, " myPos ", storeya.myPos }));
            if (<>f__am$cache8 == null)
            {
                <>f__am$cache8 = kv => kv.Value;
            }
            return this._checkPlayerDic.Where<KeyValuePair<ulong, PlayerDisplay>>(new Func<KeyValuePair<ulong, PlayerDisplay>, bool>(storeya.<>m__1CB)).Select<KeyValuePair<ulong, PlayerDisplay>, PlayerDisplay>(<>f__am$cache8).FirstOrDefault<PlayerDisplay>();
        }

        public void Setup(BattleObjectVo vo, BattleObjectDisplay battleObjectDisplay)
        {
            this._meVo = vo;
            this._meDisplay = battleObjectDisplay;
            this._anim = this._meDisplay.Animator;
            this._go = base.gameObject;
            this._chCtrl = this._go.GetComponent<CharacterController>();
            this._selfTransform = base.transform;
        }

        private void Update()
        {
            this.ApplyGravity();
            if (!this._isPicked && AppMap.Instance.IsInWifiPVP)
            {
                PlayerDisplay picker = this.IsAnyBodyNear();
                Log.AI(base.gameObject, " AnyPlayer NearToPick Me " + picker);
                if (picker != null)
                {
                    BattleObjectMgr.Instance.Pick(this._meVo, picker);
                    this._isPicked = true;
                }
            }
        }

        private void UpdatePlayerDic(params IEnumerable<PlayerDisplay>[] listContainer)
        {
            foreach (IEnumerable<PlayerDisplay> enumerable in listContainer)
            {
                IEnumerator<PlayerDisplay> enumerator = enumerable.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        PlayerDisplay current = enumerator.Current;
                        ulong id = current.GetVo().Id;
                        if (!this._checkPlayerDic.ContainsKey(id))
                        {
                            this._checkPlayerDic.Add(id, current);
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <IsAnyBodyNear>c__AnonStorey11A
        {
            internal Vector3 myPos;
            internal float range;

            internal bool <>m__1CB(KeyValuePair<ulong, PlayerDisplay> kv)
            {
                return ((!kv.Value.GetVo().IsEmptyHp && (kv.Value.GoBase != null)) && (Util.XZSqrMagnitude(this.myPos, kv.Value.GoBase.transform.position) < this.range));
            }
        }
    }
}

