﻿namespace com.u3d.bases.display.controler
{
    using com.game;
    using com.game.module.core;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.vo;
    using System;
    using UnityEngine;

    internal class ChestController : BaseControler
    {
        private Animator _anim;
        private CharacterController _chCtrl;
        private ChestDisplay _dis;
        private GameObject _go;
        private ChestVo _info;
        private bool _locked;
        private Transform _trans;

        protected override void ApplyGravity()
        {
            Vector3 zero = Vector3.zero;
            zero.y = -100f;
            this._chCtrl.Move((Vector3) (Time.deltaTime * zero));
            Vector3 position = this._trans.position;
            if (position.y < -512f)
            {
                position.y = 10f;
                this._trans.position = position;
            }
        }

        private bool IsSelfNearby()
        {
            MeDisplay me = AppMap.Instance.me;
            if (((me == null) || (null == me.GoBase)) || (me.GetVo() == null))
            {
                return false;
            }
            if (me.GetVo().IsEmptyHp)
            {
                return false;
            }
            Vector3 position = me.GoBase.transform.position;
            Vector3 b = base.transform.position;
            position.y = b.y = 0f;
            return (Vector3.Distance(position, b) <= (this._info.sysVo.radius * 0.001f));
        }

        public void Setup(ChestVo info, ChestDisplay dis)
        {
            this._info = info;
            this._dis = dis;
            this._anim = this._dis.Animator;
            this._go = base.gameObject;
            this._chCtrl = this._go.GetComponent<CharacterController>();
            this._trans = base.transform;
            this._trans.rotation = Quaternion.EulerRotation(0f, this._info.sysVo.deg, 0f);
            this.locked = true;
        }

        private void Update()
        {
            if (this.IsSelfNearby())
            {
                this.locked = false;
                base.enabled = false;
                Singleton<ChestMgr>.Instance.Pick(this.info);
            }
            this.ApplyGravity();
        }

        public ChestVo info
        {
            get
            {
                return this._info;
            }
        }

        private bool locked
        {
            get
            {
                return this._locked;
            }
            set
            {
                if (this._locked != value)
                {
                    this._locked = value;
                    if (null != this._anim)
                    {
                        if (this._locked)
                        {
                            this._anim.SetInteger("State", 0);
                        }
                        else
                        {
                            this._anim.SetInteger("State", 10);
                        }
                    }
                }
            }
        }
    }
}

