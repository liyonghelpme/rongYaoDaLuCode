﻿namespace com.u3d.bases.display.controler
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class KeyCodeDicHelper
    {
        public static void Update(this Dictionary<KeyCode, bool> dic, KeyCode key, bool value)
        {
            if (dic.ContainsKey(key))
            {
                dic.Remove(key);
            }
            dic.Add(key, value);
        }
    }
}

