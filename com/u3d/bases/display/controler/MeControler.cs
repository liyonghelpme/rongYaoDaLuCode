﻿namespace com.u3d.bases.display.controler
{
    using com.game;
    using com.game.module.core;
    using com.game.module.effect;
    using com.game.module.fight.vo;
    using com.game.module.hud;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.joystick;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public class MeControler : PlayerControler
    {
        private float _lastCheckTimeWhenChasing;
        private bool _mouseDownStart;
        private int _moveStep = 1;
        private ActionDisplay _targetDisplayWhenSkillChase;
        private int _targetSkillIdAfterChase;
        private float _targetSkillRadius;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map3B;
        private Vector2 fakeTgPosition;
        private bool isKeyboardMove;
        private Dictionary<KeyCode, bool> keycodeDic = new Dictionary<KeyCode, bool>();
        private MeCommandHandler meCommandHandler;

        protected override void Awake()
        {
            base.Awake();
            base.gameObject.AddComponent<MeCommandHandler>();
        }

        protected override void BaseMove(Vector3 lookAtPoint)
        {
            base.BaseMove(lookAtPoint);
            if ((Time.time - base.lastTime) > 0.5f)
            {
                base.lastTime = Time.time;
                SendPos();
            }
        }

        private void BaseMoveForJoystick(Vector3 dir)
        {
            if (base.StatuController.CurrentStatu != 1)
            {
                base.StatuController.SetStatu(1);
            }
            base.characterController.SimpleMove((Vector3) (dir * base.MoveSpeed));
            if ((Time.time - base.lastTime) > 0.5f)
            {
                base.lastTime = Time.time;
                SendPos();
            }
        }

        private bool canSortMouseDown()
        {
            if (UICamera.lastHit.collider != null)
            {
                return false;
            }
            if (AppMap.Instance.monseClickEnable())
            {
                return false;
            }
            return (1 == AppMap.Instance.mapParserII.MapVo.type);
        }

        private void CheckHit()
        {
            if ((AppMap.Instance.checkVo != null) && AppMap.Instance.checkVo.IsHit())
            {
                this.StopWalk();
                AppMap.Instance.checkVo.Call();
            }
        }

        protected bool CheckShouldRunWithCurrentStatu()
        {
            MeVo meVoByType = base.Me.GetMeVoByType<MeVo>();
            if (meVoByType.stateInfo.IsFrozen || meVoByType.stateInfo.ShouldPauseJoystickAndAtkBtn)
            {
                Log.AI(null, " CheckRunWithStatu False ");
                return false;
            }
            if (base.GetComponent<StateMachine>() == null)
            {
                Log.AI(null, " StateMachine Not Exist ");
                return false;
            }
            AIState curState = base.GetComponent<StateMachine>().GetCurState();
            bool flag = false;
            if (base.SkillController.CurrentSkillVo != null)
            {
                flag = base.SkillController.CurrentSkillVo.can_move_step;
            }
            Log.AI(base.gameObject, string.Concat(new object[] { " JoyStick CanMoveWhenSkill ", flag, " condition3  condtion 1 2  22 ", curState.type }));
            return ((((curState.type == AIState.AIStateEnum.IDLE) || (curState.type == AIState.AIStateEnum.MOVE_ATTACK)) || ((curState.type == AIState.AIStateEnum.MOVE_KEYBOARD) || (curState.type == AIState.AIStateEnum.HURT))) || flag);
        }

        private void ClearAfterChase()
        {
            Log.AI(base.gameObject, " enemyInAtk ClearAfterChase");
            this._targetDisplayWhenSkillChase = null;
            this._targetSkillIdAfterChase = 0;
            this._targetSkillRadius = 0f;
        }

        private void ClickPoint(float x, float y)
        {
            this.Target.y = y - 0.8f;
            Vector3 position = base.transform.position;
            if (x > position.x)
            {
                this.Target.x = x - 0.5f;
            }
            if (x < position.x)
            {
                this.Target.x = x + 0.5f;
            }
        }

        public void DoRelive()
        {
            Log.AI(base.gameObject, " DoRelive 人物复活");
            base.buffController.Reborn();
            BaseRoleVo meVoByType = base.Me.GetMeVoByType<BaseRoleVo>();
            meVoByType.AddHpToPercent((uint) 100);
            HudView.Instance.AddPlayerHealthBarWhenEmpty(base.Me as ActionDisplay);
            base.Me.Controller.GoHp.GetComponent<UpdateHpBar>().SetHp(meVoByType.Hp, meVoByType.CurHp, false);
            CommandHandler.AddCommandStatic(base.gameObject, "idle relive", 1f);
        }

        protected override void Execute()
        {
            this.MouseClick();
            this.CheckHit();
            base.Execute();
        }

        private MoveType GetMoveType()
        {
            MoveType doNotMove = MoveType.DoNotMove;
            if ((base.IsUsing && (base.Me is MeDisplay)) && JoystickController.instance.joystick.onPressed)
            {
                return MoveType.MoveThroughJoyStick;
            }
            if ((base.IsUsing && (base.Me is MeDisplay)) && this.isKeyboardMove)
            {
                return MoveType.TestMoveInKeyboard;
            }
            if (base.gameObject.GetComponent<MonsterMoveAI>() != null)
            {
                return MoveType.MoveThroughAStar;
            }
            if ((!base.IsUsing && base.IsMoveDirectly) && (base.Me.Type == 100))
            {
                doNotMove = MoveType.MoveDirectly;
            }
            return doNotMove;
        }

        private void HandleKey()
        {
            this.fakeTgPosition = Vector3.zero;
            this.fakeTgPosition.x = this.meCommandHandler.inputX;
            this.fakeTgPosition.y = this.meCommandHandler.inputY;
            if (this.fakeTgPosition != Vector2.zero)
            {
                if (base.AiController != null)
                {
                    base.GetComponent<PlayerAiController>().OnlyStopAi();
                }
                if (!this.CheckShouldRunWithCurrentStatu())
                {
                    CommandHandler.AddCommandStatic(base.gameObject, "insert_command move_keyboard", 1f);
                }
                else
                {
                    CommandHandler.AddCommandStatic(base.gameObject, "move_keyboard", 1f);
                    this.isKeyboardMove = true;
                }
            }
            else if (this.isKeyboardMove)
            {
                this.isKeyboardMove = false;
                CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
            }
        }

        private void InitKeycodeDic()
        {
            List<KeyCode> list = new List<KeyCode> { 0x77, 0x73, 0x61, 100 };
            this.keycodeDic.Clear();
            foreach (KeyCode code in list)
            {
                this.keycodeDic.Add(code, false);
            }
        }

        public void JoystickIdle()
        {
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        public void JoystickMove(Vector2 moveTgPosition)
        {
            Log.AI(null, " JoystickMove to " + moveTgPosition);
            if (this.CheckShouldRunWithCurrentStatu())
            {
                Vector3 vector = new Vector3(base.transform.position.x + moveTgPosition.x, base.transform.position.y, base.transform.position.z + moveTgPosition.y);
                Vector3 dir = Vector3.Normalize(vector - base.transform.position);
                if (((dir.x != 0f) || (dir.y != 0f)) || (dir.z != 0f))
                {
                    CommandHandler.AddCommandStatic(base.gameObject, "move_keyboard", 1f);
                    base.ApplyRotation(dir);
                    this.BaseMoveForJoystick(dir);
                    base.CheckMoveDistance();
                }
            }
            else
            {
                CommandHandler.AddCommandStatic(base.gameObject, "insert_command move_keyboard", 1f);
            }
        }

        private void ListenKeyboardAttack()
        {
            if (!AppMap.Instance.IsInMainCity)
            {
                if (Input.GetKeyDown(KeyCode.Z) && (base.AiController != null))
                {
                    base.AiController.SetAi(!base.AiController.IsAiEnable);
                }
                if (!base.StatuController.IsStatusHashDoingSkill())
                {
                    if (Input.GetKeyDown(KeyCode.U))
                    {
                        if ((base.AiController != null) && base.AiController.IsAiEnable)
                        {
                            base.AiController.SetAi(false);
                        }
                        ActionVo vo = new ActionVo {
                            ActionType = "attack"
                        };
                        base.AttackController.AddAttackList(vo, false, false);
                    }
                    if (Input.GetKeyDown(KeyCode.I))
                    {
                        if (base.AiController != null)
                        {
                            base.AiController.SetAi(false);
                        }
                        base.SkillController.RequestUseSkill(4, false, false, null, null, null);
                    }
                    if (Input.GetKeyDown(KeyCode.J))
                    {
                        if (base.AiController != null)
                        {
                            base.AiController.SetAi(false);
                        }
                        base.SkillController.RequestUseSkill(5, false, false, null, null, null);
                    }
                    if (Input.GetKeyDown(KeyCode.K))
                    {
                        if (base.AiController != null)
                        {
                            base.AiController.SetAi(false);
                        }
                        base.SkillController.RequestUseSkill(6, false, false, null, null, null);
                    }
                    if (Input.GetKeyDown(KeyCode.L))
                    {
                        if (base.AiController != null)
                        {
                            base.AiController.SetAi(false);
                        }
                        base.SkillController.RequestUseSkill(7, false, false, null, null, null);
                    }
                }
            }
        }

        protected override void Logic()
        {
        }

        private void MapAiStop()
        {
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        private void MouseClick()
        {
            if (AppMap.Instance.IsInMainCity)
            {
                if (UICamera.lastHit.collider != null)
                {
                    if (Input.GetMouseButtonDown(0) && ((UICamera.lastHit.transform.gameObject.name != "InteractMenuView(Clone)") && (Singleton<InteractMenuView>.Instance != null)))
                    {
                        Singleton<InteractMenuView>.Instance.CloseView();
                    }
                }
                else if ((!AppMap.Instance.monseClickEnable() && Input.GetMouseButtonUp(0)) && !object.ReferenceEquals(Camera.main, null))
                {
                    GameObject obj2 = this.Raycast(Input.mousePosition);
                    if (obj2 != null)
                    {
                        if (Singleton<InteractMenuView>.Instance != null)
                        {
                            Singleton<InteractMenuView>.Instance.CloseView();
                        }
                        string tag = obj2.tag;
                        if (tag != null)
                        {
                            int num;
                            if (<>f__switch$map3B == null)
                            {
                                Dictionary<string, int> dictionary = new Dictionary<string, int>(6);
                                dictionary.Add("ExteriorWall", 0);
                                dictionary.Add("InteriorWall", 1);
                                dictionary.Add("Floor", 2);
                                dictionary.Add("MeDisplay", 3);
                                dictionary.Add("MonsterDisplay", 4);
                                dictionary.Add("PlayerDisplay", 5);
                                <>f__switch$map3B = dictionary;
                            }
                            if (<>f__switch$map3B.TryGetValue(tag, out num))
                            {
                                switch (num)
                                {
                                    case 5:
                                    {
                                        char[] separator = new char[] { '_' };
                                        string[] strArray = obj2.name.Split(separator);
                                        Singleton<RoleMode>.Instance.CurrentRoleId = Convert.ToUInt64(strArray[2]);
                                        Singleton<InteractMenuView>.Instance.OpenView();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    this._mouseDownStart = false;
                }
            }
        }

        protected override void Move()
        {
            if (this.CheckMove())
            {
                switch (this.GetMoveType())
                {
                    case MoveType.DoNotMove:
                        this.StopWalk();
                        break;

                    case MoveType.MoveDirectly:
                        this.MoveDirectly();
                        break;

                    case MoveType.TestMoveInKeyboard:
                        this.MoveThroughKeyboard();
                        break;
                }
            }
        }

        private void MoveDirectly()
        {
            Vector3 position = base.transform.position;
            base.destination = new Vector3(this.destination.x, position.y, this.destination.z);
            if (Vector3.Distance(position, base.destination) <= 0.5f)
            {
                if (base._moveEndCallback != null)
                {
                    base._moveEndCallback(this);
                }
                this.StopWalk();
            }
            else
            {
                this.BaseMove(base.destination);
                base.CheckMoveDistance();
            }
        }

        private void MoveThroughKeyboard()
        {
            if (this.CheckShouldRunWithCurrentStatu())
            {
                Vector3 position = base.transform.position;
                base.destination = new Vector3(position.x + this.fakeTgPosition.x, position.y, position.z + this.fakeTgPosition.y);
                if (Vector3.Distance(position, base.destination) <= 0.5f)
                {
                    if (base._moveEndCallback != null)
                    {
                        base._moveEndCallback(this);
                    }
                    this.isKeyboardMove = false;
                    this.StopWalk();
                }
                else
                {
                    this.BaseMove(base.destination);
                    base.CheckMoveDistance();
                }
            }
        }

        public void MoveToAndTellServer(float x, float y, MoveEndCallback callback = null)
        {
            x = (float) Math.Round((double) x, 2);
            y = (float) Math.Round((double) y, 2);
            this.MoveTo(x, y, callback);
            AppMap.Instance.tellServer(x, y);
        }

        public void PlayerRunToEnemyAndUseSkill(ActionDisplay enemy, int skillId, float radius, float nowDistance, int skillType)
        {
            this._lastCheckTimeWhenChasing = Time.time;
            this._targetDisplayWhenSkillChase = enemy;
            this._targetSkillIdAfterChase = skillId;
            this._targetSkillRadius = radius;
            if (((enemy.Controller == null) || (enemy.GoBase.transform == null)) || enemy.GetMeVoByType<BaseRoleVo>().IsEmptyHp)
            {
                this.MapAiStop();
                this.StopMove();
            }
            else if (nowDistance < radius)
            {
                this.StopWalk();
            }
            else
            {
                Log.AI(base.gameObject, " Player RunTo Attack With SkillType Press " + skillType);
                Vector3 position = enemy.GoBase.transform.position;
                ActionVo vo = new ActionVo {
                    Destination = position,
                    ActionType = "run",
                    IsAStarMove = true,
                    SkillType = skillType,
                    Target = enemy
                };
                base.AttackController.AddAttackList(vo, false, false);
            }
        }

        public bool PointIsUsable(Vector3 point)
        {
            return this.Raycast(point, "Floor");
        }

        public GameObject Raycast(Vector3 point)
        {
            RaycastHit hit;
            if (!Physics.Raycast(Camera.main.ScreenPointToRay(point), out hit))
            {
                return null;
            }
            return hit.transform.gameObject;
        }

        public bool Raycast(Vector3 point, string tag)
        {
            RaycastHit hit;
            if (!Physics.Raycast(Camera.main.ScreenPointToRay(point), out hit))
            {
                return false;
            }
            return (hit.transform.gameObject.tag == tag);
        }

        public void RefreshTargetWhenChasing(ActionDisplay ad)
        {
            this._targetDisplayWhenSkillChase = ad;
        }

        public void Relive()
        {
            CommandHandler.AddCommandStatic(base.gameObject, "idle relive", 1f);
        }

        public bool ReliveIfDie()
        {
            BaseRoleVo meVoByType = base.Me.GetMeVoByType<BaseRoleVo>();
            int currentStatu = base.StatuController.CurrentStatu;
            if ((!meVoByType.IsEmptyHp && (currentStatu != 11)) && (currentStatu != 0x13))
            {
                return false;
            }
            base.Invoke("DoRelive", 1.5f);
            return true;
        }

        public static void SendPos()
        {
            if (AppMap.Instance.mapParserII.NeedSyn)
            {
                Singleton<RoleMode>.Instance.SendStatuChange();
            }
        }

        public void SendPosInWifiPvp()
        {
            Singleton<WifiPvpMode>.Instance.FirstForceMoveSync(this);
        }

        private void showCursor(Vector3 pos)
        {
            Vector3 vector = pos;
            vector.y = base.transform.position.y;
            vector.z = 0f;
            EffectMgr.Instance.CreateMainEffect("20002", vector, false, null, 0f);
        }

        private void Start()
        {
            this.meCommandHandler = base.GetComponent<MeCommandHandler>();
            this.InitKeycodeDic();
            base.MoveSpeed = base.GetMeVo().GetRoleMoveSpeed();
        }

        private void StopMove()
        {
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        public override void StopWalk()
        {
            CommandHandler.AddCommandStatic(base.gameObject, "idle", 1f);
        }

        public GameObject UIRaycast(Vector3 point)
        {
            RaycastHit hit;
            if (!Physics.Raycast(UICamera.currentCamera.ScreenPointToRay(point), out hit))
            {
                return null;
            }
            return hit.transform.gameObject;
        }

        public static bool IsPressedMoveButton
        {
            [CompilerGenerated]
            get
            {
                return <IsPressedMoveButton>k__BackingField;
            }
            [CompilerGenerated]
            set
            {
                <IsPressedMoveButton>k__BackingField = value;
            }
        }

        public enum MoveType
        {
            DoNotMove = 1,
            MoveDirectly = 4,
            MoveThroughAStar = 3,
            MoveThroughJoyStick = 2,
            TestMoveInKeyboard = 5
        }
    }
}

