﻿namespace com.u3d.bases.display.controler
{
    using com.game;
    using com.game.basic;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.ai;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class MonsterControler : ActionControler
    {
        private MonsterDisplay monsterDisplay;
        private int tick = 0x7d;

        protected override void HandlerMonsterSpeakWords()
        {
            List<KeyValueInfo> monsterPaoPaoList = (this.monsterDisplay.GetVo() as MonsterVo).MonsterPaoPaoList;
            if ((monsterPaoPaoList != null) && ((this.monsterDisplay.GetVo() as MonsterVo).MonsterPaoPao != null))
            {
                KeyValueInfo item = null;
                for (int i = 0; i < monsterPaoPaoList.Count; i++)
                {
                    switch (int.Parse(monsterPaoPaoList[i].key))
                    {
                        case 0x3e9:
                        {
                            Vector3 point = (Vector3) (StringUtils.StringToVector3(monsterPaoPaoList[i].value) * 0.001f);
                            if (this.IsSelfNearToPoint(point) && base.SpeakWord(monsterPaoPaoList[i].descriptId))
                            {
                                item = monsterPaoPaoList[i];
                            }
                            break;
                        }
                        case 0x3ea:
                        {
                            float bloodPercent = float.Parse(monsterPaoPaoList[i].value) / 100f;
                            if (this.IsBloodTrigger(bloodPercent) && base.SpeakWord(monsterPaoPaoList[i].descriptId))
                            {
                                item = monsterPaoPaoList[i];
                            }
                            break;
                        }
                        case 0x3eb:
                        {
                            int skillType = int.Parse(monsterPaoPaoList[i].value);
                            this.tick++;
                            if (((this.tick >= 0x7d) && this.IsBossSkillTrigger(skillType)) && base.SpeakWord(monsterPaoPaoList[i].descriptId))
                            {
                                this.tick = 0;
                            }
                            break;
                        }
                    }
                }
                if (item != null)
                {
                    monsterPaoPaoList.Remove(item);
                }
            }
        }

        private bool IsBloodTrigger(float bloodPercent)
        {
            if (bloodPercent == 1f)
            {
                MonsterAiController aiController = this.monsterDisplay.Controller.AiController as MonsterAiController;
                return ((aiController != null) && (aiController.GetTargetDisplay() != null));
            }
            return ((this.monsterDisplay.GetVo() as MonsterVo).CurHpPercent <= bloodPercent);
        }

        private bool IsBossSkillTrigger(int skillType)
        {
            return ((((this.monsterDisplay.GetVo() as MonsterVo).CurHpPercent > 0f) && this.monsterDisplay.isBoss) && (this.monsterDisplay.Controller.StatuController.CurrentStatu >= skillType));
        }

        private bool IsSelfNearToPoint(Vector3 point)
        {
            MeDisplay me = AppMap.Instance.me;
            if ((me == null) || (me.GoBase == null))
            {
                return false;
            }
            return (MathUtils.VectorDistance(me.GoBase.transform.position, point) <= 5f);
        }

        protected override void Move()
        {
            if ((this.CheckMove() && ((base.gameObject.GetComponent<MonsterMoveAI>() == null) || base.isAutoWalkToDestination)) && base.isAutoWalkToDestination)
            {
                Vector3 position = base.transform.position;
                base.destination = new Vector3(this.destination.x, position.y, this.destination.z);
                if (Vector3.Distance(position, base.destination) < 0.3f)
                {
                    if (base._moveEndCallback != null)
                    {
                        base._moveEndCallback(this);
                    }
                    base.isAutoWalkToDestination = false;
                    this.StopWalk();
                }
                else
                {
                    this.BaseMove(base.destination);
                    base.CheckMoveDistance();
                }
            }
        }

        private void Start()
        {
            base.MoveSpeed = base.GetMeVo().GetMonMoveSpeed();
            this.monsterDisplay = base.Me as MonsterDisplay;
        }
    }
}

