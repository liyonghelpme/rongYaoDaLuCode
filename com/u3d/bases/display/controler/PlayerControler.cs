﻿namespace com.u3d.bases.display.controler
{
    using com.game.vo;
    using System;
    using UnityEngine;

    public class PlayerControler : ActionControler
    {
        protected float AccuTime;
        public PlayerVo MePlayerVo;

        protected override void Move()
        {
            if (this.CheckMove() && base.isAutoWalkToDestination)
            {
                Vector3 position = base.transform.position;
                base.destination = new Vector3(this.destination.x, position.y, this.destination.z);
                if (Vector3.Distance(position, base.destination) < 0.3f)
                {
                    if (base._moveEndCallback != null)
                    {
                        base._moveEndCallback(this);
                    }
                    base.isAutoWalkToDestination = false;
                    this.StopWalk();
                }
                else
                {
                    this.BaseMove(base.destination);
                    base.CheckMoveDistance();
                }
            }
        }

        private void Start()
        {
            this.MePlayerVo = base.GetMeVoByType<PlayerVo>();
            base.characterController = base.transform.gameObject.GetComponent<CharacterController>();
            base.MoveSpeed = base.GetMeVo().GetRoleMoveSpeed();
        }
    }
}

