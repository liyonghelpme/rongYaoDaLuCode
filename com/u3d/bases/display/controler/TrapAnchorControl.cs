﻿namespace com.u3d.bases.display.controler
{
    using com.game.vo;
    using com.u3d.bases.display.character;
    using System;
    using UnityEngine;

    public class TrapAnchorControl : MonoBehaviour
    {
        public com.u3d.bases.display.controler.ActionControler ActionControler;
        public TrapVo ThisTrapVo;
        public TrapDisplay TrapMeDisplay;

        private void ActionAttack()
        {
            if (this.ActionControler != null)
            {
                this.ActionControler.SkillController.RequestUseSkill(0, false, false, null, null, null);
                vp_Timer.In(this.ThisTrapVo.SysTrapVo.AttackInterval * 0.001f, new vp_Timer.Callback(this.ActionAttack), null);
            }
        }

        private void Start()
        {
            this.ActionControler.StatuController.SetStatu(0);
            this.TrapMeDisplay = this.ActionControler.Me as TrapDisplay;
            this.ThisTrapVo = this.TrapMeDisplay.GetTrapVo();
            vp_Timer.In(this.ThisTrapVo.SysTrapVo.AttackInterval * 0.001f, new vp_Timer.Callback(this.ActionAttack), null);
        }
    }
}

