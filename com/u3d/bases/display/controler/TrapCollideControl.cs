﻿namespace com.u3d.bases.display.controler
{
    using com.game.data;
    using com.game.manager;
    using com.game.module.effect;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.display.character;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class TrapCollideControl : MonoBehaviour
    {
        private float _lastAttackTime = 10f;
        private SysSkillBaseVo _skillBaseVo;
        private float _startTime;
        public com.u3d.bases.display.controler.ActionControler ActionControler;
        public List<ActionDisplay> LastAttackedActionDisplay;
        public TrapVo ThisTrapVo;
        public TrapDisplay TrapMeDisplay;

        private void DeleteMainCylinderTrigger()
        {
            GameObject mainEffectGameObject = EffectMgr.Instance.GetMainEffectGameObject("20013", string.Empty);
            if (mainEffectGameObject != null)
            {
                UnityEngine.Object.Destroy(mainEffectGameObject);
            }
        }

        private void Start()
        {
            this.ActionControler.StatuController.SetStatu(0);
            this.TrapMeDisplay = this.ActionControler.Me as TrapDisplay;
            this.ThisTrapVo = this.TrapMeDisplay.GetTrapVo();
            this._skillBaseVo = BaseDataMgr.instance.GetSysSkillBaseVo(uint.Parse(StringUtils.GetValueListFromString(this.ThisTrapVo.SysTrapVo.SkillIds, ',')[0]));
            this.LastAttackedActionDisplay = new List<ActionDisplay>();
            this._startTime = 0f;
            Vector3 position = base.transform.position;
            EffectMgr.Instance.CreateMainEffect("20012", position, true, null, 0f);
        }

        private void Update()
        {
            this._startTime += Time.deltaTime;
            this._lastAttackTime += Time.deltaTime;
            if (this._startTime > 0.5f)
            {
            }
        }
    }
}

