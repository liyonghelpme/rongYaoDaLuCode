﻿namespace com.u3d.bases.display.vo
{
    using com.game.data;
    using com.game.utils;
    using com.game.vo;
    using System;
    using System.Collections.Generic;

    public class BattleObjectVo : BaseRoleVo
    {
        private com.game.data.SysBattleObjectVo _vo;
        public List<SimpleBuffVo> buff_list;
        public int SpawnPoolId = -1;
        public uint template_id;

        public com.game.data.SysBattleObjectVo SysBattleObjectVo
        {
            get
            {
                return this._vo;
            }
            set
            {
                this._vo = value;
                this.buff_list = StringUtils.GetBuffFromString(this._vo.buff_list);
            }
        }
    }
}

