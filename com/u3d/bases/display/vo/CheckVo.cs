﻿namespace com.u3d.bases.display.vo
{
    using com.game;
    using com.u3d.bases.map;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CheckVo
    {
        private readonly BaseMap _map;
        private Transform doorTransform;
        private GameObject hitObj;

        public CheckVo(BaseMap map)
        {
            this._map = map;
        }

        public void Call()
        {
            if (this.hitObj != null)
            {
                this._map.hitCallback(this.hitObj);
            }
        }

        public DoorPosition GetDoorPosition()
        {
            DoorPosition position = new DoorPosition();
            if (this.doorTransform == null)
            {
                position.isNull = true;
                return position;
            }
            position.Set(false, this.doorTransform.position);
            return position;
        }

        public bool IsHit()
        {
            GameObject[] objArray = GameObject.FindGameObjectsWithTag("Collider");
            if (objArray.Length > 0)
            {
                this.doorTransform = objArray[0].transform;
                foreach (GameObject obj2 in objArray)
                {
                    bool flag = Math.Abs(Vector3.Distance(obj2.transform.position, AppMap.Instance.me.GoBase.transform.position)) <= 0.8f;
                    this.hitObj = !flag ? null : obj2;
                    return flag;
                }
            }
            return false;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DoorPosition
        {
            public bool isNull;
            public Vector3 position;
            public void Set(bool flag, Vector3 pos)
            {
                this.isNull = flag;
                this.position = pos;
            }
        }
    }
}

