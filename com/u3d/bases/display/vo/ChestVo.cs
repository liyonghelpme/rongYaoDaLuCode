﻿namespace com.u3d.bases.display.vo
{
    using com.game.data;
    using com.game.manager;
    using System;

    public class ChestVo : DisplayVo
    {
        private SysDungeonChestVo _sysVo;
        private uint _uid;

        public uint sysId
        {
            get
            {
                return this._uid;
            }
            set
            {
                if (this._uid != value)
                {
                    this._uid = value;
                    this._sysVo = BaseDataMgr.instance.GetDungeonChestVo(this._uid);
                }
            }
        }

        public SysDungeonChestVo sysVo
        {
            get
            {
                if (this._sysVo == null)
                {
                    return this._sysVo;
                }
                return this._sysVo;
            }
        }
    }
}

