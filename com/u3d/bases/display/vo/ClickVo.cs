﻿namespace com.u3d.bases.display.vo
{
    using com.u3d.bases.display;
    using com.u3d.bases.map;
    using System;
    using UnityEngine;

    public class ClickVo
    {
        private BaseDisplay _display;
        private bool _isClick;
        private readonly BaseMap _map;

        public ClickVo(BaseMap map)
        {
            this._map = map;
        }

        public void Call()
        {
            this._isClick = false;
        }

        public void Call(BaseDisplay bd)
        {
            this._isClick = false;
        }

        public bool IsHit()
        {
            if (!this._isClick || (this._display == null))
            {
                return false;
            }
            GameObject goCloth = this._map.me.GoCloth;
            GameObject obj3 = this._display.GoCloth;
            if ((goCloth == null) || (obj3 == null))
            {
                return false;
            }
            return (Mathf.Abs((float) (goCloth.transform.position.x - obj3.transform.position.x)) <= 0.8f);
        }

        public void SaveClicker(BaseDisplay clicker)
        {
            if (clicker == null)
            {
                this._isClick = false;
            }
            else
            {
                this._display = clicker;
                this._isClick = true;
            }
        }
    }
}

