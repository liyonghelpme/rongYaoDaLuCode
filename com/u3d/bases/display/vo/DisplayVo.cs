﻿namespace com.u3d.bases.display.vo
{
    using PCustomDataType;
    using System;
    using UnityEngine;

    public class DisplayVo
    {
        public float boundDistance;
        public string ClothUrl;
        public ulong Id;
        public bool isChangeModleTransform;
        public Action<BaseDisplay> ModelLoadCallBack;
        public string Name;
        public float rotateY;
        public Vector3 scale = Vector3.one;
        public PStylebin stylebin;
        public int SubType;
        public int Type;
        public uint uniKeyId;
        public float X;
        public float Y;
        public float Z;

        public string Key
        {
            get
            {
                return (this.Type + "_" + this.Id);
            }
        }
    }
}

