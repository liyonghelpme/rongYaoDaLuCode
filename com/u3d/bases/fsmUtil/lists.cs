﻿namespace com.u3d.bases.fsmUtil
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading;

    public static class lists
    {
        public static IList<T> DropLast<T>(IList<T> list)
        {
            int count = list.Count;
            if (count != 0)
            {
                list.RemoveAt(count - 1);
            }
            return list;
        }

        public static ICollection<T> Duplicate<T>(int n, T value)
        {
            ICollection<T> is2 = new Collection<T>();
            while (n > 0)
            {
                n--;
                is2.Add(value);
            }
            return is2;
        }

        [DebuggerHidden]
        public static IEnumerable<T> Filter<T>(this IEnumerable<T> list, Predicate<T> filter)
        {
            return new <Filter>c__Iterator51<T> { list = list, filter = filter, <$>list = list, <$>filter = filter, $PC = -2 };
        }

        [DebuggerHidden]
        public static IEnumerable<T> Filter<T>(Predicate<T> filter, IEnumerable<T> list)
        {
            return new <Filter>c__Iterator53<T> { list = list, filter = filter, <$>list = list, <$>filter = filter, $PC = -2 };
        }

        public static IList<T> FilterMap<T>(Func<T, Tuple<T>> func, ICollection<T> list) where T: EqualityComparer<T>, IComparable
        {
            IList<T> list2 = new List<T>();
            IEnumerator<T> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    T current = enumerator.Current;
                    Tuple<T> tuple = func(current);
                    if (tuple.result)
                    {
                        list2.Add((tuple.obj != null) ? tuple.obj : current);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return list2;
        }

        public static TR Foldl<T, TR>(this IEnumerable<T> list, Func<TR, T, TR> accumlator, TR startVal)
        {
            TR local = startVal;
            IEnumerator<T> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    T current = enumerator.Current;
                    local = accumlator(local, current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return local;
        }

        public static TR Foldl<T, TR>(Func<TR, T, TR> accumlator, TR startVal, IEnumerable<T> list)
        {
            TR local = startVal;
            IEnumerator<T> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    T current = enumerator.Current;
                    local = accumlator(local, current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return local;
        }

        public static TR FoldR<T, TR>(Func<TR, T, TR> accumulator, TR startVal, IList<T> list)
        {
            TR local = startVal;
            for (int i = list.Count<T>(); i > 0; i--)
            {
                local = accumulator(local, list[i]);
            }
            return local;
        }

        public static void Foreach<T>(Action<T> func, IEnumerable<T> list)
        {
            IEnumerator<T> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    T current = enumerator.Current;
                    func(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static void Foreach<T>(this IEnumerable<T> list, Action<T> func)
        {
            IEnumerator<T> enumerator = list.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    T current = enumerator.Current;
                    func(current);
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public static T Last<T>(IList<T> list1) where T: class
        {
            int count = list1.Count;
            if (count == 0)
            {
                return null;
            }
            return list1[count - 1];
        }

        public static int Length<T>(IEnumerable<T> list)
        {
            ICollection<T> is2 = list as ICollection<T>;
            if (is2 != null)
            {
                return is2.Count;
            }
            using (IEnumerator<T> enumerator = list.GetEnumerator())
            {
                int num = 0;
                while (enumerator.MoveNext())
                {
                    num++;
                }
                return num;
            }
        }

        [DebuggerHidden]
        public static IEnumerable<TR> Map<T, TR>(this IEnumerable<T> list, Converter<T, TR> func)
        {
            return new <Map>c__Iterator50<T, TR> { list = list, func = func, <$>list = list, <$>func = func, $PC = -2 };
        }

        [DebuggerHidden]
        public static IEnumerable<TR> Map<T, TR>(Converter<T, TR> func, IEnumerable<T> list)
        {
            return new <Map>c__Iterator52<T, TR> { list = list, func = func, <$>list = list, <$>func = func, $PC = -2 };
        }

        public static T Max<T>(IEnumerable<T> list1)
        {
            return list1.Max<T>();
        }

        public static List<T> Merge<T>(List<List<T>> listOfList)
        {
            List<T> list = new List<T>();
            foreach (List<T> list2 in listOfList)
            {
                foreach (T local in list2)
                {
                    if (!list.Contains(local))
                    {
                        list.Add(local);
                    }
                }
            }
            return list;
        }

        public static List<T> Merge<T>(params List<T>[] lists)
        {
            return Merge<T>(lists.ToList<List<T>>());
        }

        [CompilerGenerated]
        private sealed class <Filter>c__Iterator51<T> : IEnumerator, IEnumerable, IDisposable, IEnumerable<T>, IEnumerator<T>
        {
            internal T $current;
            internal int $PC;
            internal Predicate<T> <$>filter;
            internal IEnumerable<T> <$>list;
            internal IEnumerator<T> <$s_830>__0;
            internal T <val>__1;
            internal Predicate<T> filter;
            internal IEnumerable<T> list;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            if (this.<$s_830>__0 == null)
                            {
                            }
                            this.<$s_830>__0.Dispose();
                        }
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<$s_830>__0 = this.list.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00BE;
                }
                try
                {
                    while (this.<$s_830>__0.MoveNext())
                    {
                        this.<val>__1 = this.<$s_830>__0.Current;
                        if (this.filter(this.<val>__1))
                        {
                            this.$current = this.<val>__1;
                            this.$PC = 1;
                            flag = true;
                            return true;
                        }
                    }
                }
                finally
                {
                    if (!flag)
                    {
                    }
                    if (this.<$s_830>__0 == null)
                    {
                    }
                    this.<$s_830>__0.Dispose();
                }
                this.$PC = -1;
            Label_00BE:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new lists.<Filter>c__Iterator51<T> { list = this.<$>list, filter = this.<$>filter };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<T>.GetEnumerator();
            }

            T IEnumerator<T>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Filter>c__Iterator53<T> : IEnumerator, IEnumerable, IDisposable, IEnumerable<T>, IEnumerator<T>
        {
            internal T $current;
            internal int $PC;
            internal Predicate<T> <$>filter;
            internal IEnumerable<T> <$>list;
            internal IEnumerator<T> <$s_835>__0;
            internal T <val>__1;
            internal Predicate<T> filter;
            internal IEnumerable<T> list;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            if (this.<$s_835>__0 == null)
                            {
                            }
                            this.<$s_835>__0.Dispose();
                        }
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<$s_835>__0 = this.list.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00BE;
                }
                try
                {
                    while (this.<$s_835>__0.MoveNext())
                    {
                        this.<val>__1 = this.<$s_835>__0.Current;
                        if (this.filter(this.<val>__1))
                        {
                            this.$current = this.<val>__1;
                            this.$PC = 1;
                            flag = true;
                            return true;
                        }
                    }
                }
                finally
                {
                    if (!flag)
                    {
                    }
                    if (this.<$s_835>__0 == null)
                    {
                    }
                    this.<$s_835>__0.Dispose();
                }
                this.$PC = -1;
            Label_00BE:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new lists.<Filter>c__Iterator53<T> { list = this.<$>list, filter = this.<$>filter };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<T>.GetEnumerator();
            }

            T IEnumerator<T>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Map>c__Iterator50<T, TR> : IEnumerator, IEnumerable, IDisposable, IEnumerable<TR>, IEnumerator<TR>
        {
            internal TR $current;
            internal int $PC;
            internal Converter<T, TR> <$>func;
            internal IEnumerable<T> <$>list;
            internal IEnumerator<T> <$s_828>__0;
            internal T <val>__1;
            internal Converter<T, TR> func;
            internal IEnumerable<T> list;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            if (this.<$s_828>__0 == null)
                            {
                            }
                            this.<$s_828>__0.Dispose();
                        }
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<$s_828>__0 = this.list.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00B3;
                }
                try
                {
                    while (this.<$s_828>__0.MoveNext())
                    {
                        this.<val>__1 = this.<$s_828>__0.Current;
                        this.$current = this.func(this.<val>__1);
                        this.$PC = 1;
                        flag = true;
                        return true;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                    }
                    if (this.<$s_828>__0 == null)
                    {
                    }
                    this.<$s_828>__0.Dispose();
                }
                this.$PC = -1;
            Label_00B3:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<TR> IEnumerable<TR>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new lists.<Map>c__Iterator50<T, TR> { list = this.<$>list, func = this.<$>func };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<TR>.GetEnumerator();
            }

            TR IEnumerator<TR>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Map>c__Iterator52<T, TR> : IEnumerator, IEnumerable, IDisposable, IEnumerable<TR>, IEnumerator<TR>
        {
            internal TR $current;
            internal int $PC;
            internal Converter<T, TR> <$>func;
            internal IEnumerable<T> <$>list;
            internal IEnumerator<T> <$s_833>__0;
            internal T <val>__1;
            internal Converter<T, TR> func;
            internal IEnumerable<T> list;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            if (this.<$s_833>__0 == null)
                            {
                            }
                            this.<$s_833>__0.Dispose();
                        }
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<$s_833>__0 = this.list.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_00B3;
                }
                try
                {
                    while (this.<$s_833>__0.MoveNext())
                    {
                        this.<val>__1 = this.<$s_833>__0.Current;
                        this.$current = this.func(this.<val>__1);
                        this.$PC = 1;
                        flag = true;
                        return true;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                    }
                    if (this.<$s_833>__0 == null)
                    {
                    }
                    this.<$s_833>__0.Dispose();
                }
                this.$PC = -1;
            Label_00B3:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<TR> IEnumerable<TR>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new lists.<Map>c__Iterator52<T, TR> { list = this.<$>list, func = this.<$>func };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.System.Collections.Generic.IEnumerable<TR>.GetEnumerator();
            }

            TR IEnumerator<TR>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        public class Tuple<T>
        {
            public T obj;
            public bool result;
        }
    }
}

