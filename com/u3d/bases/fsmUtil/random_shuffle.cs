﻿namespace com.u3d.bases.fsmUtil
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography;
    using System.Threading;

    public static class random_shuffle
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            Random random = new Random();
            int count = list.Count;
            while (count > 1)
            {
                count--;
                int num2 = random.Next(count + 1);
                T local = list[num2];
                list[num2] = list[count];
                list[count] = local;
            }
        }

        public static void ShuffleBetter<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int count = list.Count;
            while (count > 1)
            {
                byte[] data = new byte[1];
                do
                {
                    provider.GetBytes(data);
                }
                while (data[0] >= (count * (0xff / count)));
                int num2 = data[0] % count;
                count--;
                T local = list[num2];
                list[num2] = list[count];
                list[count] = local;
            }
        }

        public static IList<T> ShuffleLinq<T>(this IList<T> list)
        {
            <ShuffleLinq>c__AnonStorey11B<T> storeyb = new <ShuffleLinq>c__AnonStorey11B<T> {
                rand = new Random()
            };
            return list.OrderBy<T, int>(new Func<T, int>(storeyb.<>m__1CD)).ToList<T>();
        }

        public static void ShuffleThreadSafe<T>(this IList<T> list)
        {
            int count = list.Count;
            while (count > 1)
            {
                count--;
                int num2 = ThreadSafeRandom.ThisThreadRandom.Next(count + 1);
                T local = list[num2];
                list[num2] = list[count];
                list[count] = local;
            }
        }

        [CompilerGenerated]
        private sealed class <ShuffleLinq>c__AnonStorey11B<T>
        {
            internal Random rand;

            internal int <>m__1CD(T x)
            {
                return this.rand.Next();
            }
        }

        public static class ThreadSafeRandom
        {
            [ThreadStatic]
            private static Random local;

            public static Random ThisThreadRandom
            {
                get
                {
                    if (local == null)
                    {
                    }
                    return (local = new Random((Environment.TickCount * 0x1f) + Thread.CurrentThread.ManagedThreadId));
                }
            }
        }
    }
}

