﻿namespace com.u3d.bases.fsmUtil
{
    using com.u3d.bases.display;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class tools
    {
        public static DateTime GetDataTime(int unixtimestamp)
        {
            DateTime time = new DateTime(0x7b2, 1, 1);
            return time.AddSeconds((double) unixtimestamp).ToLocalTime();
        }

        public static int GetUnixTimestamp()
        {
            return (int) DateTime.UtcNow.Subtract(new DateTime(0x7b2, 1, 1)).TotalSeconds;
        }

        public static void LookAtTarget(this Transform trans1, BaseDisplay dis)
        {
            if (dis.GoBase != null)
            {
                trans1.LookAtTarget(dis.GoBase.transform.position);
            }
        }

        public static void LookAtTarget(this Transform trans1, Transform trans2)
        {
            if (trans2 != null)
            {
                trans1.LookAtTarget(trans2.position);
            }
        }

        public static void LookAtTarget(this Transform trans, Vector3 vec)
        {
            Vector3 forward = vec - trans.position;
            Vector3 eulerAngles = Quaternion.LookRotation(forward).eulerAngles;
            eulerAngles.z = eulerAngles.x = 0f;
            Quaternion quaternion = Quaternion.Euler(eulerAngles);
            trans.rotation = quaternion;
        }

        [DebuggerHidden]
        public static IEnumerator WaitforSeveralFrames(int waitFrameCount)
        {
            return new <WaitforSeveralFrames>c__Iterator54 { waitFrameCount = waitFrameCount, <$>waitFrameCount = waitFrameCount };
        }

        [CompilerGenerated]
        private sealed class <WaitforSeveralFrames>c__Iterator54 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal int <$>waitFrameCount;
            internal int waitFrameCount;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        break;

                    case 1:
                        this.waitFrameCount--;
                        break;

                    default:
                        goto Label_005A;
                }
                if (this.waitFrameCount > 0)
                {
                    this.$current = null;
                    this.$PC = 1;
                    return true;
                }
                this.$PC = -1;
            Label_005A:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }
    }
}

