﻿namespace com.u3d.bases.interfaces
{
    using System;

    internal interface ICurrying<out TResult>
    {
        TResult Currying<TResult>(Delegate function, params object[] paramsValue);
    }
}

