﻿namespace com.u3d.bases.interfaces
{
    using com.u3d.bases.ai;
    using System;

    internal interface IFsmState
    {
        void Enter(AiControllerBase ai, params object[] args);
        void Exit(AiControllerBase owner, params object[] args);
        void Process(AiControllerBase owner, params object[] args);
    }
}

