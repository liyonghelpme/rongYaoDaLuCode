﻿namespace com.u3d.bases.interfaces
{
    using System;

    public interface IPoolable
    {
        void Dispose();
        void getBefore();
    }
}

