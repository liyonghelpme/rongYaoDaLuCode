﻿namespace com.u3d.bases.joystick
{
    using com.game;
    using com.u3d.bases.display.controler;
    using System;
    using System.Runtime.CompilerServices;
    using UnityLog;

    public sealed class JoystickController
    {
        public GuideDungeon guideDungeonCallBack;
        public static readonly JoystickController instance = new JoystickController();
        private MeControler meController;

        public void Reset()
        {
            if (this.joystick != null)
            {
                this.joystick.Reset();
                this.joystick.onPressed = false;
            }
        }

        public void setCurrentJoystick(NGUIJoystick joystickIns)
        {
            this.joystick = joystickIns;
        }

        public void Update()
        {
            if (this.joystick != null)
            {
                this.meController = AppMap.Instance.MeControler();
                if (this.meController == null)
                {
                    Log.AI(null, " JoyStick Not Find Me ");
                }
                else if (!this.meController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.CanMove)
                {
                    Log.AI(null, " JoyStick Can't Move");
                }
                else if (this.meController.Me.GetMeVoByType<BaseRoleVo>().stateInfo.ShouldPauseJoystickAndAtkBtn)
                {
                    Log.AI(null, " JoyStick Should Pause JoyStick ");
                }
                else if (this.joystick.onPressed)
                {
                    if (!this.LastJoystickStatus)
                    {
                        this.LastJoystickStatus = true;
                        if (this.meController.GetComponent<PlayerAiController>() != null)
                        {
                            this.meController.GetComponent<PlayerAiController>().OnlyStopAi();
                        }
                    }
                    if (this.joystick.onDrag)
                    {
                        this.meController.JoystickMove(this.joystick.position);
                    }
                    if (this.guideDungeonCallBack != null)
                    {
                        this.guideDungeonCallBack();
                    }
                }
                else if (this.LastJoystickStatus && !this.joystick.onPressed)
                {
                    this.meController.JoystickIdle();
                    this.LastJoystickStatus = false;
                }
            }
        }

        public NGUIJoystick joystick { get; private set; }

        private bool LastJoystickStatus { get; set; }

        public bool OnPressed
        {
            get
            {
                return ((this.joystick != null) && this.joystick.onPressed);
            }
        }

        public delegate void GuideDungeon();
    }
}

