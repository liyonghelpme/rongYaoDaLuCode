﻿namespace com.u3d.bases.loader
{
    using System;

    public interface ILoadingBar
    {
        void remove();
        void show();
        void update(int progreess);
    }
}

