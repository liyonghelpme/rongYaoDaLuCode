﻿namespace com.u3d.bases.loader
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public delegate void LoadFinishBack(string url, object[] args = null);
}

