﻿namespace com.u3d.bases.loader
{
    using System;
    using System.Text;
    using UnityEngine;

    public class LoadItem
    {
        internal object[] args;
        internal LoadFinishBack callback;
        internal LoadFinishBack errorback;
        internal int loadBeginTime;
        public int priority;
        public int resType;
        private StringBuilder str;
        internal System.Type types;
        public string url;
        internal WWW www;

        internal void call()
        {
            if (this.callback != null)
            {
                this.callback(this.url, this.args);
            }
        }

        internal void callError()
        {
            if (this.errorback != null)
            {
                this.errorback(this.url, this.args);
            }
        }

        internal void dispose()
        {
            this.str = null;
            this.url = null;
            this.args = null;
            this.www = null;
            this.types = null;
            this.callback = null;
            this.errorback = null;
        }

        public string toString()
        {
            if (this.str == null)
            {
                this.str = new StringBuilder();
                this.str.Append("[resType:" + this.resType + ",");
                this.str.Append("[url:" + this.url + ",");
                this.str.Append("[priority:" + this.priority + "]");
            }
            return this.str.ToString();
        }
    }
}

