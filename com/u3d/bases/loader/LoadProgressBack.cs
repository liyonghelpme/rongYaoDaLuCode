﻿namespace com.u3d.bases.loader
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void LoadProgressBack(string url, int progress);
}

