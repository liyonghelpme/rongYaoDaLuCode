﻿namespace com.u3d.bases.loader
{
    using com.bases.utils;
    using com.u3d.bases.debug;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    internal class ResPool
    {
        internal static ResPool instance = new ResPool();
        private IDictionary<string, UnityEngine.Object> loadedPool = new Dictionary<string, UnityEngine.Object>();
        private IDictionary<string, UnityEngine.Object> usedPool = new Dictionary<string, UnityEngine.Object>();

        internal void addLoadedRes(string url, UnityEngine.Object data)
        {
            if ((data != null) && !StringUtils.isEmpty(url))
            {
                if (!this.loadedPool.ContainsKey(url))
                {
                    this.loadedPool.Add(url, data);
                }
                Log.info(this, "-addLoadedRes() url:" + url + " Add [OK]");
            }
        }

        internal void addRes(string url, UnityEngine.Object data)
        {
            if (((data != null) && !StringUtils.isEmpty(url)) && !this.usedPool.ContainsKey(url))
            {
                this.usedPool.Add(url, data);
            }
        }

        internal void delLoadedRes(string url)
        {
            if (this.hasLoadedRes(url))
            {
                this.loadedPool.Remove(url);
            }
        }

        internal bool delRes(string url)
        {
            if (!this.hasRes(url))
            {
                return false;
            }
            return this.usedPool.Remove(url);
        }

        internal void dispose(string url)
        {
            this.delRes(url);
            this.delLoadedRes(url);
        }

        internal AnimationClip getAnimationClip(string url)
        {
            return (AnimationClip) this.getRes(url);
        }

        internal UnityEngine.Object getLoaedRes(string url)
        {
            if (StringUtils.isEmpty(url))
            {
                return null;
            }
            return (!this.loadedPool.ContainsKey(url) ? null : this.loadedPool[url]);
        }

        internal UnityEngine.Object getRes(string url)
        {
            return (!StringUtils.isEmpty(url) ? this.usedPool[url] : null);
        }

        internal Texture2D getTexture2D(string url)
        {
            return (Texture2D) this.getRes(url);
        }

        internal bool hasLoadedRes(string url)
        {
            if (StringUtils.isEmpty(url))
            {
                return false;
            }
            return this.loadedPool.ContainsKey(url);
        }

        internal bool hasRes(string url)
        {
            if (StringUtils.isEmpty(url))
            {
                return false;
            }
            return this.usedPool.ContainsKey(url);
        }
    }
}

