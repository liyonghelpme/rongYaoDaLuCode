﻿namespace com.u3d.bases.loader
{
    using com.bases.utils;
    using System;
    using System.Collections.Generic;

    public class VerMgr
    {
        private string _basePath = string.Empty;
        private string _language = string.Empty;
        private IDictionary<string, int> versionDict = new Dictionary<string, int>();

        public string getLoadUrl(string url)
        {
            if (StringUtils.isEmpty(url))
            {
                return string.Empty;
            }
            return (this._language + "/" + url);
        }

        public string getNotVersionUrl(string url)
        {
            if (StringUtils.isEmpty(url))
            {
                return string.Empty;
            }
            return (this.basePath + url);
        }

        public int getVersion(string url)
        {
            if (StringUtils.isEmpty(url))
            {
                return 0;
            }
            return (!this.versionDict.ContainsKey(url) ? 0 : this.versionDict[url]);
        }

        public string getVersionUrl(string url)
        {
            if (StringUtils.isEmpty(url))
            {
                return string.Empty;
            }
            int num = this.getVersion(url);
            return ((url.IndexOf("?") != -1) ? string.Concat(new object[] { this.basePath, url, "&v=", num }) : string.Concat(new object[] { this.basePath, url, "?v=", num }));
        }

        public void init(string basePath, string language)
        {
            if (!StringUtils.isEmpty(basePath) && !StringUtils.isEmpty(language))
            {
                this._language = language;
                string str = string.Empty;
                basePath = basePath.Replace('\\', '/');
                if (basePath.LastIndexOf("/") != (basePath.Length - 1))
                {
                    str = "/";
                }
                this._basePath = basePath + str + language + "/";
            }
        }

        public string basePath
        {
            get
            {
                return this._basePath;
            }
        }

        public string language
        {
            get
            {
                return this._language;
            }
        }
    }
}

