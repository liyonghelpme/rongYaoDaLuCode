﻿namespace com.u3d.bases.loader.consts
{
    using System;

    public class ResPriority
    {
        public const int BINARY = 0x44c;
        public const int EFFECT = 0x4b0;
        public const int HORSE = 0x1388;
        public const int IMGAE = 0x578;
        public const int LOW = 500;
        public const int ME_ROLE = 0x17d4;
        public const int ME_SKILL = 0x1bbc;
        public const int MONSTER = 0xfa0;
        public const int NPC = 0x7d0;
        public const int ROLE = 0x1770;
        public const int SKILL = 0x1b58;
        public const int SOUND = 0x3e8;
        internal const int SYSTEM = 0x2328;
        public const int UI = 0x1f40;
        public const int WEAPON = 0xbb8;
        public const int XML = 0x514;
    }
}

