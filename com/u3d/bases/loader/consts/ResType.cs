﻿namespace com.u3d.bases.loader.consts
{
    using System;

    public class ResType
    {
        public const int ANIMATION = 0x1f40;
        public const int BINARY = 0x1b58;
        public const int DATAVO = 0x1770;
        public const int IMGAE = 0xbb8;
        public const int SOUND = 0xfa0;
        public const int TXT = 0x7d0;
        public const int XML = 0x3e8;
        public const int ZIP = 0x1388;
    }
}

