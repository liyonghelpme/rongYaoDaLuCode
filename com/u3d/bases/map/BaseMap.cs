﻿namespace com.u3d.bases.map
{
    using com.game.module.core;
    using com.game.module.map.NPC;
    using com.game.vo;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.vo;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class BaseMap
    {
        protected MeDisplay _me;
        protected List<MonsterDisplay> _monsterList;
        protected List<PlayerDisplay> _myPartnerList;
        private List<NpcDisplay> _npcList;
        protected List<BaseSceneNPC> _npcObjectList;
        internal IList<PlayerDisplay> _playerList;
        internal CheckVo checker;
        internal ClickVo clicker;
        protected MeDisplay EditorMe;
        public bool IsEditor;
        internal IList<BaseDisplay> objectList;
        protected MapParserII parserII;

        public BaseMap()
        {
            this.clicker = new ClickVo(this);
            this.checker = new CheckVo(this);
            this.parserII = new MapParserII(this);
            this.objectList = new List<BaseDisplay>();
            this._playerList = new List<PlayerDisplay>();
            this._monsterList = new List<MonsterDisplay>();
            this._npcList = new List<NpcDisplay>();
            this._npcObjectList = new List<BaseSceneNPC>();
            this._myPartnerList = new List<PlayerDisplay>();
        }

        public void dispose()
        {
            if ((this._playerList != null) && (this._playerList.Count > 0))
            {
                IEnumerator<PlayerDisplay> enumerator = this._playerList.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        enumerator.Current.Dispose();
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
                this._playerList.Clear();
            }
            if ((this._monsterList != null) && (this._monsterList.Count > 0))
            {
                foreach (BaseDisplay display2 in this._monsterList)
                {
                    display2.Dispose();
                }
                this._monsterList.Clear();
            }
            if ((this._npcList != null) && (this._npcList.Count > 0))
            {
                foreach (BaseDisplay display3 in this._npcList)
                {
                    display3.Dispose();
                }
                this._npcList.Clear();
            }
            if ((this._myPartnerList != null) && (this._myPartnerList.Count > 0))
            {
                foreach (BaseDisplay display4 in this._myPartnerList)
                {
                    display4.Dispose();
                }
                this._myPartnerList.Clear();
            }
            if ((this._npcObjectList != null) && (this._npcObjectList.Count > 0))
            {
                foreach (BaseSceneNPC enpc in this._npcObjectList)
                {
                    enpc.Dispose();
                }
                this._npcObjectList.Clear();
            }
            this.objectList.Clear();
        }

        public virtual void hitCallback(GameObject target)
        {
        }

        public bool isAllDeadByGroup(int group)
        {
            if (Singleton<MonsterMgr>.Instance.GetNumOfMonster(0, true, false, false) > 0)
            {
                return false;
            }
            IEnumerator<MonsterDisplay> enumerator = this.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    MonsterVo vo = current.GetVo() as MonsterVo;
                    if (vo.groupIndex == group)
                    {
                        return false;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return true;
        }

        public virtual bool monseClickEnable()
        {
            return false;
        }

        public bool remove(BaseDisplay display)
        {
            if (display != null)
            {
                display.IsUsing = false;
                if (display is PlayerDisplay)
                {
                    this._playerList.Remove((PlayerDisplay) display);
                }
                if (display is MonsterDisplay)
                {
                    this._monsterList.Remove((MonsterDisplay) display);
                }
                if (this.objectList.IndexOf(display) != -1)
                {
                    this.objectList.Remove(display);
                    display.Dispose();
                    return true;
                }
                display.Dispose();
            }
            return false;
        }

        public void RemoveWhiteEffect()
        {
            foreach (MonsterDisplay display in this._monsterList)
            {
                display.RemoveWhiteColor();
            }
            IEnumerator<PlayerDisplay> enumerator = this._playerList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.RemoveWhiteColor();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void ShowWhiteEffect()
        {
            foreach (MonsterDisplay display in this._monsterList)
            {
                display.ShowWhiteColor();
            }
            IEnumerator<PlayerDisplay> enumerator = this._playerList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    enumerator.Current.ShowWhiteColor();
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public virtual void stopAutoRoad()
        {
        }

        internal void stopRender()
        {
            IEnumerator<BaseDisplay> enumerator = this.objectList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    BaseDisplay current = enumerator.Current;
                    current.IsUsing = false;
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public virtual void tellServer(float x, float y)
        {
        }

        public CheckVo checkVo
        {
            get
            {
                return this.checker;
            }
        }

        public ClickVo clickVo
        {
            get
            {
                return this.clicker;
            }
        }

        public bool isAllDead
        {
            get
            {
                if (Singleton<MonsterMgr>.Instance.GetNumOfMonster(0, false, false, false) > 0)
                {
                    return false;
                }
                if ((this._monsterList != null) && (this._monsterList.Count > 0))
                {
                    foreach (MonsterDisplay display in this._monsterList)
                    {
                        MonsterVo meVoByType = display.GetMeVoByType<MonsterVo>();
                        if (((meVoByType != null) && (meVoByType.MonsterVO.type != 2)) && ((display.GetVo() as BaseRoleVo).CurHp > 0))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        public MapParserII mapParserII
        {
            get
            {
                return this.parserII;
            }
        }

        public MeDisplay me
        {
            get
            {
                if (this.IsEditor)
                {
                    return this.EditorMe;
                }
                return this._me;
            }
            set
            {
                this._me = value;
            }
        }

        public IList<MonsterDisplay> monsterList
        {
            get
            {
                return this._monsterList;
            }
        }

        public List<PlayerDisplay> myPartnerList
        {
            get
            {
                return this._myPartnerList;
            }
        }

        public List<NpcDisplay> NpcDisplayList
        {
            get
            {
                return this._npcList;
            }
        }

        public IList<PlayerDisplay> playerList
        {
            get
            {
                return this._playerList;
            }
        }
    }
}

