﻿namespace com.u3d.bases.map
{
    using com.bases.utils;
    using com.game.basic.events;
    using com.game.data;
    using com.game.manager;
    using com.game.module.hud;
    using com.game.module.map;
    using com.game.module.map.Data;
    using com.game.module.map.NPC;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.effect;
    using com.u3d.bases.display.vo;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Events;

    public class GameMap : BaseMap
    {
        private List<PlayerDisplay> _selfplayerList = new List<PlayerDisplay>();
        public com.u3d.bases.display.effect.MapPointDisplay MapPointDisplay;

        public void AddMonster(MonsterDisplay display)
        {
            base._monsterList.Add(display);
        }

        public void ClearSelfDisplays()
        {
            if (this._selfplayerList.Count > 0)
            {
                foreach (PlayerDisplay display in this._selfplayerList)
                {
                    if (display.GoBase != null)
                    {
                        base.remove(display);
                    }
                }
                this._selfplayerList.Clear();
            }
        }

        public CopyPointDisplay CreateCopyPoint(DisplayVo vo)
        {
            if (vo == null)
            {
                return null;
            }
            vo.Type = 600;
            CopyPointDisplay item = new CopyPointDisplay();
            base.objectList.Add(item);
            item.SetVo(vo);
            return item;
        }

        public void CreateEditorMe(PlayerVo vo)
        {
            if (vo != null)
            {
                vo.Type = 100;
                base.EditorMe = new MeDisplay();
                vo.SubType = 1;
                object[] objArray1 = new object[] { "Model/Role/", vo.generalTemplateInfo.model_id, "/Model/", vo.generalTemplateInfo.model_id, ".assetbundle" };
                vo.ClothUrl = string.Concat(objArray1);
                base.EditorMe.SetVo(vo);
            }
        }

        public com.u3d.bases.display.effect.MapPointDisplay CreateMapPoint(DisplayVo vo)
        {
            if (vo == null)
            {
                return null;
            }
            vo.Type = 500;
            com.u3d.bases.display.effect.MapPointDisplay item = new com.u3d.bases.display.effect.MapPointDisplay();
            base.objectList.Add(item);
            item.SetVo(vo);
            this.MapPointDisplay = item;
            return item;
        }

        public void CreateMe(PlayerVo vo)
        {
            if (vo != null)
            {
                vo.Type = 100;
                uint type = Convert.ToUInt32(vo.generalTemplateInfo.model_id);
                SysModelVo modelVo = BaseDataMgr.instance.GetModelVo(type);
                if (modelVo != null)
                {
                    vo.scale = MathUtils.GetVector3(modelVo.scale);
                }
                base._me = new MeDisplay();
                this.SelfplayerList.Add(base._me);
                if (BaseDataMgr.instance.GetModelVo((uint) vo.generalTemplateInfo.model_id) != null)
                {
                    vo.isChangeModleTransform = true;
                    vo.scale = MathUtils.GetVector3(BaseDataMgr.instance.GetModelVo((uint) vo.generalTemplateInfo.model_id).scale);
                }
                object[] objArray1 = new object[] { "Model/Role/", vo.generalTemplateInfo.model_id, "/Model/", vo.generalTemplateInfo.model_id, ".assetbundle" };
                vo.ClothUrl = string.Concat(objArray1);
                base._me.SetVo(vo);
                base._playerList.Add(base._me);
            }
        }

        public void CreateMeInCorp(PlayerVo vo, bool onFight = false)
        {
            if (vo != null)
            {
                vo.Type = 100;
                MeDisplay item = new MeDisplay();
                this.SelfplayerList.Add(item);
                vo.SubType = 1;
                if (BaseDataMgr.instance.GetModelVo((uint) vo.generalTemplateInfo.model_id) != null)
                {
                    vo.isChangeModleTransform = true;
                    vo.scale = MathUtils.GetVector3(BaseDataMgr.instance.GetModelVo((uint) vo.generalTemplateInfo.model_id).scale);
                }
                object[] objArray1 = new object[] { "Model/Role/", vo.generalTemplateInfo.model_id, "/Model/", vo.generalTemplateInfo.model_id, ".assetbundle" };
                vo.ClothUrl = string.Concat(objArray1);
                if (onFight)
                {
                    base._me = item;
                }
                item.SetVo(vo);
                base._playerList.Add(item);
            }
        }

        public MonsterDisplay CreateMonster(DisplayVo vo)
        {
            if (vo == null)
            {
                return null;
            }
            vo.Type = 400;
            MonsterDisplay item = new MonsterDisplay();
            base.objectList.Add(item);
            MonsterVo vo2 = vo as MonsterVo;
            SysModelVo modelVo = BaseDataMgr.instance.GetModelVo((uint) vo2.MonsterVO.res);
            if (modelVo != null)
            {
                vo.boundDistance = modelVo.boundDistance * 0.001f;
            }
            switch (vo2.MonsterVO.resType)
            {
                case 0:
                    vo.ClothUrl = "Model/Monster/" + vo2.MonsterVO.res + "/Model/BIP.assetbundle";
                    break;

                case 1:
                {
                    object[] objArray1 = new object[] { "Model/Role/", vo2.MonsterVO.res, "/Model/", vo2.MonsterVO.res, ".assetbundle" };
                    vo.ClothUrl = string.Concat(objArray1);
                    break;
                }
            }
            vo.scale = MathUtils.GetVector3(vo2.MonsterVO.scale);
            vo.isChangeModleTransform = true;
            item.SetVo(vo);
            return item;
        }

        public void CreateNpc(uint npcId)
        {
            SysNpcVo npcVoById = BaseDataMgr.instance.GetNpcVoById(npcId);
            BaseSceneNPCInfo sceneNPCInfo = new BaseSceneNPCInfo {
                modelId = npcVoById.model_id
            };
            sceneNPCInfo.SetTemplateId((uint) npcVoById.npcId);
            sceneNPCInfo.SetObjId(npcId);
            Vector3 vector = (Vector3) (com.game.utils.StringUtils.StringToVector3(npcVoById.position) * 0.001f);
            sceneNPCInfo.scenePos = vector;
            sceneNPCInfo.rotation = new Vector3(0f, (float) (npcVoById.yRotation / 0x3e8), 0f);
            BaseSceneNPC item = new BaseSceneNPC(sceneNPCInfo);
            item.AddEventListener("characterCreateComplete", new UnityAction<EventObject>(this.CreateNPCComplete));
            base._npcObjectList.Add(item);
        }

        private void CreateNPCComplete(EventObject evt)
        {
            BaseSceneNPC target = evt.Target as BaseSceneNPC;
            GameObject obj2 = NGUITools.AddChild(target.character.CharacterModel);
            obj2.transform.localPosition = new Vector3(0f, 1.8f, 0f);
            GameObject speakView = HudView.Instance.AddItem("msg", obj2.transform, HudView.Type.Monster);
            target.InitSpeakView(speakView);
            target.SpeakWord(target.GetSceneNPCInfo().npcTemplate.descript);
            HudView.Instance.AddItem(target.GetSceneNPCInfo().npcTemplate.name, obj2.transform, HudView.Type.Npc);
        }

        public PlayerDisplay CreatePlayer(PlayerVo vo)
        {
            uint generalUniKey = GlobalData.GetGeneralUniKey(vo.stylebin.id, vo.stylebin.quality);
            vo.Type = 100;
            vo.SubType = 2;
            SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo((uint) vo.stylebin.id, vo.stylebin.quality);
            vo.generalTemplateInfo = generalVo;
            PlayerDisplay item = new PlayerDisplay();
            base.objectList.Add(item);
            base._playerList.Add(item);
            vo.uniKeyId = generalUniKey;
            vo.generalList.Add(generalUniKey, generalVo.model_id);
            if (BaseDataMgr.instance.GetModelVo((uint) vo.generalTemplateInfo.model_id) != null)
            {
                vo.scale = MathUtils.GetVector3(BaseDataMgr.instance.GetModelVo((uint) vo.generalTemplateInfo.model_id).scale);
                vo.isChangeModleTransform = true;
            }
            object[] objArray1 = new object[] { "Model/Role/", vo.generalTemplateInfo.model_id, "/Model/", vo.generalTemplateInfo.model_id, ".assetbundle" };
            vo.ClothUrl = string.Concat(objArray1);
            item.SetVo(vo);
            return item;
        }

        public TrapDisplay CreateTrap(DisplayVo vo)
        {
            if (vo == null)
            {
                return null;
            }
            vo.Type = 0x1f5;
            TrapDisplay item = new TrapDisplay();
            base.objectList.Add(item);
            SysTrap trapVoById = BaseDataMgr.instance.GetTrapVoById((uint) vo.Id);
            vo.ClothUrl = "Model/Trap/" + trapVoById.Model + "/Model/BIP.assetbundle";
            item.SetVo(vo);
            return item;
        }

        public com.u3d.bases.display.effect.MapPointDisplay CreateWorldMapPoint(DisplayVo vo)
        {
            if (vo == null)
            {
                return null;
            }
            vo.Type = 500;
            com.u3d.bases.display.effect.MapPointDisplay item = new com.u3d.bases.display.effect.MapPointDisplay();
            base.objectList.Add(item);
            item.SetVo(vo);
            return item;
        }

        public CopyPointDisplay GetCopyPoint(int copyGroupId)
        {
            <GetCopyPoint>c__AnonStoreyCC ycc = new <GetCopyPoint>c__AnonStoreyCC {
                copyGroupId = copyGroupId
            };
            if (ycc.copyGroupId < 1)
            {
                return null;
            }
            return base.objectList.OfType<CopyPointDisplay>().FirstOrDefault<CopyPointDisplay>(new Func<CopyPointDisplay, bool>(ycc.<>m__2A));
        }

        public CopyPointDisplay GetCopyPoint(string id)
        {
            return (CopyPointDisplay) this.GetDisplay(id, 600);
        }

        internal BaseDisplay GetDisplay(string key)
        {
            <GetDisplay>c__AnonStoreyCD ycd = new <GetDisplay>c__AnonStoreyCD {
                key = key
            };
            if (com.bases.utils.StringUtils.isEmpty(ycd.key))
            {
                return null;
            }
            return base.objectList.FirstOrDefault<BaseDisplay>(new Func<BaseDisplay, bool>(ycd.<>m__2B));
        }

        private BaseDisplay GetDisplay(string id, int type)
        {
            if (com.bases.utils.StringUtils.isEmpty(id))
            {
                return null;
            }
            return this.GetDisplay(type + "_" + id);
        }

        public com.u3d.bases.display.effect.MapPointDisplay GetMapPoint(string id)
        {
            return (com.u3d.bases.display.effect.MapPointDisplay) this.GetDisplay(id, 500);
        }

        public List<MonsterDisplay> GetMonster()
        {
            return base._monsterList;
        }

        public MonsterDisplay GetMonster(string id)
        {
            return (MonsterDisplay) this.GetDisplay(id, 400);
        }

        public BaseSceneNPC GetNpcById(uint id)
        {
            <GetNpcById>c__AnonStoreyCB ycb = new <GetNpcById>c__AnonStoreyCB {
                id = id
            };
            return base._npcObjectList.FirstOrDefault<BaseSceneNPC>(new Func<BaseSceneNPC, bool>(ycb.<>m__29));
        }

        public BaseSceneNPC GetNpcByModuleId(int moduleId)
        {
            foreach (BaseSceneNPC enpc in base._npcObjectList)
            {
                if (enpc.GetBaseSceneObjInfo().OpenModuleId == moduleId)
                {
                    return enpc;
                }
            }
            return null;
        }

        public int GetNumOfMonster(uint monsterId, bool includeDead = false, bool includeStaticBox = false, bool includeMySide = false)
        {
            int num = 0;
            IEnumerator<MonsterDisplay> enumerator = base.monsterList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    MonsterDisplay current = enumerator.Current;
                    MonsterVo meVoByType = current.GetMeVoByType<MonsterVo>();
                    if ((((meVoByType != null) && (includeStaticBox || (meVoByType.MonsterVO.type != 2))) && (includeDead || (((current.GetVo() as BaseRoleVo).CurHp > 0) && ((monsterId == 0) || (meVoByType.MonsterVO.id == monsterId))))) && (includeMySide || !meVoByType.isMySide))
                    {
                        num++;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return num;
        }

        public PlayerDisplay GetPlayer(string id)
        {
            if ((base.me != null) && com.bases.utils.StringUtils.isEqualsExcFree(base.me.GetVo().Id.ToString(CultureInfo.InvariantCulture), id))
            {
                return base.me;
            }
            return (PlayerDisplay) this.GetDisplay(id, 100);
        }

        public void HideOtherPlayer()
        {
            IEnumerator<PlayerDisplay> enumerator = base._playerList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    if (current.GetVo().Id != base.me.GetVo().Id)
                    {
                        current.GoBase.SetActive(false);
                        current.Controller.GoName.SetActive(false);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        private void LoadAllModelCallBack(BaseDisplay display)
        {
            SysGeneralVo generalVo = BaseDataMgr.instance.GetGeneralVo((uint) display.GetVo().stylebin.id, display.GetVo().stylebin.quality);
            display.SetCurrentFightGeneralModeActive((uint) generalVo.model_id);
        }

        public bool Remove(string id, int type)
        {
            return base.remove(this.GetDisplay(id, type));
        }

        public void SetMonsterAnimationSpeed(float speed)
        {
            foreach (MonsterDisplay display in base._monsterList)
            {
                display.Animator.speed = speed;
            }
        }

        public void ShowOtherPlayer()
        {
            IEnumerator<PlayerDisplay> enumerator = base._playerList.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    PlayerDisplay current = enumerator.Current;
                    if (current.GetVo().Id != base.me.GetVo().Id)
                    {
                        current.GoBase.SetActive(true);
                        current.Controller.GoName.SetActive(true);
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
        }

        public void StartAllMonstersAi()
        {
            MonsterMgr.CanSetAi = true;
        }

        public void StopAllMonstersAi()
        {
            MonsterMgr.CanSetAi = false;
        }

        public int MonsterNumber
        {
            get
            {
                return base._monsterList.Count;
            }
        }

        public List<PlayerDisplay> SelfplayerList
        {
            get
            {
                return this._selfplayerList;
            }
            set
            {
                this._selfplayerList = value;
            }
        }

        [CompilerGenerated]
        private sealed class <GetCopyPoint>c__AnonStoreyCC
        {
            internal int copyGroupId;

            internal bool <>m__2A(CopyPointDisplay copyPoint)
            {
                return (copyPoint.copyGroupId == this.copyGroupId);
            }
        }

        [CompilerGenerated]
        private sealed class <GetDisplay>c__AnonStoreyCD
        {
            internal string key;

            internal bool <>m__2B(BaseDisplay display)
            {
                return display.Key.Equals(this.key);
            }
        }

        [CompilerGenerated]
        private sealed class <GetNpcById>c__AnonStoreyCB
        {
            internal uint id;

            internal bool <>m__29(BaseSceneNPC npcObject)
            {
                return (npcObject.GetBaseSceneObjInfo().GetObjId() == this.id);
            }
        }
    }
}

