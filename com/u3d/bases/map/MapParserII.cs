﻿namespace com.u3d.bases.map
{
    using com.game;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.map;
    using com.game.utils;
    using com.game.vo;
    using com.u3d.bases.loader;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class MapParserII
    {
        private bool _isChange = true;
        private ILoadingBar _loadingBar;
        private readonly BaseMap _map;
        private MapCallBack _mapCallBack;
        private uint _mapId;
        private float _transPosX;
        private float _transPosY;
        private float _transPosZ;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map3C;
        public List<GameObject> boxList = new List<GameObject>();
        public int lightMapCount;
        private List<Texture2D> lightmaps;
        public SysSceneVo MapVo;
        private GameObject prevMap;
        public SysSceneResourceVo resourceVo;
        private SysSceneVo sceneVo;

        public MapParserII(BaseMap map)
        {
            this._map = map;
        }

        public void AddLoadingBar(ILoadingBar mapLoadingBar)
        {
            this._loadingBar = mapLoadingBar;
        }

        public void AddMapCallback(MapCallBack mapLoadCallBack)
        {
            this._mapCallBack = mapLoadCallBack;
        }

        public float GetFinalMonsterX(float curX)
        {
            return curX;
        }

        public float GetFinalPlayerX(float curX)
        {
            return curX;
        }

        public void LightmapComplete(Texture2D textrue)
        {
            if (textrue != null)
            {
                this.lightmaps.Add(textrue);
                StartLoadingView instance = Singleton<StartLoadingView>.Instance;
                instance.PreLoadedNum++;
                if (this.lightmaps.Count >= this.lightMapCount)
                {
                    AssetManager.Instance.LoadScenePathFinding(this.resourceVo.find_path.ToString());
                }
            }
        }

        public void LoadMapStarData(TextAsset txtAsset)
        {
            if (txtAsset != null)
            {
                StartLoadingView instance = Singleton<StartLoadingView>.Instance;
                instance.PreLoadedNum++;
                AstarPath.active.astarData.DeserializeGraphs(txtAsset.bytes);
                AssetManager.Instance.LoadRenderSeting(this.resourceVo.fog_id.ToString());
            }
        }

        public void LoadRenderSetingComplete(TextAsset txtAsset)
        {
            if (txtAsset != null)
            {
                StartLoadingView instance = Singleton<StartLoadingView>.Instance;
                instance.PreLoadedNum++;
                IEnumerator enumerator = XMLParser.Parse(txtAsset.text).GetNodeList("Fog>0>item").GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        XMLNode current = (XMLNode) enumerator.Current;
                        string str = current.GetValue("@name");
                        string str2 = current.GetValue("_text");
                        string key = str;
                        if (key != null)
                        {
                            int num;
                            if (<>f__switch$map3C == null)
                            {
                                Dictionary<string, int> dictionary = new Dictionary<string, int>(6);
                                dictionary.Add("fog", 0);
                                dictionary.Add("fogColor", 1);
                                dictionary.Add("fogMode", 2);
                                dictionary.Add("fogDensity", 3);
                                dictionary.Add("fogStart", 4);
                                dictionary.Add("fogEnd", 5);
                                <>f__switch$map3C = dictionary;
                            }
                            if (<>f__switch$map3C.TryGetValue(key, out num))
                            {
                                switch (num)
                                {
                                    case 0:
                                        RenderSettings.fog = str2 == "1";
                                        break;

                                    case 1:
                                    {
                                        string[] valueListFromString = StringUtils.GetValueListFromString(str2, ',');
                                        Color color = new Color {
                                            r = float.Parse(valueListFromString[0]),
                                            g = float.Parse(valueListFromString[1]),
                                            b = float.Parse(valueListFromString[2]),
                                            a = float.Parse(valueListFromString[3])
                                        };
                                        RenderSettings.fogColor = color;
                                        break;
                                    }
                                    case 2:
                                        RenderSettings.fogMode = (UnityEngine.FogMode) int.Parse(str2);
                                        break;

                                    case 3:
                                        RenderSettings.fogDensity = float.Parse(str2);
                                        break;

                                    case 4:
                                        RenderSettings.fogStartDistance = float.Parse(str2);
                                        break;

                                    case 5:
                                        RenderSettings.fogEndDistance = float.Parse(str2);
                                        break;
                                }
                            }
                        }
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable == null)
                    {
                    }
                    disposable.Dispose();
                }
            }
            else
            {
                RenderSettings.fog = false;
            }
            AssetManager.Instance.LoadSceneResource(this.resourceVo.resource_id.ToString());
        }

        public void LoadSceneBack(GameObject obj)
        {
            if (this.prevMap != null)
            {
                UnityEngine.Object.Destroy(this.prevMap);
                this.prevMap = null;
            }
            StartLoadingView instance = Singleton<StartLoadingView>.Instance;
            instance.PreLoadedNum++;
            if (this.lightmaps.Count > 0)
            {
                LightmapData[] dataArray = new LightmapData[this.lightmaps.Count];
                for (int i = 0; i < this.lightmaps.Count; i++)
                {
                    dataArray[i] = new LightmapData { lightmapFar = this.lightmaps[i] };
                }
                LightmapSettings.lightmaps = dataArray;
            }
            this.prevMap = UnityEngine.Object.Instantiate(obj) as GameObject;
            this.prevMap.transform.position = new Vector3(0f, 0f, 0f);
            StaticBatchingUtility.Combine(this.prevMap);
            if (Camera.main.GetComponent<Skybox>().material == null)
            {
                if (this.resourceVo.sky_box != "-1")
                {
                    AssetManager.Instance.LoadSkyBox(this.resourceVo.sky_box);
                }
                else if (this._mapCallBack != null)
                {
                    this._mapCallBack(this.MapId);
                }
            }
            else if (Camera.main.GetComponent<Skybox>().material.name != this.resourceVo.sky_box)
            {
                AssetManager.Instance.LoadSkyBox(this.resourceVo.sky_box);
            }
            else if (this._mapCallBack != null)
            {
                this._mapCallBack(this.MapId);
            }
            this.ShowNPC();
        }

        public void LoadSkyBoxComplete(Material skyBoxMaterial)
        {
            Camera.main.GetComponent<Skybox>().material = skyBoxMaterial;
            if (this._mapCallBack != null)
            {
                this._mapCallBack(this.MapId);
            }
        }

        public void PlayBossDeathScript(GameObject oj, int cameraScriptId)
        {
            MapCameraScriptData cameraData = new MapCameraScriptData(cameraScriptId);
            Camera.main.gameObject.AddMissingComponent<MouseOrbit>().SetPositionInTime(oj.transform.position, cameraData);
            Time.timeScale = 0.2f;
        }

        private void readyLoadMap()
        {
            if (this.lightmaps == null)
            {
                this.lightmaps = new List<Texture2D>();
            }
            this.lightmaps.Clear();
            this.resourceVo = BaseDataMgr.instance.getSceneResourceVo((uint) this.MapVo.resource_id);
            string[] valueListFromString = StringUtils.GetValueListFromString(this.resourceVo.lightmap_id, ',');
            this.lightMapCount = valueListFromString.Length;
            int num = valueListFromString.Length + 2;
            StartLoadingView instance = Singleton<StartLoadingView>.Instance;
            instance.PreLoadNum += num;
            AssetManager.Instance.LoadSceneLightMapFar(valueListFromString);
        }

        private void ShowNPC()
        {
            char[] separator = new char[] { ',' };
            string[] strArray = StringUtils.GetValueString(this.sceneVo.npcId).Split(separator);
            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] != string.Empty)
                {
                    AppMap.Instance.CreateNpc(uint.Parse(strArray[i]));
                }
            }
        }

        public void StartLoadMap(uint mapId)
        {
            this.IsChange = true;
            this.MapId = mapId;
            this.sceneVo = BaseDataMgr.instance.GetSceneVo(mapId);
            this._map.stopRender();
            Singleton<StartLoadingView>.Instance.loaderList.Clear();
            this.readyLoadMap();
            if (this.sceneVo.type != 1)
            {
                AssetManager.Instance.PreLoadEffect();
            }
        }

        public bool IsChange
        {
            get
            {
                return this._isChange;
            }
            set
            {
                this._isChange = value;
                if (this._isChange)
                {
                    MeVo.instance.IsUnbeatable = true;
                }
                else
                {
                    MeVo.instance.IsUnbeatable = false;
                }
            }
        }

        public uint MapId
        {
            get
            {
                return this._mapId;
            }
            set
            {
                this._mapId = value;
                this.MapVo = BaseDataMgr.instance.GetSceneVo(this._mapId);
            }
        }

        public bool NeedSyn
        {
            get
            {
                if (AppMap.Instance.IsEditor)
                {
                    return false;
                }
                this.sceneVo = BaseDataMgr.instance.GetSceneVo(this._mapId);
                return this.sceneVo.need_sync;
            }
        }

        public float TransPosX
        {
            get
            {
                return this._transPosX;
            }
            set
            {
                this._transPosX = value;
            }
        }

        public float TransPosY
        {
            get
            {
                return this._transPosY;
            }
            set
            {
                this._transPosY = value;
            }
        }

        public float TransPosZ
        {
            get
            {
                return this._transPosZ;
            }
            set
            {
                this._transPosZ = value;
            }
        }
    }
}

