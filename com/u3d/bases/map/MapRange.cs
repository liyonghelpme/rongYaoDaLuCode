﻿namespace com.u3d.bases.map
{
    using System;

    public class MapRange
    {
        public float MaxX;
        public float MaxY;
        public float MinX;
        public float MinY;
    }
}

