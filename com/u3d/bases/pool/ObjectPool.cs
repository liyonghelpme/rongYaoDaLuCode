﻿namespace com.u3d.bases.pool
{
    using com.u3d.bases.interfaces;
    using System;
    using System.Collections.Generic;

    public class ObjectPool
    {
        public string className;
        public IList<IPoolable> list;

        public ObjectPool(string className)
        {
            this.className = className;
            this.list = new List<IPoolable>();
        }

        public IPoolable get()
        {
            if (this.list.Count < 1)
            {
                return (IPoolable) Activator.CreateInstance(Type.GetType(this.className));
            }
            IPoolable poolable = this.list[0];
            this.list.RemoveAt(0);
            return poolable;
        }

        public void put(IPoolable obj)
        {
            if (obj != null)
            {
                this.list.Add(obj);
            }
        }
    }
}

