﻿namespace com.u3d.bases.pool
{
    using com.bases.utils;
    using com.u3d.bases.interfaces;
    using System;
    using System.Collections.Generic;

    public class ObjectPoolMgr
    {
        private static Dictionary<string, ObjectPool> pool = new Dictionary<string, ObjectPool>();

        public static IPoolable get(string className)
        {
            IPoolable poolable = getPool(className).get();
            poolable.getBefore();
            return poolable;
        }

        private static ObjectPool getPool(string className)
        {
            ObjectPool pool = !ObjectPoolMgr.pool.ContainsKey(className) ? null : ObjectPoolMgr.pool[className];
            if (pool == null)
            {
                pool = new ObjectPool(className);
                ObjectPoolMgr.pool[className] = pool;
            }
            return pool;
        }

        public static void put(string className, IPoolable poolable)
        {
            if ((poolable != null) && !StringUtils.isEmpty(className))
            {
                getPool(className).put(poolable);
            }
        }
    }
}

