﻿namespace com.u3d.bases.skills
{
    using com.game;
    using com.game.consts;
    using com.game.data;
    using com.game.manager;
    using com.game.module.core;
    using com.game.module.fight.arpg;
    using com.game.module.fight.vo;
    using com.game.module.map;
    using com.game.module.Role;
    using com.game.utils;
    using com.game.vo;
    using com.liyong;
    using com.u3d.bases.ai;
    using com.u3d.bases.consts;
    using com.u3d.bases.controller;
    using com.u3d.bases.display;
    using com.u3d.bases.display.character;
    using com.u3d.bases.display.controler;
    using com.u3d.bases.fsmUtil;
    using PCustomDataType;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityLog;

    public static class SkillUtil
    {
        [CompilerGenerated]
        private static MoveOperation <>f__am$cache0;
        [CompilerGenerated]
        private static StartCallback <>f__am$cache1;
        [CompilerGenerated]
        private static EndCallback <>f__am$cache2;
        [CompilerGenerated]
        private static ReplaceOperation <>f__am$cache3;
        [CompilerGenerated]
        private static ReplaceOperation <>f__am$cache4;
        [CompilerGenerated]
        private static StartCallback <>f__am$cache5;
        [CompilerGenerated]
        private static ReplaceOperation <>f__am$cache6;
        [CompilerGenerated]
        private static Func<string, string[]> <>f__am$cache7;
        public const int RESOTRE_SHADER_NEED_FRAMES = 10;

        public static void AddActivation(ActionControler controler, SysSkillBaseVo skillBaseVo)
        {
            string str = skillBaseVo.param_list;
            if (str.Length >= "[1,2]|[[3,4]]".Length)
            {
                char[] trimChars = new char[] { ' ' };
                char[] separator = new char[] { '|' };
                string[] strArray = str.Trim(trimChars).Split(separator);
                if (strArray.Length >= 2)
                {
                    int[] arrayStringToInt = StringUtils.GetArrayStringToInt(strArray[0]);
                    if (arrayStringToInt.Length >= 2)
                    {
                        ulong id = controler.GetMeVo().Id;
                        List<PDamageBuff> additionBuffList = controler.SkillController.AdditionBuffList;
                        additionBuffList.Clear();
                        int[] numArray2 = StringUtils.GetArrayStringToInt(strArray[1]);
                        for (int i = 0; i < numArray2.Length; i += 2)
                        {
                            PDamageBuff item = new PDamageBuff {
                                attackerId = id,
                                attackerType = 2,
                                buffId = Convert.ToUInt32(numArray2[i]),
                                lvl = Convert.ToByte(numArray2[i + 1])
                            };
                            additionBuffList.Add(item);
                        }
                        uint buffId = Convert.ToUInt32(arrayStringToInt[0]);
                        uint buffLvl = Convert.ToUInt32(arrayStringToInt[1]);
                        controler.buffController.AddBuff(buffId, buffLvl, controler.GetMeByType<ActionDisplay>());
                    }
                }
            }
        }

        public static void AddExtendBulletBuff(ActionControler controler, int buffId, int buffLevel)
        {
            if (controler.GetMeVoByType<BaseRoleVo>().Camp == 0)
            {
                controler.buffController.AddBuff(buffId, buffLevel, null);
            }
        }

        public static void CancelExtendBullet(ActionControler controler)
        {
            controler.ExtendedBulletFlyDistance = 0f;
        }

        private static void ChangeCamp(ActionDisplay dis, ActionDisplay attacker)
        {
            if (AppMap.Instance.IsInWifiPVP)
            {
                if (dis is MeDisplay)
                {
                    SetCamp(dis, GameConst.CampType.enemyside);
                }
                else if (dis is PlayerDisplay)
                {
                    SetCamp(dis, GameConst.CampType.myside);
                }
                else if (dis is MonsterDisplay)
                {
                    switch (dis.GetMeVoByType<MonsterVo>().MonsterVO.type)
                    {
                        case 3:
                        case 5:
                            return;
                    }
                    if (attacker == null)
                    {
                        return;
                    }
                    uint camp = attacker.GetMeVoByType<BaseRoleVo>().Camp;
                    SetCamp(dis, (GameConst.CampType) camp);
                }
            }
            else if (AppMap.Instance.IsInArena)
            {
                if (dis is MeDisplay)
                {
                    SetCamp(dis, GameConst.CampType.enemyside);
                }
                else if (dis is PlayerDisplay)
                {
                    SetCamp(dis, GameConst.CampType.myside);
                }
            }
            else if (GlobalData.isInCopy)
            {
                if (dis is MonsterDisplay)
                {
                    SetCamp(dis, GameConst.CampType.myside);
                }
                else if (dis is MeDisplay)
                {
                    SetCamp(dis, GameConst.CampType.pve_monster);
                }
            }
            RefreshAI(dis);
        }

        public static bool CheckIsBullect(SysSkillBaseVo vo)
        {
            SysSkillActionVo dataByTypeAndId = BaseDataMgr.instance.GetDataByTypeAndId<SysSkillActionVo>("SysSkillActionVo", vo.skill_group);
            return ((dataByTypeAndId != null) && dataByTypeAndId.IsBullet);
        }

        public static int CullingBladeCheck(SysSkillBaseVo skillBaseVo, BaseRoleVo targetVo, int damageVal)
        {
            if (skillBaseVo.subtype == 11)
            {
                int[] arrayStringToInt = StringUtils.GetArrayStringToInt(skillBaseVo.param_list);
                if (arrayStringToInt.Length < 2)
                {
                    return damageVal;
                }
                if ((targetVo is MonsterVo) && ShouldBlockThisSkillSubtype(11, (MonsterVo) targetVo))
                {
                    return damageVal;
                }
                int num = Convert.ToInt32(targetVo.CurHp);
                switch (arrayStringToInt[0])
                {
                    case 0:
                        return damageVal;

                    case 1:
                        if (targetVo.DeltaToEmptyHp > Convert.ToUInt32(arrayStringToInt[1]))
                        {
                            return damageVal;
                        }
                        return num;

                    case 2:
                    {
                        float num2 = arrayStringToInt[1] * 0.001f;
                        if (targetVo.CurHpPercent > num2)
                        {
                            return damageVal;
                        }
                        return num;
                    }
                }
            }
            return damageVal;
        }

        public static void DamageCheckAgain(BaseDisplay attacker, Transform trans, uint skillId, int ratio)
        {
            if ((attacker != null) && (attacker.Controller != null))
            {
                SkillController skillController = attacker.Controller.SkillController;
                SysSkillBaseVo skillVo = GetSkillVo(skillId);
                if ((skillController != null) && (skillVo != null))
                {
                    int num = ratio;
                    skillController.CheckDamage(trans.position, skillVo, 0, null, null, 0, num);
                }
            }
        }

        public static int DamageDecayCheck(SysSkillBaseVo skillVo, int hurtCount, int damageVal)
        {
            if (skillVo.subtype != 0x11)
            {
                return damageVal;
            }
            int[] arrayStringToInt = StringUtils.GetArrayStringToInt(skillVo.param_list);
            if (arrayStringToInt.Length < 4)
            {
                return damageVal;
            }
            float f = arrayStringToInt[3] * 0.001f;
            return Convert.ToInt32((float) (Mathf.Pow(f, (float) hurtCount) * damageVal));
        }

        [DebuggerHidden]
        private static IEnumerator DoBeForcedToAttack(BaseControler meController, ActionDisplay needToAttack)
        {
            return new <DoBeForcedToAttack>c__Iterator58 { meController = meController, needToAttack = needToAttack, <$>meController = meController, <$>needToAttack = needToAttack };
        }

        [DebuggerHidden]
        private static IEnumerator DoColorChange(Material mat, StateVo stateVo)
        {
            return new <DoColorChange>c__Iterator57 { mat = mat, stateVo = stateVo, <$>mat = mat, <$>stateVo = stateVo };
        }

        private static void DoSummon1(BaseRoleVo summonerVo, MonsterVo monsterVo, uint templateId, float radius, int angle, float maxAliveTime)
        {
            Vector3 position = summonerVo.Controller.Me.GoBase.transform.position;
            Vector3 eulerAngles = summonerVo.Controller.transform.rotation.eulerAngles;
            ulong id = summonerVo.Id;
            radius *= UnityEngine.Random.Range((float) 0.5f, (float) 1f);
            angle += MathUtils.Random(-10, 10);
            float y = eulerAngles.y + angle;
            Vector3 point = ((Vector3) ((Quaternion.Euler(eulerAngles.x, y, eulerAngles.z) * Vector3.forward) * radius)) + position;
            if (!Util.GetPointMovable(ref point))
            {
                point = position;
            }
            point.y = position.y + 1f;
            DoSummon2(monsterVo, templateId, point, eulerAngles.y, id, maxAliveTime);
        }

        private static void DoSummon2(MonsterVo monsterVo, uint templateId, Vector3 targetPos, float rotateY, ulong summonerId, float maxAliveTime)
        {
            float num = maxAliveTime;
            PMapMon mon = ToPMapMon(monsterVo, targetPos, rotateY, templateId, summonerId, 0, 0, num);
            List<PMapMon> list = new List<PMapMon> {
                mon
            };
            Singleton<MapMode>.Instance.Summon(summonerId, SkillSynType.Player, list);
        }

        public static void EndActivation(ActionControler controler)
        {
            if (controler != null)
            {
                SkillController skillController = controler.SkillController;
                skillController.IsInActivation = false;
                skillController.AdditionBuffList.Clear();
                skillController.EvaluateFunc = null;
            }
        }

        public static void EndEndured(ActionControler ac)
        {
            MonsterVo meVoByType = ac.Me.GetMeVoByType<MonsterVo>();
            meVoByType.stateInfo.RemoveState(StateVo.ENDURED);
            meVoByType.endure = meVoByType.MonsterVO.endurance_initval;
        }

        public static void EndHpProtected(ActionControler controler)
        {
            controler.GetMeVoByType<BaseRoleVo>().HpProtected = null;
        }

        public static void EndKnockBack(BaseControler controler)
        {
            controler.Me.GetMeVoByType<BaseRoleVo>().stateInfo.RemoveState(StateVo.KNOCK_BACK);
        }

        public static void EndSplit(ActionControler controler)
        {
        }

        public static void EndTauntedAndCancelAttack(ActionControler meControler)
        {
            SkillController skillController = meControler.SkillController;
            GameObject goBase = meControler.Me.GoBase;
            if ((skillController != null) && (goBase != null))
            {
                skillController.IsForcedToAttack = false;
                meControler.GetMeVo().stateInfo.BeTaunted = false;
                meControler.AttackController.ClearAttackList();
                CommandHandler.AddCommandStatic(goBase, "idle", 1f);
            }
        }

        public static void EndTemptation(ActionDisplay targetDisplay)
        {
            RevertTargetCamp(targetDisplay);
        }

        public static void ExtendBullet(ActionControler controler, float distance)
        {
            controler.ExtendedBulletFlyDistance = distance;
        }

        public static int GetBlockedIdFromSkillSubtype(int sub)
        {
            switch (((SkillConst.HeroSkillSubtype) sub))
            {
                case SkillConst.HeroSkillSubtype.culling_blade:
                    return 2;

                case SkillConst.HeroSkillSubtype.temptation:
                    return 1;

                case SkillConst.HeroSkillSubtype.floating:
                    return 3;
            }
            return 0;
        }

        public static Color GetColorFromVector3(Vector3 colorVec, Color originColor)
        {
            return new Color(colorVec.x, colorVec.y, colorVec.z, originColor.a);
        }

        public static Vector3 GetColorVector3(Color c)
        {
            return new Vector3(c.r, c.g, c.b);
        }

        public static float GetFloat(string name)
        {
            return (Singleton<ConfigConst>.Instance.GetConfigData(name) * 0.001f);
        }

        public static float GetLerpFloat(float start, float delta, int nowCount, float totalCount = 10)
        {
            return (start + (delta * ((totalCount - nowCount) / totalCount)));
        }

        public static SysSkillBaseVo GetSkillVo(uint skillId)
        {
            return BaseDataMgr.instance.GetSysSkillBaseVo(skillId);
        }

        public static Vector3 GetVector3(string name)
        {
            return (Vector3) (StringUtils.StringToVector3(Singleton<ConfigConst>.Instance.GetConfigDataString(name)) / 255f);
        }

        public static void Heal(SysSkillBaseVo skillvo, ActionDisplay meDisplay, IEnumerable<ActionDisplay> targetList, bool needSync)
        {
            DamageCheck.AddBuffToMySide(skillvo, meDisplay, targetList);
        }

        private static void RefreshAI(ActionDisplay targetDisplay)
        {
            AiControllerBase aiController = targetDisplay.Controller.AiController;
            if (aiController != null)
            {
                bool isAiEnable = aiController.IsAiEnable;
                aiController.SetAi(false);
                aiController.UpdateCamp();
                aiController.SetAi(isAiEnable);
            }
        }

        private static void RestoreCamp(BaseDisplay display)
        {
            display.GetMeVoByType<BaseRoleVo>().RevertToLastCamp();
        }

        private static void RevertTargetCamp(ActionDisplay targetDisplay)
        {
            RestoreCamp(targetDisplay);
            RefreshAI(targetDisplay);
        }

        private static void SetCamp(BaseDisplay display, GameConst.CampType camp)
        {
            display.GetMeVoByType<BaseRoleVo>().Camp = (uint) camp;
        }

        public static void Shield(SysSkillBaseVo skillvo, ActionDisplay meDisplay, IEnumerable<ActionDisplay> targetList, bool needSync)
        {
            <Shield>c__AnonStorey121 storey = new <Shield>c__AnonStorey121 {
                meDisplay = meDisplay
            };
            targetList = targetList.OrderBy<ActionDisplay, float>(new Func<ActionDisplay, float>(storey.<>m__1E5)).ToList<ActionDisplay>();
            BaseRoleVo meVoByType = storey.meDisplay.GetMeVoByType<BaseRoleVo>();
            string str = skillvo.param_list;
            if (str.Length < "[1,2,3]".Length)
            {
                DamageCheck.AddBuffToMySide(skillvo, storey.meDisplay, targetList);
            }
            else
            {
                int[] arrayStringToInt = StringUtils.GetArrayStringToInt(str);
                if (arrayStringToInt[0] != 0)
                {
                    int buffId = arrayStringToInt[1];
                    int buffLevel = arrayStringToInt[2];
                    Transform ownerTransform = storey.meDisplay.GoBase.transform;
                    int num3 = skillvo.target_num;
                    IEnumerator<ActionDisplay> enumerator = targetList.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            ActionDisplay current = enumerator.Current;
                            GameObject goBase = current.GoBase;
                            if (goBase != null)
                            {
                                float boundDistance = current.BoundDistance;
                                if (DamageCheck.IsSkillCovered3D(skillvo, ownerTransform, goBase.transform.position, boundDistance) && (num3 > 0))
                                {
                                    num3--;
                                    uint recoverHp = 0;
                                    BaseRoleVo der = current.GetMeVoByType<BaseRoleVo>();
                                    GameFormula.CalcRecoverHp(meVoByType, der, skillvo, out recoverHp);
                                    current.Controller.buffController.AddBuffWithAbortDamage(buffId, buffLevel, recoverHp);
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                }
            }
        }

        public static bool ShouldBlockThisSkillSubtype(SysSkillBaseVo skillvo, MonsterVo vo)
        {
            ListDictionary arrayStringToIntFaster = StringUtils.GetArrayStringToIntFaster(vo.MonsterVO.blocked_skill_subtype);
            if (arrayStringToIntFaster.Count == 0)
            {
                return false;
            }
            int blockedIdFromSkillSubtype = GetBlockedIdFromSkillSubtype(skillvo.subtype);
            return arrayStringToIntFaster.Contains(blockedIdFromSkillSubtype);
        }

        public static bool ShouldBlockThisSkillSubtype(SysSkillBaseVo skillvo, BaseDisplay ad)
        {
            if (ad is MonsterDisplay)
            {
                MonsterVo meVoByType = ad.GetMeVoByType<MonsterVo>();
                return ShouldBlockThisSkillSubtype(skillvo, meVoByType);
            }
            return false;
        }

        public static bool ShouldBlockThisSkillSubtype(int subtype, MonsterVo vo)
        {
            ListDictionary arrayStringToIntFaster = StringUtils.GetArrayStringToIntFaster(vo.MonsterVO.blocked_skill_subtype);
            if (arrayStringToIntFaster.Count == 0)
            {
                return false;
            }
            int blockedIdFromSkillSubtype = GetBlockedIdFromSkillSubtype(subtype);
            return arrayStringToIntFaster.Contains(blockedIdFromSkillSubtype);
        }

        public static bool ShouldBlockThisSkillSubtype(int subtype, BaseDisplay ad)
        {
            if (ad is MonsterDisplay)
            {
                MonsterVo meVoByType = ad.GetMeVoByType<MonsterVo>();
                return ShouldBlockThisSkillSubtype(subtype, meVoByType);
            }
            return false;
        }

        public static void StartActivation(ActionControler controler)
        {
            if (controler != null)
            {
                controler.SkillController.IsInActivation = true;
                controler.SkillController.EvaluateFunc = new Predicate<SysSkillBaseVo>(SkillUtil.CheckIsBullect);
            }
        }

        public static void StartAttrTuring(ActionControler controler, SysSkillBaseVo skillBaseVo)
        {
            char[] trimChars = new char[] { ' ' };
            char[] separator = new char[] { '|' };
            string[] strArray = skillBaseVo.param_list.Trim(trimChars).Split(separator);
            string str = strArray[0];
            string str2 = strArray[1];
            if (str.Length >= "[0,1,2,3,4]".Length)
            {
                char type = str[1];
                char buffId = str2[0];
                char buffLevel = str2[1];
                int specBuffAttr = StringUtils.GetSpecBuffAttr(controler.GetMeVoByType<BaseRoleVo>(), str);
                BuffController buffController = controler.buffController;
                if (buffController != null)
                {
                    buffController.AddBuffWithAttr(buffId, buffLevel, type, specBuffAttr);
                }
            }
        }

        public static void StartDistanceBlink(BaseControler meControler, float moveDistance)
        {
            <StartDistanceBlink>c__AnonStorey11C storeyc = new <StartDistanceBlink>c__AnonStorey11C {
                moveDistance = moveDistance,
                meControler = meControler
            };
            EndChecker checker = new EndChecker(storeyc.<>m__1CE);
            if (<>f__am$cache0 == null)
            {
                <>f__am$cache0 = delegate (BaseDisplay basedisplay, MoveEventCheck check, float deltaTime) {
                    Transform transform = basedisplay.GoBase.transform;
                    basedisplay.Controller.characterController.SimpleMove((Vector3) (transform.forward * check.speed));
                    check.distance += check.speed / 30f;
                };
            }
            MoveOperation operation = <>f__am$cache0;
            if (<>f__am$cache1 == null)
            {
                <>f__am$cache1 = delegate (BaseDisplay basedisplay, MoveEventCheck check) {
                    basedisplay.Controller.StatuController.StopAnimation();
                    basedisplay.GoCloth.SetActive(false);
                    Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Monster"));
                };
            }
            StartCallback callback = <>f__am$cache1;
            EndCallback callback2 = new EndCallback(storeyc.<>m__1D1);
            MoveEventCheck mec = new MoveEventCheck {
                type = 2,
                speed = storeyc.moveDistance * 10f,
                moveOperation = operation,
                endChecker = checker,
                endFun = callback2,
                startFun = callback
            };
            storeyc.meControler.MoveController.AddMoveEvent(mec);
        }

        public static void StartEndured(ActionControler ac)
        {
            MonsterVo meVoByType = ac.Me.GetMeVoByType<MonsterVo>();
            if (meVoByType != null)
            {
                meVoByType.stateInfo.AddState(StateVo.ENDURED);
                SkinnedMeshRenderer componentInChildren = ac.Me.GoCloth.GetComponentInChildren<SkinnedMeshRenderer>();
                if (componentInChildren != null)
                {
                    StateVo stateInfo = ac.Me.GetMeVoByType<BaseRoleVo>().stateInfo;
                    ac.Me.Controller.StartCoroutine(DoColorChange(componentInChildren.material, stateInfo));
                }
            }
        }

        public static void StartFloat(BaseControler meControler, float speed, float totalTime)
        {
            if (!ShouldBlockThisSkillSubtype(20, meControler.Me))
            {
                float maxHeight = Singleton<ConfigConst>.Instance.GetConfigData("FLOAT_MAX_HEIGHT") * 0.001f;
                if (speed == 0f)
                {
                    speed = Singleton<ConfigConst>.Instance.GetConfigData("FLOAT_MOVING_SPEED") * 0.001f;
                }
                StartFloat(meControler, maxHeight, speed, totalTime);
            }
        }

        public static void StartFloat(BaseControler meControler, float maxHeight, float speed, float totalTime)
        {
            <StartFloat>c__AnonStorey11D storeyd = new <StartFloat>c__AnonStorey11D {
                totalTime = totalTime,
                maxHeight = maxHeight,
                speed = speed
            };
            EndChecker checker = new EndChecker(storeyd.<>m__1D2);
            MoveOperation operation = new MoveOperation(storeyd.<>m__1D3);
            if (<>f__am$cache2 == null)
            {
                <>f__am$cache2 = delegate (BaseDisplay basedisplay, MoveEventCheck check) {
                    basedisplay.GetMeVoByType<BaseRoleVo>().stateInfo.IsFloat = false;
                    basedisplay.Controller.ResetGravity();
                    AiControllerBase aiController = basedisplay.Controller.AiController;
                    if (aiController != null)
                    {
                        aiController.ClearCacheAiBehavior();
                    }
                    basedisplay.Controller.AttackController.ClearAttackList();
                    StatuControllerBase statuController = basedisplay.Controller.StatuController;
                    statuController.ResetAnimation();
                    CommandHandler.AddCommandStatic(statuController.gameObject, "idle", 1f);
                    statuController.SetStatu(0);
                    statuController.SetIdleImmediately();
                };
            }
            EndCallback callback = <>f__am$cache2;
            StartCallback callback2 = new StartCallback(storeyd.<>m__1D5);
            if (<>f__am$cache3 == null)
            {
                <>f__am$cache3 = delegate (IEnumerable<MoveEventCheck> list, MoveEventCheck check) {
                    IEnumerator<MoveEventCheck> enumerator = list.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            MoveEventCheck current = enumerator.Current;
                            check.distance += current.distance;
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                    return check;
                };
            }
            ReplaceOperation operation2 = <>f__am$cache3;
            MoveEventCheck mec = new MoveEventCheck {
                type = 1,
                speed = storeyd.speed,
                moveOperation = operation,
                endChecker = checker,
                endFun = callback,
                startFun = callback2,
                repFun = operation2
            };
            meControler.MoveController.AddMoveEvent(mec);
        }

        public static void StartFloatAndDamageAgain(BaseControler meControler, float speed, float totalTime, BaseDisplay attacker, uint skillId, int ratio = 0x3e8)
        {
            if (!ShouldBlockThisSkillSubtype(20, meControler.Me))
            {
                float maxHeight = Singleton<ConfigConst>.Instance.GetConfigData("FLOAT_MAX_HEIGHT") * 0.001f;
                if (speed == 0f)
                {
                    speed = Singleton<ConfigConst>.Instance.GetConfigData("FLOAT_MOVING_SPEED") * 0.001f;
                }
                StartFloatAndDamageAgain(meControler, maxHeight, speed, totalTime, attacker, skillId, ratio);
            }
        }

        public static void StartFloatAndDamageAgain(BaseControler meControler, float maxHeight, float speed, float totalTime, BaseDisplay attacker, uint skillId, int ratio = 0x3e8)
        {
            <StartFloatAndDamageAgain>c__AnonStorey11E storeye = new <StartFloatAndDamageAgain>c__AnonStorey11E {
                totalTime = totalTime,
                maxHeight = maxHeight,
                speed = speed,
                attacker = attacker,
                skillId = skillId,
                ratio = ratio
            };
            EndChecker checker = new EndChecker(storeye.<>m__1D7);
            MoveOperation operation = new MoveOperation(storeye.<>m__1D8);
            EndCallback callback = new EndCallback(storeye.<>m__1D9);
            StartCallback callback2 = new StartCallback(storeye.<>m__1DA);
            if (<>f__am$cache4 == null)
            {
                <>f__am$cache4 = delegate (IEnumerable<MoveEventCheck> list, MoveEventCheck check) {
                    IEnumerator<MoveEventCheck> enumerator = list.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            MoveEventCheck current = enumerator.Current;
                            check.distance += current.distance;
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                    return check;
                };
            }
            ReplaceOperation operation2 = <>f__am$cache4;
            MoveEventCheck mec = new MoveEventCheck {
                type = 4,
                speed = storeye.speed,
                moveOperation = operation,
                endChecker = checker,
                endFun = callback,
                startFun = callback2,
                repFun = operation2
            };
            meControler.MoveController.AddMoveEvent(mec);
        }

        public static void StartHpProtected(ActionControler controler, int type, int val)
        {
            BaseRoleVo.HpProtectedElem elem = new BaseRoleVo.HpProtectedElem((BaseRoleVo.HpProtectedType) type, val);
            controler.GetMeVoByType<BaseRoleVo>().HpProtected = elem;
        }

        public static void StartKnockBack(BaseControler controler, float totalDis, float totalTime)
        {
            <StartKnockBack>c__AnonStorey11F storeyf = new <StartKnockBack>c__AnonStorey11F {
                controler = controler,
                totalTime1 = totalTime * 0.001f
            };
            float num = totalDis * 0.001f;
            EndChecker checker = new EndChecker(storeyf.<>m__1DC);
            MoveOperation operation = new MoveOperation(storeyf.<>m__1DD);
            if (<>f__am$cache5 == null)
            {
                <>f__am$cache5 = (StartCallback) ((display, check) => display.Controller.Me.GetMeVoByType<BaseRoleVo>().stateInfo.AddState(StateVo.KNOCK_BACK));
            }
            StartCallback callback = <>f__am$cache5;
            EndCallback callback2 = new EndCallback(storeyf.<>m__1DF);
            if (<>f__am$cache6 == null)
            {
                <>f__am$cache6 = delegate (IEnumerable<MoveEventCheck> list, MoveEventCheck check) {
                    IEnumerator<MoveEventCheck> enumerator = list.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            MoveEventCheck current = enumerator.Current;
                            check.distance += current.distance;
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                    return check;
                };
            }
            ReplaceOperation operation2 = <>f__am$cache6;
            MoveEventCheck mec = new MoveEventCheck {
                type = 3,
                speed = num / storeyf.totalTime1,
                moveOperation = operation,
                endChecker = checker,
                endFun = callback2,
                startFun = callback,
                repFun = operation2
            };
            storeyf.controler.MoveController.AddMoveEvent(mec);
        }

        public static void StartMoveStraight(BaseControler meControler, float speed, float moveTime, bool hitStop, bool stopAnim)
        {
            <StartMoveStraight>c__AnonStorey120 storey = new <StartMoveStraight>c__AnonStorey120 {
                moveTime = moveTime,
                hitStop = hitStop,
                meControler = meControler,
                speed = speed,
                stopAnim = stopAnim
            };
            EndChecker checker = new EndChecker(storey.<>m__1E1);
            MoveOperation operation = new MoveOperation(storey.<>m__1E2);
            StartCallback callback = new StartCallback(storey.<>m__1E3);
            EndCallback callback2 = new EndCallback(storey.<>m__1E4);
            MoveEventCheck mec = new MoveEventCheck {
                type = 2,
                speed = storey.speed,
                moveOperation = operation,
                endChecker = checker,
                endFun = callback2,
                startFun = callback
            };
            storey.meControler.MoveController.AddMoveEvent(mec);
        }

        public static void StartSplit(ActionControler controler)
        {
        }

        public static void StartTaunted(ActionControler meControler, ActionDisplay targetDisplay)
        {
            SkillController skillController = meControler.SkillController;
            if (skillController != null)
            {
                Log.AI(skillController.gameObject, " ForceAttack " + targetDisplay);
                meControler.GetMeVo().stateInfo.BeTaunted = true;
                skillController.IsForcedToAttack = true;
                skillController.StartCoroutine(DoBeForcedToAttack(meControler, targetDisplay));
            }
        }

        public static void StartTemptation(ActionDisplay targetDisplay, ActionDisplay attackerDisplay)
        {
            ChangeCamp(targetDisplay, attackerDisplay);
        }

        public static void Summon()
        {
            SysSkillBaseVo skillBaseVo = new SysSkillBaseVo {
                subtype = 2,
                param_list = "1101101#1000#45#60#[[11,10,100,24]]|1101101#1000#-45#60#[[11,10,100,24]]"
            };
            Summon(AppMap.Instance.me, skillBaseVo);
        }

        public static void Summon(ActionDisplay display, SysSkillBaseVo skillBaseVo)
        {
            if (skillBaseVo.subtype == 2)
            {
                char[] separator = new char[] { '|' };
                string[] source = skillBaseVo.param_list.Split(separator);
                if (source.Length >= 1)
                {
                    if (<>f__am$cache7 == null)
                    {
                        <>f__am$cache7 = confString => confString.Split(new char[] { '#' });
                    }
                    IEnumerator<string[]> enumerator = source.Select<string, string[]>(<>f__am$cache7).GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            string[] current = enumerator.Current;
                            if (current.Length < 5)
                            {
                                return;
                            }
                            uint templateId = uint.Parse(current[0]);
                            float radius = int.Parse(current[1]) * 0.001f;
                            int angle = int.Parse(current[2]);
                            int num4 = int.Parse(current[3]);
                            string str = current[4];
                            MonsterVo vo = new MonsterVo {
                                monsterId = templateId
                            };
                            BaseRoleVo meVoByType = display.GetMeVoByType<BaseRoleVo>();
                            MonsterMgr.CalcBornMonsterAttr(meVoByType, vo, str);
                            DoSummon1(meVoByType, vo, templateId, radius, angle, (float) num4);
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                }
            }
        }

        public static PMapMon ToPMapMon(MonsterVo vo, Vector3 pos, float rotateY, uint givenTemplateId = 0, ulong parentId = 0, int parentTemplateId = 0, byte rebornCount = 0, float maxAliveTime = 0)
        {
            int unixTimestamp = tools.GetUnixTimestamp();
            int num2 = Convert.ToInt32(maxAliveTime);
            return new PMapMon { id = (ulong) unixTimestamp, monid = (givenTemplateId == 0) ? vo.monsterId : givenTemplateId, lvl = Convert.ToUInt16(vo.Level), parentMonId = (parentId == 0) ? vo.parent_id : parentId, parentMonTemplateId = (parentTemplateId == 0) ? vo.parent_template_id : parentTemplateId, rebornCount = (rebornCount == 0) ? Convert.ToByte(vo.reborn_count) : rebornCount, maxAliveTime = (maxAliveTime == 0f) ? vo.max_alive_time : num2, hp = vo.CurHp, hpFull = vo.Hp, x = pos.x.ToProtocolInt(), y = pos.y.ToProtocolInt(), z = pos.z.ToProtocolInt(), rotateY = Convert.ToInt32(rotateY), groupId = -((int) Time.time) };
        }

        [CompilerGenerated]
        private sealed class <DoBeForcedToAttack>c__Iterator58 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal BaseControler <$>meController;
            internal ActionDisplay <$>needToAttack;
            internal float <_directAtkRange>__2;
            internal SkillController <_meSkillController>__0;
            internal Transform <_selfTransform>__1;
            internal ActionVo <atkVo>__4;
            internal float <dis>__3;
            internal Vector3 <targetPos>__5;
            internal BaseControler meController;
            internal ActionDisplay needToAttack;

            internal void <>m__1E7(BaseControler c)
            {
                CommandHandler.AddCommandStatic(this.meController.gameObject, "idle", 1f);
            }

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<_meSkillController>__0 = this.meController.SkillController;
                        this.<_selfTransform>__1 = this.<_meSkillController>__0.transform;
                        this.<_directAtkRange>__2 = this.<_meSkillController>__0.UpdateDirectAtkRange();
                        break;

                    case 1:
                        break;

                    default:
                        goto Label_01CC;
                }
                if (this.<_meSkillController>__0.IsForcedToAttack)
                {
                    if (this.needToAttack.GoBase != null)
                    {
                        ActionVo vo;
                        this.<dis>__3 = Util.XZSqrMagnitude(this.needToAttack.GoBase.transform, this.<_selfTransform>__1);
                        if (this.<dis>__3 < this.<_directAtkRange>__2.Square())
                        {
                            vo = new ActionVo {
                                ActionType = "attack",
                                Attacker = this.meController.Me.GetMeByType<ActionDisplay>(),
                                Target = this.needToAttack
                            };
                            this.<atkVo>__4 = vo;
                        }
                        else
                        {
                            this.<targetPos>__5 = DamageCheck.GetFinalDestinationBeforeChase(this.needToAttack.GoBase.transform.position, this.<_selfTransform>__1, this.<_directAtkRange>__2 * 0.9f);
                            vo = new ActionVo {
                                Destination = this.<targetPos>__5,
                                ActionType = "run",
                                Attacker = this.meController.Me.GetMeByType<ActionDisplay>(),
                                IsAStarMove = true,
                                Target = this.needToAttack,
                                Callback = new MoveEndCallback(this.<>m__1E7)
                            };
                            this.<atkVo>__4 = vo;
                        }
                        this.meController.AttackController.AddAttackList(this.<atkVo>__4, false, false);
                        this.$current = this.meController.StartCoroutine(tools.WaitforSeveralFrames(6));
                        this.$PC = 1;
                        return true;
                    }
                }
                else
                {
                    this.$PC = -1;
                }
            Label_01CC:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <DoColorChange>c__Iterator57 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal int $PC;
            internal Material <$>mat;
            internal StateVo <$>stateVo;
            internal int <count>__20;
            internal float <curAmbient>__11;
            internal Vector3 <curDIRLightColor>__7;
            internal float <curDIRLightIntensity>__10;
            internal Vector3 <curLightColor>__6;
            internal Vector3 <curRimColor>__5;
            internal float <curRimFalloff>__9;
            internal float <curRimIntensity>__8;
            internal float <deltaAmbient>__15;
            internal float <deltaDIRLightIntensity>__14;
            internal float <deltaRimFalloff>__13;
            internal float <deltaRimIntensity>__12;
            internal float <deltaTime>__21;
            internal SkillUtil.EnduringVo <endVo>__18;
            internal int <factor>__19;
            internal Shader <oldShader>__0;
            internal SkillUtil.EnduringVo <originVo>__1;
            internal float <passTime>__4;
            internal float <rate>__16;
            internal int <repeatCount>__3;
            internal SkillUtil.EnduringVo <startVo>__17;
            internal SkillUtil.EnduringVo <targetVo>__2;
            internal Material mat;
            internal StateVo stateVo;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<oldShader>__0 = this.mat.shader;
                        if (this.<oldShader>__0.name != "X1/TransparentDetailCharacter")
                        {
                            this.mat.shader = Shader.Find("X1/TransparentDetailCharacter");
                        }
                        this.<originVo>__1 = new SkillUtil.EnduringVo(this.mat);
                        this.<targetVo>__2 = new SkillUtil.EnduringVo();
                        this.<repeatCount>__3 = 0;
                        this.<passTime>__4 = 0f;
                        this.<curRimColor>__5 = new Vector3();
                        this.<curLightColor>__6 = new Vector3();
                        this.<curDIRLightColor>__7 = new Vector3();
                        this.<curRimIntensity>__8 = 0f;
                        this.<curRimFalloff>__9 = 0f;
                        this.<curDIRLightIntensity>__10 = 0f;
                        this.<curAmbient>__11 = 0f;
                        this.<deltaRimIntensity>__12 = this.<targetVo>__2.RimIntensity - this.<originVo>__1.RimIntensity;
                        this.<deltaRimFalloff>__13 = this.<targetVo>__2.RimFalloff - this.<originVo>__1.RimFalloff;
                        this.<deltaDIRLightIntensity>__14 = this.<targetVo>__2.DIRLightIntensity - this.<originVo>__1.DIRLightIntensity;
                        this.<deltaAmbient>__15 = this.<targetVo>__2.Ambient - this.<originVo>__1.Ambient;
                        break;

                    case 1:
                        goto Label_01C6;

                    case 2:
                        break;

                    case 3:
                        goto Label_0675;

                    case 4:
                        this.$PC = -1;
                        goto Label_06CC;

                    default:
                        goto Label_06CC;
                }
                while (this.stateVo.IsInState(StateVo.ENDURED))
                {
                    this.<rate>__16 = this.<passTime>__4 * 2f;
                    if (this.<rate>__16 >= 1f)
                    {
                        this.<passTime>__4 = 0f;
                        this.<rate>__16 = 0f;
                        this.<repeatCount>__3++;
                        this.$current = null;
                        this.$PC = 1;
                        goto Label_06CE;
                    }
                Label_01C6:
                    this.<startVo>__17 = ((this.<repeatCount>__3 % 2) != 0) ? this.<targetVo>__2 : this.<originVo>__1;
                    this.<endVo>__18 = ((this.<repeatCount>__3 % 2) != 0) ? this.<originVo>__1 : this.<targetVo>__2;
                    this.<factor>__19 = ((this.<repeatCount>__3 % 2) != 0) ? -1 : 1;
                    this.<curRimColor>__5 = Vector3.Lerp(this.<startVo>__17.RimColorVec, this.<endVo>__18.RimColorVec, this.<rate>__16);
                    this.<curLightColor>__6 = Vector3.Lerp(this.<startVo>__17.LightColorVec, this.<endVo>__18.LightColorVec, this.<rate>__16);
                    this.<curDIRLightColor>__7 = Vector3.Lerp(this.<startVo>__17.DIRLightColorVec, this.<endVo>__18.DIRLightColorVec, this.<rate>__16);
                    this.<curRimIntensity>__8 = this.<startVo>__17.RimIntensity + ((this.<factor>__19 * this.<deltaRimIntensity>__12) * this.<rate>__16);
                    this.<curDIRLightIntensity>__10 = this.<startVo>__17.DIRLightIntensity + ((this.<factor>__19 * this.<deltaDIRLightIntensity>__14) * this.<passTime>__4);
                    this.<curAmbient>__11 = this.<startVo>__17.Ambient + ((this.<factor>__19 * this.<deltaAmbient>__15) * this.<passTime>__4);
                    this.<curRimFalloff>__9 = this.<startVo>__17.RimFalloff + ((this.<factor>__19 * this.<deltaRimFalloff>__13) * this.<passTime>__4);
                    this.mat.SetColor("_RimColor", SkillUtil.GetColorFromVector3(this.<curRimColor>__5, this.<originVo>__1.RimColor));
                    this.mat.SetColor("_LightColor", SkillUtil.GetColorFromVector3(this.<curLightColor>__6, this.<originVo>__1.LightColor));
                    this.mat.SetColor("_DIRLightColor", SkillUtil.GetColorFromVector3(this.<curDIRLightColor>__7, this.<originVo>__1.DIRLightColor));
                    this.mat.SetFloat("_RimIntensity", this.<curRimIntensity>__8);
                    this.mat.SetFloat("_DIRLightIntensity", this.<curDIRLightIntensity>__10);
                    this.mat.SetFloat("_Ambient", this.<curAmbient>__11);
                    this.mat.SetFloat("_RimFalloff", this.<curRimFalloff>__9);
                    this.<passTime>__4 += Time.deltaTime;
                    this.$current = null;
                    this.$PC = 2;
                    goto Label_06CE;
                }
                this.<deltaAmbient>__15 = this.<curAmbient>__11 - this.<originVo>__1.Ambient;
                this.<deltaDIRLightIntensity>__14 = this.<curDIRLightIntensity>__10 - this.<originVo>__1.DIRLightIntensity;
                this.<deltaRimIntensity>__12 = this.<curRimIntensity>__8 - this.<originVo>__1.RimIntensity;
                this.<deltaRimFalloff>__13 = this.<curRimFalloff>__9 - this.<originVo>__1.RimFalloff;
                this.<count>__20 = 0;
                while (this.<count>__20 < 10)
                {
                    this.<deltaTime>__21 = Time.deltaTime;
                    this.<curRimColor>__5 = Vector3.Lerp(this.<curRimColor>__5, this.<originVo>__1.RimColorVec, this.<deltaTime>__21 * 5f);
                    this.<curLightColor>__6 = Vector3.Lerp(this.<curLightColor>__6, this.<originVo>__1.LightColorVec, this.<deltaTime>__21 * 5f);
                    this.<curDIRLightColor>__7 = Vector3.Lerp(this.<curDIRLightColor>__7, this.<originVo>__1.DIRLightColorVec, this.<deltaTime>__21 * 5f);
                    this.mat.SetColor("_RimColor", SkillUtil.GetColorFromVector3(this.<curRimColor>__5, this.<originVo>__1.RimColor));
                    this.mat.SetColor("_LightColor", SkillUtil.GetColorFromVector3(this.<curLightColor>__6, this.<originVo>__1.LightColor));
                    this.mat.SetColor("_DIRLightColor", SkillUtil.GetColorFromVector3(this.<curDIRLightColor>__7, this.<originVo>__1.DIRLightColor));
                    this.mat.SetFloat("_RimIntensity", SkillUtil.GetLerpFloat(this.<originVo>__1.RimIntensity, this.<deltaRimIntensity>__12, this.<count>__20, 10f));
                    this.mat.SetFloat("_DIRLightIntensity", SkillUtil.GetLerpFloat(this.<originVo>__1.DIRLightIntensity, this.<deltaDIRLightIntensity>__14, this.<count>__20, 10f));
                    this.mat.SetFloat("_RimFalloff", SkillUtil.GetLerpFloat(this.<originVo>__1.RimFalloff, this.<deltaRimFalloff>__13, this.<count>__20, 10f));
                    this.mat.SetFloat("_Ambient", SkillUtil.GetLerpFloat(this.<originVo>__1.Ambient, this.<deltaAmbient>__15, this.<count>__20, 10f));
                    this.$current = null;
                    this.$PC = 3;
                    goto Label_06CE;
                Label_0675:
                    this.<count>__20++;
                }
                this.<originVo>__1.SetMaterial(this.mat);
                this.mat.shader = this.<oldShader>__0;
                this.$current = null;
                this.$PC = 4;
                goto Label_06CE;
            Label_06CC:
                return false;
            Label_06CE:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }

            object IEnumerator.Current
            {
                [DebuggerHidden]
                get
                {
                    return this.$current;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <Shield>c__AnonStorey121
        {
            internal ActionDisplay meDisplay;

            internal float <>m__1E5(ActionDisplay x)
            {
                return Util.XZSqrMagnitude(this.meDisplay.GoBase.transform.position, x.GoBase.transform.position);
            }
        }

        [CompilerGenerated]
        private sealed class <StartDistanceBlink>c__AnonStorey11C
        {
            internal BaseControler meControler;
            internal float moveDistance;

            internal bool <>m__1CE(Transform trans, MoveEventCheck check)
            {
                return (check.distance >= (this.moveDistance - 0.2));
            }

            internal void <>m__1D1(BaseDisplay basedisplay, MoveEventCheck check)
            {
                basedisplay.Controller.StatuController.ResetAnimation();
                basedisplay.GoCloth.SetActive(true);
                Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Monster"), false);
                this.meControler.SkillController.ShowBlinkEffect();
            }
        }

        [CompilerGenerated]
        private sealed class <StartFloat>c__AnonStorey11D
        {
            internal float maxHeight;
            internal float speed;
            internal float totalTime;

            internal bool <>m__1D2(Transform trans, MoveEventCheck check)
            {
                return (check.timecost >= this.totalTime);
            }

            internal void <>m__1D3(BaseDisplay basedisplay, MoveEventCheck check, float deltaTime)
            {
                float num = this.totalTime * 0.5f;
                CharacterController characterController = basedisplay.Controller.characterController;
                float distance = check.distance;
                if ((distance >= this.maxHeight) && (check.timecost <= num))
                {
                    check.speed = distance / num;
                }
                else if (check.timecost <= num)
                {
                    check.distance += this.speed * deltaTime;
                    characterController.Move((Vector3) ((Vector3.up * this.speed) * deltaTime));
                }
                else if (check.timecost > num)
                {
                    check.distance -= check.speed * deltaTime;
                    characterController.Move((Vector3) ((-Vector3.up * check.speed) * deltaTime));
                    if (characterController.collisionFlags != CollisionFlags.None)
                    {
                        check.timecost = this.totalTime;
                    }
                }
            }

            internal void <>m__1D5(BaseDisplay basedisplay, MoveEventCheck check)
            {
                basedisplay.GetMeVoByType<BaseRoleVo>().stateInfo.IsFloat = true;
                basedisplay.Controller.PauseGravity();
                AiControllerBase aiController = basedisplay.Controller.AiController;
                if (aiController != null)
                {
                    aiController.ClearCacheAiBehavior();
                }
                basedisplay.Controller.AttackController.ClearAttackList();
                basedisplay.Controller.StatuController.StopAnimation();
                check.speed = this.speed;
            }
        }

        [CompilerGenerated]
        private sealed class <StartFloatAndDamageAgain>c__AnonStorey11E
        {
            internal BaseDisplay attacker;
            internal float maxHeight;
            internal int ratio;
            internal uint skillId;
            internal float speed;
            internal float totalTime;

            internal bool <>m__1D7(Transform trans, MoveEventCheck check)
            {
                return (check.timecost >= this.totalTime);
            }

            internal void <>m__1D8(BaseDisplay basedisplay, MoveEventCheck check, float deltaTime)
            {
                float num = this.totalTime * 0.5f;
                CharacterController characterController = basedisplay.Controller.characterController;
                float distance = check.distance;
                if ((distance >= this.maxHeight) && (check.timecost <= num))
                {
                    check.speed = distance / num;
                }
                else if (check.timecost <= num)
                {
                    check.distance += this.speed * deltaTime;
                    characterController.Move((Vector3) ((Vector3.up * this.speed) * deltaTime));
                }
                else if (check.timecost > num)
                {
                    check.distance -= check.speed * deltaTime;
                    characterController.Move((Vector3) ((-Vector3.up * check.speed) * deltaTime));
                    if (characterController.collisionFlags != CollisionFlags.None)
                    {
                        check.timecost = this.totalTime;
                    }
                }
            }

            internal void <>m__1D9(BaseDisplay basedisplay, MoveEventCheck check)
            {
                SkillUtil.DamageCheckAgain(this.attacker, basedisplay.Controller.transform, this.skillId, this.ratio);
                basedisplay.GetMeVoByType<BaseRoleVo>().stateInfo.IsFloat = false;
                basedisplay.Controller.ResetGravity();
                AiControllerBase aiController = basedisplay.Controller.AiController;
                if (aiController != null)
                {
                    aiController.ClearCacheAiBehavior();
                }
                basedisplay.Controller.AttackController.ClearAttackList();
                StatuControllerBase statuController = basedisplay.Controller.StatuController;
                statuController.ResetAnimation();
                CommandHandler.AddCommandStatic(statuController.gameObject, "idle", 1f);
                statuController.SetStatu(0);
                statuController.SetIdleImmediately();
            }

            internal void <>m__1DA(BaseDisplay basedisplay, MoveEventCheck check)
            {
                basedisplay.GetMeVoByType<BaseRoleVo>().stateInfo.IsFloat = true;
                basedisplay.Controller.PauseGravity();
                AiControllerBase aiController = basedisplay.Controller.AiController;
                if (aiController != null)
                {
                    aiController.ClearCacheAiBehavior();
                }
                basedisplay.Controller.AttackController.ClearAttackList();
                basedisplay.Controller.StatuController.StopAnimation();
                check.speed = this.speed;
            }
        }

        [CompilerGenerated]
        private sealed class <StartKnockBack>c__AnonStorey11F
        {
            internal BaseControler controler;
            internal float totalTime1;

            internal bool <>m__1DC(Transform trans, MoveEventCheck check)
            {
                return (check.timecost >= this.totalTime1);
            }

            internal void <>m__1DD(BaseDisplay display, MoveEventCheck check, float time)
            {
                Transform transform = display.GoBase.transform;
                CharacterController characterController = display.characterController;
                characterController.SimpleMove((Vector3) (-transform.forward * check.speed));
                if (characterController.collisionFlags != CollisionFlags.None)
                {
                    check.timecost = this.totalTime1;
                }
            }

            internal void <>m__1DF(BaseDisplay display, MoveEventCheck check)
            {
                this.controler.Me.GetMeVoByType<BaseRoleVo>().stateInfo.RemoveState(StateVo.KNOCK_BACK);
            }
        }

        [CompilerGenerated]
        private sealed class <StartMoveStraight>c__AnonStorey120
        {
            internal bool hitStop;
            internal BaseControler meControler;
            internal float moveTime;
            internal float speed;
            internal bool stopAnim;

            internal bool <>m__1E1(Transform trans, MoveEventCheck check)
            {
                bool flag = check.timecost >= this.moveTime;
                bool flag2 = this.hitStop && this.meControler.SkillController.HasHitEnemy();
                return (flag || flag2);
            }

            internal void <>m__1E2(BaseDisplay baseDisplay, MoveEventCheck check, float deltaTime)
            {
                Transform transform = baseDisplay.GoBase.transform;
                baseDisplay.characterController.SimpleMove((Vector3) (transform.forward * this.speed));
            }

            internal void <>m__1E3(BaseDisplay baseDisplay, MoveEventCheck check)
            {
                StateVo stateInfo = this.meControler.Me.GetMeVoByType<BaseRoleVo>().stateInfo;
                StatuControllerBase statuController = baseDisplay.Controller.StatuController;
                stateInfo.AddState(StateVo.BLINK);
                if (this.stopAnim)
                {
                    statuController.StopAnimation();
                }
                if (!this.hitStop)
                {
                    Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Monster"));
                }
            }

            internal void <>m__1E4(BaseDisplay baseDisplay, MoveEventCheck check)
            {
                StateVo stateInfo = this.meControler.Me.GetMeVoByType<BaseRoleVo>().stateInfo;
                baseDisplay.Controller.StatuController.ResetAnimation();
                stateInfo.RemoveState(StateVo.BLINK);
                if (!this.hitStop)
                {
                    Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Monster"), true);
                }
            }
        }

        public class EnduringVo
        {
            public readonly float Ambient;
            public readonly Color DIRLightColor;
            public readonly Vector3 DIRLightColorVec;
            public readonly float DIRLightIntensity;
            public readonly Color LightColor;
            public readonly Vector3 LightColorVec;
            public readonly Color RimColor;
            public readonly Vector3 RimColorVec;
            public readonly float RimFalloff;
            public readonly float RimIntensity;

            public EnduringVo()
            {
                this.RimColorVec = SkillUtil.GetVector3("_RimColor");
                this.LightColorVec = SkillUtil.GetVector3("_LightColor");
                this.DIRLightColorVec = SkillUtil.GetVector3("_DIRLightColor");
                this.RimIntensity = SkillUtil.GetFloat("_RimIntensity");
                this.RimFalloff = SkillUtil.GetFloat("_RimFalloff");
                this.DIRLightIntensity = SkillUtil.GetFloat("_DIRLightIntensity");
                this.Ambient = SkillUtil.GetFloat("_Ambient");
            }

            public EnduringVo(Material mat)
            {
                this.RimColor = mat.GetColor("_RimColor");
                this.LightColor = mat.GetColor("_LightColor");
                this.DIRLightColor = mat.GetColor("_DIRLightColor");
                this.RimColorVec = SkillUtil.GetColorVector3(this.RimColor);
                this.LightColorVec = SkillUtil.GetColorVector3(this.LightColor);
                this.DIRLightColorVec = SkillUtil.GetColorVector3(this.DIRLightColor);
                this.RimIntensity = mat.GetFloat("_RimIntensity");
                this.RimFalloff = mat.GetFloat("_RimFalloff");
                this.DIRLightIntensity = mat.GetFloat("_DIRLightIntensity");
                this.Ambient = mat.GetFloat("_Ambient");
            }

            public void SetMaterial(Material mat)
            {
                mat.SetColor("_RimColor", this.RimColor);
                mat.SetColor("_LightColor", this.LightColor);
                mat.SetColor("_DIRLightColor", this.DIRLightColor);
                mat.SetFloat("_RimIntensity", this.RimIntensity);
                mat.SetFloat("_RimFalloff", this.RimFalloff);
                mat.SetFloat("_DIRLightIntensity", this.DIRLightIntensity);
                mat.SetFloat("_Ambient", this.Ambient);
            }
        }
    }
}

