﻿namespace com.u3d.bases.task
{
    using com.bases.utils;
    using com.u3d.bases.debug;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Task
    {
        private IList<string> acceptAfterSayList;
        private IList<string> acceptBeforeSayList;
        private int count;
        private IList<string> finishSayList;
        private string formatReward = string.Empty;
        private IList<TaskCondition> vector;

        public Task(string taskId, int state, string target)
        {
            this.taskId = taskId;
            this.state = state;
            this.vector = new List<TaskCondition>();
            this.parserTarget(target);
        }

        public IList<string> acceptAfterSay()
        {
            if (StringUtils.isEmpty(this.startTalk2))
            {
                return null;
            }
            if (this.acceptAfterSayList == null)
            {
                this.acceptAfterSayList = this.str2vector(this.startTalk2);
            }
            return this.acceptAfterSayList;
        }

        public IList<string> acceptBeforeSay()
        {
            if (StringUtils.isEmpty(this.startTalk1))
            {
                return null;
            }
            if (this.acceptBeforeSayList == null)
            {
                this.acceptBeforeSayList = this.str2vector(this.startTalk1);
            }
            return this.acceptBeforeSayList;
        }

        public IList<TaskCondition> conditionList()
        {
            return this.vector;
        }

        public TaskCondition curCondition()
        {
            IEnumerator<TaskCondition> enumerator = this.vector.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    TaskCondition current = enumerator.Current;
                    if (!current.isFinish() && (((current.sort == "3") || (current.sort == "4")) || (((current.sort == "5") || (current.sort == "6")) || (current.sort == "9"))))
                    {
                        return current;
                    }
                }
            }
            finally
            {
                if (enumerator == null)
                {
                }
                enumerator.Dispose();
            }
            return null;
        }

        public string curNpc()
        {
            if (this.state == 0)
            {
                return this.startNpc;
            }
            if (this.state == 1)
            {
                return this.startNpc;
            }
            if (this.state == 2)
            {
                return this.endNpc;
            }
            if (this.state == 3)
            {
                return this.startNpc;
            }
            return string.Empty;
        }

        public void dispose()
        {
            if (this.vector != null)
            {
                this.vector.Clear();
            }
            if (this.acceptAfterSayList != null)
            {
                this.acceptAfterSayList.Clear();
            }
            if (this.acceptBeforeSayList != null)
            {
                this.acceptBeforeSayList.Clear();
            }
            if (this.finishSayList != null)
            {
                this.finishSayList.Clear();
            }
            this.vector = null;
            this.formatReward = null;
            this.finishSayList = null;
            this.acceptAfterSayList = null;
            this.acceptBeforeSayList = null;
        }

        public void execute(string sort, string id, int sum = 1, string mapId = null, string backupId = null, string templateId = null)
        {
            if (this.state == 1)
            {
                if (this.vector.Count < 1)
                {
                    Log.info(this, "-execute() 任务ID:" + this.taskId + " 无条件直接完成！");
                    this.taskFinishCallback();
                }
                else
                {
                    int num = 0;
                    IEnumerator<TaskCondition> enumerator = this.vector.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            TaskCondition current = enumerator.Current;
                            current.match(sort, id, sum, mapId, backupId, templateId, null);
                            if (current.isFinish())
                            {
                                num++;
                            }
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                    Log.info(this, string.Concat(new object[] { "taskId:", this.taskId, ",isFinish:", num >= this.vector.Count, ",progress:", this.progress() }));
                    if (num >= this.vector.Count)
                    {
                        Log.info(this, "-execute() 任务ID:" + this.taskId + " 条件已完成！");
                        this.taskFinishCallback();
                    }
                }
            }
        }

        public IList<string> finishSay()
        {
            if (StringUtils.isEmpty(this.endTalk))
            {
                return null;
            }
            if (this.finishSayList == null)
            {
                this.finishSayList = this.str2vector(this.endTalk);
            }
            return this.finishSayList;
        }

        public TaskCondition getCondition(int index)
        {
            if (index >= 1)
            {
                IEnumerator<TaskCondition> enumerator = this.vector.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        TaskCondition current = enumerator.Current;
                        if (current.index == index)
                        {
                            return current;
                        }
                    }
                }
                finally
                {
                    if (enumerator == null)
                    {
                    }
                    enumerator.Dispose();
                }
            }
            return null;
        }

        public bool npcSayable(string npcId)
        {
            if (StringUtils.isEmpty(npcId) || !this.curNpc().Equals(npcId))
            {
                return false;
            }
            return (((this.state == 0) && !StringUtils.isEmpty(this.startTalk1)) || ((this.state == 1) || (((this.state == 2) && !StringUtils.isEmpty(this.endTalk)) || ((this.state == 3) && !StringUtils.isEmpty(this.startTalk1)))));
        }

        public int npcTaskState(string npcId)
        {
            if (!StringUtils.isEmpty(npcId) && this.curNpc().Equals(npcId))
            {
                if (this.state == 0)
                {
                    return 1;
                }
                if (this.state == 1)
                {
                    return 2;
                }
                if (this.state == 2)
                {
                    return 3;
                }
                if (this.state == 3)
                {
                    return 1;
                }
            }
            return 0;
        }

        private void parserTarget(string target)
        {
            if (!StringUtils.isEmpty(target) && (target.Length > 2))
            {
                target = target.Substring(1, target.Length - 2);
                int index = 0;
                while (target.Length > 0)
                {
                    index = target.IndexOf("}");
                    if (index == -1)
                    {
                        break;
                    }
                    TaskCondition item = new TaskCondition(target.Substring(0, index + 1));
                    target = target.Substring((target.Length < (index + 2)) ? (index + 1) : (index + 2));
                    this.vector.Add(item);
                }
            }
        }

        public string progress()
        {
            if ((this.vector == null) || (this.vector.Count < 1))
            {
                return string.Empty;
            }
            TaskCondition condition = this.vector[0];
            return ((condition.count <= 0) ? string.Empty : (condition.value + "/" + condition.count));
        }

        private IList<string> str2vector(string str)
        {
            int index = 0;
            string item = null;
            IList<string> list = new List<string>();
            str = str.Substring(1, str.Length - 2);
            while (str.Length > 0)
            {
                index = str.IndexOf("}");
                if (index == -1)
                {
                    return list;
                }
                item = str.Substring(0, index + 1);
                item = item.Substring(3, item.Length - 4);
                list.Add(item);
                str = str.Substring((str.Length < (index + 2)) ? (index + 1) : (index + 2));
            }
            return list;
        }

        private void taskFinishCallback()
        {
            this.state = 2;
            TaskCallback callback = TaskExecute.instance.callback;
            if (callback != null)
            {
                callback(this);
            }
        }

        public void update(int state, int count = -1, string c = null)
        {
            this.state = state;
            this.count = count;
            if ((state == 1) && (this.vector.Count < 1))
            {
                this.taskFinishCallback();
                return;
            }
            if (StringUtils.isEmpty(c))
            {
                return;
            }
            char[] separator = new char[] { '@' };
            foreach (string str in c.Split(separator))
            {
                char[] chArray2 = new char[] { '#' };
                string[] strArray = str.Split(chArray2);
                if ((strArray != null) && (strArray.Length >= 1))
                {
                    IEnumerator<TaskCondition> enumerator = this.vector.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            TaskCondition current = enumerator.Current;
                            if (current.index == int.Parse(strArray[0]))
                            {
                                current.value = int.Parse(strArray[1]);
                                continue;
                            }
                        }
                    }
                    finally
                    {
                        if (enumerator == null)
                        {
                        }
                        enumerator.Dispose();
                    }
                }
            }
            if (state != 1)
            {
                return;
            }
            bool flag = true;
            IEnumerator<TaskCondition> enumerator2 = this.vector.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    TaskCondition condition2 = enumerator2.Current;
                    if (!condition2.isFinish())
                    {
                        flag = false;
                        goto Label_0148;
                    }
                }
            }
            finally
            {
                if (enumerator2 == null)
                {
                }
                enumerator2.Dispose();
            }
        Label_0148:
            if (flag)
            {
                this.taskFinishCallback();
            }
        }

        public int copper { get; set; }

        public int cryle { get; set; }

        public string desc { get; set; }

        public int diam { get; set; }

        public string endNpc { get; set; }

        public string endTalk { get; set; }

        public int exp { get; set; }

        public int manual { get; set; }

        public int prestige { get; set; }

        public string rewardGoodsDesc { get; set; }

        public string rewardMoneyDesc
        {
            get
            {
                if (StringUtils.isEmpty(this.formatReward))
                {
                    string formatReward;
                    if (this.copper > 0)
                    {
                        formatReward = this.formatReward;
                        object[] objArray1 = new object[] { formatReward, "[name]铜钱[-] [value]", this.copper, "[-] " };
                        this.formatReward = string.Concat(objArray1);
                    }
                    if (this.diam > 0)
                    {
                        formatReward = this.formatReward;
                        object[] objArray2 = new object[] { formatReward, "[name]元宝[-] [value]", this.diam, "[-] " };
                        this.formatReward = string.Concat(objArray2);
                    }
                    if (this.exp > 0)
                    {
                        formatReward = this.formatReward;
                        object[] objArray3 = new object[] { formatReward, "[name]经验[-] [value]", this.exp, "[-] " };
                        this.formatReward = string.Concat(objArray3);
                    }
                }
                return this.formatReward;
            }
        }

        public string startNpc { get; set; }

        public string startTalk1 { get; set; }

        public string startTalk2 { get; set; }

        public int state { get; set; }

        public string taskId { get; set; }

        public string taskName { get; set; }

        public int taskType { get; set; }

        public string tip { get; set; }
    }
}

