﻿namespace com.u3d.bases.task
{
    using com.bases.utils;
    using System;
    using System.Runtime.InteropServices;

    public class TaskCondition
    {
        public string backupId;
        internal int count;
        internal int index;
        public string mapId;
        public string sort;
        public string targetId;
        public string templateId;
        internal int value;

        public TaskCondition(string target)
        {
            target = target.Substring(1, target.Length - 2);
            char[] separator = new char[] { ',' };
            this.init(target.Split(separator));
        }

        private void init(string[] split)
        {
            this.index = int.Parse(split[0]);
            this.sort = split[1];
            this.backupId = split[2];
            this.mapId = split[3];
            this.targetId = split[4];
            this.templateId = split[5];
            this.count = int.Parse(split[6]);
        }

        public bool isFinish()
        {
            if (this.count < 1)
            {
                if (this.value >= 1)
                {
                    return true;
                }
            }
            else if (this.value >= this.count)
            {
                return true;
            }
            return false;
        }

        internal void match(string sort, string id, int sum = 1, string mapId = null, string backupId = null, string cardId = null, string templateId = null)
        {
            if (((StringUtils.isEquals(sort, this.sort) && ((sort.Equals("7") || sort.Equals("9")) || (sort.Equals("10") || StringUtils.isEquals(id, this.targetId)))) && (((!sort.Equals("4") && !sort.Equals("5")) && !sort.Equals("6")) || StringUtils.isEquals(templateId, this.templateId))) && (((!sort.Equals("3") && !sort.Equals("5")) && !sort.Equals("9")) || StringUtils.isEquals(mapId, this.mapId)))
            {
                this.value += sum;
            }
        }
    }
}

