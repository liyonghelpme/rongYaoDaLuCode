﻿namespace com.u3d.bases.task
{
    using System;

    public class TaskConst
    {
        public const string C_10 = "10";
        public const string C_3 = "3";
        public const string C_4 = "4";
        public const string C_5 = "5";
        public const string C_6 = "6";
        public const string C_7 = "7";
        public const string C_8 = "8";
        public const string C_9 = "9";
        public const int DAY_TASK = 3;
        public const int FOR_TASK = 4;
        public const int MAIN_TASK = 1;
        public const int NEW_TAKS = 5;
        public const int NPC_TASK_ACCPET = 1;
        public const int NPC_TASK_DOING = 2;
        public const int NPC_TASK_FINISH = 3;
        public const int NPC_TASK_NO = 0;
        public const int STATE_ACCEPT = 0;
        public const int STATE_DOING = 1;
        public const int STATE_FINISH = 2;
        public const int STATE_UNACCEPT = 3;
        public const int SUB_TASK = 2;
        public const string ZERO = "0";

        public static string getNpcTaskState(int state)
        {
            switch (state)
            {
                case 1:
                    return "可接";

                case 2:
                    return "已接";

                case 3:
                    return "完成";
            }
            return "没有";
        }

        public static string getTaskState(int state)
        {
            string str = string.Empty;
            switch (state)
            {
                case 0:
                    return "可接";

                case 1:
                    return "已接";

                case 2:
                    return "完成";

                case 3:
                    return "不可接";
            }
            return str;
        }

        public static string getTaskType(int type)
        {
            string str = string.Empty;
            switch (type)
            {
                case 1:
                    return "主";

                case 2:
                    return "支";

                case 3:
                    return "日";

                case 4:
                    return "军";
            }
            return str;
        }
    }
}

