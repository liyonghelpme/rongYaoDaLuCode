﻿namespace com.u3d.bases.task
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public class TaskExecute
    {
        internal TaskCallback callback;
        public static TaskExecute instance = new TaskExecute();
        private IList<Task> npcTaskList = new List<Task>();
        private Dictionary<string, Task> taskList = new Dictionary<string, Task>();

        public Task add(string taskId, int state, int count, string target)
        {
            if (!this.valid(taskId))
            {
                return null;
            }
            if ((state < 0) || (state > 3))
            {
                return null;
            }
            Task task = this.get(taskId);
            if (task == null)
            {
                task = new Task(taskId, state, target);
                this.taskList[taskId] = task;
            }
            return task;
        }

        public void addTaskFinishCallback(TaskCallback callback)
        {
            this.callback = callback;
        }

        public void execute(string sort, string id, int sum = 1, string mapId = null, string backupId = null, string templateId = null)
        {
            foreach (Task task in this.taskList.Values)
            {
                task.execute(sort, id, sum, mapId, backupId, templateId);
            }
        }

        public Task get(string taskId)
        {
            if (!this.valid(taskId))
            {
                return null;
            }
            return (!this.taskList.ContainsKey(taskId) ? null : this.taskList[taskId]);
        }

        public string getCurNpc(string taskId)
        {
            Task task = this.get(taskId);
            return ((task == null) ? null : task.curNpc());
        }

        public Task getMainTask()
        {
            foreach (Task task in this.taskList.Values)
            {
                if (task.taskType == 1)
                {
                    return task;
                }
            }
            return null;
        }

        public string getStartNpc(string taskId)
        {
            Task task = this.get(taskId);
            return ((task == null) ? null : task.startNpc);
        }

        public IList<Task> npcTask(string npcId)
        {
            this.npcTaskList.Clear();
            foreach (Task task in this.taskList.Values)
            {
                if (task.npcSayable(npcId))
                {
                    this.npcTaskList.Add(task);
                }
            }
            return this.npcTaskList;
        }

        public int npcTaskState(string npcId)
        {
            int num = 0;
            int num2 = 0;
            foreach (Task task in this.taskList.Values)
            {
                num = task.npcTaskState(npcId);
                switch (num)
                {
                    case 1:
                        return num;

                    case 3:
                        num2 = num;
                        break;

                    default:
                        if ((num == 2) && (num2 != 3))
                        {
                            num2 = num;
                        }
                        break;
                }
            }
            return num2;
        }

        public bool remove(string taskId)
        {
            if (!this.valid(taskId))
            {
                return false;
            }
            return this.taskList.Remove(taskId);
        }

        public void update(string taskId, int state, int count = -1, string c = null)
        {
            if ((state >= 0) && (state <= 3))
            {
                Task task = this.get(taskId);
                if (task != null)
                {
                    task.update(state, count, c);
                }
            }
        }

        private bool valid(string str)
        {
            return ((str != null) && (str.Trim().Length > 0));
        }

        public Dictionary<string, Task> allTask
        {
            get
            {
                return this.taskList;
            }
        }
    }
}

