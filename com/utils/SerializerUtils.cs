﻿namespace com.utils
{
    using com.game.manager;
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;

    public class SerializerUtils
    {
        public static object binaryDerialize(byte[] bytes)
        {
            try
            {
                MemoryStream serializationStream = new MemoryStream(bytes);
                object obj2 = new BinaryFormatter { Binder = new UBinder() }.Deserialize(serializationStream);
                serializationStream.Close();
                serializationStream.Dispose();
                return obj2;
            }
            catch (Exception exception)
            {
                Debug.LogError("-binaryDeserialize() 基于二进制反序列化失败:" + exception.Message);
            }
            return null;
        }

        public static object jsonDerialize(byte[] bytes)
        {
            return null;
        }
    }
}

